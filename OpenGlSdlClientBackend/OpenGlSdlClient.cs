﻿using ACClientLib;
using ACClientLib.Lib;
using ACClientLib.Lib.Networking.Constants;
using ACClientLib.Lib.Networking.Transports;
using ACDesktopClient.Lib;
using ImGuiNET;
using Microsoft.Extensions.Logging;
using System.Numerics;
using System.Reflection;
using System.Reflection.Emit;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using static System.Net.Mime.MediaTypeNames;

namespace OpenGlSdlClientBackend {
    public enum AppState {
        SERVER_SELECT,
        CONNECTING,
        CHARACTER_SELECT,
        ENTERING_GAME,
        IN_GAME,
        ERROR
    }

    public class AppStateChangedEventArgs : EventArgs {
        public AppState OldState { get; }
        public AppState NewState { get; }
        public AppStateChangedEventArgs(AppState oldState, AppState newState) {
            OldState = oldState;
            NewState = newState;
        }
    }
    public class OpenGlSdlClient {
        public Config Config { get; } = new Config();
        public IRenderer Renderer { get; private set; }

        private AppState _appState;
        public AppState State {
            get => _appState;
            set {
                if (_appState == value)
                    return;
                OnAppStateChanged?.Invoke(this, new AppStateChangedEventArgs(_appState, value));
                _appState = value;
            }
        }

        private bool _useThreading;
        private IAsyncDatReader? _datReader;
        private INetworkTransport? _netTransport;

        public ImGuiLogger Log { get; private set; }
        public ACClient ACClient { get; private set; }

        public event EventHandler<AppStateChangedEventArgs> OnAppStateChanged;
        public event EventHandler<EventArgs> OnStarted;

        private string _host = "127.0.0.1";
        private int _port = 9000;
        private string _username = "Admin";
        private string _password = "test";
        private string _datDirectory = @"C:\ACE\Dats\";

        private string _error = "";
        private bool _didOpen;
        private bool _didOpenLoggingIn;

        private StringBuilder _chatBuilder = new StringBuilder();
        private string _chatText = "";
        private bool _autoScrollLogWindow;
        private bool _scrollToBottom;
        private string _consoleInput = "";
        private string setNext = null;

        private class InputHistory {
            public List<string> History { get; } = new List<string>();
            public int HistoryIndex = 0;
        }
        private Dictionary<string, InputHistory> _inputHistories = new Dictionary<string, InputHistory>();

        public OpenGlSdlClient(IAsyncDatReader? datReader = null, INetworkTransport? netTransport = null, bool useThreading = true) {
            _useThreading = useThreading;
            _datReader = datReader;
            _netTransport = netTransport;
            Log = new ImGuiLogger(LogLevel.Information);

            var writer = new ConsoleWriter();
            writer.WriteEvent += (s, e) => { };
            writer.WriteLineEvent += (s, e) => {
                if (!string.IsNullOrWhiteSpace(e?.Value)) {
                    Log.Log(e.Value);
                }
            };
            Console.SetOut(writer);
            Renderer = new OpenGLRenderer();

            Renderer.OnRender2D += Renderer_OnRender2D;
        }

        public void Run() {
            var start = DateTime.UtcNow;
            while (true) {
                double time = (DateTime.UtcNow - start).TotalSeconds;
                Update(time);
            }
        }

        public void Update(double time) {
            Renderer.Update(time);
            Renderer.Render(time);
            if (!_useThreading) {
                ACClient?.Update();
            }
        }

        public void StartClient() {
            ACClient = new ACClient(new ACClientOptions() {
                DatReader = _datReader ?? new FSDatReader(_datDirectory),
                Logger = Log,
                NetTransport = _netTransport ?? new UDPTransport()
            });

            ACClient.OnStateChanged += ACClient_OnStateChanged;
            ACClient.Connect(_host, _port, _username, _password, new CancellationToken(), _useThreading);

            OnStarted?.Invoke(this, EventArgs.Empty);
        }

        private void ACClient_OnStateChanged(object? sender, UtilityBelt.Scripting.Events.StateChangedEventArgs e) {
            Log.Log($"State Changed: {e.NewState}");
            switch (e.NewState) {
                case ClientState.Character_Select_Screen:
                    State = AppState.CHARACTER_SELECT;
                    break;
                case ClientState.Entering_Game:
                    State = AppState.ENTERING_GAME;
                    break;
                case ClientState.In_Game:
                    ACClient.ScriptHost.Scripts.GameState.WorldState.OnChatText += WorldState_OnChatText;
                    State = AppState.IN_GAME;
                    break;
            }
        }
        private void WorldState_OnChatText(object? sender, UtilityBelt.Scripting.Events.ChatEventArgs e) {
            var chatString = new StringBuilder();

            if (e.Room != ChatChannel.None) {
                chatString.Append($"[{e.Room}] ");
            }

            if (!string.IsNullOrEmpty(e.SenderName)) {
                chatString.Append($"{e.SenderName} says, \"{e.Message}\"");
            }
            else {
                chatString.AppendLine(e.Message);
            }

            _chatBuilder.AppendLine(chatString.ToString());
            _chatText = _chatBuilder.ToString();
            _scrollToBottom = true;
        }

        private void ShowError(string error) {
            _error = error;
            State = AppState.ERROR;
        }

        private void Renderer_OnRender2D(object? sender, EventArgs e) {
            Log.Render();
            switch (State) {
                case AppState.SERVER_SELECT:
                    RenderServerSelect();
                    break;
                case AppState.CONNECTING:
                    RenderConnecting();
                    break;
                case AppState.CHARACTER_SELECT:
                    RenderCharacterSelect();
                    break;
                case AppState.ENTERING_GAME:
                    RenderEnteringGame();
                    break;
                case AppState.IN_GAME:
                    RenderGame();
                    break;
                case AppState.ERROR:
                    RenderError();
                    break;
            }
        }

        private void RenderServerSelect() {
            Vector2 center = ImGui.GetMainViewport().GetCenter();
            ImGui.SetNextWindowPos(center, ImGuiCond.FirstUseEver, new Vector2(0.5f, 0.5f));
            if (ImGui.Begin("Select a Server / Login", ImGuiWindowFlags.AlwaysAutoResize)) {
                ImGui.InputText("Dat Directory", ref _datDirectory, 2048);
                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.InputText("Host", ref _host, 150);
                ImGui.InputInt("Port", ref _port);
                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.InputText("Username", ref _username, 150);
                ImGui.InputText("Password", ref _password, 250, ImGuiInputTextFlags.Password);

                if (ImGui.Button("Login")) {
                    State = AppState.CONNECTING;
                    if (_useThreading) {
                        StartClient();
                    }
                }
            }
            ImGui.End();
        }

        private void RenderConnecting() {
            Vector2 center = ImGui.GetMainViewport().GetCenter();
            ImGui.SetNextWindowPos(center, ImGuiCond.FirstUseEver, new Vector2(0.5f, 0.5f));

            if (ImGui.Begin($"Logging In", ImGuiWindowFlags.AlwaysAutoResize)) {
                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.Text("Logging in....");
                ImGui.Spacing();
                ImGui.Spacing();
            }
            ImGui.End();
        }

        private void RenderCharacterSelect() {
            Vector2 center = ImGui.GetMainViewport().GetCenter();
            ImGui.SetNextWindowPos(center, ImGuiCond.FirstUseEver, new Vector2(0.5f, 0.5f));
            if (ImGui.Begin("Choose a character:", ImGuiWindowFlags.AlwaysAutoResize)) {
                foreach (var character in ACClient.ScriptHost.Scripts.GameState.Characters) {
                    if (ImGui.Selectable($"{character.Name} (0x{character.Id:X8})")) {
                        ACClient.EnterWorld(character.Id);
                    }
                }
            }
            ImGui.End();
        }

        private void RenderEnteringGame() {
            Vector2 center = ImGui.GetMainViewport().GetCenter();
            ImGui.SetNextWindowPos(center, ImGuiCond.FirstUseEver, new Vector2(0.5f, 0.5f));

            if (ImGui.Begin($"Entering Game", ImGuiWindowFlags.AlwaysAutoResize)) {
                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.Text($"Entering Game as {ACClient.ScriptHost.Scripts.GameState.Character?.Weenie?.Name}");
                ImGui.Spacing();
                ImGui.Spacing();
            }
            ImGui.End();
        }

        private unsafe void RenderGame() {
            ImGui.SetNextWindowSize(new Vector2(400, 500), ImGuiCond.FirstUseEver);
            Vector2 center = ImGui.GetMainViewport().GetCenter();
            ImGui.SetNextWindowPos(center, ImGuiCond.FirstUseEver, new Vector2(0.5f, 0.5f));
            if (ImGui.Begin("RenderGame")) {
                if (ImGui.BeginPopup("Options")) {
                    ImGui.Checkbox("AutoScroll", ref _autoScrollLogWindow);
                    ImGui.EndPopup();
                }
                if (ImGui.Button("Options")) {
                    ImGui.OpenPopup("Options");
                }
                ImGui.SameLine();
                if (ImGui.Button("Clear Chat")) {
                    _chatBuilder.Clear();
                    _chatText = "";
                }

                var scrollRegionSize = ImGui.GetContentRegionAvail() - new Vector2(0, 20);
                if (ImGui.BeginChild("ScrollingRegion", scrollRegionSize, false, ImGuiWindowFlags.HorizontalScrollbar)) {
                    ImGui.TextUnformatted(_chatText);

                    if (_scrollToBottom || (_autoScrollLogWindow && ImGui.GetScrollY() >= ImGui.GetScrollMaxY()))
                        ImGui.SetScrollHereY(1.0f);

                    _scrollToBottom = false;
                }
                ImGui.EndChild();
                bool reclaim_focus = false;
                ImGuiInputTextFlags input_text_flags = ImGuiInputTextFlags.EnterReturnsTrue | ImGuiInputTextFlags.CallbackHistory | ImGuiInputTextFlags.CallbackEdit | ImGuiInputTextFlags.CallbackAlways;

                var callback = (ImGuiInputTextCallback)((data) => {
                    var dataPtr = new ImGuiInputTextCallbackDataPtr(data);

                    switch (dataPtr.EventFlag) {
                        case ImGuiInputTextFlags.CallbackHistory:
                            int historyPosition = -1;
                            int prevHistoryPosition = -1;
                            int historyLength = 0;
                            if (_inputHistories.TryGetValue("main", out InputHistory history)) {
                                prevHistoryPosition = history.HistoryIndex;
                                historyPosition = history.HistoryIndex;
                                historyLength = history.History.Count;
                            }
                            if (dataPtr.EventKey == ImGuiKey.UpArrow && historyLength > 0) {
                                historyPosition = historyPosition + 1;
                                if (historyPosition > historyLength - 1)
                                    historyPosition = -1;
                            }
                            else if (dataPtr.EventKey == ImGuiKey.DownArrow && historyLength > 0) {
                                historyPosition = historyPosition - 1;
                                if (historyPosition < -1)
                                    historyPosition = historyLength - 1;
                            }

                            // A better implementation would preserve the data on the current input line along with cursor position.
                            if (prevHistoryPosition != historyPosition && history != null) {
                                history.HistoryIndex = historyPosition;
                                var newValue = (historyPosition >= 0) ? history.History[historyPosition] : "";
                                dataPtr.DeleteChars(0, dataPtr.BufTextLen);
                                dataPtr.InsertChars(0, newValue);
                            }
                            break;
                    }
                    return 0;
                });

                var cursor = ImGui.GetCursorPos();

                if (setNext != null) {
                    _consoleInput = setNext;
                    setNext = null;
                }

                ImGui.PushItemWidth(ImGui.GetFontSize() * -4);
                if (ImGui.InputText("Chat", ref _consoleInput, 2048, input_text_flags, callback)) {
                    if (!string.IsNullOrWhiteSpace(_consoleInput)) {
                        if (!_inputHistories.ContainsKey("main")) {
                            _inputHistories.Add("main", new InputHistory());
                        }
                        _inputHistories["main"].History.Insert(0, _consoleInput);
                        _inputHistories["main"].HistoryIndex = -1;
                        ACClient.InvokeChatParser(_consoleInput);
                        _consoleInput = "";
                        reclaim_focus = true;
                    }
                }
                ImGui.PopItemWidth();

                // Auto-focus on window apparition
                ImGui.SetItemDefaultFocus();
                if (reclaim_focus)
                    ImGui.SetKeyboardFocusHere(-1); // Auto focus previous widget
            }
            ImGui.End();
        }

        private void RenderError() {
            Vector2 center = ImGui.GetMainViewport().GetCenter();
            ImGui.SetNextWindowPos(center, ImGuiCond.Appearing, new Vector2(0.5f, 0.5f));

            if (ImGui.BeginPopupModal($"Error")) {
                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.Text(_error);
                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.Separator();

                if (ImGui.Button("Ok", new Vector2(120, 0))) {
                    //ImGui.CloseCurrentPopup();
                }

                ImGui.SetItemDefaultFocus();
                ImGui.EndPopup();
            }

            if (!_didOpen) {
                ImGui.OpenPopup($"Error");
                _didOpen = true;
            }
        }
    }
}