﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveEngine.Bindings;
using Vector3 = System.Numerics.Vector3;
using Matrix4x4 = OpenGL.Matrix4x4f;

namespace ACDesktopClient.Lib {
    public class Camera {
        public Vector3 Position { get; set; } = new Vector3(0, 0, -129);

        private bool _viewDirty;

        public Vector3 Right { get; set; } = Vector3.UnitX;
        public Vector3 Up { get; set; } = Vector3.UnitY;
        public Vector3 Look { get; set; } = Vector3.UnitZ;

        public float NearZ { get; private set; }
        public float FarZ { get; private set; }
        public float Aspect { get; private set; }
        public float FovY { get; private set; }
        public float FovX {
            get {
                float halfWidth = 0.5f * NearWindowWidth;
                return 2.0f * (float)Math.Atan(halfWidth / NearZ);
            }
        }
        public float NearWindowHeight { get; private set; }
        public float NearWindowWidth => Aspect * NearWindowHeight;
        public float FarWindowHeight { get; private set; }
        public float FarWindowWidth => Aspect * FarWindowHeight;

        public Matrix4x4 View { get; private set; } = Matrix4x4.Identity;
        public Matrix4x4 Projection { get; private set; } = Matrix4x4.Identity;

        public Matrix4x4 ViewProj => View * Projection;

        public float MovementSpeed { get; private set; } = 0.00004f;
        public float MouseSensitivity { get; private set; } = 10f;

        private bool _isLooking = false;
        private Point _dragStartMousePos;

        public Dictionary<SDL.SDL_Keycode, bool> KeyStates = new Dictionary<SDL.SDL_Keycode, bool>();
        public Dictionary<uint, bool> MouseButtonStates = new Dictionary<uint, bool>();
        private int _lastMouseX;
        private int _lastMouseY;

        public Camera() {
            foreach (var e in Enum.GetValues(typeof(SDL.SDL_Keycode)).Cast<SDL.SDL_Keycode>()) {
                KeyStates.Add(e, false);
            }
            MouseButtonStates.Add(SDL.SDL_BUTTON_LEFT, false);
            MouseButtonStates.Add(SDL.SDL_BUTTON_MIDDLE, false);
            MouseButtonStates.Add(SDL.SDL_BUTTON_RIGHT, false);
            MouseButtonStates.Add(SDL.SDL_BUTTON_X1, false);
            MouseButtonStates.Add(SDL.SDL_BUTTON_X2, false);
        }

        public void Initialize() {

        }

        internal void HandleEvent(SDL.SDL_Event e) {
            switch (e.type) {
                case SDL.SDL_EventType.SDL_KEYDOWN:
                    KeyStates[e.key.keysym.sym] = true;
                    break;
                case SDL.SDL_EventType.SDL_KEYUP:
                    KeyStates[e.key.keysym.sym] = false;
                    break;
                case SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN:
                    MouseButtonStates[e.button.button] = true;
                    break;
                case SDL.SDL_EventType.SDL_MOUSEBUTTONUP:
                    MouseButtonStates[e.button.button] = false;
                    break;
            }
        }

        public void Update(double frameTime) {
            SDL.SDL_GetMouseState(out var mouseX, out var mouseY);

            if (MouseButtonStates[SDL.SDL_BUTTON_RIGHT]) {
                SDL.SDL_ShowCursor(0);
                SDL.SDL_CaptureMouse(SDL.SDL_bool.SDL_TRUE);
                float dx = MathUtil.DegreesToRadians(-(mouseX - _lastMouseX) * MouseSensitivity);
                float dy = MathUtil.DegreesToRadians((mouseY - _lastMouseY) * MouseSensitivity / 100f);

                SetYaw(dx);
                SetPitch(dy);
                _viewDirty = true;
                _isLooking = true;
            }
            else {
                _isLooking = false;
                _viewDirty = true;
                SDL.SDL_CaptureMouse(SDL.SDL_bool.SDL_FALSE);
                SDL.SDL_ShowCursor(1);
            }

            if (KeyStates[SDL.SDL_Keycode.SDLK_w]) {
                Position += Look * CapSpeed(frameTime);
                _viewDirty = true;
            }

            if (KeyStates[SDL.SDL_Keycode.SDLK_s]) {
                Position -= Look * CapSpeed(frameTime);
                _viewDirty = true;
            }

            if (KeyStates[SDL.SDL_Keycode.SDLK_a]) {
                Position += Right * CapSpeed(frameTime);
                _viewDirty = true;
            }

            if (KeyStates[SDL.SDL_Keycode.SDLK_d]) {
                Position -= Right * CapSpeed(frameTime);
                _viewDirty = true;
            }

            if (KeyStates[SDL.SDL_Keycode.SDLK_q]) {
                Position -= Up * CapSpeed(frameTime);
                _viewDirty = true;
            }

            if (KeyStates[SDL.SDL_Keycode.SDLK_e]) {
                Position += Up * CapSpeed(frameTime);
                _viewDirty = true;
            }

            UpdateViewMatrix();

            _lastMouseX = mouseX;
            _lastMouseY = mouseY;
        }

        private float CapSpeed(double speed) {
            return speed > 0 ? 1f : -1f;
            //return MovementSpeed * (float)speed;
            //var shift = ImGui.GetIO().KeyShift;
            //return MovementSpeed * (shift ? MyClass.Config.TurboSpeedMultiplier : 1f) * (float)speed;
        }


        public void Render(double time) {
            UpdateLens();
        }

        public void UpdateLens() {
            //var aspectRatio = (float)Program.Renderer.Width / (float)Program.Renderer.Height;
            //SetLens(Program.Config.FieldOfView, aspectRatio, Program.Config.ScreenNear, Program.Config.ScreenDepth);
        }

        public void SetLens(float fovY, float aspect, float zn, float zf) {
            FovY = fovY;
            Aspect = aspect;
            NearZ = zn;
            FarZ = zf;

            NearWindowHeight = 2.0f * zn * (float)Math.Tan(0.5f * fovY);
            FarWindowHeight = 2.0f * zf * (float)Math.Tan(0.5f * fovY);

            Projection = Matrix4x4.Perspective(FovY, aspect, zn, zf);
        }

        public void GoTo(Vector3 pos) {
            Position = pos;
            _viewDirty = true;
        }

        public void LookAt(Vector3 pos, Vector3 target, Vector3 up) {
            Position = pos;
            Look = Vector3.Normalize(target - pos);
            Right = Vector3.Normalize(Vector3.Cross(Up, Look));
            Up = Vector3.Normalize(Vector3.Cross(Look, Right));
            _viewDirty = true;
        }

        public void SetPitch(float angle) {
            // Rotate up and look vector about the right vector.
            Matrix4x4 r = CreateFromAxisAngle(Right, angle);

            Up = Vector3.Normalize(TransformNormal(Up, r));
            Look = Vector3.Normalize(TransformNormal(Look, r));

            _viewDirty = true;
        }

        public void SetYaw(float angle) {
            // Rotate the basis vectors about the world y-axis.

            Matrix4x4 r = Matrix4x4.RotatedY(angle);

            Right = Vector3.Normalize(TransformNormal(Right, r));
            Up = Vector3.Normalize(TransformNormal(Up, r));
            Look = Vector3.Normalize(TransformNormal(Look, r));

            _viewDirty = true;
        }

        public Vector3 TransformNormal(Vector3 normal, Matrix4x4 matrix) {
            return new Vector3 {
                X = normal.X * matrix[0, 0] + normal.Y * matrix[1, 0] + normal.Z * matrix[2, 0],
                Y = normal.X * matrix[0, 1] + normal.Y * matrix[1, 1] + normal.Z * matrix[2, 1],
                Z = normal.X * matrix[0, 2] + normal.Y * matrix[1, 2] + normal.Z * matrix[2, 2]
            };
        }

        public static Matrix4x4 CreateFromAxisAngle(Vector3 axis, float angle) {
            Matrix4x4 matrix = Matrix4x4.Identity;
            float x = axis.X;
            float y = axis.Y;
            float z = axis.Z;
            float num2 = (float)Math.Sin((double)angle);
            float num = (float)Math.Cos((double)angle);
            float num11 = x * x;
            float num10 = y * y;
            float num9 = z * z;
            float num8 = x * y;
            float num7 = x * z;
            float num6 = y * z;
            matrix[0, 0] = num11 + num * (1f - num11);
            matrix[0, 1] = num8 - num * num8 + num2 * z;
            matrix[0, 2] = num7 - num * num7 - num2 * y;
            matrix[0, 3] = 0f;
            matrix[1, 0] = num8 - num * num8 - num2 * z;
            matrix[1, 1] = num10 + num * (1f - num10);
            matrix[1, 2] = num6 - num * num6 + num2 * x;
            matrix[1, 3] = 0f;
            matrix[2, 0] = num7 - num * num7 + num2 * y;
            matrix[2, 1] = num6 - num * num6 - num2 * x;
            matrix[2, 2] = num9 + num * (1f - num9);
            matrix[2, 3] = 0f;
            matrix[3, 0] = 0f;
            matrix[3, 1] = 0f;
            matrix[3, 2] = 0f;
            matrix[3, 3] = 1f;
            return matrix;
        }

        public void UpdateViewMatrix() {
            UpdateLens();
            if (!_viewDirty) return;


            Look = Vector3.Normalize(Look);
            Up = Vector3.Normalize(Vector3.Cross(Look, Right));

            Right = Vector3.Normalize(Vector3.Cross(Up, Look));

            View = Matrix4x4.LookAt(new OpenGL.Vertex3f(Position.X, Position.Y, Position.Z),
                new OpenGL.Vertex3f(Position.X + Look.X, Position.Y + Look.Y, Position.Z + Look.Z),
                new OpenGL.Vertex3f(Up.X, Up.Y, Up.Z));

            _viewDirty = false;
        }
    }
}
