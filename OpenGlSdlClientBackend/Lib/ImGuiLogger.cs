﻿using ImGuiNET;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace ACDesktopClient.Lib {
    public class ConsoleWriterEventArgs : EventArgs {
        public string Value { get; private set; }
        public ConsoleWriterEventArgs(string value) {
            Value = value;
        }
    }
    public class ConsoleWriter : TextWriter {
        public override Encoding Encoding { get { return Encoding.UTF8; } }

        public override void Write(string value) {
            WriteEvent(this, new ConsoleWriterEventArgs(value));
            base.Write(value);
        }

        public override void WriteLine(string value) {
            if (WriteLineEvent != null) WriteLineEvent(this, new ConsoleWriterEventArgs(value));
            base.WriteLine(value);
        }

        public event EventHandler<ConsoleWriterEventArgs> WriteEvent;
        public event EventHandler<ConsoleWriterEventArgs> WriteLineEvent;
    }
    public class ImGuiLogger : ILogger {
        private struct LogLine {
            public LogLevel LogLevel { get; }
            public string Message { get; }
            public LogLine(LogLevel logLevel, string message) {
                LogLevel = logLevel;
                Message = message;
            }
        }

        private List<LogLine> _logLines = new List<LogLine>();
        private StringBuilder _logBuilder = new StringBuilder();
        private bool _autoScrollLogWindow;
        private bool _showInfoLogs;
        private string _logText = "";
        private bool _scrollToBottom;

        public LogLevel LogLevel { get; set; }

        public ImGuiLogger(LogLevel logLevel) {
            LogLevel = logLevel;
        }
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {
            Log(formatter(state, exception), logLevel);
        }

        public bool IsEnabled(LogLevel logLevel) {
            return logLevel >= LogLevel;
        }

        private class LogScope : IDisposable {
            public void Dispose() {

            }
        }

        public IDisposable BeginScope<TState>(TState state) where TState : notnull {
            return new LogScope();
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        public void Log(Exception ex) {
            WriteLog(ex.ToString(), LogLevel.Error);
        }

        public void Print(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        private void WriteLog(string text, LogLevel level) {
            if (IsEnabled(level)) {
                _logBuilder.AppendLine($"[{level}] {text}");
                _logText = _logBuilder.ToString();
                //_logLines.Add(new LogLine(level, text));
                _scrollToBottom = true;
            }
        }

        public void Render() {
            Vector2 viewportSize = ImGui.GetMainViewport().Size;
            ImGui.SetNextWindowSize(new Vector2(viewportSize.X - 20, 200), ImGuiCond.FirstUseEver);
            ImGui.SetNextWindowPos(new Vector2(10, viewportSize.Y - 210), ImGuiCond.FirstUseEver);
            if (ImGui.Begin("Logs")) {
                if (ImGui.BeginPopup("Options")) {
                    ImGui.Checkbox("AutoScroll", ref _autoScrollLogWindow);
                    ImGui.EndPopup();
                }
                if (ImGui.Button("Options")) {
                    ImGui.OpenPopup("Options");
                }
                ImGui.SameLine();
                if (ImGui.Button("Clear Logs")) {
                    //_logLines.Clear();
                    _logBuilder.Clear();
                    _logText = "";
                }

                if (ImGui.BeginChild("ScrollingRegion", Vector2.Zero, false, ImGuiWindowFlags.HorizontalScrollbar)) {
                    //var lines = _logLines.ToArray();
                    //ImGui.TextUnformatted(string.Join("\n", _logLines.ToArray().Select(ll => $"[{ll.LogLevel}] {ll.Message}")));

                    ImGui.TextUnformatted(_logText);

                    if (_scrollToBottom || (_autoScrollLogWindow && ImGui.GetScrollY() >= ImGui.GetScrollMaxY()))
                        ImGui.SetScrollHereY(1.0f);

                    _scrollToBottom = false;
                }
                ImGui.EndChild();
            }
            ImGui.End();
        }
    }
}
