﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static SDL2.SDL;
using WaveEngine.Bindings.OpenGL;
using ImGuiNET;
using OpenGL;

namespace ACDesktopClient.Lib {
    unsafe public class OpenGLRenderer : IRenderer {
        internal SDL_DisplayMode CurrentDisplayMode;
        private IntPtr SDLWindowHandle;
        private IntPtr SDLGLContext;
        private string LandblockText = "0x0316";

        private const string SHADER_VERSION = "#version 300 es";

        static string vertexShaderSource = SHADER_VERSION + @"
            // Vertex shader that takes in per-vertex colour data and outputs it as interpolated per-fragment data.
            uniform mat4 model;
            uniform mat4 view;
            uniform mat4 projection;

            // Inputs
            layout (location = 0) in vec4 position;
            layout (location = 1) in vec4 colour;
            layout (location = 2) in vec2 aTexCoord;
            layout (location = 3) in vec3 normal;
            // Outputs
            out vec4 interpolated_colour;
            out vec2 TexCoord;

            void main()
            {
                interpolated_colour = colour;
                TexCoord = vec2(aTexCoord.x, aTexCoord.y);
	            gl_Position = projection * view * model * position;
            }
        ";

        static string fragmentShaderSource = SHADER_VERSION + @"
            precision mediump float;
            uniform sampler2D gridTexture;
            out vec4 FragColor;
  
            in vec4 interpolated_colour; // the input variable from the vertex shader (same name and same type)  
            in vec2 TexCoord; // the input variable from the vertex shader (same name and same type)  

            void main()
            {
                //FragColor = interpolated_colour;
                //FragColor = mix(texture(texture1, TexCoord), interpolated_colour, 0.2);
                FragColor = texture(gridTexture, TexCoord);
            } 
        ";

        private static uint VBO;
        private static uint VAO;
        private static uint shaderProgram;

        private struct VBAO {
            public uint VAO;
            public uint VBO;

            public int Length;

            public VBAO(uint vao, uint vbo, int length) {
                VAO = vao;
                VBO = vbo;
                Length = length;
            }
        }

        private List<VBAO> VBAOS = new List<VBAO>();

        public int Width = 800;
        public int Height = 600;
        private string myText = "";
        private uint texture;
        private uint textureUnit;
        private int currentLandblock = 12;

        public Camera Camera { get; }

        public event EventHandler<EventArgs> OnRender2D;

        int IRenderer.Width => Width;

        int IRenderer.Height => Height;

        public OpenGLRenderer() {
            SetupSDL();
            SetupImGui();
            SetupOpenGL();

            InitTextures();

            Camera = new Camera();
        }

        private void InitTextures() {
            var textureArray = stackalloc uint[1];
            GL.glGenTextures(1, textureArray);
            texture = textureArray[0];

            textureUnit = 0;
            GL.glActiveTexture((TextureUnit)(0x84c0 + textureUnit));
            GL.glBindTexture(TextureTarget.Texture2d, texture);

            // set the texture wrapping/filtering options (on the currently bound texture object)
            GL.glTexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.glTexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
            GL.glTexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
            GL.glTexParameteri(TextureTarget.Texture2d, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

            var width = 32;
            var height = 32;

            byte[] pixels = new byte[width * height * 4];
            for (var y = 0; y < height; y++) {
                for (var x = 0; x < width; x++) {
                    var o = (height * y + x) * 4;
                    // middle x / y should be a different color to form a grid
                    if (y == height / 2 || x == width / 2) {
                        pixels[o + 0] = 0xff; // r
                        pixels[o + 1] = 0x00; // g
                        pixels[o + 2] = 0xff; // b
                        pixels[o + 3] = 0xff; // a
                    }
                    else {
                        pixels[o + 0] = 0xff; // r
                        pixels[o + 1] = 0xff; // g
                        pixels[o + 2] = 0xff; // b
                        pixels[o + 3] = 0xff; // a
                    }
                }
            }

            fixed (byte* fpixels = pixels) {
                var pType = (PixelType)5121; /*GL_UNSIGNED_BYTE*/
                GL.glTexImage2D(TextureTarget.Texture2d, 0, (int)PixelFormat.Rgba, width, height, 0, PixelFormat.Rgba, pType, fpixels);
                GL.glGenerateMipmap(TextureTarget.Texture2d);
            }

            /*
            var width = 32;
            var height = 32;

            uint[] pixels = new uint[width * height];
            for (var y = 0; y < height; y++) {
                for (var x = 0; x < width; x++) {
                    if (y == height / 2 || x == width / 2) {
                        pixels[height * y + x] = 0xff000044;
                    }
                    else {
                        pixels[height * y + x] = 0xff000044;
                    }
                }
            }

            fixed (uint* fpixels = pixels) {
                var pType = (PixelType)5121; // unsigned byte
                GL.glTexImage2D(TextureTarget.Texture2d, 0, (int)PixelFormat.Rgba, width, height, 0, PixelFormat.Rgba, pType, fpixels);
                GL.glGenerateMipmap(TextureTarget.Texture2d);
            }
         */
        }

        #region graphics / input setup
        private void SetupOpenGL() {
            GL.LoadAllFunctions(SDL2.SDL.SDL_GL_GetProcAddress);

            uint vertexShader = GL.glCreateShader(ShaderType.VertexShader);

            IntPtr* textPtr = stackalloc IntPtr[1];
            var lengthArray = stackalloc int[1];

            lengthArray[0] = vertexShaderSource.Length;
            textPtr[0] = Marshal.StringToHGlobalAnsi(vertexShaderSource);

            GL.glShaderSource(vertexShader, 1, (IntPtr)textPtr, lengthArray);
            GL.glCompileShader(vertexShader);

            int success = 0;
            var infoLog = stackalloc char[512];
            lengthArray[0] = success;
            GL.glGetShaderiv(vertexShader, ShaderParameterName.CompileStatus, lengthArray);
            if (success > 0) {
                GL.glGetShaderInfoLog(vertexShader, 512, (int*)0, infoLog);
                Console.WriteLine($"Error: shader vertex compilation failed: {new string(infoLog)}");
            }

            uint fragmentShader = GL.glCreateShader(ShaderType.FragmentShader);

            lengthArray[0] = fragmentShaderSource.Length;
            textPtr[0] = Marshal.StringToHGlobalAnsi(fragmentShaderSource);
            GL.glShaderSource(fragmentShader, 1, (IntPtr)textPtr, lengthArray);
            GL.glCompileShader(fragmentShader);

            lengthArray[0] = success;
            GL.glGetShaderiv(fragmentShader, ShaderParameterName.CompileStatus, lengthArray);
            if (success > 0) {
                GL.glGetShaderInfoLog(fragmentShader, 512, (int*)0, infoLog);
                Console.WriteLine($"Error: shader fragment compilation failed: {new string(infoLog)}");
            }

            shaderProgram = GL.glCreateProgram();
            GL.glAttachShader(shaderProgram, vertexShader);
            GL.glAttachShader(shaderProgram, fragmentShader);
            GL.glLinkProgram(shaderProgram);

            lengthArray[0] = success;
            GL.glGetProgramiv(shaderProgram, ProgramPropertyARB.LinkStatus, lengthArray);
            if (success > 0) {
                GL.glGetProgramInfoLog(shaderProgram, 512, (int*)0, infoLog);
                Console.WriteLine($"Error: shader program compilation failed: {new string(infoLog)}");
            }

            //*
            Console.WriteLine($"Vendor graphics card: {Marshal.PtrToStringUTF8((IntPtr)GL.glGetString(StringName.Vendor))}");
            Console.WriteLine($"Renderer: {Marshal.PtrToStringUTF8((IntPtr)GL.glGetString(StringName.Renderer))}");
            Console.WriteLine($"Version GL: {Marshal.PtrToStringUTF8((IntPtr)GL.glGetString(StringName.Version))}");
            Console.WriteLine($"Version GLSL: {Marshal.PtrToStringUTF8((IntPtr)GL.glGetString(StringName.ShadingLanguageVersion))}");
            //*/
        }

        private void SetupImGui() {
            ImGui.CreateContext();

            if (!ImGuiImpl.ImGui_ImplSDL2_InitForOpenGL(SDLWindowHandle, SDLGLContext)) {
                Console.WriteLine($"FAILED ImGui_ImplSDL2_InitForOpenGL");
            }

            ImGuiImpl.ImGui_ImplOpenGL3_Init(SHADER_VERSION);
        }

        private void SetupSDL() {
            if (SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
                Console.WriteLine($"SDL_Init failed: {SDL_GetError()}");
            }

            SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengl");
            SDL_GL_SetAttribute(SDL_GLattr.SDL_GL_DEPTH_SIZE, 24);
            SDL_GL_SetAttribute(SDL_GLattr.SDL_GL_STENCIL_SIZE, 8);
            SDL_GL_SetAttribute(SDL_GLattr.SDL_GL_DOUBLEBUFFER, 1);
            SDL_GetCurrentDisplayMode(0, out CurrentDisplayMode);

            var windowFlags = SDL_WindowFlags.SDL_WINDOW_SHOWN | SDL_WindowFlags.SDL_WINDOW_OPENGL | SDL_WindowFlags.SDL_WINDOW_RESIZABLE;
            SDLWindowHandle = SDL_CreateWindow("Hello", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, Width, Height, windowFlags);
            if (SDLWindowHandle == IntPtr.Zero) {
                Console.WriteLine($"Failed to create window: {SDL_GetError()}");
                return;
            }

            SDLGLContext = SDL_GL_CreateContext(SDLWindowHandle);
            if (SDLGLContext == IntPtr.Zero) {
                Console.WriteLine($"Failed to create gl context: {SDL_GetError()}");
                return;
            }
        }
        #endregion // graphics / input setup


        public void Resize(int width, int height) {
            Width = width;
            Height = height;
        }

        public void Update(double time) {
            SDL_SetWindowSize(SDLWindowHandle, Width, Height);
            GL.glViewport(0, 0, Width, Height);

            Camera.Update(time);
        }

        public void Render(double time) {
            SDL_GL_MakeCurrent(SDLWindowHandle, SDLGLContext);

            GL.glEnable(EnableCap.DepthTest);
            GL.glEnable(EnableCap.CullFace);

            GL.glClearColor(0f, 0f, 0.0f, 1.0f);
            GL.glClear((uint)(AttribMask.ColorBufferBit | AttribMask.DepthBufferBit));

            GL.glUseProgram(shaderProgram);

            IntPtr cName = IntPtr.Zero;
            try {
                cName = Marshal.StringToHGlobalAnsi("gridTexture");
                GL.glUniform1i(GL.glGetUniformLocation(shaderProgram, (char*)cName), (int)textureUnit);
            }
            finally {
                if (cName != IntPtr.Zero) {
                    Marshal.FreeHGlobal(cName);
                }
            }

            SetTransform("projection", Camera.Projection);
            SetTransform("view", Camera.View);
            SetTransform("model", Matrix4x4f.Identity);

            if (VBAOS.Count > 0) {
                GL.glBindVertexArray(VBAOS.Last().VAO);
                GL.glDrawArrays(PrimitiveType.Lines, 0, VBAOS.Last().Length);
            }
            /*
            foreach (var vbao in VBAOS) {
                GL.glBindVertexArray(vbao.VAO);
                GL.glDrawArrays(PrimitiveType.Lines, 0, vbao.Length);
            }
            */

            Camera.Render(time);

            RenderImGui();

            SDL2.SDL.SDL_GL_SwapWindow(SDLWindowHandle);

            SDL_Event e;

            while (SDL_PollEvent(out e) != 0) {
                ImGuiImpl.ImGui_ImplSDL2_ProcessEvent(&e);
                Camera.HandleEvent(e);
            }
        }

        private void SetTransform(string location, Matrix4x4f mat) {
            IntPtr cName = IntPtr.Zero;
            try {
                cName = Marshal.StringToHGlobalAnsi(location);
                fixed (float* transform = (float[])mat) {
                    GL.glUniformMatrix4fv(GL.glGetUniformLocation(shaderProgram, (char*)cName), 1, false, transform);
                }
            }
            finally {
                if (cName != IntPtr.Zero) {
                    Marshal.FreeHGlobal(cName);
                }
            }
        }

        private void RenderImGui() {
            ImGuiImpl.ImGui_ImplOpenGL3_NewFrame();
            ImGuiImpl.ImGui_ImplSDL2_NewFrame();
            ImGui.NewFrame();

            ImGui.SetNextWindowPos(new Vector2(20, 250), ImGuiCond.FirstUseEver);
            ImGui.ShowMetricsWindow();

            if (ImGui.Begin("Audio Player", ImGuiWindowFlags.AlwaysAutoResize)) {

                if (ImGui.Button("Play")) {
                    if (!LandblockText.StartsWith("0x")) {
                        LandblockText = $"0x{LandblockText}";
                    }
                    var waveId = Convert.ToUInt32(LandblockText, 16);
                    if (waveId < 0xFFFF) {
                        waveId = 0x0A000000 + (waveId);
                    }
                    //Program.ACClient?.PlayAudio(waveId);
                }
                ImGui.SameLine();
                if (ImGui.InputText("FileId", ref LandblockText, 50)) {

                }

                //if (IsLoading) {
                //    var percent = TotalRequests == 0 ? 0 : CurrentRequest / (double)TotalRequests * 100.0;
                //    ImGui.Text($"Loading: {Math.Round(percent, 2)}%%");
                //}
            }
            ImGui.End();

            OnRender2D?.Invoke(this, EventArgs.Empty);

            ImGui.Render();
            ImGuiImpl.ImGui_ImplOpenGL3_RenderDrawData((IntPtr)ImGui.GetDrawData().NativePtr);
        }

        public void AddTriangles(List<VerticeData> verticeDatas) {
            int numFloats = 13;
            int stride = numFloats * sizeof(float);
            float[] vertices = new float[stride * verticeDatas.Count];

            var i = 0;
            foreach (var vertData in verticeDatas) {
                // vec4 position x, y, z, w
                vertices[i + 0] = vertData.Position.X;
                vertices[i + 1] = vertData.Position.Y;
                vertices[i + 2] = vertData.Position.Z;
                vertices[i + 3] = 1.0f;

                // vec4 color r, g, b, a
                vertices[i + 4] = vertData.Color.X;
                vertices[i + 5] = vertData.Color.Y;
                vertices[i + 6] = vertData.Color.Z;
                vertices[i + 7] = vertData.Color.W;

                // vec2 texture coords
                vertices[i + 8] = vertData.TextureCoords.X;
                vertices[i + 9] = vertData.TextureCoords.Y;

                // vec3 normal
                vertices[i + 10] = vertData.Normal.X;
                vertices[i + 11] = vertData.Normal.Y;
                vertices[i + 12] = vertData.Normal.Z;

                i += numFloats;
            }

            var _VBO = 0u;
            var _VAO = 0u;
            GL.glGenVertexArrays(1, &_VAO);
            GL.glGenBuffers(1, &_VBO);

            GL.glBindVertexArray(_VAO);
            GL.glBindBuffer(BufferTargetARB.ArrayBuffer, _VBO);

            fixed (float* verticesPtr = &vertices[0]) {
                GL.glBufferData(BufferTargetARB.ArrayBuffer, vertices.Length * sizeof(float), verticesPtr, BufferUsageARB.StaticDraw);
            }
            GL.glVertexAttribPointer(0, 4, VertexAttribPointerType.Float, false, stride, (void*)null);
            GL.glEnableVertexAttribArray(0);

            GL.glVertexAttribPointer(1, 4, VertexAttribPointerType.Float, false, stride, (void*)16);
            GL.glEnableVertexAttribArray(1);

            GL.glVertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, stride, (void*)32);
            GL.glEnableVertexAttribArray(2);

            GL.glVertexAttribPointer(3, 3, VertexAttribPointerType.Float, true, stride, (void*)40);
            GL.glEnableVertexAttribArray(3);

            GL.glBindBuffer(BufferTargetARB.ArrayBuffer, 0);

            GL.glBindVertexArray(0);

            VBAOS.Add(new VBAO(_VAO, _VBO, verticeDatas.Count));
        }
    }
}
