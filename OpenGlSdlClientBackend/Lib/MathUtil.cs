﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACDesktopClient.Lib {
    internal static class MathUtil {
        internal static float DegreesToRadians(float degree) {
            return degree * ((float)Math.PI / 180f);
        }
    }
}
