﻿using System.Numerics;

namespace ACDesktopClient.Lib {

    public class VerticeData {
        public Vector3 Position;
        public Vector4 Color;
        public Vector2 TextureCoords;

        public Vector3 Normal { get; internal set; }
    }
    public interface IRenderer {
        public int Width { get; }
        public int Height { get; }

        public event EventHandler<EventArgs> OnRender2D;

        void AddTriangles(List<VerticeData> vector3s);
        void Render(double time);
        void Resize(int width, int height);
        void Update(double time);
    }
}