﻿using ImGuiNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

#pragma warning disable 1591
internal unsafe static class ImGuiImpl {
    /*
    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern bool ImGui_ImplGlfw_InitForOpenGL(IntPtr window, bool install_callbacks);

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplGlfw_NewFrame(int width, int height);

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplGlfw_Shutdown();
    */
    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern bool ImGui_ImplSDL2_InitForOpenGL(IntPtr window, IntPtr gl_context);

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplSDL2_NewFrame();

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplSDL2_Shutdown();

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern bool ImGui_ImplSDL2_ProcessEvent(SDL2.SDL.SDL_Event* e);



    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern bool ImGui_ImplOpenGL3_Init(byte* glsl_version);

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplOpenGL3_Shutdown();

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplOpenGL3_NewFrame();

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplOpenGL3_RenderDrawData(IntPtr draw_data);

    public static void ImGui_ImplOpenGL3_Init(string version) {
        byte* native_fmt;
        int fmt_byteCount = 0;
        if (version != null) {
            fmt_byteCount = Encoding.UTF8.GetByteCount(version);
            if (fmt_byteCount > Util.StackAllocationSizeLimit) {
                native_fmt = Util.Allocate(fmt_byteCount + 1);
            }
            else {
                byte* native_fmt_stackBytes = stackalloc byte[fmt_byteCount + 1];
                native_fmt = native_fmt_stackBytes;
            }
            int native_fmt_offset = Util.GetUtf8(version, native_fmt, fmt_byteCount);
            native_fmt[native_fmt_offset] = 0;
        }
        else { native_fmt = null; }
        ImGuiImpl.ImGui_ImplOpenGL3_Init(native_fmt);
        if (fmt_byteCount > Util.StackAllocationSizeLimit) {
            Util.Free(native_fmt);
        }
    }

    /*
    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern bool ImGui_ImplOpenGL2_Init();

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplOpenGL2_Shutdown();

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplOpenGL2_NewFrame();

    [DllImport("cimgui", CallingConvention = CallingConvention.Cdecl)]
    public static extern void ImGui_ImplOpenGL2_RenderDrawData(IntPtr draw_data);
    */

}