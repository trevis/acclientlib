﻿using ACClientLib.Lib.Networking.Transports;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Scripting.Interop;

namespace ACClientLib {
    public class ACClientOptions {
        /// <summary>
        /// The network transport to use for communicating with the AC server.
        /// </summary>
        public INetworkTransport NetTransport { get; set; }

        /// <summary>
        /// The dat reader to use.
        /// </summary>
        public IAsyncDatReader DatReader { get; set; }

        /// <summary>
        /// The logger to use
        /// </summary>
        public ILogger Logger { get; set; }
    }
}
