﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ACClientLib.Lib.Networking;
using ACClientLib.Lib.Networking.Packets;
using ACClientLib.Lib.Networking.Packets.Events;
using ACClientLib.Lib.Scripting;
using ACE.DatLoader.FileTypes;
using ACE.DatLoader;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using System.IO;
using System.Runtime.InteropServices.ComTypes;

namespace ACClientLib {
    public class ACClient {
        private string _username;
        private string _password;
        private CancellationToken _cancellationToken;
        private bool _useThreading;
        private NetworkManager _networkManager;
        private bool _serverReadyToEnter;
        private Action<uint, byte[]> _audioHandler;

        public string Host { get; private set; }
        public int Port { get; private set; }

        public ACClientOptions Options { get; }
        public ScriptHost ScriptHost { get; }

        public event EventHandler<StateChangedEventArgs> OnStateChanged;

        public ACClient(ACClientOptions options) {
            Options = options;

            ScriptHost = new ScriptHost(options.DatReader, options.Logger);
        }

        public void Connect(string host, int port, string username, string password, CancellationToken cancellationToken, bool useThreading = true) {
            Host = host;
            Port = port;
            _username = username;
            _password = password;
            _cancellationToken = cancellationToken;
            _useThreading = useThreading;

            if (_useThreading) {
                var thread = new Thread(DoRunThread);
                thread.Start();
            }
            else {
                DoRunThread(null);
            }
        }

        private async void DoRunThread(object obj) {
            await ScriptHost.Init();

            ScriptHost.Scripts.GameState.OnStateChanged += GameState_OnStateChanged;

            ScriptHost.Scripts.MessageHandler.Incoming.Message += Incoming_Message;
            ScriptHost.Scripts.MessageHandler.Outgoing.Message += Outgoing_Message;

            ScriptHost.Scripts.MessageHandler.Incoming.DDD_InterrogationMessage += Incoming_DDD_InterrogationMessage;
            ScriptHost.Scripts.MessageHandler.Incoming.DDD_OnEndDDD += Incoming_DDD_OnEndDDD;

            ScriptHost.Scripts.MessageHandler.Incoming.Login_AccountBooted += Incoming_Login_AccountBooted;
            ScriptHost.Scripts.MessageHandler.Incoming.Character_CharacterError += Incoming_Character_CharacterError;

            ScriptHost.Scripts.MessageHandler.Incoming.Login_EnterGame_ServerReady += Incoming_Login_EnterGame_ServerReady;

            _networkManager = new NetworkManager(Options.NetTransport);

            _networkManager.OnIncomingMessage += _networkManager_OnIncoming;
            _networkManager.OnOutgoingMessage += _networkManager_OnOutgoingMessage;

            _networkManager.Connect(Host, Port, _username, _password);

            if (_useThreading) {
                while (!_cancellationToken.IsCancellationRequested) {
                    Update();
                    if (!Thread.Yield()) {
                        await Task.Delay(10);
                    }
                }
            }
        }

        public void Update() {
            _networkManager.DoNetworking();
            ScriptHost.Scripts.Render3D();
            ScriptHost.Scripts.Tick();
            ScriptHost.Scripts.Render2D();
        }

        public void RegisterAudioHandler(Action<uint, byte[]> callback) {
            _audioHandler = callback;
        }

        public async void PlayAudio(uint fileId) {
            if (_audioHandler != null) {
                var wav = await ScriptHost.Scripts.Resolve<IAsyncDatReader>().ReadFromDat<Wave>(fileId);
                if (wav?.Data?.Length > 0) {
                    using (var stream = new MemoryStream()) {
                        wav.ReadData(stream);

                        _audioHandler?.Invoke(fileId, stream.ToArray());
                    }
                }
            }
            
        }

        private void Incoming_Login_EnterGame_ServerReady(object sender, UtilityBelt.Common.Messages.Events.Login_EnterGame_ServerReady_S2C_EventArgs e) {
            _serverReadyToEnter = true;
        }

        public async void EnterWorld(uint characterId) {
            var character = ScriptHost.Scripts.GameState.Characters.FirstOrDefault(c => c.Id == characterId);
            if (character == null) {
                Console.WriteLine($"Tried to log in invalid character id: {characterId}");
                return;
            }

            if (_serverReadyToEnter) {
                SendEnterWorld(character);
            }
            else {
                EventHandler<Login_EnterGame_ServerReady_S2C_EventArgs> _serverReadyHandler = null;
                _serverReadyHandler = (s, e) => {
                    SendEnterWorld(character);
                    ScriptHost.Scripts.MessageHandler.Incoming.Login_EnterGame_ServerReady -= _serverReadyHandler;
                };
                ScriptHost.Scripts.MessageHandler.Incoming.Login_EnterGame_ServerReady += _serverReadyHandler;
            }
        }

        private async void SendEnterWorld(CharacterIdentity character) {
            _networkManager.SendMessage(new SendEnterWorldFragment((int)character.Id, ScriptHost.Scripts.GameState.AccountName));
            await Task.Delay(200);
            _networkManager.SendEventMessage(new LoginCompleteNotificationEventFragment());
        }

        public void InvokeChatParser(string message) {
            _networkManager.SendEventMessage(new TalkEvent(message));
        }

        private void GameState_OnStateChanged(object sender, StateChangedEventArgs e) {
            switch (e.NewState) {
                case UtilityBelt.Scripting.Enums.ClientState.Entering_Game:
                    PlayAudio(0x0A000316);
                    break;
            }
            OnStateChanged?.Invoke(this, e);
        }

        private void _networkManager_OnOutgoingMessage(object sender, Lib.Networking.FragmentedPacketEventArgs e) {
            ScriptHost.Scripts.HandleOutgoing(e.Data);
        }

        private void _networkManager_OnIncoming(object sender, Lib.Networking.FragmentedPacketEventArgs e) {
            ScriptHost.Scripts.HandleIncoming(e.Data);
        }

        private void Incoming_DDD_InterrogationMessage(object sender, UtilityBelt.Common.Messages.Events.DDD_InterrogationMessage_S2C_EventArgs e) {
            _networkManager.SendMessage(new DatabaseSyncFragment());
            _networkManager.SendMessage(new Fragment(0xF7C8, 4)); // Request Enter Game
        }

        private void Incoming_DDD_OnEndDDD(object sender, UtilityBelt.Common.Messages.Events.DDD_OnEndDDD_S2C_EventArgs e) {
            _networkManager.SendMessage(new Fragment(0xF7EA, 4)); // DDD End
        }

        private void Incoming_Character_CharacterError(object sender, UtilityBelt.Common.Messages.Events.Character_CharacterError_S2C_EventArgs e) {
            Console.WriteLine($"Character Error: {e.Data.Type}");
        }

        private void Incoming_Login_AccountBooted(object sender, UtilityBelt.Common.Messages.Events.Login_AccountBooted_S2C_EventArgs e) {
            Console.WriteLine($"Account Booted: {e.Data.ReasonText} ({e.Data.AdditionalReasonText})");
        }

        private void Outgoing_Message(object sender, UtilityBelt.Common.Messages.Events.MessageEventArgs e) {
            Console.WriteLine($"Outgoing Message: {e.Type} // {e.Data}");
        }

        private void Incoming_Message(object sender, UtilityBelt.Common.Messages.Events.MessageEventArgs e) {
            Console.WriteLine($"Incoming Message: {e.Type} // {e.Data}");
        }
    }
}
