﻿using ACE.DatLoader;
using ACE.DatLoader.FileTypes;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Interop;
using Environment = ACE.DatLoader.FileTypes.Environment;

namespace ACClientLib.Lib {
    public class FSDatReader : IAsyncDatReader {
        private static Dictionary<Type, Func<uint, Task<object>>> _switch = new Dictionary<Type, Func<uint, Task<object>>> {
                    { typeof(CellLandblock), async (fileId) => await TryLoadCellFile<CellLandblock>(fileId) },
                    { typeof(LandblockInfo), async (fileId) => await TryLoadCellFile<LandblockInfo>(fileId) },
                    { typeof(EnvCell), async (fileId) => await TryLoadCellFile<EnvCell>(fileId) },

                    { typeof(GfxObj), async (fileId) => await TryLoadPortalFile<GfxObj>(fileId) },
                    { typeof(SetupModel), async (fileId) => await TryLoadPortalFile<SetupModel>(fileId) },
                    { typeof(Animation), async (fileId) => await TryLoadPortalFile<Animation>(fileId) },
                    { typeof(Palette), async (fileId) => await TryLoadPortalFile<Palette>(fileId) },
                    { typeof(SurfaceTexture), async (fileId) => await TryLoadPortalFile<SurfaceTexture>(fileId) },
                    { typeof(Texture), async (fileId) => await TryLoadPortalFile<Texture>(fileId) },
                    { typeof(MotionTable), async (fileId) => await TryLoadPortalFile<MotionTable>(fileId) },
                    { typeof(Wave), async (fileId) => await TryLoadPortalFile<Wave>(fileId) },
                    { typeof(Environment), async (fileId) => await TryLoadPortalFile<Environment>(fileId) },
                    { typeof(ChatPoseTable), async (fileId) => await TryLoadPortalFile<ChatPoseTable>(fileId) },
                    { typeof(BadData), async (fileId) => await TryLoadPortalFile<BadData>(fileId) },
                    { typeof(NameFilterTable), async (fileId) => await TryLoadPortalFile<NameFilterTable>(fileId) },
                    { typeof(PaletteSet), async (fileId) => await TryLoadPortalFile<PaletteSet>(fileId) },
                    { typeof(ClothingTable), async (fileId) => await TryLoadPortalFile<ClothingTable>(fileId) },
                    { typeof(GfxObjDegradeInfo), async (fileId) => await TryLoadPortalFile<GfxObjDegradeInfo>(fileId) },
                    { typeof(Scene), async (fileId) => await TryLoadPortalFile<Scene>(fileId) },
                    { typeof(RegionDesc), async (fileId) => await TryLoadPortalFile<RegionDesc>(fileId) },
                    { typeof(RenderTexture), async (fileId) => await TryLoadPortalFile<RenderTexture>(fileId) },
                    { typeof(SoundTable), async (fileId) => await TryLoadPortalFile<SoundTable>(fileId) },
                    { typeof(EnumMapper), async (fileId) => await TryLoadPortalFile<EnumMapper>(fileId) },
                    { typeof(DidMapper), async (fileId) => await TryLoadPortalFile<DidMapper>(fileId) },
                    { typeof(DualDidMapper), async (fileId) => await TryLoadPortalFile<DualDidMapper>(fileId) },
                    { typeof(ParticleEmitterInfo), async (fileId) => await TryLoadPortalFile<ParticleEmitterInfo>(fileId) },
                    { typeof(PhysicsScript), async (fileId) => await TryLoadPortalFile<PhysicsScript>(fileId) },
                    { typeof(PhysicsScriptTable), async (fileId) => await TryLoadPortalFile<PhysicsScriptTable>(fileId) },
                    { typeof(Font), async (fileId) => await TryLoadPortalFile<Font>(fileId) },
                    { typeof(SecondaryAttributeTable), async (fileId) => await TryLoadPortalFile<SecondaryAttributeTable>(fileId) },
                    { typeof(SkillTable), async (fileId) => await TryLoadPortalFile<SkillTable>(fileId) },
                    { typeof(SpellTable), async (fileId) => await TryLoadPortalFile<SpellTable>(fileId) },
                    { typeof(SpellComponentsTable), async (fileId) => await TryLoadPortalFile<SpellComponentsTable>(fileId) },
                    { typeof(XpTable), async (fileId) => await TryLoadPortalFile<XpTable>(fileId) },
                    { typeof(ContractTable), async (fileId) => await TryLoadPortalFile<ContractTable>(fileId) }
            };

        private static CellDatDatabase CellDat;
        private static PortalDatDatabase PortalDat;
        private static LanguageDatDatabase LanguageDat;

        public SecondaryAttributeTable SecondaryAttributeTable => PortalDat?.SecondaryAttributeTable;
        public CharGen CharGen => PortalDat?.CharGen;
        public SkillTable SkillTable => PortalDat?.SkillTable;
        public SpellComponentsTable SpellComponentsTable => PortalDat?.SpellComponentsTable;
        public SpellTable SpellTable => PortalDat?.SpellTable;
        public XpTable XpTable => PortalDat?.XpTable;
        public DualDidMapper DualDidMapper => PortalDat?.DualDidMapper;

        public FSDatReader(string datDirectory) {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var cellDatPath = Path.Combine(datDirectory, "client_cell_1.dat");
            var portalDatPath = Path.Combine(datDirectory, "client_portal.dat");
            var languageDatPath = Path.Combine(datDirectory, "client_local_English.dat");

            if (!File.Exists(cellDatPath)) {
                throw new FileNotFoundException($"Unable to load cellDat: {cellDatPath}");
            }
            if (!File.Exists(portalDatPath)) {
                throw new FileNotFoundException($"Unable to load portalDat: {portalDatPath}");
            }
            if (!File.Exists(languageDatPath)) {
                throw new FileNotFoundException($"Unable to load languageDat: {portalDatPath}");
            }

            CellDat = new CellDatDatabase(cellDatPath, true);
            PortalDat = new PortalDatDatabase(portalDatPath, true);
            LanguageDat = new LanguageDatDatabase(languageDatPath, true);

        }

        public void Init() {
            
        }

        private static async Task<T> TryLoadPortalFile<T>(uint fileId) where T : FileType, new() {
            return PortalDat.ReadFromDat<T>(fileId);
        }

        private static async Task<T> TryLoadCellFile<T>(uint fileId) where T : FileType, new() {
            return CellDat.ReadFromDat<T>(fileId);
        }

        public async Task<T> ReadFromDat<T>(uint fileId) where T : FileType, new() {
            return await _switch[typeof(T)](fileId) as T;
        }
    }
}
