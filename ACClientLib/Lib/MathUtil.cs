﻿using System;

namespace ACClientLib.Lib
{
    internal static class MathUtil
    {
        internal static float DegreesToRadians(float degree)
        {
            return degree * ((float)Math.PI / 180f);
        }
    }
}