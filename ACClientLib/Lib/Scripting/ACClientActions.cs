﻿using Microsoft.Extensions.Logging;
using System;
using System.Runtime.InteropServices;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Interop;

namespace ACClientLib.Lib.Scripting
{

    public class ACClientActions : IClientActionsRaw
    {
        private ILogger _log;

        public ACClientActions(ILogger logger)
        {
            _log = logger;
        }

        public void AttributeAddExperience(AttributeId attribute, uint experienceToSpend)
        {
            try
            {
                //CoreManager.Current.Actions.AddAttributeExperience((Decal.Adapter.Wrappers.AttributeType)attribute, (int)experienceToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SkillAddExperience(SkillId skill, uint experienceToSpend)
        {
            try
            {
                //CoreManager.Current.Actions.AddSkillExperience((Decal.Adapter.Wrappers.SkillType)skill, (int)experienceToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VitalAddExperience(VitalId vital, uint experienceToSpend)
        {
            try
            {
                //CoreManager.Current.Actions.AddVitalExperience((Decal.Adapter.Wrappers.VitalType)(vital + 1), (int)experienceToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void ApplyWeenie(uint sourceObjectId, uint targetObjectId)
        {
            try
            {
                //CoreManager.Current.Actions.ApplyItem((int)sourceObjectId, (int)targetObjectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AutoWield(uint objectId, EquipMask slot)
        {
            try
            {
                //AcClient.CM_Inventory.Event_GetAndWieldItem(objectId, (uint)slot);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void CastSpell(uint spellId, uint targetId)
        {
            try
            {
                //CoreManager.Current.Actions.CastSpell((int)spellId, (int)targetId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void Dispose()
        {

        }

        public void DropObject(uint objectId)
        {
            try
            {
                //CoreManager.Current.Actions.DropItem((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipCreate(string name, bool shareExperience)
        {
            try
            {
                //UBHelper.Fellow.Create(name/*, shareExperience */);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipDisband()
        {
            try
            {
                //UBHelper.Fellow.Disband();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipDismiss(uint objectId)
        {
            try
            {
                //UBHelper.Fellow.Dismiss((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipSetLeader(uint objectId)
        {
            try
            {
                //UBHelper.Fellow.Leader = (int)objectId;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipQuit(bool disband)
        {
            try
            {
                // UBHelper.Fellow.Quit(/* disband */);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipRecruit(uint objectId)
        {
            try
            {
                //UBHelper.Fellow.Recruit((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipSetOpen(bool open)
        {
            try
            {
                //UBHelper.Fellow.Open = open;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void GiveWeenie(uint objectId, uint targetId)
        {
            try
            {
                //CoreManager.Current.Actions.GiveItem((int)objectId, (int)targetId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void InvokeChatParser(string text)
        {
            try
            {
                //if (!Decal_DispatchOnChatCommand(text))
                //    CoreManager.Current.Actions.InvokeChatParser(text);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void MoveWeenie(uint objectId, uint containerId, uint slot, bool stack)
        {
            try
            {
                //CoreManager.Current.Actions.MoveItem((int)objectId, (int)containerId, (int)slot, stack);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void Appraise(uint objectId)
        {
            try
            {
                //if (_requestIdInternal == null) {
                //    _requestIdInternal = (RequestIdDelegate)Marshal.GetDelegateForFunctionPointer((IntPtr)(436554 << 4), typeof(RequestIdDelegate));
                //}

                //_requestIdInternal((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SalvagePanelAdd(uint objectId)
        {
            try
            {
                //CoreManager.Current.Actions.SalvagePanelAdd((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SalvagePanelSalvage()
        {
            try
            {
                //CoreManager.Current.Actions.SalvagePanelSalvage();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SelectWeenie(uint objectId)
        {
            try
            {
                //CoreManager.Current.Actions.SelectItem((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SetAutorun(bool enabled)
        {
            try
            {
                //CoreManager.Current.Actions.SetAutorun(enabled);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SetCombatMode(CombatMode combatMode)
        {
            try
            {
                //CoreManager.Current.Actions.SetCombatMode((Decal.Adapter.Wrappers.CombatState)combatMode);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void TradeAccept()
        {
            try
            {
                //CoreManager.Current.Actions.TradeAccept();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void TradeAdd(uint objectId)
        {
            try
            {
                //CoreManager.Current.Actions.TradeAdd((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void TradeDecline()
        {
            try
            {
                //CoreManager.Current.Actions.TradeDecline();
            }
            catch (Exception ex)
            {
                _log.LogError(ex.ToString());
            }
        }

        public void TradeEnd()
        {
            try
            {
                //CoreManager.Current.Actions.TradeEnd();
            }
            catch (Exception ex)
            {
                _log.LogError(ex.ToString());
            }
        }

        public void TradeReset()
        {
            try
            {
                //CoreManager.Current.Actions.TradeReset();
            }
            catch (Exception ex)
            {
                _log.LogError(ex.ToString());
            }
        }

        public void UseWeenie(uint objectId)
        {
            try
            {
                //CoreManager.Current.Actions.UseItem((int)objectId, 0);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public uint SelectedObject()
        {
            try
            {
                //return (uint)CoreManager.Current.Actions.CurrentSelection;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
            return 0;
        }

        unsafe public void Login(uint id)
        {
            try
            {
                //AcClient.CPlayerSystem.GetPlayerSystem()->LogOnCharacter(id);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        unsafe public void Logout()
        {
            try
            {
                //AcClient.CPlayerSystem.GetPlayerSystem()->LogOffCharacter(0);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SkillAdvance(SkillId skill, uint creditsToSpend)
        {
            try
            {
                //AcClient.CM_Train.Event_TrainSkillAdvancementClass((uint)skill, creditsToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AllegianceBreak(uint objectId)
        {
            try
            {
                //AcClient.CM_Allegiance.Event_BreakAllegiance(objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AllegianceSwear(uint objectId)
        {
            try
            {
                //AcClient.CM_Allegiance.Event_SwearAllegiance(objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorAddToBuyList(uint objectId, uint amount = 1)
        {
            try
            {
                //CoreManager.Current.Actions.VendorAddBuyList((int)objectId, (int)amount);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorAddToSellList(uint objectId)
        {
            try
            {
                //CoreManager.Current.Actions.VendorAddSellList((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorBuyAll()
        {
            try
            {
                //CoreManager.Current.Actions.VendorBuyAll();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorSellAll()
        {
            try
            {
                //CoreManager.Current.Actions.VendorSellAll();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorClearBuyList()
        {
            try
            {
                //CoreManager.Current.Actions.VendorClearBuyList();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorClearSellList()
        {
            try
            {
                //CoreManager.Current.Actions.VendorClearSellList();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SplitObject(uint objectId, uint targetId, uint newStackSize, uint slot = 0)
        {
            try
            {
                //var weenie = new UBHelper.Weenie((int)objectId);
                //weenie.Split((int)targetId, (int)slot, (int)newStackSize);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SendTellByObjectId(uint objectId, string message)
        {
            try
            {
                //UBHelper.Core.SendTellByGUID((int)objectId, message);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AcceptNextConfirmationRequest()
        {
            try
            {
                //UBHelper.ConfirmationRequest.ConfirmationRequestEvent += ConfirmationRequest_ConfirmationRequestEvent;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }
    }
}
