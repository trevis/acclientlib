﻿using ACE.DatLoader;
using System.Collections.Generic;
using System.IO;
using System;
using UtilityBelt.Scripting;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;
using UtilityBelt.Scripting.Lib.ScriptTypes;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace ACClientLib.Lib.Scripting {
    public class ScriptHost : IDisposable {

        public string ScriptDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Decal Plugins", "UtilityBelt", "scripts");

        public string ScriptDataDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Decal Plugins", "UtilityBelt", "scriptdata");

        private readonly IAsyncDatReader datReader;
        private readonly ILogger logger;

        public ScriptManager Scripts { get; private set; }

        public ScriptHost(IAsyncDatReader datReader, ILogger logger) {
            this.datReader = datReader;
            this.logger = logger;
        }

        public Task Init() {
            var options = new ScriptManagerOptions() {
                ScriptDirectory = ScriptDirectory,
                StartVSCodeDebugServer = false,
                ClientCapabilities = ClientCapability.ImGui,
                ScriptDataDirectory = ScriptDataDirectory
            };

            Scripts = new ScriptManager(options);

            Scripts.RegisterComponent(typeof(ILogger), logger.GetType(), true, logger);
            Scripts.RegisterComponent(typeof(IClientActionsRaw), typeof(ACClientActions), true, new ACClientActions(logger));
            Scripts.RegisterComponent(typeof(IAsyncDatReader), datReader.GetType(), true, datReader);

            LoadTypes($"ACClientLib.UtilityBelt.Scripting.ScriptTypes.json");
            LoadTypes($"ACClientLib.UtilityBelt.Common.ScriptTypes.json");

            return Scripts.Initialize((s) => {
                logger.Log(LogLevel.Information, s);
            });
        }

        private void LoadTypes(string resourceName) {
            var assembly = Assembly.GetExecutingAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream)) {
                var jObj = JObject.Parse(reader.ReadToEnd());
                ScriptableTypes.LoadScriptTypes(assembly, jObj);
            }
        }

        public object RunLua(string lua) {
            return Scripts.GlobalScriptContext.RunText(lua);
        }

        public void Dispose() {

        }

    }
}
