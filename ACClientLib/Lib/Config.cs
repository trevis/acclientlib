﻿using Newtonsoft.Json;
using System;

namespace ACClientLib.Lib
{
    public enum SelectionType
    {
        Triangle,
        Inspectable
    }

    public enum RenderMode
    {
        Landscape = 1,
        Indoors = 2,
        Dungeons = 3,
        All = 4
    }

    public class Config
    {
        [JsonIgnore]
        public string Title => "UtilityBelt NavMesh Editor";

        [JsonIgnore]
        public SelectionType SelectionType { get; set; } = SelectionType.Inspectable;
        [JsonIgnore]
        public RenderMode RenderMode { get; set; } = RenderMode.All;

        public bool ShowLandblockChunkClassifier = true;
        public bool ShowPerformance = true;
        public bool ShowDebugLogs = true;
        public bool ShowLandblockExplorer = false;
        public bool ShowNavMeshBuilder = true;


        public float MouseSensitivity = 0.15f;
        public float MovementSpeed = 0.04f;
        public float TurboSpeedMultiplier = 2f;
        public float ScreenDepth = 10000.0f;
        public float ScreenNear = 0.1f;
        public int WindowWidth = 1600;
        public int WindowHeight = 1100;
        public float FieldOfView = 45f;


        [JsonIgnore]
        private string _lastSaveContents = "";
        [JsonIgnore]
        private DateTime _lastSaveCheck = DateTime.UtcNow;

        public Config()
        {
            TryLoad();
        }

        public void Update()
        {
            if (DateTime.UtcNow - _lastSaveCheck > TimeSpan.FromSeconds(5))
            {
                _lastSaveCheck = DateTime.UtcNow;
                TrySave();
            }
        }

        public void TryLoad()
        {
            /*
            if (File.Exists("settings.json")) {
                try {
                    var json = File.ReadAllText("settings.json");
                    JsonConvert.PopulateObject(json, this);
                    _lastSaveContents = json;
                }
                catch (Exception ex) {
                    DebugLogs.WriteLine(ex.ToString());
                }
            }
            */
        }

        public void TrySave()
        {
            /*
            var newJson = JsonConvert.SerializeObject(this);
            if (newJson != _lastSaveContents) {
                File.WriteAllText("settings.json", newJson);
                _lastSaveContents = newJson;
                DebugLogs.WriteLine($"Saved settings to settings.json");
            }
            */
        }
    }
}
