﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACClientLib.Lib.Networking.Constants
{
    public enum CombatState : uint
    {
        NonCombat = 0x00000001,
        Melee = 0x00000002,
        Missile = 0x00000004,
        Magic = 0x00000008
    }
}
