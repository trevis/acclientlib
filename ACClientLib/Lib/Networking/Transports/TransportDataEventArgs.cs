﻿using System.Net;

namespace ACClientLib.Lib.Networking.Transports {
    public class TransportDataEventArgs : System.EventArgs {
        public byte[] Data { get; }
        public IPEndPoint Server { get; }

        public TransportDataEventArgs(IPEndPoint server, byte[] data) {
            Data = data;
            Server = server;
        }
    }
}