﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ACClientLib.Lib.Networking.Packets {
    class LoginCompleteNotificationEventFragment : EventFragment {
        public LoginCompleteNotificationEventFragment() : base(0x00a1) {
        }
    }
}
