
namespace UtilityBelt.Common.Enums {
    public enum WeaponType {
        Undef,
        Unarmed,
        Sword,
        Axe,
        Mace,
        Spear,
        Dagger,
        Staff,
        Bow,
        Crossbow,
        Thrown,
        TwoHanded,
        Magic
    }
}
