﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Common.Enums {
    /// <summary>
    /// The type of spells to dispel
    /// </summary>
    public enum DispelType {
        All,
        Positive,
        Negative
    }
}
