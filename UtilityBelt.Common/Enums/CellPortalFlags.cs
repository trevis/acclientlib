using System;

namespace UtilityBelt.Common.Enums {
    [Flags]
    public enum CellPortalFlags {
        ExactMatch = 0x1,
        PortalSide = 0x2
    }

    public static class PortalFlagsExtensions {
        public static bool HasFlag(this CellPortalFlags type, CellPortalFlags flag) {
            return ((uint)type & (uint)flag) != 0;
        }
    }
}
