﻿using System;

namespace UtilityBelt.Common.Enums {

    /// <summary>
    /// Represents the direction of a network message
    /// </summary>
    public enum MessageDirection : byte {
        /// <summary>
        /// Server to Client
        /// </summary>
        S2C = 1,

        /// <summary>
        /// Client to Server
        /// </summary>
        C2S = 2
    }

    /// <summary>
    /// TODO
    /// </summary>
    public enum Queues : uint {
        /// <summary>
        /// C2S, small number of admin messages and a few others
        /// </summary>
        Control = 0x0002,

        /// <summary>
        /// C2S, most all game actions, all ordered
        /// </summary>
        Weenie = 0x0003,

        /// <summary>
        /// Bidirectional, login messages, turbine chat
        /// </summary>
        Logon = 0x0004,

        /// <summary>
        /// Bidirectional, DAT patching
        /// </summary>
        CLCache = 0x0005,

        /// <summary>
        /// S2C, game events (ordered) and other character related messages
        /// </summary>
        UIQueue = 0x0009,

        /// <summary>
        /// S2C, messages mostly related to object creation/deletion and their motion, effects
        /// </summary>
        SmartBox = 0x000A,

    }

    /// <summary>
    /// All message types of game messages
    /// </summary>
    public enum MessageType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AllegianceUpdateAborted = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_PopUpString = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        Character_PlayerOptionChangedEvent = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_TargetedMeleeAttack = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_TargetedMissileAttack = 0x000A,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_SetAFKMode = 0x000F,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_SetAFKMessage = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        Login_PlayerDescription = 0x0013,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_Talk = 0x0015,

        /// <summary>
        /// TODO
        /// </summary>
        Social_RemoveFriend = 0x0017,

        /// <summary>
        /// TODO
        /// </summary>
        Social_AddFriend = 0x0018,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_PutItemInContainer = 0x0019,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_GetAndWieldItem = 0x001A,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_DropItem = 0x001B,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_SwearAllegiance = 0x001D,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_BreakAllegiance = 0x001E,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_UpdateRequest = 0x001F,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AllegianceUpdate = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        Social_FriendsUpdate = 0x0021,

        /// <summary>
        /// TODO
        /// </summary>
        Item_ServerSaysContainID = 0x0022,

        /// <summary>
        /// TODO
        /// </summary>
        Item_WearItem = 0x0023,

        /// <summary>
        /// TODO
        /// </summary>
        Item_ServerSaysRemove = 0x0024,

        /// <summary>
        /// TODO
        /// </summary>
        Social_ClearFriends = 0x0025,

        /// <summary>
        /// TODO
        /// </summary>
        Character_TeleToPKLArena = 0x0026,

        /// <summary>
        /// TODO
        /// </summary>
        Character_TeleToPKArena = 0x0027,

        /// <summary>
        /// TODO
        /// </summary>
        Social_CharacterTitleTable = 0x0029,

        /// <summary>
        /// TODO
        /// </summary>
        Social_AddOrSetCharacterTitle = 0x002B,

        /// <summary>
        /// TODO
        /// </summary>
        Social_SetDisplayCharacterTitle = 0x002C,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_QueryAllegianceName = 0x0030,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_ClearAllegianceName = 0x0031,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_TalkDirect = 0x0032,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_SetAllegianceName = 0x0033,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_UseWithTargetEvent = 0x0035,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_UseEvent = 0x0036,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_SetAllegianceOfficer = 0x003B,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_SetAllegianceOfficerTitle = 0x003C,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_ListAllegianceOfficerTitles = 0x003D,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_ClearAllegianceOfficerTitles = 0x003E,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_DoAllegianceLockAction = 0x003F,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_SetAllegianceApprovedVassal = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AllegianceChatGag = 0x0041,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_DoAllegianceHouseAction = 0x0042,

        /// <summary>
        /// TODO
        /// </summary>
        Train_TrainAttribute2nd = 0x0044,

        /// <summary>
        /// TODO
        /// </summary>
        Train_TrainAttribute = 0x0045,

        /// <summary>
        /// TODO
        /// </summary>
        Train_TrainSkill = 0x0046,

        /// <summary>
        /// TODO
        /// </summary>
        Train_TrainSkillAdvancementClass = 0x0047,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_CastUntargetedSpell = 0x0048,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_CastTargetedSpell = 0x004A,

        /// <summary>
        /// TODO
        /// </summary>
        Item_StopViewingObjectContents = 0x0052,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_ChangeCombatMode = 0x0053,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_StackableMerge = 0x0054,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_StackableSplitToContainer = 0x0055,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_StackableSplitTo3D = 0x0056,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_ModifyCharacterSquelch = 0x0058,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_ModifyAccountSquelch = 0x0059,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_ModifyGlobalSquelch = 0x005B,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_TalkDirectByName = 0x005D,

        /// <summary>
        /// TODO
        /// </summary>
        Vendor_Buy = 0x005F,

        /// <summary>
        /// TODO
        /// </summary>
        Vendor_Sell = 0x0060,

        /// <summary>
        /// TODO
        /// </summary>
        Vendor_VendorInfo = 0x0062,

        /// <summary>
        /// TODO
        /// </summary>
        Character_TeleToLifestone = 0x0063,

        /// <summary>
        /// TODO
        /// </summary>
        Character_StartBarber = 0x0075,

        /// <summary>
        /// TODO
        /// </summary>
        Character_ServerSaysAttemptFailed = 0x00A0,

        /// <summary>
        /// TODO
        /// </summary>
        Character_LoginCompleteNotification = 0x00A1,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_Create = 0x00A2,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_Quit = 0x00A3,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_Dismiss = 0x00A4,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_Recruit = 0x00A5,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_UpdateRequest = 0x00A6,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookAddPage = 0x00AA,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookModifyPage = 0x00AB,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookData = 0x00AC,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookDeletePage = 0x00AD,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookPageData = 0x00AE,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookOpen = 0x00B4,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookAddPageResponse = 0x00B6,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookDeletePageResponse = 0x00B7,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_BookPageDataResponse = 0x00B8,

        /// <summary>
        /// TODO
        /// </summary>
        Writing_SetInscription = 0x00BF,

        /// <summary>
        /// TODO
        /// </summary>
        Item_GetInscriptionResponse = 0x00C3,

        /// <summary>
        /// TODO
        /// </summary>
        Item_Appraise = 0x00C8,

        /// <summary>
        /// TODO
        /// </summary>
        Item_SetAppraiseInfo = 0x00C9,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_GiveObjectRequest = 0x00CD,

        /// <summary>
        /// TODO
        /// </summary>
        Advocate_Teleport = 0x00D6,

        /// <summary>
        /// TODO
        /// </summary>
        Character_AbuseLogRequest = 0x0140,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_AddToChannel = 0x0145,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_RemoveFromChannel = 0x0146,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_ChannelBroadcast = 0x0147,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_ChannelList = 0x0148,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_ChannelIndex = 0x0149,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_NoLongerViewingContents = 0x0195,

        /// <summary>
        /// TODO
        /// </summary>
        Item_OnViewContents = 0x0196,

        /// <summary>
        /// TODO
        /// </summary>
        Item_UpdateStackSize = 0x0197,

        /// <summary>
        /// TODO
        /// </summary>
        Item_ServerSaysMoveItem = 0x019A,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_StackableSplitToWield = 0x019B,

        /// <summary>
        /// TODO
        /// </summary>
        Character_AddShortCut = 0x019C,

        /// <summary>
        /// TODO
        /// </summary>
        Character_RemoveShortCut = 0x019D,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandlePlayerDeathEvent = 0x019E,

        /// <summary>
        /// TODO
        /// </summary>
        Character_CharacterOptionsEvent = 0x01A1,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleAttackDoneEvent = 0x01A7,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_RemoveSpell = 0x01A8,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleVictimNotificationEventSelf = 0x01AC,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleVictimNotificationEventOther = 0x01AD,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleAttackerNotificationEvent = 0x01B1,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleDefenderNotificationEvent = 0x01B2,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleEvasionAttackerNotificationEvent = 0x01B3,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleEvasionDefenderNotificationEvent = 0x01B4,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_CancelAttack = 0x01B7,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_HandleCommenceAttackEvent = 0x01B8,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_QueryHealth = 0x01BF,

        /// <summary>
        /// TODO
        /// </summary>
        Combat_QueryHealthResponse = 0x01C0,

        /// <summary>
        /// TODO
        /// </summary>
        Character_QueryAge = 0x01C2,

        /// <summary>
        /// TODO
        /// </summary>
        Character_QueryAgeResponse = 0x01C3,

        /// <summary>
        /// TODO
        /// </summary>
        Character_QueryBirth = 0x01C4,

        /// <summary>
        /// TODO
        /// </summary>
        Item_UseDone = 0x01C7,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_FellowUpdateDone = 0x01C9,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_FellowStatsDone = 0x01CA,

        /// <summary>
        /// TODO
        /// </summary>
        Item_AppraiseDone = 0x01CB,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemoveIntEvent = 0x01D1,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemoveIntEvent = 0x01D2,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemoveBoolEvent = 0x01D3,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemoveBoolEvent = 0x01D4,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemoveFloatEvent = 0x01D5,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemoveFloatEvent = 0x01D6,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemoveStringEvent = 0x01D7,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemoveStringEvent = 0x01D8,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemoveDataIDEvent = 0x01D9,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemoveDataIDEvent = 0x01DA,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemoveInstanceIDEvent = 0x01DB,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemoveInstanceIDEvent = 0x01DC,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemovePositionEvent = 0x01DD,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemovePositionEvent = 0x01DE,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_Emote = 0x01DF,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_HearEmote = 0x01E0,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_SoulEmote = 0x01E1,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_HearSoulEmote = 0x01E2,

        /// <summary>
        /// TODO
        /// </summary>
        Character_AddSpellFavorite = 0x01E3,

        /// <summary>
        /// TODO
        /// </summary>
        Character_RemoveSpellFavorite = 0x01E4,

        /// <summary>
        /// TODO
        /// </summary>
        Character_RequestPing = 0x01E9,

        /// <summary>
        /// TODO
        /// </summary>
        Character_ReturnPing = 0x01EA,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_SetSquelchDB = 0x01F4,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_OpenTradeNegotiations = 0x01F6,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_CloseTradeNegotiations = 0x01F7,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_AddToTrade_C2S = 0x01F8,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_AcceptTrade_C2S = 0x01FA,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_DeclineTrade_C2S = 0x01FB,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_RegisterTrade = 0x01FD,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_OpenTrade = 0x01FE,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_CloseTrade = 0x01FF,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_AddToTrade = 0x0200,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_RemoveFromTrade = 0x0201,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_AcceptTrade = 0x0202,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_DeclineTrade = 0x0203,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_ResetTrade_C2S = 0x0204,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_ResetTrade = 0x0205,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_TradeFailure = 0x0207,

        /// <summary>
        /// TODO
        /// </summary>
        Trade_ClearTradeAcceptance = 0x0208,

        /// <summary>
        /// TODO
        /// </summary>
        Character_ClearPlayerConsentList = 0x0216,

        /// <summary>
        /// TODO
        /// </summary>
        Character_DisplayPlayerConsentList = 0x0217,

        /// <summary>
        /// TODO
        /// </summary>
        Character_RemoveFromPlayerConsentList = 0x0218,

        /// <summary>
        /// TODO
        /// </summary>
        Character_AddPlayerPermission = 0x0219,

        /// <summary>
        /// TODO
        /// </summary>
        House_BuyHouse = 0x021C,

        /// <summary>
        /// TODO
        /// </summary>
        House_HouseProfile = 0x021D,

        /// <summary>
        /// TODO
        /// </summary>
        House_QueryHouse = 0x021E,

        /// <summary>
        /// TODO
        /// </summary>
        House_AbandonHouse = 0x021F,

        /// <summary>
        /// TODO
        /// </summary>
        Character_RemovePlayerPermission = 0x0220,

        /// <summary>
        /// TODO
        /// </summary>
        House_RentHouse = 0x0221,

        /// <summary>
        /// TODO
        /// </summary>
        Character_SetDesiredComponentLevel = 0x0224,

        /// <summary>
        /// TODO
        /// </summary>
        House_HouseData = 0x0225,

        /// <summary>
        /// TODO
        /// </summary>
        House_HouseStatus = 0x0226,

        /// <summary>
        /// TODO
        /// </summary>
        House_UpdateRentTime = 0x0227,

        /// <summary>
        /// TODO
        /// </summary>
        House_UpdateRentPayment = 0x0228,

        /// <summary>
        /// TODO
        /// </summary>
        House_AddPermanentGuest = 0x0245,

        /// <summary>
        /// TODO
        /// </summary>
        House_RemovePermanentGuest = 0x0246,

        /// <summary>
        /// TODO
        /// </summary>
        House_SetOpenHouseStatus = 0x0247,

        /// <summary>
        /// TODO
        /// </summary>
        House_UpdateRestrictions = 0x0248,

        /// <summary>
        /// TODO
        /// </summary>
        House_ChangeStoragePermission = 0x0249,

        /// <summary>
        /// TODO
        /// </summary>
        House_BootSpecificHouseGuest = 0x024A,

        /// <summary>
        /// TODO
        /// </summary>
        House_RemoveAllStoragePermission = 0x024C,

        /// <summary>
        /// TODO
        /// </summary>
        House_RequestFullGuestList = 0x024D,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_SetMotd = 0x0254,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_QueryMotd = 0x0255,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_ClearMotd = 0x0256,

        /// <summary>
        /// TODO
        /// </summary>
        House_UpdateHAR = 0x0257,

        /// <summary>
        /// TODO
        /// </summary>
        House_QueryLord = 0x0258,

        /// <summary>
        /// TODO
        /// </summary>
        House_HouseTransaction = 0x0259,

        /// <summary>
        /// TODO
        /// </summary>
        House_AddAllStoragePermission = 0x025C,

        /// <summary>
        /// TODO
        /// </summary>
        House_RemoveAllPermanentGuests = 0x025E,

        /// <summary>
        /// TODO
        /// </summary>
        House_BootEveryone = 0x025F,

        /// <summary>
        /// TODO
        /// </summary>
        House_TeleToHouse = 0x0262,

        /// <summary>
        /// TODO
        /// </summary>
        Item_QueryItemMana = 0x0263,

        /// <summary>
        /// TODO
        /// </summary>
        Item_QueryItemManaResponse = 0x0264,

        /// <summary>
        /// TODO
        /// </summary>
        House_SetHooksVisibility = 0x0266,

        /// <summary>
        /// TODO
        /// </summary>
        House_ModifyAllegianceGuestPermission = 0x0267,

        /// <summary>
        /// TODO
        /// </summary>
        House_ModifyAllegianceStoragePermission = 0x0268,

        /// <summary>
        /// TODO
        /// </summary>
        Game_Join = 0x0269,

        /// <summary>
        /// TODO
        /// </summary>
        Game_Quit = 0x026A,

        /// <summary>
        /// TODO
        /// </summary>
        Game_Move = 0x026B,

        /// <summary>
        /// TODO
        /// </summary>
        Game_MovePass = 0x026D,

        /// <summary>
        /// TODO
        /// </summary>
        Game_Stalemate = 0x026E,

        /// <summary>
        /// TODO
        /// </summary>
        House_ListAvailableHouses = 0x0270,

        /// <summary>
        /// TODO
        /// </summary>
        House_AvailableHouses = 0x0271,

        /// <summary>
        /// TODO
        /// </summary>
        Character_ConfirmationRequest = 0x0274,

        /// <summary>
        /// TODO
        /// </summary>
        Character_ConfirmationResponse = 0x0275,

        /// <summary>
        /// TODO
        /// </summary>
        Character_ConfirmationDone = 0x0276,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_BreakAllegianceBoot = 0x0277,

        /// <summary>
        /// TODO
        /// </summary>
        House_TeleToMansion = 0x0278,

        /// <summary>
        /// TODO
        /// </summary>
        Character_Suicide = 0x0279,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AllegianceLoginNotificationEvent = 0x027A,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AllegianceInfoRequest = 0x027B,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AllegianceInfoResponseEvent = 0x027C,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_CreateTinkeringTool = 0x027D,

        /// <summary>
        /// TODO
        /// </summary>
        Game_JoinGameResponse = 0x0281,

        /// <summary>
        /// TODO
        /// </summary>
        Game_StartGame = 0x0282,

        /// <summary>
        /// TODO
        /// </summary>
        Game_MoveResponse = 0x0283,

        /// <summary>
        /// TODO
        /// </summary>
        Game_OpponentTurn = 0x0284,

        /// <summary>
        /// TODO
        /// </summary>
        Game_OpponentStalemateState = 0x0285,

        /// <summary>
        /// TODO
        /// </summary>
        Character_SpellbookFilterEvent = 0x0286,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_WeenieError = 0x028A,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_WeenieErrorWithString = 0x028B,

        /// <summary>
        /// TODO
        /// </summary>
        Game_GameOver = 0x028C,

        /// <summary>
        /// TODO
        /// </summary>
        Character_TeleToMarketplace = 0x028D,

        /// <summary>
        /// TODO
        /// </summary>
        Character_EnterPKLite = 0x028F,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_AssignNewLeader = 0x0290,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_ChangeFellowOpeness = 0x0291,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_ChatRoomTracker = 0x0295,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AllegianceChatBoot = 0x02A0,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_AddAllegianceBan = 0x02A1,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_RemoveAllegianceBan = 0x02A2,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_ListAllegianceBans = 0x02A3,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_RemoveAllegianceOfficer = 0x02A5,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_ListAllegianceOfficers = 0x02A6,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_ClearAllegianceOfficers = 0x02A7,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance_RecallAllegianceHometown = 0x02AB,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_QueryPluginList = 0x02AE,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_QueryPluginListResponse = 0x02AF,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_QueryPlugin = 0x02B1,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_QueryPluginResponse = 0x02B2,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_QueryPluginResponse2 = 0x02B3,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_SalvageOperationsResultData = 0x02B4,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateRemoveInt64Event = 0x02B8,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_RemoveInt64Event = 0x02B9,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_HearSpeech = 0x02BB,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_HearRangedSpeech = 0x02BC,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_HearDirectSpeech = 0x02BD,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_FullUpdate = 0x02BE,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_Disband = 0x02BF,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship_UpdateFellow = 0x02C0,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_UpdateSpell = 0x02C1,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_UpdateEnchantment = 0x02C2,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_RemoveEnchantment = 0x02C3,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_UpdateMultipleEnchantments = 0x02C4,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_RemoveMultipleEnchantments = 0x02C5,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_PurgeEnchantments = 0x02C6,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_DispelEnchantment = 0x02C7,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_DispelMultipleEnchantments = 0x02C8,

        /// <summary>
        /// TODO
        /// </summary>
        Misc_PortalStormBrewing = 0x02C9,

        /// <summary>
        /// TODO
        /// </summary>
        Misc_PortalStormImminent = 0x02CA,

        /// <summary>
        /// TODO
        /// </summary>
        Misc_PortalStorm = 0x02CB,

        /// <summary>
        /// TODO
        /// </summary>
        Misc_PortalStormSubsided = 0x02CC,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateInt = 0x02CD,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateInt = 0x02CE,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateInt64 = 0x02CF,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateInt64 = 0x02D0,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateBool = 0x02D1,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateBool = 0x02D2,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateFloat = 0x02D3,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateFloat = 0x02D4,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateString = 0x02D5,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateString = 0x02D6,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateDataID = 0x02D7,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateDataID = 0x02D8,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateInstanceID = 0x02D9,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateInstanceID = 0x02DA,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdatePosition = 0x02DB,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdatePosition = 0x02DC,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateSkill = 0x02DD,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateSkill = 0x02DE,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateSkillLevel = 0x02DF,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateSkillLevel = 0x02E0,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateSkillAC = 0x02E1,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateSkillAC = 0x02E2,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateAttribute = 0x02E3,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateAttribute = 0x02E4,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateAttributeLevel = 0x02E5,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateAttributeLevel = 0x02E6,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateAttribute2nd = 0x02E7,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateAttribute2nd = 0x02E8,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_PrivateUpdateAttribute2ndLevel = 0x02E9,

        /// <summary>
        /// TODO
        /// </summary>
        Qualities_UpdateAttribute2ndLevel = 0x02EA,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_TransientString = 0x02EB,

        /// <summary>
        /// TODO
        /// </summary>
        Character_FinishBarber = 0x0311,

        /// <summary>
        /// TODO
        /// </summary>
        Magic_PurgeBadEnchantments = 0x0312,

        /// <summary>
        /// TODO
        /// </summary>
        Social_SendClientContractTrackerTable = 0x0314,

        /// <summary>
        /// TODO
        /// </summary>
        Social_SendClientContractTracker = 0x0315,

        /// <summary>
        /// TODO
        /// </summary>
        Social_AbandonContract = 0x0316,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_Environs = 0xEA60,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_PositionAndMovementEvent = 0xF619,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_Jump = 0xF61B,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_MoveToState = 0xF61C,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_DoMovementCommand = 0xF61E,

        /// <summary>
        /// TODO
        /// </summary>
        Item_ObjDescEvent = 0xF625,

        /// <summary>
        /// TODO
        /// </summary>
        Character_SetPlayerVisualDesc = 0xF630,

        /// <summary>
        /// TODO
        /// </summary>
        Character_CharGenVerificationResponse = 0xF643,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_TurnToEvent = 0xF649,

        /// <summary>
        /// TODO
        /// </summary>
        Login_AwaitingSubscriptionExpiration = 0xF651,

        /// <summary>
        /// TODO
        /// </summary>
        Login_LogOffCharacter = 0xF653,

        /// <summary>
        /// TODO
        /// </summary>
        Login_ExecuteLogOff = 0xF653,

        /// <summary>
        /// TODO
        /// </summary>
        Character_CharacterDelete = 0xF655,

        /// <summary>
        /// TODO
        /// </summary>
        Character_SendCharGenResult = 0xF656,

        /// <summary>
        /// TODO
        /// </summary>
        Login_SendEnterWorld = 0xF657,

        /// <summary>
        /// TODO
        /// </summary>
        Login_LoginCharacterSet = 0xF658,

        /// <summary>
        /// TODO
        /// </summary>
        Character_CharacterError = 0xF659,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_StopMovementCommand = 0xF661,

        /// <summary>
        /// TODO
        /// </summary>
        Object_SendForceObjdesc = 0xF6EA,

        /// <summary>
        /// TODO
        /// </summary>
        Item_CreateObject = 0xF745,

        /// <summary>
        /// TODO
        /// </summary>
        Login_CreatePlayer = 0xF746,

        /// <summary>
        /// TODO
        /// </summary>
        Item_DeleteObject = 0xF747,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_PositionEvent = 0xF748,

        /// <summary>
        /// TODO
        /// </summary>
        Item_ParentEvent = 0xF749,

        /// <summary>
        /// TODO
        /// </summary>
        Inventory_PickupEvent = 0xF74A,

        /// <summary>
        /// TODO
        /// </summary>
        Item_SetState = 0xF74B,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_SetObjectMovement = 0xF74C,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_VectorUpdate = 0xF74E,

        /// <summary>
        /// TODO
        /// </summary>
        Effects_SoundEvent = 0xF750,

        /// <summary>
        /// TODO
        /// </summary>
        Effects_PlayerTeleport = 0xF751,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_AutonomyLevel = 0xF752,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_AutonomousPosition = 0xF753,

        /// <summary>
        /// TODO
        /// </summary>
        Effects_PlayScriptID = 0xF754,

        /// <summary>
        /// TODO
        /// </summary>
        Effects_PlayScriptType = 0xF755,

        /// <summary>
        /// TODO
        /// </summary>
        Network_WOrderHdr = 0xF7B0,

        /// <summary>
        /// TODO
        /// </summary>
        Network_OrderHdr = 0xF7B1,

        /// <summary>
        /// TODO
        /// </summary>
        Login_AccountBanned = 0xF7C1,

        /// <summary>
        /// TODO
        /// </summary>
        Login_SendEnterWorldRequest = 0xF7C8,

        /// <summary>
        /// TODO
        /// </summary>
        Movement_Jump_NonAutonomous = 0xF7C9,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_ReceiveAccountData = 0xF7CA,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_ReceivePlayerData = 0xF7CB,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_SendAdminGetServerVersion = 0xF7CC,

        /// <summary>
        /// TODO
        /// </summary>
        Social_SendFriendsCommand = 0xF7CD,

        /// <summary>
        /// TODO
        /// </summary>
        Admin_SendAdminRestoreCharacter = 0xF7D9,

        /// <summary>
        /// TODO
        /// </summary>
        Item_UpdateObject = 0xF7DB,

        /// <summary>
        /// TODO
        /// </summary>
        Login_AccountBooted = 0xF7DC,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_TurbineChat = 0xF7DE,

        /// <summary>
        /// TODO
        /// </summary>
        Login_EnterGame_ServerReady = 0xF7DF,

        /// <summary>
        /// TODO
        /// </summary>
        Communication_TextboxString = 0xF7E0,

        /// <summary>
        /// TODO
        /// </summary>
        Login_WorldInfo = 0xF7E1,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_DataMessage = 0xF7E2,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_RequestDataMessage = 0xF7E3,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_ErrorMessage = 0xF7E4,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_InterrogationMessage = 0xF7E5,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_InterrogationResponseMessage = 0xF7E6,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_BeginDDDMessage = 0xF7E7,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_OnEndDDD = 0xF7EA,

        /// <summary>
        /// TODO
        /// </summary>
        DDD_EndDDDMessage = 0xF7EB,

    }

    /// <summary>
    /// Set of predefined status messages, some of which take additional data as input
    /// </summary>
    public enum StatusMessage : uint {
        /// <summary>
        /// No error (success)
        /// </summary>
        None = 0x0000,

        NoMem = 0x0001,
        BadParam = 0x0002,
        DivZero = 0x0003,
        SegV = 0x0004,
        Unimplemented = 0x0005,
        UnknownMessageType = 0x0006,
        NoAnimationTable = 0x0007,
        NoPhysicsObject = 0x0008,
        NoBookieObject = 0x0009,
        NoWslObject = 0x000A,
        NoMotionInterpreter = 0x000B,
        UnhandledSwitch = 0x000C,
        DefaultConstructorCalled = 0x000D,
        InvalidCombatManeuver = 0x000E,
        BadCast = 0x000F,
        MissingQuality = 0x0010,
        // Skip 1
        MissingDatabaseObject = 0x0012,
        NoCallbackSet = 0x0013,
        CorruptQuality = 0x0014,
        BadContext = 0x0015,
        NoEphseqManager = 0x0016,

        /// <summary>
        /// You failed to go to non-combat mode.
        /// </summary>
        BadMovementEvent = 0x0017,

        CannotCreateNewObject = 0x0018,
        NoControllerObject = 0x0019,
        CannotSendEvent = 0x001A,
        PhysicsCantTransition = 0x001B,
        PhysicsMaxDistanceExceeded = 0x001C,

        /// <summary>
        /// You're too busy!
        /// </summary>
        YoureTooBusy = 0x001D,

        CannotSendMessage = 0x001F,

        /// <summary>
        /// You must control both objects!
        /// </summary>
        IllegalInventoryTransaction = 0x0020,

        ExternalWeenieObject = 0x0021,
        InternalWeenieObject = 0x0022,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        MotionFailure = 0x0023,

        /// <summary>
        /// You can't jump while in the air
        /// </summary>
        YouCantJumpWhileInTheAir = 0x0024,

        InqCylSphereFailure = 0x0025,

        /// <summary>
        /// That is not a valid command.
        /// </summary>
        ThatIsNotAValidCommand = 0x0026,

        CarryingItem = 0x0027,

        /// <summary>
        /// The item is under someone else's control!
        /// </summary>
        Frozen = 0x0028,

        /// <summary>
        /// You cannot pick that up!
        /// </summary>
        Stuck = 0x0029,

        /// <summary>
        /// You are too encumbered to carry that!
        /// </summary>
        YouAreTooEncumbered = 0x002A,

        BadContain = 0x002C,
        BadParent = 0x002D,
        BadDrop = 0x002E,
        BadRelease = 0x002F,
        MsgBadMsg = 0x0030,
        MsgUnpackFailed = 0x0031,
        MsgNoMsg = 0x0032,
        MsgUnderflow = 0x0033,
        MsgOverflow = 0x0034,
        MsgCallbackFailed = 0x0035,

        /// <summary>
        /// Action cancelled!
        /// </summary>
        ActionCancelled = 0x0036,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        ObjectGone = 0x0037,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        NoObject = 0x0038,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        CantGetThere = 0x0039,

        /// <summary>
        /// You can't do that... you're dead! or You can't do that... its dead!
        /// </summary>
        Dead = 0x003A,

        ILeftTheWorld = 0x003B,
        ITeleported = 0x003C,

        /// <summary>
        /// You charged too far!
        /// </summary>
        YouChargedTooFar = 0x003D,

        /// <summary>
        /// You are too tired to do that!
        /// </summary>
        YouAreTooTiredToDoThat = 0x003E,

        // Client side only
        CantCrouchInCombat = 0x003F,
        // Client side only
        CantSitInCombat = 0x0040,
        // Client side only
        CantLieDownInCombat = 0x0041,
        // Client side only
        CantChatEmoteInCombat = 0x0042,

        NoMtableData = 0x0043,

        // Client side only
        CantChatEmoteNotStanding = 0x0044,

        TooManyActions = 0x0045,
        Hidden = 0x0046,
        GeneralMovementFailure = 0x0047,

        /// <summary>
        /// You can't jump from this position
        /// </summary>
        YouCantJumpFromThisPosition = 0x0048,

        // Client side only
        CantJumpLoadedDown = 0x0049,

        /// <summary>
        /// Ack! You killed yourself!
        /// </summary>
        YouKilledYourself = 0x004A,

        MsgResponseFailure = 0x004B,
        ObjectIsStatic = 0x004C,

        /// <summary>
        /// Invalid PK Status!
        /// </summary>
        InvalidPkStatus = 0x004D,

        InvalidXpAmount = 0x03E9,
        InvalidPpCalculation = 0x03EA,
        InvalidCpCalculation = 0x03EB,
        UnhandledStatAnswer = 0x03EC,
        HeartAttack = 0x03ED,

        /// <summary>
        /// The container is closed!
        /// </summary>
        TheContainerIsClosed = 0x03EE,

        InvalidInventoryLocation = 0x03F0,

        /// <summary>
        /// You failed to go to non-combat mode.
        /// </summary>
        ChangeCombatModeFailure = 0x03F1,


        FullInventoryLocation = 0x03F2,
        ConflictingInventoryLocation = 0x03F3,
        ItemNotPending = 0x03F4,
        BeWieldedFailure = 0x03F5,
        BeDroppedFailure = 0x03F6,

        /// <summary>
        /// You are too fatigued to attack!
        /// </summary>
        YouAreTooFatiguedToAttack = 0x03F7,

        /// <summary>
        /// You are out of ammunition!
        /// </summary>
        YouAreOutOfAmmunition = 0x03F8,

        /// <summary>
        /// Your missile attack misfired!
        /// </summary>
        YourAttackMisfired = 0x03F9,

        /// <summary>
        /// You've attempted an impossible spell path!
        /// </summary>
        YouveAttemptedAnImpossibleSpellPath = 0x03FA,

        MagicIncompleteAnimList = 0x03FB,
        MagicInvalidSpellType = 0x03FC,
        MagicInqPositionAndVelocityFailure = 0x03FD,

        /// <summary>
        /// You don't know that spell!
        /// </summary>
        YouDontKnowThatSpell = 0x03FE,

        /// <summary>
        /// Incorrect target type
        /// </summary>
        IncorrectTargetType = 0x03FF,

        /// <summary>
        /// You don't have all the components for this spell.
        /// </summary>
        YouDontHaveAllTheComponents = 0x0400,

        /// <summary>
        /// You don't have enough Mana to cast this spell.
        /// </summary>
        YouDontHaveEnoughManaToCast = 0x0401,

        /// <summary>
        /// Your spell fizzled.
        /// </summary>
        YourSpellFizzled = 0x0402,

        /// <summary>
        /// Your spell's target is missing!
        /// </summary>
        YourSpellTargetIsMissing = 0x0403,

        /// <summary>
        /// Your projectile spell mislaunched!
        /// </summary>
        YourProjectileSpellMislaunched = 0x0404,

        MagicSpellbookAddSpellFailure = 0x0405,
        MagicTargetOutOfRange = 0x0406,

        /// <summary>
        /// Your spell cannot be cast outside
        /// </summary>
        YourSpellCannotBeCastOutside = 0x0407,

        /// <summary>
        /// Your spell cannot be cast inside
        /// </summary>
        YourSpellCannotBeCastInside = 0x0408,

        MagicGeneralFailure = 0x0409,

        /// <summary>
        /// You are unprepared to cast a spell
        /// </summary>
        YouAreUnpreparedToCastASpell = 0x040A,

        /// <summary>
        /// You've already sworn your Allegiance
        /// </summary>
        YouveAlreadySwornAllegiance = 0x040B,

        /// <summary>
        /// You don't have enough experience available to swear Allegiance
        /// </summary>
        CantSwearAllegianceInsufficientXp = 0x040C,

        AllegianceIgnoringRequests = 0x040D,
        AllegianceSquelched = 0x040E,
        AllegianceMaxDistanceExceeded = 0x040F,
        AllegianceIllegalLevel = 0x0410,
        AllegianceBadCreation = 0x0411,
        AllegiancePatronBusy = 0x0412,

        /// <summary>
        /// You are not in an allegiance!
        /// </summary>
        YouAreNotInAllegiance = 0x0414,

        AllegianceRemoveHierarchyFailure = 0x0415,

        FellowshipIgnoringRequests = 0x0417,
        FellowshipSquelched = 0x0418,
        FellowshipMaxDistanceExceeded = 0x0419,
        FellowshipMember = 0x041A,
        FellowshipIllegalLevel = 0x041B,
        FellowshipRecruitBusy = 0x041C,

        /// <summary>
        /// You must be the leader of a Fellowship
        /// </summary>
        YouMustBeLeaderOfFellowship = 0x041D,

        /// <summary>
        /// Your Fellowship is full
        /// </summary>
        YourFellowshipIsFull = 0x041E,

        /// <summary>
        /// That Fellowship name is not permitted
        /// </summary>
        FellowshipNameIsNotPermitted = 0x041F,

        LevelTooLow = 0x0420,
        LevelTooHigh = 0x0421,

        /// <summary>
        /// That channel doesn't exist.
        /// </summary>
        ThatChannelDoesntExist = 0x0422,

        /// <summary>
        /// You can't use that channel.
        /// </summary>
        YouCantUseThatChannel = 0x0423,

        /// <summary>
        /// You're already on that channel.
        /// </summary>
        YouAreAlreadyOnThatChannel = 0x0424,

        /// <summary>
        /// You're not currently on that channel.
        /// </summary>
        YouAreNotOnThatChannel = 0x0425,

        /// <summary>
        /// _ can't be dropped
        /// </summary>
        AttunedItem = 0x0426,

        /// <summary>
        /// You cannot merge different stacks!
        /// </summary>
        YouCannotMergeDifferentStacks = 0x0427,

        /// <summary>
        /// You cannot merge enchanted items!
        /// </summary>
        YouCannotMergeEnchantedItems = 0x0428,

        /// <summary>
        /// You must control at least one stack!
        /// </summary>
        YouMustControlAtLeastOneStack = 0x0429,

        CurrentlyAttacking = 0x042A,
        MissileAttackNotOk = 0x042B,
        TargetNotAcquired = 0x042C,
        ImpossibleShot = 0x042D,
        BadWeaponSkill = 0x042E,
        UnwieldFailure = 0x042F,
        LaunchFailure = 0x0430,
        ReloadFailure = 0x0431,

        /// <summary>
        /// Your craft attempt fails.
        /// </summary>
        UnableToMakeCraftReq = 0x0432,

        /// <summary>
        /// Your craft attempt fails.
        /// </summary>
        CraftAnimationFailed = 0x0433,

        /// <summary>
        /// Given that number of items, you cannot craft anything.
        /// </summary>
        YouCantCraftWithThatNumberOfItems = 0x0434,

        /// <summary>
        /// Your craft attempt fails.
        /// </summary>
        CraftGeneralErrorUiMsg = 0x0435,

        CraftGeneralErrorNoUiMsg = 0x0436,

        /// <summary>
        /// Either you or one of the items involved does not pass the requirements for this craft interaction.
        /// </summary>
        YouDoNotPassCraftingRequirements = 0x0437,

        /// <summary>
        /// You do not have all the neccessary items.
        /// </summary>
        YouDoNotHaveAllTheNecessaryItems = 0x0438,

        /// <summary>
        /// Not all the items are avaliable.
        /// </summary>
        NotAllTheItemsAreAvailable = 0x0439,

        /// <summary>
        /// You must be at rest in peace mode to do trade skills.
        /// </summary>
        YouMustBeInPeaceModeToTrade = 0x043A,

        /// <summary>
        /// You are not trained in that trade skill.
        /// </summary>
        YouAreNotTrainedInThatTradeSkill = 0x043B,

        /// <summary>
        /// Your hands must be free.
        /// </summary>
        YourHandsMustBeFree = 0x043C,

        /// <summary>
        /// You cannot link to that portal!
        /// </summary>
        YouCannotLinkToThatPortal = 0x043D,

        /// <summary>
        /// You have solved this quest too recently!
        /// </summary>
        YouHaveSolvedThisQuestTooRecently = 0x043E,

        /// <summary>
        /// You have solved this quest too many times!
        /// </summary>
        YouHaveSolvedThisQuestTooManyTimes = 0x043F,

        QuestUnknown = 0x0440,
        QuestTableCorrupt = 0x0441,
        QuestBad = 0x0442,
        QuestDuplicate = 0x0443,
        QuestUnsolved = 0x0444,

        /// <summary>
        /// This item requires you to complete a specific quest before you can pick it up!
        /// </summary>
        ItemRequiresQuestToBePickedUp = 0x0445,

        QuestSolvedTooLongAgo = 0x0446,

        TradeIgnoringRequests = 0x044C,
        TradeSquelched = 0x044D,
        TradeMaxDistanceExceeded = 0x044E,
        TradeAlreadyTrading = 0x044F,
        TradeBusy = 0x0450,
        TradeClosed = 0x0451,
        TradeExpired = 0x0452,
        TradeItemBeingTraded = 0x0453,
        TradeNonEmptyContainer = 0x0454,
        TradeNonCombatMode = 0x0455,
        TradeIncomplete = 0x0456,
        TradeStampMismatch = 0x0457,
        TradeUnopened = 0x0458,
        TradeEmpty = 0x0459,
        TradeAlreadyAccepted = 0x045A,
        TradeOutOfSync = 0x045B,

        /// <summary>
        /// Player killers may not interact with that portal!
        /// </summary>
        PKsMayNotUsePortal = 0x045C,

        /// <summary>
        /// Non-player killers may not interact with that portal!
        /// </summary>
        NonPKsMayNotUsePortal = 0x045D,

        /// <summary>
        ///  You do not own a house!
        /// </summary>
        HouseAbandoned = 0x045E,

        /// <summary>
        ///  You do not own a house!
        /// </summary>
        HouseEvicted = 0x045F,

        HouseAlreadyOwned = 0x0460,
        HouseBuyFailed = 0x0461,
        HouseRentFailed = 0x0462,
        Hooked = 0x0463,

        MagicInvalidPosition = 0x0465,

        /// <summary>
        /// You must purchase Asheron's Call: Dark Majesty to interact with that portal.
        /// </summary>
        YouMustHaveDarkMajestyToUsePortal = 0x0466,

        InvalidAmmoType = 0x0467,
        SkillTooLow = 0x0468,

        /// <summary>
        /// You have used all the hooks you are allowed to use for this house.
        /// </summary>
        YouHaveUsedAllTheHooks = 0x0469,

        /// <summary>
        /// _ doesn't know what to do with that.
        /// </summary>
        TradeAiDoesntWant = 0x046A,

        HookHouseNotOwned = 0x046B,

        /// <summary>
        /// You must complete a quest to interact with that portal.
        /// </summary>
        YouMustCompleteQuestToUsePortal = 0x0474,

        HouseNoAllegiance = 0x047E,

        /// <summary>
        /// You must own a house to use this command.
        /// </summary>
        YouMustOwnHouseToUseCommand = 0x047F,

        /// <summary>
        /// Your monarch does not own a mansion or a villa!
        /// </summary>
        YourMonarchDoesNotOwnAMansionOrVilla = 0x0480,

        /// <summary>
        /// Your monarch does not own a mansion or a villa!
        /// </summary>
        YourMonarchsHouseIsNotAMansionOrVilla = 0x0481,

        /// <summary>
        /// Your monarch has closed the mansion to the Allegiance.
        /// </summary>
        YourMonarchHasClosedTheMansion = 0x0482,

        /// <summary>
        /// You must be a monarch to purchase this dwelling.
        /// </summary>
        YouMustBeMonarchToPurchaseDwelling = 0x048A,

        // Perhaps 30 seconds like fellowship timeout?
        AllegianceTimeout = 0x048D,

        /// <summary>
        /// Your offer of Allegiance has been ignored.
        /// </summary>
        YourOfferOfAllegianceWasIgnored = 0x048E,

        /// <summary>
        /// You are already involved in something!
        /// </summary>
        ConfirmationInProgress = 0x048F,

        /// <summary>
        /// You must be a monarch to use this command.
        /// </summary>
        YouMustBeAMonarchToUseCommand = 0x0490,

        /// <summary>
        /// You must specify a character to boot. (From allegiance)
        /// </summary>
        YouMustSpecifyCharacterToBoot = 0x0491,

        /// <summary>
        /// You can't boot yourself! (From allegiance)
        /// </summary>
        YouCantBootYourself = 0x0492,

        /// <summary>
        /// That character does not exist.
        /// </summary>
        ThatCharacterDoesNotExist = 0x0493,

        /// <summary>
        /// That person is not a member of your Allegiance!
        /// </summary>
        ThatPersonIsNotInYourAllegiance = 0x0494,

        /// <summary>
        /// No patron from which to break!
        /// </summary>
        CantBreakFromPatronNotInAllegiance = 0x0495,

        /// <summary>
        /// Your Allegiance has been dissolved!
        /// </summary>
        YourAllegianceHasBeenDissolved = 0x0496,

        /// <summary>
        /// Your patron's Allegiance to you has been broken!
        /// </summary>
        YourPatronsAllegianceHasBeenBroken = 0x0497,

        /// <summary>
        /// You have moved too far!
        /// </summary>
        YouHaveMovedTooFar = 0x0498,

        /// <summary>
        /// That is not a valid destination!
        /// </summary>
        TeleToInvalidPosition = 0x0499,

        /// <summary>
        /// You must purchase Asheron's Call -- Dark Majesty to use this function.
        /// </summary>
        MustHaveDarkMajestyToUse = 0x049A,

        /// <summary>
        /// You fail to link with the lifestone!
        /// </summary>
        YouFailToLinkWithLifestone = 0x049B,

        /// <summary>
        /// You wandered too far to link with the lifestone!
        /// </summary>
        YouWanderedTooFarToLinkWithLifestone = 0x049C,

        /// <summary>
        /// You successfully link with the lifestone!
        /// </summary>
        YouSuccessfullyLinkWithLifestone = 0x049D,

        /// <summary>
        /// You must have linked with a lifestone in order to recall to it!
        /// </summary>
        YouMustLinkToLifestoneToRecall = 0x049E,

        /// <summary>
        /// You fail to recall to the lifestone!
        /// </summary>
        YouFailToRecallToLifestone = 0x049F,

        /// <summary>
        /// You fail to link with the portal!
        /// </summary>
        YouFailToLinkWithPortal = 0x04A0,

        /// <summary>
        /// You successfully link with the portal!
        /// </summary>
        YouSuccessfullyLinkWithPortal = 0x04A1,

        /// <summary>
        /// You fail to recall to the portal!
        /// </summary>
        YouFailToRecallToPortal = 0x04A2,

        /// <summary>
        /// You must have linked with a portal in order to recall to it!
        /// </summary>
        YouMustLinkToPortalToRecall = 0x04A3,

        /// <summary>
        /// You fail to summon the portal!
        /// </summary>
        YouFailToSummonPortal = 0x04A4,

        /// <summary>
        /// You must have linked with a portal in order to summon it!
        /// </summary>
        YouMustLinkToPortalToSummonIt = 0x04A5,

        /// <summary>
        /// You fail to teleport!
        /// </summary>
        YouFailToTeleport = 0x04A6,

        /// <summary>
        /// You have been teleported too recently!
        /// </summary>
        YouHaveBeenTeleportedTooRecently = 0x04A7,

        /// <summary>
        /// You must be an Advocate to interact with that portal.
        /// </summary>
        YouMustBeAnAdvocateToUsePortal = 0x04A8,

        PortalAisNotAllowed = 0x04A9,

        /// <summary>
        /// Players may not interact with that portal.
        /// </summary>
        PlayersMayNotUsePortal = 0x04AA,

        /// <summary>
        /// You are not powerful enough to interact with that portal!
        /// </summary>
        YouAreNotPowerfulEnoughToUsePortal = 0x04AB,

        /// <summary>
        /// You are too powerful to interact with that portal!
        /// </summary>
        YouAreTooPowerfulToUsePortal = 0x04AC,

        /// <summary>
        /// You cannot recall to that portal!
        /// </summary>
        YouCannotRecallPortal = 0x04AD,

        /// <summary>
        /// You cannot summon that portal!
        /// </summary>
        YouCannotSummonPortal = 0x04AE,

        /// <summary>
        /// The lock is already unlocked.
        /// </summary>
        LockAlreadyUnlocked = 0x04AF,

        /// <summary>
        /// You can't lock or unlock that!
        /// </summary>
        YouCannotLockOrUnlockThat = 0x04B0,

        /// <summary>
        /// You can't lock or unlock what is open!
        /// </summary>
        YouCannotLockWhatIsOpen = 0x04B1,

        /// <summary>
        /// The key doesn't fit this lock.
        /// </summary>
        KeyDoesntFitThisLock = 0x04B2,

        /// <summary>
        /// The lock has been used too recently.
        /// </summary>
        LockUsedTooRecently = 0x04B3,

        /// <summary>
        /// You aren't trained in lockpicking!
        /// </summary>
        YouArentTrainedInLockpicking = 0x04B4,

        /// <summary>
        /// You must specify a character to query.
        /// </summary>
        AllegianceInfoEmptyName = 0x04B5,

        /// <summary>
        /// Please use the allegiance panel to view your own information.
        /// </summary>
        AllegianceInfoSelf = 0x04B6,

        /// <summary>
        /// You have used that command too recently.
        /// </summary>
        AllegianceInfoTooRecent = 0x04B7,

        AbuseNoSuchCharacter = 0x04B8,
        AbuseReportedSelf = 0x04B9,
        AbuseComplaintHandled = 0x04BA,

        /// <summary>
        /// You do not own that salvage tool!
        /// </summary>
        YouDoNotOwnThatSalvageTool = 0x04BD,

        /// <summary>
        /// You do not own that item!
        /// </summary>
        YouDoNotOwnThatItem = 0x04BE,

        /// <summary>
        /// The material cannot be created.
        /// </summary>
        MaterialCannotBeCreated = 0x04C1,

        /// <summary>
        /// The list of items you are attempting to salvage is invalid.
        /// </summary>
        ItemsAttemptingToSalvageIsInvalid = 0x04C2,

        /// <summary>
        /// You cannot salvage items that you are trading!
        /// </summary>
        YouCannotSalvageItemsInTrading = 0x04C3,

        /// <summary>
        /// You must be a guest in this house to interact with that portal.
        /// </summary>
        YouMustBeHouseGuestToUsePortal = 0x04C4,

        /// <summary>
        /// Your Allegiance Rank is too low to use that item's magic.
        /// </summary>
        YourAllegianceRankIsTooLowToUseMagic = 0x04C5,

        /// <summary>
        /// Your Arcane Lore skill is too low to use that item's magic.
        /// </summary>
        YourArcaneLoreIsTooLowToUseMagic = 0x04C7,

        /// <summary>
        /// That item doesn't have enough Mana.
        /// </summary>
        ItemDoesntHaveEnoughMana = 0x04C8,

        /// <summary>
        /// You have been involved in a player killer battle too recently to do that!
        /// </summary>
        YouHaveBeenInPKBattleTooRecently = 0x04CC,

        TradeAiRefuseEmote = 0x04CD,

        /// <summary>
        /// You have failed to alter your skill.
        /// </summary>
        YouFailToAlterSkill = 0x04D0,

        FellowshipDeclined = 0x04DB,

        /// <summary>
        /// Appears to be sent to a recruiter 30 seconds after the initial recruit message when the recruitee doesn't respond.
        /// </summary>
        FellowshipTimeout = 0x04DC,

        /// <summary>
        /// You have failed to alter your attributes.
        /// </summary>
        YouHaveFailedToAlterAttributes = 0x04DD,

        /// <summary>
        /// You are currently wielding items which require a certain level of skill. Your attributes cannot be transferred while you are wielding these items. Please remove these items and try again.
        /// </summary>
        CannotTransferAttributesWhileWieldingItem = 0x04E0,

        /// <summary>
        /// You have succeeded in transferring your attributes!
        /// </summary>
        YouHaveSucceededTransferringAttributes = 0x04E1,

        /// <summary>
        /// This hook is a duplicated housing object. You may not add items to a duplicated housing object. Please empty the hook and allow it to reset.
        /// </summary>
        HookIsDuplicated = 0x04E2,

        /// <summary>
        /// That item is of the wrong type to be placed on this hook.
        /// </summary>
        ItemIsWrongTypeForHook = 0x04E3,

        /// <summary>
        /// This chest is a duplicated housing object. You may not add items to a duplicated housing object. Please empty everything -- including backpacks -- out of the chest and allow the chest to reset.
        /// </summary>
        HousingChestIsDuplicated = 0x04E4,

        /// <summary>
        /// This hook was a duplicated housing object. Since it is now empty, it will be deleted momentarily. Once it is gone, it is safe to use the other, non-duplicated hook that is here.
        /// </summary>
        HookWillBeDeleted = 0x04E5,

        /// <summary>
        /// This chest was a duplicated housing object. Since it is now empty, it will be deleted momentarily. Once it is gone, it is safe to use the other, non-duplicated chest that is here.
        /// </summary>
        HousingChestWillBeDeleted = 0x04E6,

        /// <summary>
        /// You cannot swear allegiance to anyone because you own a monarch-only house. Please abandon your house and try again.
        /// </summary>
        CannotSwearAllegianceWhileOwningMansion = 0x04E7,

        /// <summary>
        /// You can't do that while in the air!
        /// </summary>
        YouCantDoThatWhileInTheAir = 0x04EB,

        /// <summary>
        /// You cannot modify your player killer status while you are recovering from a PK death.
        /// </summary>
        CannotChangePKStatusWhileRecovering = 0x04EC,

        /// <summary>
        /// Advocates may not change their player killer status!
        /// </summary>
        AdvocatesCannotChangePKStatus = 0x04ED,

        /// <summary>
        /// Your level is too low to change your player killer status with this object.
        /// </summary>
        LevelTooLowToChangePKStatusWithObject = 0x04EE,

        /// <summary>
        /// Your level is too high to change your player killer status with this object.
        /// </summary>
        LevelTooHighToChangePKStatusWithObject = 0x04EF,

        /// <summary>
        /// You feel a harsh dissonance, and you sense that an act of killing you have committed recently is interfering with the conversion.
        /// </summary>
        YouFeelAHarshDissonance = 0x04F0,

        /// <summary>
        /// Bael'Zharon's power flows through you again. You are once more a player killer.
        /// </summary>
        YouArePKAgain = 0x04F1,

        /// <summary>
        /// Bael'Zharon has granted you respite after your moment of weakness. You are temporarily no longer a player killer.
        /// </summary>
        YouAreTemporarilyNoLongerPK = 0x04F2,

        /// <summary>
        /// Lite Player Killers may not interact with that portal!
        /// </summary>
        PKLiteMayNotUsePortal = 0x04F3,

        /// <summary>
        /// You aren't trained in healing!
        /// </summary>
        YouArentTrainedInHealing = 0x04FC,

        /// <summary>
        /// You don't own that healing kit!
        /// </summary>
        YouDontOwnThatHealingKit = 0x04FD,

        /// <summary>
        /// You can't heal that!
        /// </summary>
        YouCantHealThat = 0x04FE,

        /// <summary>
        /// You aren't ready to heal!
        /// </summary>
        YouArentReadyToHeal = 0x0500,

        /// <summary>
        /// You can only use Healing Kits on player characters.
        /// </summary>
        YouCanOnlyHealPlayers = 0x0501,

        /// <summary>
        /// The Lifestone's magic protects you from the attack!
        /// </summary>
        LifestoneMagicProtectsYou = 0x0502,

        /// <summary>
        /// The portal's residual energy protects you from the attack!
        /// </summary>
        PortalEnergyProtectsYou = 0x0503,

        /// <summary>
        /// You are enveloped in a feeling of warmth as you are brought back into the protection of the Light. You are once again a Non-Player Killer.
        /// </summary>
        YouAreNonPKAgain = 0x0504,

        /// <summary>
        /// You're too close to your sanctuary!
        /// </summary>
        YoureTooCloseToYourSanctuary = 0x0505,

        /// <summary>
        /// You can't do that -- you're trading!
        /// </summary>
        CantDoThatTradeInProgress = 0x0506,

        /// <summary>
        /// Only Non-Player Killers may enter PK Lite. Please see @help pklite for more details about this command.
        /// </summary>
        OnlyNonPKsMayEnterPKLite = 0x0507,

        /// <summary>
        /// A cold wind touches your heart. You are now a Player Killer Lite.
        /// </summary>
        YouAreNowPKLite = 0x0508,

        /// <summary>
        ///  is now an open fellowship; anyone may recruit new members.
        /// </summary>
        IsNowOpenFellowship = 0x050B,

        /// <summary>
        ///  is now a closed fellowship.
        /// </summary>
        IsNowClosedFellowship = 0x050C,

        /// <summary>
        /// You do not belong to a Fellowship.
        /// </summary>
        YouDoNotBelongToAFellowship = 0x050F,

        UsingMaxHooksSilent = 0x0511,

        /// <summary>
        /// You are now using the maximum number of hooks.  You cannot use another hook until you take an item off one of your hooks.
        /// </summary>
        YouAreNowUsingMaxHooks = 0x0512,

        /// <summary>
        /// You are no longer using the maximum number of hooks.  You may again add items to your hooks.
        /// </summary>
        YouAreNoLongerUsingMaxHooks = 0x0513,

        /// <summary>
        /// You are not permitted to use that hook.
        /// </summary>
        YouAreNotPermittedToUseThatHook = 0x0516,

        /// <summary>
        /// This fellowship is locked;  cannot be recruited into the fellowship.
        /// </summary>
        LockedFellowshipCannotRecruit = 0x0518,

        /// <summary>
        /// The fellowship is locked, you were not added to the fellowship.
        /// </summary>
        LockedFellowshipCannotRecruitYou = 0x0519,

        /// <summary>
        /// Only the original owner may use that item's magic.
        /// </summary>
        ActivationNotAllowedNotOwner = 0x051A,

        /// <summary>
        /// Turbine Chat is enabled.
        /// </summary>
        TurbineChatIsEnabled = 0x051D,

        /// <summary>
        /// You cannot add anymore people to the list of players that you can hear.
        /// </summary>
        YouCannotAddPeopleToHearList = 0x0520,

        /// <summary>
        /// You are now deaf to player's screams.
        /// </summary>
        YouAreNowDeafTo_Screams = 0x0523,

        /// <summary>
        /// You can hear all players once again.
        /// </summary>
        YouCanHearAllPlayersOnceAgain = 0x0524,

        /// <summary>
        /// You chicken out.
        /// </summary>
        YouChickenOut = 0x0526,

        /// <summary>
        /// You cannot posssibly succeed.
        /// </summary>
        YouCanPossiblySucceed = 0x0527,

        /// <summary>
        /// The fellowship is locked; you cannot open locked fellowships.
        /// </summary>
        FellowshipIsLocked = 0x0528,

        /// <summary>
        /// Trade Complete!
        /// </summary>
        TradeComplete = 0x0529,

        /// <summary>
        /// That is not a salvaging tool.
        /// </summary>
        NotASalvageTool = 0x052A,

        /// <summary>
        /// That person is not available now.
        /// </summary>
        CharacterNotAvailable = 0x052B,

        /// <summary>
        /// You must wait 30 days after purchasing a house before you may purchase another with any character on the same account. This applies to all housing except apartments.
        /// </summary>
        YouMustWaitToPurchaseHouse = 0x0532,

        /// <summary>
        /// You do not have the authority within your allegiance to do that.
        /// </summary>
        YouDoNotHaveAuthorityInAllegiance = 0x0535,

        /// <summary>
        /// You have the maximum number of accounts banned.!
        /// </summary>
        YouHaveMaxAccountsBanned = 0x0540,

        /// <summary>
        /// You already have the maximum number of allegiance officers. You must remove some before you add any more.
        /// </summary>
        YouHaveMaxAllegianceOfficers = 0x0545,

        /// <summary>
        /// Your allegiance officers have been cleared.
        /// </summary>
        YourAllegianceOfficersHaveBeenCleared = 0x0546,

        /// <summary>
        /// You cannot join any chat channels while gagged.
        /// </summary>
        YouCannotJoinChannelsWhileGagged = 0x0548,

        /// <summary>
        /// You are no longer an allegiance officer.
        /// </summary>
        YouAreNoLongerAllegianceOfficer = 0x054A,

        /// <summary>
        /// Your allegiance does not have a hometown.
        /// </summary>
        YourAllegianceDoesNotHaveHometown = 0x054C,

        /// <summary>
        /// The hook does not contain a usable item. You cannot open the hook because you do not own the house to which it belongs.
        /// </summary>
        HookItemNotUsable_CannotOpen = 0x054E,

        /// <summary>
        /// The hook does not contain a usable item. Use the '@house hooks on'command to make the hook openable.
        /// </summary>
        HookItemNotUsable_CanOpen = 0x054F,

        /// <summary>
        /// Out of Range!
        /// </summary>
        MissileOutOfRange = 0x0550,

        /// <summary>
        /// You must purchase Asheron's Call -- Throne of Destiny to use this function.
        /// </summary>
        MustPurchaseThroneOfDestinyToUseFunction = 0x0552,

        /// <summary>
        /// You must purchase Asheron's Call -- Throne of Destiny to use this item.
        /// </summary>
        MustPurchaseThroneOfDestinyToUseItem = 0x0553,

        /// <summary>
        /// You must purchase Asheron's Call -- Throne of Destiny to use this portal.
        /// </summary>
        MustPurchaseThroneOfDestinyToUsePortal = 0x0554,

        /// <summary>
        /// You must purchase Asheron's Call -- Throne of Destiny to access this quest.
        /// </summary>
        MustPurchaseThroneOfDestinyToAccessQuest = 0x0555,

        /// <summary>
        /// You have failed to complete the augmentation.
        /// </summary>
        YouFailedToCompleteAugmentation = 0x0556,

        /// <summary>
        /// You have used this augmentation too many times already.
        /// </summary>
        AugmentationUsedTooManyTimes = 0x0557,

        /// <summary>
        /// You have used augmentations of this type too many times already.
        /// </summary>
        AugmentationTypeUsedTooManyTimes = 0x0558,

        /// <summary>
        /// You do not have enough unspent experience available to purchase this augmentation.
        /// </summary>
        AugmentationNotEnoughExperience = 0x0559,

        /// <summary>
        /// You must exit the Training Academy before that command will be available to you.
        /// </summary>
        ExitTrainingAcademyToUseCommand = 0x055D,

        /// <summary>
        /// Only Player Killer characters may use this command!
        /// </summary>
        OnlyPKsMayUseCommand = 0x055F,

        /// <summary>
        /// Only Player Killer Lite characters may use this command!
        /// </summary>
        OnlyPKLiteMayUseCommand = 0x0560,

        /// <summary>
        /// You may only have a maximum of 50 friends at once. If you wish to add more friends, you must first remove some.
        /// </summary>
        MaxFriendsExceeded = 0x0561,

        /// <summary>
        /// That character is not on your friends list!
        /// </summary>
        ThatCharacterNotOnYourFriendsList = 0x0563,

        /// <summary>
        /// Only the character who owns the house may use this command.
        /// </summary>
        OnlyHouseOwnerCanUseCommand = 0x0564,

        /// <summary>
        /// That allegiance name is invalid because it is empty. Please use the @allegiance name clear command to clear your allegiance name.
        /// </summary>
        InvalidAllegianceNameCantBeEmpty = 0x0565,

        /// <summary>
        /// That allegiance name is too long. Please choose another name.
        /// </summary>
        InvalidAllegianceNameTooLong = 0x0566,

        /// <summary>
        /// That allegiance name contains illegal characters. Please choose another name using only letters, spaces, - and '.
        /// </summary>
        InvalidAllegianceNameBadCharacters = 0x0567,

        /// <summary>
        /// That allegiance name is not appropriate. Please choose another name.
        /// </summary>
        InvalidAllegianceNameInappropriate = 0x0568,

        /// <summary>
        /// That allegiance name is already in use. Please choose another name.
        /// </summary>
        InvalidAllegianceNameAlreadyInUse = 0x0569,

        /// <summary>
        /// Your allegiance name has been cleared.
        /// </summary>
        AllegianceNameCleared = 0x056B,

        /// <summary>
        /// That is already the name of your allegiance!
        /// </summary>
        InvalidAllegianceNameSameName = 0x056C,

        /// <summary>
        /// That is an invalid officer level.
        /// </summary>
        InvalidOfficerLevel = 0x056F,

        /// <summary>
        /// That allegiance officer title is not appropriate.
        /// </summary>
        AllegianceOfficerTitleIsNotAppropriate = 0x0570,

        /// <summary>
        /// That allegiance name is too long. Please choose another name.
        /// </summary>
        AllegianceNameIsTooLong = 0x0571,

        /// <summary>
        /// All of your allegiance officer titles have been cleared.
        /// </summary>
        AllegianceOfficerTitlesCleared = 0x0572,

        /// <summary>
        /// That allegiance title contains illegal characters. Please choose another name using only letters, spaces, - and '.
        /// </summary>
        AllegianceTitleHasIllegalChars = 0x0573,

        /// <summary>
        /// You have not pre-approved any vassals to join your allegiance.
        /// </summary>
        YouHaveNotPreApprovedVassals = 0x0579,

        /// <summary>
        /// You have cleared the pre-approved vassal for your allegiance.
        /// </summary>
        YouHaveClearedPreApprovedVassal = 0x057C,

        /// <summary>
        /// That character is already gagged!
        /// </summary>
        CharIsAlreadyGagged = 0x057D,

        /// <summary>
        /// That character is not currently gagged!
        /// </summary>
        CharIsNotCurrentlyGagged = 0x057E,

        /// <summary>
        /// Your allegiance chat privileges have been restored.
        /// </summary>
        YourAllegianceChatPrivilegesRestored = 0x0581,

        /// <summary>
        /// You cannot pick up more of that item!
        /// </summary>
        TooManyUniqueItems = 0x0584,

        /// <summary>
        /// You are restricted to clothes and armor created for your race.
        /// </summary>
        HeritageRequiresSpecificArmor = 0x0585,

        /// <summary>
        /// That item was specifically created for another race.
        /// </summary>
        ArmorRequiresSpecificHeritage = 0x0586,

        /// <summary>
        /// Olthoi cannot interact with that!
        /// </summary>
        OlthoiCannotInteractWithThat = 0x0587,

        /// <summary>
        /// Olthoi cannot use regular lifestones! Asheron would not allow it!
        /// </summary>
        OlthoiCannotUseLifestones = 0x0588,

        /// <summary>
        /// The vendor looks at you in horror!
        /// </summary>
        OlthoiVendorLooksInHorror = 0x0589,

        /// <summary>
        /// As a mindless engine of destruction an Olthoi cannot join a fellowship!
        /// </summary>
        OlthoiCannotJoinFellowship = 0x058B,

        /// <summary>
        /// The Olthoi only have an allegiance to the Olthoi Queen!
        /// </summary>
        OlthoiCannotJoinAllegiance = 0x058C,

        /// <summary>
        /// You cannot use that item!
        /// </summary>
        YouCannotUseThatItem = 0x058D,

        /// <summary>
        /// This person will not interact with you!
        /// </summary>
        ThisPersonWillNotInteractWithYou = 0x058E,

        /// <summary>
        /// Only Olthoi may pass through this portal!
        /// </summary>
        OnlyOlthoiMayUsePortal = 0x058F,

        /// <summary>
        /// Olthoi may not pass through this portal!
        /// </summary>
        OlthoiMayNotUsePortal = 0x0590,

        /// <summary>
        /// You may not pass through this portal while Vitae weakens you!
        /// </summary>
        YouMayNotUsePortalWithVitae = 0x0591,

        /// <summary>
        /// This character must be two weeks old or have been created on an account at least two weeks old to use this portal!
        /// </summary>
        YouMustBeTwoWeeksOldToUsePortal = 0x0592,

        /// <summary>
        /// Olthoi characters can only use Lifestone and PK Arena recalls!
        /// </summary>
        OlthoiCanOnlyRecallToLifestone = 0x0593,

        ContractError = 0x0594
    }

    /// <summary>
    /// The PositionFlags value defines the fields present in the Position structure.
    /// </summary>
    [Flags]
    public enum PositionFlags : uint {
        /// <summary>
        /// velocity vector is present
        /// </summary>
        HasVelocity = 0x0001,

        /// <summary>
        /// placement id is present
        /// </summary>
        HasPlacementID = 0x0002,

        /// <summary>
        /// object is grounded
        /// </summary>
        IsGrounded = 0x0004,

        /// <summary>
        /// orientation quaternion has no w component
        /// </summary>
        OrientationHasNoW = 0x0008,

        /// <summary>
        /// orientation quaternion has no x component
        /// </summary>
        OrientationHasNoX = 0x0010,

        /// <summary>
        /// orientation quaternion has no y component
        /// </summary>
        OrientationHasNoY = 0x0020,

        /// <summary>
        /// orientation quaternion has no z component
        /// </summary>
        OrientationHasNoZ = 0x0040,

    }

    /// <summary>
    /// The OptionPropertyID identifies a specific character option.
    /// </summary>
    public enum OptionPropertyID : int {
        /// <summary>
        /// TODO
        /// </summary>
        AutomaticallyRepeatAttacks = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreAllegianceRequests = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreFellowshipRequests = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        ShareFellowshipExperience = 0x0F,

        /// <summary>
        /// TODO
        /// </summary>
        AcceptCorpseLootingPermissions = 0x10,

        /// <summary>
        /// TODO
        /// </summary>
        ShareFellowshipLoot = 0x11,

        /// <summary>
        /// TODO
        /// </summary>
        AutomaticallyAcceptFellowshipRequests = 0x12,

        /// <summary>
        /// TODO
        /// </summary>
        UseChargeAttack = 0x19,

        /// <summary>
        /// TODO
        /// </summary>
        ListenToAllegianceChat = 0x1B,

        /// <summary>
        /// TODO
        /// </summary>
        ListenToGeneralChat = 0x23,

        /// <summary>
        /// TODO
        /// </summary>
        ListenToTradeChat = 0x24,

        /// <summary>
        /// TODO
        /// </summary>
        ListenToLFGChat = 0x25,

        /// <summary>
        /// TODO
        /// </summary>
        ListenToRoleplayingChat = 0x26,

        /// <summary>
        /// TODO
        /// </summary>
        LeadMissleTargets = 0x2A,

        /// <summary>
        /// TODO
        /// </summary>
        UseFastMissles = 0x2B,

    }

    /// <summary>
    /// Height of the attack.  TODO these need to be verified.
    /// </summary>
    public enum AttackHeight : uint {
        /// <summary>
        /// TODO
        /// </summary>
        High = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Medium = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Low = 0x03,

    }

    /// <summary>
    /// Container properties of an item
    /// </summary>
    public enum ContainerProperties : uint {
        /// <summary>
        /// TODO
        /// </summary>
        None = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Container = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Foci = 0x02,

    }

    /// <summary>
    /// The objects type information
    /// </summary>
    [Flags]
    public enum ObjectType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        MeleeWeapon = 0x00000001,

        /// <summary>
        /// TODO
        /// </summary>
        Armor = 0x00000002,

        /// <summary>
        /// TODO
        /// </summary>
        Clothing = 0x00000004,

        /// <summary>
        /// TODO
        /// </summary>
        Jewelry = 0x00000008,

        /// <summary>
        /// TODO
        /// </summary>
        Creature = 0x00000010,

        /// <summary>
        /// TODO
        /// </summary>
        Food = 0x00000020,

        /// <summary>
        /// TODO
        /// </summary>
        Money = 0x00000040,

        /// <summary>
        /// TODO
        /// </summary>
        Misc = 0x00000080,

        /// <summary>
        /// TODO
        /// </summary>
        MissileWeapon = 0x00000100,

        /// <summary>
        /// TODO
        /// </summary>
        Container = 0x00000200,

        /// <summary>
        /// TODO
        /// </summary>
        Useless = 0x00000400,

        /// <summary>
        /// TODO
        /// </summary>
        Gem = 0x00000800,

        /// <summary>
        /// TODO
        /// </summary>
        SpellComponents = 0x00001000,

        /// <summary>
        /// TODO
        /// </summary>
        Writable = 0x00002000,

        /// <summary>
        /// TODO
        /// </summary>
        Key = 0x00004000,

        /// <summary>
        /// TODO
        /// </summary>
        Caster = 0x00008000,

        /// <summary>
        /// TODO
        /// </summary>
        Portal = 0x00010000,

        /// <summary>
        /// TODO
        /// </summary>
        Lockable = 0x00020000,

        /// <summary>
        /// TODO
        /// </summary>
        PromissoryNote = 0x00040000,

        /// <summary>
        /// TODO
        /// </summary>
        ManaStone = 0x00080000,

        /// <summary>
        /// TODO
        /// </summary>
        Service = 0x00100000,

        /// <summary>
        /// TODO
        /// </summary>
        MagicWieldable = 0x00200000,

        /// <summary>
        /// TODO
        /// </summary>
        CraftCookingBase = 0x00400000,

        /// <summary>
        /// TODO
        /// </summary>
        CraftAlchemyBase = 0x00800000,

        /// <summary>
        /// TODO
        /// </summary>
        CraftFletchingBase = 0x02000000,

        /// <summary>
        /// TODO
        /// </summary>
        CraftAlchemyIntermediate = 0x04000000,

        /// <summary>
        /// TODO
        /// </summary>
        CraftFletchingIntermediate = 0x08000000,

        /// <summary>
        /// TODO
        /// </summary>
        LifeStone = 0x10000000,

        /// <summary>
        /// TODO
        /// </summary>
        TinkeringTool = 0x20000000,

        /// <summary>
        /// TODO
        /// </summary>
        TinkeringMaterial = 0x40000000,

        /// <summary>
        /// TODO
        /// </summary>
        Gameboard = 0x80000000,

    }

    /// <summary>
    /// The IntPropertyID identifies a specific Character or Object int property.
    /// </summary>
    public enum IntId : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Undef = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        ObjectType = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        CreatureType = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        PaletteTemplate = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        ClothingPriority = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        EncumbranceVal = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        ItemsCapacity = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        ContainersCapacity = 0x0007,

        /// <summary>
        /// TODO
        /// </summary>
        Mass = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        ValidLocations = 0x0009,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentWieldedLocation = 0x000A,

        /// <summary>
        /// TODO
        /// </summary>
        MaxStackSize = 0x000B,

        /// <summary>
        /// TODO
        /// </summary>
        StackSize = 0x000C,

        /// <summary>
        /// TODO
        /// </summary>
        StackUnitEncumbrance = 0x000D,

        /// <summary>
        /// TODO
        /// </summary>
        StackUnitMass = 0x000E,

        /// <summary>
        /// TODO
        /// </summary>
        StackUnitValue = 0x000F,

        /// <summary>
        /// TODO
        /// </summary>
        ItemUseable = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        RareId = 0x0011,

        /// <summary>
        /// TODO
        /// </summary>
        UiEffects = 0x0012,

        /// <summary>
        /// TODO
        /// </summary>
        Value = 0x0013,

        /// <summary>
        /// TODO
        /// </summary>
        CoinValue = 0x0014,

        /// <summary>
        /// TODO
        /// </summary>
        TotalExperience = 0x0015,

        /// <summary>
        /// TODO
        /// </summary>
        AvailableCharacter = 0x0016,

        /// <summary>
        /// TODO
        /// </summary>
        TotalSkillCredits = 0x0017,

        /// <summary>
        /// TODO
        /// </summary>
        AvailableSkillCredits = 0x0018,

        /// <summary>
        /// TODO
        /// </summary>
        Level = 0x0019,

        /// <summary>
        /// TODO
        /// </summary>
        AccountRequirements = 0x001A,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorType = 0x001B,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorLevel = 0x001C,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceCpPool = 0x001D,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceRank = 0x001E,

        /// <summary>
        /// TODO
        /// </summary>
        ChannelsAllowed = 0x001F,

        /// <summary>
        /// TODO
        /// </summary>
        ChannelsActive = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        Bonded = 0x0021,

        /// <summary>
        /// TODO
        /// </summary>
        MonarchsRank = 0x0022,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceFollowers = 0x0023,

        /// <summary>
        /// TODO
        /// </summary>
        ResistMagic = 0x0024,

        /// <summary>
        /// TODO
        /// </summary>
        ResistItemAppraisal = 0x0025,

        /// <summary>
        /// TODO
        /// </summary>
        ResistLockpick = 0x0026,

        /// <summary>
        /// TODO
        /// </summary>
        DeprecatedResistRepair = 0x0027,

        /// <summary>
        /// TODO
        /// </summary>
        CombatMode = 0x0028,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentAttackHeight = 0x0029,

        /// <summary>
        /// TODO
        /// </summary>
        CombatCollisions = 0x002A,

        /// <summary>
        /// TODO
        /// </summary>
        NumDeaths = 0x002B,

        /// <summary>
        /// TODO
        /// </summary>
        Damage = 0x002C,

        /// <summary>
        /// TODO
        /// </summary>
        DamageType = 0x002D,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultCombatStyle = 0x002E,

        /// <summary>
        /// TODO
        /// </summary>
        AttackType = 0x002F,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponSkill = 0x0030,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponTime = 0x0031,

        /// <summary>
        /// TODO
        /// </summary>
        AmmoType = 0x0032,

        /// <summary>
        /// TODO
        /// </summary>
        CombatUse = 0x0033,

        /// <summary>
        /// TODO
        /// </summary>
        ParentLocation = 0x0034,

        /// <summary>
        /// TODO
        /// </summary>
        PlacementPosition = 0x0035,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponEncumbrance = 0x0036,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponMass = 0x0037,

        /// <summary>
        /// TODO
        /// </summary>
        ShieldValue = 0x0038,

        /// <summary>
        /// TODO
        /// </summary>
        ShieldEncumbrance = 0x0039,

        /// <summary>
        /// TODO
        /// </summary>
        MissileInventoryLocation = 0x003A,

        /// <summary>
        /// TODO
        /// </summary>
        FullDamageType = 0x003B,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponRange = 0x003C,

        /// <summary>
        /// TODO
        /// </summary>
        AttackersSkill = 0x003D,

        /// <summary>
        /// TODO
        /// </summary>
        DefendersSkill = 0x003E,

        /// <summary>
        /// TODO
        /// </summary>
        AttackersSkillValue = 0x003F,

        /// <summary>
        /// TODO
        /// </summary>
        AttackersClass = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        Placement = 0x0041,

        /// <summary>
        /// TODO
        /// </summary>
        CheckpointStatus = 0x0042,

        /// <summary>
        /// TODO
        /// </summary>
        Tolerance = 0x0043,

        /// <summary>
        /// TODO
        /// </summary>
        TargetingTactic = 0x0044,

        /// <summary>
        /// TODO
        /// </summary>
        CombatTactic = 0x0045,

        /// <summary>
        /// TODO
        /// </summary>
        HomesickTargetingTactic = 0x0046,

        /// <summary>
        /// TODO
        /// </summary>
        NumFollowFailures = 0x0047,

        /// <summary>
        /// TODO
        /// </summary>
        FriendType = 0x0048,

        /// <summary>
        /// TODO
        /// </summary>
        FoeType = 0x0049,

        /// <summary>
        /// TODO
        /// </summary>
        MerchandiseObjectTypes = 0x004A,

        /// <summary>
        /// TODO
        /// </summary>
        MerchandiseMinValue = 0x004B,

        /// <summary>
        /// TODO
        /// </summary>
        MerchandiseMaxValue = 0x004C,

        /// <summary>
        /// TODO
        /// </summary>
        NumItemsSold = 0x004D,

        /// <summary>
        /// TODO
        /// </summary>
        NumItemsBought = 0x004E,

        /// <summary>
        /// TODO
        /// </summary>
        MoneyIncome = 0x004F,

        /// <summary>
        /// TODO
        /// </summary>
        MoneyOutflow = 0x0050,

        /// <summary>
        /// TODO
        /// </summary>
        MaxGeneratedObjects = 0x0051,

        /// <summary>
        /// TODO
        /// </summary>
        InitGeneratedObjects = 0x0052,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationResponse = 0x0053,

        /// <summary>
        /// TODO
        /// </summary>
        OriginalValue = 0x0054,

        /// <summary>
        /// TODO
        /// </summary>
        NumMoveFailures = 0x0055,

        /// <summary>
        /// TODO
        /// </summary>
        MinLevel = 0x0056,

        /// <summary>
        /// TODO
        /// </summary>
        MaxLevel = 0x0057,

        /// <summary>
        /// TODO
        /// </summary>
        LockpickMod = 0x0058,

        /// <summary>
        /// TODO
        /// </summary>
        BoosterEnum = 0x0059,

        /// <summary>
        /// TODO
        /// </summary>
        BoostValue = 0x005A,

        /// <summary>
        /// TODO
        /// </summary>
        MaxStructure = 0x005B,

        /// <summary>
        /// TODO
        /// </summary>
        Structure = 0x005C,

        /// <summary>
        /// TODO
        /// </summary>
        PhysicsState = 0x005D,

        /// <summary>
        /// TODO
        /// </summary>
        TargetType = 0x005E,

        /// <summary>
        /// TODO
        /// </summary>
        RadarBlipColor = 0x005F,

        /// <summary>
        /// TODO
        /// </summary>
        EncumbranceCapacity = 0x0060,

        /// <summary>
        /// TODO
        /// </summary>
        LoginTimestamp = 0x0061,

        /// <summary>
        /// TODO
        /// </summary>
        CreationTimestamp = 0x0062,

        /// <summary>
        /// TODO
        /// </summary>
        PkLevelModifier = 0x0063,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorType = 0x0064,

        /// <summary>
        /// TODO
        /// </summary>
        AiAllowedCombatStyle = 0x0065,

        /// <summary>
        /// TODO
        /// </summary>
        LogoffTimestamp = 0x0066,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorDestructionType = 0x0067,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationCreateClass = 0x0068,

        /// <summary>
        /// TODO
        /// </summary>
        ItemWorkmanship = 0x0069,

        /// <summary>
        /// TODO
        /// </summary>
        ItemSpellcraft = 0x006A,

        /// <summary>
        /// TODO
        /// </summary>
        ItemCurMana = 0x006B,

        /// <summary>
        /// TODO
        /// </summary>
        ItemMaxMana = 0x006C,

        /// <summary>
        /// TODO
        /// </summary>
        ItemDifficulty = 0x006D,

        /// <summary>
        /// TODO
        /// </summary>
        ItemAllegianceRankLimit = 0x006E,

        /// <summary>
        /// TODO
        /// </summary>
        PortalBitmask = 0x006F,

        /// <summary>
        /// TODO
        /// </summary>
        AdvocateLevel = 0x0070,

        /// <summary>
        /// TODO
        /// </summary>
        Gender = 0x0071,

        /// <summary>
        /// TODO
        /// </summary>
        Attuned = 0x0072,

        /// <summary>
        /// TODO
        /// </summary>
        ItemSkillLevelLimit = 0x0073,

        /// <summary>
        /// TODO
        /// </summary>
        GateLogic = 0x0074,

        /// <summary>
        /// TODO
        /// </summary>
        ItemManaCost = 0x0075,

        /// <summary>
        /// TODO
        /// </summary>
        Logoff = 0x0076,

        /// <summary>
        /// TODO
        /// </summary>
        Active = 0x0077,

        /// <summary>
        /// TODO
        /// </summary>
        AttackHeight = 0x0078,

        /// <summary>
        /// TODO
        /// </summary>
        NumAttackFailures = 0x0079,

        /// <summary>
        /// TODO
        /// </summary>
        AiCpThreshold = 0x007A,

        /// <summary>
        /// TODO
        /// </summary>
        AiAdvancementStrategy = 0x007B,

        /// <summary>
        /// TODO
        /// </summary>
        Version = 0x007C,

        /// <summary>
        /// TODO
        /// </summary>
        Age = 0x007D,

        /// <summary>
        /// TODO
        /// </summary>
        VendorHappyMean = 0x007E,

        /// <summary>
        /// TODO
        /// </summary>
        VendorHappyVariance = 0x007F,

        /// <summary>
        /// TODO
        /// </summary>
        CloakStatus = 0x0080,

        /// <summary>
        /// TODO
        /// </summary>
        VitaeCpPool = 0x0081,

        /// <summary>
        /// TODO
        /// </summary>
        NumServicesSold = 0x0082,

        /// <summary>
        /// TODO
        /// </summary>
        MaterialType = 0x0083,

        /// <summary>
        /// TODO
        /// </summary>
        NumAllegianceBreaks = 0x0084,

        /// <summary>
        /// TODO
        /// </summary>
        ShowableOnRadar = 0x0085,

        /// <summary>
        /// TODO
        /// </summary>
        PlayerKillerStatus = 0x0086,

        /// <summary>
        /// TODO
        /// </summary>
        VendorHappyMaxItems = 0x0087,

        /// <summary>
        /// TODO
        /// </summary>
        ScorePageNum = 0x0088,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreConfigNum = 0x0089,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreNumScores = 0x008A,

        /// <summary>
        /// TODO
        /// </summary>
        DeathLevel = 0x008B,

        /// <summary>
        /// TODO
        /// </summary>
        AiOptions = 0x008C,

        /// <summary>
        /// TODO
        /// </summary>
        OpenToEveryone = 0x008D,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorTimeType = 0x008E,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorStartTime = 0x008F,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorEndTime = 0x0090,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorEndDestructionType = 0x0091,

        /// <summary>
        /// TODO
        /// </summary>
        XpOverride = 0x0092,

        /// <summary>
        /// TODO
        /// </summary>
        NumCrashAndTurns = 0x0093,

        /// <summary>
        /// TODO
        /// </summary>
        ComponentWarningThreshold = 0x0094,

        /// <summary>
        /// TODO
        /// </summary>
        HouseStatus = 0x0095,

        /// <summary>
        /// TODO
        /// </summary>
        HookPlacement = 0x0096,

        /// <summary>
        /// TODO
        /// </summary>
        HookType = 0x0097,

        /// <summary>
        /// TODO
        /// </summary>
        HookObjectType = 0x0098,

        /// <summary>
        /// TODO
        /// </summary>
        AiPpThreshold = 0x0099,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorVersion = 0x009A,

        /// <summary>
        /// TODO
        /// </summary>
        HouseType = 0x009B,

        /// <summary>
        /// TODO
        /// </summary>
        PickupEmoteOffset = 0x009C,

        /// <summary>
        /// TODO
        /// </summary>
        WeenieIteration = 0x009D,

        /// <summary>
        /// TODO
        /// </summary>
        WieldRequirements = 0x009E,

        /// <summary>
        /// TODO
        /// </summary>
        WieldSkilltype = 0x009F,

        /// <summary>
        /// TODO
        /// </summary>
        WieldDifficulty = 0x00A0,

        /// <summary>
        /// TODO
        /// </summary>
        HouseMaxHooksUsable = 0x00A1,

        /// <summary>
        /// TODO
        /// </summary>
        HouseCurrentHooksUsable = 0x00A2,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceMinLevel = 0x00A3,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceMaxLevel = 0x00A4,

        /// <summary>
        /// TODO
        /// </summary>
        HouseRelinkHookCount = 0x00A5,

        /// <summary>
        /// TODO
        /// </summary>
        SlayerCreatureType = 0x00A6,

        /// <summary>
        /// TODO
        /// </summary>
        ConfirmationInProgress = 0x00A7,

        /// <summary>
        /// TODO
        /// </summary>
        ConfirmationTypeInProgress = 0x00A8,

        /// <summary>
        /// TODO
        /// </summary>
        TsysMutationData = 0x00A9,

        /// <summary>
        /// TODO
        /// </summary>
        NumItemsInMaterial = 0x00AA,

        /// <summary>
        /// TODO
        /// </summary>
        NumTimesTinkered = 0x00AB,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalLongDescDecoration = 0x00AC,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalLockpickSuccessPercent = 0x00AD,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalPages = 0x00AE,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalMaxPages = 0x00AF,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalItemSkill = 0x00B0,

        /// <summary>
        /// TODO
        /// </summary>
        GemCount = 0x00B1,

        /// <summary>
        /// TODO
        /// </summary>
        GemType = 0x00B2,

        /// <summary>
        /// TODO
        /// </summary>
        ImbuedEffect = 0x00B3,

        /// <summary>
        /// TODO
        /// </summary>
        AttackersRawSkillValue = 0x00B4,

        /// <summary>
        /// TODO
        /// </summary>
        ChessRank = 0x00B5,

        /// <summary>
        /// TODO
        /// </summary>
        ChessTotalGames = 0x00B6,

        /// <summary>
        /// TODO
        /// </summary>
        ChessGamesWon = 0x00B7,

        /// <summary>
        /// TODO
        /// </summary>
        ChessGamesLost = 0x00B8,

        /// <summary>
        /// TODO
        /// </summary>
        TypeOfAlteration = 0x00B9,

        /// <summary>
        /// TODO
        /// </summary>
        SkillToBeAltered = 0x00BA,

        /// <summary>
        /// TODO
        /// </summary>
        SkillAlterationCount = 0x00BB,

        /// <summary>
        /// TODO
        /// </summary>
        HeritageGroup = 0x00BC,

        /// <summary>
        /// TODO
        /// </summary>
        TransferFromAttribute = 0x00BD,

        /// <summary>
        /// TODO
        /// </summary>
        TransferToAttribute = 0x00BE,

        /// <summary>
        /// TODO
        /// </summary>
        AttributeTransferCount = 0x00BF,

        /// <summary>
        /// TODO
        /// </summary>
        FakeFishingSkill = 0x00C0,

        /// <summary>
        /// TODO
        /// </summary>
        NumKeys = 0x00C1,

        /// <summary>
        /// TODO
        /// </summary>
        DeathTimestamp = 0x00C2,

        /// <summary>
        /// TODO
        /// </summary>
        PkTimestamp = 0x00C3,

        /// <summary>
        /// TODO
        /// </summary>
        VictimTimestamp = 0x00C4,

        /// <summary>
        /// TODO
        /// </summary>
        HookGroup = 0x00C5,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceSwearTimestamp = 0x00C6,

        /// <summary>
        /// TODO
        /// </summary>
        HousePurchaseTimestamp = 0x00C7,

        /// <summary>
        /// TODO
        /// </summary>
        RedirectableEquippedArmorCount = 0x00C8,

        /// <summary>
        /// TODO
        /// </summary>
        MeleedefenseImbuedEffectTypeCache = 0x00C9,

        /// <summary>
        /// TODO
        /// </summary>
        MissileDefenseImbuedEffectTypeCache = 0x00CA,

        /// <summary>
        /// TODO
        /// </summary>
        MagicDefenseImbuedEffectTypeCache = 0x00CB,

        /// <summary>
        /// TODO
        /// </summary>
        ElementalDamageBonus = 0x00CC,

        /// <summary>
        /// TODO
        /// </summary>
        ImbueAttempts = 0x00CD,

        /// <summary>
        /// TODO
        /// </summary>
        ImbueSuccesses = 0x00CE,

        /// <summary>
        /// TODO
        /// </summary>
        CreatureKills = 0x00CF,

        /// <summary>
        /// TODO
        /// </summary>
        PlayerKillsPk = 0x00D0,

        /// <summary>
        /// TODO
        /// </summary>
        PlayerKillsPkl = 0x00D1,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierOne = 0x00D2,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierTwo = 0x00D3,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierThree = 0x00D4,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierFour = 0x00D5,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierFive = 0x00D6,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationStat = 0x00D7,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationFamilyStat = 0x00D8,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInnateFamily = 0x00D9,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInnateStrength = 0x00DA,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInnateEndurance = 0x00DB,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInnateCoordination = 0x00DC,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInnateQuickness = 0x00DD,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInnateFocus = 0x00DE,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInnateSelf = 0x00DF,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSpecializeSalvaging = 0x00E0,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSpecializeItemTinkering = 0x00E1,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSpecializeArmorTinkering = 0x00E2,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSpecializeMagicItemTinkering = 0x00E3,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSpecializeWeaponTinkering = 0x00E4,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationExtraPackSlot = 0x00E5,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationIncreasedCarryingCapacity = 0x00E6,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationLessDeathItemLoss = 0x00E7,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSpellsRemainPastDeath = 0x00E8,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationCriticalDefense = 0x00E9,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationBonusXp = 0x00EA,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationBonusSalvage = 0x00EB,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationBonusImbueChance = 0x00EC,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationFasterRegen = 0x00ED,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationIncreasedSpellDuration = 0x00EE,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceFamily = 0x00EF,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceSlash = 0x00F0,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistancePierce = 0x00F1,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceBlunt = 0x00F2,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceAcid = 0x00F3,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceFire = 0x00F4,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceFrost = 0x00F5,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceLightning = 0x00F6,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierOneLogin = 0x00F7,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierTwoLogin = 0x00F8,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierThreeLogin = 0x00F9,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierFourLogin = 0x00FA,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierFiveLogin = 0x00FB,

        /// <summary>
        /// TODO
        /// </summary>
        RaresLoginTimestamp = 0x00FC,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierSix = 0x00FD,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierSeven = 0x00FE,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierSixLogin = 0x00FF,

        /// <summary>
        /// TODO
        /// </summary>
        RaresTierSevenLogin = 0x0100,

        /// <summary>
        /// TODO
        /// </summary>
        ItemAttributeLimit = 0x0101,

        /// <summary>
        /// TODO
        /// </summary>
        ItemAttributeLevelLimit = 0x0102,

        /// <summary>
        /// TODO
        /// </summary>
        ItemAttribute2ndLimit = 0x0103,

        /// <summary>
        /// TODO
        /// </summary>
        ItemAttribute2ndLevelLimit = 0x0104,

        /// <summary>
        /// TODO
        /// </summary>
        CharacterTitleId = 0x0105,

        /// <summary>
        /// TODO
        /// </summary>
        NumCharacterTitles = 0x0106,

        /// <summary>
        /// TODO
        /// </summary>
        ResistanceModifierType = 0x0107,

        /// <summary>
        /// TODO
        /// </summary>
        FreeTinkersBitfield = 0x0108,

        /// <summary>
        /// TODO
        /// </summary>
        EquipmentSetId = 0x0109,

        /// <summary>
        /// TODO
        /// </summary>
        PetClass = 0x010A,

        /// <summary>
        /// TODO
        /// </summary>
        Lifespan = 0x010B,

        /// <summary>
        /// TODO
        /// </summary>
        RemainingLifespan = 0x010C,

        /// <summary>
        /// TODO
        /// </summary>
        UseCreateQuantity = 0x010D,

        /// <summary>
        /// TODO
        /// </summary>
        WieldRequirements2 = 0x010E,

        /// <summary>
        /// TODO
        /// </summary>
        WieldSkilltype2 = 0x010F,

        /// <summary>
        /// TODO
        /// </summary>
        WieldDifficulty2 = 0x0110,

        /// <summary>
        /// TODO
        /// </summary>
        WieldRequirements3 = 0x0111,

        /// <summary>
        /// TODO
        /// </summary>
        WieldSkilltype3 = 0x0112,

        /// <summary>
        /// TODO
        /// </summary>
        WieldDifficulty3 = 0x0113,

        /// <summary>
        /// TODO
        /// </summary>
        WieldRequirements4 = 0x0114,

        /// <summary>
        /// TODO
        /// </summary>
        WieldSkilltype4 = 0x0115,

        /// <summary>
        /// TODO
        /// </summary>
        WieldDifficulty4 = 0x0116,

        /// <summary>
        /// TODO
        /// </summary>
        Unique = 0x0117,

        /// <summary>
        /// TODO
        /// </summary>
        SharedCooldown = 0x0118,

        /// <summary>
        /// TODO
        /// </summary>
        Faction1Bits = 0x0119,

        /// <summary>
        /// TODO
        /// </summary>
        Faction2Bits = 0x011A,

        /// <summary>
        /// TODO
        /// </summary>
        Faction3Bits = 0x011B,

        /// <summary>
        /// TODO
        /// </summary>
        Hatred1Bits = 0x011C,

        /// <summary>
        /// TODO
        /// </summary>
        Hatred2Bits = 0x011D,

        /// <summary>
        /// TODO
        /// </summary>
        Hatred3Bits = 0x011E,

        /// <summary>
        /// TODO
        /// </summary>
        SocietyRankCelhan = 0x011F,

        /// <summary>
        /// TODO
        /// </summary>
        SocietyRankEldweb = 0x0120,

        /// <summary>
        /// TODO
        /// </summary>
        SocietyRankRadblo = 0x0121,

        /// <summary>
        /// TODO
        /// </summary>
        HearLocalSignals = 0x0122,

        /// <summary>
        /// TODO
        /// </summary>
        HearLocalSignalsRadius = 0x0123,

        /// <summary>
        /// TODO
        /// </summary>
        Cleaving = 0x0124,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSpecializeGearcraft = 0x0125,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInfusedCreatureMagic = 0x0126,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInfusedItemMagic = 0x0127,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInfusedLifeMagic = 0x0128,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInfusedWarMagic = 0x0129,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationCriticalExpertise = 0x012A,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationCriticalPower = 0x012B,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSkilledMelee = 0x012C,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSkilledMissile = 0x012D,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationSkilledMagic = 0x012E,

        /// <summary>
        /// TODO
        /// </summary>
        ImbuedEffect2 = 0x012F,

        /// <summary>
        /// TODO
        /// </summary>
        ImbuedEffect3 = 0x0130,

        /// <summary>
        /// TODO
        /// </summary>
        ImbuedEffect4 = 0x0131,

        /// <summary>
        /// TODO
        /// </summary>
        ImbuedEffect5 = 0x0132,

        /// <summary>
        /// TODO
        /// </summary>
        DamageRating = 0x0133,

        /// <summary>
        /// TODO
        /// </summary>
        DamageResistRating = 0x0134,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationDamageBonus = 0x0135,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationDamageReduction = 0x0136,

        /// <summary>
        /// TODO
        /// </summary>
        ImbueStackingBits = 0x0137,

        /// <summary>
        /// TODO
        /// </summary>
        HealOverTime = 0x0138,

        /// <summary>
        /// TODO
        /// </summary>
        CritRating = 0x0139,

        /// <summary>
        /// TODO
        /// </summary>
        CritDamageRating = 0x013A,

        /// <summary>
        /// TODO
        /// </summary>
        CritResistRating = 0x013B,

        /// <summary>
        /// TODO
        /// </summary>
        CritDamageResistRating = 0x013C,

        /// <summary>
        /// TODO
        /// </summary>
        HealingResistRating = 0x013D,

        /// <summary>
        /// TODO
        /// </summary>
        DamageOverTime = 0x013E,

        /// <summary>
        /// TODO
        /// </summary>
        ItemMaxLevel = 0x013F,

        /// <summary>
        /// TODO
        /// </summary>
        ItemXpStyle = 0x0140,

        /// <summary>
        /// TODO
        /// </summary>
        EquipmentSetExtra = 0x0141,

        /// <summary>
        /// TODO
        /// </summary>
        AetheriaBitfield = 0x0142,

        /// <summary>
        /// TODO
        /// </summary>
        HealingBoostRating = 0x0143,

        /// <summary>
        /// TODO
        /// </summary>
        HeritageSpecificArmor = 0x0144,

        /// <summary>
        /// TODO
        /// </summary>
        AlternateRacialSkills = 0x0145,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationJackOfAllTrades = 0x0146,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationResistanceNether = 0x0147,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationInfusedVoidMagic = 0x0148,

        /// <summary>
        /// TODO
        /// </summary>
        WeaknessRating = 0x0149,

        /// <summary>
        /// TODO
        /// </summary>
        NetherOverTime = 0x014A,

        /// <summary>
        /// TODO
        /// </summary>
        NetherResistRating = 0x014B,

        /// <summary>
        /// TODO
        /// </summary>
        LuminanceAward = 0x014C,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugDamageRating = 0x014D,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugDamageReductionRating = 0x014E,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugCritDamageRating = 0x014F,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugCritReductionRating = 0x0150,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugSurgeEffectRating = 0x0151,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugSurgeChanceRating = 0x0152,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugItemManaUsage = 0x0153,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugItemManaGain = 0x0154,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugVitality = 0x0155,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugHealingRating = 0x0156,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugSkilledCraft = 0x0157,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugSkilledSpec = 0x0158,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugNoDestroyCraft = 0x0159,

        /// <summary>
        /// TODO
        /// </summary>
        RestrictInteraction = 0x015A,

        /// <summary>
        /// TODO
        /// </summary>
        OlthoiLootTimestamp = 0x015B,

        /// <summary>
        /// TODO
        /// </summary>
        OlthoiLootStep = 0x015C,

        /// <summary>
        /// TODO
        /// </summary>
        UseCreatesContractId = 0x015D,

        /// <summary>
        /// TODO
        /// </summary>
        DotResistRating = 0x015E,

        /// <summary>
        /// TODO
        /// </summary>
        LifeResistRating = 0x015F,

        /// <summary>
        /// TODO
        /// </summary>
        CloakWeaveProc = 0x0160,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponType = 0x0161,

        /// <summary>
        /// TODO
        /// </summary>
        MeleeMastery = 0x0162,

        /// <summary>
        /// TODO
        /// </summary>
        RangedMastery = 0x0163,

        /// <summary>
        /// TODO
        /// </summary>
        SneakAttackRating = 0x0164,

        /// <summary>
        /// TODO
        /// </summary>
        RecklessnessRating = 0x0165,

        /// <summary>
        /// TODO
        /// </summary>
        DeceptionRating = 0x0166,

        /// <summary>
        /// TODO
        /// </summary>
        CombatPetRange = 0x0167,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponAuraDamage = 0x0168,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponAuraSpeed = 0x0169,

        /// <summary>
        /// TODO
        /// </summary>
        SummoningMastery = 0x016A,

        /// <summary>
        /// TODO
        /// </summary>
        HeartbeatLifespan = 0x016B,

        /// <summary>
        /// TODO
        /// </summary>
        UseLevelRequirement = 0x016C,

        /// <summary>
        /// TODO
        /// </summary>
        LumAugAllSkills = 0x016D,

        /// <summary>
        /// TODO
        /// </summary>
        UseRequiresSkill = 0x016E,

        /// <summary>
        /// TODO
        /// </summary>
        UseRequiresSkillLevel = 0x016F,

        /// <summary>
        /// TODO
        /// </summary>
        UseRequiresSkillSpec = 0x0170,

        /// <summary>
        /// TODO
        /// </summary>
        UseRequiresLevel = 0x0171,

        /// <summary>
        /// TODO
        /// </summary>
        GearDamage = 0x0172,

        /// <summary>
        /// TODO
        /// </summary>
        GearDamageResist = 0x0173,

        /// <summary>
        /// TODO
        /// </summary>
        GearCrit = 0x0174,

        /// <summary>
        /// TODO
        /// </summary>
        GearCritResist = 0x0175,

        /// <summary>
        /// TODO
        /// </summary>
        GearCritDamage = 0x0176,

        /// <summary>
        /// TODO
        /// </summary>
        GearCritDamageResist = 0x0177,

        /// <summary>
        /// TODO
        /// </summary>
        GearHealingBoost = 0x0178,

        /// <summary>
        /// TODO
        /// </summary>
        GearNetherResist = 0x0179,

        /// <summary>
        /// TODO
        /// </summary>
        GearLifeResist = 0x017A,

        /// <summary>
        /// TODO
        /// </summary>
        GearMaxHealth = 0x017B,

        /// <summary>
        /// TODO
        /// </summary>
        Unknown380 = 0x017C,

        /// <summary>
        /// TODO
        /// </summary>
        PKDamageRating = 0x017D,

        /// <summary>
        /// TODO
        /// </summary>
        PKDamageResistRating = 0x017E,

        /// <summary>
        /// TODO
        /// </summary>
        GearPKDamageRating = 0x017F,

        /// <summary>
        /// TODO
        /// </summary>
        GearPKDamageResistRating = 0x0180,

        /// <summary>
        /// TODO
        /// </summary>
        Unknown385 = 0x0181,

        /// <summary>
        /// Overpower chance % for endgame creatures.
        /// </summary>
        Overpower = 0x0182,

        /// <summary>
        /// TODO
        /// </summary>
        OverpowerResist = 0x0183,

        /// <summary>
        /// TODO
        /// </summary>
        GearOverpower = 0x0184,

        /// <summary>
        /// TODO
        /// </summary>
        GearOverpowerResist = 0x0185,

        /// <summary>
        /// TODO
        /// </summary>
        Enlightenment = 0x0186,

    }

    /// <summary>
    /// The Int64PropertyID identifies a specific Character or Object int64 property.
    /// </summary>
    public enum Int64Id : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Undef = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        TotalExperience = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        AvailableExperience = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationCost = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        ItemTotalXp = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        ItemBaseXp = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        AvailableLuminance = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        MaximumLuminance = 0x0007,

        /// <summary>
        /// TODO
        /// </summary>
        InteractionReqs = 0x0008,

    }

    /// <summary>
    /// The BooleanPropertyID identifies a specific Character or Object boolean property.
    /// </summary>
    public enum BoolId : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Undef = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        Stuck = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Open = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        Locked = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        RotProof = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceUpdateRequest = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        AiUsesMana = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        AiUseHumanMagicAnimations = 0x0007,

        /// <summary>
        /// TODO
        /// </summary>
        AllowGive = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentlyAttacking = 0x0009,

        /// <summary>
        /// TODO
        /// </summary>
        AttackerAi = 0x000A,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreCollisions = 0x000B,

        /// <summary>
        /// TODO
        /// </summary>
        ReportCollisions = 0x000C,

        /// <summary>
        /// TODO
        /// </summary>
        Ethereal = 0x000D,

        /// <summary>
        /// TODO
        /// </summary>
        GravityStatus = 0x000E,

        /// <summary>
        /// TODO
        /// </summary>
        LightsStatus = 0x000F,

        /// <summary>
        /// TODO
        /// </summary>
        ScriptedCollision = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        Inelastic = 0x0011,

        /// <summary>
        /// TODO
        /// </summary>
        Visibility = 0x0012,

        /// <summary>
        /// TODO
        /// </summary>
        Attackable = 0x0013,

        /// <summary>
        /// TODO
        /// </summary>
        SafeSpellComponents = 0x0014,

        /// <summary>
        /// TODO
        /// </summary>
        AdvocateState = 0x0015,

        /// <summary>
        /// TODO
        /// </summary>
        Inscribable = 0x0016,

        /// <summary>
        /// TODO
        /// </summary>
        DestroyOnSell = 0x0017,

        /// <summary>
        /// TODO
        /// </summary>
        UiHidden = 0x0018,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreHouseBarriers = 0x0019,

        /// <summary>
        /// TODO
        /// </summary>
        HiddenAdmin = 0x001A,

        /// <summary>
        /// TODO
        /// </summary>
        PkWounder = 0x001B,

        /// <summary>
        /// TODO
        /// </summary>
        PkKiller = 0x001C,

        /// <summary>
        /// TODO
        /// </summary>
        NoCorpse = 0x001D,

        /// <summary>
        /// TODO
        /// </summary>
        UnderLifestoneProtection = 0x001E,

        /// <summary>
        /// TODO
        /// </summary>
        ItemManaUpdatePending = 0x001F,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorStatus = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        ResetMessagePending = 0x0021,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultOpen = 0x0022,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultLocked = 0x0023,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultOn = 0x0024,

        /// <summary>
        /// TODO
        /// </summary>
        OpenForBusiness = 0x0025,

        /// <summary>
        /// TODO
        /// </summary>
        IsFrozen = 0x0026,

        /// <summary>
        /// TODO
        /// </summary>
        DealMagicalItems = 0x0027,

        /// <summary>
        /// TODO
        /// </summary>
        LogoffImDead = 0x0028,

        /// <summary>
        /// TODO
        /// </summary>
        ReportCollisionsAsEnvironment = 0x0029,

        /// <summary>
        /// TODO
        /// </summary>
        AllowEdgeSlide = 0x002A,

        /// <summary>
        /// TODO
        /// </summary>
        AdvocateQuest = 0x002B,

        /// <summary>
        /// TODO
        /// </summary>
        IsAdmin = 0x002C,

        /// <summary>
        /// TODO
        /// </summary>
        IsArch = 0x002D,

        /// <summary>
        /// TODO
        /// </summary>
        IsSentinel = 0x002E,

        /// <summary>
        /// TODO
        /// </summary>
        IsAdvocate = 0x002F,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentlyPoweringUp = 0x0030,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorEnteredWorld = 0x0031,

        /// <summary>
        /// TODO
        /// </summary>
        NeverFailCasting = 0x0032,

        /// <summary>
        /// TODO
        /// </summary>
        VendorService = 0x0033,

        /// <summary>
        /// TODO
        /// </summary>
        AiImmobile = 0x0034,

        /// <summary>
        /// TODO
        /// </summary>
        DamagedByCollisions = 0x0035,

        /// <summary>
        /// TODO
        /// </summary>
        IsDynamic = 0x0036,

        /// <summary>
        /// TODO
        /// </summary>
        IsHot = 0x0037,

        /// <summary>
        /// TODO
        /// </summary>
        IsAffecting = 0x0038,

        /// <summary>
        /// TODO
        /// </summary>
        AffectsAis = 0x0039,

        /// <summary>
        /// TODO
        /// </summary>
        SpellQueueActive = 0x003A,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorDisabled = 0x003B,

        /// <summary>
        /// TODO
        /// </summary>
        IsAcceptingTells = 0x003C,

        /// <summary>
        /// TODO
        /// </summary>
        LoggingChannel = 0x003D,

        /// <summary>
        /// TODO
        /// </summary>
        OpensAnyLock = 0x003E,

        /// <summary>
        /// TODO
        /// </summary>
        UnlimitedUse = 0x003F,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratedTreasureItem = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreMagicResist = 0x0041,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreMagicArmor = 0x0042,

        /// <summary>
        /// TODO
        /// </summary>
        AiAllowTrade = 0x0043,

        /// <summary>
        /// TODO
        /// </summary>
        SpellComponentsRequired = 0x0044,

        /// <summary>
        /// TODO
        /// </summary>
        IsSellable = 0x0045,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreShieldsBySkill = 0x0046,

        /// <summary>
        /// TODO
        /// </summary>
        NoDraw = 0x0047,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationUntargeted = 0x0048,

        /// <summary>
        /// TODO
        /// </summary>
        HouseHasGottenPriorityBootPos = 0x0049,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorAutomaticDestruction = 0x004A,

        /// <summary>
        /// TODO
        /// </summary>
        HouseHooksVisible = 0x004B,

        /// <summary>
        /// TODO
        /// </summary>
        HouseRequiresMonarch = 0x004C,

        /// <summary>
        /// TODO
        /// </summary>
        HouseHooksEnabled = 0x004D,

        /// <summary>
        /// TODO
        /// </summary>
        HouseNotifiedHudOfHookCount = 0x004E,

        /// <summary>
        /// TODO
        /// </summary>
        AiAcceptEverything = 0x004F,

        /// <summary>
        /// TODO
        /// </summary>
        IgnorePortalRestrictions = 0x0050,

        /// <summary>
        /// TODO
        /// </summary>
        RequiresBackpackSlot = 0x0051,

        /// <summary>
        /// TODO
        /// </summary>
        DontTurnOrMoveWhenGiving = 0x0052,

        /// <summary>
        /// TODO
        /// </summary>
        NpcLooksLikeObject = 0x0053,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreCloIcons = 0x0054,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalHasAllowedWielder = 0x0055,

        /// <summary>
        /// TODO
        /// </summary>
        ChestRegenOnClose = 0x0056,

        /// <summary>
        /// TODO
        /// </summary>
        LogoffInMinigame = 0x0057,

        /// <summary>
        /// TODO
        /// </summary>
        PortalShowDestination = 0x0058,

        /// <summary>
        /// TODO
        /// </summary>
        PortalIgnoresPkAttackTimer = 0x0059,

        /// <summary>
        /// TODO
        /// </summary>
        NpcInteractsSilently = 0x005A,

        /// <summary>
        /// TODO
        /// </summary>
        Retained = 0x005B,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreAuthor = 0x005C,

        /// <summary>
        /// TODO
        /// </summary>
        Limbo = 0x005D,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalHasAllowedActivator = 0x005E,

        /// <summary>
        /// TODO
        /// </summary>
        ExistedBeforeAllegianceXpChanges = 0x005F,

        /// <summary>
        /// TODO
        /// </summary>
        IsDeaf = 0x0060,

        /// <summary>
        /// TODO
        /// </summary>
        IsPsr = 0x0061,

        /// <summary>
        /// TODO
        /// </summary>
        Invincible = 0x0062,

        /// <summary>
        /// TODO
        /// </summary>
        Ivoryable = 0x0063,

        /// <summary>
        /// TODO
        /// </summary>
        Dyable = 0x0064,

        /// <summary>
        /// TODO
        /// </summary>
        CanGenerateRare = 0x0065,

        /// <summary>
        /// TODO
        /// </summary>
        CorpseGeneratedRare = 0x0066,

        /// <summary>
        /// TODO
        /// </summary>
        NonProjectileMagicImmune = 0x0067,

        /// <summary>
        /// TODO
        /// </summary>
        ActdReceivedItems = 0x0068,

        /// <summary>
        /// TODO
        /// </summary>
        Unknown105 = 0x0069,

        /// <summary>
        /// TODO
        /// </summary>
        FirstEnterWorldDone = 0x006A,

        /// <summary>
        /// TODO
        /// </summary>
        RecallsDisabled = 0x006B,

        /// <summary>
        /// TODO
        /// </summary>
        RareUsesTimer = 0x006C,

        /// <summary>
        /// TODO
        /// </summary>
        ActdPreorderReceivedItems = 0x006D,

        /// <summary>
        /// TODO
        /// </summary>
        Afk = 0x006E,

        /// <summary>
        /// TODO
        /// </summary>
        IsGagged = 0x006F,

        /// <summary>
        /// TODO
        /// </summary>
        ProcSpellSelfTargeted = 0x0070,

        /// <summary>
        /// TODO
        /// </summary>
        IsAllegianceGagged = 0x0071,

        /// <summary>
        /// TODO
        /// </summary>
        EquipmentSetTriggerPiece = 0x0072,

        /// <summary>
        /// TODO
        /// </summary>
        Uninscribe = 0x0073,

        /// <summary>
        /// TODO
        /// </summary>
        WieldOnUse = 0x0074,

        /// <summary>
        /// TODO
        /// </summary>
        ChestClearedWhenClosed = 0x0075,

        /// <summary>
        /// TODO
        /// </summary>
        NeverAttack = 0x0076,

        /// <summary>
        /// TODO
        /// </summary>
        SuppressGenerateEffect = 0x0077,

        /// <summary>
        /// TODO
        /// </summary>
        TreasureCorpse = 0x0078,

        /// <summary>
        /// TODO
        /// </summary>
        EquipmentSetAddLevel = 0x0079,

        /// <summary>
        /// TODO
        /// </summary>
        BarberActive = 0x007A,

        /// <summary>
        /// TODO
        /// </summary>
        TopLayerPriority = 0x007B,

        /// <summary>
        /// TODO
        /// </summary>
        NoHeldItemShown = 0x007C,

        /// <summary>
        /// TODO
        /// </summary>
        LoginAtLifestone = 0x007D,

        /// <summary>
        /// TODO
        /// </summary>
        OlthoiPk = 0x007E,

        /// <summary>
        /// TODO
        /// </summary>
        Account15Days = 0x007F,

        /// <summary>
        /// TODO
        /// </summary>
        HadNoVitae = 0x0080,

        /// <summary>
        /// TODO
        /// </summary>
        NoOlthoiTalk = 0x0081,

        /// <summary>
        /// TODO
        /// </summary>
        AutowieldLeft = 0x0082,

    }

    /// <summary>
    /// The FloatPropertyID identifies a specific Character or Object float property.
    /// </summary>
    public enum FloatId : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Undef = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        HeartbeatInterval = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        HeartbeatTimestamp = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        HealthRate = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        StaminaRate = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        ManaRate = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        HealthUponResurrection = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        StaminaUponResurrection = 0x0007,

        /// <summary>
        /// TODO
        /// </summary>
        ManaUponResurrection = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        StartTime = 0x0009,

        /// <summary>
        /// TODO
        /// </summary>
        StopTime = 0x000A,

        /// <summary>
        /// TODO
        /// </summary>
        ResetInterval = 0x000B,

        /// <summary>
        /// TODO
        /// </summary>
        Shade = 0x000C,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsSlash = 0x000D,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsPierce = 0x000E,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsBludgeon = 0x000F,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsCold = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsFire = 0x0011,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsAcid = 0x0012,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsElectric = 0x0013,

        /// <summary>
        /// TODO
        /// </summary>
        CombatSpeed = 0x0014,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponLength = 0x0015,

        /// <summary>
        /// TODO
        /// </summary>
        DamageVariance = 0x0016,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentPowerMod = 0x0017,

        /// <summary>
        /// TODO
        /// </summary>
        AccuracyMod = 0x0018,

        /// <summary>
        /// TODO
        /// </summary>
        StrengthMod = 0x0019,

        /// <summary>
        /// TODO
        /// </summary>
        MaximumVelocity = 0x001A,

        /// <summary>
        /// TODO
        /// </summary>
        RotationSpeed = 0x001B,

        /// <summary>
        /// TODO
        /// </summary>
        MotionTimestamp = 0x001C,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponDefense = 0x001D,

        /// <summary>
        /// TODO
        /// </summary>
        WimpyLevel = 0x001E,

        /// <summary>
        /// TODO
        /// </summary>
        VisualAwarenessRange = 0x001F,

        /// <summary>
        /// TODO
        /// </summary>
        AuralAwarenessRange = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        PerceptionLevel = 0x0021,

        /// <summary>
        /// TODO
        /// </summary>
        PowerupTime = 0x0022,

        /// <summary>
        /// TODO
        /// </summary>
        MaxChargeDistance = 0x0023,

        /// <summary>
        /// TODO
        /// </summary>
        ChargeSpeed = 0x0024,

        /// <summary>
        /// TODO
        /// </summary>
        BuyPrice = 0x0025,

        /// <summary>
        /// TODO
        /// </summary>
        SellPrice = 0x0026,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultScale = 0x0027,

        /// <summary>
        /// TODO
        /// </summary>
        LockpickMod = 0x0028,

        /// <summary>
        /// TODO
        /// </summary>
        RegenerationInterval = 0x0029,

        /// <summary>
        /// TODO
        /// </summary>
        RegenerationTimestamp = 0x002A,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorRadius = 0x002B,

        /// <summary>
        /// TODO
        /// </summary>
        TimeToRot = 0x002C,

        /// <summary>
        /// TODO
        /// </summary>
        DeathTimestamp = 0x002D,

        /// <summary>
        /// TODO
        /// </summary>
        PkTimestamp = 0x002E,

        /// <summary>
        /// TODO
        /// </summary>
        VictimTimestamp = 0x002F,

        /// <summary>
        /// TODO
        /// </summary>
        LoginTimestamp = 0x0030,

        /// <summary>
        /// TODO
        /// </summary>
        CreationTimestamp = 0x0031,

        /// <summary>
        /// TODO
        /// </summary>
        MinimumTimeSincePk = 0x0032,

        /// <summary>
        /// TODO
        /// </summary>
        DeprecatedHousekeepingPriority = 0x0033,

        /// <summary>
        /// TODO
        /// </summary>
        AbuseLoggingTimestamp = 0x0034,

        /// <summary>
        /// TODO
        /// </summary>
        LastPortalTeleportTimestamp = 0x0035,

        /// <summary>
        /// TODO
        /// </summary>
        UseRadius = 0x0036,

        /// <summary>
        /// TODO
        /// </summary>
        HomeRadius = 0x0037,

        /// <summary>
        /// TODO
        /// </summary>
        ReleasedTimestamp = 0x0038,

        /// <summary>
        /// TODO
        /// </summary>
        MinHomeRadius = 0x0039,

        /// <summary>
        /// TODO
        /// </summary>
        Facing = 0x003A,

        /// <summary>
        /// TODO
        /// </summary>
        ResetTimestamp = 0x003B,

        /// <summary>
        /// TODO
        /// </summary>
        LogoffTimestamp = 0x003C,

        /// <summary>
        /// TODO
        /// </summary>
        EconRecoveryInterval = 0x003D,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponOffense = 0x003E,

        /// <summary>
        /// TODO
        /// </summary>
        DamageMod = 0x003F,

        /// <summary>
        /// TODO
        /// </summary>
        ResistSlash = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        ResistPierce = 0x0041,

        /// <summary>
        /// TODO
        /// </summary>
        ResistBludgeon = 0x0042,

        /// <summary>
        /// TODO
        /// </summary>
        ResistFire = 0x0043,

        /// <summary>
        /// TODO
        /// </summary>
        ResistCold = 0x0044,

        /// <summary>
        /// TODO
        /// </summary>
        ResistAcid = 0x0045,

        /// <summary>
        /// TODO
        /// </summary>
        ResistElectric = 0x0046,

        /// <summary>
        /// TODO
        /// </summary>
        ResistHealthBoost = 0x0047,

        /// <summary>
        /// TODO
        /// </summary>
        ResistStaminaDrain = 0x0048,

        /// <summary>
        /// TODO
        /// </summary>
        ResistStaminaBoost = 0x0049,

        /// <summary>
        /// TODO
        /// </summary>
        ResistManaDrain = 0x004A,

        /// <summary>
        /// TODO
        /// </summary>
        ResistManaBoost = 0x004B,

        /// <summary>
        /// TODO
        /// </summary>
        Translucency = 0x004C,

        /// <summary>
        /// TODO
        /// </summary>
        PhysicsScriptIntensity = 0x004D,

        /// <summary>
        /// TODO
        /// </summary>
        Friction = 0x004E,

        /// <summary>
        /// TODO
        /// </summary>
        Elasticity = 0x004F,

        /// <summary>
        /// TODO
        /// </summary>
        AiUseMagicDelay = 0x0050,

        /// <summary>
        /// TODO
        /// </summary>
        ItemMinSpellcraftMod = 0x0051,

        /// <summary>
        /// TODO
        /// </summary>
        ItemMaxSpellcraftMod = 0x0052,

        /// <summary>
        /// TODO
        /// </summary>
        ItemRankProbability = 0x0053,

        /// <summary>
        /// TODO
        /// </summary>
        Shade2 = 0x0054,

        /// <summary>
        /// TODO
        /// </summary>
        Shade3 = 0x0055,

        /// <summary>
        /// TODO
        /// </summary>
        Shade4 = 0x0056,

        /// <summary>
        /// TODO
        /// </summary>
        ItemEfficiency = 0x0057,

        /// <summary>
        /// TODO
        /// </summary>
        ItemManaUpdateTimestamp = 0x0058,

        /// <summary>
        /// TODO
        /// </summary>
        SpellGestureSpeedMod = 0x0059,

        /// <summary>
        /// TODO
        /// </summary>
        SpellStanceSpeedMod = 0x005A,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceAppraisalTimestamp = 0x005B,

        /// <summary>
        /// TODO
        /// </summary>
        PowerLevel = 0x005C,

        /// <summary>
        /// TODO
        /// </summary>
        AccuracyLevel = 0x005D,

        /// <summary>
        /// TODO
        /// </summary>
        AttackAngle = 0x005E,

        /// <summary>
        /// TODO
        /// </summary>
        AttackTimestamp = 0x005F,

        /// <summary>
        /// TODO
        /// </summary>
        CheckpointTimestamp = 0x0060,

        /// <summary>
        /// TODO
        /// </summary>
        SoldTimestamp = 0x0061,

        /// <summary>
        /// TODO
        /// </summary>
        UseTimestamp = 0x0062,

        /// <summary>
        /// TODO
        /// </summary>
        UseLockTimestamp = 0x0063,

        /// <summary>
        /// TODO
        /// </summary>
        HealkitMod = 0x0064,

        /// <summary>
        /// TODO
        /// </summary>
        FrozenTimestamp = 0x0065,

        /// <summary>
        /// TODO
        /// </summary>
        HealthRateMod = 0x0066,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceSwearTimestamp = 0x0067,

        /// <summary>
        /// TODO
        /// </summary>
        ObviousRadarRange = 0x0068,

        /// <summary>
        /// TODO
        /// </summary>
        HotspotCycleTime = 0x0069,

        /// <summary>
        /// TODO
        /// </summary>
        HotspotCycleTimeVariance = 0x006A,

        /// <summary>
        /// TODO
        /// </summary>
        SpamTimestamp = 0x006B,

        /// <summary>
        /// TODO
        /// </summary>
        SpamRate = 0x006C,

        /// <summary>
        /// TODO
        /// </summary>
        BondWieldedTreasure = 0x006D,

        /// <summary>
        /// TODO
        /// </summary>
        BulkMod = 0x006E,

        /// <summary>
        /// TODO
        /// </summary>
        SizeMod = 0x006F,

        /// <summary>
        /// TODO
        /// </summary>
        GagTimestamp = 0x0070,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorUpdateTimestamp = 0x0071,

        /// <summary>
        /// TODO
        /// </summary>
        DeathSpamTimestamp = 0x0072,

        /// <summary>
        /// TODO
        /// </summary>
        DeathSpamRate = 0x0073,

        /// <summary>
        /// TODO
        /// </summary>
        WildAttackProbability = 0x0074,

        /// <summary>
        /// TODO
        /// </summary>
        FocusedProbability = 0x0075,

        /// <summary>
        /// TODO
        /// </summary>
        CrashAndTurnProbability = 0x0076,

        /// <summary>
        /// TODO
        /// </summary>
        CrashAndTurnRadius = 0x0077,

        /// <summary>
        /// TODO
        /// </summary>
        CrashAndTurnBias = 0x0078,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorInitialDelay = 0x0079,

        /// <summary>
        /// TODO
        /// </summary>
        AiAcquireHealth = 0x007A,

        /// <summary>
        /// TODO
        /// </summary>
        AiAcquireStamina = 0x007B,

        /// <summary>
        /// TODO
        /// </summary>
        AiAcquireMana = 0x007C,

        /// <summary>
        /// TODO
        /// </summary>
        ResistHealthDrain = 0x007D,

        /// <summary>
        /// TODO
        /// </summary>
        LifestoneProtectionTimestamp = 0x007E,

        /// <summary>
        /// TODO
        /// </summary>
        AiCounteractEnchantment = 0x007F,

        /// <summary>
        /// TODO
        /// </summary>
        AiDispelEnchantment = 0x0080,

        /// <summary>
        /// TODO
        /// </summary>
        TradeTimestamp = 0x0081,

        /// <summary>
        /// TODO
        /// </summary>
        AiTargetedDetectionRadius = 0x0082,

        /// <summary>
        /// TODO
        /// </summary>
        EmotePriority = 0x0083,

        /// <summary>
        /// TODO
        /// </summary>
        LastTeleportStartTimestamp = 0x0084,

        /// <summary>
        /// TODO
        /// </summary>
        EventSpamTimestamp = 0x0085,

        /// <summary>
        /// TODO
        /// </summary>
        EventSpamRate = 0x0086,

        /// <summary>
        /// TODO
        /// </summary>
        InventoryOffset = 0x0087,

        /// <summary>
        /// TODO
        /// </summary>
        CriticalMultiplier = 0x0088,

        /// <summary>
        /// TODO
        /// </summary>
        ManaStoneDestroyChance = 0x0089,

        /// <summary>
        /// TODO
        /// </summary>
        SlayerDamageBonus = 0x008A,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceInfoSpamTimestamp = 0x008B,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceInfoSpamRate = 0x008C,

        /// <summary>
        /// TODO
        /// </summary>
        NextSpellcastTimestamp = 0x008D,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalRequestedTimestamp = 0x008E,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalHeartbeatDueTimestamp = 0x008F,

        /// <summary>
        /// TODO
        /// </summary>
        ManaConversionMod = 0x0090,

        /// <summary>
        /// TODO
        /// </summary>
        LastPkAttackTimestamp = 0x0091,

        /// <summary>
        /// TODO
        /// </summary>
        FellowshipUpdateTimestamp = 0x0092,

        /// <summary>
        /// TODO
        /// </summary>
        CriticalFrequency = 0x0093,

        /// <summary>
        /// TODO
        /// </summary>
        LimboStartTimestamp = 0x0094,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponMissileDefense = 0x0095,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponMagicDefense = 0x0096,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreShield = 0x0097,

        /// <summary>
        /// TODO
        /// </summary>
        ElementalDamageMod = 0x0098,

        /// <summary>
        /// TODO
        /// </summary>
        StartMissileAttackTimestamp = 0x0099,

        /// <summary>
        /// TODO
        /// </summary>
        LastRareUsedTimestamp = 0x009A,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreArmor = 0x009B,

        /// <summary>
        /// TODO
        /// </summary>
        ProcSpellRate = 0x009C,

        /// <summary>
        /// TODO
        /// </summary>
        ResistanceModifier = 0x009D,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceGagTimestamp = 0x009E,

        /// <summary>
        /// TODO
        /// </summary>
        AbsorbMagicDamage = 0x009F,

        /// <summary>
        /// TODO
        /// </summary>
        CachedMaxAbsorbMagicDamage = 0x00A0,

        /// <summary>
        /// TODO
        /// </summary>
        GagDuration = 0x00A1,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceGagDuration = 0x00A2,

        /// <summary>
        /// TODO
        /// </summary>
        GlobalXpMod = 0x00A3,

        /// <summary>
        /// TODO
        /// </summary>
        HealingModifier = 0x00A4,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorModVsNether = 0x00A5,

        /// <summary>
        /// TODO
        /// </summary>
        ResistNether = 0x00A6,

        /// <summary>
        /// TODO
        /// </summary>
        CooldownDuration = 0x00A7,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponAuraOffense = 0x00A8,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponAuraDefense = 0x00A9,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponAuraElemental = 0x00AA,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponAuraManaConv = 0x00AB,

    }

    /// <summary>
    /// The StringPropertyID identifies a specific Character or Object string property.
    /// </summary>
    public enum StringId : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Undef = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        Name = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Title = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        Sex = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        HeritageGroup = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        Template = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        AttackersName = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        Inscription = 0x0007,

        /// <summary>
        /// TODO
        /// </summary>
        ScribeName = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        VendorsName = 0x0009,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship = 0x000A,

        /// <summary>
        /// TODO
        /// </summary>
        MonarchsName = 0x000B,

        /// <summary>
        /// TODO
        /// </summary>
        LockCode = 0x000C,

        /// <summary>
        /// TODO
        /// </summary>
        KeyCode = 0x000D,

        /// <summary>
        /// TODO
        /// </summary>
        Use = 0x000E,

        /// <summary>
        /// TODO
        /// </summary>
        ShortDesc = 0x000F,

        /// <summary>
        /// TODO
        /// </summary>
        LongDesc = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationTalk = 0x0011,

        /// <summary>
        /// TODO
        /// </summary>
        UseMessage = 0x0012,

        /// <summary>
        /// TODO
        /// </summary>
        ItemHeritageGroupRestriction = 0x0013,

        /// <summary>
        /// TODO
        /// </summary>
        PluralName = 0x0014,

        /// <summary>
        /// TODO
        /// </summary>
        MonarchsTitle = 0x0015,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationFailure = 0x0016,

        /// <summary>
        /// TODO
        /// </summary>
        ScribeAccount = 0x0017,

        /// <summary>
        /// TODO
        /// </summary>
        TownName = 0x0018,

        /// <summary>
        /// TODO
        /// </summary>
        CraftsmanName = 0x0019,

        /// <summary>
        /// TODO
        /// </summary>
        UsePkServerError = 0x001A,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreCachedText = 0x001B,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreDefaultEntryFormat = 0x001C,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreFirstEntryFormat = 0x001D,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreLastEntryFormat = 0x001E,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreOnlyEntryFormat = 0x001F,

        /// <summary>
        /// TODO
        /// </summary>
        ScoreNoEntry = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        Quest = 0x0021,

        /// <summary>
        /// TODO
        /// </summary>
        GeneratorEvent = 0x0022,

        /// <summary>
        /// TODO
        /// </summary>
        PatronsTitle = 0x0023,

        /// <summary>
        /// TODO
        /// </summary>
        HouseOwnerName = 0x0024,

        /// <summary>
        /// TODO
        /// </summary>
        QuestRestriction = 0x0025,

        /// <summary>
        /// TODO
        /// </summary>
        AppraisalPortalDestination = 0x0026,

        /// <summary>
        /// TODO
        /// </summary>
        TinkerName = 0x0027,

        /// <summary>
        /// TODO
        /// </summary>
        ImbuerName = 0x0028,

        /// <summary>
        /// TODO
        /// </summary>
        HouseOwnerAccount = 0x0029,

        /// <summary>
        /// TODO
        /// </summary>
        DisplayName = 0x002A,

        /// <summary>
        /// TODO
        /// </summary>
        DateOfBirth = 0x002B,

        /// <summary>
        /// TODO
        /// </summary>
        ThirdPartyApi = 0x002C,

        /// <summary>
        /// TODO
        /// </summary>
        KillQuest = 0x002D,

        /// <summary>
        /// TODO
        /// </summary>
        Afk = 0x002E,

        /// <summary>
        /// TODO
        /// </summary>
        AllegianceName = 0x002F,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationAddQuest = 0x0030,

        /// <summary>
        /// TODO
        /// </summary>
        KillQuest2 = 0x0031,

        /// <summary>
        /// TODO
        /// </summary>
        KillQuest3 = 0x0032,

        /// <summary>
        /// TODO
        /// </summary>
        UseSendsSignal = 0x0033,

        /// <summary>
        /// TODO
        /// </summary>
        GearPlatingName = 0x0034,

    }

    /// <summary>
    /// The DataPropertyId identifies a specific Character or Object data property.
    /// </summary>
    public enum DataId : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Undef = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        Setup = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        MotionTable = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        SoundTable = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        CombatTable = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        QualityFilter = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        PaletteBase = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        ClothingBase = 0x0007,

        /// <summary>
        /// TODO
        /// </summary>
        Icon = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        EyesTexture = 0x0009,

        /// <summary>
        /// TODO
        /// </summary>
        NoseTexture = 0x000A,

        /// <summary>
        /// TODO
        /// </summary>
        MouthTexture = 0x000B,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultEyesTexture = 0x000C,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultNoseTexture = 0x000D,

        /// <summary>
        /// TODO
        /// </summary>
        DefaultMouthTexture = 0x000E,

        /// <summary>
        /// TODO
        /// </summary>
        HairPalette = 0x000F,

        /// <summary>
        /// TODO
        /// </summary>
        EyesPalette = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        SkinPalette = 0x0011,

        /// <summary>
        /// TODO
        /// </summary>
        HeadObject = 0x0012,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationAnimation = 0x0013,

        /// <summary>
        /// TODO
        /// </summary>
        InitMotion = 0x0014,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationSound = 0x0015,

        /// <summary>
        /// TODO
        /// </summary>
        PhysicsEffectTable = 0x0016,

        /// <summary>
        /// TODO
        /// </summary>
        UseSound = 0x0017,

        /// <summary>
        /// TODO
        /// </summary>
        UseTargetAnimation = 0x0018,

        /// <summary>
        /// TODO
        /// </summary>
        UseTargetSuccessAnimation = 0x0019,

        /// <summary>
        /// TODO
        /// </summary>
        UseTargetFailureAnimation = 0x001A,

        /// <summary>
        /// TODO
        /// </summary>
        UseUserAnimation = 0x001B,

        /// <summary>
        /// TODO
        /// </summary>
        Spell = 0x001C,

        /// <summary>
        /// TODO
        /// </summary>
        SpellComponent = 0x001D,

        /// <summary>
        /// TODO
        /// </summary>
        PhysicsScript = 0x001E,

        /// <summary>
        /// TODO
        /// </summary>
        LinkedPortalOne = 0x001F,

        /// <summary>
        /// TODO
        /// </summary>
        WieldedTreasureType = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        UnknownGuessedname = 0x0021,

        /// <summary>
        /// TODO
        /// </summary>
        UnknownGuessedname2 = 0x0022,

        /// <summary>
        /// TODO
        /// </summary>
        DeathTreasureType = 0x0023,

        /// <summary>
        /// TODO
        /// </summary>
        MutateFilter = 0x0024,

        /// <summary>
        /// TODO
        /// </summary>
        ItemSkillLimit = 0x0025,

        /// <summary>
        /// TODO
        /// </summary>
        UseCreateItem = 0x0026,

        /// <summary>
        /// TODO
        /// </summary>
        DeathSpell = 0x0027,

        /// <summary>
        /// TODO
        /// </summary>
        VendorsClassId = 0x0028,

        /// <summary>
        /// TODO
        /// </summary>
        ItemSpecializedOnly = 0x0029,

        /// <summary>
        /// TODO
        /// </summary>
        HouseId = 0x002A,

        /// <summary>
        /// TODO
        /// </summary>
        AccountHouseId = 0x002B,

        /// <summary>
        /// TODO
        /// </summary>
        RestrictionEffect = 0x002C,

        /// <summary>
        /// TODO
        /// </summary>
        CreationMutationFilter = 0x002D,

        /// <summary>
        /// TODO
        /// </summary>
        TsysMutationFilter = 0x002E,

        /// <summary>
        /// TODO
        /// </summary>
        LastPortal = 0x002F,

        /// <summary>
        /// TODO
        /// </summary>
        LinkedPortalTwo = 0x0030,

        /// <summary>
        /// TODO
        /// </summary>
        OriginalPortal = 0x0031,

        /// <summary>
        /// TODO
        /// </summary>
        IconOverlay = 0x0032,

        /// <summary>
        /// TODO
        /// </summary>
        IconOverlaySecondary = 0x0033,

        /// <summary>
        /// TODO
        /// </summary>
        IconUnderlay = 0x0034,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationMutationFilter = 0x0035,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationEffect = 0x0036,

        /// <summary>
        /// TODO
        /// </summary>
        ProcSpell = 0x0037,

        /// <summary>
        /// TODO
        /// </summary>
        AugmentationCreateItem = 0x0038,

        /// <summary>
        /// TODO
        /// </summary>
        AlternateCurrency = 0x0039,

        /// <summary>
        /// TODO
        /// </summary>
        BlueSurgeSpell = 0x003A,

        /// <summary>
        /// TODO
        /// </summary>
        YellowSurgeSpell = 0x003B,

        /// <summary>
        /// TODO
        /// </summary>
        RedSurgeSpell = 0x003C,

        /// <summary>
        /// TODO
        /// </summary>
        OlthoiDeathTreasureType = 0x003D,

    }

    /// <summary>
    /// The InstancePropertyID identifies a specific Character or Object instance property.
    /// </summary>
    public enum InstanceId : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Undef = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        Owner = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Container = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        Wielder = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        Freezer = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        Viewer = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        Generator = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        Scribe = 0x0007,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentCombatTarget = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentEnemy = 0x0009,

        /// <summary>
        /// TODO
        /// </summary>
        ProjectileLauncher = 0x000A,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentAttacker = 0x000B,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentDamager = 0x000C,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentFollowTarget = 0x000D,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentAppraisalTarget = 0x000E,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentFellowshipAppraisalTarget = 0x000F,

        /// <summary>
        /// TODO
        /// </summary>
        ActivationTarget = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        Creator = 0x0011,

        /// <summary>
        /// TODO
        /// </summary>
        Victim = 0x0012,

        /// <summary>
        /// TODO
        /// </summary>
        Killer = 0x0013,

        /// <summary>
        /// TODO
        /// </summary>
        Vendor = 0x0014,

        /// <summary>
        /// TODO
        /// </summary>
        Customer = 0x0015,

        /// <summary>
        /// TODO
        /// </summary>
        Bonded = 0x0016,

        /// <summary>
        /// TODO
        /// </summary>
        Wounder = 0x0017,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance = 0x0018,

        /// <summary>
        /// TODO
        /// </summary>
        Patron = 0x0019,

        /// <summary>
        /// TODO
        /// </summary>
        Monarch = 0x001A,

        /// <summary>
        /// TODO
        /// </summary>
        CombatTarget = 0x001B,

        /// <summary>
        /// TODO
        /// </summary>
        HealthQueryTarget = 0x001C,

        /// <summary>
        /// TODO
        /// </summary>
        LastUnlocker = 0x001D,

        /// <summary>
        /// TODO
        /// </summary>
        CrashAndTurnTarget = 0x001E,

        /// <summary>
        /// TODO
        /// </summary>
        AllowedActivator = 0x001F,

        /// <summary>
        /// TODO
        /// </summary>
        HouseOwner = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        House = 0x0021,

        /// <summary>
        /// TODO
        /// </summary>
        Slumlord = 0x0022,

        /// <summary>
        /// TODO
        /// </summary>
        ManaQueryTarget = 0x0023,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentGame = 0x0024,

        /// <summary>
        /// TODO
        /// </summary>
        RequestedAppraisalTarget = 0x0025,

        /// <summary>
        /// TODO
        /// </summary>
        AllowedWielder = 0x0026,

        /// <summary>
        /// TODO
        /// </summary>
        AssignedTarget = 0x0027,

        /// <summary>
        /// TODO
        /// </summary>
        LimboSource = 0x0028,

        /// <summary>
        /// TODO
        /// </summary>
        Snooper = 0x0029,

        /// <summary>
        /// TODO
        /// </summary>
        TeleportedCharacter = 0x002A,

        /// <summary>
        /// TODO
        /// </summary>
        Pet = 0x002B,

        /// <summary>
        /// TODO
        /// </summary>
        PetOwner = 0x002C,

        /// <summary>
        /// TODO
        /// </summary>
        PetDevice = 0x002D,

    }

    /// <summary>
    /// The PositionPropertyID identifies a specific Character or Object position property.
    /// </summary>
    public enum PositionPropertyID : uint {
        /// <summary>
        /// I got nothing for you.
        /// </summary>
        Undef = 0,

        /// <summary>
        /// Current Position
        /// </summary>
        Location = 1,

        /// <summary>
        /// May be used to store where we are headed when we teleport (?)
        /// </summary>
        Destination = 2,

        /// <summary>
        /// Where will we pop into the world (?)
        /// </summary>
        Instantiation = 3,

        /// <summary>
        ///  Last Lifestone Used? (@ls)? | @home | @save | @recall
        /// </summary>
        Sanctuary = 4,

        /// <summary>
        /// This is the home, starting, or base position of an object.
        /// It's usually the position the object first spawned in at.
        /// </summary>
        Home = 5,

        /// <summary>
        /// The need to research
        /// </summary>
        ActivationMove = 6,

        /// <summary>
        /// The the position of target.
        /// </summary>
        Target = 7,

        /// <summary>
        /// Primary Portal Recall | Summon Primary Portal | Primary Portal Tie
        /// </summary>
        LinkedPortalOne = 8,

        /// <summary>
        /// Portal Recall (Last Used Portal that can be recalled to)
        /// </summary>
        LastPortal = 9,

        /// <summary>
        /// The portal storm - need research - maybe where you were portaled from or to - to does not seem likely to me.
        /// </summary>
        PortalStorm = 10,

        /// <summary>
        /// The crash and turn - I can't wait to find out.
        /// </summary>
        CrashAndTurn = 11,

        /// <summary>
        /// We are tracking what the portal ties are - could this be the physical location of the portal you summoned?   More research needed.
        /// </summary>
        PortalSummonLoc = 12,

        /// <summary>
        /// That little spot you get sent to just outside the barrier when the slum lord evicts you (??)
        /// </summary>
        HouseBoot = 13,

        /// <summary>
        /// The last outside death. --- boy would I love to extend this to cover deaths in dungeons as well.
        /// </summary>
        LastOutsideDeath = 14, // Location of Corpse

        /// <summary>
        /// The linked lifestone - Lifestone Recall | Lifestone Tie
        /// </summary>
        LinkedLifestone = 15,

        /// <summary>
        /// Secondary Portal Recall | Summon Secondary Portal | Secondary Portal Tie
        /// </summary>
        LinkedPortalTwo = 16,

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save1 = 17, // @save 1 | @home 1 | @recall 1

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save2 = 18, // @save 2 | @home 2 | @recall 2

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save3 = 19, // @save 3 | @home 3 | @recall 3

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save4 = 20, // @save 4 | @home 4 | @recall 4

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save5 = 21, // @save 5 | @home 5 | @recall 5

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save6 = 22, // @save 6 | @home 6 | @recall 6

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save7 = 23, // @save 7 | @home 7 | @recall 7

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save8 = 24, // @save 8 | @home 8 | @recall 8

        /// <summary>
        /// Admin Quick Recall Positions
        /// </summary>
        Save9 = 25, // @save 9 | @home 9 | @recall 9

        /// <summary>
        /// Position data is relative to Location
        /// </summary>
        RelativeDestination = 26,

        /// <summary>
        /// Admin - Position to return player to when using @telereturn which is where a character was at time of admin using @teletome
        /// </summary>
        TeleportedCharacter = 27,

    }

    /// <summary>
    /// The SkillID identifies a specific Character skill.
    /// </summary>
    public enum SkillId : uint {
        Undef = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Axe = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Bow = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Crossbow = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        Dagger = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        Mace = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        MeleeDefense = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        MissileDefense = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        Sling = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        Spear = 0x09,

        /// <summary>
        /// TODO
        /// </summary>
        Staff = 0x0A,

        /// <summary>
        /// TODO
        /// </summary>
        Sword = 0x0B,

        /// <summary>
        /// TODO
        /// </summary>
        ThrownWeapons = 0x0C,

        /// <summary>
        /// TODO
        /// </summary>
        UnarmedCombat = 0x0D,

        /// <summary>
        /// TODO
        /// </summary>
        ArcaneLore = 0x0E,

        /// <summary>
        /// TODO
        /// </summary>
        MagicDefense = 0x0F,

        /// <summary>
        /// TODO
        /// </summary>
        ManaConversion = 0x10,

        /// <summary>
        /// TODO
        /// </summary>
        Spellcraft = 0x11,

        /// <summary>
        /// TODO
        /// </summary>
        ItemTinkering = 0x12,

        /// <summary>
        /// TODO
        /// </summary>
        AssessPerson = 0x13,

        /// <summary>
        /// TODO
        /// </summary>
        Deception = 0x14,

        /// <summary>
        /// TODO
        /// </summary>
        Healing = 0x15,

        /// <summary>
        /// TODO
        /// </summary>
        Jump = 0x16,

        /// <summary>
        /// TODO
        /// </summary>
        Lockpick = 0x17,

        /// <summary>
        /// TODO
        /// </summary>
        Run = 0x18,

        /// <summary>
        /// TODO
        /// </summary>
        Awareness = 0x19,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorRepair = 0x1A,

        /// <summary>
        /// TODO
        /// </summary>
        AssessCreature = 0x1B,

        /// <summary>
        /// TODO
        /// </summary>
        WeaponTinkering = 0x1C,

        /// <summary>
        /// TODO
        /// </summary>
        ArmorTinkering = 0x1D,

        /// <summary>
        /// TODO
        /// </summary>
        MagicItemTinkering = 0x1E,

        /// <summary>
        /// TODO
        /// </summary>
        CreatureEnchantment = 0x1F,

        /// <summary>
        /// TODO
        /// </summary>
        ItemEnchantment = 0x20,

        /// <summary>
        /// TODO
        /// </summary>
        LifeMagic = 0x21,

        /// <summary>
        /// TODO
        /// </summary>
        WarMagic = 0x22,

        /// <summary>
        /// TODO
        /// </summary>
        Leadership = 0x23,

        /// <summary>
        /// TODO
        /// </summary>
        Loyalty = 0x24,

        /// <summary>
        /// TODO
        /// </summary>
        Fletching = 0x25,

        /// <summary>
        /// TODO
        /// </summary>
        Alchemy = 0x26,

        /// <summary>
        /// TODO
        /// </summary>
        Cooking = 0x27,

        /// <summary>
        /// TODO
        /// </summary>
        Salvaging = 0x28,

        /// <summary>
        /// TODO
        /// </summary>
        TwoHandedCombat = 0x29,

        /// <summary>
        /// TODO
        /// </summary>
        Gearcraft = 0x2A,

        /// <summary>
        /// TODO
        /// </summary>
        VoidMagic = 0x2B,

        /// <summary>
        /// TODO
        /// </summary>
        HeavyWeapons = 0x2C,

        /// <summary>
        /// TODO
        /// </summary>
        LightWeapons = 0x2D,

        /// <summary>
        /// TODO
        /// </summary>
        FinesseWeapons = 0x2E,

        /// <summary>
        /// TODO
        /// </summary>
        MissleWeapons = 0x2F,

        /// <summary>
        /// TODO
        /// </summary>
        Shield = 0x30,

        /// <summary>
        /// TODO
        /// </summary>
        DualWield = 0x31,

        /// <summary>
        /// TODO
        /// </summary>
        Recklessness = 0x32,

        /// <summary>
        /// TODO
        /// </summary>
        SneakAttack = 0x33,

        /// <summary>
        /// TODO
        /// </summary>
        DirtyFighting = 0x34,

        /// <summary>
        /// TODO
        /// </summary>
        Challenge = 0x35,

        /// <summary>
        /// TODO
        /// </summary>
        Summoning = 0x36,

    }

    /// <summary>
    /// The SkillState identifies whether a skill is untrained, trained or specialized.
    /// </summary>
    public enum SkillState : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Untrained = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Trained = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Specialized = 0x03,

    }

    /// <summary>
    /// The EmoteType identifies the type of emote action
    /// </summary>
    public enum EmoteType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Invalid_EmoteType = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Invalid_VendorEmoteType = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Act_EmoteType = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        AwardXP_EmoteType = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Give_EmoteType = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        MoveHome_EmoteType = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        Motion_EmoteType = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        Move_EmoteType = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        PhysScript_EmoteType = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        Say_EmoteType = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        Sound_EmoteType = 0x09,

        /// <summary>
        /// TODO
        /// </summary>
        Tell_EmoteType = 0x0A,

        /// <summary>
        /// TODO
        /// </summary>
        Turn_EmoteType = 0x0B,

        /// <summary>
        /// TODO
        /// </summary>
        TurnToTarget_EmoteType = 0x0C,

        /// <summary>
        /// TODO
        /// </summary>
        TextDirect_EmoteType = 0x0D,

        /// <summary>
        /// TODO
        /// </summary>
        CastSpell_EmoteType = 0x0E,

        /// <summary>
        /// TODO
        /// </summary>
        Activate_EmoteType = 0x0F,

        /// <summary>
        /// TODO
        /// </summary>
        WorldBroadcast_EmoteType = 0x10,

        /// <summary>
        /// TODO
        /// </summary>
        LocalBroadcast_EmoteType = 0x11,

        /// <summary>
        /// TODO
        /// </summary>
        DirectBroadcast_EmoteType = 0x12,

        /// <summary>
        /// TODO
        /// </summary>
        CastSpellInstant_EmoteType = 0x13,

        /// <summary>
        /// TODO
        /// </summary>
        UpdateQuest_EmoteType = 0x14,

        /// <summary>
        /// TODO
        /// </summary>
        InqQuest_EmoteType = 0x15,

        /// <summary>
        /// TODO
        /// </summary>
        StampQuest_EmoteType = 0x16,

        /// <summary>
        /// TODO
        /// </summary>
        StartEvent_EmoteType = 0x17,

        /// <summary>
        /// TODO
        /// </summary>
        StopEvent_EmoteType = 0x18,

        /// <summary>
        /// TODO
        /// </summary>
        BLog_EmoteType = 0x19,

        /// <summary>
        /// TODO
        /// </summary>
        AdminSpam_EmoteType = 0x1A,

        /// <summary>
        /// TODO
        /// </summary>
        TeachSpell_EmoteType = 0x1B,

        /// <summary>
        /// TODO
        /// </summary>
        AwardSkillXP_EmoteType = 0x1C,

        /// <summary>
        /// TODO
        /// </summary>
        AwardSkillPoints_EmoteType = 0x1D,

        /// <summary>
        /// TODO
        /// </summary>
        InqQuestSolves_EmoteType = 0x1E,

        /// <summary>
        /// TODO
        /// </summary>
        EraseQuest_EmoteType = 0x1F,

        /// <summary>
        /// TODO
        /// </summary>
        DecrementQuest_EmoteType = 0x20,

        /// <summary>
        /// TODO
        /// </summary>
        IncrementQuest_EmoteType = 0x21,

        /// <summary>
        /// TODO
        /// </summary>
        AddCharacterTitle_EmoteType = 0x22,

        /// <summary>
        /// TODO
        /// </summary>
        InqBoolStat_EmoteType = 0x23,

        /// <summary>
        /// TODO
        /// </summary>
        InqIntStat_EmoteType = 0x24,

        /// <summary>
        /// TODO
        /// </summary>
        InqFloatStat_EmoteType = 0x25,

        /// <summary>
        /// TODO
        /// </summary>
        InqStringStat_EmoteType = 0x26,

        /// <summary>
        /// TODO
        /// </summary>
        InqAttributeStat_EmoteType = 0x27,

        /// <summary>
        /// TODO
        /// </summary>
        InqRawAttributeStat_EmoteType = 0x28,

        /// <summary>
        /// TODO
        /// </summary>
        InqSecondaryAttributeStat_EmoteType = 0x29,

        /// <summary>
        /// TODO
        /// </summary>
        InqRawSecondaryAttributeStat_EmoteType = 0x2A,

        /// <summary>
        /// TODO
        /// </summary>
        InqSkillStat_EmoteType = 0x2B,

        /// <summary>
        /// TODO
        /// </summary>
        InqRawSkillStat_EmoteType = 0x2C,

        /// <summary>
        /// TODO
        /// </summary>
        InqSkillTrained_EmoteType = 0x2D,

        /// <summary>
        /// TODO
        /// </summary>
        InqSkillSpecialized_EmoteType = 0x2E,

        /// <summary>
        /// TODO
        /// </summary>
        AwardTrainingCredits_EmoteType = 0x2F,

        /// <summary>
        /// TODO
        /// </summary>
        InflictVitaePenalty_EmoteType = 0x30,

        /// <summary>
        /// TODO
        /// </summary>
        AwardLevelProportionalXP_EmoteType = 0x31,

        /// <summary>
        /// TODO
        /// </summary>
        AwardLevelProportionalSkillXP_EmoteType = 0x32,

        /// <summary>
        /// TODO
        /// </summary>
        InqEvent_EmoteType = 0x33,

        /// <summary>
        /// TODO
        /// </summary>
        ForceMotion_EmoteType = 0x34,

        /// <summary>
        /// TODO
        /// </summary>
        SetIntStat_EmoteType = 0x35,

        /// <summary>
        /// TODO
        /// </summary>
        IncrementIntStat_EmoteType = 0x36,

        /// <summary>
        /// TODO
        /// </summary>
        DecrementIntStat_EmoteType = 0x37,

        /// <summary>
        /// TODO
        /// </summary>
        CreateTreasure_EmoteType = 0x38,

        /// <summary>
        /// TODO
        /// </summary>
        ResetHomePosition_EmoteType = 0x39,

        /// <summary>
        /// TODO
        /// </summary>
        InqFellowQuest_EmoteType = 0x3A,

        /// <summary>
        /// TODO
        /// </summary>
        InqFellowNum_EmoteType = 0x3B,

        /// <summary>
        /// TODO
        /// </summary>
        UpdateFellowQuest_EmoteType = 0x3C,

        /// <summary>
        /// TODO
        /// </summary>
        StampFellowQuest_EmoteType = 0x3D,

        /// <summary>
        /// TODO
        /// </summary>
        AwardNoShareXP_EmoteType = 0x3E,

        /// <summary>
        /// TODO
        /// </summary>
        SetSanctuaryPosition_EmoteType = 0x3F,

        /// <summary>
        /// TODO
        /// </summary>
        TellFellow_EmoteType = 0x40,

        /// <summary>
        /// TODO
        /// </summary>
        FellowBroadcast_EmoteType = 0x41,

        /// <summary>
        /// TODO
        /// </summary>
        LockFellow_EmoteType = 0x42,

        /// <summary>
        /// TODO
        /// </summary>
        Goto_EmoteType = 0x43,

        /// <summary>
        /// TODO
        /// </summary>
        PopUp_EmoteType = 0x44,

        /// <summary>
        /// TODO
        /// </summary>
        SetBoolStat_EmoteType = 0x45,

        /// <summary>
        /// TODO
        /// </summary>
        SetQuestCompletions_EmoteType = 0x46,

        /// <summary>
        /// TODO
        /// </summary>
        InqNumCharacterTitles_EmoteType = 0x47,

        /// <summary>
        /// TODO
        /// </summary>
        Generate_EmoteType = 0x48,

        /// <summary>
        /// TODO
        /// </summary>
        PetCastSpellOnOwner_EmoteType = 0x49,

        /// <summary>
        /// TODO
        /// </summary>
        TakeItems_EmoteType = 0x4A,

        /// <summary>
        /// TODO
        /// </summary>
        InqYesNo_EmoteType = 0x4B,

        /// <summary>
        /// TODO
        /// </summary>
        InqOwnsItems_EmoteType = 0x4C,

        /// <summary>
        /// TODO
        /// </summary>
        DeleteSelf_EmoteType = 0x4D,

        /// <summary>
        /// TODO
        /// </summary>
        KillSelf_EmoteType = 0x4E,

        /// <summary>
        /// TODO
        /// </summary>
        UpdateMyQuest_EmoteType = 0x4F,

        /// <summary>
        /// TODO
        /// </summary>
        InqMyQuest_EmoteType = 0x50,

        /// <summary>
        /// TODO
        /// </summary>
        StampMyQuest_EmoteType = 0x51,

        /// <summary>
        /// TODO
        /// </summary>
        InqMyQuestSolves_EmoteType = 0x52,

        /// <summary>
        /// TODO
        /// </summary>
        EraseMyQuest_EmoteType = 0x53,

        /// <summary>
        /// TODO
        /// </summary>
        DecrementMyQuest_EmoteType = 0x54,

        /// <summary>
        /// TODO
        /// </summary>
        IncrementMyQuest_EmoteType = 0x55,

        /// <summary>
        /// TODO
        /// </summary>
        SetMyQuestCompletions_EmoteType = 0x56,

        /// <summary>
        /// TODO
        /// </summary>
        MoveToPos_EmoteType = 0x57,

        /// <summary>
        /// TODO
        /// </summary>
        LocalSignal_EmoteType = 0x58,

        /// <summary>
        /// TODO
        /// </summary>
        InqPackSpace_EmoteType = 0x59,

        /// <summary>
        /// TODO
        /// </summary>
        RemoveVitaePenalty_EmoteType = 0x5A,

        /// <summary>
        /// TODO
        /// </summary>
        SetEyeTexture_EmoteType = 0x5B,

        /// <summary>
        /// TODO
        /// </summary>
        SetEyePalette_EmoteType = 0x5C,

        /// <summary>
        /// TODO
        /// </summary>
        SetNoseTexture_EmoteType = 0x5D,

        /// <summary>
        /// TODO
        /// </summary>
        SetNosePalette_EmoteType = 0x5E,

        /// <summary>
        /// TODO
        /// </summary>
        SetMouthTexture_EmoteType = 0x5F,

        /// <summary>
        /// TODO
        /// </summary>
        SetMouthPalette_EmoteType = 0x60,

        /// <summary>
        /// TODO
        /// </summary>
        SetHeadObject_EmoteType = 0x61,

        /// <summary>
        /// TODO
        /// </summary>
        SetHeadPalette_EmoteType = 0x62,

        /// <summary>
        /// TODO
        /// </summary>
        TeleportTarget_EmoteType = 0x63,

        /// <summary>
        /// TODO
        /// </summary>
        TeleportSelf_EmoteType = 0x64,

        /// <summary>
        /// TODO
        /// </summary>
        StartBarber_EmoteType = 0x65,

        /// <summary>
        /// TODO
        /// </summary>
        InqQuestBitsOn_EmoteType = 0x66,

        /// <summary>
        /// TODO
        /// </summary>
        InqQuestBitsOff_EmoteType = 0x67,

        /// <summary>
        /// TODO
        /// </summary>
        InqMyQuestBitsOn_EmoteType = 0x68,

        /// <summary>
        /// TODO
        /// </summary>
        InqMyQuestBitsOff_EmoteType = 0x69,

        /// <summary>
        /// TODO
        /// </summary>
        SetQuestBitsOn_EmoteType = 0x6A,

        /// <summary>
        /// TODO
        /// </summary>
        SetQuestBitsOff_EmoteType = 0x6B,

        /// <summary>
        /// TODO
        /// </summary>
        SetMyQuestBitsOn_EmoteType = 0x6C,

        /// <summary>
        /// TODO
        /// </summary>
        SetMyQuestBitsOff_EmoteType = 0x6D,

        /// <summary>
        /// TODO
        /// </summary>
        UntrainSkill_EmoteType = 0x6E,

        /// <summary>
        /// TODO
        /// </summary>
        SetAltRacialSkills_EmoteType = 0x6F,

        /// <summary>
        /// TODO
        /// </summary>
        SpendLuminance_EmoteType = 0x70,

        /// <summary>
        /// TODO
        /// </summary>
        AwardLuminance_EmoteType = 0x71,

        /// <summary>
        /// TODO
        /// </summary>
        InqInt64Stat_EmoteType = 0x72,

        /// <summary>
        /// TODO
        /// </summary>
        SetInt64Stat_EmoteType = 0x73,

        /// <summary>
        /// TODO
        /// </summary>
        OpenMe_EmoteType = 0x74,

        /// <summary>
        /// TODO
        /// </summary>
        CloseMe_EmoteType = 0x75,

        /// <summary>
        /// TODO
        /// </summary>
        SetFloatStat_EmoteType = 0x76,

        /// <summary>
        /// TODO
        /// </summary>
        AddContract_EmoteType = 0x77,

        /// <summary>
        /// TODO
        /// </summary>
        RemoveContract_EmoteType = 0x78,

        /// <summary>
        /// TODO
        /// </summary>
        InqContractsFull_EmoteType = 0x79,

    }

    /// <summary>
    /// The EmoteCategory identifies the category of an emote.
    /// </summary>
    public enum EmoteCategory : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Invalid_EmoteCategory = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Refuse_EmoteCategory = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Vendor_EmoteCategory = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Death_EmoteCategory = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        Portal_EmoteCategory = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        HeartBeat_EmoteCategory = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        Give_EmoteCategory = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        Use_EmoteCategory = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        Activation_EmoteCategory = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        Generation_EmoteCategory = 0x09,

        /// <summary>
        /// TODO
        /// </summary>
        PickUp_EmoteCategory = 0x0A,

        /// <summary>
        /// TODO
        /// </summary>
        Drop_EmoteCategory = 0x0B,

        /// <summary>
        /// TODO
        /// </summary>
        QuestSuccess_EmoteCategory = 0x0C,

        /// <summary>
        /// TODO
        /// </summary>
        QuestFailure_EmoteCategory = 0x0D,

        /// <summary>
        /// TODO
        /// </summary>
        Taunt_EmoteCategory = 0x0E,

        /// <summary>
        /// TODO
        /// </summary>
        WoundedTaunt_EmoteCategory = 0x0F,

        /// <summary>
        /// TODO
        /// </summary>
        KillTaunt_EmoteCategory = 0x10,

        /// <summary>
        /// TODO
        /// </summary>
        NewEnemy_EmoteCategory = 0x11,

        /// <summary>
        /// TODO
        /// </summary>
        Scream_EmoteCategory = 0x12,

        /// <summary>
        /// TODO
        /// </summary>
        Homesick_EmoteCategory = 0x13,

        /// <summary>
        /// TODO
        /// </summary>
        ReceiveCritical_EmoteCategory = 0x14,

        /// <summary>
        /// TODO
        /// </summary>
        ResistSpell_EmoteCategory = 0x15,

        /// <summary>
        /// TODO
        /// </summary>
        TestSuccess_EmoteCategory = 0x16,

        /// <summary>
        /// TODO
        /// </summary>
        TestFailure_EmoteCategory = 0x17,

        /// <summary>
        /// TODO
        /// </summary>
        HearChat_EmoteCategory = 0x18,

        /// <summary>
        /// TODO
        /// </summary>
        Wield_EmoteCategory = 0x19,

        /// <summary>
        /// TODO
        /// </summary>
        UnWield_EmoteCategory = 0x1A,

        /// <summary>
        /// TODO
        /// </summary>
        EventSuccess_EmoteCategory = 0x1B,

        /// <summary>
        /// TODO
        /// </summary>
        EventFailure_EmoteCategory = 0x1C,

        /// <summary>
        /// TODO
        /// </summary>
        TestNoQuality_EmoteCategory = 0x1D,

        /// <summary>
        /// TODO
        /// </summary>
        QuestNoFellow_EmoteCategory = 0x1E,

        /// <summary>
        /// TODO
        /// </summary>
        TestNoFellow_EmoteCategory = 0x1F,

        /// <summary>
        /// TODO
        /// </summary>
        GotoSet_EmoteCategory = 0x20,

        /// <summary>
        /// TODO
        /// </summary>
        NumFellowsSuccess_EmoteCategory = 0x21,

        /// <summary>
        /// TODO
        /// </summary>
        NumFellowsFailure_EmoteCategory = 0x22,

        /// <summary>
        /// TODO
        /// </summary>
        NumCharacterTitlesSuccess_EmoteCategory = 0x23,

        /// <summary>
        /// TODO
        /// </summary>
        NumCharacterTitlesFailure_EmoteCategory = 0x24,

        /// <summary>
        /// TODO
        /// </summary>
        ReceiveLocalSignal_EmoteCategory = 0x25,

        /// <summary>
        /// TODO
        /// </summary>
        ReceiveTalkDirect_EmoteCategory = 0x26,

    }

    /// <summary>
    /// The CharacterOptions1 word contains character options.
    /// </summary>
    [Flags]
    public enum CharacterOptions1 : uint {
        /// <summary>
        /// unused (was Automatically Create Shortcuts)
        /// </summary>
        u_0x00000001 = 0x00000001,

        /// <summary>
        /// Automatically Repeat Attacks
        /// </summary>
        u_0x00000002 = 0x00000002,

        /// <summary>
        /// Accept Allegiance Requests (Inverted)
        /// </summary>
        u_0x00000004 = 0x00000004,

        /// <summary>
        /// Accept Fellowship Requests (Inverted)
        /// </summary>
        u_0x00000008 = 0x00000008,

        /// <summary>
        /// unused (was Invert Mouse Look Up/Down)
        /// </summary>
        u_0x00000010 = 0x00000010,

        /// <summary>
        /// unused (was Disable House Restriction Effects)
        /// </summary>
        u_0x00000020 = 0x00000020,

        /// <summary>
        /// Let Other Players Give You Items
        /// </summary>
        u_0x00000040 = 0x00000040,

        /// <summary>
        /// Automatically keep combat targets in view
        /// </summary>
        u_0x00000080 = 0x00000080,

        /// <summary>
        /// Display Tooltips
        /// </summary>
        u_0x00000100 = 0x00000100,

        /// <summary>
        /// Attempt to Deceive Other Players
        /// </summary>
        u_0x00000200 = 0x00000200,

        /// <summary>
        /// Run as Default Movement
        /// </summary>
        u_0x00000400 = 0x00000400,

        /// <summary>
        /// Stay in Chat Mode after sending a Message
        /// </summary>
        u_0x00000800 = 0x00000800,

        /// <summary>
        /// Advanced Combat Interface (No Panel)
        /// </summary>
        u_0x00001000 = 0x00001000,

        /// <summary>
        /// Auto Target
        /// </summary>
        u_0x00002000 = 0x00002000,

        /// <summary>
        /// unused (was Right-click mouselook)
        /// </summary>
        u_0x00004000 = 0x00004000,

        /// <summary>
        /// Vivid Targeting Indicator
        /// </summary>
        u_0x00008000 = 0x00008000,

        /// <summary>
        /// unused (was Disable Most Weather Effects)
        /// </summary>
        u_0x00010000 = 0x00010000,

        /// <summary>
        /// Ignore All Trade Requests
        /// </summary>
        u_0x00020000 = 0x00020000,

        /// <summary>
        /// Share Fellowship Experience
        /// </summary>
        u_0x00040000 = 0x00040000,

        /// <summary>
        /// Accept Corpse-Looting Permissions
        /// </summary>
        u_0x00080000 = 0x00080000,

        /// <summary>
        /// Share Fellowship Loot
        /// </summary>
        u_0x00100000 = 0x00100000,

        /// <summary>
        /// Stretch UI
        /// </summary>
        u_0x00200000 = 0x00200000,

        /// <summary>
        /// Show Coordinates Below The Radar
        /// </summary>
        u_0x00400000 = 0x00400000,

        /// <summary>
        /// Display Spell Durations
        /// </summary>
        u_0x00800000 = 0x00800000,

        /// <summary>
        /// unused (was Play Sounds Only When Active Application)
        /// </summary>
        u_0x01000000 = 0x01000000,

        /// <summary>
        /// Disable House Restriction Effects
        /// </summary>
        u_0x02000000 = 0x02000000,

        /// <summary>
        /// Drag Items open secure Trade
        /// </summary>
        u_0x04000000 = 0x04000000,

        /// <summary>
        /// Show Allegiance Logons
        /// </summary>
        u_0x08000000 = 0x08000000,

        /// <summary>
        /// Use Charge Attack
        /// </summary>
        u_0x10000000 = 0x10000000,

        /// <summary>
        /// Automatically Accept Fellowship Requests
        /// </summary>
        u_0x20000000 = 0x20000000,

        /// <summary>
        /// Listen to Allegiance Chat
        /// </summary>
        u_0x40000000 = 0x40000000,

        /// <summary>
        /// Use Crafting Chance of Success Dialog
        /// </summary>
        u_0x80000000 = 0x80000000,

    }

    /// <summary>
    /// The CharacterOptions2 word contains additional character options.
    /// </summary>
    [Flags]
    public enum CharacterOptions2 : uint {
        /// <summary>
        /// Always Daylight Outdoors
        /// </summary>
        u_0x00000001 = 0x00000001,

        /// <summary>
        /// Allow Others to See Your Date of Birth
        /// </summary>
        u_0x00000002 = 0x00000002,

        /// <summary>
        /// Allow Others to See Your Chess Rank
        /// </summary>
        u_0x00000004 = 0x00000004,

        /// <summary>
        /// Allow Others to See Your Fishing Skill
        /// </summary>
        u_0x00000008 = 0x00000008,

        /// <summary>
        /// Allow Others to See Your Number of Deaths
        /// </summary>
        u_0x00000010 = 0x00000010,

        /// <summary>
        /// Allow Others to See Your Age
        /// </summary>
        u_0x00000020 = 0x00000020,

        /// <summary>
        /// Display Timestamps
        /// </summary>
        u_0x00000040 = 0x00000040,

        /// <summary>
        /// Salvage Multiple Materials at Once
        /// </summary>
        u_0x00000080 = 0x00000080,

        /// <summary>
        /// Listen to General Chat
        /// </summary>
        u_0x00000100 = 0x00000100,

        /// <summary>
        /// Listen to Trade Chat
        /// </summary>
        u_0x00000200 = 0x00000200,

        /// <summary>
        /// Listen to LFG Chat
        /// </summary>
        u_0x00000400 = 0x00000400,

        /// <summary>
        /// Listen to Roleplaying Chat
        /// </summary>
        u_0x00000800 = 0x00000800,

        /// <summary>
        /// Allow Others to See Your Number of Titles
        /// </summary>
        u_0x00002000 = 0x00002000,

        /// <summary>
        /// Use Main Pack as Default for Picking Up Items
        /// </summary>
        u_0x00004000 = 0x00004000,

        /// <summary>
        /// Lead Missle Targets
        /// </summary>
        u_0x00008000 = 0x00008000,

        /// <summary>
        /// Use Fast Missles
        /// </summary>
        u_0x00010000 = 0x00010000,

        /// <summary>
        /// Filter Language
        /// </summary>
        u_0x00020000 = 0x00020000,

        /// <summary>
        /// Confirm use of Rare Gems
        /// </summary>
        u_0x00040000 = 0x00040000,

    }

    /// <summary>
    /// The PropertyType value defines the structure and content of a property.
    /// </summary>
    public enum PropertyType : uint {
        /// <summary>
        /// chat window display mask
        /// </summary>
        u_0x1000007F = 0x1000007F,

        /// <summary>
        /// inactive window opacity
        /// </summary>
        u_0x10000080 = 0x10000080,

        /// <summary>
        /// active window opacity
        /// </summary>
        u_0x10000081 = 0x10000081,

        /// <summary>
        /// chat window position (x)
        /// </summary>
        u_0x10000086 = 0x10000086,

        /// <summary>
        /// chat window position (y)
        /// </summary>
        u_0x10000087 = 0x10000087,

        /// <summary>
        /// chat window size (x)
        /// </summary>
        u_0x10000088 = 0x10000088,

        /// <summary>
        /// chat window size (y)
        /// </summary>
        u_0x10000089 = 0x10000089,

        /// <summary>
        /// chat window enabled
        /// </summary>
        u_0x1000008A = 0x1000008A,

        /// <summary>
        /// a window property list
        /// </summary>
        u_0x1000008B = 0x1000008B,

        /// <summary>
        /// a vector of window property lists
        /// </summary>
        u_0x1000008C = 0x1000008C,

        /// <summary>
        /// chat window title
        /// </summary>
        u_0x1000008D = 0x1000008D,

    }

    /// <summary>
    /// The schools of magic
    /// </summary>
    public enum MagicSchool {
        None = 0,
        WarMagic = 1,
        LifeMagic = 2,
        ItemEnchantment = 3,
        CreatureEnchantment = 4,
        VoidMagic = 5,
    }

    /// <summary>
    /// The various options for filtering the spellbook
    /// </summary>
    [Flags]
    public enum SpellBookFilterOptions : uint {
        /// <summary>
        /// TODO
        /// </summary>
        None = 0x00000000,

        /// <summary>
        /// TODO
        /// </summary>
        Creature = 0x00000001,

        /// <summary>
        /// TODO
        /// </summary>
        Item = 0x00000002,

        /// <summary>
        /// TODO
        /// </summary>
        Life = 0x00000004,

        /// <summary>
        /// TODO
        /// </summary>
        War = 0x00000008,

        /// <summary>
        /// TODO
        /// </summary>
        Level1 = 0x00000010,

        /// <summary>
        /// TODO
        /// </summary>
        Level2 = 0x00000020,

        /// <summary>
        /// TODO
        /// </summary>
        Level3 = 0x00000040,

        /// <summary>
        /// TODO
        /// </summary>
        Level4 = 0x00000080,

        /// <summary>
        /// TODO
        /// </summary>
        Level5 = 0x00000100,

        /// <summary>
        /// TODO
        /// </summary>
        Level6 = 0x00000200,

        /// <summary>
        /// TODO
        /// </summary>
        Level7 = 0x00000400,

        /// <summary>
        /// TODO
        /// </summary>
        Level8 = 0x00000800,

        /// <summary>
        /// TODO
        /// </summary>
        Level9 = 0x00001000,

        /// <summary>
        /// TODO
        /// </summary>
        Void = 0x00002000,

    }

    /// <summary>
    /// The EquipMask value describes the equipment slots an item uses.
    /// </summary>
    [Flags]
    public enum EquipMask : uint {
        /// <summary>
        /// None
        /// </summary>
        None = 0x00000000,

        /// <summary>
        /// TODO
        /// </summary>
        Head = 0x00000001,

        /// <summary>
        /// TODO
        /// </summary>
        ChestUnderwear = 0x00000002,

        /// <summary>
        /// TODO
        /// </summary>
        AbdomenUnderwear = 0x00000004,

        /// <summary>
        /// TODO
        /// </summary>
        UpperArmsUnderwear = 0x00000008,

        /// <summary>
        /// TODO
        /// </summary>
        LowerArmsUnderwear = 0x00000010,

        /// <summary>
        /// TODO
        /// </summary>
        Hands = 0x00000020,

        /// <summary>
        /// TODO
        /// </summary>
        UpperLegsUnderwear = 0x00000040,

        /// <summary>
        /// TODO
        /// </summary>
        LowerLegsUnderwear = 0x00000080,

        /// <summary>
        /// TODO
        /// </summary>
        Feet = 0x00000100,

        /// <summary>
        /// TODO
        /// </summary>
        Chest = 0x00000200,

        /// <summary>
        /// TODO
        /// </summary>
        Abdomen = 0x00000400,

        /// <summary>
        /// TODO
        /// </summary>
        UpperArms = 0x00000800,

        /// <summary>
        /// TODO
        /// </summary>
        LowerArms = 0x00001000,

        /// <summary>
        /// TODO
        /// </summary>
        UpperLegs = 0x00002000,

        /// <summary>
        /// TODO
        /// </summary>
        LowerLegs = 0x00004000,

        /// <summary>
        /// TODO
        /// </summary>
        Necklace = 0x00008000,

        /// <summary>
        /// TODO
        /// </summary>
        RightBracelet = 0x00010000,

        /// <summary>
        /// TODO
        /// </summary>
        LeftBracelet = 0x00020000,

        /// <summary>
        /// TODO
        /// </summary>
        RightRing = 0x00040000,

        /// <summary>
        /// TODO
        /// </summary>
        LeftRing = 0x00080000,

        /// <summary>
        /// TODO
        /// </summary>
        MeleeWeapon = 0x00100000,

        /// <summary>
        /// TODO
        /// </summary>
        Shield = 0x00200000,

        /// <summary>
        /// TODO
        /// </summary>
        MissileWeapon = 0x00400000,

        /// <summary>
        /// TODO
        /// </summary>
        Ammunition = 0x00800000,

        /// <summary>
        /// TODO
        /// </summary>
        Wand = 0x01000000,

    }

    /// <summary>
    /// The type of the friend change event.
    /// </summary>
    [Flags]
    public enum FriendsUpdateType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Full = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        Added = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Removed = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        LoginChange = 0x0004,

    }

    /// <summary>
    /// The permission levels that can be given to an allegiance officer
    /// </summary>
    public enum AllegianceOfficerLevel : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Speaker = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Seneschal = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Castellan = 0x03,

    }

    /// <summary>
    /// Actions related to /allegiance lock
    /// </summary>
    public enum AllegianceLockAction : uint {
        /// <summary>
        /// TODO
        /// </summary>
        LockedOff = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        LockedOn = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        ToggleLocked = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        CheckLocked = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        DisplayBypass = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        ClearBypass = 0x06,

    }

    /// <summary>
    /// Actions related to /allegiance house
    /// </summary>
    public enum AllegianceHouseAction : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Help = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        GuestOpen = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        GuestClosed = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        StorageOpen = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        StorageClosed = 0x05,

    }

    /// <summary>
    /// The AttributeID identifies a specific Character attribute.
    /// </summary>
    public enum AttributeId : uint {
        Undef = 0x0,

        /// <summary>
        /// Strength Attribute
        /// </summary>
        Strength = 0x01,

        /// <summary>
        /// Endurance Attribute
        /// </summary>
        Endurance = 0x02,

        /// <summary>
        /// Quickness Attribute
        /// </summary>
        Quickness = 0x03,

        /// <summary>
        /// Coordination Attribute
        /// </summary>
        Coordination = 0x04,

        /// <summary>
        /// Focus Attribute
        /// </summary>
        Focus = 0x05,

        /// <summary>
        /// Self Attribute
        /// </summary>
        Self = 0x06,
    }

    /// <summary>
    /// The VitalID identifies a specific Character vital (secondary attribute).
    /// </summary>
    public enum VitalId : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Health = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Stamina = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        Mana = 0x05,
    }

    /// <summary>
    /// The CurVitalID identifies a specific Character vital (secondary attribute).
    /// </summary>
    public enum CurVitalID : uint {
        /// <summary>
        /// TODO
        /// </summary>
        CurrentHealth = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentStamina = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        CurrentMana = 0x06,

    }
    public enum Vital : uint {
        Undef,
        MaxHealth,
        Health,
        MaxStamina,
        Stamina,
        MaxMana,
        Mana
    }

    /// <summary>
    /// The combat mode for a character or monster.
    /// </summary>
    [Flags]
    public enum CombatMode : uint {
        /// <summary>
        /// TODO
        /// </summary>
        NonCombat = 0x1,

        /// <summary>
        /// TODO
        /// </summary>
        Melee = 0x2,

        /// <summary>
        /// TODO
        /// </summary>
        Missile = 0x4,

        /// <summary>
        /// TODO
        /// </summary>
        Magic = 0x8,

        Invalid = 0x99

    }

    /// <summary>
    /// The ChatMessageType categorizes chat window messages to control color and filtering.
    /// </summary>
    public enum ChatMessageType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Default = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Speech = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Tell = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        OutgoingTell = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        System = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        Combat = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        Magic = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        Channels = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        OutgoingChannel = 0x09,

        /// <summary>
        /// TODO
        /// </summary>
        Social = 0x0A,

        /// <summary>
        /// TODO
        /// </summary>
        OutgoingSocial = 0x0B,

        /// <summary>
        /// TODO
        /// </summary>
        Emote = 0x0C,

        /// <summary>
        /// TODO
        /// </summary>
        Advancement = 0x0D,

        /// <summary>
        /// TODO
        /// </summary>
        Abuse = 0x0E,

        /// <summary>
        /// TODO
        /// </summary>
        Help = 0x0F,

        /// <summary>
        /// TODO
        /// </summary>
        Appraisal = 0x10,

        /// <summary>
        /// TODO
        /// </summary>
        Spellcasting = 0x11,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance = 0x12,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship = 0x13,

        /// <summary>
        /// TODO
        /// </summary>
        WorldBroadcast = 0x14,

        /// <summary>
        /// TODO
        /// </summary>
        CombatEnemy = 0x15,

        /// <summary>
        /// TODO
        /// </summary>
        CombatSelf = 0x16,

        /// <summary>
        /// TODO
        /// </summary>
        Recall = 0x17,

        /// <summary>
        /// TODO
        /// </summary>
        Craft = 0x18,

        /// <summary>
        /// TODO
        /// </summary>
        Salvaging = 0x19,

        /// <summary>
        /// TODO
        /// </summary>
        AdminTell = 0x1F,

    }

    /// <summary>
    /// Flags related to the use of the item.
    /// </summary>
    [Flags]
    public enum ObjectDescriptionFlag : uint {
        /// <summary>
        /// can be opened (false if locked)
        /// </summary>
        Openable = 0x00000001,

        /// <summary>
        /// inscribable
        /// </summary>
        Inscribable = 0x00000002,

        /// <summary>
        /// cannot be picked up
        /// </summary>
        Stuck = 0x00000004,

        /// <summary>
        /// player
        /// </summary>
        Player = 0x00000008,

        /// <summary>
        /// attackable
        /// </summary>
        Attackable = 0x00000010,

        /// <summary>
        /// player killer
        /// </summary>
        PlayerKiller = 0x00000020,

        /// <summary>
        /// hidden admin
        /// </summary>
        HiddenAdmin = 0x00000040,

        /// <summary>
        /// hidden
        /// </summary>
        UiHidden = 0x00000080,

        /// <summary>
        /// book
        /// </summary>
        Book = 0x00000100,

        /// <summary>
        /// merchant
        /// </summary>
        Vendor = 0x00000200,

        /// <summary>
        /// pk altar
        /// </summary>
        PkSwitch = 0x00000400,

        /// <summary>
        /// npk altar
        /// </summary>
        NpkSwitch = 0x00000800,

        /// <summary>
        /// door
        /// </summary>
        Door = 0x00001000,

        /// <summary>
        /// corpse
        /// </summary>
        Corpse = 0x00002000,

        /// <summary>
        /// lifestone
        /// </summary>
        LifeStone = 0x00004000,

        /// <summary>
        /// food
        /// </summary>
        Food = 0x00008000,

        /// <summary>
        /// healing kit
        /// </summary>
        Healer = 0x00010000,

        /// <summary>
        /// lockpick
        /// </summary>
        Lockpick = 0x00020000,

        /// <summary>
        /// portal
        /// </summary>
        Portal = 0x00040000,

        /// <summary>
        /// admin
        /// </summary>
        Admin = 0x00100000,

        /// <summary>
        /// free pk status
        /// </summary>
        FreePkStatus = 0x00200000,

        /// <summary>
        /// immute cell restrictions
        /// </summary>
        ImmuneCellRestrictions = 0x00400000,

        /// <summary>
        /// requires pack slot
        /// </summary>
        RequiresPackSlot = 0x00800000,

        /// <summary>
        /// retained
        /// </summary>
        Retained = 0x01000000,

        /// <summary>
        /// pklite status
        /// </summary>
        PkLiteStatus = 0x02000000,

        /// <summary>
        /// has an extra flags DWORD
        /// </summary>
        IncludesSecondHeader = 0x04000000,

        /// <summary>
        /// bindstone
        /// </summary>
        BindStone = 0x08000000,

        /// <summary>
        /// volatile rare
        /// </summary>
        VolatileRare = 0x10000000,

        /// <summary>
        /// wield on use
        /// </summary>
        WieldOnUse = 0x20000000,

        /// <summary>
        /// wield left
        /// </summary>
        WieldLeft = 0x40000000,

    }

    /// <summary>
    /// The AmmoType value describes the type of ammunition a missile weapon uses.
    /// </summary>
    [Flags]
    public enum AmmoType : ushort {
        /// <summary>
        /// TODO
        /// </summary>
        ThrownWeapon = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        Arrow = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Bolt = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        Dart = 0x0004,

    }

    /// <summary>
    /// The useablilty flags of the object
    /// </summary>
    [Flags]
    public enum UsableType : uint {
        /// <summary>
        /// source not usable
        /// </summary>
        SourceUnusable = 0x00000001,

        /// <summary>
        /// source self
        /// </summary>
        SourceSelf = 0x00000002,

        /// <summary>
        /// source usable while wielded
        /// </summary>
        SourceWielded = 0x00000004,

        /// <summary>
        /// source usable while contained (owned by player)
        /// </summary>
        SourceContained = 0x00000008,

        /// <summary>
        /// source usable while viewed
        /// </summary>
        SourceViewed = 0x00000010,

        /// <summary>
        /// source usable while remote
        /// </summary>
        SourceRemote = 0x00000020,

        /// <summary>
        /// source don&#39;t approach
        /// </summary>
        SourceNoApproach = 0x00000040,

        /// <summary>
        /// source object self
        /// </summary>
        SourceObjectSelf = 0x00000080,

        /// <summary>
        /// target not usable
        /// </summary>
        TargetUnusable = 0x00010000,

        /// <summary>
        /// target self
        /// </summary>
        TargetSelf = 0x00020000,

        /// <summary>
        /// target usable while wielded
        /// </summary>
        TargetWielded = 0x00040000,

        /// <summary>
        /// target usable while contained (owned by player)
        /// </summary>
        TargetContained = 0x00080000,

        /// <summary>
        /// target usable while viewed
        /// </summary>
        TargetViewed = 0x00100000,

        /// <summary>
        /// target usable while remote
        /// </summary>
        TargetRemote = 0x00200000,

        /// <summary>
        /// target don&#39;t approach
        /// </summary>
        TargetNoApproach = 0x00400000,

        /// <summary>
        /// target object self
        /// </summary>
        TargetObjectSelf = 0x00800000,

    }

    /// <summary>
    /// The CoverageMask value describes what parts of the body an item protects.
    /// </summary>
    [Flags]
    public enum CoverageMask : uint {
        /// <summary>
        /// TODO
        /// </summary>
        UpperLegsUnderwear = 0x00000002,

        /// <summary>
        /// TODO
        /// </summary>
        LowerLegsUnderwear = 0x00000004,

        /// <summary>
        /// TODO
        /// </summary>
        ChestUnderwear = 0x00000008,

        /// <summary>
        /// TODO
        /// </summary>
        AbdomenUnderwear = 0x00000010,

        /// <summary>
        /// TODO
        /// </summary>
        UpperArmsUnderwear = 0x00000020,

        /// <summary>
        /// TODO
        /// </summary>
        LowerArmsUnderwear = 0x00000040,

        /// <summary>
        /// TODO
        /// </summary>
        UpperLegs = 0x00000100,

        /// <summary>
        /// TODO
        /// </summary>
        LowerLegs = 0x00000200,

        /// <summary>
        /// TODO
        /// </summary>
        Chest = 0x00000400,

        /// <summary>
        /// TODO
        /// </summary>
        Abdomen = 0x00000800,

        /// <summary>
        /// TODO
        /// </summary>
        UpperArms = 0x00001000,

        /// <summary>
        /// TODO
        /// </summary>
        LowerArms = 0x00002000,

        /// <summary>
        /// TODO
        /// </summary>
        Head = 0x00004000,

        /// <summary>
        /// TODO
        /// </summary>
        Hands = 0x00008000,

        /// <summary>
        /// TODO
        /// </summary>
        Feet = 0x00010000,

    }

    /// <summary>
    /// The HookType identifies the types of dwelling hooks.
    /// </summary>
    [Flags]
    public enum HookType : ushort {
        /// <summary>
        /// TODO
        /// </summary>
        Floor = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Wall = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        Ceiling = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        Yard = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        Roof = 0x0010,

    }

    /// <summary>
    /// The MaterialType identifies the material an object is made of.
    /// </summary>
    public enum MaterialType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Ceramic = 0x00000001,

        /// <summary>
        /// TODO
        /// </summary>
        Porcelain = 0x00000002,

        /// <summary>
        /// TODO
        /// </summary>
        Linen = 0x00000004,

        /// <summary>
        /// TODO
        /// </summary>
        Satin = 0x00000005,

        /// <summary>
        /// TODO
        /// </summary>
        Silk = 0x00000006,

        /// <summary>
        /// TODO
        /// </summary>
        Velvet = 0x00000007,

        /// <summary>
        /// TODO
        /// </summary>
        Wool = 0x00000008,

        /// <summary>
        /// TODO
        /// </summary>
        Agate = 0x0000000A,

        /// <summary>
        /// TODO
        /// </summary>
        Amber = 0x0000000B,

        /// <summary>
        /// TODO
        /// </summary>
        Amethyst = 0x0000000C,

        /// <summary>
        /// TODO
        /// </summary>
        Aquamarine = 0x0000000D,

        /// <summary>
        /// TODO
        /// </summary>
        Azurite = 0x0000000E,

        /// <summary>
        /// TODO
        /// </summary>
        BlackGarnet = 0x0000000F,

        /// <summary>
        /// TODO
        /// </summary>
        BlackOpal = 0x00000010,

        /// <summary>
        /// TODO
        /// </summary>
        Bloodstone = 0x00000011,

        /// <summary>
        /// TODO
        /// </summary>
        Carnelian = 0x00000012,

        /// <summary>
        /// TODO
        /// </summary>
        Citrine = 0x00000013,

        /// <summary>
        /// TODO
        /// </summary>
        Diamond = 0x00000014,

        /// <summary>
        /// TODO
        /// </summary>
        Emerald = 0x00000015,

        /// <summary>
        /// TODO
        /// </summary>
        FireOpal = 0x00000016,

        /// <summary>
        /// TODO
        /// </summary>
        GreenGarnet = 0x00000017,

        /// <summary>
        /// TODO
        /// </summary>
        GreenJade = 0x00000018,

        /// <summary>
        /// TODO
        /// </summary>
        Hematite = 0x00000019,

        /// <summary>
        /// TODO
        /// </summary>
        ImperialTopaz = 0x0000001A,

        /// <summary>
        /// TODO
        /// </summary>
        Jet = 0x0000001B,

        /// <summary>
        /// TODO
        /// </summary>
        LapisLazuli = 0x0000001C,

        /// <summary>
        /// TODO
        /// </summary>
        LavenderJade = 0x0000001D,

        /// <summary>
        /// TODO
        /// </summary>
        Malachite = 0x0000001E,

        /// <summary>
        /// TODO
        /// </summary>
        Moonstone = 0x0000001F,

        /// <summary>
        /// TODO
        /// </summary>
        Onyx = 0x00000020,

        /// <summary>
        /// TODO
        /// </summary>
        Opal = 0x00000021,

        /// <summary>
        /// TODO
        /// </summary>
        Peridot = 0x00000022,

        /// <summary>
        /// TODO
        /// </summary>
        RedGarnet = 0x00000023,

        /// <summary>
        /// TODO
        /// </summary>
        RedJade = 0x00000024,

        /// <summary>
        /// TODO
        /// </summary>
        RoseQuartz = 0x00000025,

        /// <summary>
        /// TODO
        /// </summary>
        Ruby = 0x00000026,

        /// <summary>
        /// TODO
        /// </summary>
        Sapphire = 0x00000027,

        /// <summary>
        /// TODO
        /// </summary>
        SmokeyQuartz = 0x00000028,

        /// <summary>
        /// TODO
        /// </summary>
        Sunstone = 0x00000029,

        /// <summary>
        /// TODO
        /// </summary>
        TigerEye = 0x0000002A,

        /// <summary>
        /// TODO
        /// </summary>
        Tourmaline = 0x0000002B,

        /// <summary>
        /// TODO
        /// </summary>
        Turquoise = 0x0000002C,

        /// <summary>
        /// TODO
        /// </summary>
        WhiteJade = 0x0000002D,

        /// <summary>
        /// TODO
        /// </summary>
        WhiteQuartz = 0x0000002E,

        /// <summary>
        /// TODO
        /// </summary>
        WhiteSapphire = 0x0000002F,

        /// <summary>
        /// TODO
        /// </summary>
        YellowGarnet = 0x00000030,

        /// <summary>
        /// TODO
        /// </summary>
        YellowTopaz = 0x00000031,

        /// <summary>
        /// TODO
        /// </summary>
        Zircon = 0x00000032,

        /// <summary>
        /// TODO
        /// </summary>
        Ivory = 0x00000033,

        /// <summary>
        /// TODO
        /// </summary>
        Leather = 0x00000034,

        /// <summary>
        /// TODO
        /// </summary>
        ArmoredilloHide = 0x00000035,

        /// <summary>
        /// TODO
        /// </summary>
        GromnieHide = 0x00000036,

        /// <summary>
        /// TODO
        /// </summary>
        ReedSharkHide = 0x00000037,

        /// <summary>
        /// TODO
        /// </summary>
        Brass = 0x00000039,

        /// <summary>
        /// TODO
        /// </summary>
        Bronze = 0x0000003A,

        /// <summary>
        /// TODO
        /// </summary>
        Copper = 0x0000003B,

        /// <summary>
        /// TODO
        /// </summary>
        Gold = 0x0000003C,

        /// <summary>
        /// TODO
        /// </summary>
        Iron = 0x0000003D,

        /// <summary>
        /// TODO
        /// </summary>
        Pyreal = 0x0000003E,

        /// <summary>
        /// TODO
        /// </summary>
        Silver = 0x0000003F,

        /// <summary>
        /// TODO
        /// </summary>
        Steel = 0x00000040,

        /// <summary>
        /// TODO
        /// </summary>
        Alabaster = 0x00000042,

        /// <summary>
        /// TODO
        /// </summary>
        Granite = 0x00000043,

        /// <summary>
        /// TODO
        /// </summary>
        Marble = 0x00000044,

        /// <summary>
        /// TODO
        /// </summary>
        Obsidian = 0x00000045,

        /// <summary>
        /// TODO
        /// </summary>
        Sandstone = 0x00000046,

        /// <summary>
        /// TODO
        /// </summary>
        Serpentine = 0x00000047,

        /// <summary>
        /// TODO
        /// </summary>
        Ebony = 0x00000049,

        /// <summary>
        /// TODO
        /// </summary>
        Mahogany = 0x0000004A,

        /// <summary>
        /// TODO
        /// </summary>
        Oak = 0x0000004B,

        /// <summary>
        /// TODO
        /// </summary>
        Pine = 0x0000004C,

        /// <summary>
        /// TODO
        /// </summary>
        Teak = 0x0000004D,

    }

    /// <summary>
    /// The ConfirmationType identifies the specific confirmation panel to be displayed.
    /// </summary>
    public enum ConfirmationType : uint {
        /// <summary>
        /// Swear Allegiance Request
        /// </summary>
        SwearAllegiance = 0x01,

        /// <summary>
        /// Alter Skill Confirmation Request
        /// </summary>
        AlterSkill = 0x02,

        /// <summary>
        /// Alter Attribute Confirmation Request
        /// </summary>
        AlterAttribute = 0x03,

        /// <summary>
        /// Fellowship Request
        /// </summary>
        Fellowship = 0x04,

        /// <summary>
        /// Craft Interaction Confirmation Request
        /// </summary>
        Craft = 0x05,

        /// <summary>
        /// Augmentation Confirmation Request
        /// </summary>
        Augmentation = 0x06,

        /// <summary>
        /// Yes/No Confirmation Request
        /// </summary>
        YesNo = 0x07,

    }

    /// <summary>
    /// The EnvrionChangeType identifies the environment option set.
    /// </summary>
    public enum EnvironChangeType : uint {
        /// <summary>
        /// Removes all overrides
        /// </summary>
        Clear = 0x00,

        /// <summary>
        /// Sets Red Fog
        /// </summary>
        RedFog = 0x01,

        /// <summary>
        /// Sets Blue Fog
        /// </summary>
        BlueFog = 0x02,

        /// <summary>
        /// Sets White Fog
        /// </summary>
        WhiteFog = 0x03,

        /// <summary>
        /// Sets Green Fog
        /// </summary>
        GreenFog = 0x04,

        /// <summary>
        /// Sets Black Fog
        /// </summary>
        BlackFog = 0x05,

        /// <summary>
        /// Sets Black Fog
        /// </summary>
        BlackFog2 = 0x06,

        /// <summary>
        /// Play Roar Sound
        /// </summary>
        RoarSound = 0x65,

        /// <summary>
        /// Play Bell Sound
        /// </summary>
        BellSound = 0x66,

        /// <summary>
        /// Play Chant1 Sound
        /// </summary>
        Chant1Sound = 0x67,

        /// <summary>
        /// Play Chant2 Sound
        /// </summary>
        Chant2Sound = 0x68,

        /// <summary>
        /// Play DarkWhispers1 Sound
        /// </summary>
        DarkWhispers1Sound = 0x69,

        /// <summary>
        /// Play DarkWhispers2 Sound
        /// </summary>
        DarkWhispers2Sound = 0x6A,

        /// <summary>
        /// Play DarkLaugh Sound
        /// </summary>
        DarkLaughSound = 0x6B,

        /// <summary>
        /// Play DarkWind Sound
        /// </summary>
        DarkWindSound = 0x6C,

        /// <summary>
        /// Play DarkSpeech Sound
        /// </summary>
        DarkSpeechSound = 0x6D,

        /// <summary>
        /// Play Drums Sound
        /// </summary>
        DrumsSound = 0x6E,

        /// <summary>
        /// Play GhostSpeak Sound
        /// </summary>
        GhostSpeakSound = 0x6F,

        /// <summary>
        /// Play Breathing Sound
        /// </summary>
        BreathingSound = 0x70,

        /// <summary>
        /// Play Howl Sound
        /// </summary>
        HowlSound = 0x71,

        /// <summary>
        /// Play LostSouls Sound
        /// </summary>
        LostSoulsSound = 0x72,

        /// <summary>
        /// Play Squeal Sound
        /// </summary>
        SquealSound = 0x75,

        /// <summary>
        /// Play Thunder1 Sound
        /// </summary>
        Thunder1Sound = 0x76,

        /// <summary>
        /// Play Thunder2 Sound
        /// </summary>
        Thunder2Sound = 0x77,

        /// <summary>
        /// Play Thunder3 Sound
        /// </summary>
        Thunder3Sound = 0x78,

        /// <summary>
        /// Play Thunder4 Sound
        /// </summary>
        Thunder4Sound = 0x79,

        /// <summary>
        /// Play Thunder5 Sound
        /// </summary>
        Thunder5Sound = 0x7A,

        /// <summary>
        /// Play Thunder6 Sound
        /// </summary>
        Thunder6Sound = 0x7B,

    }

    /// <summary>
    /// The movement type defines the fields for the rest of the message
    /// </summary>
    public enum MovementType : byte {
        /// <summary>
        /// TODO
        /// </summary>
        InterpertedMotionState = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        MoveToObject = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        MoveToPosition = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        TurnToObject = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        TurnToPosition = 0x09,

    }

    /// <summary>
    /// Additional movement options
    /// </summary>
    public enum MovementOption : byte {
        /// <summary>
        /// TODO
        /// </summary>
        None = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        StickToObject = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        StandingLongJump = 0x02,

    }


    /// <summary>
    /// The stance for a character or monster.
    /// </summary>
    public enum MotionStance : uint {
        /// <summary>
        /// TODO
        /// </summary>
        HandCombat = 0x3C,

        /// <summary>
        /// TODO
        /// </summary>
        NonCombat = 0x3D,

        /// <summary>
        /// TODO
        /// </summary>
        SwordCombat = 0x3E,

        /// <summary>
        /// TODO
        /// </summary>
        BowCombat = 0x3F,

        /// <summary>
        /// TODO
        /// </summary>
        SwordShieldCombat = 0x40,

        /// <summary>
        /// TODO
        /// </summary>
        CrossbowCombat = 0x41,

        /// <summary>
        /// TODO
        /// </summary>
        UnusedCombat = 0x42,

        /// <summary>
        /// TODO
        /// </summary>
        SlingCombat = 0x43,

        /// <summary>
        /// TODO
        /// </summary>
        TwoHandedSwordCombat = 0x44,

        /// <summary>
        /// TODO
        /// </summary>
        TwoHandedStaffCombat = 0x45,

        /// <summary>
        /// TODO
        /// </summary>
        DualWieldCombat = 0x46,

        /// <summary>
        /// TODO
        /// </summary>
        ThrownWeaponCombat = 0x47,

        /// <summary>
        /// TODO
        /// </summary>
        Magic = 0x49,

        /// <summary>
        /// TODO
        /// </summary>
        BowNoAmmo = 0xE8,

        /// <summary>
        /// TODO
        /// </summary>
        CrossBowNoAmmo = 0xE9,

        /// <summary>
        /// TODO
        /// </summary>
        AtlatlCombat = 0x138,

        /// <summary>
        /// TODO
        /// </summary>
        ThrownShieldCombat = 0x139,

    }

    [Flags]
    public enum RawMotionFlags : uint {
        Invalid = 0x0,
        CurrentHoldKey = 0x1,
        CurrentStyle = 0x2,
        ForwardCommand = 0x4,
        ForwardHoldKey = 0x8,
        ForwardSpeed = 0x10,
        SideStepCommand = 0x20,
        SideStepHoldKey = 0x40,
        SideStepSpeed = 0x80,
        TurnCommand = 0x100,
        TurnHoldKey = 0x200,
        TurnSpeed = 0x400
    }

    /// <summary>
    /// Mask for MotionCommands
    /// </summary>
    [Flags]
    public enum MotionCommandMask : uint {
        Style = 0x80000000,
        SubState = 0x40000000,
        Modifier = 0x20000000,
        Action = 0x10000000,
        UI = 0x08000000,
        Toggle = 0x04000000,
        ChatEmote = 0x02000000,
        Mappable = 0x01000000,
        Command = ~(Style | SubState | Modifier | Action | UI | Toggle | ChatEmote | Mappable)
    }
    /// <summary>
    /// The list of stances for players and creatures
    /// This is a subset of MotionCommand
    /// </summary>
    public enum MotionDatStance : uint {
        Invalid = 0x80000000,
        HandCombat = 0x8000003c,
        NonCombat = 0x8000003d,
        SwordCombat = 0x8000003e,
        BowCombat = 0x8000003f,
        SwordShieldCombat = 0x80000040,
        CrossbowCombat = 0x80000041,
        UnusedCombat = 0x80000042,
        SlingCombat = 0x80000043,
        TwoHandedSwordCombat = 0x80000044, // 2HandedSwordCombat
        TwoHandedStaffCombat = 0x80000045, // 2HandedStaffCombat 
        DualWieldCombat = 0x80000046,
        ThrownWeaponCombat = 0x80000047,
        Graze = 0x80000048,
        Magic = 0x80000049,
        BowNoAmmo = 0x800000e8,
        CrossBowNoAmmo = 0x800000e9,
        AtlatlCombat = 0x8000013b, // 138 in PY16
        ThrownShieldCombat = 0x8000013c, // 139 in PY16
    }

    /// <summary>
    /// The movement (forward, side, turn) for a character or monster.
    /// </summary>
    public enum MotionCommand : uint {
        Invalid = 0x0,
        HoldRun = 0x00000001,
        HoldSidestep = 0x00000002,
        Ready = 0x00000003,
        Stop = 0x00000004,
        WalkForward = 0x00000005,
        WalkBackwards = 0x00000006,
        RunForward = 0x00000007,
        Fallen = 0x00000008,
        Interpolating = 0x00000009,
        Hover = 0x0000000a,
        On = 0x0000000b,
        Off = 0x0000000c,
        TurnRight = 0x0000000d,
        TurnLeft = 0x0000000e,
        SideStepRight = 0x0000000f,
        SideStepLeft = 0x00000010,
        Dead = 0x00000011,
        Crouch = 0x00000012,
        Sitting = 0x00000013,
        Sleeping = 0x00000014,
        Falling = 0x00000015,
        Reload = 0x00000016,
        Unload = 0x00000017,
        Pickup = 0x00000018,
        StoreInBackpack = 0x00000019,
        Eat = 0x0000001a,
        Drink = 0x0000001b,
        Reading = 0x0000001c,
        JumpCharging = 0x0000001d,
        AimLevel = 0x0000001e,
        AimHigh15 = 0x0000001f,
        AimHigh30 = 0x00000020,
        AimHigh45 = 0x00000021,
        AimHigh60 = 0x00000022,
        AimHigh75 = 0x00000023,
        AimHigh90 = 0x00000024,
        AimLow15 = 0x00000025,
        AimLow30 = 0x00000026,
        AimLow45 = 0x00000027,
        AimLow60 = 0x00000028,
        AimLow75 = 0x00000029,
        AimLow90 = 0x0000002a,
        MagicBlast = 0x0000002b,
        MagicSelfHead = 0x0000002c,
        MagicSelfHeart = 0x0000002d,
        MagicBonus = 0x0000002e,
        MagicClap = 0x0000002f,
        MagicHarm = 0x00000030,
        MagicHeal = 0x00000031,
        MagicThrowMissile = 0x00000032,
        MagicRecoilMissile = 0x00000033,
        MagicPenalty = 0x00000034,
        MagicTransfer = 0x00000035,
        MagicVision = 0x00000036,
        MagicEnchantItem = 0x00000037,
        MagicPortal = 0x00000038,
        MagicPray = 0x00000039,
        StopTurning = 0x0000003a,
        Jump = 0x0000003b,
        HandCombat = 0x0000003c,
        NonCombat = 0x0000003d,
        SwordCombat = 0x0000003e,
        BowCombat = 0x0000003f,
        SwordShieldCombat = 0x00000040,
        CrossbowCombat = 0x00000041,
        UnusedCombat = 0x00000042,
        SlingCombat = 0x00000043,
        TwoHandedSwordCombat = 0x00000044,
        TwoHandedStaffCombat = 0x00000045,
        DualWieldCombat = 0x00000046,
        ThrownWeaponCombat = 0x00000047,
        Graze = 0x00000048,
        Magic = 0x00000049,
        Hop = 0x0000004a,
        Jumpup = 0x0000004b,
        Cheer = 0x0000004c,
        ChestBeat = 0x0000004d,
        TippedLeft = 0x0000004e,
        TippedRight = 0x0000004f,
        FallDown = 0x00000050,
        Twitch1 = 0x00000051,
        Twitch2 = 0x00000052,
        Twitch3 = 0x00000053,
        Twitch4 = 0x00000054,
        StaggerBackward = 0x00000055,
        StaggerForward = 0x00000056,
        Sanctuary = 0x00000057,
        ThrustMed = 0x00000058,
        ThrustLow = 0x00000059,
        ThrustHigh = 0x0000005a,
        SlashHigh = 0x0000005b,
        SlashMed = 0x0000005c,
        SlashLow = 0x0000005d,
        BackhandHigh = 0x0000005e,
        BackhandMed = 0x0000005f,
        BackhandLow = 0x00000060,
        Shoot = 0x00000061,
        AttackHigh1 = 0x00000062,
        AttackMed1 = 0x00000063,
        AttackLow1 = 0x00000064,
        AttackHigh2 = 0x00000065,
        AttackMed2 = 0x00000066,
        AttackLow2 = 0x00000067,
        AttackHigh3 = 0x00000068,
        AttackMed3 = 0x00000069,
        AttackLow3 = 0x0000006a,
        HeadThrow = 0x0000006b,
        FistSlam = 0x0000006c,
        BreatheFlame = 0x0000006d,
        SpinAttack = 0x0000006e,
        MagicPowerUp01 = 0x0000006f,
        MagicPowerUp02 = 0x00000070,
        MagicPowerUp03 = 0x00000071,
        MagicPowerUp04 = 0x00000072,
        MagicPowerUp05 = 0x00000073,
        MagicPowerUp06 = 0x00000074,
        MagicPowerUp07 = 0x00000075,
        MagicPowerUp08 = 0x00000076,
        MagicPowerUp09 = 0x00000077,
        MagicPowerUp10 = 0x00000078,
        ShakeFist = 0x00000079,
        Beckon = 0x0000007a,
        BeSeeingYou = 0x0000007b,
        BlowKiss = 0x0000007c,
        BowDeep = 0x0000007d,
        ClapHands = 0x0000007e,
        Cry = 0x0000007f,
        Laugh = 0x00000080,
        MimeEat = 0x00000081,
        MimeDrink = 0x00000082,
        Nod = 0x00000083,
        Point = 0x00000084,
        ShakeHead = 0x00000085,
        Shrug = 0x00000086,
        Wave = 0x00000087,
        Akimbo = 0x00000088,
        HeartyLaugh = 0x00000089,
        Salute = 0x0000008a,
        ScratchHead = 0x0000008b,
        SmackHead = 0x0000008c,
        TapFoot = 0x0000008d,
        WaveHigh = 0x0000008e,
        WaveLow = 0x0000008f,
        YawnStretch = 0x00000090,
        Cringe = 0x00000091,
        Kneel = 0x00000092,
        Plead = 0x00000093,
        Shiver = 0x00000094,
        Shoo = 0x00000095,
        Slouch = 0x00000096,
        Spit = 0x00000097,
        Surrender = 0x00000098,
        Woah = 0x00000099,
        Winded = 0x0000009a,
        YMCA = 0x0000009b,
        EnterGame = 0x0000009c,
        ExitGame = 0x0000009d,
        OnCreation = 0x0000009e,
        OnDestruction = 0x0000009f,
        EnterPortal = 0x000000a0,
        ExitPortal = 0x000000a1,
        Cancel = 0x00000a2,
        UseSelected = 0x00000a3,
        AutosortSelected = 0x00000a4,
        DropSelected = 0x00000a5,
        GiveSelected = 0x00000a6,
        SplitSelected = 0x00000a7,
        ExamineSelected = 0x00000a8,
        CreateShortcutToSelected = 0x00000a9,
        PreviousCompassItem = 0x00000aa,
        NextCompassItem = 0x00000ab,
        ClosestCompassItem = 0x00000ac,
        PreviousSelection = 0x00000ad,
        LastAttacker = 0x00000ae,
        PreviousFellow = 0x00000af,
        NextFellow = 0x00000b0,
        ToggleCombat = 0x00000b1,
        HighAttack = 0x00000b2,
        MediumAttack = 0x00000b3,
        LowAttack = 0x00000b4,
        EnterChat = 0x00000b5,
        ToggleChat = 0x00000b6,
        SavePosition = 0x00000b7,
        OptionsPanel = 0x00000b8,
        ResetView = 0x00000b9,
        CameraLeftRotate = 0x00000ba,
        CameraRightRotate = 0x00000bb,
        CameraRaise = 0x00000bc,
        CameraLower = 0x00000bd,
        CameraCloser = 0x00000be,
        CameraFarther = 0x00000bf,
        FloorView = 0x00000c0,
        MouseLook = 0x00000c1,
        PreviousItem = 0x00000c2,
        NextItem = 0x00000c3,
        ClosestItem = 0x00000c4,
        ShiftView = 0x00000c5,
        MapView = 0x00000c6,
        AutoRun = 0x00000c7,
        DecreasePowerSetting = 0x00000c8,
        IncreasePowerSetting = 0x00000c9,
        Pray = 0x000000ca,
        Mock = 0x000000cb,
        Teapot = 0x000000cc,
        SpecialAttack1 = 0x000000cd,
        SpecialAttack2 = 0x000000ce,
        SpecialAttack3 = 0x000000cf,
        MissileAttack1 = 0x000000d0,
        MissileAttack2 = 0x000000d1,
        MissileAttack3 = 0x000000d2,
        CastSpell = 0x000000d3,
        Flatulence = 0x000000d4,
        FirstPersonView = 0x00000d5,
        AllegiancePanel = 0x00000d6,
        FellowshipPanel = 0x00000d7,
        SpellbookPanel = 0x00000d8,
        SpellComponentsPanel = 0x00000d9,
        HousePanel = 0x00000da,
        AttributesPanel = 0x00000db,
        SkillsPanel = 0x00000dc,
        MapPanel = 0x00000dd,
        InventoryPanel = 0x00000de,
        Demonet = 0x000000df,
        UseMagicStaff = 0x000000e0,
        UseMagicWand = 0x000000e1,
        Blink = 0x000000e2,
        Bite = 0x000000e3,
        TwitchSubstate1 = 0x000000e4,
        TwitchSubstate2 = 0x000000e5,
        TwitchSubstate3 = 0x000000e6,
        CaptureScreenshotToFile = 0x00000e7,
        BowNoAmmo = 0x000000e8,
        CrossBowNoAmmo = 0x000000e9,
        ShakeFistState = 0x000000ea,
        PrayState = 0x000000eb,
        BowDeepState = 0x000000ec,
        ClapHandsState = 0x000000ed,
        CrossArmsState = 0x000000ee,
        ShiverState = 0x000000ef,
        PointState = 0x000000f0,
        WaveState = 0x000000f1,
        AkimboState = 0x000000f2,
        SaluteState = 0x000000f3,
        ScratchHeadState = 0x000000f4,
        TapFootState = 0x000000f5,
        LeanState = 0x000000f6,
        KneelState = 0x000000f7,
        PleadState = 0x000000f8,
        ATOYOT = 0x000000f9,
        SlouchState = 0x000000fa,
        SurrenderState = 0x000000fb,
        WoahState = 0x000000fc,
        WindedState = 0x000000fd,
        AutoCreateShortcuts = 0x00000fe,
        AutoRepeatAttacks = 0x00000ff,
        AutoTarget = 0x0000100,
        AdvancedCombatInterface = 0x0000101,
        IgnoreAllegianceRequests = 0x0000102,
        IgnoreFellowshipRequests = 0x0000103,
        InvertMouseLook = 0x0000104,
        LetPlayersGiveYouItems = 0x0000105,
        AutoTrackCombatTargets = 0x0000106,
        DisplayTooltips = 0x0000107,
        AttemptToDeceivePlayers = 0x0000108,
        RunAsDefaultMovement = 0x0000109,
        StayInChatModeAfterSend = 0x000010a,
        RightClickToMouseLook = 0x000010b,
        VividTargetIndicator = 0x000010c,
        SelectSelf = 0x000010d,
        SkillHealSelf = 0x0000010e,
        //WoahDuplicate1                      = 0x0000010f,  // ushort collision with NextMonster?
        SkillHealOther = 0x0000010f,
        //MimeDrinkDuplicate1                 = 0x00000110,  // ushort collision with PreviousMonster?
        //MimeDrinkDuplicate2                 = 0x00000111,  // ushort collision with ClosestMonster?
        //NextMonster                         = 0x000010f,
        PreviousMonster = 0x0000110,
        ClosestMonster = 0x0000111,
        NextPlayer = 0x0000112,
        PreviousPlayer = 0x0000113,
        ClosestPlayer = 0x0000114,
        SnowAngelState = 0x00000118,
        WarmHands = 0x00000119,
        CurtseyState = 0x0000011a,
        AFKState = 0x0000011b,
        MeditateState = 0x0000011c,
        TradePanel = 0x000011d,
        LogOut = 0x0000011e,
        DoubleSlashLow = 0x0000011f,
        DoubleSlashMed = 0x00000120,
        DoubleSlashHigh = 0x00000121,
        TripleSlashLow = 0x00000122,
        TripleSlashMed = 0x00000123,
        TripleSlashHigh = 0x00000124,
        DoubleThrustLow = 0x00000125,
        DoubleThrustMed = 0x00000126,
        DoubleThrustHigh = 0x00000127,
        TripleThrustLow = 0x00000128,
        TripleThrustMed = 0x00000129,
        TripleThrustHigh = 0x0000012a,
        MagicPowerUp01Purple = 0x0000012b,
        MagicPowerUp02Purple = 0x0000012c,
        MagicPowerUp03Purple = 0x0000012d,
        MagicPowerUp04Purple = 0x0000012e,
        MagicPowerUp05Purple = 0x0000012f,
        MagicPowerUp06Purple = 0x00000130,
        MagicPowerUp07Purple = 0x00000131,
        MagicPowerUp08Purple = 0x00000132,
        MagicPowerUp09Purple = 0x00000133,
        MagicPowerUp10Purple = 0x00000134,
        Helper = 0x00000135,
        Pickup5 = 0x00000136,
        Pickup10 = 0x00000137,
        Pickup15 = 0x00000138,
        Pickup20 = 0x00000139,
        HouseRecall = 0x0000013a,
        AtlatlCombat = 0x0000013b,
        ThrownShieldCombat = 0x0000013c,
        SitState = 0x0000013d,
        SitCrossleggedState = 0x0000013e,
        SitBackState = 0x0000013f,
        PointLeftState = 0x00000140,
        PointRightState = 0x00000141,
        TalktotheHandState = 0x00000142,
        PointDownState = 0x00000143,
        DrudgeDanceState = 0x00000144,
        PossumState = 0x00000145,
        ReadState = 0x00000146,
        ThinkerState = 0x00000147,
        HaveASeatState = 0x00000148,
        AtEaseState = 0x00000149,
        NudgeLeft = 0x0000014a,
        NudgeRight = 0x0000014b,
        PointLeft = 0x0000014c,
        PointRight = 0x0000014d,
        PointDown = 0x0000014e,
        Knock = 0x0000014f,
        ScanHorizon = 0x00000150,
        DrudgeDance = 0x00000151,
        HaveASeat = 0x00000152,
        LifestoneRecall = 0x00000153,
        CharacterOptionsPanel = 0x0000154,
        SoundAndGraphicsPanel = 0x0000155,
        HelpfulSpellsPanel = 0x0000156,
        HarmfulSpellsPanel = 0x0000157,
        CharacterInformationPanel = 0x0000158,
        LinkStatusPanel = 0x0000159,
        VitaePanel = 0x000015a,
        ShareFellowshipXP = 0x000015b,
        ShareFellowshipLoot = 0x000015c,
        AcceptCorpseLooting = 0x000015d,
        IgnoreTradeRequests = 0x000015e,
        DisableWeather = 0x000015f,
        DisableHouseEffect = 0x0000160,
        SideBySideVitals = 0x0000161,
        ShowRadarCoordinates = 0x0000162,
        ShowSpellDurations = 0x0000163,
        MuteOnLosingFocus = 0x0000164,
        Fishing = 0x00000165,
        MarketplaceRecall = 0x00000166,
        EnterPKLite = 0x00000167,
        AllegianceChat = 0x0000168,
        AutomaticallyAcceptFellowshipRequests = 0x0000169,
        Reply = 0x000016a,
        MonarchReply = 0x000016b,
        PatronReply = 0x000016c,
        ToggleCraftingChanceOfSuccessDialog = 0x000016d,
        UseClosestUnopenedCorpse = 0x000016e,
        UseNextUnopenedCorpse = 0x000016f,
        IssueSlashCommand = 0x0000170,
        AllegianceHometownRecall = 0x00000171,
        PKArenaRecall = 0x00000172,
        OffhandSlashHigh = 0x00000173,
        OffhandSlashMed = 0x00000174,
        OffhandSlashLow = 0x00000175,
        OffhandThrustHigh = 0x00000176,
        OffhandThrustMed = 0x00000177,
        OffhandThrustLow = 0x00000178,
        OffhandDoubleSlashLow = 0x00000179,
        OffhandDoubleSlashMed = 0x0000017a,
        OffhandDoubleSlashHigh = 0x0000017b,
        OffhandTripleSlashLow = 0x0000017c,
        OffhandTripleSlashMed = 0x0000017d,
        OffhandTripleSlashHigh = 0x0000017e,
        OffhandDoubleThrustLow = 0x0000017f,
        OffhandDoubleThrustMed = 0x00000180,
        OffhandDoubleThrustHigh = 0x00000181,
        OffhandTripleThrustLow = 0x00000182,
        OffhandTripleThrustMed = 0x00000183,
        OffhandTripleThrustHigh = 0x00000184,
        OffhandKick = 0x00000185,
        AttackHigh4 = 0x00000186,
        AttackMed4 = 0x00000187,
        AttackLow4 = 0x00000188,
        AttackHigh5 = 0x00000189,
        AttackMed5 = 0x0000018a,
        AttackLow5 = 0x0000018b,
        AttackHigh6 = 0x0000018c,
        AttackMed6 = 0x0000018d,
        AttackLow6 = 0x0000018e,
        PunchFastHigh = 0x0000018f,
        PunchFastMed = 0x00000190,
        PunchFastLow = 0x00000191,
        PunchSlowHigh = 0x00000192,
        PunchSlowMed = 0x00000193,
        PunchSlowLow = 0x00000194,
        OffhandPunchFastHigh = 0x00000195,
        OffhandPunchFastMed = 0x00000196,
        OffhandPunchFastLow = 0x00000197,
        OffhandPunchSlowHigh = 0x00000198,
        OffhandPunchSlowMed = 0x00000199,
        OffhandPunchSlowLow = 0x0000019a,
        WoahDuplicate2 = 0x0000019b, // Appears to be the same as Motion_Woah except it starts with 0x10 instead of 0x13
    }
    // TODO: Figure out what bitfield(s) these values map to and replace with OR's
    // Note: These IDs are from the last version of the client. Earlier versions of the client had different values for some of the enums.
    public enum MotionDatCommand : uint {
        Invalid = 0x0,
        HoldRun = 0x85000001,
        HoldSidestep = 0x85000002,
        Ready = 0x41000003,
        Stop = 0x40000004,
        WalkForward = 0x45000005,
        WalkBackwards = 0x45000006,
        RunForward = 0x44000007,
        Fallen = 0x40000008,
        Interpolating = 0x40000009,
        Hover = 0x4000000a,
        On = 0x4000000b,
        Off = 0x4000000c,
        TurnRight = 0x6500000d,
        TurnLeft = 0x6500000e,
        SideStepRight = 0x6500000f,
        SideStepLeft = 0x65000010,
        Dead = 0x40000011,
        Crouch = 0x41000012,
        Sitting = 0x41000013,
        Sleeping = 0x41000014,
        Falling = 0x40000015,
        Reload = 0x40000016,
        Unload = 0x40000017,
        Pickup = 0x40000018,
        StoreInBackpack = 0x40000019,
        Eat = 0x4000001a,
        Drink = 0x4000001b,
        Reading = 0x4000001c,
        JumpCharging = 0x4000001d,
        AimLevel = 0x4000001e,
        AimHigh15 = 0x4000001f,
        AimHigh30 = 0x40000020,
        AimHigh45 = 0x40000021,
        AimHigh60 = 0x40000022,
        AimHigh75 = 0x40000023,
        AimHigh90 = 0x40000024,
        AimLow15 = 0x40000025,
        AimLow30 = 0x40000026,
        AimLow45 = 0x40000027,
        AimLow60 = 0x40000028,
        AimLow75 = 0x40000029,
        AimLow90 = 0x4000002a,
        MagicBlast = 0x4000002b,
        MagicSelfHead = 0x4000002c,
        MagicSelfHeart = 0x4000002d,
        MagicBonus = 0x4000002e,
        MagicClap = 0x4000002f,
        MagicHarm = 0x40000030,
        MagicHeal = 0x40000031,
        MagicThrowMissile = 0x40000032,
        MagicRecoilMissile = 0x40000033,
        MagicPenalty = 0x40000034,
        MagicTransfer = 0x40000035,
        MagicVision = 0x40000036,
        MagicEnchantItem = 0x40000037,
        MagicPortal = 0x40000038,
        MagicPray = 0x40000039,
        StopTurning = 0x2000003a,
        Jump = 0x2500003b,
        HandCombat = 0x8000003c,
        NonCombat = 0x8000003d,
        SwordCombat = 0x8000003e,
        BowCombat = 0x8000003f,
        SwordShieldCombat = 0x80000040,
        CrossbowCombat = 0x80000041,
        UnusedCombat = 0x80000042,
        SlingCombat = 0x80000043,
        TwoHandedSwordCombat = 0x80000044,
        TwoHandedStaffCombat = 0x80000045,
        DualWieldCombat = 0x80000046,
        ThrownWeaponCombat = 0x80000047,
        Graze = 0x80000048,
        Magic = 0x80000049,
        Hop = 0x1000004a,
        Jumpup = 0x1000004b,
        Cheer = 0x1300004c,
        ChestBeat = 0x1000004d,
        TippedLeft = 0x1000004e,
        TippedRight = 0x1000004f,
        FallDown = 0x10000050,
        Twitch1 = 0x10000051,
        Twitch2 = 0x10000052,
        Twitch3 = 0x10000053,
        Twitch4 = 0x10000054,
        StaggerBackward = 0x10000055,
        StaggerForward = 0x10000056,
        Sanctuary = 0x10000057,
        ThrustMed = 0x10000058,
        ThrustLow = 0x10000059,
        ThrustHigh = 0x1000005a,
        SlashHigh = 0x1000005b,
        SlashMed = 0x1000005c,
        SlashLow = 0x1000005d,
        BackhandHigh = 0x1000005e,
        BackhandMed = 0x1000005f,
        BackhandLow = 0x10000060,
        Shoot = 0x10000061,
        AttackHigh1 = 0x10000062,
        AttackMed1 = 0x10000063,
        AttackLow1 = 0x10000064,
        AttackHigh2 = 0x10000065,
        AttackMed2 = 0x10000066,
        AttackLow2 = 0x10000067,
        AttackHigh3 = 0x10000068,
        AttackMed3 = 0x10000069,
        AttackLow3 = 0x1000006a,
        HeadThrow = 0x1000006b,
        FistSlam = 0x1000006c,
        BreatheFlame = 0x1000006d,
        SpinAttack = 0x1000006e,
        MagicPowerUp01 = 0x1000006f,
        MagicPowerUp02 = 0x10000070,
        MagicPowerUp03 = 0x10000071,
        MagicPowerUp04 = 0x10000072,
        MagicPowerUp05 = 0x10000073,
        MagicPowerUp06 = 0x10000074,
        MagicPowerUp07 = 0x10000075,
        MagicPowerUp08 = 0x10000076,
        MagicPowerUp09 = 0x10000077,
        MagicPowerUp10 = 0x10000078,
        ShakeFist = 0x13000079,
        Beckon = 0x1300007a,
        BeSeeingYou = 0x1300007b,
        BlowKiss = 0x1300007c,
        BowDeep = 0x1300007d,
        ClapHands = 0x1300007e,
        Cry = 0x1300007f,
        Laugh = 0x13000080,
        MimeEat = 0x13000081,
        MimeDrink = 0x13000082,
        Nod = 0x13000083,
        Point = 0x13000084,
        ShakeHead = 0x13000085,
        Shrug = 0x13000086,
        Wave = 0x13000087,
        Akimbo = 0x13000088,
        HeartyLaugh = 0x13000089,
        Salute = 0x1300008a,
        ScratchHead = 0x1300008b,
        SmackHead = 0x1300008c,
        TapFoot = 0x1300008d,
        WaveHigh = 0x1300008e,
        WaveLow = 0x1300008f,
        YawnStretch = 0x13000090,
        Cringe = 0x13000091,
        Kneel = 0x13000092,
        Plead = 0x13000093,
        Shiver = 0x13000094,
        Shoo = 0x13000095,
        Slouch = 0x13000096,
        Spit = 0x13000097,
        Surrender = 0x13000098,
        Woah = 0x13000099,
        Winded = 0x1300009a,
        YMCA = 0x1200009b,
        EnterGame = 0x1000009c,
        ExitGame = 0x1000009d,
        OnCreation = 0x1000009e,
        OnDestruction = 0x1000009f,
        EnterPortal = 0x100000a0,
        ExitPortal = 0x100000a1,
        Cancel = 0x80000a2,
        UseSelected = 0x90000a3,
        AutosortSelected = 0x90000a4,
        DropSelected = 0x90000a5,
        GiveSelected = 0x90000a6,
        SplitSelected = 0x90000a7,
        ExamineSelected = 0x90000a8,
        CreateShortcutToSelected = 0x80000a9,
        PreviousCompassItem = 0x90000aa,
        NextCompassItem = 0x90000ab,
        ClosestCompassItem = 0x90000ac,
        PreviousSelection = 0x90000ad,
        LastAttacker = 0x90000ae,
        PreviousFellow = 0x90000af,
        NextFellow = 0x90000b0,
        ToggleCombat = 0x90000b1,
        HighAttack = 0xd0000b2,
        MediumAttack = 0xd0000b3,
        LowAttack = 0xd0000b4,
        EnterChat = 0x80000b5,
        ToggleChat = 0x80000b6,
        SavePosition = 0x80000b7,
        OptionsPanel = 0x90000b8,
        ResetView = 0x90000b9,
        CameraLeftRotate = 0xd0000ba,
        CameraRightRotate = 0xd0000bb,
        CameraRaise = 0xd0000bc,
        CameraLower = 0xd0000bd,
        CameraCloser = 0xd0000be,
        CameraFarther = 0xd0000bf,
        FloorView = 0x90000c0,
        MouseLook = 0xc0000c1,
        PreviousItem = 0x90000c2,
        NextItem = 0x90000c3,
        ClosestItem = 0x90000c4,
        ShiftView = 0xd0000c5,
        MapView = 0x90000c6,
        AutoRun = 0x90000c7,
        DecreasePowerSetting = 0x90000c8,
        IncreasePowerSetting = 0x90000c9,
        Pray = 0x130000ca,
        Mock = 0x130000cb,
        Teapot = 0x130000cc,
        SpecialAttack1 = 0x100000cd,
        SpecialAttack2 = 0x100000ce,
        SpecialAttack3 = 0x100000cf,
        MissileAttack1 = 0x100000d0,
        MissileAttack2 = 0x100000d1,
        MissileAttack3 = 0x100000d2,
        CastSpell = 0x400000d3,
        Flatulence = 0x120000d4,
        FirstPersonView = 0x90000d5,
        AllegiancePanel = 0x90000d6,
        FellowshipPanel = 0x90000d7,
        SpellbookPanel = 0x90000d8,
        SpellComponentsPanel = 0x90000d9,
        HousePanel = 0x90000da,
        AttributesPanel = 0x90000db,
        SkillsPanel = 0x90000dc,
        MapPanel = 0x90000dd,
        InventoryPanel = 0x90000de,
        Demonet = 0x120000df,
        UseMagicStaff = 0x400000e0,
        UseMagicWand = 0x400000e1,
        Blink = 0x100000e2,
        Bite = 0x100000e3,
        TwitchSubstate1 = 0x400000e4,
        TwitchSubstate2 = 0x400000e5,
        TwitchSubstate3 = 0x400000e6,
        CaptureScreenshotToFile = 0x90000e7,
        BowNoAmmo = 0x800000e8,
        CrossBowNoAmmo = 0x800000e9,
        ShakeFistState = 0x430000ea,
        PrayState = 0x430000eb,
        BowDeepState = 0x430000ec,
        ClapHandsState = 0x430000ed,
        CrossArmsState = 0x430000ee,
        ShiverState = 0x430000ef,
        PointState = 0x430000f0,
        WaveState = 0x430000f1,
        AkimboState = 0x430000f2,
        SaluteState = 0x430000f3,
        ScratchHeadState = 0x430000f4,
        TapFootState = 0x430000f5,
        LeanState = 0x430000f6,
        KneelState = 0x430000f7,
        PleadState = 0x430000f8,
        ATOYOT = 0x420000f9,
        SlouchState = 0x430000fa,
        SurrenderState = 0x430000fb,
        WoahState = 0x430000fc,
        WindedState = 0x430000fd,
        AutoCreateShortcuts = 0x90000fe,
        AutoRepeatAttacks = 0x90000ff,
        AutoTarget = 0x9000100,
        AdvancedCombatInterface = 0x9000101,
        IgnoreAllegianceRequests = 0x9000102,
        IgnoreFellowshipRequests = 0x9000103,
        InvertMouseLook = 0x9000104,
        LetPlayersGiveYouItems = 0x9000105,
        AutoTrackCombatTargets = 0x9000106,
        DisplayTooltips = 0x9000107,
        AttemptToDeceivePlayers = 0x9000108,
        RunAsDefaultMovement = 0x9000109,
        StayInChatModeAfterSend = 0x900010a,
        RightClickToMouseLook = 0x900010b,
        VividTargetIndicator = 0x900010c,
        SelectSelf = 0x900010d,
        SkillHealSelf = 0x1000010e,
        //WoahDuplicate1                      = 0x1000010f,  // ushort collision with NextMonster?
        SkillHealOther = 0x1000010f,
        //MimeDrinkDuplicate1                 = 0x10000110,  // ushort collision with PreviousMonster?
        //MimeDrinkDuplicate2                 = 0x10000111,  // ushort collision with ClosestMonster?
        //NextMonster                         = 0x900010f,
        PreviousMonster = 0x9000110,
        ClosestMonster = 0x9000111,
        NextPlayer = 0x9000112,
        PreviousPlayer = 0x9000113,
        ClosestPlayer = 0x9000114,
        SnowAngelState = 0x43000118,
        WarmHands = 0x13000119,
        CurtseyState = 0x4300011a,
        AFKState = 0x4300011b,
        MeditateState = 0x4300011c,
        TradePanel = 0x900011d,
        LogOut = 0x1000011e,
        DoubleSlashLow = 0x1000011f,
        DoubleSlashMed = 0x10000120,
        DoubleSlashHigh = 0x10000121,
        TripleSlashLow = 0x10000122,
        TripleSlashMed = 0x10000123,
        TripleSlashHigh = 0x10000124,
        DoubleThrustLow = 0x10000125,
        DoubleThrustMed = 0x10000126,
        DoubleThrustHigh = 0x10000127,
        TripleThrustLow = 0x10000128,
        TripleThrustMed = 0x10000129,
        TripleThrustHigh = 0x1000012a,
        MagicPowerUp01Purple = 0x1000012b,
        MagicPowerUp02Purple = 0x1000012c,
        MagicPowerUp03Purple = 0x1000012d,
        MagicPowerUp04Purple = 0x1000012e,
        MagicPowerUp05Purple = 0x1000012f,
        MagicPowerUp06Purple = 0x10000130,
        MagicPowerUp07Purple = 0x10000131,
        MagicPowerUp08Purple = 0x10000132,
        MagicPowerUp09Purple = 0x10000133,
        MagicPowerUp10Purple = 0x10000134,
        Helper = 0x13000135,
        Pickup5 = 0x40000136,
        Pickup10 = 0x40000137,
        Pickup15 = 0x40000138,
        Pickup20 = 0x40000139,
        HouseRecall = 0x1000013a,
        AtlatlCombat = 0x8000013b,
        ThrownShieldCombat = 0x8000013c,
        SitState = 0x4300013d,
        SitCrossleggedState = 0x4300013e,
        SitBackState = 0x4300013f,
        PointLeftState = 0x43000140,
        PointRightState = 0x43000141,
        TalktotheHandState = 0x43000142,
        PointDownState = 0x43000143,
        DrudgeDanceState = 0x43000144,
        PossumState = 0x43000145,
        ReadState = 0x43000146,
        ThinkerState = 0x43000147,
        HaveASeatState = 0x43000148,
        AtEaseState = 0x43000149,
        NudgeLeft = 0x1300014a,
        NudgeRight = 0x1300014b,
        PointLeft = 0x1300014c,
        PointRight = 0x1300014d,
        PointDown = 0x1300014e,
        Knock = 0x1300014f,
        ScanHorizon = 0x13000150,
        DrudgeDance = 0x13000151,
        HaveASeat = 0x13000152,
        LifestoneRecall = 0x10000153,
        CharacterOptionsPanel = 0x9000154,
        SoundAndGraphicsPanel = 0x9000155,
        HelpfulSpellsPanel = 0x9000156,
        HarmfulSpellsPanel = 0x9000157,
        CharacterInformationPanel = 0x9000158,
        LinkStatusPanel = 0x9000159,
        VitaePanel = 0x900015a,
        ShareFellowshipXP = 0x900015b,
        ShareFellowshipLoot = 0x900015c,
        AcceptCorpseLooting = 0x900015d,
        IgnoreTradeRequests = 0x900015e,
        DisableWeather = 0x900015f,
        DisableHouseEffect = 0x9000160,
        SideBySideVitals = 0x9000161,
        ShowRadarCoordinates = 0x9000162,
        ShowSpellDurations = 0x9000163,
        MuteOnLosingFocus = 0x9000164,
        Fishing = 0x10000165,
        MarketplaceRecall = 0x10000166,
        EnterPKLite = 0x10000167,
        AllegianceChat = 0x9000168,
        AutomaticallyAcceptFellowshipRequests = 0x9000169,
        Reply = 0x900016a,
        MonarchReply = 0x900016b,
        PatronReply = 0x900016c,
        ToggleCraftingChanceOfSuccessDialog = 0x900016d,
        UseClosestUnopenedCorpse = 0x900016e,
        UseNextUnopenedCorpse = 0x900016f,
        IssueSlashCommand = 0x9000170,
        AllegianceHometownRecall = 0x10000171,
        PKArenaRecall = 0x10000172,
        OffhandSlashHigh = 0x10000173,
        OffhandSlashMed = 0x10000174,
        OffhandSlashLow = 0x10000175,
        OffhandThrustHigh = 0x10000176,
        OffhandThrustMed = 0x10000177,
        OffhandThrustLow = 0x10000178,
        OffhandDoubleSlashLow = 0x10000179,
        OffhandDoubleSlashMed = 0x1000017a,
        OffhandDoubleSlashHigh = 0x1000017b,
        OffhandTripleSlashLow = 0x1000017c,
        OffhandTripleSlashMed = 0x1000017d,
        OffhandTripleSlashHigh = 0x1000017e,
        OffhandDoubleThrustLow = 0x1000017f,
        OffhandDoubleThrustMed = 0x10000180,
        OffhandDoubleThrustHigh = 0x10000181,
        OffhandTripleThrustLow = 0x10000182,
        OffhandTripleThrustMed = 0x10000183,
        OffhandTripleThrustHigh = 0x10000184,
        OffhandKick = 0x10000185,
        AttackHigh4 = 0x10000186,
        AttackMed4 = 0x10000187,
        AttackLow4 = 0x10000188,
        AttackHigh5 = 0x10000189,
        AttackMed5 = 0x1000018a,
        AttackLow5 = 0x1000018b,
        AttackHigh6 = 0x1000018c,
        AttackMed6 = 0x1000018d,
        AttackLow6 = 0x1000018e,
        PunchFastHigh = 0x1000018f,
        PunchFastMed = 0x10000190,
        PunchFastLow = 0x10000191,
        PunchSlowHigh = 0x10000192,
        PunchSlowMed = 0x10000193,
        PunchSlowLow = 0x10000194,
        OffhandPunchFastHigh = 0x10000195,
        OffhandPunchFastMed = 0x10000196,
        OffhandPunchFastLow = 0x10000197,
        OffhandPunchSlowHigh = 0x10000198,
        OffhandPunchSlowMed = 0x10000199,
        OffhandPunchSlowLow = 0x1000019a,
        WoahDuplicate2 = 0x1000019b, // Appears to be the same as Motion_Woah except it starts with 0x10 instead of 0x13
    }

    /// <summary>
    /// The type response to a chargen request
    /// </summary>
    public enum CharGenResponseType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        OK = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        NameInUse = 0x0003,

        /// <summary>
        /// TODO
        /// </summary>
        NameBanned = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        Corrupt = 0x0005,

        /// <summary>
        /// TODO
        /// </summary>
        Corrupt_0x0006 = 0x0006,

        /// <summary>
        /// TODO
        /// </summary>
        AdminPrivilegeDenied = 0x0007,

    }

    /// <summary>
    /// The CharacterErrorType identifies the type of character error that has occured.
    /// </summary>
    public enum CharacterErrorType : uint {
        /// <summary>
        /// ID_CHAR_ERROR_LOGON
        /// </summary>
        Logon = 0x01,

        /// <summary>
        /// ID_CHAR_ERROR_ACCOUNT_LOGON
        /// </summary>
        AccountLogin = 0x03,

        /// <summary>
        /// ID_CHAR_ERROR_SERVER_CRASH
        /// </summary>
        ServerCrash = 0x04,

        /// <summary>
        /// ID_CHAR_ERROR_LOGOFF
        /// </summary>
        Logoff = 0x05,

        /// <summary>
        /// ID_CHAR_ERROR_DELETE
        /// </summary>
        Delete = 0x06,

        /// <summary>
        /// ID_CHAR_ERROR_SERVER_CRASH
        /// </summary>
        ServerCrash2 = 0x08,

        /// <summary>
        /// ID_CHAR_ERROR_ACCOUNT_INVALID
        /// </summary>
        AccountInvalid = 0x09,

        /// <summary>
        /// ID_CHAR_ERROR_ACCOUNT_DOESNT_EXIST
        /// </summary>
        AccountDoesntExist = 0x0A,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_GENERIC
        /// </summary>
        EnterGameGeneric = 0x0B,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_STRESS_ACCOUNT
        /// </summary>
        EnterGameStressAccount = 0x0C,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_IN_WORLD
        /// </summary>
        EnterGameCharacterInWorld = 0x0D,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_PLAYER_ACCOUNT_MISSING
        /// </summary>
        EnterGamePlayerAccountMissing = 0x0E,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_NOT_OWNED
        /// </summary>
        EnterGameCharacterNotOwned = 0x0F,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_IN_WORLD_SERVER
        /// </summary>
        EnterGameCharacterInWorldServer = 0x10,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_OLD_CHARACTER
        /// </summary>
        EnterGameOldCharacter = 0x11,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CORRUPT_CHARACTER
        /// </summary>
        EnterGameCorruptCharacter = 0x12,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_START_SERVER_DOWN
        /// </summary>
        EnterGameStartServerDown = 0x13,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_COULDNT_PLACE_CHARACTER
        /// </summary>
        EnterGameCouldntPlaceCharacter = 0x14,

        /// <summary>
        /// ID_CHAR_ERROR_LOGON_SERVER_FULL
        /// </summary>
        LogonServerFull = 0x15,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_LOCKED
        /// </summary>
        EnterGameCharacterLocked = 0x17,

        /// <summary>
        /// ID_CHAR_ERROR_SUBSCRIPTION_EXPIRED
        /// </summary>
        SubscriptionExpired = 0x18,

    }

    /// <summary>
    /// The state flags for an object
    /// </summary>
    [Flags]
    public enum PhysicsState : uint {
        /// <summary>
        /// TODO
        /// </summary>
        None = 0x00000000,

        /// <summary>
        /// TODO
        /// </summary>
        Static = 0x00000001,

        /// <summary>
        /// TODO
        /// </summary>
        Ethereal = 0x00000004,

        /// <summary>
        /// TODO
        /// </summary>
        ReportCollision = 0x00000008,

        /// <summary>
        /// TODO
        /// </summary>
        IgnoreCollision = 0x00000010,

        /// <summary>
        /// TODO
        /// </summary>
        NoDraw = 0x00000020,

        /// <summary>
        /// TODO
        /// </summary>
        Missle = 0x00000040,

        /// <summary>
        /// TODO
        /// </summary>
        Pushable = 0x00000080,

        /// <summary>
        /// TODO
        /// </summary>
        AlignPath = 0x00000100,

        /// <summary>
        /// TODO
        /// </summary>
        PathClipped = 0x00000200,

        /// <summary>
        /// TODO
        /// </summary>
        Gravity = 0x00000400,

        /// <summary>
        /// TODO
        /// </summary>
        LightingOn = 0x00000800,

        /// <summary>
        /// TODO
        /// </summary>
        ParticleEmitter = 0x00001000,

        /// <summary>
        /// TODO
        /// </summary>
        Hidden = 0x00004000,

        /// <summary>
        /// TODO
        /// </summary>
        ScriptedCollision = 0x00008000,

        /// <summary>
        /// TODO
        /// </summary>
        HasPhysicsBsp = 0x00010000,

        /// <summary>
        /// TODO
        /// </summary>
        Inelastic = 0x00020000,

        /// <summary>
        /// TODO
        /// </summary>
        HasDefaultAnim = 0x00040000,

        /// <summary>
        /// TODO
        /// </summary>
        HasDefaultScript = 0x00080000,

        /// <summary>
        /// TODO
        /// </summary>
        Cloaked = 0x00100000,

        /// <summary>
        /// TODO
        /// </summary>
        ReportCollisionAsEnvironment = 0x00200000,

        /// <summary>
        /// TODO
        /// </summary>
        EdgeSlide = 0x00400000,

        /// <summary>
        /// TODO
        /// </summary>
        Sledding = 0x00800000,

        /// <summary>
        /// TODO
        /// </summary>
        Frozen = 0x01000000,

    }

    /// <summary>
    /// The TurbineChatType identifies the type of Turbine Chat message.
    /// </summary>
    public enum TurbineChatType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        ServerToClientMessage = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        ClientToServerMessage = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        AckClientToServerMessage = 0x05,

    }

    /// <summary>
    /// The ResourceType identifies the dat file to be used.
    /// </summary>
    public enum ResourceType : long {
        /// <summary>
        /// TODO
        /// </summary>
        client_portal = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        client_cell_1 = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        client_local_English = 0x03,

    }

    /// <summary>
    /// The CompressionType identifies the type of data compression used.
    /// </summary>
    public enum CompressionType : byte {
        /// <summary>
        /// TODO
        /// </summary>
        None = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        ZLib = 0x01,

    }

    // from ACE

    public enum SpellId : uint {
        Undef = 0u,
        StrengthOther1 = 1u,
        StrengthSelf1 = 2u,
        WeaknessOther1 = 3u,
        WeaknessSelf1 = 4u,
        HealOther1 = 5u,
        HealSelf1 = 6u,
        HarmOther1 = 7u,
        HarmSelf1 = 8u,
        InfuseMana1 = 9u,
        UNKNOWN__GUESSEDNAME10 = 10u,
        UNKNOWN__GUESSEDNAME11 = 11u,
        UNKNOWN__GUESSEDNAME12 = 12u,
        UNKNOWN__GUESSEDNAME13 = 13u,
        UNKNOWN__GUESSEDNAME14 = 14u,
        VulnerabilityOther1 = 0xFu,
        VulnerabilitySelf1 = 0x10u,
        InvulnerabilityOther1 = 17u,
        InvulnerabilitySelf1 = 18u,
        FireProtectionOther1 = 19u,
        FireProtectionSelf1 = 20u,
        FireVulnerabilityOther1 = 21u,
        FireVulnerabilitySelf1 = 22u,
        ArmorOther1 = 23u,
        ArmorSelf1 = 24u,
        ImperilOther1 = 25u,
        ImperilSelf1 = 26u,
        FlameBolt1 = 27u,
        FrostBolt1 = 28u,
        UNKNOWN__GUESSEDNAME29 = 29u,
        UNKNOWN__GUESSEDNAME30 = 30u,
        UNKNOWN__GUESSEDNAME31 = 0x1Fu,
        UNKNOWN__GUESSEDNAME32 = 0x20u,
        UNKNOWN__GUESSEDNAME33 = 33u,
        UNKNOWN__GUESSEDNAME34 = 34u,
        BloodDrinkerSelf1 = 35u,
        BloodLoather = 36u,
        BladeBane1 = 37u,
        BladeLure1 = 38u,
        UNKNOWN__GUESSEDNAME39 = 39u,
        UNKNOWN__GUESSEDNAME40 = 40u,
        UNKNOWN__GUESSEDNAME41 = 41u,
        UNKNOWN__GUESSEDNAME42 = 42u,
        UNKNOWN__GUESSEDNAME43 = 43u,
        UNKNOWN__GUESSEDNAME44 = 44u,
        UNKNOWN__GUESSEDNAME45 = 45u,
        UNKNOWN__GUESSEDNAME46 = 46u,
        PortalTie1 = 47u,
        PortalTieRecall1 = 48u,
        SwiftKillerSelf1 = 49u,
        LeadenWeapon1 = 50u,
        Impenetrability1 = 51u,
        UNKNOWN__GUESSEDNAME52 = 52u,
        RejuvenationOther1 = 53u,
        RejuvenationSelf1 = 54u,
        UNKNOWN__GUESSEDNAME55 = 55u,
        UNKNOWN__GUESSEDNAME56 = 56u,
        MagicBolt1 = 57u,
        AcidStream1 = 58u,
        AcidStream2 = 59u,
        AcidStream3 = 60u,
        AcidStream4 = 61u,
        AcidStream5 = 62u,
        AcidStream6 = 0x3Fu,
        ShockWave1 = 0x40u,
        ShockWave2 = 65u,
        ShockWave3 = 66u,
        ShockWave4 = 67u,
        ShockWave5 = 68u,
        ShockWave6 = 69u,
        FrostBolt2 = 70u,
        FrostBolt3 = 71u,
        FrostBolt4 = 72u,
        FrostBolt5 = 73u,
        FrostBolt6 = 74u,
        LightningBolt1 = 75u,
        LightningBolt2 = 76u,
        LightningBolt3 = 77u,
        LightningBolt4 = 78u,
        LightningBolt5 = 79u,
        LightningBolt6 = 80u,
        FlameBolt2 = 81u,
        FlameBolt3 = 82u,
        FlameBolt4 = 83u,
        FlameBolt5 = 84u,
        FlameBolt6 = 85u,
        ForceBolt1 = 86u,
        ForceBolt2 = 87u,
        ForceBolt3 = 88u,
        ForceBolt4 = 89u,
        ForceBolt5 = 90u,
        ForceBolt6 = 91u,
        WhirlingBlade1 = 92u,
        WhirlingBlade2 = 93u,
        WhirlingBlade3 = 94u,
        WhirlingBlade4 = 95u,
        WhirlingBlade5 = 96u,
        WhirlingBlade6 = 97u,
        UNKNOWN__GUESSEDNAME98 = 98u,
        AcidBlast3 = 99u,
        AcidBlast4 = 100u,
        AcidBlast5 = 101u,
        AcidBlast6 = 102u,
        ShockBlast3 = 103u,
        ShockBlast4 = 104u,
        ShockBlast5 = 105u,
        ShockBlast6 = 106u,
        FrostBlast3 = 107u,
        FrostBlast4 = 108u,
        FrostBlast5 = 109u,
        FrostBlast6 = 110u,
        LightningBlast3 = 111u,
        LightningBlast4 = 112u,
        LightningBlast5 = 113u,
        LightningBlast6 = 114u,
        FlameBlast3 = 115u,
        FlameBlast4 = 116u,
        FlameBlast5 = 117u,
        FlameBlast6 = 118u,
        ForceBlast3 = 119u,
        ForceBlast4 = 120u,
        ForceBlast5 = 121u,
        ForceBlast6 = 122u,
        BladeBlast3 = 123u,
        BladeBlast4 = 124u,
        BladeBlast5 = 125u,
        BladeBlast6 = 126u,
        AcidVolley3 = 0x7Fu,
        AcidVolley4 = 0x80u,
        AcidVolley5 = 129u,
        AcidVolley6 = 130u,
        BludgeoningVolley3 = 131u,
        BludgeoningVolley4 = 132u,
        BludgeoningVolley5 = 133u,
        BludgeoningVolley6 = 134u,
        FrostVolley3 = 135u,
        FrostVolley4 = 136u,
        FrostVolley5 = 137u,
        FrostVolley6 = 138u,
        LightningVolley3 = 139u,
        LightningVolley4 = 140u,
        LightningVolley5 = 141u,
        LightningVolley6 = 142u,
        FlameVolley3 = 143u,
        FlameVolley4 = 144u,
        FlameVolley5 = 145u,
        FlameVolley6 = 146u,
        ForceVolley3 = 147u,
        ForceVolley4 = 148u,
        ForceVolley5 = 149u,
        ForceVolley6 = 150u,
        BladeVolley3 = 151u,
        BladeVolley4 = 152u,
        BladeVolley5 = 153u,
        BladeVolley6 = 154u,
        UNKNOWN__GUESSEDNAME155 = 155u,
        UNKNOWN__GUESSEDNAME156 = 156u,
        SummonPortal1 = 157u,
        SummonPortal2 = 158u,
        RegenerationOther1 = 159u,
        RegenerationOther2 = 160u,
        RegenerationOther3 = 161u,
        RegenerationOther4 = 162u,
        RegenerationOther5 = 163u,
        RegenerationOther6 = 164u,
        RegenerationSelf1 = 165u,
        RegenerationSelf2 = 166u,
        RegenerationSelf3 = 167u,
        RegenerationSelf4 = 168u,
        RegenerationSelf5 = 169u,
        RegenerationSelf6 = 170u,
        FesterOther1 = 171u,
        FesterOther2 = 172u,
        FesterOther3 = 173u,
        FesterOther4 = 174u,
        FesterOther5 = 175u,
        FesterOther6 = 176u,
        UNKNOWN__GUESSEDNAME177 = 177u,
        FesterSelf1 = 178u,
        FesterSelf2 = 179u,
        FesterSelf3 = 180u,
        FesterSelf4 = 181u,
        FesterSelf5 = 182u,
        FesterSelf6 = 183u,
        RejuvenationOther2 = 184u,
        RejuvenationOther3 = 185u,
        RejuvenationOther4 = 186u,
        RejuvenationOther5 = 187u,
        RejuvenationOther6 = 188u,
        RejuvenationSelf2 = 189u,
        RejuvenationSelf3 = 190u,
        RejuvenationSelf4 = 191u,
        RejuvenationSelf5 = 192u,
        RejuvenationSelf6 = 193u,
        ExhaustionOther1 = 194u,
        ExhaustionOther2 = 195u,
        ExhaustionOther3 = 196u,
        ExhaustionOther4 = 197u,
        ExhaustionOther5 = 198u,
        ExhaustionOther6 = 199u,
        ExhaustionSelf1 = 200u,
        ExhaustionSelf2 = 201u,
        ExhaustionSelf3 = 202u,
        ExhaustionSelf4 = 203u,
        ExhaustionSelf5 = 204u,
        ExhaustionSelf6 = 205u,
        ManaRenewalOther1 = 206u,
        ManaRenewalOther2 = 207u,
        ManaRenewalOther3 = 208u,
        ManaRenewalOther4 = 209u,
        ManaRenewalOther5 = 210u,
        ManaRenewalOther6 = 211u,
        ManaRenewalSelf1 = 212u,
        ManaRenewalSelf2 = 213u,
        ManaRenewalSelf3 = 214u,
        ManaRenewalSelf4 = 215u,
        ManaRenewalSelf5 = 216u,
        ManaRenewalSelf6 = 217u,
        ManaDepletionOther1 = 218u,
        ManaDepletionOther2 = 219u,
        ManaDepletionOther3 = 220u,
        ManaDepletionOther4 = 221u,
        ManaDepletionOther5 = 222u,
        ManaDepletionOther6 = 223u,
        ManaDepletionSelf1 = 224u,
        ManaDepletionSelf2 = 225u,
        ManaDepletionSelf3 = 226u,
        ManaDepletionSelf4 = 227u,
        ManaDepletionSelf5 = 228u,
        ManaDepletionSelf6 = 229u,
        VulnerabilityOther2 = 230u,
        VulnerabilityOther3 = 231u,
        VulnerabilityOther4 = 232u,
        VulnerabilityOther5 = 233u,
        VulnerabilityOther6 = 234u,
        VulnerabilitySelf2 = 235u,
        VulnerabilitySelf3 = 236u,
        VulnerabilitySelf4 = 237u,
        VulnerabilitySelf5 = 238u,
        VulnerabilitySelf6 = 239u,
        InvulnerabilityOther2 = 240u,
        InvulnerabilityOther3 = 241u,
        InvulnerabilityOther4 = 242u,
        InvulnerabilityOther5 = 243u,
        InvulnerabilityOther6 = 244u,
        InvulnerabilitySelf2 = 245u,
        InvulnerabilitySelf3 = 246u,
        InvulnerabilitySelf4 = 247u,
        InvulnerabilitySelf5 = 248u,
        InvulnerabilitySelf6 = 249u,
        ImpregnabilityOther1 = 250u,
        ImpregnabilityOther2 = 251u,
        ImpregnabilityOther3 = 252u,
        ImpregnabilityOther4 = 253u,
        ImpregnabilityOther5 = 254u,
        ImpregnabilityOther6 = 0xFFu,
        ImpregnabilitySelf1 = 0x100u,
        ImpregnabilitySelf2 = 257u,
        ImpregnabilitySelf3 = 258u,
        ImpregnabilitySelf4 = 259u,
        ImpregnabilitySelf5 = 260u,
        ImpregnabilitySelf6 = 261u,
        DefenselessnessOther1 = 262u,
        DefenselessnessOther2 = 263u,
        DefenselessnessOther3 = 264u,
        DefenselessnessOther4 = 265u,
        DefenselessnessOther5 = 266u,
        DefenselessnessOther6 = 267u,
        MagicResistanceOther1 = 268u,
        MagicResistanceOther2 = 269u,
        MagicResistanceOther3 = 270u,
        MagicResistanceOther4 = 271u,
        MagicResistanceOther5 = 272u,
        MagicResistanceOther6 = 273u,
        MagicResistanceSelf1 = 274u,
        MagicResistanceSelf2 = 275u,
        MagicResistanceSelf3 = 276u,
        MagicResistanceSelf4 = 277u,
        MagicResistanceSelf5 = 278u,
        MagicResistanceSelf6 = 279u,
        MagicYieldOther1 = 280u,
        MagicYieldOther2 = 281u,
        MagicYieldOther3 = 282u,
        MagicYieldOther4 = 283u,
        MagicYieldOther5 = 284u,
        MagicYieldOther6 = 285u,
        MagicYieldSelf1 = 286u,
        MagicYieldSelf2 = 287u,
        MagicYieldSelf3 = 288u,
        MagicYieldSelf4 = 289u,
        MagicYieldSelf5 = 290u,
        MagicYieldSelf6 = 291u,
        LightWeaponsMasteryOther1 = 292u,
        LightWeaponsMasteryOther2 = 293u,
        LightWeaponsMasteryOther3 = 294u,
        LightWeaponsMasteryOther4 = 295u,
        LightWeaponsMasteryOther5 = 296u,
        LightWeaponsMasteryOther6 = 297u,
        LightWeaponsMasterySelf1 = 298u,
        LightWeaponsMasterySelf2 = 299u,
        LightWeaponsMasterySelf3 = 300u,
        LightWeaponsMasterySelf4 = 301u,
        LightWeaponsMasterySelf5 = 302u,
        LightWeaponsMasterySelf6 = 303u,
        LightWeaponsIneptitudeOther1 = 304u,
        LightWeaponsIneptitudeOther2 = 305u,
        LightWeaponsIneptitudeOther3 = 306u,
        LightWeaponsIneptitudeOther4 = 307u,
        LightWeaponsIneptitudeOther5 = 308u,
        LightWeaponsIneptitudeOther6 = 309u,
        LightWeaponsIneptitudeSelf1 = 310u,
        LightWeaponsIneptitudeSelf2 = 311u,
        LightWeaponsIneptitudeSelf3 = 312u,
        LightWeaponsIneptitudeSelf4 = 313u,
        LightWeaponsIneptitudeSelf5 = 314u,
        LightWeaponsIneptitudeSelf6 = 315u,
        FinesseWeaponsMasteryOther1 = 316u,
        FinesseWeaponsMasteryOther2 = 317u,
        FinesseWeaponsMasteryOther3 = 318u,
        FinesseWeaponsMasteryOther4 = 319u,
        FinesseWeaponsMasteryOther5 = 320u,
        FinesseWeaponsMasteryOther6 = 321u,
        FinesseWeaponsMasterySelf1 = 322u,
        FinesseWeaponsMasterySelf2 = 323u,
        FinesseWeaponsMasterySelf3 = 324u,
        FinesseWeaponsMasterySelf4 = 325u,
        FinesseWeaponsMasterySelf5 = 326u,
        FinesseWeaponsMasterySelf6 = 327u,
        FinesseWeaponsIneptitudeOther1 = 328u,
        FinesseWeaponsIneptitudeOther2 = 329u,
        FinesseWeaponsIneptitudeOther3 = 330u,
        FinesseWeaponsIneptitudeOther4 = 331u,
        FinesseWeaponsIneptitudeOther5 = 332u,
        FinesseWeaponsIneptitudeOther6 = 333u,
        FinesseWeaponsIneptitudeSelf1 = 334u,
        FinesseWeaponsIneptitudeSelf2 = 335u,
        FinesseWeaponsIneptitudeSelf3 = 336u,
        FinesseWeaponsIneptitudeSelf4 = 337u,
        FinesseWeaponsIneptitudeSelf5 = 338u,
        FinesseWeaponsIneptitudeSelf6 = 339u,
        MaceMasteryOther1 = 340u,
        MaceMasteryOther2 = 341u,
        MaceMasteryOther3 = 342u,
        MaceMasteryOther4 = 343u,
        MaceMasteryOther5 = 344u,
        MaceMasteryOther6 = 345u,
        MaceMasterySelf1 = 346u,
        MaceMasterySelf2 = 347u,
        MaceMasterySelf3 = 348u,
        MaceMasterySelf4 = 349u,
        MaceMasterySelf5 = 350u,
        MaceMasterySelf6 = 351u,
        MaceIneptitudeOther1 = 352u,
        MaceIneptitudeOther2 = 353u,
        MaceIneptitudeOther3 = 354u,
        MaceIneptitudeOther4 = 355u,
        MaceIneptitudeOther5 = 356u,
        MaceIneptitudeOther6 = 357u,
        MaceIneptitudeSelf1 = 358u,
        MaceIneptitudeSelf2 = 359u,
        MaceIneptitudeSelf3 = 360u,
        MaceIneptitudeSelf4 = 361u,
        MaceIneptitudeSelf5 = 362u,
        MaceIneptitudeSelf6 = 363u,
        SpearMasteryOther1 = 364u,
        SpearMasteryOther2 = 365u,
        SpearMasteryOther3 = 366u,
        SpearMasteryOther4 = 367u,
        SpearMasteryOther5 = 368u,
        SpearMasteryOther6 = 369u,
        SpearMasterySelf1 = 370u,
        SpearMasterySelf2 = 371u,
        SpearMasterySelf3 = 372u,
        SpearMasterySelf4 = 373u,
        SpearMasterySelf5 = 374u,
        SpearMasterySelf6 = 375u,
        SpearIneptitudeOther1 = 376u,
        SpearIneptitudeOther2 = 377u,
        SpearIneptitudeOther3 = 378u,
        SpearIneptitudeOther4 = 379u,
        SpearIneptitudeOther5 = 380u,
        SpearIneptitudeOther6 = 381u,
        SpearIneptitudeSelf1 = 382u,
        SpearIneptitudeSelf2 = 383u,
        SpearIneptitudeSelf3 = 384u,
        SpearIneptitudeSelf4 = 385u,
        SpearIneptitudeSelf5 = 386u,
        SpearIneptitudeSelf6 = 387u,
        StaffMasteryOther1 = 388u,
        StaffMasteryOther2 = 389u,
        StaffMasteryOther3 = 390u,
        StaffMasteryOther4 = 391u,
        StaffMasteryOther5 = 392u,
        StaffMasteryOther6 = 393u,
        StaffMasterySelf1 = 394u,
        StaffMasterySelf2 = 395u,
        StaffMasterySelf3 = 396u,
        StaffMasterySelf4 = 397u,
        StaffMasterySelf5 = 398u,
        StaffMasterySelf6 = 399u,
        StaffIneptitudeOther1 = 400u,
        StaffIneptitudeOther2 = 401u,
        StaffIneptitudeOther3 = 402u,
        StaffIneptitudeOther4 = 403u,
        StaffIneptitudeOther5 = 404u,
        StaffIneptitudeOther6 = 405u,
        StaffIneptitudeSelf1 = 406u,
        StaffIneptitudeSelf2 = 407u,
        StaffIneptitudeSelf3 = 408u,
        StaffIneptitudeSelf4 = 409u,
        StaffIneptitudeSelf5 = 410u,
        StaffIneptitudeSelf6 = 411u,
        HeavyWeaponsMasteryOther1 = 412u,
        HeavyWeaponsMasteryOther2 = 413u,
        HeavyWeaponsMasteryOther3 = 414u,
        HeavyWeaponsMasteryOther4 = 415u,
        HeavyWeaponsMasteryOther5 = 416u,
        HeavyWeaponsMasteryOther6 = 417u,
        HeavyWeaponsMasterySelf1 = 418u,
        HeavyWeaponsMasterySelf2 = 419u,
        HeavyWeaponsMasterySelf3 = 420u,
        HeavyWeaponsMasterySelf4 = 421u,
        HeavyWeaponsMasterySelf5 = 422u,
        HeavyWeaponsMasterySelf6 = 423u,
        HeavyWeaponsIneptitudeOther1 = 424u,
        HeavyWeaponsIneptitudeOther2 = 425u,
        HeavyWeaponsIneptitudeOther3 = 426u,
        HeavyWeaponsIneptitudeOther4 = 427u,
        HeavyWeaponsIneptitudeOther5 = 428u,
        HeavyWeaponsIneptitudeOther6 = 429u,
        HeavyWeaponsIneptitudeSelf1 = 430u,
        HeavyWeaponsIneptitudeSelf2 = 431u,
        HeavyWeaponsIneptitudeSelf3 = 432u,
        HeavyWeaponsIneptitudeSelf4 = 433u,
        UNKNOWN__GUESSEDNAME434 = 434u,
        HeavyWeaponsIneptitudeSelf5 = 435u,
        HeavyWeaponsIneptitudeSelf6 = 436u,
        UnarmedCombatMasteryOther1 = 437u,
        UnarmedCombatMasteryOther2 = 438u,
        UnarmedCombatMasteryOther3 = 439u,
        UnarmedCombatMasteryOther4 = 440u,
        UnarmedCombatMasteryOther5 = 441u,
        UnarmedCombatMasteryOther6 = 442u,
        UnarmedCombatMasterySelf1 = 443u,
        UnarmedCombatMasterySelf2 = 444u,
        UnarmedCombatMasterySelf3 = 445u,
        UnarmedCombatMasterySelf4 = 446u,
        UnarmedCombatMasterySelf5 = 447u,
        UnarmedCombatMasterySelf6 = 448u,
        UnarmedCombatIneptitudeOther1 = 449u,
        UnarmedCombatIneptitudeOther2 = 450u,
        UnarmedCombatIneptitudeOther3 = 451u,
        UnarmedCombatIneptitudeOther4 = 452u,
        UnarmedCombatIneptitudeOther5 = 453u,
        UnarmedCombatIneptitudeOther6 = 454u,
        UnarmedCombatIneptitudeSelf1 = 455u,
        UnarmedCombatIneptitudeSelf2 = 456u,
        UnarmedCombatIneptitudeSelf3 = 457u,
        UnarmedCombatIneptitudeSelf4 = 458u,
        UnarmedCombatIneptitudeSelf5 = 459u,
        UnarmedCombatIneptitudeSelf6 = 460u,
        MissileWeaponsMasteryOther1 = 461u,
        MissileWeaponsMasteryOther2 = 462u,
        MissileWeaponsMasteryOther3 = 463u,
        MissileWeaponsMasteryOther4 = 464u,
        MissileWeaponsMasteryOther5 = 465u,
        MissileWeaponsMasteryOther6 = 466u,
        MissileWeaponsMasterySelf1 = 467u,
        MissileWeaponsMasterySelf2 = 468u,
        MissileWeaponsMasterySelf3 = 469u,
        MissileWeaponsMasterySelf4 = 470u,
        MissileWeaponsMasterySelf5 = 471u,
        MissileWeaponsMasterySelf6 = 472u,
        MissileWeaponsIneptitudeOther1 = 473u,
        MissileWeaponsIneptitudeOther2 = 474u,
        MissileWeaponsIneptitudeOther3 = 475u,
        MissileWeaponsIneptitudeOther4 = 476u,
        MissileWeaponsIneptitudeOther5 = 477u,
        MissileWeaponsIneptitudeOther6 = 478u,
        MissileWeaponsIneptitudeSelf1 = 479u,
        MissileWeaponsIneptitudeSelf2 = 480u,
        MissileWeaponsIneptitudeSelf3 = 481u,
        MissileWeaponsIneptitudeSelf4 = 482u,
        MissileWeaponsIneptitudeSelf5 = 483u,
        MissileWeaponsIneptitudeSelf6 = 484u,
        CrossbowMasteryOther1 = 485u,
        CrossbowMasteryOther2 = 486u,
        CrossbowMasteryOther3 = 487u,
        CrossbowMasteryOther4 = 488u,
        CrossbowMasteryOther5 = 489u,
        CrossbowMasteryOther6 = 490u,
        CrossbowMasterySelf1 = 491u,
        CrossbowMasterySelf2 = 492u,
        CrossbowMasterySelf3 = 493u,
        CrossbowMasterySelf4 = 494u,
        CrossbowMasterySelf5 = 495u,
        CrossbowMasterySelf6 = 496u,
        CrossbowIneptitudeOther1 = 497u,
        CrossbowIneptitudeOther2 = 498u,
        CrossbowIneptitudeOther3 = 499u,
        CrossbowIneptitudeOther4 = 500u,
        CrossbowIneptitudeOther5 = 501u,
        CrossbowIneptitudeOther6 = 502u,
        CrossbowIneptitudeSelf1 = 503u,
        CrossbowIneptitudeSelf2 = 504u,
        CrossbowIneptitudeSelf3 = 505u,
        CrossbowIneptitudeSelf4 = 506u,
        CrossbowIneptitudeSelf5 = 507u,
        CrossbowIneptitudeSelf6 = 508u,
        AcidProtectionOther1 = 509u,
        AcidProtectionOther2 = 510u,
        AcidProtectionOther3 = 0x1FFu,
        AcidProtectionOther4 = 0x200u,
        AcidProtectionOther5 = 513u,
        AcidProtectionOther6 = 514u,
        AcidProtectionSelf1 = 515u,
        AcidProtectionSelf2 = 516u,
        AcidProtectionSelf3 = 517u,
        AcidProtectionSelf4 = 518u,
        AcidProtectionSelf5 = 519u,
        AcidProtectionSelf6 = 520u,
        AcidVulnerabilityOther1 = 521u,
        AcidVulnerabilityOther2 = 522u,
        AcidVulnerabilityOther3 = 523u,
        AcidVulnerabilityOther4 = 524u,
        AcidVulnerabilityOther5 = 525u,
        AcidVulnerabilityOther6 = 526u,
        AcidVulnerabilitySelf1 = 527u,
        AcidVulnerabilitySelf2 = 528u,
        AcidVulnerabilitySelf3 = 529u,
        AcidVulnerabilitySelf4 = 530u,
        AcidVulnerabilitySelf5 = 531u,
        AcidVulnerabilitySelf6 = 532u,
        ThrownWeaponMasteryOther1 = 533u,
        ThrownWeaponMasteryOther2 = 534u,
        ThrownWeaponMasteryOther3 = 535u,
        ThrownWeaponMasteryOther4 = 536u,
        ThrownWeaponMasteryOther5 = 537u,
        ThrownWeaponMasteryOther6 = 538u,
        ThrownWeaponMasterySelf1 = 539u,
        ThrownWeaponMasterySelf2 = 540u,
        ThrownWeaponMasterySelf3 = 541u,
        ThrownWeaponMasterySelf4 = 542u,
        ThrownWeaponMasterySelf5 = 543u,
        ThrownWeaponMasterySelf6 = 544u,
        ThrownWeaponIneptitudeOther1 = 545u,
        ThrownWeaponIneptitudeOther2 = 546u,
        ThrownWeaponIneptitudeOther3 = 547u,
        ThrownWeaponIneptitudeOther4 = 548u,
        ThrownWeaponIneptitudeOther5 = 549u,
        ThrownWeaponIneptitudeOther6 = 550u,
        ThrownWeaponIneptitudeSelf1 = 551u,
        ThrownWeaponIneptitudeSelf2 = 552u,
        ThrownWeaponIneptitudeSelf3 = 553u,
        ThrownWeaponIneptitudeSelf4 = 554u,
        ThrownWeaponIneptitudeSelf5 = 555u,
        ThrownWeaponIneptitudeSelf6 = 556u,
        CreatureEnchantmentMasterySelf1 = 557u,
        CreatureEnchantmentMasterySelf2 = 558u,
        CreatureEnchantmentMasterySelf3 = 559u,
        CreatureEnchantmentMasterySelf4 = 560u,
        CreatureEnchantmentMasterySelf5 = 561u,
        CreatureEnchantmentMasterySelf6 = 562u,
        CreatureEnchantmentMasteryOther1 = 563u,
        CreatureEnchantmentMasteryOther2 = 564u,
        CreatureEnchantmentMasteryOther3 = 565u,
        CreatureEnchantmentMasteryOther4 = 566u,
        CreatureEnchantmentMasteryOther5 = 567u,
        CreatureEnchantmentMasteryOther6 = 568u,
        CreatureEnchantmentIneptitudeOther1 = 569u,
        CreatureEnchantmentIneptitudeOther2 = 570u,
        CreatureEnchantmentIneptitudeOther3 = 571u,
        CreatureEnchantmentIneptitudeOther4 = 572u,
        CreatureEnchantmentIneptitudeOther5 = 573u,
        CreatureEnchantmentIneptitudeOther6 = 574u,
        CreatureEnchantmentIneptitudeSelf1 = 575u,
        CreatureEnchantmentIneptitudeSelf2 = 576u,
        CreatureEnchantmentIneptitudeSelf3 = 577u,
        CreatureEnchantmentIneptitudeSelf4 = 578u,
        CreatureEnchantmentIneptitudeSelf5 = 579u,
        CreatureEnchantmentIneptitudeSelf6 = 580u,
        ItemEnchantmentMasterySelf1 = 581u,
        ItemEnchantmentMasterySelf2 = 582u,
        ItemEnchantmentMasterySelf3 = 583u,
        ItemEnchantmentMasterySelf4 = 584u,
        ItemEnchantmentMasterySelf5 = 585u,
        ItemEnchantmentMasterySelf6 = 586u,
        ItemEnchantmentMasteryOther1 = 587u,
        ItemEnchantmentMasteryOther2 = 588u,
        ItemEnchantmentMasteryOther3 = 589u,
        ItemEnchantmentMasteryOther4 = 590u,
        ItemEnchantmentMasteryOther5 = 591u,
        ItemEnchantmentMasteryOther6 = 592u,
        ItemEnchantmentIneptitudeOther1 = 593u,
        ItemEnchantmentIneptitudeOther2 = 594u,
        ItemEnchantmentIneptitudeOther3 = 595u,
        ItemEnchantmentIneptitudeOther4 = 596u,
        ItemEnchantmentIneptitudeOther5 = 597u,
        ItemEnchantmentIneptitudeOther6 = 598u,
        ItemEnchantmentIneptitudeSelf1 = 599u,
        ItemEnchantmentIneptitudeSelf2 = 600u,
        ItemEnchantmentIneptitudeSelf3 = 601u,
        ItemEnchantmentIneptitudeSelf4 = 602u,
        ItemEnchantmentIneptitudeSelf5 = 603u,
        ItemEnchantmentIneptitudeSelf6 = 604u,
        LifeMagicMasterySelf1 = 605u,
        LifeMagicMasterySelf2 = 606u,
        LifeMagicMasterySelf3 = 607u,
        LifeMagicMasterySelf4 = 608u,
        LifeMagicMasterySelf5 = 609u,
        LifeMagicMasterySelf6 = 610u,
        LifeMagicMasteryOther1 = 611u,
        LifeMagicMasteryOther2 = 612u,
        LifeMagicMasteryOther3 = 613u,
        LifeMagicMasteryOther4 = 614u,
        LifeMagicMasteryOther5 = 615u,
        LifeMagicMasteryOther6 = 616u,
        LifeMagicIneptitudeSelf1 = 617u,
        LifeMagicIneptitudeSelf2 = 618u,
        LifeMagicIneptitudeSelf3 = 619u,
        LifeMagicIneptitudeSelf4 = 620u,
        LifeMagicIneptitudeSelf5 = 621u,
        LifeMagicIneptitudeSelf6 = 622u,
        LifeMagicIneptitudeOther1 = 623u,
        LifeMagicIneptitudeOther2 = 624u,
        LifeMagicIneptitudeOther3 = 625u,
        LifeMagicIneptitudeOther4 = 626u,
        LifeMagicIneptitudeOther5 = 627u,
        LifeMagicIneptitudeOther6 = 628u,
        WarMagicMasterySelf1 = 629u,
        WarMagicMasterySelf2 = 630u,
        WarMagicMasterySelf3 = 631u,
        WarMagicMasterySelf4 = 632u,
        WarMagicMasterySelf5 = 633u,
        WarMagicMasterySelf6 = 634u,
        WarMagicMasteryOther1 = 635u,
        WarMagicMasteryOther2 = 636u,
        WarMagicMasteryOther3 = 637u,
        WarMagicMasteryOther4 = 638u,
        WarMagicMasteryOther5 = 639u,
        WarMagicMasteryOther6 = 640u,
        WarMagicIneptitudeSelf1 = 641u,
        WarMagicIneptitudeSelf2 = 642u,
        WarMagicIneptitudeSelf3 = 643u,
        WarMagicIneptitudeSelf4 = 644u,
        WarMagicIneptitudeSelf5 = 645u,
        WarMagicIneptitudeSelf6 = 646u,
        WarMagicIneptitudeOther1 = 647u,
        WarMagicIneptitudeOther2 = 648u,
        WarMagicIneptitudeOther3 = 649u,
        WarMagicIneptitudeOther4 = 650u,
        WarMagicIneptitudeOther5 = 651u,
        WarMagicIneptitudeOther6 = 652u,
        ManaMasterySelf1 = 653u,
        ManaMasterySelf2 = 654u,
        ManaMasterySelf3 = 655u,
        ManaMasterySelf4 = 656u,
        ManaMasterySelf5 = 657u,
        ManaMasterySelf6 = 658u,
        ManaMasteryOther1 = 659u,
        ManaMasteryOther2 = 660u,
        ManaMasteryOther3 = 661u,
        ManaMasteryOther4 = 662u,
        ManaMasteryOther5 = 663u,
        ManaMasteryOther6 = 664u,
        ManaIneptitudeSelf1 = 665u,
        Vitae = 666u,
        ManaIneptitudeSelf2 = 667u,
        ManaIneptitudeSelf3 = 668u,
        ManaIneptitudeSelf4 = 669u,
        ManaIneptitudeSelf5 = 670u,
        ManaIneptitudeSelf6 = 671u,
        ManaIneptitudeOther1 = 672u,
        ManaIneptitudeOther2 = 673u,
        ManaIneptitudeOther3 = 674u,
        ManaIneptitudeOther4 = 675u,
        ManaIneptitudeOther5 = 676u,
        ManaIneptitudeOther6 = 677u,
        ArcaneEnlightenmentSelf1 = 678u,
        ArcaneEnlightenmentSelf2 = 679u,
        ArcaneEnlightenmentSelf3 = 680u,
        ArcaneEnlightenmentSelf4 = 681u,
        ArcaneEnlightenmentSelf5 = 682u,
        ArcaneEnlightenmentSelf6 = 683u,
        ArcaneEnlightenmentOther1 = 684u,
        ArcaneEnlightenmentOther2 = 685u,
        ArcaneEnlightenmentOther3 = 686u,
        ArcaneEnlightenmentOther4 = 687u,
        ArcaneEnlightenmentOther5 = 688u,
        ArcaneEnlightenmentOther6 = 689u,
        ArcaneBenightednessSelf1 = 690u,
        ArcaneBenightednessSelf2 = 691u,
        ArcaneBenightednessSelf3 = 692u,
        ArcaneBenightednessSelf4 = 693u,
        ArcaneBenightednessSelf5 = 694u,
        ArcaneBenightednessSelf6 = 695u,
        ArcaneBenightednessOther1 = 696u,
        ArcaneBenightednessOther2 = 697u,
        ArcaneBenightednessOther3 = 698u,
        ArcaneBenightednessOther4 = 699u,
        ArcaneBenightednessOther5 = 700u,
        ArcaneBenightednessOther6 = 701u,
        ArmorExpertiseSelf1 = 702u,
        ArmorExpertiseSelf2 = 703u,
        ArmorExpertiseSelf3 = 704u,
        ArmorExpertiseSelf4 = 705u,
        ArmorExpertiseSelf5 = 706u,
        ArmorExpertiseSelf6 = 707u,
        ArmorExpertiseOther1 = 708u,
        ArmorExpertiseOther2 = 709u,
        ArmorExpertiseOther3 = 710u,
        ArmorExpertiseOther4 = 711u,
        ArmorExpertiseOther5 = 712u,
        ArmorExpertiseOther6 = 713u,
        ArmorIgnoranceSelf1 = 714u,
        ArmorIgnoranceSelf2 = 715u,
        ArmorIgnoranceSelf3 = 716u,
        ArmorIgnoranceSelf4 = 717u,
        ArmorIgnoranceSelf5 = 718u,
        ArmorIgnoranceSelf6 = 719u,
        ArmorIgnoranceOther1 = 720u,
        ArmorIgnoranceOther2 = 721u,
        ArmorIgnoranceOther3 = 722u,
        ArmorIgnoranceOther4 = 723u,
        ArmorIgnoranceOther5 = 724u,
        ArmorIgnoranceOther6 = 725u,
        ItemExpertiseSelf1 = 726u,
        ItemExpertiseSelf2 = 727u,
        ItemExpertiseSelf3 = 728u,
        ItemExpertiseSelf4 = 729u,
        ItemExpertiseSelf5 = 730u,
        ItemExpertiseSelf6 = 731u,
        ItemExpertiseOther1 = 732u,
        ItemExpertiseOther2 = 733u,
        ItemExpertiseOther3 = 734u,
        ItemExpertiseOther4 = 735u,
        ItemExpertiseOther5 = 736u,
        ItemExpertiseOther6 = 737u,
        ItemIgnoranceSelf1 = 738u,
        ItemIgnoranceSelf2 = 739u,
        ItemIgnoranceSelf3 = 740u,
        ItemIgnoranceSelf4 = 741u,
        ItemIgnoranceSelf5 = 742u,
        ItemIgnoranceSelf6 = 743u,
        ItemIgnoranceOther1 = 744u,
        ItemIgnoranceOther2 = 745u,
        ItemIgnoranceOther3 = 746u,
        ItemIgnoranceOther4 = 747u,
        ItemIgnoranceOther5 = 748u,
        ItemIgnoranceOther6 = 749u,
        MagicItemExpertiseSelf1 = 750u,
        MagicItemExpertiseSelf2 = 751u,
        MagicItemExpertiseSelf3 = 752u,
        MagicItemExpertiseSelf4 = 753u,
        MagicItemExpertiseSelf5 = 754u,
        MagicItemExpertiseSelf6 = 755u,
        MagicItemExpertiseOther1 = 756u,
        MagicItemExpertiseOther2 = 757u,
        MagicItemExpertiseOther3 = 758u,
        MagicItemExpertiseOther4 = 759u,
        MagicItemExpertiseOther5 = 760u,
        MagicItemExpertiseOther6 = 761u,
        MagicItemIgnoranceSelf1 = 762u,
        MagicItemIgnoranceSelf2 = 763u,
        MagicItemIgnoranceSelf3 = 764u,
        MagicItemIgnoranceSelf4 = 765u,
        MagicItemIgnoranceSelf5 = 766u,
        MagicItemIgnoranceSelf6 = 767u,
        MagicItemIgnoranceOther1 = 768u,
        MagicItemIgnoranceOther2 = 769u,
        MagicItemIgnoranceOther3 = 770u,
        MagicItemIgnoranceOther4 = 771u,
        MagicItemIgnoranceOther5 = 772u,
        MagicItemIgnoranceOther6 = 773u,
        WeaponExpertiseSelf1 = 774u,
        WeaponExpertiseSelf2 = 775u,
        WeaponExpertiseSelf3 = 776u,
        WeaponExpertiseSelf4 = 777u,
        WeaponExpertiseSelf5 = 778u,
        WeaponExpertiseSelf6 = 779u,
        WeaponExpertiseOther1 = 780u,
        WeaponExpertiseOther2 = 781u,
        WeaponExpertiseOther3 = 782u,
        WeaponExpertiseOther4 = 783u,
        WeaponExpertiseOther5 = 784u,
        WeaponExpertiseOther6 = 785u,
        WeaponIgnoranceSelf1 = 786u,
        WeaponIgnoranceSelf2 = 787u,
        WeaponIgnoranceSelf3 = 788u,
        WeaponIgnoranceSelf4 = 789u,
        WeaponIgnoranceSelf5 = 790u,
        WeaponIgnoranceSelf6 = 791u,
        WeaponIgnoranceOther1 = 792u,
        WeaponIgnoranceOther2 = 793u,
        WeaponIgnoranceOther3 = 794u,
        WeaponIgnoranceOther4 = 795u,
        WeaponIgnoranceOther5 = 796u,
        WeaponIgnoranceOther6 = 797u,
        MonsterAttunementSelf1 = 798u,
        MonsterAttunementSelf2 = 799u,
        MonsterAttunementSelf3 = 800u,
        MonsterAttunementSelf4 = 801u,
        MonsterAttunementSelf5 = 802u,
        MonsterAttunementSelf6 = 803u,
        MonsterAttunementOther1 = 804u,
        MonsterAttunementOther2 = 805u,
        MonsterAttunementOther3 = 806u,
        MonsterAttunementOther4 = 807u,
        MonsterAttunementOther5 = 808u,
        MonsterAttunementOther6 = 809u,
        FireProtectionOther2 = 810u,
        MonsterUnfamiliaritySelf1 = 811u,
        MonsterUnfamiliaritySelf2 = 812u,
        MonsterUnfamiliaritySelf3 = 813u,
        MonsterUnfamiliaritySelf4 = 814u,
        MonsterUnfamiliaritySelf5 = 815u,
        MonsterUnfamiliaritySelf6 = 816u,
        MonsterUnfamiliarityOther1 = 817u,
        MonsterUnfamiliarityOther2 = 818u,
        MonsterUnfamiliarityOther3 = 819u,
        MonsterUnfamiliarityOther4 = 820u,
        MonsterUnfamiliarityOther5 = 821u,
        MonsterUnfamiliarityOther6 = 822u,
        UNKNOWN__GUESSEDNAME823 = 823u,
        PersonAttunementSelf1 = 824u,
        PersonAttunementSelf2 = 825u,
        PersonAttunementSelf3 = 826u,
        PersonAttunementSelf4 = 827u,
        PersonAttunementSelf5 = 828u,
        PersonAttunementSelf6 = 829u,
        PersonAttunementOther1 = 830u,
        PersonAttunementOther2 = 831u,
        PersonAttunementOther3 = 832u,
        PersonAttunementOther4 = 833u,
        PersonAttunementOther5 = 834u,
        PersonAttunementOther6 = 835u,
        FireProtectionOther3 = 836u,
        PersonUnfamiliaritySelf1 = 837u,
        PersonUnfamiliaritySelf2 = 838u,
        PersonUnfamiliaritySelf3 = 839u,
        PersonUnfamiliaritySelf4 = 840u,
        PersonUnfamiliaritySelf5 = 841u,
        PersonUnfamiliaritySelf6 = 842u,
        PersonUnfamiliarityOther1 = 843u,
        PersonUnfamiliarityOther2 = 844u,
        PersonUnfamiliarityOther3 = 845u,
        PersonUnfamiliarityOther4 = 846u,
        PersonUnfamiliarityOther5 = 847u,
        PersonUnfamiliarityOther6 = 848u,
        FireProtectionOther4 = 849u,
        DeceptionMasterySelf1 = 850u,
        DeceptionMasterySelf2 = 851u,
        DeceptionMasterySelf3 = 852u,
        DeceptionMasterySelf4 = 853u,
        DeceptionMasterySelf5 = 854u,
        DeceptionMasterySelf6 = 855u,
        DeceptionMasteryOther1 = 856u,
        DeceptionMasteryOther2 = 857u,
        DeceptionMasteryOther3 = 858u,
        DeceptionMasteryOther4 = 859u,
        DeceptionMasteryOther5 = 860u,
        DeceptionMasteryOther6 = 861u,
        DeceptionIneptitudeSelf1 = 862u,
        DeceptionIneptitudeSelf2 = 863u,
        DeceptionIneptitudeSelf3 = 864u,
        DeceptionIneptitudeSelf4 = 865u,
        DeceptionIneptitudeSelf5 = 866u,
        DeceptionIneptitudeSelf6 = 867u,
        DeceptionIneptitudeOther1 = 868u,
        DeceptionIneptitudeOther2 = 869u,
        DeceptionIneptitudeOther3 = 870u,
        DeceptionIneptitudeOther4 = 871u,
        DeceptionIneptitudeOther5 = 872u,
        DeceptionIneptitudeOther6 = 873u,
        HealingMasterySelf1 = 874u,
        HealingMasterySelf2 = 875u,
        HealingMasterySelf3 = 876u,
        HealingMasterySelf4 = 877u,
        HealingMasterySelf5 = 878u,
        HealingMasterySelf6 = 879u,
        HealingMasteryOther1 = 880u,
        HealingMasteryOther2 = 881u,
        HealingMasteryOther3 = 882u,
        HealingMasteryOther4 = 883u,
        HealingMasteryOther5 = 884u,
        HealingMasteryOther6 = 885u,
        HealingIneptitudeSelf1 = 886u,
        HealingIneptitudeSelf2 = 887u,
        HealingIneptitudeSelf3 = 888u,
        HealingIneptitudeSelf4 = 889u,
        HealingIneptitudeSelf5 = 890u,
        HealingIneptitudeSelf6 = 891u,
        HealingIneptitudeOther1 = 892u,
        HealingIneptitudeOther2 = 893u,
        HealingIneptitudeOther3 = 894u,
        HealingIneptitudeOther4 = 895u,
        HealingIneptitudeOther5 = 896u,
        HealingIneptitudeOther6 = 897u,
        LeadershipMasterySelf1 = 898u,
        LeadershipMasterySelf2 = 899u,
        LeadershipMasterySelf3 = 900u,
        LeadershipMasterySelf4 = 901u,
        LeadershipMasterySelf5 = 902u,
        LeadershipMasterySelf6 = 903u,
        LeadershipMasteryOther1 = 904u,
        LeadershipMasteryOther2 = 905u,
        LeadershipMasteryOther3 = 906u,
        LeadershipMasteryOther4 = 907u,
        LeadershipMasteryOther5 = 908u,
        LeadershipMasteryOther6 = 909u,
        LeadershipIneptitudeSelf1 = 910u,
        LeadershipIneptitudeSelf2 = 911u,
        LeadershipIneptitudeSelf3 = 912u,
        LeadershipIneptitudeSelf4 = 913u,
        LeadershipIneptitudeSelf5 = 914u,
        LeadershipIneptitudeSelf6 = 915u,
        LeadershipIneptitudeOther1 = 916u,
        LeadershipIneptitudeOther2 = 917u,
        LeadershipIneptitudeOther3 = 918u,
        LeadershipIneptitudeOther4 = 919u,
        LeadershipIneptitudeOther5 = 920u,
        LeadershipIneptitudeOther6 = 921u,
        LockpickMasterySelf1 = 922u,
        LockpickMasterySelf2 = 923u,
        LockpickMasterySelf3 = 924u,
        LockpickMasterySelf4 = 925u,
        LockpickMasterySelf5 = 926u,
        LockpickMasterySelf6 = 927u,
        LockpickMasteryOther1 = 928u,
        LockpickMasteryOther2 = 929u,
        LockpickMasteryOther3 = 930u,
        LockpickMasteryOther4 = 931u,
        LockpickMasteryOther5 = 932u,
        LockpickMasteryOther6 = 933u,
        LockpickIneptitudeSelf1 = 934u,
        LockpickIneptitudeSelf2 = 935u,
        LockpickIneptitudeSelf3 = 936u,
        LockpickIneptitudeSelf4 = 937u,
        LockpickIneptitudeSelf5 = 938u,
        LockpickIneptitudeSelf6 = 939u,
        LockpickIneptitudeOther1 = 940u,
        LockpickIneptitudeOther2 = 941u,
        LockpickIneptitudeOther3 = 942u,
        LockpickIneptitudeOther4 = 943u,
        LockpickIneptitudeOther5 = 944u,
        LockpickIneptitudeOther6 = 945u,
        FealtySelf1 = 946u,
        FealtySelf2 = 947u,
        FealtySelf3 = 948u,
        FealtySelf4 = 949u,
        FealtySelf5 = 950u,
        FealtySelf6 = 951u,
        FealtyOther1 = 952u,
        FealtyOther2 = 953u,
        FealtyOther3 = 954u,
        FealtyOther4 = 955u,
        FealtyOther5 = 956u,
        FealtyOther6 = 957u,
        FaithlessnessSelf1 = 958u,
        FaithlessnessSelf2 = 959u,
        FaithlessnessSelf3 = 960u,
        FaithlessnessSelf4 = 961u,
        FaithlessnessSelf5 = 962u,
        FaithlessnessSelf6 = 963u,
        FaithlessnessOther1 = 964u,
        FaithlessnessOther2 = 965u,
        FaithlessnessOther3 = 966u,
        FaithlessnessOther4 = 967u,
        FaithlessnessOther5 = 968u,
        FaithlessnessOther6 = 969u,
        JumpingMasterySelf1 = 970u,
        JumpingMasterySelf2 = 971u,
        JumpingMasterySelf3 = 972u,
        JumpingMasterySelf4 = 973u,
        JumpingMasterySelf5 = 974u,
        JumpingMasterySelf6 = 975u,
        JumpingMasteryOther1 = 976u,
        JumpingMasteryOther2 = 977u,
        JumpingMasteryOther3 = 978u,
        JumpingMasteryOther4 = 979u,
        JumpingMasteryOther5 = 980u,
        JumpingMasteryOther6 = 981u,
        SprintSelf1 = 982u,
        SprintSelf2 = 983u,
        SprintSelf3 = 984u,
        SprintSelf4 = 985u,
        SprintSelf5 = 986u,
        SprintSelf6 = 987u,
        SprintOther1 = 988u,
        SprintOther2 = 989u,
        SprintOther3 = 990u,
        SprintOther4 = 991u,
        SprintOther5 = 992u,
        SprintOther6 = 993u,
        LeadenFeetSelf1 = 994u,
        LeadenFeetSelf2 = 995u,
        LeadenFeetSelf3 = 996u,
        LeadenFeetSelf4 = 997u,
        LeadenFeetSelf5 = 998u,
        LeadenFeetSelf6 = 999u,
        LeadenFeetOther1 = 1000u,
        LeadenFeetOther2 = 1001u,
        LeadenFeetOther3 = 1002u,
        LeadenFeetOther4 = 1003u,
        LeadenFeetOther5 = 1004u,
        LeadenFeetOther6 = 1005u,
        JumpingIneptitudeSelf1 = 1006u,
        JumpingIneptitudeSelf2 = 1007u,
        JumpingIneptitudeSelf3 = 1008u,
        JumpingIneptitudeSelf4 = 1009u,
        JumpingIneptitudeSelf5 = 1010u,
        JumpingIneptitudeSelf6 = 1011u,
        JumpingIneptitudeOther1 = 1012u,
        JumpingIneptitudeOther2 = 1013u,
        JumpingIneptitudeOther3 = 1014u,
        JumpingIneptitudeOther4 = 1015u,
        JumpingIneptitudeOther5 = 1016u,
        JumpingIneptitudeOther6 = 1017u,
        BludgeonProtectionSelf1 = 1018u,
        BludgeonProtectionSelf2 = 1019u,
        BludgeonProtectionSelf3 = 1020u,
        BludgeonProtectionSelf4 = 1021u,
        BludgeonProtectionSelf5 = 1022u,
        BludgeonProtectionSelf6 = 0x3FFu,
        BludgeonProtectionOther1 = 0x400u,
        BludgeonProtectionOther2 = 1025u,
        BludgeonProtectionOther3 = 1026u,
        BludgeonProtectionOther4 = 1027u,
        BludgeonProtectionOther5 = 1028u,
        BludgeonProtectionOther6 = 1029u,
        ColdProtectionSelf1 = 1030u,
        ColdProtectionSelf2 = 1031u,
        ColdProtectionSelf3 = 1032u,
        ColdProtectionSelf4 = 1033u,
        ColdProtectionSelf5 = 1034u,
        ColdProtectionSelf6 = 1035u,
        ColdProtectionOther1 = 1036u,
        ColdProtectionOther2 = 1037u,
        ColdProtectionOther3 = 1038u,
        ColdProtectionOther4 = 1039u,
        ColdProtectionOther5 = 1040u,
        ColdProtectionOther6 = 1041u,
        BludgeonVulnerabilitySelf1 = 1042u,
        BludgeonVulnerabilitySelf2 = 1043u,
        BludgeonVulnerabilitySelf3 = 1044u,
        BludgeonVulnerabilitySelf4 = 1045u,
        BludgeonVulnerabilitySelf5 = 1046u,
        BludgeonVulnerabilitySelf6 = 1047u,
        BludgeonVulnerabilityOther1 = 1048u,
        BludgeonVulnerabilityOther2 = 1049u,
        BludgeonVulnerabilityOther3 = 1050u,
        BludgeonVulnerabilityOther4 = 1051u,
        BludgeonVulnerabilityOther5 = 1052u,
        BludgeonVulnerabilityOther6 = 1053u,
        ColdVulnerabilitySelf1 = 1054u,
        ColdVulnerabilitySelf2 = 1055u,
        ColdVulnerabilitySelf3 = 1056u,
        ColdVulnerabilitySelf4 = 1057u,
        ColdVulnerabilitySelf5 = 1058u,
        ColdVulnerabilitySelf6 = 1059u,
        ColdVulnerabilityOther1 = 1060u,
        ColdVulnerabilityOther2 = 1061u,
        ColdVulnerabilityOther3 = 1062u,
        ColdVulnerabilityOther4 = 1063u,
        ColdVulnerabilityOther5 = 1064u,
        ColdVulnerabilityOther6 = 1065u,
        LightningProtectionSelf1 = 1066u,
        LightningProtectionSelf2 = 1067u,
        LightningProtectionSelf3 = 1068u,
        LightningProtectionSelf4 = 1069u,
        LightningProtectionSelf5 = 1070u,
        LightningProtectionSelf6 = 1071u,
        LightningProtectionOther1 = 1072u,
        LightningProtectionOther2 = 1073u,
        LightningProtectionOther3 = 1074u,
        LightningProtectionOther4 = 1075u,
        LightningProtectionOther5 = 1076u,
        LightningProtectionOther6 = 1077u,
        LightningVulnerabilitySelf1 = 1078u,
        LightningVulnerabilitySelf2 = 1079u,
        LightningVulnerabilitySelf3 = 1080u,
        LightningVulnerabilitySelf4 = 1081u,
        LightningVulnerabilitySelf5 = 1082u,
        LightningVulnerabilitySelf6 = 1083u,
        LightningVulnerabilityOther1 = 1084u,
        LightningVulnerabilityOther2 = 1085u,
        LightningVulnerabilityOther3 = 1086u,
        LightningVulnerabilityOther4 = 1087u,
        LightningVulnerabilityOther5 = 1088u,
        LightningVulnerabilityOther6 = 1089u,
        FireProtectionSelf2 = 1090u,
        FireProtectionSelf3 = 1091u,
        FireProtectionSelf4 = 1092u,
        FireProtectionSelf5 = 1093u,
        FireProtectionSelf6 = 1094u,
        FireProtectionOther5 = 1095u,
        FireProtectionOther6 = 1096u,
        Nullmissile = 1097u,
        FireVulnerabilitySelf2 = 1098u,
        FireVulnerabilitySelf3 = 1099u,
        FireVulnerabilitySelf4 = 1100u,
        FireVulnerabilitySelf5 = 1101u,
        FireVulnerabilitySelf6 = 1102u,
        UNKNOWN__GUESSEDNAME1103 = 1103u,
        FireVulnerabilityOther2 = 1104u,
        FireVulnerabilityOther3 = 1105u,
        FireVulnerabilityOther4 = 1106u,
        FireVulnerabilityOther5 = 1107u,
        FireVulnerabilityOther6 = 1108u,
        BladeProtectionSelf1 = 1109u,
        BladeProtectionSelf2 = 1110u,
        BladeProtectionSelf3 = 1111u,
        BladeProtectionSelf4 = 1112u,
        BladeProtectionSelf5 = 1113u,
        BladeProtectionSelf6 = 1114u,
        BladeProtectionOther1 = 1115u,
        BladeProtectionOther2 = 1116u,
        BladeProtectionOther3 = 1117u,
        BladeProtectionOther4 = 1118u,
        BladeProtectionOther5 = 1119u,
        BladeProtectionOther6 = 1120u,
        BladeVulnerabilitySelf1 = 1121u,
        BladeVulnerabilitySelf2 = 1122u,
        BladeVulnerabilitySelf3 = 1123u,
        BladeVulnerabilitySelf4 = 1124u,
        BladeVulnerabilitySelf5 = 1125u,
        BladeVulnerabilitySelf6 = 1126u,
        BladeVulnerabilityOther1 = 1127u,
        BladeVulnerabilityOther2 = 1128u,
        BladeVulnerabilityOther3 = 1129u,
        BladeVulnerabilityOther4 = 1130u,
        BladeVulnerabilityOther5 = 1131u,
        BladeVulnerabilityOther6 = 1132u,
        PiercingProtectionSelf1 = 1133u,
        PiercingProtectionSelf2 = 1134u,
        PiercingProtectionSelf3 = 1135u,
        PiercingProtectionSelf4 = 1136u,
        PiercingProtectionSelf5 = 1137u,
        PiercingProtectionSelf6 = 1138u,
        PiercingProtectionOther1 = 1139u,
        PiercingProtectionOther2 = 1140u,
        PiercingProtectionOther3 = 1141u,
        PiercingProtectionOther4 = 1142u,
        PiercingProtectionOther5 = 1143u,
        PiercingProtectionOther6 = 1144u,
        PiercingVulnerabilitySelf1 = 1145u,
        PiercingVulnerabilitySelf2 = 1146u,
        PiercingVulnerabilitySelf3 = 1147u,
        PiercingVulnerabilitySelf4 = 1148u,
        PiercingVulnerabilitySelf5 = 1149u,
        PiercingVulnerabilitySelf6 = 1150u,
        PiercingVulnerabilityOther1 = 1151u,
        PiercingVulnerabilityOther2 = 1152u,
        PiercingVulnerabilityOther3 = 1153u,
        PiercingVulnerabilityOther4 = 1154u,
        PiercingVulnerabilityOther5 = 1155u,
        PiercingVulnerabilityOther6 = 1156u,
        HealSelf2 = 1157u,
        HealSelf3 = 1158u,
        HealSelf4 = 1159u,
        HealSelf5 = 1160u,
        HealSelf6 = 1161u,
        HealOther2 = 1162u,
        HealOther3 = 1163u,
        HealOther4 = 1164u,
        HealOther5 = 1165u,
        HealOther6 = 1166u,
        HarmSelf2 = 1167u,
        HarmSelf3 = 1168u,
        HarmSelf4 = 1169u,
        HarmSelf5 = 1170u,
        HarmSelf6 = 1171u,
        HarmOther2 = 1172u,
        HarmOther3 = 1173u,
        HarmOther4 = 1174u,
        HarmOther5 = 1175u,
        HarmOther6 = 1176u,
        RevitalizeSelf1 = 1177u,
        RevitalizeSelf2 = 1178u,
        RevitalizeSelf3 = 1179u,
        RevitalizeSelf4 = 1180u,
        RevitalizeSelf5 = 1181u,
        RevitalizeSelf6 = 1182u,
        RevitalizeOther1 = 1183u,
        RevitalizeOther2 = 1184u,
        RevitalizeOther3 = 1185u,
        RevitalizeOther4 = 1186u,
        RevitalizeOther5 = 1187u,
        RevitalizeOther6 = 1188u,
        EnfeebleSelf1 = 1189u,
        EnfeebleSelf2 = 1190u,
        EnfeebleSelf3 = 1191u,
        EnfeebleSelf4 = 1192u,
        EnfeebleSelf5 = 1193u,
        EnfeebleSelf6 = 1194u,
        EnfeebleOther1 = 1195u,
        EnfeebleOther2 = 1196u,
        EnfeebleOther3 = 1197u,
        EnfeebleOther4 = 1198u,
        EnfeebleOther5 = 1199u,
        EnfeebleOther6 = 1200u,
        ManaBoostSelf1 = 1201u,
        ManaBoostSelf2 = 1202u,
        ManaBoostSelf3 = 1203u,
        ManaBoostSelf4 = 1204u,
        ManaBoostSelf5 = 1205u,
        ManaBoostSelf6 = 1206u,
        ManaBoostOther1 = 1207u,
        ManaBoostOther2 = 1208u,
        ManaBoostOther3 = 1209u,
        ManaBoostOther4 = 1210u,
        ManaBoostOther5 = 1211u,
        ManaBoostOther6 = 1212u,
        ManaDrainSelf1 = 1213u,
        ManaDrainSelf2 = 1214u,
        ManaDrainSelf3 = 1215u,
        ManaDrainSelf4 = 1216u,
        ManaDrainSelf5 = 1217u,
        ManaDrainSelf6 = 1218u,
        ManaDrainOther1 = 1219u,
        ManaDrainOther2 = 1220u,
        ManaDrainOther3 = 1221u,
        ManaDrainOther4 = 1222u,
        ManaDrainOther5 = 1223u,
        ManaDrainOther6 = 1224u,
        InfuseHealth1 = 1225u,
        InfuseHealth2 = 1226u,
        InfuseHealth3 = 1227u,
        InfuseHealth4 = 1228u,
        InfuseHealth5 = 1229u,
        InfuseHealth6 = 1230u,
        UNKNOWN__GUESSEDNAME1231 = 1231u,
        UNKNOWN__GUESSEDNAME1232 = 1232u,
        UNKNOWN__GUESSEDNAME1233 = 1233u,
        UNKNOWN__GUESSEDNAME1234 = 1234u,
        UNKNOWN__GUESSEDNAME1235 = 1235u,
        UNKNOWN__GUESSEDNAME1236 = 1236u,
        DrainHealth1 = 1237u,
        DrainHealth2 = 1238u,
        DrainHealth3 = 1239u,
        DrainHealth4 = 1240u,
        DrainHealth5 = 1241u,
        DrainHealth6 = 1242u,
        InfuseStamina1 = 1243u,
        InfuseStamina2 = 1244u,
        InfuseStamina3 = 1245u,
        InfuseStamina4 = 1246u,
        InfuseStamina5 = 1247u,
        InfuseStamina6 = 1248u,
        DrainStamina1 = 1249u,
        DrainStamina2 = 1250u,
        DrainStamina3 = 1251u,
        DrainStamina4 = 1252u,
        DrainStamina5 = 1253u,
        DrainStamina6 = 1254u,
        InfuseMana2 = 1255u,
        InfuseMana3 = 1256u,
        InfuseMana4 = 1257u,
        InfuseMana5 = 1258u,
        InfuseMana6 = 1259u,
        DrainMana1 = 1260u,
        DrainMana2 = 1261u,
        DrainMana3 = 1262u,
        DrainMana4 = 1263u,
        DrainMana5 = 1264u,
        DrainMana6 = 1265u,
        HealthToStaminaOther1 = 1266u,
        HealthToStaminaOther2 = 1267u,
        HealthToStaminaOther3 = 1268u,
        HealthToStaminaOther4 = 1269u,
        HealthToStaminaOther5 = 1270u,
        HealthToStaminaOther6 = 1271u,
        HealthToStaminaSelf1 = 1272u,
        HealthToStaminaSelf2 = 1273u,
        HealthToStaminaSelf3 = 1274u,
        HealthToStaminaSelf4 = 1275u,
        HealthToStaminaSelf5 = 1276u,
        HealthToStaminaSelf6 = 1277u,
        HealthToManaSelf1 = 1278u,
        HealthToManaSelf2 = 1279u,
        HealthToManaSelf3 = 1280u,
        HealthToManaOther4 = 1281u,
        HealthToManaOther5 = 1282u,
        HealthToManaOther6 = 1283u,
        ManaToHealthOther1 = 1284u,
        ManaToHealthOther2 = 1285u,
        ManaToHealthOther3 = 1286u,
        ManaToHealthOther4 = 1287u,
        ManaToHealthOther5 = 1288u,
        ManaToHealthOther6 = 1289u,
        ManaToHealthSelf1 = 1290u,
        ManaToHealthSelf2 = 1291u,
        ManaToHealthSelf3 = 1292u,
        ManaToHealthSelf4 = 1293u,
        ManaToHealthSelf5 = 1294u,
        ManaToHealthSelf6 = 1295u,
        ManaToStaminaSelf1 = 1296u,
        ManaToStaminaSelf2 = 1297u,
        ManaToStaminaSelf3 = 1298u,
        ManaToStaminaSelf4 = 1299u,
        ManaToStaminaSelf5 = 1300u,
        ManaToStaminaSelf6 = 1301u,
        ManaToStaminaOther1 = 1302u,
        ManaToStaminaOther2 = 1303u,
        ManaToStaminaOther3 = 1304u,
        ManaToStaminaOther4 = 1305u,
        ManaToStaminaOther5 = 1306u,
        ManaToStaminaOther6 = 1307u,
        ArmorSelf2 = 1308u,
        ArmorSelf3 = 1309u,
        ArmorSelf4 = 1310u,
        ArmorSelf5 = 1311u,
        ArmorSelf6 = 1312u,
        ArmorOther2 = 1313u,
        ArmorOther3 = 1314u,
        ArmorOther4 = 1315u,
        ArmorOther5 = 1316u,
        ArmorOther6 = 1317u,
        ImperilSelf2 = 1318u,
        ImperilSelf3 = 1319u,
        ImperilSelf4 = 1320u,
        ImperilSelf5 = 1321u,
        ImperilSelf6 = 1322u,
        ImperilOther2 = 1323u,
        ImperilOther3 = 1324u,
        ImperilOther4 = 1325u,
        ImperilOther5 = 1326u,
        ImperilOther6 = 1327u,
        StrengthSelf2 = 1328u,
        StrengthSelf3 = 1329u,
        StrengthSelf4 = 1330u,
        StrengthSelf5 = 1331u,
        StrengthSelf6 = 1332u,
        StrengthOther2 = 1333u,
        StrengthOther3 = 1334u,
        StrengthOther4 = 1335u,
        StrengthOther5 = 1336u,
        StrengthOther6 = 1337u,
        UNKNOWN__GUESSEDNAME1338 = 1338u,
        WeaknessOther2 = 1339u,
        WeaknessOther3 = 1340u,
        WeaknessOther4 = 1341u,
        WeaknessOther5 = 1342u,
        WeaknessOther6 = 1343u,
        WeaknessSelf2 = 1344u,
        WeaknessSelf3 = 1345u,
        WeaknessSelf4 = 1346u,
        WeaknessSelf5 = 1347u,
        WeaknessSelf6 = 1348u,
        EnduranceSelf1 = 1349u,
        EnduranceSelf2 = 1350u,
        EnduranceSelf3 = 1351u,
        EnduranceSelf4 = 1352u,
        EnduranceSelf5 = 1353u,
        EnduranceSelf6 = 1354u,
        EnduranceOther1 = 1355u,
        EnduranceOther2 = 1356u,
        EnduranceOther3 = 1357u,
        EnduranceOther4 = 1358u,
        EnduranceOther5 = 1359u,
        EnduranceOther6 = 1360u,
        FrailtySelf1 = 1361u,
        FrailtySelf2 = 1362u,
        FrailtySelf3 = 1363u,
        FrailtySelf4 = 1364u,
        FrailtySelf5 = 1365u,
        FrailtySelf6 = 1366u,
        FrailtyOther1 = 1367u,
        FrailtyOther2 = 1368u,
        FrailtyOther3 = 1369u,
        FrailtyOther4 = 1370u,
        FrailtyOther5 = 1371u,
        FrailtyOther6 = 1372u,
        CoordinationSelf1 = 1373u,
        CoordinationSelf2 = 1374u,
        CoordinationSelf3 = 1375u,
        CoordinationSelf4 = 1376u,
        CoordinationSelf5 = 1377u,
        CoordinationSelf6 = 1378u,
        CoordinationOther1 = 1379u,
        CoordinationOther2 = 1380u,
        CoordinationOther3 = 1381u,
        CoordinationOther4 = 1382u,
        CoordinationOther5 = 1383u,
        CoordinationOther6 = 1384u,
        ClumsinessSelf1 = 1385u,
        ClumsinessSelf2 = 1386u,
        ClumsinessSelf3 = 1387u,
        ClumsinessSelf4 = 1388u,
        ClumsinessSelf5 = 1389u,
        ClumsinessSelf6 = 1390u,
        ClumsinessOther1 = 1391u,
        ClumsinessOther2 = 1392u,
        ClumsinessOther3 = 1393u,
        ClumsinessOther4 = 1394u,
        ClumsinessOther5 = 1395u,
        ClumsinessOther6 = 1396u,
        QuicknessSelf1 = 1397u,
        QuicknessSelf2 = 1398u,
        QuicknessSelf3 = 1399u,
        QuicknessSelf4 = 1400u,
        QuicknessSelf5 = 1401u,
        QuicknessSelf6 = 1402u,
        QuicknessOther1 = 1403u,
        QuicknessOther2 = 1404u,
        QuicknessOther3 = 1405u,
        QuicknessOther4 = 1406u,
        QuicknessOther5 = 1407u,
        QuicknessOther6 = 1408u,
        SlownessSelf1 = 1409u,
        SlownessSelf2 = 1410u,
        SlownessSelf3 = 1411u,
        SlownessSelf4 = 1412u,
        SlownessSelf5 = 1413u,
        SlownessSelf6 = 1414u,
        SlownessOther1 = 1415u,
        SlownessOther2 = 1416u,
        SlownessOther3 = 1417u,
        SlownessOther4 = 1418u,
        SlownessOther5 = 1419u,
        SlownessOther6 = 1420u,
        FocusSelf1 = 1421u,
        FocusSelf2 = 1422u,
        FocusSelf3 = 1423u,
        FocusSelf4 = 1424u,
        FocusSelf5 = 1425u,
        FocusSelf6 = 1426u,
        FocusOther1 = 1427u,
        FocusOther2 = 1428u,
        FocusOther3 = 1429u,
        FocusOther4 = 1430u,
        FocusOther5 = 1431u,
        FocusOther6 = 1432u,
        BafflementSelf1 = 1433u,
        BafflementSelf2 = 1434u,
        BafflementSelf3 = 1435u,
        BafflementSelf4 = 1436u,
        BafflementSelf5 = 1437u,
        BafflementSelf6 = 1438u,
        BafflementOther1 = 1439u,
        BafflementOther2 = 1440u,
        BafflementOther3 = 1441u,
        BafflementOther4 = 1442u,
        BafflementOther5 = 1443u,
        BafflementOther6 = 1444u,
        WillpowerSelf1 = 1445u,
        WillpowerSelf2 = 1446u,
        WillpowerSelf3 = 1447u,
        WillpowerSelf4 = 1448u,
        WillpowerSelf5 = 1449u,
        WillpowerSelf6 = 1450u,
        WillpowerOther1 = 1451u,
        WillpowerOther2 = 1452u,
        WillpowerOther3 = 1453u,
        WillpowerOther4 = 1454u,
        WillpowerOther5 = 1455u,
        WillpowerOther6 = 1456u,
        FeeblemindSelf1 = 1457u,
        FeeblemindSelf2 = 1458u,
        FeeblemindSelf3 = 1459u,
        FeeblemindSelf4 = 1460u,
        FeeblemindSelf5 = 1461u,
        FeeblemindSelf6 = 1462u,
        FeeblemindOther1 = 1463u,
        FeeblemindOther2 = 1464u,
        FeeblemindOther3 = 1465u,
        FeeblemindOther4 = 1466u,
        FeeblemindOther5 = 1467u,
        FeeblemindOther6 = 1468u,
        HermeticVoid1 = 1469u,
        HermeticVoid2 = 1470u,
        HermeticVoid3 = 1471u,
        HermeticVoid4 = 1472u,
        HermeticVoid5 = 1473u,
        HermeticVoid6 = 1474u,
        HermeticLinkSelf1 = 1475u,
        HermeticLinkSelf2 = 1476u,
        HermeticLinkSelf3 = 1477u,
        HermeticLinkSelf4 = 1478u,
        HermeticLinkSelf5 = 1479u,
        HermeticLinkSelf6 = 1480u,
        Nullmissilevolley = 1481u,
        Impenetrability2 = 1482u,
        Impenetrability3 = 1483u,
        Impenetrability4 = 1484u,
        Impenetrability5 = 1485u,
        Impenetrability6 = 1486u,
        Brittlemail1 = 1487u,
        Brittlemail2 = 1488u,
        Brittlemail3 = 1489u,
        Brittlemail4 = 1490u,
        Brittlemail5 = 1491u,
        Brittlemail6 = 1492u,
        AcidBane1 = 1493u,
        AcidBane2 = 1494u,
        AcidBane3 = 1495u,
        AcidBane4 = 1496u,
        AcidBane5 = 1497u,
        AcidBane6 = 1498u,
        AcidLure1 = 1499u,
        AcidLure2 = 1500u,
        AcidLure3 = 1501u,
        AcidLure4 = 1502u,
        AcidLure5 = 1503u,
        AcidLure6 = 1504u,
        BludgeonLure1 = 1505u,
        BludgeonLure2 = 1506u,
        BludgeonLure3 = 1507u,
        BludgeonLure4 = 1508u,
        BludgeonLure5 = 1509u,
        BludgeonLure6 = 1510u,
        BludgeonBane1 = 1511u,
        BludgeonBane2 = 1512u,
        BludgeonBane3 = 1513u,
        BludgeonBane4 = 1514u,
        BludgeonBane5 = 1515u,
        BludgeonBane6 = 1516u,
        FrostLure1 = 1517u,
        FrostLure2 = 1518u,
        FrostLure3 = 1519u,
        FrostLure4 = 1520u,
        FrostLure5 = 1521u,
        FrostLure6 = 1522u,
        FrostBane1 = 1523u,
        FrostBane2 = 1524u,
        FrostBane3 = 1525u,
        FrostBane4 = 1526u,
        FrostBane5 = 1527u,
        FrostBane6 = 1528u,
        LightningLure1 = 1529u,
        LightningLure2 = 1530u,
        LightningLure3 = 1531u,
        LightningLure4 = 1532u,
        LightningLure5 = 1533u,
        LightningLure6 = 1534u,
        LightningBane1 = 1535u,
        LightningBane2 = 1536u,
        LightningBane3 = 1537u,
        LightningBane4 = 1538u,
        LightningBane5 = 1539u,
        LightningBane6 = 1540u,
        FlameLure1 = 1541u,
        FlameLure2 = 1542u,
        FlameLure3 = 1543u,
        FlameLure4 = 1544u,
        FlameLure5 = 1545u,
        FlameLure6 = 1546u,
        FlameBane1 = 1547u,
        FlameBane2 = 1548u,
        FlameBane3 = 1549u,
        FlameBane4 = 1550u,
        FlameBane5 = 1551u,
        FlameBane6 = 1552u,
        BladeLure2 = 1553u,
        BladeLure3 = 1554u,
        BladeLure4 = 1555u,
        BladeLure5 = 1556u,
        BladeLure6 = 1557u,
        BladeBane2 = 1558u,
        BladeBane3 = 1559u,
        BladeBane4 = 1560u,
        BladeBane5 = 1561u,
        BladeBane6 = 1562u,
        PiercingLure1 = 1563u,
        PiercingLure2 = 1564u,
        PiercingLure3 = 1565u,
        PiercingLure4 = 1566u,
        PiercingLure5 = 1567u,
        PiercingLure6 = 1568u,
        PiercingBane1 = 1569u,
        PiercingBane2 = 1570u,
        PiercingBane3 = 1571u,
        PiercingBane4 = 1572u,
        PiercingBane5 = 1573u,
        PiercingBane6 = 1574u,
        StrengthenLock1 = 1575u,
        StrengthenLock2 = 1576u,
        StrengthenLock3 = 1577u,
        StrengthenLock4 = 1578u,
        StrengthenLock5 = 1579u,
        StrengthenLock6 = 1580u,
        WeakenLock1 = 1581u,
        WeakenLock2 = 1582u,
        WeakenLock3 = 1583u,
        WeakenLock4 = 1584u,
        WeakenLock5 = 1585u,
        WeakenLock6 = 1586u,
        HeartSeekerSelf1 = 1587u,
        HeartSeekerSelf2 = 1588u,
        HeartSeekerSelf3 = 1589u,
        HeartSeekerSelf4 = 1590u,
        HeartSeekerSelf5 = 1591u,
        HeartSeekerSelf6 = 1592u,
        TurnBlade1 = 1593u,
        TurnBlade2 = 1594u,
        TurnBlade3 = 1595u,
        TurnBlade4 = 1596u,
        TurnBlade5 = 1597u,
        TurnBlade6 = 1598u,
        DefenderSelf1 = 1599u,
        UNKNOWN__GUESSEDNAME1600 = 1600u,
        DefenderSelf2 = 1601u,
        DefenderSelf3 = 1602u,
        DefenderSelf4 = 1603u,
        DefenderSelf5 = 1604u,
        DefenderSelf6 = 1605u,
        LureBlade1 = 1606u,
        LureBlade2 = 1607u,
        LureBlade3 = 1608u,
        LureBlade4 = 1609u,
        LureBlade5 = 1610u,
        LureBlade6 = 1611u,
        BloodDrinkerSelf2 = 1612u,
        BloodDrinkerSelf3 = 1613u,
        BloodDrinkerSelf4 = 1614u,
        BloodDrinkerSelf5 = 1615u,
        BloodDrinkerSelf6 = 1616u,
        BloodLoather2 = 1617u,
        BloodLoather3 = 1618u,
        BloodLoather4 = 1619u,
        BloodLoather5 = 1620u,
        BloodLoather6 = 1621u,
        UNKNOWN__GUESSEDNAME1622 = 1622u,
        SwiftKillerSelf2 = 1623u,
        SwiftKillerSelf3 = 1624u,
        SwiftKillerSelf4 = 1625u,
        SwiftKillerSelf5 = 1626u,
        SwiftKillerSelf6 = 1627u,
        UNKNOWN__GUESSEDNAME1628 = 1628u,
        LeadenWeapon2 = 1629u,
        LeadenWeapon3 = 1630u,
        LeadenWeapon4 = 1631u,
        LeadenWeapon5 = 1632u,
        LeadenWeapon6 = 1633u,
        PortalSending1 = 1634u,
        LifestoneRecall1 = 1635u,
        LifestoneSending1 = 1636u,
        SummonPortal3 = 1637u,
        DefenselessnessSelf1 = 1638u,
        DefenselessnessSelf2 = 1639u,
        DefenselessnessSelf3 = 1640u,
        DefenselessnessSelf4 = 1641u,
        DefenselessnessSelf5 = 1642u,
        DefenselessnessSelf6 = 1643u,
        SentinelRun = 1644u,
        UNKNOWN__GUESSEDNAME1645 = 1645u,
        UNKNOWN__GUESSEDNAME1646 = 1646u,
        UNKNOWN__GUESSEDNAME1647 = 1647u,
        UNKNOWN__GUESSEDNAME1648 = 1648u,
        UNKNOWN__GUESSEDNAME1649 = 1649u,
        UNKNOWN__GUESSEDNAME1650 = 1650u,
        UNKNOWN__GUESSEDNAME1651 = 1651u,
        UNKNOWN__GUESSEDNAME1652 = 1652u,
        UNKNOWN__GUESSEDNAME1653 = 1653u,
        UNKNOWN__GUESSEDNAME1654 = 1654u,
        UNKNOWN__GUESSEDNAME1655 = 1655u,
        UNKNOWN__GUESSEDNAME1656 = 1656u,
        UNKNOWN__GUESSEDNAME1657 = 1657u,
        StaminaToHealthOther1 = 1658u,
        StaminaToHealthOther2 = 1659u,
        StaminaToHealthOther3 = 1660u,
        StaminaToHealthOther4 = 1661u,
        StaminaToHealthOther5 = 1662u,
        StaminaToHealthOther6 = 1663u,
        StaminaToHealthSelf1 = 1664u,
        StaminaToHealthSelf2 = 1665u,
        StaminaToHealthSelf3 = 1666u,
        StaminaToHealthSelf4 = 1667u,
        StaminaToHealthSelf5 = 1668u,
        StaminaToHealthSelf6 = 1669u,
        StaminaToManaOther1 = 1670u,
        StaminaToManaOther2 = 1671u,
        StaminaToManaOther3 = 1672u,
        StaminaToManaOther4 = 1673u,
        StaminaToManaOther5 = 1674u,
        StaminaToManaOther6 = 1675u,
        StaminaToManaSelf1 = 1676u,
        StaminaToManaSelf2 = 1677u,
        StaminaToManaSelf3 = 1678u,
        StaminaToManaSelf4 = 1679u,
        StaminaToManaSelf5 = 1680u,
        StaminaToManaSelf6 = 1681u,
        UNKNOWN__GUESSEDNAME1682 = 1682u,
        UNKNOWN__GUESSEDNAME1683 = 1683u,
        UNKNOWN__GUESSEDNAME1684 = 1684u,
        UNKNOWN__GUESSEDNAME1685 = 1685u,
        UNKNOWN__GUESSEDNAME1686 = 1686u,
        UNKNOWN__GUESSEDNAME1687 = 1687u,
        UNKNOWN__GUESSEDNAME1688 = 1688u,
        UNKNOWN__GUESSEDNAME1689 = 1689u,
        UNKNOWN__GUESSEDNAME1690 = 1690u,
        UNKNOWN__GUESSEDNAME1691 = 1691u,
        UNKNOWN__GUESSEDNAME1692 = 1692u,
        UNKNOWN__GUESSEDNAME1693 = 1693u,
        UNKNOWN__GUESSEDNAME1694 = 1694u,
        UNKNOWN__GUESSEDNAME1695 = 1695u,
        UNKNOWN__GUESSEDNAME1696 = 1696u,
        UNKNOWN__GUESSEDNAME1697 = 1697u,
        UNKNOWN__GUESSEDNAME1698 = 1698u,
        UNKNOWN__GUESSEDNAME1699 = 1699u,
        UNKNOWN__GUESSEDNAME1700 = 1700u,
        UNKNOWN__GUESSEDNAME1701 = 1701u,
        HealthToManaSelf4 = 1702u,
        HealthToManaSelf5 = 1703u,
        HealthToManaSelf6 = 1704u,
        HealthToManaOther1 = 1705u,
        HealthToManaOther2 = 1706u,
        HealthToManaOther3 = 1707u,
        WeddingBliss = 1708u,
        CookingMasteryOther1 = 1709u,
        CookingMasteryOther2 = 1710u,
        CookingMasteryOther3 = 1711u,
        CookingMasteryOther4 = 1712u,
        CookingMasteryOther5 = 1713u,
        CookingMasteryOther6 = 1714u,
        CookingMasterySelf1 = 1715u,
        CookingMasterySelf2 = 1716u,
        CookingMasterySelf3 = 1717u,
        CookingMasterySelf4 = 1718u,
        CookingMasterySelf5 = 1719u,
        CookingMasterySelf6 = 1720u,
        CookingIneptitudeOther1 = 1721u,
        CookingIneptitudeOther2 = 1722u,
        CookingIneptitudeOther3 = 1723u,
        CookingIneptitudeOther4 = 1724u,
        CookingIneptitudeOther5 = 1725u,
        CookingIneptitudeOther6 = 1726u,
        CookingIneptitudeSelf1 = 1727u,
        CookingIneptitudeSelf2 = 1728u,
        CookingIneptitudeSelf3 = 1729u,
        CookingIneptitudeSelf4 = 1730u,
        CookingIneptitudeSelf5 = 1731u,
        CookingIneptitudeSelf6 = 1732u,
        FletchingMasteryOther1 = 1733u,
        FletchingMasteryOther2 = 1734u,
        FletchingMasteryOther3 = 1735u,
        FletchingMasteryOther4 = 1736u,
        FletchingMasteryOther5 = 1737u,
        FletchingMasteryOther6 = 1738u,
        FletchingMasterySelf1 = 1739u,
        FletchingMasterySelf2 = 1740u,
        FletchingMasterySelf3 = 1741u,
        FletchingMasterySelf4 = 1742u,
        FletchingMasterySelf5 = 1743u,
        FletchingMasterySelf6 = 1744u,
        FletchingIneptitudeOther1 = 1745u,
        FletchingIneptitudeOther2 = 1746u,
        FletchingIneptitudeOther3 = 1747u,
        FletchingIneptitudeOther4 = 1748u,
        FletchingIneptitudeOther5 = 1749u,
        FletchingIneptitudeOther6 = 1750u,
        FletchingIneptitudeSelf1 = 1751u,
        FletchingIneptitudeSelf2 = 1752u,
        FletchingIneptitudeSelf3 = 1753u,
        FletchingIneptitudeSelf4 = 1754u,
        FletchingIneptitudeSelf5 = 1755u,
        FletchingIneptitudeSelf6 = 1756u,
        AlchemyMasteryOther1 = 1757u,
        AlchemyMasteryOther2 = 1758u,
        AlchemyMasteryOther3 = 1759u,
        AlchemyMasteryOther4 = 1760u,
        AlchemyMasteryOther5 = 1761u,
        AlchemyMasteryOther6 = 1762u,
        AlchemyMasterySelf1 = 1763u,
        AlchemyMasterySelf2 = 1764u,
        AlchemyMasterySelf3 = 1765u,
        AlchemyMasterySelf4 = 1766u,
        AlchemyMasterySelf5 = 1767u,
        AlchemyMasterySelf6 = 1768u,
        AlchemyIneptitudeOther1 = 1769u,
        AlchemyIneptitudeOther2 = 1770u,
        AlchemyIneptitudeOther3 = 1771u,
        AlchemyIneptitudeOther4 = 1772u,
        AlchemyIneptitudeOther5 = 1773u,
        AlchemyIneptitudeOther6 = 1774u,
        AlchemyIneptitudeSelf1 = 1775u,
        AlchemyIneptitudeSelf2 = 1776u,
        AlchemyIneptitudeSelf3 = 1777u,
        AlchemyIneptitudeSelf4 = 1778u,
        AlchemyIneptitudeSelf5 = 1779u,
        AlchemyIneptitudeSelf6 = 1780u,
        GolemDeathBlast = 1781u,
        GertarhsCurse = 1782u,
        AcidRing = 1783u,
        BladeRing = 1784u,
        FlameRing = 1785u,
        ForceRing = 1786u,
        FrostRing = 1787u,
        LightningRing = 1788u,
        ShockwaveRing = 1789u,
        AcidStreak1 = 1790u,
        AcidStreak2 = 1791u,
        AcidStreak3 = 1792u,
        AcidStreak4 = 1793u,
        AcidStreak5 = 1794u,
        AcidStreak6 = 1795u,
        FlameStreak1 = 1796u,
        FlameStreak2 = 1797u,
        FlameStreak3 = 1798u,
        FlameStreak4 = 1799u,
        FlameStreak5 = 1800u,
        FlameStreak6 = 1801u,
        ForceStreak1 = 1802u,
        ForceStreak2 = 1803u,
        ForceStreak3 = 1804u,
        ForceStreak4 = 1805u,
        ForceStreak5 = 1806u,
        ForceStreak6 = 1807u,
        FrostStreak1 = 1808u,
        FrostStreak2 = 1809u,
        FrostStreak3 = 1810u,
        FrostStreak4 = 1811u,
        FrostStreak5 = 1812u,
        FrostStreak6 = 1813u,
        LightningStreak1 = 1814u,
        LightningStreak2 = 1815u,
        LightningStreak3 = 1816u,
        LightningStreak4 = 1817u,
        LightningStreak5 = 1818u,
        LightningStreak6 = 1819u,
        ShockwaveStreak1 = 1820u,
        ShockwaveStreak2 = 1821u,
        ShockwaveStreak3 = 1822u,
        ShockwaveStreak4 = 1823u,
        ShockwaveStreak5 = 1824u,
        ShockwaveStreak6 = 1825u,
        WhirlingBladeStreak1 = 1826u,
        WhirlingBladeStreak2 = 1827u,
        WhirlingBladeStreak3 = 1828u,
        WhirlingBladeStreak4 = 1829u,
        WhirlingBladeStreak5 = 1830u,
        WhirlingBladeStreak6 = 1831u,
        AcidStrike = 1832u,
        BladeStrike = 1833u,
        FlameStrike = 1834u,
        ForceStrike = 1835u,
        FrostStrike = 1836u,
        LightningStrike = 1837u,
        ShockwaveStrike = 1838u,
        AcidWall = 1839u,
        BladeWall = 1840u,
        FlameWall = 1841u,
        ForceWall = 1842u,
        FrostWall = 1843u,
        LightningWall = 1844u,
        ShockwaveWall = 1845u,
        MagicYieldBlackFire = 1846u,
        DispelAllNeutralOther1 = 1847u,
        DispelAllGoodOther1 = 1848u,
        DispelAllBadOther1 = 1849u,
        DispelAllNeutralSelf1 = 1850u,
        DispelAllGoodSelf1 = 1851u,
        DispelAllBadSelf1 = 1852u,
        DispelAllNeutralOther2 = 1853u,
        DispelAllGoodOther2 = 1854u,
        DispelAllBadOther2 = 1855u,
        DispelAllNeutralSelf2 = 1856u,
        DispelAllGoodSelf2 = 1857u,
        DispelAllBadSelf2 = 1858u,
        DispelAllNeutralOther3 = 1859u,
        DispelAllGoodOther3 = 1860u,
        DispelAllBadOther3 = 1861u,
        DispelAllNeutralSelf3 = 1862u,
        DispelAllGoodSelf3 = 1863u,
        DispelAllBadSelf3 = 1864u,
        DispelAllNeutralOther4 = 1865u,
        DispelAllGoodOther4 = 1866u,
        DispelAllBadOther4 = 1867u,
        DispelAllNeutralSelf4 = 1868u,
        DispelAllGoodSelf4 = 1869u,
        DispelAllBadSelf4 = 1870u,
        DispelAllNeutralOther5 = 1871u,
        DispelAllGoodOther5 = 1872u,
        DispelAllBadOther5 = 1873u,
        DispelAllNeutralSelf5 = 1874u,
        DispelAllGoodSelf5 = 1875u,
        DispelAllBadSelf5 = 1876u,
        DispelAllNeutralOther6 = 1877u,
        DispelAllGoodOther6 = 1878u,
        DispelAllBadOther6 = 1879u,
        DispelAllNeutralSelf6 = 1880u,
        DispelAllGoodSelf6 = 1881u,
        DispelAllBadSelf6 = 1882u,
        DispelCreatureNeutralOther1 = 1883u,
        DispelCreatureGoodOther1 = 1884u,
        DispelCreatureBadOther1 = 1885u,
        DispelCreatureNeutralSelf1 = 1886u,
        DispelCreatureGoodSelf1 = 1887u,
        DispelCreatureBadSelf1 = 1888u,
        DispelCreatureNeutralOther2 = 1889u,
        DispelCreatureGoodOther2 = 1890u,
        DispelCreatureBadOther2 = 1891u,
        DispelCreatureNeutralSelf2 = 1892u,
        DispelCreatureGoodSelf2 = 1893u,
        DispelCreatureBadSelf2 = 1894u,
        DispelCreatureNeutralOther3 = 1895u,
        DispelCreatureGoodOther3 = 1896u,
        DispelCreatureBadOther3 = 1897u,
        DispelCreatureNeutralSelf3 = 1898u,
        DispelCreatureGoodSelf3 = 1899u,
        DispelCreatureBadSelf3 = 1900u,
        DispelCreatureNeutralOther4 = 1901u,
        DispelCreatureGoodOther4 = 1902u,
        DispelCreatureBadOther4 = 1903u,
        DispelCreatureNeutralSelf4 = 1904u,
        DispelCreatureGoodSelf4 = 1905u,
        DispelCreatureBadSelf4 = 1906u,
        DispelCreatureNeutralOther5 = 1907u,
        DispelCreatureGoodOther5 = 1908u,
        DispelCreatureBadOther5 = 1909u,
        DispelCreatureNeutralSelf5 = 1910u,
        DispelCreatureGoodSelf5 = 1911u,
        DispelCreatureBadSelf5 = 1912u,
        DispelCreatureNeutralOther6 = 1913u,
        DispelCreatureGoodOther6 = 1914u,
        DispelCreatureBadOther6 = 1915u,
        DispelCreatureNeutralSelf6 = 1916u,
        DispelCreatureGoodSelf6 = 1917u,
        DispelCreatureBadSelf6 = 1918u,
        DispelItemNeutralOther1 = 1919u,
        DispelItemGoodOther1 = 1920u,
        DispelItemBadOther1 = 1921u,
        DispelItemNeutralSelf1 = 1922u,
        DispelItemGoodSelf1 = 1923u,
        DispelItemBadSelf1 = 1924u,
        DispelItemNeutralOther2 = 1925u,
        DispelItemGoodOther2 = 1926u,
        DispelItemBadOther2 = 1927u,
        DispelItemNeutralSelf2 = 1928u,
        DispelItemGoodSelf2 = 1929u,
        DispelItemBadSelf2 = 1930u,
        DispelItemNeutralOther3 = 1931u,
        DispelItemGoodOther3 = 1932u,
        DispelItemBadOther3 = 1933u,
        DispelItemNeutralSelf3 = 1934u,
        DispelItemGoodSelf3 = 1935u,
        DispelItemBadSelf3 = 1936u,
        DispelItemNeutralOther4 = 1937u,
        DispelItemGoodOther4 = 1938u,
        DispelItemBadOther4 = 1939u,
        DispelItemNeutralSelf4 = 1940u,
        DispelItemGoodSelf4 = 1941u,
        DispelItemBadSelf4 = 1942u,
        DispelItemNeutralOther5 = 1943u,
        DispelItemGoodOther5 = 1944u,
        DispelItemBadOther5 = 1945u,
        DispelItemNeutralSelf5 = 1946u,
        DispelItemGoodSelf5 = 1947u,
        DispelItemBadSelf5 = 1948u,
        DispelItemNeutralOther6 = 1949u,
        DispelItemGoodOther6 = 1950u,
        DispelItemBadOther6 = 1951u,
        DispelItemNeutralSelf6 = 1952u,
        DispelItemGoodSelf6 = 1953u,
        DispelItemBadSelf6 = 1954u,
        DispelLifeNeutralOther1 = 1955u,
        DispelLifeGoodOther1 = 1956u,
        DispelLifeBadOther1 = 1957u,
        DispelLifeNeutralSelf1 = 1958u,
        DispelLifeGoodSelf1 = 1959u,
        DispelLifeBadSelf1 = 1960u,
        DispelLifeNeutralOther2 = 1961u,
        DispelLifeGoodOther2 = 1962u,
        DispelLifeBadOther2 = 1963u,
        DispelLifeNeutralSelf2 = 1964u,
        DispelLifeGoodSelf2 = 1965u,
        DispelLifeBadSelf2 = 1966u,
        DispelLifeNeutralOther3 = 1967u,
        DispelLifeGoodOther3 = 1968u,
        DispelLifeBadOther3 = 1969u,
        DispelLifeNeutralSelf3 = 1970u,
        DispelLifeGoodSelf3 = 1971u,
        DispelLifeBadSelf3 = 1972u,
        DispelLifeNeutralOther4 = 1973u,
        DispelLifeGoodOther4 = 1974u,
        DispelLifeBadOther4 = 1975u,
        DispelLifeNeutralSelf4 = 1976u,
        DispelLifeGoodSelf4 = 1977u,
        DispelLifeBadSelf4 = 1978u,
        DispelLifeNeutralOther5 = 1979u,
        DispelLifeGoodOther5 = 1980u,
        DispelLifeBadOther5 = 1981u,
        DispelLifeNeutralSelf5 = 1982u,
        DispelLifeGoodSelf5 = 1983u,
        DispelLifeBadSelf5 = 1984u,
        DispelLifeNeutralOther6 = 1985u,
        DispelLifeGoodOther6 = 1986u,
        DispelLifeBadOther6 = 1987u,
        DispelLifeNeutralSelf6 = 1988u,
        DispelLifeGoodSelf6 = 1989u,
        DispelLifeBadSelf6 = 1990u,
        ManaBlight = 1991u,
        CampingMastery = 1992u,
        CampingIneptitude = 1993u,
        WoundTwister = 1994u,
        Alacrity = 1995u,
        SoulHunter = 1996u,
        LifeGiver = 1997u,
        StaminaGiver = 1998u,
        ManaGiver = 1999u,
        PortalSendingCaulnalain = 2000u,
        PortalSendingFenmalain = 2001u,
        PortalSendingShendolain = 2002u,
        WarriorsLesserVitality = 2003u,
        WarriorsVitality = 2004u,
        WarriorsGreaterVitality = 2005u,
        WarriorsUltimateVitality = 2006u,
        WarriorsLesserVigor = 2007u,
        WarriorsVigor = 2008u,
        WarriorsGreaterVigor = 2009u,
        WarriorsUltimateVigor = 2010u,
        WizardsLesserIntellect = 2011u,
        WizardsIntellect = 2012u,
        WizardsGreaterIntellect = 2013u,
        WizardsUltimateIntellect = 2014u,
        AerfallesWard = 2015u,
        Impulse = 2016u,
        BunnySmite = 2017u,
        BaelZharonSmite = 2018u,
        CrystalSunderRing = 2019u,
        RecallAsmolum1 = 2020u,
        ShadowCloudManaDrain = 2021u,
        ShadowCloudLifeDrain = 2022u,
        SanctuaryRecall = 2023u,
        RecallAsmolum2 = 2024u,
        RecallAsmolum3 = 2025u,
        ShadowCloudStamDrain = 2026u,
        Martyr = 2027u,
        SummonPortalCoIPK = 2028u,
        StaminaBlight = 2029u,
        FlamingBlaze = 2030u,
        SteelThorns = 2031u,
        ElectricBlaze = 2032u,
        AcidicSpray = 2033u,
        ExplodingFury = 2034u,
        ElectricDischarge = 2035u,
        FumingAcid = 2036u,
        FlamingIrruption = 2037u,
        ExplodingIce = 2038u,
        SparkingFury = 2039u,
        SummonPortalHopeslayer = 2040u,
        RecallAerlinthe = 2041u,
        BaelzharonWallFire = 2042u,
        BaelzharonWeaknessOther = 2043u,
        BaelzharonItemIneptOther = 2044u,
        BaelzharonRainBludgeon = 2045u,
        BaelzharonPortalExile = 2046u,
        BaelzharonArmorOther = 0x7FFu,
        BaelzharonMagicDefense = 0x800u,
        BaelzharonBloodDrinker = 2049u,
        WeddingSteele = 2050u,
        SteeleGenericFancy = 2051u,
        ArmorOther7 = 2052u,
        ArmorSelf7 = 2053u,
        BafflementOther7 = 2054u,
        BafflementSelf7 = 2055u,
        ClumsinessOther7 = 2056u,
        ClumsinessSelf7 = 2057u,
        CoordinationOther7 = 2058u,
        CoordinationSelf7 = 2059u,
        EnduranceOther7 = 2060u,
        EnduranceSelf7 = 2061u,
        EnfeebleOther7 = 2062u,
        EnfeebleSelf7 = 2063u,
        FeeblemindOther7 = 2064u,
        FeeblemindSelf7 = 2065u,
        FocusOther7 = 2066u,
        FocusSelf7 = 2067u,
        FrailtyOther7 = 2068u,
        FrailtySelf7 = 2069u,
        HarmOther7 = 2070u,
        HarmSelf7 = 2071u,
        HealOther7 = 2072u,
        HealSelf7 = 2073u,
        ImperilOther7 = 2074u,
        ImperilSelf7 = 2075u,
        ManaBoostOther7 = 2076u,
        ManaBoostSelf7 = 2077u,
        ManaDrainOther7 = 2078u,
        ManaDrainSelf7 = 2079u,
        QuicknessOther7 = 2080u,
        QuicknessSelf7 = 2081u,
        RevitalizeOther7 = 2082u,
        RevitalizeSelf7 = 2083u,
        SlownessOther7 = 2084u,
        SlownessSelf7 = 2085u,
        StrengthOther7 = 2086u,
        StrengthSelf7 = 2087u,
        WeaknessOther7 = 2088u,
        WeaknessSelf7 = 2089u,
        WillpowerOther7 = 2090u,
        WillpowerSelf7 = 2091u,
        AcidBane7 = 2092u,
        AcidLure7 = 2093u,
        BladeBane7 = 2094u,
        BladeLure7 = 2095u,
        BloodDrinkerSelf7 = 2096u,
        BloodLoather7 = 2097u,
        BludgeonBane7 = 2098u,
        BludgeonLure7 = 2099u,
        Brittlemail7 = 2100u,
        DefenderSelf7 = 2101u,
        FlameBane7 = 2102u,
        FlameLure7 = 2103u,
        FrostBane7 = 2104u,
        FrostLure7 = 2105u,
        HeartSeekerSelf7 = 2106u,
        HermeticVoid7 = 2107u,
        Impenetrability7 = 2108u,
        LeadenWeapon7 = 2109u,
        LightningBane7 = 2110u,
        LightningLure7 = 2111u,
        LureBlade7 = 2112u,
        PiercingBane7 = 2113u,
        PiercingLure7 = 2114u,
        StrengthenLock7 = 2115u,
        SwiftKillerSelf7 = 2116u,
        HermeticLinkSelf7 = 2117u,
        TurnBlade7 = 2118u,
        WeakenLock7 = 2119u,
        AcidBlast7 = 2120u,
        AcidStreak7 = 2121u,
        AcidStream7 = 2122u,
        AcidVolley7 = 2123u,
        BladeBlast7 = 2124u,
        BladeVolley7 = 2125u,
        BludgeoningVolley7 = 2126u,
        FlameBlast7 = 2127u,
        FlameBolt7 = 2128u,
        FlameStreak7 = 2129u,
        FlameVolley7 = 2130u,
        ForceBlast7 = 2131u,
        ForceBolt7 = 2132u,
        ForceStreak7 = 2133u,
        ForceVolley7 = 2134u,
        FrostBlast7 = 2135u,
        FrostBolt7 = 2136u,
        FrostStreak7 = 2137u,
        FrostVolley7 = 2138u,
        LightningBlast7 = 2139u,
        LightningBolt7 = 2140u,
        LightningStreak7 = 2141u,
        LightningVolley7 = 2142u,
        ShockBlast7 = 2143u,
        ShockWave7 = 2144u,
        ShockwaveStreak7 = 2145u,
        WhirlingBlade7 = 2146u,
        WhirlingBladeStreak7 = 2147u,
        AcidProtectionOther7 = 2148u,
        AcidProtectionSelf7 = 2149u,
        BladeProtectionOther7 = 2150u,
        BladeProtectionSelf7 = 2151u,
        BludgeonProtectionOther7 = 2152u,
        BludgeonProtectionSelf7 = 2153u,
        ColdProtectionOther7 = 2154u,
        ColdProtectionSelf7 = 2155u,
        FireProtectionOther7 = 2156u,
        FireProtectionSelf7 = 2157u,
        LightningProtectionOther7 = 2158u,
        LightningProtectionSelf7 = 2159u,
        PiercingProtectionOther7 = 2160u,
        PiercingProtectionSelf7 = 2161u,
        AcidVulnerabilityOther7 = 2162u,
        AcidVulnerabilitySelf7 = 2163u,
        BladeVulnerabilityOther7 = 2164u,
        BladeVulnerabilitySelf7 = 2165u,
        BludgeonVulnerabilityOther7 = 2166u,
        BludgeonVulnerabilitySelf7 = 2167u,
        ColdVulnerabilityOther7 = 2168u,
        ColdVulnerabilitySelf7 = 2169u,
        FireVulnerabilityOther7 = 2170u,
        FireVulnerabilitySelf7 = 2171u,
        LightningVulnerabilityOther7 = 2172u,
        LightningVulnerabilitySelf7 = 2173u,
        PiercingVulnerabilityOther7 = 2174u,
        PiercingVulnerabilitySelf7 = 2175u,
        ExhaustionOther7 = 2176u,
        ExhaustionSelf7 = 2177u,
        FesterOther7 = 2178u,
        FesterSelf7 = 2179u,
        ManaDepletionOther7 = 2180u,
        ManaDepletionSelf7 = 2181u,
        ManaRenewalOther7 = 2182u,
        ManaRenewalSelf7 = 2183u,
        RegenerationOther7 = 2184u,
        RegenerationSelf7 = 2185u,
        RejuvenationOther7 = 2186u,
        RejuvenationSelf7 = 2187u,
        AlchemyIneptitudeOther7 = 2188u,
        AlchemyIneptitudeSelf7 = 2189u,
        AlchemyMasteryOther7 = 2190u,
        AlchemyMasterySelf7 = 2191u,
        ArcaneBenightednessOther7 = 2192u,
        ArcaneBenightednessSelf7 = 2193u,
        ArcaneEnlightenmentOther7 = 2194u,
        ArcaneEnlightenmentSelf7 = 2195u,
        ArmorExpertiseOther7 = 2196u,
        ArmorExpertiseSelf7 = 2197u,
        ArmorIgnoranceOther7 = 2198u,
        ArmorIgnoranceSelf7 = 2199u,
        LightWeaponsIneptitudeOther7 = 2200u,
        LightWeaponsIneptitudeSelf7 = 2201u,
        LightWeaponsMasteryOther7 = 2202u,
        LightWeaponsMasterySelf7 = 2203u,
        MissileWeaponsIneptitudeOther7 = 2204u,
        MissileWeaponsIneptitudeSelf7 = 2205u,
        MissileWeaponsMasteryOther7 = 2206u,
        MissileWeaponsMasterySelf7 = 2207u,
        CookingIneptitudeOther7 = 2208u,
        CookingIneptitudeSelf7 = 2209u,
        CookingMasteryOther7 = 2210u,
        CookingMasterySelf7 = 2211u,
        CreatureEnchantmentIneptitudeOther7 = 2212u,
        CreatureEnchantmentIneptitudeSelf7 = 2213u,
        CreatureEnchantmentMasteryOther7 = 2214u,
        CreatureEnchantmentMasterySelf7 = 2215u,
        CrossbowIneptitudeOther7 = 2216u,
        CrossbowIneptitudeSelf7 = 2217u,
        CrossbowMasteryOther7 = 2218u,
        CrossbowMasterySelf7 = 2219u,
        FinesseWeaponsIneptitudeOther7 = 2220u,
        FinesseWeaponsIneptitudeSelf7 = 2221u,
        FinesseWeaponsMasteryOther7 = 2222u,
        FinesseWeaponsMasterySelf7 = 2223u,
        DeceptionIneptitudeOther7 = 2224u,
        DeceptionIneptitudeSelf7 = 2225u,
        DeceptionMasteryOther7 = 2226u,
        DeceptionMasterySelf7 = 2227u,
        DefenselessnessOther7 = 2228u,
        DefenselessnessSelf7 = 2229u,
        FaithlessnessOther7 = 2230u,
        FaithlessnessSelf7 = 2231u,
        FealtyOther7 = 2232u,
        FealtySelf7 = 2233u,
        FletchingIneptitudeOther7 = 2234u,
        FletchingIneptitudeSelf7 = 2235u,
        FletchingMasteryOther7 = 2236u,
        FletchingMasterySelf7 = 2237u,
        HealingIneptitudeOther7 = 2238u,
        HealingIneptitudeSelf7 = 2239u,
        HealingMasteryOther7 = 2240u,
        HealingMasterySelf7 = 2241u,
        ImpregnabilityOther7 = 2242u,
        ImpregnabilitySelf7 = 2243u,
        InvulnerabilityOther7 = 2244u,
        InvulnerabilitySelf7 = 2245u,
        ItemEnchantmentIneptitudeOther7 = 2246u,
        ItemEnchantmentIneptitudeSelf7 = 2247u,
        ItemEnchantmentMasteryOther7 = 2248u,
        ItemEnchantmentMasterySelf7 = 2249u,
        ItemExpertiseOther7 = 2250u,
        ItemExpertiseSelf7 = 2251u,
        ItemIgnoranceOther7 = 2252u,
        ItemIgnoranceSelf7 = 2253u,
        JumpingIneptitudeOther7 = 2254u,
        JumpingIneptitudeSelf7 = 2255u,
        JumpingMasteryOther7 = 2256u,
        JumpingMasterySelf7 = 2257u,
        LeadenFeetOther7 = 2258u,
        LeadenFeetSelf7 = 2259u,
        LeadershipIneptitudeOther7 = 2260u,
        LeadershipIneptitudeSelf7 = 2261u,
        LeadershipMasteryOther7 = 2262u,
        LeadershipMasterySelf7 = 2263u,
        LifeMagicIneptitudeOther7 = 2264u,
        LifeMagicIneptitudeSelf7 = 2265u,
        LifeMagicMasteryOther7 = 2266u,
        LifeMagicMasterySelf7 = 2267u,
        LockpickIneptitudeOther7 = 2268u,
        LockpickIneptitudeSelf7 = 2269u,
        LockpickMasteryOther7 = 2270u,
        LockpickMasterySelf7 = 2271u,
        MaceIneptitudeOther7 = 2272u,
        MaceIneptitudeSelf7 = 2273u,
        MaceMasteryOther7 = 2274u,
        MaceMasterySelf7 = 2275u,
        MagicItemExpertiseOther7 = 2276u,
        MagicItemExpertiseSelf7 = 2277u,
        MagicItemIgnoranceOther7 = 2278u,
        MagicItemIgnoranceSelf7 = 2279u,
        MagicResistanceOther7 = 2280u,
        MagicResistanceSelf7 = 2281u,
        MagicYieldOther7 = 2282u,
        MagicYieldSelf7 = 2283u,
        ManaIneptitudeOther7 = 2284u,
        ManaIneptitudeSelf7 = 2285u,
        ManaMasteryOther7 = 2286u,
        ManaMasterySelf7 = 2287u,
        MonsterAttunementOther7 = 2288u,
        MonsterAttunementSelf7 = 2289u,
        MonsterUnfamiliarityOther7 = 2290u,
        MonsterUnfamiliaritySelf7 = 2291u,
        PersonAttunementOther7 = 2292u,
        PersonAttunementSelf7 = 2293u,
        PersonUnfamiliarityOther7 = 2294u,
        PersonUnfamiliaritySelf7 = 2295u,
        SpearIneptitudeOther7 = 2296u,
        SpearIneptitudeSelf7 = 2297u,
        SpearMasteryOther7 = 2298u,
        SpearMasterySelf7 = 2299u,
        SprintOther7 = 2300u,
        SprintSelf7 = 2301u,
        StaffIneptitudeOther7 = 2302u,
        StaffIneptitudeSelf7 = 2303u,
        StaffMasteryOther7 = 2304u,
        StaffMasterySelf7 = 2305u,
        HeavyWeaponsIneptitudeOther7 = 2306u,
        HeavyWeaponsIneptitudeSelf7 = 2307u,
        HeavyWeaponsMasteryOther7 = 2308u,
        HeavyWeaponsMasterySelf7 = 2309u,
        ThrownWeaponIneptitudeOther7 = 2310u,
        ThrownWeaponIneptitudeSelf7 = 2311u,
        ThrownWeaponMasteryOther7 = 2312u,
        ThrownWeaponMasterySelf7 = 2313u,
        UnarmedCombatIneptitudeOther7 = 2314u,
        UnarmedCombatMasteryOther7 = 2315u,
        UnarmedCombatMasterySelf7 = 2316u,
        UnarmedCombatIneptitudeSelf7 = 2317u,
        VulnerabilityOther7 = 2318u,
        VulnerabilitySelf7 = 2319u,
        WarMagicIneptitudeOther7 = 2320u,
        WarMagicIneptitudeSelf7 = 2321u,
        WarMagicMasteryOther7 = 2322u,
        WarMagicMasterySelf7 = 2323u,
        WeaponExpertiseOther7 = 2324u,
        WeaponExpertiseSelf7 = 2325u,
        WeaponIgnoranceOther7 = 2326u,
        WeaponIgnoranceSelf7 = 2327u,
        DrainHealth7 = 2328u,
        DrainMana7 = 2329u,
        DrainStamina7 = 2330u,
        HealthToManaOther7 = 2331u,
        HealthToManaSelf7 = 2332u,
        HealthToStaminaOther7 = 2333u,
        HealthToStaminaSelf7 = 2334u,
        InfuseHealth7 = 2335u,
        InfuseMana7 = 2336u,
        InfuseStamina7 = 2337u,
        ManaToHealthOther7 = 2338u,
        ManaToHealthSelf7 = 2339u,
        ManaToStaminaOther7 = 2340u,
        ManaToStaminaSelf7 = 2341u,
        StaminaToHealthOther7 = 2342u,
        StaminaToHealthSelf7 = 2343u,
        StaminaToManaOther7 = 2344u,
        StaminaToManaSelf7 = 2345u,
        MaledictionSelf = 2346u,
        ConcentrationSelf = 2347u,
        BrillianceOther = 2348u,
        HieroWard = 2349u,
        DecayDurance = 2350u,
        ConsumptionDurance = 2351u,
        StasisDurance = 2352u,
        StimulationDurance = 2353u,
        PiercingDuranceLess = 2354u,
        SlashingDuranceLess = 2355u,
        BludgeoningDuranceLess = 2356u,
        FaunaPerlustration = 2357u,
        RecallLyceum = 2358u,
        PortalSendTrial1 = 2359u,
        PortalSendTrial2 = 2360u,
        PortalSendTrial3 = 2361u,
        PortalSendTrial4 = 2362u,
        PortalSendTrial5 = 2363u,
        PortalSendingHHSW = 2364u,
        PortalSendingObsidianRim = 2365u,
        BovineIntervention = 2366u,
        PortalSummonReward = 2367u,
        PortalSendingTombExit = 2368u,
        AerfalleExpulsion = 2369u,
        AerfalleColdVuln = 2370u,
        AerfalleFireVuln = 2371u,
        AerfalleFester = 2372u,
        AerfalleLifeLower = 2373u,
        AerfalleStamLower = 2374u,
        AerfalleManaLower = 2375u,
        AnnihilationGlimpse = 2376u,
        AnnihilationVision = 2377u,
        BeastMurmur = 2378u,
        BeastWhisper = 2379u,
        InstrumentalityGrip = 2380u,
        InstrumentalityTouch = 2381u,
        UnnaturalPersistence = 2382u,
        DarkFlame = 2383u,
        ArcaneRestoration = 2384u,
        Vigilance = 2385u,
        Indomitability = 2386u,
        Determination = 2387u,
        Caution = 2388u,
        Vigor = 2389u,
        Haste = 2390u,
        Prowess = 2391u,
        Serenity = 2392u,
        ForceArmor = 2393u,
        AcidShield = 2394u,
        ElectricShield = 2395u,
        FlameShield = 2396u,
        IceShield = 2397u,
        BludgeonShield = 2398u,
        PiercingShield = 2399u,
        SlashingShield = 2400u,
        PortalSendingBunnyGarden = 2401u,
        EssenceLull = 2402u,
        BalancedBreakfast = 2403u,
        CollectorAcidProtection = 2404u,
        CollectorBladeProtection = 2405u,
        CollectorBludgeoningProtection = 2406u,
        CollectorColdProtection = 2407u,
        CollectorFireProtection = 2408u,
        CollectorLightningProtection = 2409u,
        CollectorPiercingProtection = 2410u,
        Discipline = 2411u,
        EnduringCoordination = 2412u,
        EnduringFocus = 2413u,
        EnduringStoicism = 2414u,
        EyeHunter = 2415u,
        HighTensionString = 2416u,
        Obedience = 2417u,
        OccultPotence = 2418u,
        PanicAttack = 2419u,
        PanoplyQueenslayer = 2420u,
        ParalyzingFear = 2421u,
        PortalSendDryreach = 2422u,
        Precise = 2423u,
        RabbitsEye = 2424u,
        StoneWall = 2425u,
        StrongPull = 2426u,
        SugarRush = 2427u,
        TimaruShelterMagic = 2428u,
        TimaruShelterMelee = 2429u,
        TimaruShelterMissile = 2430u,
        Vivification = 2431u,
        AcidWard = 2432u,
        FlameWard = 2433u,
        FrostWard = 2434u,
        LightningWard = 2435u,
        LayingonofHands = 2436u,
        RockslideGreater = 2437u,
        RockslideLesser = 2438u,
        Rockslide = 2439u,
        StoneCliffsGreater = 2440u,
        StoneCliffsLesser = 2441u,
        StoneCliffs = 2442u,
        StrengthofEarthGreater = 2443u,
        StrengthofEarthLesser = 2444u,
        StrengthofEarth = 2445u,
        GrowthGreater = 2446u,
        GrowthLesser = 2447u,
        Growth = 2448u,
        HuntersAcumenGreater = 2449u,
        HuntersAcumenLesser = 2450u,
        HuntersAcumen = 2451u,
        ThornsGreater = 2452u,
        ThornsLesser = 2453u,
        Thorns = 2454u,
        CascadeAxeGreater = 2455u,
        CascadeAxeLesser = 2456u,
        CascadeAxe = 2457u,
        CascadeDaggerGreater = 2458u,
        CascadeDaggerLesser = 2459u,
        CascadeDagger = 2460u,
        CascadeMaceGreater = 2461u,
        CascadeMaceLesser = 2462u,
        CascadeMace = 2463u,
        CascadeSpearGreater = 2464u,
        CascadeSpearLesser = 2465u,
        CascadeSpear = 2466u,
        CascadeStaffGreater = 2467u,
        CascadeStaffLesser = 2468u,
        CascadeStaff = 2469u,
        StillWaterGreater = 2470u,
        StillWaterLesser = 2471u,
        StillWater = 2472u,
        TorrentGreater = 2473u,
        TorrentLesser = 2474u,
        Torrent = 2475u,
        PortalSendTimaru = 2476u,
        PORTALTUMEROKWARALU = 2477u,
        PORTALTUMEROKWARALUGREATER = 2478u,
        PORTALTUMEROKWARALULESSER = 2479u,
        PORTALTUMEROKWARGHA = 2480u,
        PORTALTUMEROKWARGHAGREATER = 2481u,
        PORTALTUMEROKWARGHALESSER = 2482u,
        PORTALTUMEROKWARSHO = 2483u,
        PORTALTUMEROKWARSHOGREATER = 2484u,
        PORTALTUMEROKWARSHOLESSER = 2485u,
        BLOODTHIRST = 2486u,
        SPIRITSTRIKE = 2487u,
        ElariBowFamiliarity = 2488u,
        PORTALSENDSHOUSHISE = 2489u,
        PORTALSENDHOLTS = 2490u,
        PORTALSENDHOLTW = 2491u,
        PORTALSENDSHOUSHIW = 2492u,
        PORTALSENDYARAQE = 2493u,
        PORTALSENDYARAQN = 2494u,
        PORTALSENDINGAREACHOLTW = 2495u,
        PORTALSENDINGAREACHOLTS = 2496u,
        PORTALSENDINGAREACSHOUSHISE = 2497u,
        PORTALSENDINGAREACSHOUSHIW = 2498u,
        PORTALSENDINGAREACYARAQE = 2499u,
        PORTALSENDINGAREACYARAQN = 2500u,
        CANTRIPALCHEMICALPROWESS2 = 2501u,
        CANTRIPARCANEPROWESS2 = 2502u,
        CANTRIPARMOREXPERTISE2 = 2503u,
        CANTRIPLIGHTWEAPONSAPTITUDE2 = 2504u,
        CANTRIPMISSILEWEAPONSAPTITUDE2 = 2505u,
        CANTRIPCOOKINGPROWESS2 = 2506u,
        CANTRIPCREATUREENCHANTMENTAPTITUDE2 = 2507u,
        CANTRIPCROSSBOWAPTITUDE2 = 2508u,
        CANTRIPFINESSEWEAPONSAPTITUDE2 = 2509u,
        CANTRIPDECEPTIONPROWESS2 = 2510u,
        CANTRIPFEALTY2 = 2511u,
        CANTRIPFLETCHINGPROWESS2 = 2512u,
        CANTRIPHEALINGPROWESS2 = 2513u,
        CANTRIPIMPREGNABILITY2 = 2514u,
        CANTRIPINVULNERABILITY2 = 2515u,
        CANTRIPITEMENCHANTMENTAPTITUDE2 = 2516u,
        CANTRIPITEMEXPERTISE2 = 2517u,
        CANTRIPJUMPINGPROWESS2 = 2518u,
        CANTRIPLEADERSHIP2 = 2519u,
        CANTRIPLIFEMAGICAPTITUDE2 = 2520u,
        CANTRIPLOCKPICKPROWESS2 = 2521u,
        CANTRIPMACEAPTITUDE2 = 2522u,
        CANTRIPMAGICITEMEXPERTISE2 = 2523u,
        CANTRIPMAGICRESISTANCE2 = 2524u,
        CANTRIPMANACONVERSIONPROWESS2 = 2525u,
        CANTRIPMONSTERATTUNEMENT2 = 2526u,
        CANTRIPPERSONATTUNEMENT2 = 2527u,
        CANTRIPSPEARAPTITUDE2 = 2528u,
        CANTRIPSPRINT2 = 2529u,
        CANTRIPSTAFFAPTITUDE2 = 2530u,
        CANTRIPHEAVYWEAPONSAPTITUDE2 = 2531u,
        CANTRIPTHROWNAPTITUDE2 = 2532u,
        CANTRIPUNARMEDAPTITUDE2 = 2533u,
        CANTRIPWARMAGICAPTITUDE2 = 2534u,
        CANTRIPWEAPONEXPERTISE2 = 2535u,
        CANTRIPALCHEMICALPROWESS1 = 2536u,
        CANTRIPARCANEPROWESS1 = 2537u,
        CANTRIPARMOREXPERTISE1 = 2538u,
        CANTRIPLIGHTWEAPONSAPTITUDE1 = 2539u,
        CANTRIPMISSILEWEAPONSAPTITUDE1 = 2540u,
        CANTRIPCOOKINGPROWESS1 = 2541u,
        CANTRIPCREATUREENCHANTMENTAPTITUDE1 = 2542u,
        CANTRIPCROSSBOWAPTITUDE1 = 2543u,
        CANTRIPFINESSEWEAPONSAPTITUDE1 = 2544u,
        CANTRIPDECEPTIONPROWESS1 = 2545u,
        CANTRIPFEALTY1 = 2546u,
        CANTRIPFLETCHINGPROWESS1 = 2547u,
        CANTRIPHEALINGPROWESS1 = 2548u,
        CANTRIPIMPREGNABILITY1 = 2549u,
        CANTRIPINVULNERABILITY1 = 2550u,
        CANTRIPITEMENCHANTMENTAPTITUDE1 = 2551u,
        CANTRIPITEMEXPERTISE1 = 2552u,
        CANTRIPJUMPINGPROWESS1 = 2553u,
        CANTRIPLEADERSHIP1 = 2554u,
        CANTRIPLIFEMAGICAPTITUDE1 = 2555u,
        CANTRIPLOCKPICKPROWESS1 = 2556u,
        CANTRIPMACEAPTITUDE1 = 2557u,
        CANTRIPMAGICITEMEXPERTISE1 = 2558u,
        CANTRIPMAGICRESISTANCE1 = 2559u,
        CANTRIPMANACONVERSIONPROWESS1 = 2560u,
        CANTRIPMONSTERATTUNEMENT1 = 2561u,
        CANTRIPPERSONATTUNEMENT1 = 2562u,
        CANTRIPSPEARAPTITUDE1 = 2563u,
        CANTRIPSPRINT1 = 2564u,
        CANTRIPSTAFFAPTITUDE1 = 2565u,
        CANTRIPHEAVYWEAPONSAPTITUDE1 = 2566u,
        CANTRIPTHROWNAPTITUDE1 = 2567u,
        CANTRIPUNARMEDAPTITUDE1 = 2568u,
        CANTRIPWARMAGICAPTITUDE1 = 2569u,
        CANTRIPWEAPONEXPERTISE1 = 2570u,
        CANTRIPARMOR2 = 2571u,
        CANTRIPCOORDINATION2 = 2572u,
        CANTRIPENDURANCE2 = 2573u,
        CANTRIPFOCUS2 = 2574u,
        CANTRIPQUICKNESS2 = 2575u,
        CANTRIPSTRENGTH2 = 2576u,
        CANTRIPWILLPOWER2 = 2577u,
        CANTRIPARMOR1 = 2578u,
        CANTRIPCOORDINATION1 = 2579u,
        CANTRIPENDURANCE1 = 2580u,
        CANTRIPFOCUS1 = 2581u,
        CANTRIPQUICKNESS1 = 2582u,
        CANTRIPSTRENGTH1 = 2583u,
        CANTRIPWILLPOWER1 = 2584u,
        CANTRIPACIDBANE2 = 2585u,
        CANTRIPBLOODTHIRST2 = 2586u,
        CANTRIPBLUDGEONINGBANE2 = 2587u,
        CANTRIPDEFENDER2 = 2588u,
        CANTRIPFLAMEBANE2 = 2589u,
        CANTRIPFROSTBANE2 = 2590u,
        CANTRIPHEARTTHIRST2 = 2591u,
        CANTRIPIMPENETRABILITY2 = 2592u,
        CANTRIPPIERCINGBANE2 = 2593u,
        CANTRIPSLASHINGBANE2 = 2594u,
        CANTRIPSTORMBANE2 = 2595u,
        CANTRIPSWIFTHUNTER2 = 2596u,
        CANTRIPACIDBANE1 = 2597u,
        CANTRIPBLOODTHIRST1 = 2598u,
        CANTRIPBLUDGEONINGBANE1 = 2599u,
        CANTRIPDEFENDER1 = 2600u,
        CANTRIPFLAMEBANE1 = 2601u,
        CANTRIPFROSTBANE1 = 2602u,
        CANTRIPHEARTTHIRST1 = 2603u,
        CANTRIPIMPENETRABILITY1 = 2604u,
        CANTRIPPIERCINGBANE1 = 2605u,
        CANTRIPSLASHINGBANE1 = 2606u,
        CANTRIPSTORMBANE1 = 2607u,
        CANTRIPSWIFTHUNTER1 = 2608u,
        CANTRIPACIDWARD2 = 2609u,
        CANTRIPBLUDGEONINGWARD2 = 2610u,
        CANTRIPFLAMEWARD2 = 2611u,
        CANTRIPFROSTWARD2 = 2612u,
        CANTRIPPIERCINGWARD2 = 2613u,
        CANTRIPSLASHINGWARD2 = 2614u,
        CANTRIPSTORMWARD2 = 2615u,
        CANTRIPACIDWARD1 = 2616u,
        CANTRIPBLUDGEONINGWARD1 = 2617u,
        CANTRIPFLAMEWARD1 = 2618u,
        CANTRIPFROSTWARD1 = 2619u,
        CANTRIPPIERCINGWARD1 = 2620u,
        CANTRIPSLASHINGWARD1 = 2621u,
        CANTRIPSTORMWARD1 = 2622u,
        CANTRIPHEALTHGAIN2 = 2623u,
        CANTRIPMANAGAIN2 = 2624u,
        CANTRIPSTAMINAGAIN2 = 2625u,
        CANTRIPHEALTHGAIN1 = 2626u,
        CANTRIPMANAGAIN1 = 2627u,
        CANTRIPSTAMINAGAIN1 = 2628u,
        HuntressBoon = 2629u,
        PreysReflex = 2630u,
        PortalSendingHallofHollows = 2631u,
        PortalSendingHallofHollowsSurface = 2632u,
        PortalSendingRegicideBasement = 2633u,
        PortalSendingRegicideBasementSurface = 2634u,
        PortalSummonDungeonWitshire = 2635u,
        KarenuasCurse = 2636u,
        InvokingAunTanua = 2637u,
        HeartofOak = 2638u,
        Repulsion = 2639u,
        Devourer = 2640u,
        ForcetoArms = 2641u,
        Consumption = 2642u,
        Stasis = 2643u,
        LifestoneTie1 = 2644u,
        PortalRecall = 2645u,
        PortalTie2 = 2646u,
        PortalTieRecall2 = 2647u,
        SummonSecondPortal1 = 2648u,
        SummonSecondPortal2 = 2649u,
        SummonSecondPortal3 = 2650u,
        PortalSendingSelfSacrifice = 2651u,
        PortalSendingMerciless = 2652u,
        FeebleWillpower = 2653u,
        FeebleEndurance = 2654u,
        FeebleFocus = 2655u,
        FeebleQuickness = 2656u,
        FeebleStrength = 2657u,
        FeebleCoordination = 2658u,
        ModerateCoordination = 2659u,
        ModerateEndurance = 2660u,
        ModerateFocus = 2661u,
        ModerateQuickness = 2662u,
        ModerateStrength = 2663u,
        ModerateWillpower = 2664u,
        EssenceSluice = 2665u,
        EssenceGlutton = 2666u,
        EssenceSpike = 2667u,
        NuhmudirasBenefaction = 2668u,
        NuhmudirasBestowment = 2669u,
        NuhmudirasEndowment = 2670u,
        PortalSendingLabyrinthRewards = 2671u,
        MartineRing1 = 2672u,
        MartineRing2 = 2673u,
        MartineStrike = 2674u,
        FeebleAxeAptitude = 2675u,
        FeebleBowAptitude = 2676u,
        FeebleCrossbowAptitude = 2677u,
        FeebleDaggerAptitude = 2678u,
        FeebleMaceAptitude = 2679u,
        FeebleManaConversionProwess = 2680u,
        FeebleSpearAptitude = 2681u,
        FeebleStaffAptitude = 2682u,
        FeebleSwordAptitude = 2683u,
        FeebleThrownAptitude = 2684u,
        FeebleUnarmedAptitude = 2685u,
        ModerateAxeAptitude = 2686u,
        ModerateBowAptitude = 2687u,
        ModerateCrossbowAptitude = 2688u,
        ModerateDaggerAptitude = 2689u,
        ModerateMaceAptitude = 2690u,
        ModerateManaConversionProwess = 2691u,
        ModerateSpearAptitude = 2692u,
        ModerateStaffAptitude = 2693u,
        ModerateSwordAptitude = 2694u,
        ModerateThrownAptitude = 2695u,
        ModerateUnarmedAptitude = 2696u,
        MartineDebuff = 2697u,
        MartineDegeneration = 2698u,
        AuroricWhip = 2699u,
        CorrosiveCloud = 2700u,
        ElementalFury1 = 2701u,
        ElementalFury2 = 2702u,
        ElementalFury3 = 2703u,
        ElementalFury4 = 2704u,
        ElementalistsBoon = 2705u,
        ElementalWard = 2706u,
        PortalSendingGaerlan = 2707u,
        StasisField = 2708u,
        SummonPortalFloater = 2709u,
        VolcanicBlast = 2710u,
        AcidArc1 = 2711u,
        AcidArc2 = 2712u,
        AcidArc3 = 2713u,
        AcidArc4 = 2714u,
        AcidArc5 = 2715u,
        AcidArc6 = 2716u,
        AcidArc7 = 2717u,
        ForceArc1 = 2718u,
        ForceArc2 = 2719u,
        ForceArc3 = 2720u,
        ForceArc4 = 2721u,
        ForceArc5 = 2722u,
        ForceArc6 = 2723u,
        ForceArc7 = 2724u,
        FrostArc1 = 2725u,
        FrostArc2 = 2726u,
        FrostArc3 = 2727u,
        FrostArc4 = 2728u,
        FrostArc5 = 2729u,
        FrostArc6 = 2730u,
        FrostArc7 = 2731u,
        LightningArc1 = 2732u,
        LightningArc2 = 2733u,
        LightningArc3 = 2734u,
        LightningArc4 = 2735u,
        LightningArc5 = 2736u,
        LightningArc6 = 2737u,
        LightningArc7 = 2738u,
        FlameArc1 = 2739u,
        FlameArc2 = 2740u,
        FlameArc3 = 2741u,
        FlameArc4 = 2742u,
        FlameArc5 = 2743u,
        FlameArc6 = 2744u,
        FlameArc7 = 2745u,
        ShockArc1 = 2746u,
        ShockArc2 = 2747u,
        ShockArc3 = 2748u,
        ShockArc4 = 2749u,
        ShockArc5 = 2750u,
        ShockArc6 = 2751u,
        ShockArc7 = 2752u,
        BladeArc1 = 2753u,
        BladeArc2 = 2754u,
        BladeArc3 = 2755u,
        BladeArc4 = 2756u,
        BladeArc5 = 2757u,
        BladeArc6 = 2758u,
        BladeArc7 = 2759u,
        HealthBolt1 = 2760u,
        HealthBolt2 = 2761u,
        HealthBolt3 = 2762u,
        HealthBolt4 = 2763u,
        HealthBolt5 = 2764u,
        HealthBolt6 = 2765u,
        HealthBolt7 = 2766u,
        StaminaBolt1 = 2767u,
        StaminaBolt2 = 2768u,
        StaminaBolt3 = 2769u,
        StaminaBolt4 = 2770u,
        StaminaBolt5 = 2771u,
        StaminaBolt6 = 2772u,
        StaminaBolt7 = 2773u,
        ManaBolt1 = 2774u,
        ManaBolt2 = 2775u,
        ManaBolt3 = 2776u,
        ManaBolt4 = 2777u,
        ManaBolt5 = 2778u,
        ManaBolt6 = 2779u,
        ManaBolt7 = 2780u,
        LesserElementalFuryAcid = 2781u,
        LesserElementalFuryFlame = 2782u,
        LesserElementalFuryFrost = 2783u,
        LesserElementalFuryLightning = 2784u,
        LesserStasisField = 2785u,
        Madness = 2786u,
        Supremacy = 2787u,
        HarbingerManaBlight = 2788u,
        HarbingerSmite = 2789u,
        HarbingerWeaknessOther = 2790u,
        RollingDeathAcid = 2791u,
        RollingDeathFlame = 2792u,
        RollingDeathFrost = 2793u,
        RollingDeathLightning = 2794u,
        PortalSendingCitadelLibrary = 2795u,
        PortalSendingCitadelSurface = 2796u,
        PortalSendingProvingGrounds2Extreme = 2797u,
        PortalSendingProvingGrounds2High = 2798u,
        PortalSendingProvingGrounds2Low = 2799u,
        PortalSendingProvingGrounds2Mid = 2800u,
        PortalSendingProvingGrounds3Extreme = 2801u,
        PortalSendingProvingGrounds3High = 2802u,
        PortalSendingProvingGrounds3Low = 2803u,
        PortalSendingProvingGrounds3Mid = 2804u,
        PortalSendingProvingGrounds4Extreme = 2805u,
        PortalSendingProvingGrounds4High = 2806u,
        PortalSendingProvingGrounds4Low = 2807u,
        PortalSendingProvingGrounds4Mid = 2808u,
        ModerateArcaneProwess = 2809u,
        ModerateLifeMagicAptitude = 2810u,
        ModerateMagicResistance = 2811u,
        ModerateWarMagicAptitude = 2812u,
        RecallLethe = 2813u,
        ImpiousCurse = 2814u,
        FireworkOutBlack1 = 2815u,
        FireworkOutBlack2 = 2816u,
        FireworkOutBlack3 = 2817u,
        FireworkOutBlack4 = 2818u,
        FireworkOutBlack5 = 2819u,
        FireworkOutBlack6 = 2820u,
        FireworkOutBlack7 = 2821u,
        FireworkOutBlue1 = 2822u,
        FireworkOutBlue2 = 2823u,
        FireworkOutBlue3 = 2824u,
        FireworkOutBlue4 = 2825u,
        FireworkOutBlue5 = 2826u,
        FireworkOutBlue6 = 2827u,
        FireworkOutBlue7 = 2828u,
        FireworkOutGreen1 = 2829u,
        FireworkOutGreen2 = 2830u,
        FireworkOutGreen3 = 2831u,
        FireworkOutGreen4 = 2832u,
        FireworkOutGreen5 = 2833u,
        FireworkOutGreen6 = 2834u,
        FireworkOutGreen7 = 2835u,
        FireworkOutOrange1 = 2836u,
        FireworkOutOrange2 = 2837u,
        FireworkOutOrange3 = 2838u,
        FireworkOutOrange4 = 2839u,
        FireworkOutOrange5 = 2840u,
        FireworkOutOrange6 = 2841u,
        FireworkOutOrange7 = 2842u,
        FireworkOutPurple1 = 2843u,
        FireworkOutPurple2 = 2844u,
        FireworkOutPurple3 = 2845u,
        FireworkOutPurple4 = 2846u,
        FireworkOutPurple5 = 2847u,
        FireworkOutPurple6 = 2848u,
        FireworkOutPurple7 = 2849u,
        FireworkOutRed1 = 2850u,
        FireworkOutRed2 = 2851u,
        FireworkOutRed3 = 2852u,
        FireworkOutRed4 = 2853u,
        FireworkOutRed5 = 2854u,
        FireworkOutRed6 = 2855u,
        FireworkOutRed7 = 2856u,
        FireworkOutWhite1 = 2857u,
        FireworkOutWhite2 = 2858u,
        FireworkOutWhite3 = 2859u,
        FireworkOutWhite4 = 2860u,
        FireworkOutWhite5 = 2861u,
        FireworkOutWhite6 = 2862u,
        FireworkOutWhite7 = 2863u,
        FireworkOutYellow1 = 2864u,
        FireworkOutYellow2 = 2865u,
        FireworkOutYellow3 = 2866u,
        FireworkOutYellow4 = 2867u,
        FireworkOutYellow5 = 2868u,
        FireworkOutYellow6 = 2869u,
        FireworkOutYellow7 = 2870u,
        FireworkUpBlack1 = 2871u,
        FireworkUpBlack2 = 2872u,
        FireworkUpBlack3 = 2873u,
        FireworkUpBlack4 = 2874u,
        FireworkUpBlack5 = 2875u,
        FireworkUpBlack6 = 2876u,
        FireworkUpBlack7 = 2877u,
        FireworkUpBlue1 = 2878u,
        FireworkUpBlue2 = 2879u,
        FireworkUpBlue3 = 2880u,
        FireworkUpBlue4 = 2881u,
        FireworkUpBlue5 = 2882u,
        FireworkUpBlue6 = 2883u,
        FireworkUpBlue7 = 2884u,
        FireworkUpGreen1 = 2885u,
        FireworkUpGreen2 = 2886u,
        FireworkUpGreen3 = 2887u,
        FireworkUpGreen4 = 2888u,
        FireworkUpGreen5 = 2889u,
        FireworkUpGreen6 = 2890u,
        FireworkUpGreen7 = 2891u,
        FireworkUpOrange1 = 2892u,
        FireworkUpOrange2 = 2893u,
        FireworkUpOrange3 = 2894u,
        FireworkUpOrange4 = 2895u,
        FireworkUpOrange5 = 2896u,
        FireworkUpOrange6 = 2897u,
        FireworkUpOrange7 = 2898u,
        FireworkUpPurple1 = 2899u,
        FireworkUpPurple2 = 2900u,
        FireworkUpPurple3 = 2901u,
        FireworkUpPurple4 = 2902u,
        FireworkUpPurple5 = 2903u,
        FireworkUpPurple6 = 2904u,
        FireworkUpPurple7 = 2905u,
        FireworkUpRed1 = 2906u,
        FireworkUpRed2 = 2907u,
        FireworkUpRed3 = 2908u,
        FireworkUpRed4 = 2909u,
        FireworkUpRed5 = 2910u,
        FireworkUpRed6 = 2911u,
        FireworkUpRed7 = 2912u,
        FireworkUpWhite1 = 2913u,
        FireworkUpWhite2 = 2914u,
        FireworkUpWhite3 = 2915u,
        FireworkUpWhite4 = 2916u,
        FireworkUpWhite5 = 2917u,
        FireworkUpWhite6 = 2918u,
        FireworkUpWhite7 = 2919u,
        FireworkUpYellow1 = 2920u,
        FireworkUpYellow2 = 2921u,
        FireworkUpYellow3 = 2922u,
        FireworkUpYellow4 = 2923u,
        FireworkUpYellow5 = 2924u,
        FireworkUpYellow6 = 2925u,
        FireworkUpYellow7 = 2926u,
        FireworkOldSchool = 2927u,
        TuskerHide = 2928u,
        TuskerMight = 2929u,
        TuskerSkin = 2930u,
        TuskerIslandRecall = 2931u,
        TuskerLeap = 2932u,
        TuskerSprint = 2933u,
        TuskerFists = 2934u,
        PortalSendingMowen = 2935u,
        PortalSendingTuskerIsland = 2936u,
        ModerateImpregnability = 2937u,
        ModerateInvulnerability = 2938u,
        PortalSendingTempleEnlightenment = 2939u,
        PortalSendingTempleForgetfulness = 2940u,
        UlgrimsRecall = 2941u,
        PortalSendAbandonedMine = 2942u,
        SingularityIslandRecall = 2943u,
        PortalSendingCrystalMindsWarehseOK = 2944u,
        PortalSendingCrystalMindsWarehseRobbed = 2945u,
        ModerateCreatureMagicAptitude = 2946u,
        DispelAllBadOther6NoManaCon = 2947u,
        HieroWardGreat = 2948u,
        LightbringersWay = 2949u,
        MaidensKiss = 2950u,
        PortalSendingKnorr = 2951u,
        PortalSendingKnorr2 = 2952u,
        PortalSendingKnorr3 = 2953u,
        PortalSendingKnorrBarracks = 2954u,
        PortalSendingKnorrForge = 2955u,
        PortalSendingKnorrResearch = 2956u,
        PortalSendingKnorrSeat = 2957u,
        BlessingofthePriestess = 2958u,
        MarkofthePriestess = 2959u,
        BludgeoningDurance = 2960u,
        PiercingDurance = 2961u,
        SlashingDurance = 2962u,
        CunningHunter = 2963u,
        MarkHunter = 2964u,
        MurderousIntent = 2965u,
        MurderousThirst = 2966u,
        SpeedHunter = 2967u,
        VisionHunter = 2968u,
        MothersBlessing = 2969u,
        HuntersLash = 2970u,
        PortalSendingOswaldLogic = 2971u,
        PortalSendingOswaldsRoom = 2972u,
        PortalSendingSecretLair = 2973u,
        PortalSendingVagabondPassed = 2974u,
        ModerateItemEnchantmentAptitude = 2975u,
        OlthoiAcidBlast = 2976u,
        PortalSendHiddenWarehouseROT2 = 2977u,
        DispelAllNeutralOther7 = 2978u,
        DestinyWind = 2979u,
        EndlessVigor = 2980u,
        HealFellow1 = 2981u,
        AlchemyMasteryFellow1 = 2982u,
        DispelLifeBadFellow1 = 2983u,
        FellowPortalSendHoltS = 2984u,
        Ardence = 2985u,
        Vim = 2986u,
        Volition = 2987u,
        PortalSendingCageNoir1 = 2988u,
        PortalSendingDarkNoir1 = 2989u,
        PortalSendingTreeNoir1 = 2990u,
        WintersKiss = 2991u,
        Depletion = 2992u,
        GraceUnicorn = 2993u,
        Plague = 2994u,
        PowerDragon = 2995u,
        Scourge = 2996u,
        SplendorFireBird = 2997u,
        WrathPuppeteer = 2998u,
        EnduranceAbyss = 2999u,
        IreDarkPrince = 3000u,
        PuppetString = 3001u,
        WillpowerQuiddity = 3002u,
        DarkWave = 3003u,
        PuppetStrings = 3004u,
        Dispersion = 3005u,
        Foresight = 3006u,
        UncannyDodge = 3007u,
        Finesse = 3008u,
        Thew = 3009u,
        Zeal = 3010u,
        EndlessSight = 3011u,
        FarSight = 3012u,
        OasisHealthRegen = 3013u,
        OasisManaRegen = 3014u,
        OasisStaminaRegen = 3015u,
        RaptorSight = 3016u,
        PortalSendingCandethKeepBattleDungeon = 3017u,
        PortalSendingFortTethBattleDungeon = 3018u,
        PortalSendingNantoBattleDungeon = 3019u,
        PortalSendingPlateauBattleDungeon = 3020u,
        PortalSendingQalabarBattleDungeon = 3021u,
        PortalSendingTouTouBattleDungeon = 3022u,
        PortalSendingXarabydunBattleDungeon = 3023u,
        PortalSendingYaraqBattleDungeon = 3024u,
        Shriek = 3025u,
        PortalSendingCandethKeepBattleDungeonPKL = 3026u,
        PortalSendingFortTethBattleDungeonPKL = 3027u,
        PortalSendingNantoBattleDungeonPKL = 3028u,
        PortalSendingPlateauBattleDungeonPKL = 3029u,
        PortalSendingQalabarBattleDungeonPKL = 3030u,
        PortalSendingTouTouBattleDungeonPKL = 3031u,
        PortalSendingXarabydunBattleDungeonPKL = 3032u,
        PortalSendingYaraqBattleDungeonPKL = 3033u,
        BenedictionImmortality = 3034u,
        ClosingGreatDivide = 3035u,
        ColdGripGrave = 3036u,
        DeathsCall = 3037u,
        DeathsEmbrace = 3038u,
        DeathsFeast = 3039u,
        DeathsKiss = 3040u,
        EssenceDissolution = 3041u,
        GripDeath = 3042u,
        KissGrave = 3043u,
        LesserBenedictionImmortality = 3044u,
        LesserClosingGreatDivide = 3045u,
        LesserMistsBur = 3046u,
        MatronsBarb = 3047u,
        MinorBenedictionImmortality = 3048u,
        MinorClosingGreatDivide = 3049u,
        MinorMistsBur = 3050u,
        MireFoot = 3051u,
        MistsBur = 3052u,
        ParalyzingTouch = 3053u,
        SoulDissolution = 3054u,
        Asphyxiation = 3055u,
        DeathsVice = 3056u,
        Enervation = 3057u,
        GreaterAsphyxiation = 3058u,
        GreaterEnervation = 3059u,
        GreaterPoisonBlood = 3060u,
        GreaterTaintMana = 3061u,
        LesserAsphyxation = 3062u,
        LesserEnervation = 3063u,
        LesserPoisonBlood = 3064u,
        LesserTaintMana = 3065u,
        LesserWardRebirth = 3066u,
        MatronsCurse = 3067u,
        MinorWardRebirth = 3068u,
        PoisonBlood = 3069u,
        TaintMana = 3070u,
        WardRebirth = 3071u,
        PortalSendingFellowshipTempleGuardians = 3072u,
        PortalSendingFellowshipTempleMatron = 3073u,
        FeebleFleshBruised = 3074u,
        FeebleFleshCloth = 3075u,
        FeebleFleshExposed = 3076u,
        FeebleFleshFlint = 3077u,
        FeebleFleshWeak = 3078u,
        FeebleThinSkin = 3079u,
        FleshBruised = 3080u,
        FleshCloth = 3081u,
        FleshExposed = 3082u,
        FleshFlint = 3083u,
        FleshWeak = 3084u,
        LesserFleshBruised = 3085u,
        LesserFleshCloth = 3086u,
        LesserFleshExposed = 3087u,
        LesserFleshFlint = 3088u,
        LesserFleshWeak = 3089u,
        LesserThinSkin = 3090u,
        ThinSkin = 3091u,
        LesserSkinFiazhat = 3092u,
        MinorSkinFiazhat = 3093u,
        SkinFiazhat = 3094u,
        PortalSendingTempleApprentice = 3095u,
        PortalSendingTempleConsort = 3096u,
        PortalSendingTempleGuardOrange = 3097u,
        PortalSendingTempleGuardRed = 3098u,
        PortalSendingTempleGuardYellow = 3099u,
        PortalSendingTempleIxir = 3100u,
        PortalSendingTempleIxirZiCrypt = 3101u,
        PortalSendingTempleKivikLir = 3102u,
        PortalSendingTempleMatriarch = 3103u,
        PortalSendingTempleTestsOrange = 3104u,
        PortalSendingTempleTestsRed = 3105u,
        PortalSendingTempleTestsYellow = 3106u,
        FlaySoul = 3107u,
        GreaterFlaySoul = 3108u,
        GreaterLiquefyFlesh = 3109u,
        GreaterSearFlesh = 3110u,
        GreaterSoulHammer = 3111u,
        GreaterSoulSpike = 3112u,
        LesserFlaySoul = 3113u,
        LesserLiquefyFlesh = 3114u,
        LesserSearFlesh = 3115u,
        LesserSoulHammer = 3116u,
        LesserSoulSpike = 3117u,
        LiquefyFlesh = 3118u,
        SearFlesh = 3119u,
        SoulHammer = 3120u,
        SoulSpike = 3121u,
        SacrificialEdge = 3122u,
        SacrificialEdges = 3123u,
        BlightMana = 3124u,
        EnervateBeing = 3125u,
        PoisonHealth = 3126u,
        FellWind = 3127u,
        InfectedBlood = 3128u,
        InfirmedMana = 3129u,
        PortalSendingFellowshipLiazkBurun40 = 3130u,
        PortalSendingFellowshipLiazkBurun60 = 3131u,
        PortalSendingFellowshipLiazkBurun80 = 3132u,
        PortalSendingFellowshipLiazkBurun100 = 3133u,
        PortalSendingLiazkBurun40 = 3134u,
        PortalSendingLiazkBurun60 = 3135u,
        PortalSendingLiazkBurun80 = 3136u,
        PortalSendingLiazkBurun100 = 3137u,
        PortalSendingLiazkItziAntechamber = 3138u,
        PortalSendingLiazkItziCrypt = 3139u,
        PortalSendingLiazkJump40 = 3140u,
        PortalSendingLiazkJump60 = 3141u,
        PortalSendingLiazkJump80 = 3142u,
        PortalSendingLiazkJump100 = 3143u,
        PortalSendingLiazkSpirits40 = 3144u,
        PortalSendingLiazkSpirits60 = 3145u,
        PortalSendingLiazkSpirits80 = 3146u,
        PortalSendingLiazkSpirits100 = 3147u,
        PortalSendingLiazkTest40 = 3148u,
        PortalSendingLiazkTest60 = 3149u,
        PortalSendingLiazkTest80 = 3150u,
        PortalSendingLiazkTest100 = 3151u,
        InferiorScytheAegis = 3152u,
        LesserScytheAegis = 3153u,
        ScytheAegis = 3154u,
        CoordinationFellowship4 = 3155u,
        CoordinationFellowship5 = 3156u,
        CoordinationFellowship6 = 3157u,
        CoordinationFellowship7 = 3158u,
        EnduranceFellowship4 = 3159u,
        EnduranceFellowship5 = 3160u,
        EnduranceFellowship6 = 3161u,
        EnduranceFellowship7 = 3162u,
        FocusFellowship4 = 3163u,
        FocusFellowship5 = 3164u,
        FocusFellowship6 = 3165u,
        FocusFellowship7 = 3166u,
        QuicknessFellowship4 = 3167u,
        QuicknessFellowship5 = 3168u,
        QuicknessFellowship6 = 3169u,
        QuicknessFellowship7 = 3170u,
        SelfFellowship4 = 3171u,
        SelfFellowship5 = 3172u,
        SelfFellowship6 = 3173u,
        SelfFellowship7 = 3174u,
        StrengthFellowship4 = 3175u,
        StrengthFellowship5 = 3176u,
        StrengthFellowship6 = 3177u,
        StrengthFellowship7 = 3178u,
        DispelAllBadOther7 = 3179u,
        DispelAllBadSelf7 = 3180u,
        DispelAllGoodOther7 = 3181u,
        DispelAllGoodSelf7 = 3182u,
        DispelAllNeutralSelf7 = 3183u,
        DispelCreatureBadOther7 = 3184u,
        DispelCreatureBadSelf7 = 3185u,
        DispelCreatureGoodOther7 = 3186u,
        DispelCreatureGoodSelf7 = 3187u,
        DispelCreatureNeutralOther7 = 3188u,
        DispelCreatureNeutralSelf7 = 3189u,
        DispelItemBadOther7 = 3190u,
        DispelItemGoodOther7 = 3191u,
        DispelItemNeutralOther7 = 3192u,
        DispelLifeBadOther7 = 3193u,
        DispelLifeBadSelf7 = 3194u,
        DispelLifeGoodOther7 = 3195u,
        DispelLifeGoodSelf7 = 3196u,
        DispelLifeNeutralOther7 = 3197u,
        DispelLifeNeutralSelf7 = 3198u,
        CantripHermeticLink1 = 3199u,
        CantripHermeticLink2 = 3200u,
        CantripHermeticLinkFeeble = 3201u,
        CantripHermeticLinkModerate = 3202u,
        DispelAllBadOther7NoManaCon = 3203u,
        GolemHunterHealthHigh = 3204u,
        GolemHunterHealthLow = 3205u,
        GolemHunterManaHigh = 3206u,
        GolemHunterManaLow = 3207u,
        GolemHunterStaminaHigh = 3208u,
        GolemHunterStaminaLow = 3209u,
        Agitate = 3210u,
        Annoyance = 3211u,
        GuiltTrip = 3212u,
        Heartache = 3213u,
        Sorrow = 3214u,
        Underfoot = 3215u,
        PortalSendingForbiddenCatacombs = 3216u,
        CascadeAtlatl = 3217u,
        CascadeAtlatlGreater = 3218u,
        CascadeAtlatlLesser = 3219u,
        CascadeBow = 3220u,
        CascadeBowGreater = 3221u,
        CascadeBowLesser = 3222u,
        CascadeManaC = 3223u,
        CascadeManaCGreater = 3224u,
        CascadeManaCLesser = 3225u,
        CascadeSword = 3226u,
        CascadeSwordGreater = 3227u,
        CascadeSwordLesser = 3228u,
        CascadeUA = 3229u,
        CascadeUAGreater = 3230u,
        CascadeUALesser = 3231u,
        CascadeXBow = 3232u,
        CascadeXBowGreater = 3233u,
        CascadeXBowLesser = 3234u,
        DarkPower = 3235u,
        RestorativeDraught = 3236u,
        Fanaticism = 3237u,
        PortalSendingNannerIsland = 3238u,
        InsightKhe = 3239u,
        WisdomKhe = 3240u,
        FlameBurst = 3241u,
        ChoriziteWeave = 3242u,
        Consecration = 3243u,
        DevineManipulation = 3244u,
        SacrosanctTouch = 3245u,
        CantripRegenAdja = 3246u,
        CantripRejuvAdja = 3247u,
        CantripRenewAdja = 3248u,
        ManaRenewalAdja = 3249u,
        CantripSpiritThirst2 = 3250u,
        CantripSpiritThirst1 = 3251u,
        ModerateSpiritThirst = 3252u,
        SpiritDrinkerSelf1 = 3253u,
        SpiritDrinkerSelf2 = 3254u,
        SpiritDrinkerSelf3 = 3255u,
        SpiritDrinkerSelf4 = 3256u,
        SpiritDrinkerSelf5 = 3257u,
        SpiritDrinkerSelf6 = 3258u,
        SpiritDrinkerSelf7 = 3259u,
        SpiritLoather1 = 3260u,
        SpiritLoather2 = 3261u,
        SpiritLoather3 = 3262u,
        SpiritLoather4 = 3263u,
        SpiritLoather5 = 3264u,
        SpiritLoather6 = 3265u,
        SpiritLoather7 = 3266u,
        BitBetweenTeeth = 3267u,
        BitingBonds = 3268u,
        UnderTheLash = 3269u,
        PortalSendingHezhitFight1 = 3270u,
        PortalSendingHezhitFight2 = 3271u,
        PortalSendingHezhitFight3 = 3272u,
        PortalSendingHezhitPrison1 = 3273u,
        PortalSendingHezhitPrison2 = 3274u,
        PortalSendingHezhitPrison3 = 3275u,
        PortalSendingHezhitPrison4 = 3276u,
        PortalSendingHezhitPrison5 = 3277u,
        PortalSendingHezhitPrison6 = 3278u,
        PortalSendingHizkRiAntechamber = 3279u,
        PortalSendingHizkRiCorridor = 3280u,
        PortalSendingHizkRiGauntlet60 = 3281u,
        PortalSendingHizkRiGauntlet80 = 3282u,
        PortalSendingHizkRiGauntlet100 = 3283u,
        PortalSendingHizkRiHezhit = 3284u,
        PortalSendingHizkRiJrvik = 3285u,
        PortalSendingHizkRiWell60 = 3286u,
        PortalSendingHizkRiWell80 = 3287u,
        PortalSendingHizkRiWell100 = 3288u,
        PortalSendingHizkRiZixki = 3289u,
        PortalSendingJrvikFight1 = 3290u,
        PortalSendingJrvikFight2 = 3291u,
        PortalSendingJrvikFight3 = 3292u,
        PortalSendingJrvikPrison1 = 3293u,
        PortalSendingJrvikPrison2 = 3294u,
        PortalSendingJrvikPrison3 = 3295u,
        PortalSendingJrvikPrison4 = 3296u,
        PortalSendingJrvikPrison5 = 3297u,
        PortalSendingJrvikPrison6 = 3298u,
        PortalSendingZixkFight1 = 3299u,
        PortalSendingZixkFight2 = 3300u,
        PortalSendingZixkFight3 = 3301u,
        PortalSendingZixkPrison1 = 3302u,
        PortalSendingZixkPrison2 = 3303u,
        PortalSendingZixkPrison3 = 3304u,
        PortalSendingZixkPrison4 = 3305u,
        PortalSendingZixkPrison5 = 3306u,
        PortalSendingZixkPrison6 = 3307u,
        FlangeAegis = 3308u,
        InferiorFlangeAegis = 3309u,
        InferiorLanceAegis = 3310u,
        LanceAegis = 3311u,
        LesserFlangeAegis = 3312u,
        LesserLanceAegis = 3313u,
        ChainedToTheWall = 3314u,
        PortalSendingHizkRiGuruk60 = 3315u,
        PortalSendingHizkRiGuruk80 = 3316u,
        PortalSendingHizkRiGuruk100 = 3317u,
        PortalSendingHizkRiRewards = 3318u,
        PortalSendingTempleIzjiQo = 3319u,
        AcidProtectionFellowship4 = 3320u,
        AcidProtectionFellowship5 = 3321u,
        AcidProtectionFellowship6 = 3322u,
        AcidProtectionFellowship7 = 3323u,
        BladeProtectionFellowship4 = 3324u,
        BladeProtectionFellowship5 = 3325u,
        BladeProtectionFellowship6 = 3326u,
        BladeProtectionFellowship7 = 3327u,
        BludgeonProtectionFellowship4 = 3328u,
        BludgeonProtectionFellowship5 = 3329u,
        BludgeonProtectionFellowship6 = 3330u,
        BludgeonProtectionFellowship7 = 3331u,
        ColdProtectionFellowship4 = 3332u,
        ColdProtectionFellowship5 = 3333u,
        ColdProtectionFellowship6 = 3334u,
        ColdProtectionFellowship7 = 3335u,
        FireProtectionFellowship4 = 3336u,
        FireProtectionFellowship5 = 3337u,
        FireProtectionFellowship6 = 3338u,
        FireProtectionFellowship7 = 3339u,
        LightningProtectionFellowship4 = 3340u,
        LightningProtectionFellowship5 = 3341u,
        LightningProtectionFellowship6 = 3342u,
        LightningProtectionFellowship7 = 3343u,
        PierceProtectionFellowship4 = 3344u,
        PierceProtectionFellowship5 = 3345u,
        PierceProtectionFellowship6 = 3346u,
        PierceProtectionFellowship7 = 3347u,
        ImpregnabilityFellowship4 = 3348u,
        ImpregnabilityFellowship5 = 3349u,
        ImpregnabilityFellowship6 = 3350u,
        ImpregnabilityFellowship7 = 3351u,
        InvulnerabilityFellowship4 = 3352u,
        InvulnerabilityFellowship5 = 3353u,
        InvulnerabilityFellowship6 = 3354u,
        InvulnerabilityFellowship7 = 3355u,
        MagicResistanceFellowship4 = 3356u,
        MagicResistanceFellowship5 = 3357u,
        MagicResistanceFellowship6 = 3358u,
        MagicResistanceFellowship7 = 3359u,
        PortalSendingBurunShrine = 3360u,
        ArtDestruction = 3361u,
        BlessingHorn = 3362u,
        BlessingScale = 3363u,
        BlessingWing = 3364u,
        GiftEnhancement = 3365u,
        HeartsTouch = 3366u,
        LeapingLegs = 3367u,
        MagesUnderstanding = 3368u,
        OntheRun = 3369u,
        PowerEnchantment = 3370u,
        lifegivergreater = 3371u,
        DebilitatingSpore = 3372u,
        DiseasedAir = 3373u,
        DrainHealthKivikLir = 3374u,
        FungalBloom = 3375u,
        LesserVisionBeyondTheGrave = 3376u,
        MinorVisionBeyondTheGrave = 3377u,
        VisionBeyondTheGrave = 3378u,
        VitaeGreaterKivikLir = 3379u,
        VitaeKivikLir = 3380u,
        DebilitatingSporeFellowship = 3381u,
        DiseasedAirFellowship = 3382u,
        FungalBloomFellowship = 3383u,
        CreatureEnchantmentMasteryFellow4 = 3384u,
        CreatureEnchantmentMasteryFellow5 = 3385u,
        CreatureEnchantmentMasteryFellow6 = 3386u,
        CreatureEnchantmentMasteryFellow7 = 3387u,
        ItemEnchantmentMasteryFellow4 = 3388u,
        ItemEnchantmentMasteryFellow5 = 3389u,
        ItemEnchantmentMasteryFellow6 = 3390u,
        ItemEnchantmentMasteryFellow7 = 3391u,
        LifeMagicMasteryFellow4 = 3392u,
        LifeMagicMasteryFellow5 = 3393u,
        LifeMagicMasteryFellow6 = 3394u,
        LifeMagicMasteryFellow7 = 3395u,
        ManaConversionMasteryFellow4 = 3396u,
        ManaConversionMasteryFellow5 = 3397u,
        ManaConversionMasteryFellow6 = 3398u,
        ManaConversionMasteryFellow7 = 3399u,
        WarMagicMasteryFellow4 = 3400u,
        WarMagicMasteryFellow5 = 3401u,
        WarMagicMasteryFellow6 = 3402u,
        WarMagicMasteryFellow7 = 3403u,
        EvilThirst = 3404u,
        GiftoftheFiazhat = 3405u,
        KivikLirTimerSpell = 3406u,
        LesserEvilThirst = 3407u,
        LesserGiftoftheFiazhat = 3408u,
        MinorEvilThirst = 3409u,
        MinorGiftoftheFiazhat = 3410u,
        PortalSendingMorgluukReward = 3411u,
        PortalSendingKivikLirAntechamber = 3412u,
        PortalSendingKivikLirArena60 = 3413u,
        PortalSendingKivikLirArena80 = 3414u,
        PortalSendingKivikLirArena100 = 3415u,
        PortalSendingKivikLirBoss60 = 3416u,
        PortalSendingKivikLirBoss80 = 3417u,
        PortalSendingKivikLirBoss100 = 3418u,
        PortalSendingKivikLirExit = 3419u,
        PortalSendingKivikLirHaven60 = 3420u,
        PortalSendingKivikLirHaven80 = 3421u,
        PortalSendingKivikLirHaven100 = 3422u,
        PortalSendingKivikLirSplittingHalls = 3423u,
        PortalSendingKivikLirSplittingHallsUpper = 3424u,
        PortalSendingKivikLirStagingArea = 3425u,
        GreaterWhithering = 3426u,
        LesserWhithering = 3427u,
        Whithering = 3428u,
        ImperilKivikLir = 3429u,
        InferiorScourgeAegis = 3430u,
        LesserScourgeAegis = 3431u,
        ScourgeAegis = 3432u,
        Decay = 3433u,
        EyesBeyondtheMist = 3434u,
        GreaterMucorBlight = 3435u,
        LesserEyesBeyondtheMist = 3436u,
        LesserMucorBlight = 3437u,
        MinorEyesBeyondtheMist = 3438u,
        MucorBlight = 3439u,
        LugianHealth = 3440u,
        LugianInsight = 3441u,
        LugianStamina = 3442u,
        SwampBlight = 3443u,
        JusticeSleepingOne = 3444u,
        PurgeSleepingOne = 3445u,
        SwampWrath = 3446u,
        ChokingSporeCloud = 3447u,
        MassBloodAffliction = 3448u,
        MassBloodDisease = 3449u,
        MoldSporeCloud = 3450u,
        ConcussiveBelch = 3451u,
        ConcussiveWail = 3452u,
        FeelunBlight = 3453u,
        FeelunWrath = 3454u,
        KoruuCloud = 3455u,
        KoruuWrath = 3456u,
        ManaBolt = 3457u,
        ManaPurge = 3458u,
        MucorCloud = 3459u,
        MucorWrath = 3460u,
        BatterFlesh = 3461u,
        CankerFlesh = 3462u,
        CharFlesh = 3463u,
        NumbFlesh = 3464u,
        BloodAffliction = 3465u,
        BloodDisease = 3466u,
        ChokingSpores = 3467u,
        MoldSpores = 3468u,
        ParasiticAffliction = 3469u,
        ManaRenewalFellowship4 = 3470u,
        ManaRenewalFellowship5 = 3471u,
        ManaRenewalFellowship6 = 3472u,
        ManaRenewalFellowship7 = 3473u,
        RegenerationFellowship4 = 3474u,
        RegenerationFellowship5 = 3475u,
        RegenerationFellowship6 = 3476u,
        RegenerationFellowship7 = 3477u,
        RejuvenationFellowship4 = 3478u,
        RejuvenationFellowship5 = 3479u,
        RejuvenationFellowship6 = 3480u,
        RejuvenationFellowship7 = 3481u,
        PortalSendingIzjiQoAntechamber = 3482u,
        PortalSendingIzjiQoGauntlet60 = 3483u,
        PortalSendingIzjiQoGauntlet80 = 3484u,
        PortalSendingIzjiQoGauntlet100 = 3485u,
        PortalSendingIzjiQoReceivingChamber = 3486u,
        PortalSendingIzjiQoReceivingChamber1 = 3487u,
        PortalSendingIzjiQoReceivingChamber2 = 3488u,
        PortalSendingIzjiQoReceivingChamber3 = 3489u,
        PortalSendingIzjiQoReceivingChamber4 = 3490u,
        PortalSendingIzjiQoReceivingChamber5 = 3491u,
        PortalSendingIzjiQoReceivingChamber6 = 3492u,
        PortalSendingIzjiQoReceivingChamber7 = 3493u,
        PortalSendingIzjiQoRewards = 3494u,
        PortalSendingIzjiQoTest60 = 3495u,
        PortalSendingIzjiQoTest80 = 3496u,
        PortalSendingIzjiQoTest100 = 3497u,
        PortalSendingLifestoneOther = 3498u,
        ArcanumSalvagingSelf1 = 3499u,
        ArcanumSalvagingSelf2 = 3500u,
        ArcanumSalvagingSelf3 = 3501u,
        ArcanumSalvagingSelf4 = 3502u,
        ArcanumSalvagingSelf5 = 3503u,
        ArcanumSalvagingSelf6 = 3504u,
        ArcanumSalvagingSelf7 = 3505u,
        ArcanumSalvagingOther1 = 3506u,
        ArcanumSalvagingOther2 = 3507u,
        ArcanumSalvagingOther3 = 3508u,
        ArcanumSalvagingOther4 = 3509u,
        ArcanumSalvagingOther5 = 3510u,
        ArcanumSalvagingOther6 = 3511u,
        ArcanumSalvagingOther7 = 3512u,
        NuhmudirasWisdom1 = 3513u,
        NuhmudirasWisdom2 = 3514u,
        NuhmudirasWisdom3 = 3515u,
        NuhmudirasWisdom4 = 3516u,
        NuhmudirasWisdom5 = 3517u,
        NuhmudirasWisdom6 = 3518u,
        NuhmudirasWisdom7 = 3519u,
        NuhmudirasWisdomOther1 = 3520u,
        NuhmudirasWisdomOther2 = 3521u,
        NuhmudirasWisdomOther3 = 3522u,
        NuhmudirasWisdomOther4 = 3523u,
        NuhmudirasWisdomOther5 = 3524u,
        NuhmudirasWisdomOther6 = 3525u,
        NuhmudirasWisdomOther7 = 3526u,
        Intoxication1 = 3527u,
        Intoxication2 = 3528u,
        Intoxication3 = 3529u,
        KetnansBoon = 3530u,
        BobosBlessingCoord = 3531u,
        BobosBlessingFocus = 3532u,
        BrighteyesFavor = 3533u,
        PortalSendingKnathLair = 3534u,
        PortalSendSanamar = 3535u,
        PortalSendingNumberPuzzleENTER = 3536u,
        PortalSendingNumberPuzzleEXIT = 3537u,
        PortalSendingRainbowPuzzleENTER = 3538u,
        PortalSendingRainbowPuzzleEXIT = 3539u,
        PortalSendingShellGameENTER = 3540u,
        PortalSendingShellGameEXIT = 3541u,
        PortalSendingThreeBagsPuzzleENTER = 3542u,
        PortalSendingThreeBagsPuzzleEXIT = 3543u,
        PortalSendingBloodPuzzleENTER = 3544u,
        PortalSendingBloodPuzzleEXIT = 3545u,
        PortalSendingMemoryGameENTER = 3546u,
        PortalSendingMemoryGameEXIT = 3547u,
        PortalSendingMemRoomA = 3548u,
        PortalSendingMemRoomB = 3549u,
        PortalSendingMemRoomC = 3550u,
        PortalSendingMemRoomD = 3551u,
        PortalSendingMemRoomE = 3552u,
        PortalSendingMemRoomF = 3553u,
        PortalSendingMemRoomG = 3554u,
        PortalSendingMemRoomH = 3555u,
        PortalSendingNumRoomA = 3556u,
        PortalSendingNumRoomB = 3557u,
        PortalSendingNumRoomC = 3558u,
        PortalSendingNumRoomD = 3559u,
        PortalSendingNumRoomE = 3560u,
        PortalSendingNumRoomF = 3561u,
        PortalSendingNumRoomG = 3562u,
        PortalSendingNumRoomH = 3563u,
        PortalSendingNumRoomI = 3564u,
        PortalSendingNumRoomJ = 3565u,
        PortalSendingMemoryGameFinish = 3566u,
        CantripFiunFlee = 3567u,
        CantripFiunEfficiency = 3568u,
        ManaUp10Percent = 3569u,
        StaminaUp10Percent = 3570u,
        HealthUp10Percent = 3571u,
        InnerBrilliance = 3572u,
        InnerMight = 3573u,
        InnerWill = 3574u,
        PerfectBalance = 3575u,
        PerfectHealth = 3576u,
        PerfectSpeed = 3577u,
        PortalSendingBrooduAntechamber = 3578u,
        PortalSendingBrooduGauntlet = 3579u,
        PortalSendingBrooduLair = 3580u,
        PortalSendingBrooduPuzzle = 3581u,
        PortalSendingEatenBroodu = 3582u,
        PortalSendingRegurgitatedBroodu = 3583u,
        PortalSendingBrowerkLair = 3584u,
        PortalSendingEatenBrowerk = 3585u,
        PortalSendingQinXikitAntechamber = 3586u,
        PortalSendingQinXikitGauntlet = 3587u,
        PortalSendingQinXikitPuzzle = 3588u,
        PortalSendingQinXikitReward = 3589u,
        PortalSendingRegurgitatedBrowerk = 3590u,
        PortalSendingXiRuFont = 3591u,
        SummonPortalQinXikitIsland = 3592u,
        PortalSendingEatenKeerik = 3593u,
        PortalSendingKeerikAntechamber = 3594u,
        PortalSendingKeerikGauntlet = 3595u,
        PortalSendingKeerikLair = 3596u,
        PortalSendingKeerikPuzzle = 3597u,
        PortalSendingRegurgitatedKeerik = 3598u,
        PortalSendingEatenKiree = 3599u,
        PortalSendingKireeAntechamber = 3600u,
        PortalSendingKireeGauntlet = 3601u,
        PortalSendingKireeLair = 3602u,
        PortalSendingKireePuzzle = 3603u,
        PortalSendingRegurgitatedKiree = 3604u,
        PortalSendingCragstone = 3605u,
        PortalSendingEatenReeshan = 3606u,
        PortalSendingReeshanAntechamber = 3607u,
        PortalSendingReeshanGauntlet = 3608u,
        PortalSendingReeshanLair = 3609u,
        PortalSendingReeshanPuzzle = 3610u,
        PortalSendingRegurgitatedReeshan = 3611u,
        PortalSendingEatenRehir = 3612u,
        PortalSendingRegurgitatedRehir = 3613u,
        PortalSendingRehirGauntlet = 3614u,
        PortalSendingRehirLair = 3615u,
        PortalSendingRehirPuzzle = 3616u,
        PortalSendingRehirAntechamber = 3617u,
        PortalSendingAphusLasselWest = 3618u,
        PortalSendingBlackDeath = 3619u,
        PortalSendingBSDAlu = 3620u,
        PortalSendingBSDGha = 3621u,
        PortalSendingBSDSho = 3622u,
        PortalSendingCenterObsidianPlains = 3623u,
        PortalSendingHillsCitadel = 3624u,
        PortalSendingKaraWetlands = 3625u,
        PortalSendingMarescentBase = 3626u,
        PortalSendingNeydisa = 3627u,
        PortalSendingNorthernLandbridge = 3628u,
        PortalSendingOlthoiHordeNest = 3629u,
        PortalSendingOlthoiNorth = 3630u,
        PortalSendingRenegadeFortress = 3631u,
        PortalSendingRidgeCitadel = 3632u,
        PortalSendingSouthernLandbridge = 3633u,
        PortalSendingValleyofDeath = 3634u,
        PortalSendingWildernessCitadel = 3635u,
        AxemansBoon = 3636u,
        BowmansBoon = 3637u,
        ChuckersBoon = 3638u,
        CrossbowmansBoon = 3639u,
        EnchantersBoon = 3640u,
        HieromancersBoon = 3641u,
        KnifersBoon = 3642u,
        LifeGiversBoon = 3643u,
        MacersBoon = 3644u,
        PugilistsBoon = 3645u,
        SpearmansBoon = 3646u,
        StafferBoon = 3647u,
        SwordsmansBoon = 3648u,
        AerfallesEmbrace = 3649u,
        AerfallesEnforcement = 3650u,
        AerfallesGaze = 3651u,
        AerfallesTouch = 3652u,
        AcidBlast2 = 3653u,
        AcidVolley1 = 3654u,
        AcidVolley2 = 3655u,
        BladeBlast1 = 3656u,
        BladeBlast2 = 3657u,
        BladeVolley1 = 3658u,
        BladeVolley2 = 3659u,
        BludgeoningVolley1 = 3660u,
        BludgeoningVolley2 = 3661u,
        FlameBlast2 = 3662u,
        FlameVolley1 = 3663u,
        FlameVolley2 = 3664u,
        ForceBlast1 = 3665u,
        ForceBlast2 = 3666u,
        ForceVolley1 = 3667u,
        ForceVolley2 = 3668u,
        FrostBlast1 = 3669u,
        FrostBlast2 = 3670u,
        FrostVolley1 = 3671u,
        FrostVolley2 = 3672u,
        LightningBlast1 = 3673u,
        LightningBlast2 = 3674u,
        LightningVolley1 = 3675u,
        LightningVolley2 = 3676u,
        ShockBlast1 = 3677u,
        ShockBlast2 = 3678u,
        AcidBaneRare = 3679u,
        AcidProtectionRare = 3680u,
        AlchemyMasteryRare = 3681u,
        ArcaneEnlightenmentRare = 3682u,
        ArmorExpertiseRare = 3683u,
        ArmorRare = 3684u,
        AxeMasteryRare = 3685u,
        BladeBaneRare = 3686u,
        BladeProtectionRare = 3687u,
        BloodDrinkerRare = 3688u,
        BludgeonBaneRare = 3689u,
        BludgeonProtectionRare = 3690u,
        BowMasteryRare = 3691u,
        ColdProtectionRare = 3692u,
        CookingMasteryRare = 3693u,
        CoordinationRare = 3694u,
        CreatureEnchantmentMasteryRare = 3695u,
        CrossbowMasteryRare = 3696u,
        DaggerMasteryRare = 3697u,
        DeceptionMasteryRare = 3698u,
        DefenderRare = 3699u,
        EnduranceRare = 3700u,
        FealtyRare = 3701u,
        FireProtectionRare = 3702u,
        FlameBaneRare = 3703u,
        FletchingMasteryRare = 3704u,
        FocusRare = 3705u,
        FrostBaneRare = 3706u,
        HealingMasteryRare = 3707u,
        HeartSeekerRare = 3708u,
        HermeticLinkRare = 3709u,
        ImpenetrabilityRare = 3710u,
        ImpregnabilityRare = 3711u,
        InvulnerabilityRare = 3712u,
        ItemEnchantmentMasteryRare = 3713u,
        ItemExpertiseRare = 3714u,
        JumpMasteryRare = 3715u,
        LeadershipMasteryRare = 3716u,
        LifeMagicMasteryRare = 3717u,
        LightningBaneRare = 3718u,
        LightningProtectionRare = 3719u,
        LockpickMasteryRare = 3720u,
        MaceMasteryRare = 3721u,
        MagicItemExpertiseRare = 3722u,
        MagicResistanceRare = 3723u,
        ManaConvertMasteryRare = 3724u,
        ManaRenewalRare = 3725u,
        MonsterAttunementRare = 3726u,
        PersonAttunementRare = 3727u,
        PiercingBaneRare = 3728u,
        PiercingProtectionRare = 3729u,
        QuicknessRare = 3730u,
        RegenerationRare = 3731u,
        RejuvenationRare = 3732u,
        SelfRare = 3733u,
        SpearMasteryRare = 3734u,
        SpiritDrinkerRare = 3735u,
        SprintRare = 3736u,
        StaffMasteryRare = 3737u,
        StrengthRare = 3738u,
        SwiftKillerRare = 3739u,
        SwordMasteryRare = 3740u,
        ThrownWeaponMasteryRare = 3741u,
        UnarmedCombatMasteryRare = 3742u,
        WarMagicMasteryRare = 3743u,
        WeaponExpertiseRare = 3744u,
        InferiorInfernoAegis = 3745u,
        InfernoAegis = 3746u,
        LesserInfernoAegis = 3747u,
        SalvagingMasteryForge2 = 3748u,
        AlchemyMasteryForge1 = 3749u,
        AlchemyMasteryForge2 = 3750u,
        CookingMasteryForge1 = 3751u,
        CookingMasteryForge2 = 3752u,
        FletchingMasteryForge1 = 3753u,
        FletchingMasteryForge2 = 3754u,
        LockpickMasteryForge1 = 3755u,
        LockpickMasteryForge2 = 3756u,
        SalvagingMasteryForge1 = 3757u,
        CantripInkyArmor1 = 3758u,
        MinorManaGiver = 3759u,
        SecondaryAttribsUp10Percent = 3760u,
        CantripFiunResistance = 3761u,
        PortalSendingDefiledTemple = 3762u,
        PortalSendingAludi = 3763u,
        SummonPortalBanderlingShrine = 3764u,
        PortalSendingPvPHate20Entry4 = 3765u,
        PortalSendingPvPHate20Entry5 = 3766u,
        PortalSendingPvPHate20Entry6 = 3767u,
        PortalSendingPvPHate20Punishment = 3768u,
        PortalSendingPvPHate40Entry1 = 3769u,
        PortalSendingPvPHate40Entry2 = 3770u,
        PortalSendingPvPHate40Entry3 = 3771u,
        PortalSendingPvPHate40Entry4 = 3772u,
        PortalSendingPvPHate40Entry5 = 3773u,
        PortalSendingPvPHate40Entry6 = 3774u,
        PortalSendingPvPHate40Punishment = 3775u,
        PortalSendingPvPHate60Entry1 = 3776u,
        PortalSendingPvPHate60Entry2 = 3777u,
        PortalSendingPvPHate60Entry3 = 3778u,
        PortalSendingPvPHate60Entry4 = 3779u,
        PortalSendingPvPHate60Entry5 = 3780u,
        PortalSendingPvPHate60Entry6 = 3781u,
        PortalSendingPvPHate60Punishment = 3782u,
        PortalSendingPvPHate80AccursedEntry1 = 3783u,
        PortalSendingPvPHate80AccursedEntry2 = 3784u,
        PortalSendingPvPHate80AccursedEntry3 = 3785u,
        PortalSendingPvPHate80AccursedEntry4 = 3786u,
        PortalSendingPvPHate80AccursedEntry5 = 3787u,
        PortalSendingPvPHate80AccursedEntry6 = 3788u,
        PortalSendingPvPHate80Punishment = 3789u,
        PortalSendingPvPHate80UnholyEntry1 = 3790u,
        PortalSendingPvPHate80UnholyEntry2 = 3791u,
        PortalSendingPvPHate80UnholyEntry3 = 3792u,
        PortalSendingPvPHate80UnholyEntry4 = 3793u,
        PortalSendingPvPHate80UnholyEntry5 = 3794u,
        PortalSendingPvPHate80UnholyEntry6 = 3795u,
        PortalSendingPvPHate20Entry1 = 3796u,
        PortalSendingPvPHate20Entry2 = 3797u,
        PortalSendingPvPHate20Entry3 = 3798u,
        BlackMarrowBliss = 3799u,
        BurningSpiritReliquary = 3800u,
        CantripShadowTouch = 3801u,
        CantripShadowReek = 3802u,
        CantripShadowShot = 3803u,
        CantripShadowCross = 3804u,
        AcidRingInfiltration = 3805u,
        FlameRingInfiltration = 3806u,
        ForceRingInfiltration = 3807u,
        LightningRingInfiltration = 3808u,
        CantripSalvagingMinor = 3809u,
        AsheronsBenediction = 3810u,
        BlackmoorsFavor = 3811u,
        PortalSendingRuschkTrials = 3812u,
        PortalSendShoushi = 3813u,
        PortalSendYaraq = 3814u,
        PortalSendHoltburg = 3815u,
        EyeShot = 3816u,
        HuntersWard = 3817u,
        CurseRavenFury = 3818u,
        ConscriptsMight = 3819u,
        ConscriptsWard = 3820u,
        AugursWill = 3821u,
        AugursGlare = 3822u,
        AugursWard = 3823u,
        MarksmansKen = 3824u,
        ArbalestersIntuition = 3825u,
        PortalSendingNightmareSepulcher = 3826u,
        ImpenetrabilityLunnumsPyre = 3827u,
        CantripRageofGrael = 3828u,
        MonsterPlantSundew = 3829u,
        MonsterPlantFlyTrap = 3830u,
        MonsterPlantPitcherPlant = 3831u,
        CantripMastersVoice = 3832u,
        CantripSalvaging1 = 3833u,
        CantripSalvaging2 = 3834u,
        HarmOtherLeviathan = 3835u,
        FrostBoltLeviathan = 3836u,
        PortalSendDeepWater = 3837u,
        PortalSendDeepAbandoned = 3838u,
        PortalSendDeepBrilliant = 3839u,
        PortalSendDeepDazzling = 3840u,
        PortalSendDeepDevastated = 3841u,
        PortalSendDeepEndGame = 3842u,
        PortalSendDeepFire = 3843u,
        PortalSendDeepGatekeeper = 3844u,
        PortalSendDeepRadiant = 3845u,
        PortalSendDeepRuined = 3846u,
        PortalSendXikMinruCataracts = 3847u,
        CombatMedication = 3848u,
        NightRunner = 3849u,
        Selflessness = 3850u,
        CorruptedEssence = 3851u,
        RavenousArmor = 3852u,
        ArdentDefense = 3853u,
        TrueLoyalty = 3854u,
        FlightofBats = 3855u,
        UlgrimsSending = 3856u,
        PumpkinRain = 3857u,
        PumpkinRing = 3858u,
        PumpkinWall = 3859u,
        SweetSpeed = 3860u,
        CantripTasteForBlood = 3861u,
        DukeRaoulPride = 3862u,
        HunterHardiness = 3863u,
        ZongoFist = 3864u,
        GlendenWoodRecall = 3865u,
        GlacialSpeed = 3866u,
        CantripFrostWardShadowsEmbrace = 3867u,
        PortalSendingDardanteKeep = 3868u,
        CantripSaltAshAttackMod = 3869u,
        DispelCreatureAll = 3870u,
        DispelItemAll = 3871u,
        DispelLifeAll = 3872u,
        DispelLifeGoodFellow = 3873u,
        HarmFellowEssence = 3874u,
        AcidVulnerabilityFellowEssence = 3875u,
        BladeVulnerabilityFellowEssence = 3876u,
        AcidBoltEssence = 3877u,
        FlameBoltEssence = 3878u,
        FrostBoltEssence = 3879u,
        LightningBoltEssence = 3880u,
        AcidRingEssence = 3881u,
        FlameRingEssence = 3882u,
        FlameRingWallEssence = 3883u,
        FrostRingEssence = 3884u,
        LightningRingEssence = 3885u,
        MagicYieldEssence = 3886u,
        PortalSendHatchHideout = 3887u,
        PortalSendRareChambers = 3888u,
        PortalSendingRuschkBurialMound = 3889u,
        WillPeople = 3890u,
        HonorBull = 3891u,
        SummonFlameSeekers = 3892u,
        SummonHotspot = 3893u,
        DarkPersistance = 3894u,
        DarkReflexes = 3895u,
        DarkEquilibrium = 3896u,
        DarkPurpose = 3897u,
        RecallSonPooky1 = 3898u,
        RecallSonPooky2 = 3899u,
        RecallSonPooky3 = 3900u,
        EggBomb = 3901u,
        RabbitRing = 3902u,
        WhirlwindRing = 3903u,
        ElementalFuryEssenceAcid = 3904u,
        ElementalFuryEssenceFlame = 3905u,
        ElementalFuryEssenceFrost = 3906u,
        ElementalFuryEssenceLightning = 3907u,
        ManaBoltEssence = 3908u,
        ManaDepletionEssence = 3909u,
        BrainFreeze = 3910u,
        RingHarmOther = 3911u,
        PortalSendingBlackSpearTempleLower = 3912u,
        ArmorSelfAegisGoldenFlame = 3913u,
        DarkVortex = 3914u,
        BlackMadness = 3915u,
        ImperilEssence = 3916u,
        ColdVulnerabilityFellowEssence = 3917u,
        FireVulnerabilityFellowEssence = 3918u,
        LightningVulnerabilityFellowEssence = 3919u,
        PortalSendHarbingerAerbax = 3920u,
        PortalSendHarbingerExtreme = 3921u,
        PortalSendHarbingerNuhmudira = 3922u,
        PortalSendHarbingerTunnelsAerbax = 3923u,
        PortalSendHarbingerTunnelsNuhmudira = 3924u,
        PortalSendHarbingerUber = 3925u,
        HarbingersFieryTouch = 3926u,
        ChargeFlesh = 3927u,
        MeleeYieldEssence = 3928u,
        RecallRossuMorta = 3929u,
        RecallWhisperingBlade = 3930u,
        GraelRing = 3931u,
        GraelSmite = 3932u,
        GraelBlackSpearBolt = 3933u,
        AcidRingWall = 3934u,
        BladeRingWall = 3935u,
        FlameRingWall = 3936u,
        ForceRingWall = 3937u,
        FrostRingWall = 3938u,
        GraelLightningRing = 3939u,
        GraelRingHarm = 3940u,
        LightningRingWall = 3941u,
        ShockwaveRingWall = 3942u,
        BurningEarth = 3943u,
        GraelBlackSpearRain = 3944u,
        GraelLightningRain = 3945u,
        AcidWave = 3946u,
        BladeWave = 3947u,
        FlameWave = 3948u,
        ForceWave = 3949u,
        FrostWave = 3950u,
        LightningWave = 3951u,
        ShockwaveWave = 3952u,
        CarraidasBenediction = 3953u,
        PortalSendingAsheronTower = 3954u,
        BludgeonWard = 3955u,
        PiercingWard = 3956u,
        SlashingWard = 3957u,
        PortalSendingAsheronTowerExit = 3958u,
        RedirectMotives = 3959u,
        Authority = 3960u,
        DefenseJust = 3961u,
        BoundToLaw = 3962u,
        CANTRIPCOORDINATION3 = 3963u,
        CANTRIPFOCUS3 = 3964u,
        CANTRIPSTRENGTH3 = 3965u,
        PortalSendingBanditRingleader = 3966u,
        PortalSendingBanditTrap = 3967u,
        PortalSendingSawatoExtortion = 3968u,
        AcidBomb = 3969u,
        BladeBomb = 3970u,
        FlameBomb = 3971u,
        ForceBomb = 3972u,
        FrostBomb = 3973u,
        LightningBomb = 3974u,
        ShockwaveBomb = 3975u,
        ArmorOtherIncantation = 3976u,
        CoordinationOtherIncantation = 3977u,
        FocusOtherIncantation = 3978u,
        StrengthOtherIncantation = 3979u,
        ImpenetrabilityIncantation = 3980u,
        ManarenewalOtherIncantation = 3981u,
        RegenerationOtherIncantation = 3982u,
        RejuvenationOtherIncantation = 3983u,
        MukkirFerocity = 3984u,
        MukkirSense = 3985u,
        GraelRockFall = 3986u,
        GraelRuschkBlackSpearBolt = 3987u,
        GraelShadowBlackSpearBolt = 3988u,
        LightningBoltRed = 3989u,
        GraelRuschkFrostRing = 3990u,
        DardanteLightningRingGravity = 3991u,
        GraelShadowAcidRing = 3992u,
        GraelShadowBladeRing = 3993u,
        GraelShadowFlameRing = 3994u,
        GraelShadowForceRing = 3995u,
        GraelShadowFrostRing = 3996u,
        GraelShadowLightningRing = 3997u,
        GraelShadowRing = 3998u,
        GraelShadowRingHarm = 3999u,
        GraelShadowShockwaveRing = 4000u,
        BurningEarthGreater = 4001u,
        BurningEarthLesser = 4002u,
        GraelMukkirBlackSpearWall = 4003u,
        GraelRuschkBlackSpearWall = 4004u,
        GraelShadowacidwave = 4005u,
        GraelShadowbladewave = 4006u,
        GraelShadowflamewave = 4007u,
        GraelShadowforcewave = 4008u,
        GraelShadowfrostwave = 4009u,
        GraelShadowlightningwave = 4010u,
        GraelShadowshockwavewave = 4011u,
        PortalSendingWhiteTotemTemple = 4012u,
        PortalSendingBlackTotemTemple = 4013u,
        PortalSendingAbyssalTotemTemple = 4014u,
        RuschkSkin = 4015u,
        ShadowsHeart = 4016u,
        SkillThrownWeaponPhialsAccuracy = 4017u,
        Permafrost = 4018u,
        CANTRIPQUICKNESS3 = 4019u,
        CANTRIPDECEPTIONPROWESS3 = 4020u,
        FlurryOfStars = 4021u,
        ZombiesPersistence = 4022u,
        PortalSendingDiscoInferno = 4023u,
        AsheronsLesserBenediction = 4024u,
        CastIronStomach = 4025u,
        HematicVerdure = 4026u,
        MessengersStride = 4027u,
        Snowball = 4028u,
        PortalSendingColosseum = 4029u,
        PortalSendingColosseumA1 = 4030u,
        PortalSendingColosseumA6 = 4031u,
        PortalSendingColosseumB1 = 4032u,
        PortalSendingColosseumB6 = 4033u,
        PortalSendingColosseumC1 = 4034u,
        PortalSendingColosseumC6 = 4035u,
        PortalSendingColosseumD1 = 4036u,
        PortalSendingColosseumD6 = 4037u,
        PortalSendingColosseumE1 = 4038u,
        PortalSendingColosseumE6 = 4039u,
        HealMaster = 4040u,
        PortalSendingBurLowLevel = 4041u,
        PortalSendingBurHighLevel = 4042u,
        KukuurHide = 4043u,
        AcidBall = 4044u,
        FlameBall = 4045u,
        LightningBall = 4046u,
        EpicAlchemyPetBuff = 4047u,
        EpicCookingPetBuff = 4048u,
        EpicFletchingPetBuff = 4049u,
        EpicLockpickPetBuff = 4050u,
        MajorAlchemyPetBuff = 4051u,
        MajorCookingPetBuff = 4052u,
        MajorFletchingPetBuff = 4053u,
        MajorLockpickPetBuff = 4054u,
        ModerateAlchemyPetBuff = 4055u,
        ModerateCookingPetBuff = 4056u,
        ModerateFletchingPetBuff = 4057u,
        ModerateLockpickPetBuff = 4058u,
        EnduranceOtherIncantation = 4059u,
        QuicknessOtherIncantation = 4060u,
        WillpowerOtherIncantation = 4061u,
        ImpenetrabilityEmpyreanAegis = 4062u,
        BurUpperCatacombExitPortalSending = 4063u,
        BurUpperCatacombPortalSending = 4064u,
        BurunLiberationPortalSending = 4065u,
        InstabilityBurPortalSending = 4066u,
        MucorBolt = 4067u,
        MucorManaWell = 4068u,
        MucorJolt = 4069u,
        EmpyreanManaAbsorbtion = 4070u,
        EmpyreanStaminaAbsorbtion = 4071u,
        AurlanaasResolve = 4072u,
        CantripRegenEmpyrean = 4073u,
        CantripRejuvEmpyrean = 4074u,
        CantripRenewEmpyrean = 4075u,
        EmpyreanEnlightenment = 4076u,
        ManaConvertMasteryIncantation = 4077u,
        EggBolt = 4078u,
        THOPPortalSendingDishwashing = 4079u,
        THOPPortalSendingKitchen = 4080u,
        LightningRingLarge = 4081u,
        BigFire = 4082u,
        PortalSendingKresovusWarren = 4083u,
        RecallBur = 4084u,
        PortalSendingProdigalBanderling = 4085u,
        CantripArmorIceShield = 4086u,
        ImperilOther7Proc = 4087u,
        FesterOtherAssassin = 4088u,
        DeceptionAssassinsGift = 4089u,
        DrudgeArmor = 4090u,
        DrudgeSpearBolt = 4091u,
        FlameGrenade = 4092u,
        DontBiteMe = 4093u,
        DontBurnMe = 4094u,
        DontStabMe = 0xFFFu,
        FlameChain = 0x1000u,
        VirindiLightningRain = 4097u,
        PortalSendingProdigalDrudgeTreasureRoom = 4098u,
        StrengthDiemos = 4099u,
        ChampionHealMaster = 4100u,
        ChampionSkullduggery = 4101u,
        ChampionCleverRuse = 4102u,
        PortalSendingTanadaBlackWater = 4103u,
        PortalSendingColosseumBoss10x = 4104u,
        PortalSendingColosseumBoss200 = 4105u,
        PortalSendingProdigalOlthoiLowerDungeon = 4106u,
        MarrowBlight = 4107u,
        Apathy = 4108u,
        MarrowBlightGreater = 4109u,
        PoisonedBarb = 4110u,
        TuskerHideLesser = 4111u,
        SpiritNullification = 4112u,
        FoulRing = 4113u,
        HypnoticSuggestion = 4114u,
        MesmerizingGaze = 4115u,
        Trance = 4116u,
        DarkShieldAcid = 4117u,
        DarkShieldElectric = 4118u,
        DarkShieldFlame = 4119u,
        DarkShieldIce = 4120u,
        DarkShieldBludgeon = 4121u,
        DarkShieldPierce = 4122u,
        DarkShieldSlash = 4123u,
        WallDarkNanners = 4124u,
        WallArcDarkNanners = 4125u,
        RainDarkNanners = 4126u,
        OolutangaPortalExile = 4127u,
        GraveyardRecall = 4128u,
        PortalSendingProdigalShadowLair = 4129u,
        PortalSendingProdigalShadowSanctum = 4130u,
        AxeMasterySpectral = 4131u,
        BloodDrinkerSpectral = 4132u,
        BowMasterySpectral = 4133u,
        CrossbowMasterySpectral = 4134u,
        DaggerMasterySpectral = 4135u,
        MaceMasterySpectral = 4136u,
        SpearMasterySpectral = 4137u,
        StaffMasterySpectral = 4138u,
        SwordMasterySpectral = 4139u,
        ThrownWeaponMasterySpectral = 4140u,
        UnarmedCombatMasterySpectral = 4141u,
        WarMagicMasterySpectral = 4142u,
        PortalSendingVisionQuestBranch4Stage4 = 4143u,
        PortalSendingVisionQuestBranch5Stage4 = 4144u,
        PortalSendingVisionQuestBranch1Stage6 = 4145u,
        PortalSendingVisionQuestBranch2Stage6 = 4146u,
        PortalSendingVisionQuestBranch3Stage6 = 4147u,
        PortalSendingVisionQuestBranch4Stage6 = 4148u,
        PortalSendingVisionQuestBranch5Stage6 = 4149u,
        PortalSendingVisionQuestTent = 4150u,
        PortalSendingVisionQuestBranch1Stage1 = 4151u,
        PortalSendingVisionQuestBranch2Stage1 = 4152u,
        PortalSendingVisionQuestBranch3Stage1 = 4153u,
        PortalSendingVisionQuestBranch4Stage1 = 4154u,
        PortalSendingVisionQuestBranch5Stage1 = 4155u,
        PortalSendingVisionQuestBranch1Stage2 = 4156u,
        PortalSendingVisionQuestBranch2Stage2 = 4157u,
        PortalSendingVisionQuestBranch3Stage2 = 4158u,
        PortalSendingVisionQuestBranch4Stage2 = 4159u,
        PortalSendingVisionQuestBranch5Stage2 = 4160u,
        PortalSendingVisionQuestBranch1Stage3 = 4161u,
        PortalSendingVisionQuestBranch2Stage3 = 4162u,
        PortalSendingVisionQuestBranch3Stage3 = 4163u,
        PortalSendingVisionQuestBranch4Stage3 = 4164u,
        PortalSendingVisionQuestBranch5Stage3 = 4165u,
        PortalSendingVisionQuestBranch1Stage4 = 4166u,
        PortalSendingVisionQuestBranch2Stage4 = 4167u,
        PortalSendingVisionQuestBranch3Stage4 = 4168u,
        ArmorProdigalHarbinger = 4169u,
        HarbingerCantripCoordination3 = 4170u,
        HarbingerCantripEndurance3 = 4171u,
        HarbingerCantripFocus3 = 4172u,
        HarbingerCantripQuickness3 = 4173u,
        HarbingerCantripStrength3 = 4174u,
        HarbingerCantripWillpower3 = 4175u,
        PortalSendProdigalHarbinger = 4176u,
        PortalSendProdigalHarbingerPrepAcid = 4177u,
        PortalSendProdigalHarbingerPrepCold = 4178u,
        PortalSendProdigalHarbingerPrepFire = 4179u,
        PortalSendProdigalHarbingerPrepLightning = 4180u,
        AcidBoltEssenceDeath = 4181u,
        BallLightning = 4182u,
        CorrosiveVeil = 4183u,
        FlameBoltEssenceDeath = 4184u,
        FrostBoltEssenceDeath = 4185u,
        HoarFrost = 4186u,
        LightningBoltEssenceDeath = 4187u,
        ShadowedFlame = 4188u,
        HarbingerProtectionAcid = 4189u,
        HarbingerProtectionCold = 4190u,
        HarbingerProtectionFire = 4191u,
        HarbingerProtectionLightning = 4192u,
        HarbingerMagicDefense = 4193u,
        HarbingerMagicYield = 4194u,
        HarbingerMeleeDefense = 4195u,
        HarbingerMissileDefense = 4196u,
        HarbingerVulnerability = 4197u,
        InfestedAreaRecall = 4198u,
        FrozenArmor = 4199u,
        PortalZombieKeep = 4200u,
        ColdVulnerabilityNumbing = 4201u,
        FlameStreak7Untargeted = 4202u,
        PortalSendDarkCrypt1 = 4203u,
        PortalSendDarkCrypt2 = 4204u,
        PortalSendDarkCrypt3 = 4205u,
        ChewyCenter = 4206u,
        PortalSendingColosseumBossGolemPumpkin = 4207u,
        SpectralFlame = 4208u,
        GummyShield = 4209u,
        JumpSpin = 4210u,
        LicoriceLeap = 4211u,
        StickyMelee = 4212u,
        RecallColosseum = 4213u,
        CandethKeepRecall = 4214u,
        ShadowArmor = 4215u,
        FrostWaveOrisis = 4216u,
        GourdGuard = 4217u,
        PortalSendKnockback = 4218u,
        PortalSendTrialoftheArm = 4219u,
        PortalSendTrialoftheHeart = 4220u,
        LifeMagicMasterySpectral = 4221u,
        PortalSendProdigalLugian = 4222u,
        PortalSendTrialFinalFight = 4223u,
        PortalSendTrialoftheMind = 4224u,
        PortalSendTrialPreperations = 4225u,
        CANTRIPENDURANCE3 = 4226u,
        CANTRIPWILLPOWER3 = 4227u,
        PortalSendAwakening = 4228u,
        PortalSendingApparitionBDC = 4229u,
        PortalSendBaelzharonDream = 4230u,
        LeadershipMasteryOtherIncantation = 4231u,
        CANTRIPLEADERSHIP3 = 4232u,
        PortalRecallAerbaxPlatformCenter = 4233u,
        PortalRecallAerbaxPlatformEast = 4234u,
        PortalRecallAerbaxPlatformNorth = 4235u,
        PortalRecallAerbaxPlatformSouth = 4236u,
        PortalRecallAerbaxPlatformWest = 4237u,
        PortalSendAerbaxExpel = 4238u,
        AerbaxRingHarm = 4239u,
        AerbaxMagicShield = 4240u,
        AerbaxMagicShieldNull = 4241u,
        AerbaxMeleeShield = 4242u,
        AerbaxMeleeShieldNull = 4243u,
        AerbaxMissileShield = 4244u,
        AerbaxMissileShieldNull = 4245u,
        MeteorStrike = 4246u,
        PortalSendingTanadaBattleBurrows = 4247u,
        PortalSendingShroudCabalNorth80 = 4248u,
        PortalSendingShroudCabalSouth40 = 4249u,
        PortalSendAerbaxPlatform = 4250u,
        PortalSendJester = 4251u,
        PortalSendJesterCell = 4252u,
        PortalSendJesterCellRepeat = 4253u,
        PortalSendJesterHallwayTrap = 4254u,
        PortalSendJesterPrison = 4255u,
        RecallJester1 = 4256u,
        RecallJester2 = 4257u,
        RecallJester3 = 4258u,
        RecallJester4 = 4259u,
        RecallJester5 = 4260u,
        RecallJester6 = 4261u,
        RecallJester7 = 4262u,
        RecallJester8 = 4263u,
        ArcaneDeath = 4264u,
        ArcanePyramid = 4265u,
        JesterBloodBolt = 4266u,
        JesterCowArc = 4267u,
        JesterFireworksBolt = 4268u,
        JesterPresentArc = 4269u,
        JesterTableArc = 4270u,
        AcidWallJester = 4271u,
        BladeWallJester = 4272u,
        CoinWallJester = 4273u,
        FlameWallJester = 4274u,
        LightningWallJester = 4275u,
        JesterMalevolentEye = 4276u,
        PortalSendJesterPrison2 = 4277u,
        PortalSendingRytheranLibrary = 4278u,
        UNKNOWN__GUESSEDNAME4279 = 4279u,
        CoordinationJesterDeck = 4280u,
        FocusJesterDeck = 4281u,
        ArcaneDeathLeft = 4282u,
        ArcaneDeathRight = 4283u,
        AerbaxHarmSelfEast = 4284u,
        AerbaxHarmSelfFinalFight = 4285u,
        AerbaxHarmSelfNorth = 4286u,
        AerbaxHarmSelfSouth = 4287u,
        AerbaxHarmSelfWest = 4288u,
        MessagesBurPortalSending = 4289u,
        ArmorOther8 = 4290u,
        ArmorSelf8 = 4291u,
        BafflementOther8 = 4292u,
        BafflementSelf8 = 4293u,
        ClumsinessOther8 = 4294u,
        ClumsinessSelf8 = 4295u,
        CoordinationOther8 = 4296u,
        CoordinationSelf8 = 4297u,
        EnduranceOther8 = 4298u,
        EnduranceSelf8 = 4299u,
        EnfeebleOther8 = 4300u,
        EnfeebleSelf8 = 4301u,
        FeeblemindOther8 = 4302u,
        FeeblemindSelf8 = 4303u,
        FocusOther8 = 4304u,
        FocusSelf8 = 4305u,
        FrailtyOther8 = 4306u,
        FrailtySelf8 = 4307u,
        HarmOther8 = 4308u,
        HarmSelf8 = 4309u,
        HealOther8 = 4310u,
        HealSelf8 = 4311u,
        ImperilOther8 = 4312u,
        ImperilSelf8 = 4313u,
        ManaBoostOther8 = 4314u,
        ManaBoostSelf8 = 4315u,
        ManaDrainOther8 = 4316u,
        ManaDrainSelf8 = 4317u,
        QuicknessOther8 = 4318u,
        QuicknessSelf8 = 4319u,
        RevitalizeOther8 = 4320u,
        RevitalizeSelf8 = 4321u,
        SlownessOther8 = 4322u,
        SlownessSelf8 = 4323u,
        StrengthOther8 = 4324u,
        StrengthSelf8 = 4325u,
        WeaknessOther8 = 4326u,
        WeaknessSelf8 = 4327u,
        WillpowerOther8 = 4328u,
        WillpowerSelf8 = 4329u,
        DispelAllBadOther8 = 4330u,
        DispelAllBadSelf8 = 4331u,
        DispelAllGoodOther8 = 4332u,
        DispelAllGoodSelf8 = 4333u,
        DispelAllNeutralOther8 = 4334u,
        DispelAllNeutralSelf8 = 4335u,
        DispelCreatureBadOther8 = 4336u,
        DispelCreatureBadSelf8 = 4337u,
        DispelCreatureGoodOther8 = 4338u,
        DispelCreatureGoodSelf8 = 4339u,
        DispelCreatureNeutralOther8 = 4340u,
        DispelCreatureNeutralSelf8 = 4341u,
        DispelItemBadOther8 = 4342u,
        DispelItemGoodOther8 = 4343u,
        DispelItemNeutralOther8 = 4344u,
        DispelLifeBadOther8 = 4345u,
        DispelLifeBadSelf8 = 4346u,
        DispelLifeGoodOther8 = 4347u,
        DispelLifeGoodSelf8 = 4348u,
        DispelLifeNeutralOther8 = 4349u,
        DispelLifeNeutralSelf8 = 4350u,
        CoordinationFellowship8 = 4351u,
        EnduranceFellowship8 = 4352u,
        FocusFellowship8 = 4353u,
        QuicknessFellowship8 = 4354u,
        SelfFellowship8 = 4355u,
        StrengthFellowship8 = 4356u,
        AcidProtectionFellowship8 = 4357u,
        BladeProtectionFellowship8 = 4358u,
        BludgeonProtectionFellowship8 = 4359u,
        ColdProtectionFellowship8 = 4360u,
        FireProtectionFellowship8 = 4361u,
        LightningProtectionFellowship8 = 4362u,
        PierceProtectionFellowship8 = 4363u,
        ManaRenewalFellowship8 = 4364u,
        RegenerationFellowship8 = 4365u,
        RejuvenationFellowship8 = 4366u,
        CreatureEnchantmentMasteryFellow8 = 4367u,
        ImpregnabilityFellowship8 = 4368u,
        InvulnerabilityFellowship8 = 4369u,
        ItemEnchantmentMasteryFellow8 = 4370u,
        LifeMagicMasteryFellow8 = 4371u,
        MagicResistanceFellowship8 = 4372u,
        ManaConversionMasteryFellow8 = 4373u,
        WarMagicMasteryFellow8 = 4374u,
        FireworkOutBlack8 = 4375u,
        FireworkOutBlue8 = 4376u,
        FireworkOutGreen8 = 4377u,
        FireworkOutOrange8 = 4378u,
        FireworkOutPurple8 = 4379u,
        FireworkOutRed8 = 4380u,
        FireworkOutWhite8 = 4381u,
        FireworkOutYellow8 = 4382u,
        FireworkUpBlack8 = 4383u,
        FireworkUpBlue8 = 4384u,
        FireworkUpGreen8 = 4385u,
        FireworkUpOrange8 = 4386u,
        FireworkUpPurple8 = 4387u,
        FireworkUpRed8 = 4388u,
        FireworkUpWhite8 = 4389u,
        FireworkUpYellow8 = 4390u,
        AcidBane8 = 4391u,
        AcidLure8 = 4392u,
        BladeBane8 = 4393u,
        BladeLure8 = 4394u,
        BloodDrinkerSelf8 = 4395u,
        BloodLoather8 = 4396u,
        BludgeonBane8 = 4397u,
        BludgeonLure8 = 4398u,
        Brittlemail8 = 4399u,
        DefenderSelf8 = 4400u,
        FlameBane8 = 4401u,
        FlameLure8 = 4402u,
        FrostBane8 = 4403u,
        FrostLure8 = 4404u,
        HeartSeekerSelf8 = 4405u,
        HermeticVoid8 = 4406u,
        Impenetrability8 = 4407u,
        LeadenWeapon8 = 4408u,
        LightningBane8 = 4409u,
        LightningLure8 = 4410u,
        LureBlade8 = 4411u,
        PiercingBane8 = 4412u,
        PiercingLure8 = 4413u,
        SpiritDrinkerSelf8 = 4414u,
        SpiritLoather8 = 4415u,
        StrengthenLock8 = 4416u,
        SwiftKillerSelf8 = 4417u,
        HermeticLinkSelf8 = 4418u,
        TurnBlade8 = 4419u,
        WeakenLock8 = 4420u,
        AcidArc8 = 4421u,
        BladeArc8 = 4422u,
        FlameArc8 = 4423u,
        ForceArc8 = 4424u,
        FrostArc8 = 4425u,
        LightningArc8 = 4426u,
        ShockArc8 = 4427u,
        HealthBolt8 = 4428u,
        ManaBolt8 = 4429u,
        StaminaBolt8 = 4430u,
        AcidBlast8 = 4431u,
        AcidStreak8 = 4432u,
        AcidStream8 = 4433u,
        AcidVolley8 = 4434u,
        BladeBlast8 = 4435u,
        BladeVolley8 = 4436u,
        BludgeoningVolley8 = 4437u,
        FlameBlast8 = 4438u,
        FlameBolt8 = 4439u,
        FlameStreak8 = 4440u,
        FlameVolley8 = 4441u,
        ForceBlast8 = 4442u,
        ForceBolt8 = 4443u,
        ForceStreak8 = 4444u,
        ForceVolley8 = 4445u,
        FrostBlast8 = 4446u,
        FrostBolt8 = 4447u,
        FrostStreak8 = 4448u,
        FrostVolley8 = 4449u,
        LightningBlast8 = 4450u,
        LightningBolt8 = 4451u,
        LightningStreak8 = 4452u,
        LightningVolley8 = 4453u,
        ShockBlast8 = 4454u,
        ShockWave8 = 4455u,
        ShockwaveStreak8 = 4456u,
        WhirlingBlade8 = 4457u,
        WhirlingBladeStreak8 = 4458u,
        AcidProtectionOther8 = 4459u,
        AcidProtectionSelf8 = 4460u,
        BladeProtectionOther8 = 4461u,
        BladeProtectionSelf8 = 4462u,
        BludgeonProtectionOther8 = 4463u,
        BludgeonProtectionSelf8 = 4464u,
        ColdProtectionOther8 = 4465u,
        ColdProtectionSelf8 = 4466u,
        FireProtectionOther8 = 4467u,
        FireProtectionSelf8 = 4468u,
        LightningProtectionOther8 = 4469u,
        LightningProtectionSelf8 = 4470u,
        PiercingProtectionOther8 = 4471u,
        PiercingProtectionSelf8 = 4472u,
        AcidVulnerabilityOther8 = 4473u,
        AcidVulnerabilitySelf8 = 4474u,
        BladeVulnerabilityOther8 = 4475u,
        BladeVulnerabilitySelf8 = 4476u,
        BludgeonVulnerabilityOther8 = 4477u,
        BludgeonVulnerabilitySelf8 = 4478u,
        ColdVulnerabilityOther8 = 4479u,
        ColdVulnerabilitySelf8 = 4480u,
        FireVulnerabilityOther8 = 4481u,
        FireVulnerabilitySelf8 = 4482u,
        LightningVulnerabilityOther8 = 4483u,
        LightningVulnerabilitySelf8 = 4484u,
        PiercingVulnerabilityOther8 = 4485u,
        PiercingVulnerabilitySelf8 = 4486u,
        ExhaustionOther8 = 4487u,
        ExhaustionSelf8 = 4488u,
        FesterOther8 = 4489u,
        FesterSelf8 = 4490u,
        ManaDepletionOther8 = 4491u,
        ManaDepletionSelf8 = 4492u,
        ManaRenewalOther8 = 4493u,
        ManaRenewalSelf8 = 4494u,
        RegenerationOther8 = 4495u,
        RegenerationSelf8 = 4496u,
        RejuvenationOther8 = 4497u,
        RejuvenationSelf8 = 4498u,
        ArcanumSalvagingSelf8 = 4499u,
        ArcanumSalvagingOther8 = 4500u,
        NuhmudirasWisdom8 = 4501u,
        NuhmudirasWisdomOther8 = 4502u,
        AlchemyIneptitudeOther8 = 4503u,
        AlchemyIneptitudeSelf8 = 4504u,
        AlchemyMasteryOther8 = 4505u,
        AlchemyMasterySelf8 = 4506u,
        ArcaneBenightednessOther8 = 4507u,
        ArcaneBenightednessSelf8 = 4508u,
        ArcaneEnlightenmentOther8 = 4509u,
        ArcaneEnlightenmentSelf8 = 4510u,
        ArmorExpertiseOther8 = 4511u,
        ArmorExpertiseSelf8 = 4512u,
        ArmorIgnoranceOther8 = 4513u,
        ArmorIgnoranceSelf8 = 4514u,
        LightWeaponsIneptitudeOther8 = 4515u,
        LightWeaponsIneptitudeSelf8 = 4516u,
        LightWeaponsMasteryOther8 = 4517u,
        LightWeaponsMasterySelf8 = 4518u,
        MissileWeaponsIneptitudeOther8 = 4519u,
        MissileWeaponsIneptitudeSelf8 = 4520u,
        MissileWeaponsMasteryOther8 = 4521u,
        MissileWeaponsMasterySelf8 = 4522u,
        CookingIneptitudeOther8 = 4523u,
        CookingIneptitudeSelf8 = 4524u,
        CookingMasteryOther8 = 4525u,
        CookingMasterySelf8 = 4526u,
        CreatureEnchantmentIneptitudeOther8 = 4527u,
        CreatureEnchantmentIneptitudeSelf8 = 4528u,
        CreatureEnchantmentMasteryOther8 = 4529u,
        CreatureEnchantmentMasterySelf8 = 4530u,
        CrossbowIneptitudeOther8 = 4531u,
        CrossbowIneptitudeSelf8 = 4532u,
        CrossbowMasteryOther8 = 4533u,
        CrossbowMasterySelf8 = 4534u,
        FinesseWeaponsIneptitudeOther8 = 4535u,
        FinesseWeaponsIneptitudeSelf8 = 4536u,
        FinesseWeaponsMasteryOther8 = 4537u,
        FinesseWeaponsMasterySelf8 = 4538u,
        DeceptionIneptitudeOther8 = 4539u,
        DeceptionIneptitudeSelf8 = 4540u,
        DeceptionMasteryOther8 = 4541u,
        DeceptionMasterySelf8 = 4542u,
        DefenselessnessOther8 = 4543u,
        DefenselessnessSelf8 = 4544u,
        FaithlessnessOther8 = 4545u,
        FaithlessnessSelf8 = 4546u,
        FealtyOther8 = 4547u,
        FealtySelf8 = 4548u,
        FletchingIneptitudeOther8 = 4549u,
        FletchingIneptitudeSelf8 = 4550u,
        FletchingMasteryOther8 = 4551u,
        FletchingMasterySelf8 = 4552u,
        HealingIneptitudeOther8 = 4553u,
        HealingIneptitudeSelf8 = 4554u,
        HealingMasteryOther8 = 4555u,
        HealingMasterySelf8 = 4556u,
        ImpregnabilityOther8 = 4557u,
        ImpregnabilitySelf8 = 4558u,
        InvulnerabilityOther8 = 4559u,
        InvulnerabilitySelf8 = 4560u,
        ItemEnchantmentIneptitudeOther8 = 4561u,
        ItemEnchantmentIneptitudeSelf8 = 4562u,
        ItemEnchantmentMasteryOther8 = 4563u,
        ItemEnchantmentMasterySelf8 = 4564u,
        ItemExpertiseOther8 = 4565u,
        ItemExpertiseSelf8 = 4566u,
        ItemIgnoranceOther8 = 4567u,
        ItemIgnoranceSelf8 = 4568u,
        JumpingIneptitudeOther8 = 4569u,
        JumpingIneptitudeSelf8 = 4570u,
        JumpingMasteryOther8 = 4571u,
        JumpingMasterySelf8 = 4572u,
        LeadenFeetOther8 = 4573u,
        LeadenFeetSelf8 = 4574u,
        LeadershipIneptitudeOther8 = 4575u,
        LeadershipIneptitudeSelf8 = 4576u,
        LeadershipMasteryOther8 = 4577u,
        LeadershipMasterySelf8 = 4578u,
        LifeMagicIneptitudeOther8 = 4579u,
        LifeMagicIneptitudeSelf8 = 4580u,
        LifeMagicMasteryOther8 = 4581u,
        LifeMagicMasterySelf8 = 4582u,
        LockpickIneptitudeOther8 = 4583u,
        LockpickIneptitudeSelf8 = 4584u,
        LockpickMasteryOther8 = 4585u,
        LockpickMasterySelf8 = 4586u,
        MaceIneptitudeOther8 = 4587u,
        MaceIneptitudeSelf8 = 4588u,
        MaceMasteryOther8 = 4589u,
        MaceMasterySelf8 = 4590u,
        MagicItemExpertiseOther8 = 4591u,
        MagicItemExpertiseSelf8 = 4592u,
        MagicItemIgnoranceOther8 = 4593u,
        MagicItemIgnoranceSelf8 = 4594u,
        MagicResistanceOther8 = 4595u,
        MagicResistanceSelf8 = 4596u,
        MagicYieldOther8 = 4597u,
        MagicYieldSelf8 = 4598u,
        ManaIneptitudeOther8 = 4599u,
        ManaIneptitudeSelf8 = 4600u,
        ManaMasteryOther8 = 4601u,
        ManaMasterySelf8 = 4602u,
        MonsterAttunementOther8 = 4603u,
        MonsterAttunementSelf8 = 4604u,
        MonsterUnfamiliarityOther8 = 4605u,
        MonsterUnfamiliaritySelf8 = 4606u,
        PersonAttunementOther8 = 4607u,
        PersonAttunementSelf8 = 4608u,
        PersonUnfamiliarityOther8 = 4609u,
        PersonUnfamiliaritySelf8 = 4610u,
        SpearIneptitudeOther8 = 4611u,
        SpearIneptitudeSelf8 = 4612u,
        SpearMasteryOther8 = 4613u,
        SpearMasterySelf8 = 4614u,
        SprintOther8 = 4615u,
        SprintSelf8 = 4616u,
        StaffIneptitudeOther8 = 4617u,
        StaffIneptitudeSelf8 = 4618u,
        StaffMasteryOther8 = 4619u,
        StaffMasterySelf8 = 4620u,
        HeavyWeaponsIneptitudeOther8 = 4621u,
        HeavyWeaponsIneptitudeSelf8 = 4622u,
        HeavyWeaponsMasteryOther8 = 4623u,
        HeavyWeaponsMasterySelf8 = 4624u,
        ThrownWeaponIneptitudeOther8 = 4625u,
        ThrownWeaponIneptitudeSelf8 = 4626u,
        ThrownWeaponMasteryOther8 = 4627u,
        ThrownWeaponMasterySelf8 = 4628u,
        UnarmedCombatIneptitudeOther8 = 4629u,
        UnarmedCombatMasteryOther8 = 4630u,
        UnarmedCombatMasterySelf8 = 4631u,
        UnarmedCombatIneptitudeSelf8 = 4632u,
        VulnerabilityOther8 = 4633u,
        VulnerabilitySelf8 = 4634u,
        WarMagicIneptitudeOther8 = 4635u,
        WarMagicIneptitudeSelf8 = 4636u,
        WarMagicMasteryOther8 = 4637u,
        WarMagicMasterySelf8 = 4638u,
        WeaponExpertiseOther8 = 4639u,
        WeaponExpertiseSelf8 = 4640u,
        WeaponIgnoranceOther8 = 4641u,
        WeaponIgnoranceSelf8 = 4642u,
        DrainHealth8 = 4643u,
        DrainMana8 = 4644u,
        DrainStamina8 = 4645u,
        HealthToManaOther8 = 4646u,
        HealthToManaSelf8 = 4647u,
        HealthToStaminaOther8 = 4648u,
        HealthToStaminaSelf8 = 4649u,
        InfuseHealth8 = 4650u,
        InfuseMana8 = 4651u,
        InfuseStamina8 = 4652u,
        ManaToHealthOther8 = 4653u,
        ManaToHealthSelf8 = 4654u,
        ManaToStaminaOther8 = 4655u,
        ManaToStaminaSelf8 = 4656u,
        StaminaToHealthOther8 = 4657u,
        StaminaToHealthSelf8 = 4658u,
        StaminaToManaOther8 = 4659u,
        CANTRIPACIDBANE3 = 4660u,
        CANTRIPBLOODTHIRST3 = 4661u,
        CANTRIPBLUDGEONINGBANE3 = 4662u,
        CANTRIPDEFENDER3 = 4663u,
        CANTRIPFLAMEBANE3 = 4664u,
        CANTRIPFROSTBANE3 = 4665u,
        CANTRIPHEARTTHIRST3 = 4666u,
        CANTRIPIMPENETRABILITY3 = 4667u,
        CANTRIPPIERCINGBANE3 = 4668u,
        CANTRIPSLASHINGBANE3 = 4669u,
        CANTRIPSPIRITTHIRST3 = 4670u,
        CANTRIPSTORMBANE3 = 4671u,
        CANTRIPSWIFTHUNTER3 = 4672u,
        CANTRIPACIDWARD3 = 4673u,
        CANTRIPBLUDGEONINGWARD3 = 4674u,
        CANTRIPFLAMEWARD3 = 4675u,
        CANTRIPFROSTWARD3 = 4676u,
        CANTRIPPIERCINGWARD3 = 4677u,
        CANTRIPSLASHINGWARD3 = 4678u,
        CANTRIPSTORMWARD3 = 4679u,
        CANTRIPHEALTHGAIN3 = 4680u,
        CANTRIPMANAGAIN3 = 4681u,
        CANTRIPSTAMINAGAIN3 = 4682u,
        CANTRIPALCHEMICALPROWESS3 = 4683u,
        CANTRIPARCANEPROWESS3 = 4684u,
        CANTRIPARMOREXPERTISE3 = 4685u,
        CANTRIPLIGHTWEAPONSAPTITUDE3 = 4686u,
        CANTRIPMISSILEWEAPONSAPTITUDE3 = 4687u,
        CANTRIPCOOKINGPROWESS3 = 4688u,
        CANTRIPCREATUREENCHANTMENTAPTITUDE3 = 4689u,
        CANTRIPCROSSBOWAPTITUDE3 = 4690u,
        CANTRIPFINESSEWEAPONSAPTITUDE3 = 4691u,
        CANTRIPFEALTY3 = 4692u,
        CANTRIPFLETCHINGPROWESS3 = 4693u,
        CANTRIPHEALINGPROWESS3 = 4694u,
        CANTRIPIMPREGNABILITY3 = 4695u,
        CANTRIPINVULNERABILITY3 = 4696u,
        CANTRIPITEMENCHANTMENTAPTITUDE3 = 4697u,
        CANTRIPITEMEXPERTISE3 = 4698u,
        CANTRIPJUMPINGPROWESS3 = 4699u,
        CANTRIPLIFEMAGICAPTITUDE3 = 4700u,
        CANTRIPLOCKPICKPROWESS3 = 4701u,
        CANTRIPMACEAPTITUDE3 = 4702u,
        CANTRIPMAGICITEMEXPERTISE3 = 4703u,
        CANTRIPMAGICRESISTANCE3 = 4704u,
        CANTRIPMANACONVERSIONPROWESS3 = 4705u,
        CANTRIPMONSTERATTUNEMENT3 = 4706u,
        CANTRIPPERSONATTUNEMENT3 = 4707u,
        CANTRIPSALVAGING3 = 4708u,
        CANTRIPSPEARAPTITUDE3 = 4709u,
        CANTRIPSPRINT3 = 4710u,
        CANTRIPSTAFFAPTITUDE3 = 4711u,
        CANTRIPHEAVYWEAPONSAPTITUDE3 = 4712u,
        CANTRIPTHROWNAPTITUDE3 = 4713u,
        CANTRIPUNARMEDAPTITUDE3 = 4714u,
        CANTRIPWARMAGICAPTITUDE3 = 4715u,
        BurningCurse = 4716u,
        PortalSendingUlgrimSkyHigh = 4717u,
        PortalSendingCHDSStage1 = 4718u,
        PortalSendingCHDSStage2 = 4719u,
        PortalSendingCHDSStage3 = 4720u,
        PortalSendingFreebooterIsle = 4721u,
        DoTHealthOther1 = 4722u,
        DoTHealthSelf1 = 4723u,
        PortalSendMoarsmenCity = 4724u,
        PortalSendMoarsmenCitySclavusPit = 4725u,
        MoarsmanPoison1 = 4726u,
        MoarsmanPoison2 = 4727u,
        MoarsmanPoison3 = 4728u,
        PortalSendingMasterGateway = 4729u,
        SetCoordination1 = 4730u,
        SetCoordination2 = 4731u,
        SetCoordination3 = 4732u,
        SetCoordination4 = 4733u,
        SetEndurance1 = 4734u,
        SetEndurance2 = 4735u,
        SetEndurance3 = 4736u,
        SetEndurance4 = 4737u,
        SetFocus1 = 4738u,
        SetFocus2 = 4739u,
        SetFocus3 = 4740u,
        SetFocus4 = 4741u,
        SetQuickness1 = 4742u,
        SetQuickness2 = 4743u,
        SetQuickness3 = 4744u,
        SetQuickness4 = 4745u,
        SetStrength1 = 4746u,
        SetStrength2 = 4747u,
        SetStrength3 = 4748u,
        SetStrength4 = 4749u,
        SetWillpower1 = 4750u,
        SetWillpower2 = 4751u,
        SetWillpower3 = 4752u,
        SetWillpower4 = 4753u,
        SetHealth2 = 4754u,
        SetHealth3 = 4755u,
        SetMana2 = 4756u,
        SetMana3 = 4757u,
        SetStamina2 = 4758u,
        SetStamina3 = 4759u,
        SetAcidResistance1 = 4760u,
        SetAcidResistance2 = 4761u,
        SetAcidResistance3 = 4762u,
        SetAcidResistance4 = 4763u,
        SetBludgeonResistance1 = 4764u,
        SetBludgeonResistance2 = 4765u,
        SetBludgeonResistance3 = 4766u,
        SetBludgeonResistance4 = 4767u,
        SetFlameResistance1 = 4768u,
        SetFlameResistance2 = 4769u,
        SetFlameResistance3 = 4770u,
        SetFlameResistance4 = 4771u,
        SetFrostResistance1 = 4772u,
        SetFrostResistance2 = 4773u,
        SetFrostResistance3 = 4774u,
        SetFrostResistance4 = 4775u,
        SetLightningResistance1 = 4776u,
        SetLightningResistance2 = 4777u,
        SetLightningResistance3 = 4778u,
        SetLightningResistance4 = 4779u,
        SetPierceResistance1 = 4780u,
        SetPierceResistance2 = 4781u,
        SetPierceResistance3 = 4782u,
        SetPierceResistance4 = 4783u,
        SetSlashingResistance1 = 4784u,
        SetSlashingResistance2 = 4785u,
        SetSlashingResistance3 = 4786u,
        SetSlashingResistance4 = 4787u,
        SetAlchemyAptitude1 = 4788u,
        SetAlchemyAptitude2 = 4789u,
        SetAlchemyAptitude3 = 4790u,
        SetAlchemyAptitude4 = 4791u,
        SetArmorExpertiseAptitude1 = 4792u,
        SetArmorExpertiseAptitude2 = 4793u,
        SetArmorExpertiseAptitude3 = 4794u,
        SetArmorExpertiseAptitude4 = 4795u,
        SetAxeAptitude1 = 4796u,
        SetAxeAptitude2 = 4797u,
        SetAxeAptitude3 = 4798u,
        SetAxeAptitude4 = 4799u,
        SetBowAptitude1 = 4800u,
        SetBowAptitude2 = 4801u,
        SetBowAptitude3 = 4802u,
        SetBowAptitude4 = 4803u,
        SetCookingAptitude1 = 4804u,
        SetCookingAptitude2 = 4805u,
        SetCookingAptitude3 = 4806u,
        SetCookingAptitude4 = 4807u,
        SetCreatureEnchantmentAptitude1 = 4808u,
        SetCreatureEnchantmentAptitude2 = 4809u,
        SetCreatureEnchantmentAptitude3 = 4810u,
        SetCreatureEnchantmentAptitude4 = 4811u,
        SetCrossbowAptitude1 = 4812u,
        SetCrossbowAptitude2 = 4813u,
        SetCrossbowAptitude3 = 4814u,
        SetCrossbowAptitude4 = 4815u,
        SetDaggerAptitude1 = 4816u,
        SetDaggerAptitude2 = 4817u,
        SetDaggerAptitude3 = 4818u,
        SetDaggerAptitude4 = 4819u,
        SetFletchingAptitude1 = 4820u,
        SetFletchingAptitude2 = 4821u,
        SetFletchingAptitude3 = 4822u,
        SetFletchingAptitude4 = 4823u,
        SetItemEnchantmentAptitude1 = 4824u,
        SetItemEnchantmentAptitude2 = 4825u,
        SetItemEnchantmentAptitude3 = 4826u,
        SetItemEnchantmentAptitude4 = 4827u,
        SetItemExpertiseAptitude1 = 4828u,
        SetItemExpertiseAptitude2 = 4829u,
        SetItemExpertiseAptitude3 = 4830u,
        SetItemExpertiseAptitude4 = 4831u,
        SetJumpingAptitude1 = 4832u,
        SetJumpingAptitude2 = 4833u,
        SetJumpingAptitude3 = 4834u,
        SetJumpingAptitude4 = 4835u,
        SetLifeMagicAptitude1 = 4836u,
        SetLifeMagicAptitude2 = 4837u,
        SetLifeMagicAptitude3 = 4838u,
        SetLifeMagicAptitude4 = 4839u,
        SetLockpickAptitude1 = 4840u,
        SetLockpickAptitude2 = 4841u,
        SetLockpickAptitude3 = 4842u,
        SetLockpickAptitude4 = 4843u,
        SetLoyaltyAptitude1 = 4844u,
        SetLoyaltyAptitude2 = 4845u,
        SetMaceAptitude1 = 4846u,
        SetMaceAptitude2 = 4847u,
        SetMaceAptitude3 = 4848u,
        SetMaceAptitude4 = 4849u,
        SetMagicDefenseAptitude1 = 4850u,
        SetMagicDefenseAptitude2 = 4851u,
        SetMagicDefenseAptitude3 = 4852u,
        SetMagicDefenseAptitude4 = 4853u,
        SetMagicItemExpertiseAptitude1 = 4854u,
        SetMagicItemExpertiseAptitude2 = 4855u,
        SetMagicItemExpertiseAptitude3 = 4856u,
        SetMagicItemExpertiseAptitude4 = 4857u,
        SetMeleeDefenseAptitude1 = 4858u,
        SetMeleeDefenseAptitude2 = 4859u,
        SetMeleeDefenseAptitude3 = 4860u,
        SetMeleeDefenseAptitude4 = 4861u,
        SetMissileDefenseAptitude1 = 4862u,
        SetMissileDefenseAptitude2 = 4863u,
        SetMissileDefenseAptitude3 = 4864u,
        SetMissileDefenseAptitude4 = 4865u,
        SetSalvagingAptitude1 = 4866u,
        SetSalvagingAptitude2 = 4867u,
        SetSpearAptitude1 = 4868u,
        SetSpearAptitude2 = 4869u,
        SetSpearAptitude3 = 4870u,
        SetSpearAptitude4 = 4871u,
        SetSprintAptitude1 = 4872u,
        SetSprintAptitude2 = 4873u,
        SetSprintAptitude3 = 4874u,
        SetSprintAptitude4 = 4875u,
        SetStaffAptitude1 = 4876u,
        SetStaffAptitude2 = 4877u,
        SetStaffAptitude3 = 4878u,
        SetStaffAptitude4 = 4879u,
        SetSwordAptitude1 = 4880u,
        SetSwordAptitude2 = 4881u,
        SetSwordAptitude3 = 4882u,
        SetSwordAptitude4 = 4883u,
        SetThrownAptitude1 = 4884u,
        SetThrownAptitude2 = 4885u,
        SetThrownAptitude3 = 4886u,
        SetThrownAptitude4 = 4887u,
        SetUnarmedAptitude1 = 4888u,
        SetUnarmedAptitude2 = 4889u,
        SetUnarmedAptitude3 = 4890u,
        SetUnarmedAptitude4 = 4891u,
        SetWarMagicAptitude1 = 4892u,
        SetWarMagicAptitude2 = 4893u,
        SetWarMagicAptitude3 = 4894u,
        SetWarMagicAptitude4 = 4895u,
        SetWeaponExpertiseAptitude1 = 4896u,
        SetWeaponExpertiseAptitude2 = 4897u,
        SetWeaponExpertiseAptitude3 = 4898u,
        SetWeaponExpertiseAptitude4 = 4899u,
        SetSocietyAttributeAll1 = 4900u,
        SetSocietyAttributeAll2 = 4901u,
        SetSocietyAttributeAll3 = 4902u,
        SetSocietyAttributeAll4 = 4903u,
        SetSocietyAttributeAll5 = 4904u,
        SetRejuvenation1 = 4905u,
        SetRejuvenation2 = 4906u,
        RecallCelestialHand = 4907u,
        RecallEldrytchWeb = 4908u,
        RecallRadiantBlood = 4909u,
        HarmRaiderTag = 4910u,
        CANTRIPARMOR3 = 4911u,
        CANTRIPWEAPONEXPERTISE3 = 4912u,
        PortalSendingNodePyramidAerlinthe = 4913u,
        PortalSendingNodePyramidAerlintheExit = 4914u,
        PortalSendingNodePyramidAmun = 4915u,
        PortalSendingNodePyramidAmunExit = 4916u,
        PortalSendingNodePyramidEsper = 4917u,
        PortalSendingNodePyramidEsperExit = 4918u,
        PortalSendingNodePyramidHalaetan = 4919u,
        PortalSendingNodePyramidHalaetanExit = 4920u,
        PortalSendingNodePyramidLinvak = 4921u,
        PortalSendingNodePyramidLinvakExit = 4922u,
        PortalSendingNodePyramidObsidian = 4923u,
        PortalSendingNodePyramidObsidianExit = 4924u,
        Dance = 4925u,
        TthuunSmite = 4926u,
        AcidStream8Spellpower300 = 4927u,
        AcidStream8Spellpower350 = 4928u,
        MiniArcaneDeath = 4929u,
        MiniFireball1 = 4930u,
        MiniFireball2 = 4931u,
        MiniFireball3 = 4932u,
        MiniIceball1 = 4933u,
        MiniIceball2 = 4934u,
        MiniIceball3 = 4935u,
        MiniArrow1 = 4936u,
        MiniArrow2 = 4937u,
        MiniArrow3 = 4938u,
        MiniUber = 4939u,
        MiniRing1 = 4940u,
        MiniRing2 = 4941u,
        MiniRing3 = 4942u,
        MiniFireball4 = 4943u,
        MiniIceball4 = 4944u,
        MiniArrow4 = 4945u,
        MiniRing4 = 4946u,
        MiniArcaneDeath2 = 4947u,
        MiniArcaneDeath3 = 4948u,
        MiniArcaneDeath4 = 4949u,
        PortalTowerDefenseRB = 4950u,
        PortalTowerDefenseCH = 4951u,
        PortalTowerDefenseEW = 4952u,
        PortalSendingLostPortalTest = 4953u,
        PortalSendingCrystallineCrag = 4954u,
        PortalSendingLostPortalTestRare = 4955u,
        PortalSendingTanadaScrollsToSanctum = 4956u,
        PortalSendingTanadaScrollsReturn = 4957u,
        RockslidegreaterXBow = 4958u,
        RockslidelesserBow = 4959u,
        RockslidelesserTW = 4960u,
        RockslidelesserXBow = 4961u,
        RockslideTW = 4962u,
        RockslideXBow = 4963u,
        RockslideBow = 4964u,
        RockslidegreaterBow = 4965u,
        RockslidegreaterTW = 4966u,
        cleansingring = 4967u,
        BowmansBoon3 = 4968u,
        ChuckersBoon3 = 4969u,
        CrossbowmansBoon3 = 4970u,
        EnchantersBoon3 = 4971u,
        HieromancersBoon3 = 4972u,
        KnifersBoon3 = 4973u,
        LifeGiversBoon3 = 4974u,
        MacersBoon3 = 4975u,
        PugilistsBoon3 = 4976u,
        SpearmansBoon3 = 4977u,
        StafferBoon3 = 4978u,
        SwordsmansBoon3 = 4979u,
        AxemansBoon3 = 4980u,
        StaminaToManaSelf8 = 4981u,
        LockpickBonus5 = 4982u,
        AlchemyBonus5 = 4983u,
        CookingBonus5 = 4984u,
        FletchingBonus5 = 4985u,
        AlchemyPercentBoost1 = 4986u,
        olthoiSpit = 4987u,
        PortalTunnelOut = 4988u,
        PortalSendingAssassinsRoost1 = 4989u,
        PortalSendingAssassinsRoost2 = 4990u,
        PortalSendingAssassinsRoost3 = 4991u,
        PortalSendingAssassinsRoost4 = 4992u,
        PortalSendingPKDungeon = 4993u,
        PortalSendingAssassinsRoost5 = 4994u,
        PortalSendingPKDungeonTwo = 4995u,
        PortalSendingPKDungeonThree = 4996u,
        PortalSendingPKDungeonFour = 4997u,
        ArenaStamina = 4998u,
        ArenaHealth = 4999u,
        ArenaMana = 5000u,
        PiercingProtectionOtherArena = 5001u,
        AcidProtectionOtherArena = 5002u,
        BladeProtectionOtherArena = 5003u,
        BludgeonProtectionOtherArena = 5004u,
        ColdProtectionOtherArena = 5005u,
        FireProtectionOtherArena = 5006u,
        LightningProtectionOtherArena = 5007u,
        PortalSendingApostateNexus = 5008u,
        AerfallesWardGreater = 5009u,
        PortalSendingAerfalleSanctum = 5010u,
        HarmOtherGeomanticRaze = 5011u,
        PortalSendingLostPortalMar = 5012u,
        PortalSendingLostPortaMay = 5013u,
        PortalSendingLostPortaApr = 5014u,
        PortalSendingLostPortalFeb = 5015u,
        PortalSendingLostPortaAlive = 5016u,
        PortalSendingLostPortaDead = 5017u,
        PortalDesperadoStory = 5018u,
        PortalSendingLostPortalFrost = 5019u,
        PortalSendingLostPortalLightning = 5020u,
        PortalSendingLostPortalAcid = 5021u,
        PortalSendingLostPortalFire = 5022u,
        TwoHandedMasterySpectral = 5023u,
        GearCraftMasterySpectral = 5024u,
        GearCraftMasteryRare = 5025u,
        TwoHandedMasteryRare = 5026u,
        CascadeTwoHandedGreater = 5027u,
        CascadeTwoHandedLesser = 5028u,
        CascadeTwoHanded = 5029u,
        TwoHandedBoon = 5030u,
        TwoHandedBoon3 = 5031u,
        TwoHandedMasterySelf8 = 5032u,
        CANTRIPGEARCRAFTAPTITUDE3 = 5033u,
        CANTRIPTWOHANDEDAPTITUDE3 = 5034u,
        FeebleGearCraftAptitude = 5035u,
        FeebleTwoHandedAptitude = 5036u,
        GearcraftIneptitude1 = 5037u,
        GearcraftIneptitude2 = 5038u,
        GearcraftIneptitude3 = 5039u,
        GearcraftIneptitude4 = 5040u,
        GearcraftIneptitude5 = 5041u,
        GearcraftIneptitude6 = 5042u,
        GearcraftIneptitude7 = 5043u,
        GearcraftIneptitude8 = 5044u,
        GearcraftIneptitudeSelf1 = 5045u,
        GearcraftIneptitudeSelf2 = 5046u,
        GearcraftIneptitudeSelf3 = 5047u,
        GearcraftIneptitudeSelf4 = 5048u,
        GearcraftIneptitudeSelf5 = 5049u,
        GearcraftIneptitudeSelf6 = 5050u,
        GearcraftIneptitudeSelf7 = 5051u,
        GearcraftIneptitudeSelf8 = 5052u,
        GearcraftMastery1 = 5053u,
        GearcraftMastery2 = 5054u,
        GearcraftMastery3 = 5055u,
        GearcraftMastery4 = 5056u,
        GearcraftMastery5 = 5057u,
        GearcraftMastery6 = 5058u,
        GearcraftMastery7 = 5059u,
        GearcraftMastery8 = 5060u,
        GearcraftMasterySelf1 = 5061u,
        GearcraftMasterySelf2 = 5062u,
        GearcraftMasterySelf3 = 5063u,
        GearcraftMasterySelf4 = 5064u,
        GearcraftMasterySelf5 = 5065u,
        GearcraftMasterySelf6 = 5066u,
        GearcraftMasterySelf7 = 5067u,
        GearcraftMasterySelf8 = 5068u,
        CANTRIPGEARCRAFTAPTITUDE2 = 5069u,
        CANTRIPTWOHANDEDAPTITUDE2 = 5070u,
        CANTRIPGEARCRAFTAPTITUDE1 = 5071u,
        CANTRIPTWOHANDEDAPTITUDE1 = 5072u,
        ModerateGearCraftAptitude = 5073u,
        ModerateTwoHandedAptitude = 5074u,
        TwoHandedIneptitude1 = 5075u,
        TwoHandedIneptitude2 = 5076u,
        TwoHandedIneptitude3 = 5077u,
        TwoHandedIneptitude4 = 5078u,
        TwoHandedIneptitude5 = 5079u,
        TwoHandedIneptitude6 = 5080u,
        TwoHandedIneptitude7 = 5081u,
        TwoHandedIneptitude8 = 5082u,
        TwoHandedIneptitudeSelf1 = 5083u,
        TwoHandedIneptitudeSelf2 = 5084u,
        TwoHandedIneptitudeSelf3 = 5085u,
        TwoHandedIneptitudeSelf4 = 5086u,
        TwoHandedIneptitudeSelf5 = 5087u,
        TwoHandedIneptitudeSelf6 = 5088u,
        TwoHandedIneptitudeSelf7 = 5089u,
        TwoHandedIneptitudeSelf8 = 5090u,
        TwoHandedMasteryOther1 = 5091u,
        TwoHandedMasteryOther2 = 5092u,
        TwoHandedMasteryOther3 = 5093u,
        TwoHandedMasteryOther4 = 5094u,
        TwoHandedMasteryOther5 = 5095u,
        TwoHandedMasteryOther6 = 5096u,
        TwoHandedMasteryOther7 = 5097u,
        TwoHandedMasteryOther8 = 5098u,
        TwoHandedMasterySelf1 = 5099u,
        TwoHandedMasterySelf2 = 5100u,
        TwoHandedMasterySelf3 = 5101u,
        TwoHandedMasterySelf4 = 5102u,
        TwoHandedMasterySelf5 = 5103u,
        TwoHandedMasterySelf6 = 5104u,
        TwoHandedMasterySelf7 = 5105u,
        SetGearCraftAptitude4 = 5106u,
        SetTwoHandedAptitude1 = 5107u,
        SetTwoHandedAptitude2 = 5108u,
        SetTwoHandedAptitude3 = 5109u,
        SetTwoHandedAptitude4 = 5110u,
        SetGearCraftAptitude1 = 5111u,
        SetGearCraftAptitude2 = 5112u,
        SetGearCraftAptitude3 = 5113u,
        ExposeWeakness8 = 5114u,
        ExposeWeakness1 = 5115u,
        ExposeWeakness2 = 5116u,
        ExposeWeakness3 = 5117u,
        ExposeWeakness4 = 5118u,
        ExposeWeakness5 = 5119u,
        ExposeWeakness6 = 5120u,
        ExposeWeakness7 = 5121u,
        CallOfLeadership5 = 5122u,
        AnswerOfLoyaltyMana1 = 5123u,
        AnswerOfLoyaltyMana2 = 5124u,
        AnswerOfLoyaltyMana3 = 5125u,
        AnswerOfLoyaltyMana4 = 5126u,
        AnswerOfLoyaltyMana5 = 5127u,
        AnswerOfLoyaltyStam1 = 5128u,
        AnswerOfLoyaltyStam2 = 5129u,
        AnswerOfLoyaltyStam3 = 5130u,
        AnswerOfLoyaltyStam4 = 5131u,
        AnswerOfLoyaltyStam5 = 5132u,
        CallOfLeadership1 = 5133u,
        CallOfLeadership2 = 5134u,
        CallOfLeadership3 = 5135u,
        CallOfLeadership4 = 5136u,
        TrinketXPBoost3 = 5137u,
        TrinketDamageBoost1 = 5138u,
        TrinketDamageBoost2 = 5139u,
        TrinketDamageBoost3 = 5140u,
        TrinketDamageReduction1 = 5141u,
        TrinketDamageReduction2 = 5142u,
        TrinketDamageReduction3 = 5143u,
        TrinketHealth1 = 5144u,
        TrinketHealth2 = 5145u,
        TrinketHealth3 = 5146u,
        TrinketMana1 = 5147u,
        TrinketMana2 = 5148u,
        TrinketMana3 = 5149u,
        TrinketStamina1 = 5150u,
        TrinketStamina2 = 5151u,
        TrinketStamina3 = 5152u,
        TrinketXPBoost1 = 5153u,
        TrinketXPBoost2 = 5154u,
        DeceptionArcane4 = 5155u,
        DeceptionArcane5 = 5156u,
        DeceptionArcane1 = 5157u,
        DeceptionArcane2 = 5158u,
        DeceptionArcane3 = 5159u,
        PortalMhoireCastle = 5160u,
        PortalMhoireCastleGreatHall = 5161u,
        PortalMhoireCastleNortheast = 5162u,
        PortalMhoireCastleNorthwest = 5163u,
        PortalMhoireCastleSoutheast = 5164u,
        PortalMhoireCastleSouthwest = 5165u,
        FlamingSkullTrap = 5166u,
        PortalMhoireCastleExit = 5167u,
        SpectralFountain_PortalFreefall = 5168u,
        SpectralFountain_PortalMaze1 = 5169u,
        SpectralFountain_PortalMaze2 = 5170u,
        SpectralFountain_HoT = 5171u,
        SpectralFountain_DoT = 5172u,
        SpectralFountain_HealDebuff = 5173u,
        SpectralFountain_DamageBoost = 5174u,
        PortalSendHubNPE = 5175u,
        PortalCHSocietyAug = 5176u,
        PortalRBSocietyAug = 5177u,
        PortalEWSocietyAug = 5178u,
        PortalCHSocietyAugTwo = 5179u,
        PortalRBSocietyAugTwo = 5180u,
        PortalEWSocietyAugTwo = 5181u,
        SpiritDrinker8Cast = 5182u,
        BloodDrinker8Cast = 5183u,
        RareDamageBoost7 = 5184u,
        RareDamageBoost8 = 5185u,
        RareDamageBoost9 = 5186u,
        RareDamageBoost10 = 5187u,
        RareDamageReduction1 = 5188u,
        RareDamageReduction2 = 5189u,
        RareDamageReduction3 = 5190u,
        RareDamageReduction4 = 5191u,
        RareDamageReduction5 = 5192u,
        RareDamageReduction6 = 5193u,
        RareDamageReduction7 = 5194u,
        RareDamageReduction8 = 5195u,
        RareDamageReduction9 = 5196u,
        RareDamageReduction10 = 5197u,
        RareDamageBoost1 = 5198u,
        RareDamageBoost2 = 5199u,
        RareDamageBoost3 = 5200u,
        RareDamageBoost4 = 5201u,
        RareDamageBoost5 = 5202u,
        RareDamageBoost6 = 5203u,
        AetheriaProcDamageBoost = 5204u,
        AetheriaProcDamageOverTime = 5205u,
        AetheriaProcDamageReduction = 5206u,
        AetheriaProcHealDebuff = 5207u,
        AetheriaProcHealthOverTime = 5208u,
        AetheriaCriticalDamageBoost1 = 5209u,
        AetheriaCriticalDamageBoost2 = 5210u,
        AetheriaCriticalDamageBoost3 = 5211u,
        AetheriaCriticalDamageBoost4 = 5212u,
        AetheriaCriticalDamageBoost5 = 5213u,
        AetheriaCriticalDamageBoost6 = 5214u,
        AetheriaCriticalDamageBoost7 = 5215u,
        AetheriaCriticalDamageBoost8 = 5216u,
        AetheriaCriticalDamageBoost9 = 5217u,
        AetheriaCriticalDamageBoost10 = 5218u,
        AetheriaCriticalDamageBoost11 = 5219u,
        AetheriaCriticalDamageBoost12 = 5220u,
        AetheriaCriticalDamageBoost13 = 5221u,
        AetheriaCriticalDamageBoost14 = 5222u,
        AetheriaCriticalDamageBoost15 = 5223u,
        AetheriaDamageBoost1 = 5224u,
        AetheriaDamageBoost2 = 5225u,
        AetheriaDamageBoost3 = 5226u,
        AetheriaDamageBoost4 = 5227u,
        AetheriaDamageBoost5 = 5228u,
        AetheriaDamageBoost6 = 5229u,
        AetheriaDamageBoost7 = 5230u,
        AetheriaDamageBoost8 = 5231u,
        AetheriaDamageBoost9 = 5232u,
        AetheriaDamageBoost10 = 5233u,
        AetheriaDamageBoost11 = 5234u,
        AetheriaDamageBoost12 = 5235u,
        AetheriaDamageBoost13 = 5236u,
        AetheriaDamageBoost14 = 5237u,
        AetheriaDamageBoost15 = 5238u,
        AetheriaDamageReduction1 = 5239u,
        AetheriaDamageReduction2 = 5240u,
        AetheriaDamageReduction3 = 5241u,
        AetheriaDamageReduction4 = 5242u,
        AetheriaDamageReduction5 = 5243u,
        AetheriaDamageReduction6 = 5244u,
        AetheriaDamageReduction7 = 5245u,
        AetheriaDamageReduction8 = 5246u,
        AetheriaDamageReduction9 = 5247u,
        AetheriaDamageReduction10 = 5248u,
        AetheriaDamageReduction11 = 5249u,
        AetheriaDamageReduction12 = 5250u,
        AetheriaDamageReduction13 = 5251u,
        AetheriaDamageReduction14 = 5252u,
        AetheriaDamageReduction15 = 5253u,
        AetheriaHealBuff1 = 5254u,
        AetheriaHealBuff2 = 5255u,
        AetheriaHealBuff3 = 5256u,
        AetheriaHealBuff4 = 5257u,
        AetheriaHealBuff5 = 5258u,
        AetheriaHealBuff6 = 5259u,
        AetheriaHealBuff7 = 5260u,
        AetheriaHealBuff8 = 5261u,
        AetheriaHealBuff9 = 5262u,
        AetheriaHealBuff10 = 5263u,
        AetheriaHealBuff11 = 5264u,
        AetheriaHealBuff12 = 5265u,
        AetheriaHealBuff13 = 5266u,
        AetheriaHealBuff14 = 5267u,
        AetheriaHealBuff15 = 5268u,
        AetheriaHealth1 = 5269u,
        AetheriaHealth2 = 5270u,
        AetheriaHealth3 = 5271u,
        AetheriaHealth4 = 5272u,
        AetheriaHealth5 = 5273u,
        AetheriaHealth6 = 5274u,
        AetheriaHealth7 = 5275u,
        AetheriaHealth8 = 5276u,
        AetheriaHealth9 = 5277u,
        AetheriaHealth10 = 5278u,
        AetheriaHealth11 = 5279u,
        AetheriaHealth12 = 5280u,
        AetheriaHealth13 = 5281u,
        AetheriaHealth14 = 5282u,
        AetheriaHealth15 = 5283u,
        AetheriaMana1 = 5284u,
        AetheriaMana2 = 5285u,
        AetheriaMana3 = 5286u,
        AetheriaMana4 = 5287u,
        AetheriaMana5 = 5288u,
        AetheriaMana6 = 5289u,
        AetheriaMana7 = 5290u,
        AetheriaMana8 = 5291u,
        AetheriaMana9 = 5292u,
        AetheriaMana10 = 5293u,
        AetheriaMana11 = 5294u,
        AetheriaMana12 = 5295u,
        AetheriaMana13 = 5296u,
        AetheriaMana14 = 5297u,
        AetheriaMana15 = 5298u,
        AetheriaStamina1 = 5299u,
        AetheriaStamina2 = 5300u,
        AetheriaStamina3 = 5301u,
        AetheriaStamina4 = 5302u,
        AetheriaStamina5 = 5303u,
        AetheriaStamina6 = 5304u,
        AetheriaStamina7 = 5305u,
        AetheriaStamina8 = 5306u,
        AetheriaStamina9 = 5307u,
        AetheriaStamina10 = 5308u,
        AetheriaStamina11 = 5309u,
        AetheriaStamina12 = 5310u,
        AetheriaStamina13 = 5311u,
        AetheriaStamina14 = 5312u,
        AetheriaStamina15 = 5313u,
        BlessingOfUnity = 5314u,
        AetheriaEndurance1 = 5315u,
        AetheriaEndurance2 = 5316u,
        AetheriaEndurance3 = 5317u,
        AetheriaEndurance4 = 5318u,
        AetheriaEndurance5 = 5319u,
        AetheriaEndurance6 = 5320u,
        AetheriaEndurance7 = 5321u,
        AetheriaEndurance8 = 5322u,
        AetheriaEndurance9 = 5323u,
        AetheriaEndurance10 = 5324u,
        AetheriaEndurance11 = 5325u,
        AetheriaEndurance12 = 5326u,
        AetheriaEndurance13 = 5327u,
        AetheriaEndurance14 = 5328u,
        AetheriaEndurance15 = 5329u,
        RecallGearKnightInvasionHigh = 5330u,
        BaelzharonsNetherRing = 5331u,
        BaelzharonsNetherStreak = 5332u,
        BaelzharonsNetherArc = 5333u,
        BaelzharonsCurseDestruction = 5334u,
        BaelzharonsCurseDestruction2 = 5335u,
        BaelzharonsCurseFestering = 5336u,
        CurseDestructionOther7 = 5337u,
        CurseDestructionOther8 = 5338u,
        CurseDestructionOther1 = 5339u,
        CurseDestructionOther2 = 5340u,
        CurseDestructionOther3 = 5341u,
        CurseDestructionOther4 = 5342u,
        CurseDestructionOther5 = 5343u,
        CurseDestructionOther6 = 5344u,
        NetherStreak5 = 5345u,
        NetherStreak6 = 5346u,
        NetherStreak7 = 5347u,
        NetherStreak8 = 5348u,
        NetherBolt1 = 5349u,
        NetherBolt2 = 5350u,
        NetherBolt3 = 5351u,
        NetherBolt4 = 5352u,
        NetherBolt5 = 5353u,
        NetherBolt6 = 5354u,
        NetherBolt7 = 5355u,
        NetherBolt8 = 5356u,
        NetherStreak1 = 5357u,
        NetherStreak2 = 5358u,
        NetherStreak3 = 5359u,
        NetherStreak4 = 5360u,
        NetherRing = 5361u,
        NetherArc2 = 5362u,
        NetherArc3 = 5363u,
        NetherArc4 = 5364u,
        NetherArc5 = 5365u,
        NetherArc6 = 5366u,
        NetherArc7 = 5367u,
        NetherArc8 = 5368u,
        NetherArc1 = 5369u,
        TestStreakDoTMajor = 5370u,
        CurseFestering1 = 5371u,
        CurseFestering2 = 5372u,
        CurseFestering3 = 5373u,
        CurseFestering4 = 5374u,
        CurseFestering5 = 5375u,
        CurseFestering6 = 5376u,
        CurseFestering7 = 5377u,
        CurseFestering8 = 5378u,
        CurseWeakness1 = 5379u,
        CurseWeakness2 = 5380u,
        CurseWeakness3 = 5381u,
        CurseWeakness4 = 5382u,
        CurseWeakness5 = 5383u,
        CurseWeakness6 = 5384u,
        CurseWeakness7 = 5385u,
        CurseWeakness8 = 5386u,
        Corrosion1 = 5387u,
        Corrosion2 = 5388u,
        Corrosion3 = 5389u,
        Corrosion4 = 5390u,
        Corrosion5 = 5391u,
        Corrosion6 = 5392u,
        Corrosion7 = 5393u,
        Corrosion8 = 5394u,
        Corruption1 = 5395u,
        Corruption2 = 5396u,
        Corruption3 = 5397u,
        Corruption4 = 5398u,
        Corruption5 = 5399u,
        Corruption6 = 5400u,
        Corruption7 = 5401u,
        Corruption8 = 5402u,
        VoidMagicMasteryOther1 = 5403u,
        VoidMagicMasteryOther2 = 5404u,
        VoidMagicMasteryOther3 = 5405u,
        VoidMagicMasteryOther4 = 5406u,
        VoidMagicMasteryOther5 = 5407u,
        VoidMagicMasteryOther6 = 5408u,
        VoidMagicMasteryOther7 = 5409u,
        VoidMagicMasteryOther8 = 5410u,
        VoidMagicMasterySelf1 = 5411u,
        VoidMagicMasterySelf2 = 5412u,
        VoidMagicMasterySelf3 = 5413u,
        VoidMagicMasterySelf4 = 5414u,
        VoidMagicMasterySelf5 = 5415u,
        VoidMagicMasterySelf6 = 5416u,
        VoidMagicMasterySelf7 = 5417u,
        VoidMagicMasterySelf8 = 5418u,
        VoidMagicIneptitudeOther1 = 5419u,
        VoidMagicIneptitudeOther2 = 5420u,
        VoidMagicIneptitudeOther3 = 5421u,
        VoidMagicIneptitudeOther4 = 5422u,
        VoidMagicIneptitudeOther5 = 5423u,
        VoidMagicIneptitudeOther6 = 5424u,
        VoidMagicIneptitudeOther7 = 5425u,
        VoidMagicIneptitudeOther8 = 5426u,
        CantripVoidMagicAptitude1 = 5427u,
        CantripVoidMagicAptitude2 = 5428u,
        CantripVoidMagicAptitude3 = 5429u,
        ModerateVoidMagicAptitude = 5430u,
        SetVoidMagicAptitude1 = 5431u,
        SetVoidMagicAptitude2 = 5432u,
        SetVoidMagicAptitude3 = 5433u,
        SetVoidMagicAptitude4 = 5434u,
        VoidMagicMasterySpectral = 5435u,
        VoidMagicMasteryRare = 5436u,
        CorruptorsBoon = 5437u,
        CorruptorsBoon3 = 5438u,
        AcidSpitStreak1 = 5439u,
        AcidSpit1 = 5440u,
        AcidSpit2 = 5441u,
        AcidSpitArc1 = 5442u,
        AcidSpitArc2 = 5443u,
        AcidSpitBlast1 = 5444u,
        AcidspitBlast2 = 5445u,
        AcidSpitVolley1 = 5446u,
        AcidSpitVolley2 = 5447u,
        AcidSpitStreak2 = 5448u,
        LuminanceRewardDamageBoost = 5449u,
        LuminanceRewardDamageReduction = 5450u,
        LuminanceRewardHealthRaising = 5451u,
        OlthoiWillpowerOther1 = 5452u,
        OlthoiArmorOther1 = 5453u,
        OlthoiCoordinationOther1 = 5454u,
        OlthoiEnduranceOther1 = 5455u,
        OlthoiFocusOther1 = 5456u,
        OlthoiQuicknessOther1 = 5457u,
        OlthoiStrengthOther1 = 5458u,
        OlthoiPiercingProtectionOther1 = 5459u,
        OlthoiAcidProtectionOther1 = 5460u,
        OlthoiBladeProtectionOther1 = 5461u,
        OlthoiBludgeonProtectionOther1 = 5462u,
        OlthoiColdProtectionOther1 = 5463u,
        OlthoiFireProtectionOther1 = 5464u,
        OlthoiLightningProtectionOther1 = 5465u,
        OlthoiRejuvenationOther1 = 5466u,
        OlthoiManaRenewalOther1 = 5467u,
        OlthoiRegenerationOther1 = 5468u,
        OlthoiImpregnabilityOther1 = 5469u,
        OlthoiInvulnerabilityOther1 = 5470u,
        OlthoiMagicResistanceOther1 = 5471u,
        OlthoiManaMasteryOther1 = 5472u,
        OlthoiSalvagingMasteryOther1 = 5473u,
        OlthoiSprintOther1 = 5474u,
        OlthoiUnarmedCombatMasteryOther1 = 5475u,
        OlthoiWarMagicMasteryOther1 = 5476u,
        OlthoiCriticalDamageBoost1 = 5477u,
        OlthoiCriticalDamageBoost2 = 5478u,
        OlthoiCriticalDamageBoost3 = 5479u,
        OlthoiCriticalDamageBoost4 = 5480u,
        OlthoiCriticalDamageBoost5 = 5481u,
        OlthoiCriticalDamageBoost6 = 5482u,
        OlthoiCriticalDamageBoost7 = 5483u,
        OlthoiCriticalDamageBoost8 = 5484u,
        OlthoiCriticalDamageBoost9 = 5485u,
        OlthoiCriticalDamageBoost10 = 5486u,
        OlthoiCriticalDamageBoost11 = 5487u,
        UNKNOWN__GUESSEDNAME5488 = 5488u,
        OlthoiCriticalDamageReduction1 = 5489u,
        OlthoiCriticalDamageReduction2 = 5490u,
        OlthoiCriticalDamageReduction3 = 5491u,
        OlthoiCriticalDamageReduction4 = 5492u,
        OlthoiCriticalDamageReduction5 = 5493u,
        OlthoiCriticalDamageReduction6 = 5494u,
        OlthoiCriticalDamageReduction7 = 5495u,
        OlthoiCriticalDamageReduction8 = 5496u,
        OlthoiCriticalDamageReduction9 = 5497u,
        OlthoiCriticalDamageReduction10 = 5498u,
        OlthoiCriticalDamageReduction11 = 5499u,
        OlthoiDamageBoost1 = 5500u,
        OlthoiDamageBoost2 = 5501u,
        OlthoiDamageBoost3 = 5502u,
        OlthoiDamageBoost4 = 5503u,
        OlthoiDamageBoost5 = 5504u,
        OlthoiDamageBoost6 = 5505u,
        OlthoiDamageBoost7 = 5506u,
        OlthoiDamageBoost8 = 5507u,
        OlthoiDamageBoost9 = 5508u,
        OlthoiDamageBoost10 = 5509u,
        OlthoiDamageBoost11 = 5510u,
        OlthoiDamageReduction1 = 5511u,
        OlthoiDamageReduction2 = 5512u,
        OlthoiDamageReduction3 = 5513u,
        OlthoiDamageReduction4 = 5514u,
        OlthoiDamageReduction5 = 5515u,
        OlthoiDamageReduction6 = 5516u,
        OlthoiDamageReduction7 = 5517u,
        OlthoiDamageReduction8 = 5518u,
        OlthoiDamageReduction9 = 5519u,
        OlthoiDamageReduction10 = 5520u,
        OlthoiDamageReduction11 = 5521u,
        AcidSpitVulnerability1 = 5522u,
        AcidSpitVulnerability2 = 5523u,
        FallingStoneSpikes = 5524u,
        BloodstoneBolt1 = 5525u,
        BloodstoneBolt2 = 5526u,
        BloodstoneBolt3 = 5527u,
        BloodstoneBolt4 = 5528u,
        BloodstoneBolt5 = 5529u,
        BloodstoneBolt6 = 5530u,
        BloodstoneBolt7 = 5531u,
        BloodstoneBolt8 = 5532u,
        PortalSendingBloodstoneFactory1 = 5533u,
        PortalSendingBloodstoneFactory2 = 5534u,
        AcidicBlood = 5535u,
        AcidicBloodGreater = 5536u,
        AcidicBloodLesser = 5537u,
        DarkenedHeart = 5538u,
        PortalSendingRitualTime1 = 5539u,
        PortalSendingRitualTime2 = 5540u,
        SandAreaRecall = 5541u,
        BurningSands_DoT = 5542u,
        BurningSands_HealDebuff = 5543u,
        NetherBlast1 = 5544u,
        NetherBlast2 = 5545u,
        NetherBlast3 = 5546u,
        NetherBlast4 = 5547u,
        NetherBlast5 = 5548u,
        NetherBlast6 = 5549u,
        NetherBlast7 = 5550u,
        NetherBlast8 = 5551u,
        AetheriaDoTResistance9 = 5552u,
        AetheriaHealthResistance1 = 5553u,
        AetheriaHealthResistance10 = 5554u,
        AetheriaHealthResistance11 = 5555u,
        AetheriaHealthResistance12 = 5556u,
        AetheriaHealthResistance13 = 5557u,
        AetheriaHealthResistance14 = 5558u,
        AetheriaHealthResistance15 = 5559u,
        AetheriaHealthResistance2 = 5560u,
        AetheriaHealthResistance3 = 5561u,
        AetheriaHealthResistance4 = 5562u,
        AetheriaHealthResistance5 = 5563u,
        AetheriaHealthResistance6 = 5564u,
        AetheriaHealthResistance7 = 5565u,
        AetheriaHealthResistance8 = 5566u,
        AetheriaHealthResistance9 = 5567u,
        AetheriaDoTResistance1 = 5568u,
        AetheriaDoTResistance10 = 5569u,
        AetheriaDoTResistance11 = 5570u,
        AetheriaDoTResistance12 = 5571u,
        AetheriaDoTResistance13 = 5572u,
        AetheriaDoTResistance14 = 5573u,
        AetheriaDoTResistance15 = 5574u,
        AetheriaDoTResistance2 = 5575u,
        AetheriaDoTResistance3 = 5576u,
        AetheriaDoTResistance4 = 5577u,
        AetheriaDoTResistance5 = 5578u,
        AetheriaDoTResistance6 = 5579u,
        AetheriaDoTResistance7 = 5580u,
        AetheriaDoTResistance8 = 5581u,
        DispelAllRare = 5582u,
        CloakAlchemyMastery1 = 5583u,
        CloakAlchemyMastery2 = 5584u,
        CloakAlchemyMastery3 = 5585u,
        CloakAlchemyMastery4 = 5586u,
        CloakAlchemyMastery5 = 5587u,
        CloakArcaneloreMastery1 = 5588u,
        CloakArcaneloreMastery2 = 5589u,
        CloakArcaneloreMastery3 = 5590u,
        CloakArcaneloreMastery4 = 5591u,
        CloakArcaneloreMastery5 = 5592u,
        CloakArmortinkeringMastery1 = 5593u,
        CloakArmortinkeringMastery2 = 5594u,
        CloakArmortinkeringMastery3 = 5595u,
        CloakArmortinkeringMastery4 = 5596u,
        CloakArmortinkeringMastery5 = 5597u,
        CloakAssesspersonMastery1 = 5598u,
        CloakAssesspersonMastery2 = 5599u,
        CloakAssesspersonMastery3 = 5600u,
        CloakAssesspersonMastery4 = 5601u,
        CloakAssesspersonMastery5 = 5602u,
        CloakAxeMastery1 = 5603u,
        CloakAxeMastery2 = 5604u,
        CloakAxeMastery3 = 5605u,
        CloakAxeMastery4 = 5606u,
        CloakAxeMastery5 = 5607u,
        CloakBowMastery1 = 5608u,
        CloakBowMastery2 = 5609u,
        CloakBowMastery3 = 5610u,
        CloakBowMastery4 = 5611u,
        CloakBowMastery5 = 5612u,
        CloakCookingMastery1 = 5613u,
        CloakCookingMastery2 = 5614u,
        CloakCookingMastery3 = 5615u,
        CloakCookingMastery4 = 5616u,
        CloakCookingMastery5 = 5617u,
        CloakCreatureenchantmentMastery1 = 5618u,
        CloakCreatureenchantmentMastery2 = 5619u,
        CloakCreatureenchantmentMastery3 = 5620u,
        CloakCreatureenchantmentMastery4 = 5621u,
        CloakCreatureenchantmentMastery5 = 5622u,
        CloakCrossbowMastery1 = 5623u,
        CloakCrossbowMastery2 = 5624u,
        CloakCrossbowMastery3 = 5625u,
        CloakCrossbowMastery4 = 5626u,
        CloakCrossbowMastery5 = 5627u,
        CloakDaggerMastery1 = 5628u,
        CloakDaggerMastery2 = 5629u,
        CloakDaggerMastery3 = 5630u,
        CloakDaggerMastery4 = 5631u,
        CloakDaggerMastery5 = 5632u,
        CloakDeceptionMastery1 = 5633u,
        CloakDeceptionMastery2 = 5634u,
        CloakDeceptionMastery3 = 5635u,
        CloakDeceptionMastery4 = 5636u,
        CloakDeceptionMastery5 = 5637u,
        CloakFletchingMastery1 = 5638u,
        CloakFletchingMastery2 = 5639u,
        CloakFletchingMastery3 = 5640u,
        CloakFletchingMastery4 = 5641u,
        CloakFletchingMastery5 = 5642u,
        CloakHealingMastery1 = 5643u,
        CloakHealingMastery2 = 5644u,
        CloakHealingMastery3 = 5645u,
        CloakHealingMastery4 = 5646u,
        CloakHealingMastery5 = 5647u,
        CloakItemenchantmentMastery1 = 5648u,
        CloakItemenchantmentMastery2 = 5649u,
        CloakItemenchantmentMastery3 = 5650u,
        CloakItemenchantmentMastery4 = 5651u,
        CloakItemenchantmentMastery5 = 5652u,
        CloakItemtinkeringMastery1 = 5653u,
        CloakItemtinkeringMastery2 = 5654u,
        CloakItemtinkeringMastery3 = 5655u,
        CloakItemtinkeringMastery4 = 5656u,
        CloakItemtinkeringMastery5 = 5657u,
        CloakLeadershipMastery1 = 5658u,
        CloakLeadershipMastery2 = 5659u,
        CloakLeadershipMastery3 = 5660u,
        CloakLeadershipMastery4 = 5661u,
        CloakLeadershipMastery5 = 5662u,
        CloakLifemagicMastery1 = 5663u,
        CloakLifemagicMastery2 = 5664u,
        CloakLifemagicMastery3 = 5665u,
        CloakLifemagicMastery4 = 5666u,
        CloakLifemagicMastery5 = 5667u,
        CloakLoyaltyMastery1 = 5668u,
        CloakLoyaltyMastery2 = 5669u,
        CloakLoyaltyMastery3 = 5670u,
        CloakLoyaltyMastery4 = 5671u,
        CloakLoyaltyMastery5 = 5672u,
        CloakMaceMastery1 = 5673u,
        CloakMaceMastery2 = 5674u,
        CloakMaceMastery3 = 5675u,
        CloakMaceMastery4 = 5676u,
        CloakMaceMastery5 = 5677u,
        CloakMagicdefenseMastery1 = 5678u,
        CloakMagicdefenseMastery2 = 5679u,
        CloakMagicdefenseMastery3 = 5680u,
        CloakMagicdefenseMastery4 = 5681u,
        CloakMagicdefenseMastery5 = 5682u,
        CloakMagictinkeringMastery1 = 5683u,
        CloakMagictinkeringMastery2 = 5684u,
        CloakMagictinkeringMastery3 = 5685u,
        CloakMagictinkeringMastery4 = 5686u,
        CloakMagictinkeringMastery5 = 5687u,
        CloakManaconversionMastery1 = 5688u,
        CloakManaconversionMastery2 = 5689u,
        CloakManaconversionMastery3 = 5690u,
        CloakManaconversionMastery4 = 5691u,
        CloakManaconversionMastery5 = 5692u,
        CloakMeleedefenseMastery1 = 5693u,
        CloakMeleedefenseMastery2 = 5694u,
        CloakMeleedefenseMastery3 = 5695u,
        CloakMeleedefenseMastery4 = 5696u,
        CloakMeleedefenseMastery5 = 5697u,
        CloakMissiledefenseMastery1 = 5698u,
        CloakMissiledefenseMastery2 = 5699u,
        CloakMissiledefenseMastery3 = 5700u,
        CloakMissiledefenseMastery4 = 5701u,
        CloakMissiledefenseMastery5 = 5702u,
        CloakSalvagingMastery1 = 5703u,
        CloakSalvagingMastery2 = 5704u,
        CloakSalvagingMastery3 = 5705u,
        CloakSalvagingMastery4 = 5706u,
        CloakSalvagingMastery5 = 5707u,
        CloakSpearMastery1 = 5708u,
        CloakSpearMastery2 = 5709u,
        CloakSpearMastery3 = 5710u,
        CloakSpearMastery4 = 5711u,
        CloakSpearMastery5 = 5712u,
        CloakStaffMastery1 = 5713u,
        CloakStaffMastery2 = 5714u,
        CloakStaffMastery3 = 5715u,
        CloakStaffMastery4 = 5716u,
        CloakStaffMastery5 = 5717u,
        CloakSwordMastery1 = 5718u,
        CloakSwordMastery2 = 5719u,
        CloakSwordMastery3 = 5720u,
        CloakSwordMastery4 = 5721u,
        CloakSwordMastery5 = 5722u,
        CloakThrownWeaponMastery1 = 5723u,
        CloakThrownWeaponMastery2 = 5724u,
        CloakThrownWeaponMastery3 = 5725u,
        CloakThrownWeaponMastery4 = 5726u,
        CloakThrownWeaponMastery5 = 5727u,
        CloakTwoHandedCombatMastery1 = 5728u,
        CloakTwoHandedCombatMastery2 = 5729u,
        CloakTwoHandedCombatMastery3 = 5730u,
        CloakTwoHandedCombatMastery4 = 5731u,
        CloakTwoHandedCombatMastery5 = 5732u,
        CloakUnarmedCombatMastery1 = 5733u,
        CloakUnarmedCombatMastery2 = 5734u,
        CloakUnarmedCombatMastery3 = 5735u,
        CloakUnarmedCombatMastery4 = 5736u,
        CloakUnarmedCombatMastery5 = 5737u,
        CloakVoidMagicMastery1 = 5738u,
        CloakVoidMagicMastery2 = 5739u,
        CloakVoidMagicMastery3 = 5740u,
        CloakVoidMagicMastery4 = 5741u,
        CloakVoidMagicMastery5 = 5742u,
        CloakWarMagicMastery1 = 5743u,
        CloakWarMagicMastery2 = 5744u,
        CloakWarMagicMastery3 = 5745u,
        CloakWarMagicMastery4 = 5746u,
        CloakWarMagicMastery5 = 5747u,
        CloakWeapontinkeringMastery1 = 5748u,
        CloakWeapontinkeringMastery2 = 5749u,
        CloakWeapontinkeringMastery3 = 5750u,
        CloakWeapontinkeringMastery4 = 5751u,
        CloakWeapontinkeringMastery5 = 5752u,
        CloakAllSkill = 5753u,
        CloakMagicDLower = 5754u,
        CloakMeleeDLower = 5755u,
        CloakMissileDLower = 5756u,
        CloakAssessCreatureMastery3 = 5757u,
        CloakAssessCreatureMastery4 = 5758u,
        CloakAssessCreatureMastery5 = 5759u,
        CloakAssessCreatureMastery1 = 5760u,
        CloakAssessCreatureMastery2 = 5761u,
        RollingDeathSnow = 5762u,
        DirtyFightingIneptitudeOther1 = 5763u,
        DirtyFightingIneptitudeOther2 = 5764u,
        DirtyFightingIneptitudeOther3 = 5765u,
        DirtyFightingIneptitudeOther4 = 5766u,
        DirtyFightingIneptitudeOther5 = 5767u,
        DirtyFightingIneptitudeOther6 = 5768u,
        DirtyFightingIneptitudeOther7 = 5769u,
        DirtyFightingIneptitudeOther8 = 5770u,
        DirtyFightingMasteryOther1 = 5771u,
        DirtyFightingMasteryOther2 = 5772u,
        DirtyFightingMasteryOther3 = 5773u,
        DirtyFightingMasteryOther4 = 5774u,
        DirtyFightingMasteryOther5 = 5775u,
        DirtyFightingMasteryOther6 = 5776u,
        DirtyFightingMasteryOther7 = 5777u,
        DirtyFightingMasteryOther8 = 5778u,
        DirtyFightingMasterySelf1 = 5779u,
        DirtyFightingMasterySelf2 = 5780u,
        DirtyFightingMasterySelf3 = 5781u,
        DirtyFightingMasterySelf4 = 5782u,
        DirtyFightingMasterySelf5 = 5783u,
        DirtyFightingMasterySelf6 = 5784u,
        DirtyFightingMasterySelf7 = 5785u,
        DirtyFightingMasterySelf8 = 5786u,
        DualWieldIneptitudeOther1 = 5787u,
        DualWieldIneptitudeOther2 = 5788u,
        DualWieldIneptitudeOther3 = 5789u,
        DualWieldIneptitudeOther4 = 5790u,
        DualWieldIneptitudeOther5 = 5791u,
        DualWieldIneptitudeOther6 = 5792u,
        DualWieldIneptitudeOther7 = 5793u,
        DualWieldIneptitudeOther8 = 5794u,
        DualWieldMasteryOther1 = 5795u,
        DualWieldMasteryOther2 = 5796u,
        DualWieldMasteryOther3 = 5797u,
        DualWieldMasteryOther4 = 5798u,
        DualWieldMasteryOther5 = 5799u,
        DualWieldMasteryOther6 = 5800u,
        DualWieldMasteryOther7 = 5801u,
        DualWieldMasteryOther8 = 5802u,
        DualWieldMasterySelf1 = 5803u,
        DualWieldMasterySelf2 = 5804u,
        DualWieldMasterySelf3 = 5805u,
        DualWieldMasterySelf4 = 5806u,
        DualWieldMasterySelf5 = 5807u,
        DualWieldMasterySelf6 = 5808u,
        DualWieldMasterySelf7 = 5809u,
        DualWieldMasterySelf8 = 5810u,
        RecklessnessIneptitudeOther1 = 5811u,
        RecklessnessIneptitudeOther2 = 5812u,
        RecklessnessIneptitudeOther3 = 5813u,
        RecklessnessIneptitudeOther4 = 5814u,
        RecklessnessIneptitudeOther5 = 5815u,
        RecklessnessIneptitudeOther6 = 5816u,
        RecklessnessIneptitudeOther7 = 5817u,
        RecklessnessIneptitudeOther8 = 5818u,
        RecklessnessMasteryOther1 = 5819u,
        RecklessnessMasteryOther2 = 5820u,
        RecklessnessMasteryOther3 = 5821u,
        RecklessnessMasteryOther4 = 5822u,
        RecklessnessMasteryOther5 = 5823u,
        RecklessnessMasteryOther6 = 5824u,
        RecklessnessMasteryOther7 = 5825u,
        RecklessnessMasteryOther8 = 5826u,
        RecklessnessMasterySelf1 = 5827u,
        RecklessnessMasterySelf2 = 5828u,
        RecklessnessMasterySelf3 = 5829u,
        RecklessnessMasterySelf4 = 5830u,
        RecklessnessMasterySelf5 = 5831u,
        RecklessnessMasterySelf6 = 5832u,
        RecklessnessMasterySelf7 = 5833u,
        RecklessnessMasterySelf8 = 5834u,
        ShieldIneptitudeOther1 = 5835u,
        ShieldIneptitudeOther2 = 5836u,
        ShieldIneptitudeOther3 = 5837u,
        ShieldIneptitudeOther4 = 5838u,
        ShieldIneptitudeOther5 = 5839u,
        ShieldIneptitudeOther6 = 5840u,
        ShieldIneptitudeOther7 = 5841u,
        ShieldIneptitudeOther8 = 5842u,
        ShieldMasteryOther1 = 5843u,
        ShieldMasteryOther2 = 5844u,
        ShieldMasteryOther3 = 5845u,
        ShieldMasteryOther4 = 5846u,
        ShieldMasteryOther5 = 5847u,
        ShieldMasteryOther6 = 5848u,
        ShieldMasteryOther7 = 5849u,
        ShieldMasteryOther8 = 5850u,
        ShieldMasterySelf1 = 5851u,
        ShieldMasterySelf2 = 5852u,
        ShieldMasterySelf3 = 5853u,
        ShieldMasterySelf4 = 5854u,
        ShieldMasterySelf5 = 5855u,
        ShieldMasterySelf6 = 5856u,
        ShieldMasterySelf7 = 5857u,
        ShieldMasterySelf8 = 5858u,
        SneakAttackIneptitudeOther1 = 5859u,
        SneakAttackIneptitudeOther2 = 5860u,
        SneakAttackIneptitudeOther3 = 5861u,
        SneakAttackIneptitudeOther4 = 5862u,
        SneakAttackIneptitudeOther5 = 5863u,
        SneakAttackIneptitudeOther6 = 5864u,
        SneakAttackIneptitudeOther7 = 5865u,
        SneakAttackIneptitudeOther8 = 5866u,
        SneakAttackMasteryOther1 = 5867u,
        SneakAttackMasteryOther2 = 5868u,
        SneakAttackMasteryOther3 = 5869u,
        SneakAttackMasteryOther4 = 5870u,
        SneakAttackMasteryOther5 = 5871u,
        SneakAttackMasteryOther6 = 5872u,
        SneakAttackMasteryOther7 = 5873u,
        SneakAttackMasteryOther8 = 5874u,
        SneakAttackMasterySelf1 = 5875u,
        SneakAttackMasterySelf2 = 5876u,
        SneakAttackMasterySelf3 = 5877u,
        SneakAttackMasterySelf4 = 5878u,
        SneakAttackMasterySelf5 = 5879u,
        SneakAttackMasterySelf6 = 5880u,
        SneakAttackMasterySelf7 = 5881u,
        SneakAttackMasterySelf8 = 5882u,
        CantripDirtyFightingProwess1 = 5883u,
        CantripDualWieldAptitude1 = 5884u,
        CantripRecklessnessProwess1 = 5885u,
        CantripShieldAptitude1 = 5886u,
        CantripSneakAttackProwess1 = 5887u,
        CantripDirtyFightingProwess2 = 5888u,
        CantripDualWieldAptitude2 = 5889u,
        CantripRecklessnessProwess2 = 5890u,
        CantripShieldAptitude2 = 5891u,
        CantripSneakAttackProwess2 = 5892u,
        CantripDirtyFightingProwess3 = 5893u,
        CantripDualWieldAptitude3 = 5894u,
        CantripRecklessnessProwess3 = 5895u,
        CantripShieldAptitude3 = 5896u,
        CantripSneakAttackProwess3 = 5897u,
        ModerateDirtyFightingProwess = 5898u,
        ModerateDualWieldAptitude = 5899u,
        ModerateRecklessnessProwess = 5900u,
        ModerateShieldAptitude = 5901u,
        ModerateSneakAttackProwess = 5902u,
        DualWieldMasteryRare = 5903u,
        DualWieldMasterySpectral = 5904u,
        RecklessnessMasteryRare = 5905u,
        RecklessnessMasterySpectral = 5906u,
        ShieldMasteryRare = 5907u,
        ShieldMasterySpectral = 5908u,
        SneakAttackMasteryRare = 5909u,
        SneakAttackMasterySpectral = 5910u,
        DirtyFightingMasteryRare = 5911u,
        DirtyFightingMasterySpectral = 5912u,
        CloakDirtyFightingMastery1 = 5913u,
        CloakDirtyFightingMastery2 = 5914u,
        CloakDirtyFightingMastery3 = 5915u,
        CloakDirtyFightingMastery4 = 5916u,
        CloakDirtyFightingMastery5 = 5917u,
        CloakDualWieldMastery1 = 5918u,
        CloakDualWieldMastery2 = 5919u,
        CloakDualWieldMastery3 = 5920u,
        CloakDualWieldMastery4 = 5921u,
        CloakDualWieldMastery5 = 5922u,
        CloakRecklessnessMastery1 = 5923u,
        CloakRecklessnessMastery2 = 5924u,
        CloakRecklessnessMastery3 = 5925u,
        CloakRecklessnessMastery4 = 5926u,
        CloakRecklessnessMastery5 = 5927u,
        CloakShieldMastery1 = 5928u,
        CloakShieldMastery2 = 5929u,
        CloakShieldMastery3 = 5930u,
        CloakShieldMastery4 = 5931u,
        CloakShieldMastery5 = 5932u,
        CloakSneakAttackMastery1 = 5933u,
        CloakSneakAttackMastery2 = 5934u,
        CloakSneakAttackMastery3 = 5935u,
        CloakSneakAttackMastery4 = 5936u,
        CloakSneakAttackMastery5 = 5937u,
        DF_Specialized_AttackDebuff = 5938u,
        DF_Specialized_Bleed = 5939u,
        DF_Specialized_DefenseDebuff = 5940u,
        DF_Specialized_HealingDebuff = 5941u,
        DF_Trained_AttackDebuff = 5942u,
        DF_Trained_Bleed = 5943u,
        DF_Trained_DefenseDebuff = 5944u,
        DF_Trained_HealingDebuff = 5945u,
        SetDirtyFightingAptitude1 = 5946u,
        SetDirtyFightingAptitude2 = 5947u,
        SetDirtyFightingAptitude3 = 5948u,
        SetDirtyFightingAptitude4 = 5949u,
        SetDualWieldAptitude1 = 5950u,
        SetDualWieldAptitude2 = 5951u,
        SetDualWieldAptitude3 = 5952u,
        SetDualWieldAptitude4 = 5953u,
        SetRecklessnessAptitude1 = 5954u,
        SetRecklessnessAptitude2 = 5955u,
        SetRecklessnessAptitude3 = 5956u,
        SetRecklessnessAptitude4 = 5957u,
        SetShieldAptitude1 = 5958u,
        SetShieldAptitude2 = 5959u,
        SetShieldAptitude3 = 5960u,
        SetShieldAptitude4 = 5961u,
        SetSneakAttackAptitude1 = 5962u,
        SetSneakAttackAptitude2 = 5963u,
        SetSneakAttackAptitude3 = 5964u,
        SetSneakAttackAptitude4 = 5965u,
        GeraineDefeatedHealth = 5966u,
        LightningArcBlue = 5967u,
        LightningBlastBlue = 5968u,
        LightningBoltBlue = 5969u,
        LightningStreakBlue = 5970u,
        LightningVolleyBlue = 5971u,
        LightningBombBlue = 5972u,
        PetPenguinMoufProtection = 5973u,
        RareArmorDamageBoost1 = 5974u,
        RareArmorDamageBoost2 = 5975u,
        RareArmorDamageBoost3 = 5976u,
        RareArmorDamageBoost4 = 5977u,
        RareArmorDamageBoost5 = 5978u,
        CorruptedTouch_Blight = 5979u,
        CorruptedTouch_DoT = 5980u,
        CorruptedTouch_HealDebuff = 5981u,
        HermeticLinkOther1 = 5982u,
        HermeticLinkOther2 = 5983u,
        HermeticLinkOther3 = 5984u,
        HermeticLinkOther4 = 5985u,
        HermeticLinkOther5 = 5986u,
        HermeticLinkOther6 = 5987u,
        HermeticLinkOther7 = 5988u,
        HermeticLinkOther8 = 5989u,
        BloodDrinkerOther1 = 5990u,
        BloodDrinkerOther2 = 5991u,
        BloodDrinkerOther3 = 5992u,
        BloodDrinkerOther4 = 5993u,
        BloodDrinkerOther5 = 5994u,
        BloodDrinkerOther6 = 5995u,
        BloodDrinkerOther7 = 5996u,
        BloodDrinkerOther8 = 5997u,
        BloodDrinkerOther8cast = 5998u,
        DefenderOther1 = 5999u,
        DefenderOther2 = 6000u,
        DefenderOther3 = 6001u,
        DefenderOther4 = 6002u,
        DefenderOther5 = 6003u,
        DefenderOther6 = 6004u,
        DefenderOther7 = 6005u,
        DefenderOther8 = 6006u,
        HeartSeekerOther1 = 6007u,
        HeartSeekerOther2 = 6008u,
        HeartSeekerOther3 = 6009u,
        HeartSeekerOther4 = 6010u,
        HeartSeekerOther5 = 6011u,
        HeartSeekerOther6 = 6012u,
        HeartSeekerOther7 = 6013u,
        HeartSeekerOther8 = 6014u,
        SpiritDrinkerOther1 = 6015u,
        SpiritDrinkerOther2 = 6016u,
        SpiritDrinkerOther3 = 6017u,
        SpiritDrinkerOther4 = 6018u,
        SpiritDrinkerOther5 = 6019u,
        SpiritDrinkerOther6 = 6020u,
        SpiritDrinkerOther7 = 6021u,
        SpiritDrinkerOther8 = 6022u,
        SpiritDrinkerOther8cast = 6023u,
        SwiftKillerOther1 = 6024u,
        SwiftKillerOther2 = 6025u,
        SwiftKillerOther3 = 6026u,
        SwiftKillerOther4 = 6027u,
        SwiftKillerOther5 = 6028u,
        SwiftKillerOther6 = 6029u,
        SwiftKillerOther7 = 6030u,
        SwiftKillerOther8 = 6031u,
        PortalSendingNanjouStockade = 6032u,
        PortalSendingProvingGrounds5Uber = 6033u,
        PortalSendingProvingGrounds2Uber = 6034u,
        CantripSpiritofIzexi = 6035u,
        NoEscape = 6036u,
        FleetingWill = 6037u,
        CiderStamina = 6038u,
        CantripWeaponExpertise4 = 6039u,
        CantripAlchemicalProwess4 = 6040u,
        CantripArcaneProwess4 = 6041u,
        CantripArmorExpertise4 = 6042u,
        CantripLightWeaponsAptitude4 = 6043u,
        CantripMissileWeaponsAptitude4 = 6044u,
        CantripCookingProwess4 = 6045u,
        CantripCreatureEnchantmentAptitude4 = 6046u,
        CantripFinesseWeaponsAptitude4 = 6047u,
        CantripDeceptionProwess4 = 6048u,
        CantripDirtyFightingProwess4 = 6049u,
        CantripDualWieldAptitude4 = 6050u,
        CantripFealty4 = 6051u,
        CantripFletchingProwess4 = 6052u,
        CantripHealingProwess4 = 6053u,
        CantripImpregnability4 = 6054u,
        CantripInvulnerability4 = 6055u,
        CantripItemEnchantmentAptitude4 = 6056u,
        CantripItemExpertise4 = 6057u,
        CantripJumpingProwess4 = 6058u,
        CantripLeadership4 = 6059u,
        CantripLifeMagicAptitude4 = 6060u,
        CantripLockpickProwess4 = 6061u,
        CantripMagicItemExpertise4 = 6062u,
        CantripMagicResistance4 = 6063u,
        CantripManaConversionProwess4 = 6064u,
        CantripMonsterAttunement4 = 6065u,
        CantripPersonAttunement4 = 6066u,
        CantripRecklessnessProwess4 = 6067u,
        CantripSalvaging4 = 6068u,
        CantripShieldAptitude4 = 6069u,
        CantripSneakAttackProwess4 = 6070u,
        CantripSprint4 = 6071u,
        CantripHeavyWeaponsAptitude4 = 6072u,
        CantripTwoHandedAptitude4 = 6073u,
        CantripVoidMagicAptitude4 = 6074u,
        CantripWarMagicAptitude4 = 6075u,
        CantripStaminaGain4 = 6076u,
        CantripHealthGain4 = 6077u,
        CantripManaGain4 = 6078u,
        CantripStormWard4 = 6079u,
        CantripAcidWard4 = 6080u,
        CantripBludgeoningWard4 = 6081u,
        CantripFlameWard4 = 6082u,
        CantripFrostWard4 = 6083u,
        CantripPiercingWard4 = 6084u,
        CantripSlashingWard4 = 6085u,
        CantripHermeticLink3 = 6086u,
        CantripHermeticLink4 = 6087u,
        CantripAcidBane4 = 6088u,
        CantripBloodThirst4 = 6089u,
        CantripBludgeoningBane4 = 6090u,
        CantripDefender4 = 6091u,
        CantripFlameBane4 = 6092u,
        CantripFrostBane4 = 6093u,
        CantripHeartThirst4 = 6094u,
        CantripImpenetrability4 = 6095u,
        CantripPiercingBane4 = 6096u,
        CantripSlashingBane4 = 6097u,
        CantripSpiritThirst4 = 6098u,
        CantripStormBane4 = 6099u,
        CantripSwiftHunter4 = 6100u,
        CantripWillpower4 = 6101u,
        CantripArmor4 = 6102u,
        CantripCoordination4 = 6103u,
        CantripEndurance4 = 6104u,
        CantripFocus4 = 6105u,
        CantripQuickness4 = 6106u,
        CantripStrength4 = 6107u,
        SummoningMasteryOther1 = 6108u,
        SummoningMasteryOther2 = 6109u,
        SummoningMasteryOther3 = 6110u,
        SummoningMasteryOther4 = 6111u,
        SummoningMasteryOther5 = 6112u,
        SummoningMasteryOther6 = 6113u,
        SummoningMasteryOther7 = 6114u,
        SummoningMasteryOther8 = 6115u,
        SummoningMasterySelf1 = 6116u,
        SummoningMasterySelf2 = 6117u,
        SummoningMasterySelf3 = 6118u,
        SummoningMasterySelf4 = 6119u,
        SummoningMasterySelf5 = 6120u,
        SummoningMasterySelf6 = 6121u,
        SummoningMasterySelf7 = 6122u,
        SummoningMasterySelf8 = 6123u,
        CantripSummoningProwess3 = 6124u,
        CantripSummoningProwess4 = 6125u,
        CantripSummoningProwess2 = 6126u,
        CantripSummoningProwess1 = 6127u,
        ModerateSummoningProwess = 6128u,
        SummoningIneptitudeOther1 = 6129u,
        SummoningIneptitudeOther2 = 6130u,
        SummoningIneptitudeOther3 = 6131u,
        SummoningIneptitudeOther4 = 6132u,
        SummoningIneptitudeOther5 = 6133u,
        SummoningIneptitudeOther6 = 6134u,
        SummoningIneptitudeOther7 = 6135u,
        SummoningIneptitudeOther8 = 6136u,
        CloakSummoningMastery2 = 6137u,
        CloakSummoningMastery3 = 6138u,
        CloakSummoningMastery4 = 6139u,
        CloakSummoningMastery5 = 6140u,
        CloakSummoningMastery1 = 6141u,
        SetSummoningAptitude1 = 6142u,
        SetSummoningAptitude2 = 6143u,
        SetSummoningAptitude3 = 6144u,
        SetSummoningAptitude4 = 6145u,
        RideTheLightning = 6146u,
        PortalSendingIceValley = 6147u,
        PortalSendingVisitors4 = 6148u,
        PortalSendingVisitorsVision = 6149u,
        RynthidRecall = 6150u,
        LightningRingRed = 6151u,
        LugianBomb = 6152u,
        TryptophanComa = 6153u,
        EnteringTheBasement = 6154u,
        EarthenStomp = 6155u,
        ViridianRing = 6156u,
        WitheringRing = 6157u,
        PoisonBreath = 6158u,
        ThornVolley = 6159u,
        ThornsAlt = 6160u,
        AcidicThorns = 6161u,
        ThornArc = 6162u,
        RingOfThorns = 6163u,
        DeadlyRingOfThorns = 6164u,
        DeadlyThornVolley = 6165u,
        PoisonedWounds = 6166u,
        PoisonedVitality = 6167u,
        DeadlyRingOfLightning = 6168u,
        DeadlyLightningVolley = 6169u,
        HoneyedLifeMead = 6170u,
        HoneyedManaMead = 6171u,
        HoneyedVigorMead = 6172u,
        RagingHeart = 6173u,
        TwistingWounds = 6174u,
        IncreasingPain = 6175u,
        Genius = 6176u,
        GauntletItemMastery = 6177u,
        GauntletWeaponMastery = 6178u,
        GauntletMagicItemMastery = 6179u,
        GauntletArmorMastery = 6180u,
        SingeingFlames = 6181u,
        OverExerted = 6182u,
        ReturnToTheStronghold1 = 6183u,
        ReturnToTheStronghold2 = 6184u,
        ReturnToTheStronghold3 = 6185u,
        DeafeningWail = 6186u,
        ScreechingHowl = 6187u,
        Earthquake = 6188u,
        SearingDiscII = 6189u,
        HorizonsBladesII = 6190u,
        CassiusRingOfFireII = 6191u,
        NuhmudirasSpinesII = 6192u,
        HaloOfFrostII = 6193u,
        EyeOfTheStormII = 6194u,
        CloudedSoulII = 6195u,
        TectonicRiftsII = 6196u,
        EyeOfTheStormIIAlt = 6197u,
        IncantationOfLightningBolt = 6198u,
        IncantationOfLightningArc = 6199u,
        ParagonsDualWieldMasteryV = 6200u,
        ParagonsFinesseWeaponMasteryI = 6201u,
        ParagonsFinesseWeaponMasteryII = 6202u,
        ParagonsFinesseWeaponMasteryIII = 6203u,
        ParagonsFinesseWeaponMasteryIV = 6204u,
        ParagonsFinesseWeaponMasteryV = 6205u,
        ParagonsHeavyWeaponMasteryI = 6206u,
        ParagonsHeavyWeaponMasteryII = 6207u,
        ParagonsHeavyWeaponMasteryIII = 6208u,
        ParagonsHeavyWeaponMasteryIV = 6209u,
        ParagonsHeavyWeaponMasteryV = 6210u,
        ParagonsLifeMagicMasteryI = 6211u,
        ParagonsLifeMagicMasteryII = 6212u,
        ParagonsLifeMagicMasteryIII = 6213u,
        ParagonsLifeMagicMasteryIV = 6214u,
        ParagonsLifeMagicMasteryV = 6215u,
        ParagonsLightWeaponMasteryI = 6216u,
        ParagonsLightWeaponMasteryII = 6217u,
        ParagonsLightWeaponMasteryIII = 6218u,
        ParagonsLightWeaponMasteryIV = 6219u,
        ParagonsLightWeaponMasteryV = 6220u,
        ParagonsMissileWeaponMasteryI = 6221u,
        ParagonsMissileWeaponMasteryII = 6222u,
        ParagonsMissileWeaponMasteryIII = 6223u,
        ParagonsMissileWeaponMasteryIV = 6224u,
        ParagonsMissileWeaponMasteryV = 6225u,
        ParagonsRecklessnessMasteryI = 6226u,
        ParagonsRecklessnessMasteryII = 6227u,
        ParagonsRecklessnessMasteryIII = 6228u,
        ParagonsRecklessnessMasteryIV = 6229u,
        ParagonsRecklessnessMasteryV = 6230u,
        ParagonsSneakAttackMasteryI = 6231u,
        ParagonsSneakAttackMasteryII = 6232u,
        ParagonsSneakAttackMasteryIII = 6233u,
        ParagonsSneakAttackMasteryIV = 6234u,
        ParagonsSneakAttackMasteryV = 6235u,
        ParagonsTwoHandedCombatMasteryI = 6236u,
        ParagonsTwoHandedCombatMasteryII = 6237u,
        ParagonsTwoHandedCombatMasteryIII = 6238u,
        ParagonsTwoHandedCombatMasteryIV = 6239u,
        ParagonsTwoHandedCombatMasteryV = 6240u,
        ParagonsVoidMagicMasteryI = 6241u,
        ParagonsVoidMagicMasteryII = 6242u,
        ParagonsVoidMagicMasteryIII = 6243u,
        ParagonsVoidMagicMasteryIV = 6244u,
        ParagonsVoidMagicMasteryV = 6245u,
        ParagonsWarMagicMasteryI = 6246u,
        ParagonsWarMagicMasteryII = 6247u,
        ParagonsWarMagicMasteryIII = 6248u,
        ParagonsWarMagicMasteryIV = 6249u,
        ParagonsWarMagicMasteryV = 6250u,
        ParagonsDirtyFightingMasteryI = 6251u,
        ParagonsDirtyFightingMasteryII = 6252u,
        ParagonsDirtyFightingMasteryIII = 6253u,
        ParagonsDirtyFightingMasteryIV = 6254u,
        ParagonsDirtyFightingMasteryV = 6255u,
        ParagonsDualWieldMasteryI = 6256u,
        ParagonsDualWieldMasteryII = 6257u,
        ParagonsDualWieldMasteryIII = 6258u,
        ParagonsDualWieldMasteryIV = 6259u,
        ParagonsWillpowerV = 6260u,
        ParagonsCoordinationI = 6261u,
        ParagonsCoordinationII = 6262u,
        ParagonsCoordinationIII = 6263u,
        ParagonsCoordinationIV = 6264u,
        ParagonsCoordinationV = 6265u,
        ParagonsEnduranceI = 6266u,
        ParagonsEnduranceII = 6267u,
        ParagonsEnduranceIII = 6268u,
        ParagonsEnduranceIV = 6269u,
        ParagonsEnduranceV = 6270u,
        ParagonsFocusI = 6271u,
        ParagonsFocusII = 6272u,
        ParagonsFocusIII = 6273u,
        ParagonsFocusIV = 6274u,
        ParagonsFocusV = 6275u,
        ParagonQuicknessI = 6276u,
        ParagonQuicknessII = 6277u,
        ParagonQuicknessIII = 6278u,
        ParagonQuicknessIV = 6279u,
        ParagonQuicknessV = 6280u,
        ParagonsStrengthI = 6281u,
        ParagonsStrengthII = 6282u,
        ParagonsStrengthIII = 6283u,
        ParagonsStrengthIV = 6284u,
        ParagonsStrengthV = 6285u,
        ParagonsWillpowerI = 6286u,
        ParagonsWillpowerII = 6287u,
        ParagonsWillpowerIII = 6288u,
        ParagonsWillpowerIV = 6289u,
        ParagonsStaminaV = 6290u,
        ParagonsCriticalBoostI = 6291u,
        ParagonsCriticalDamageBoostII = 6292u,
        ParagonsCriticalDamageBoostIII = 6293u,
        ParagonsCriticalDamageBoostIV = 6294u,
        ParagonsCriticalDamageBoostV = 6295u,
        ParagonsCriticalDamageReductionI = 6296u,
        ParagonsCriticalDamageReductionII = 6297u,
        ParagonsCriticalDamageReductionIII = 6298u,
        ParagonsCriticalDamageReductionIV = 6299u,
        ParagonsCriticalDamageReductionV = 6300u,
        ParagonsDamageBoostI = 6301u,
        ParagonsDamageBoostII = 6302u,
        ParagonsDamageBoostIII = 6303u,
        ParagonsDamageBoostIV = 6304u,
        ParagonsDamageBoostV = 6305u,
        ParagonsDamageReductionI = 6306u,
        ParagonsDamageReductionII = 6307u,
        ParagonsDamageReductionIII = 6308u,
        ParagonsDamageReductionIV = 6309u,
        ParagonsDamageReductionV = 6310u,
        ParagonsManaI = 6311u,
        ParagonsManaII = 6312u,
        ParagonsManaIII = 6313u,
        ParagonsManaIV = 6314u,
        ParagonsManaV = 6315u,
        ParagonsStaminaI = 6316u,
        ParagonsStaminaII = 6317u,
        ParagonsStaminaIII = 6318u,
        ParagonsStaminaIV = 6319u,
        RingOfSkullsII = 6320u,
        ViridianRiseRecall = 6321u,
        ViridianRiseGreatTreeRecall = 6322u,
        GauntletImperilSelf = 6323u,
        GauntletVulnerabilitySelf = 6324u,
        CelestialHandStrongholdRecall = 6325u,
        EldrytchWebStrongholdRecall = 6326u,
        RadiantBloodStrongholdRecall = 6327u,
        GauntletCriticalDamageBoostI = 6328u,
        GauntletCriticalDamageBoostII = 6329u,
        GauntletDamageBoostI = 6330u,
        GauntletDamageBoostII = 6331u,
        GauntletDamageReductionI = 6332u,
        GauntletDamageReductionII = 6333u,
        GauntletCriticalDamageReductionI = 6334u,
        GauntletCriticalDamageReductionII = 6335u,
        GauntletHealingBoostI = 6336u,
        GauntletHealingBoostII = 6337u,
        GauntletVitalityI = 6338u,
        GauntletVitalityII = 6339u,
        GauntletVitalityIII = 6340u,
        NumSpells = 0x2000u,
        JesterDecksCooldown = 32769u,
        AsheronsBenedictionCooldown = 32770u,
        BlackmoorsFavorCooldown = 32771u,
        HealthElixirCooldown = 32772u,
        ManaElixirCooldown = 32773u,
        CallOfLeadershipCooldown = 32774u,
        AnswerOfLoyaltyStaminaCooldown = 32775u,
        AnswerOfLoyaltyManaCooldown = 32776u,
        VirindiEssenceCooldown = 32777u,
        SurgingStrengthCooldown = 32778u,
        ToweringDefenseCooldown = 32779u,
        MhoireCastleItemsCooldown = 32818u,
        MiscTenSecondCooldown = 32819u,
        ContractCooldown = 32868u,
        SummonCreatureCooldown = 32981u,
        FacilityHubPortalGemCooldown = 32989u,
        ScryingRodCooldown = 33268u,
        PortalGemCooldown = 33768u
    }

    /// <summary>
    /// Spell category
    /// </summary>
    public enum SpellCategory : uint {
        Undef = 0,
        StrengthRaising = 1,
        StrengthLowering = 2,
        EnduranceRaising = 3,
        EnduranceLowering = 4,
        QuicknessRaising = 5,
        QuicknessLowering = 6,
        CoordinationRaising = 7,
        CoordinationLowering = 8,
        FocusRaising = 9,
        FocusLowering = 10,
        SelfRaising = 11,
        SelfLowering = 12,
        FocusConcentration = 13,
        FocusDisruption = 14,
        FocusBrilliance = 15,
        FocusDullness = 16,
        AxeRaising = 17,
        AxeLowering = 18,
        BowRaising = 19,
        BowLowering = 20,
        CrossbowRaising = 21,
        CrossbowLowering = 22,
        DaggerRaising = 23,
        DaggerLowering = 24,
        MaceRaising = 25,
        MaceLowering = 26,
        SpearRaising = 27,
        SpearLowering = 28,
        StaffRaising = 29,
        StaffLowering = 30,
        SwordRaising = 31,
        SwordLowering = 32,
        ThrownWeaponsRaising = 33,
        ThrownWeaponsLowering = 34,
        UnarmedCombatRaising = 35,
        UnarmedCombatLowering = 36,
        MeleeDefenseRaising = 37,
        MeleeDefenseLowering = 38,
        MissileDefenseRaising = 39,
        MissileDefenseLowering = 40,
        MagicDefenseRaising = 41,
        MagicDefenseLowering = 42,
        CreatureEnchantmentRaising = 43,
        CreatureEnchantmentLowering = 44,
        ItemEnchantmentRaising = 45,
        ItemEnchantmentLowering = 46,
        LifeMagicRaising = 47,
        LifeMagicLowering = 48,
        WarMagicRaising = 49,
        WarMagicLowering = 50,
        ManaConversionRaising = 51,
        ManaConversionLowering = 52,
        ArcaneLoreRaising = 53,
        ArcaneLoreLowering = 54,
        AppraiseArmorRaising = 55,
        AppraiseArmorLowering = 56,
        AppraiseItemRaising = 57,
        AppraiseItemLowering = 58,
        AppraiseMagicItemRaising = 59,
        AppraiseMagicItemLowering = 60,
        AppraiseWeaponRaising = 61,
        AppraiseWeaponLowering = 62,
        AssessMonsterRaising = 63,
        AssessMonsterLowering = 64,
        DeceptionRaising = 65,
        DeceptionLowering = 66,
        HealingRaising = 67,
        HealingLowering = 68,
        JumpRaising = 69,
        JumpLowering = 70,
        LeadershipRaising = 71,
        LeadershipLowering = 72,
        LockpickRaising = 73,
        LockpickLowering = 74,
        LoyaltyRaising = 75,
        LoyaltyLowering = 76,
        RunRaising = 77,
        RunLowering = 78,
        HealthRaising = 79,
        HealthLowering = 80,
        StaminaRaising = 81,
        StaminaLowering = 82,
        ManaRaising = 83,
        ManaLowering = 84,
        ManaRemedy = 85,
        ManaMalediction = 86,
        HealthTransfertocaster = 87,
        HealthTransferfromcaster = 88,
        StaminaTransfertocaster = 89,
        StaminaTransferfromcaster = 90,
        ManaTransfertocaster = 91,
        ManaTransferfromcaster = 92,
        HealthAccelerating = 93,
        HealthDecelerating = 94,
        StaminaAccelerating = 95,
        StaminaDecelerating = 96,
        ManaAccelerating = 97,
        ManaDecelerating = 98,
        VitaeRaising = 99,
        VitaeLowering = 100,
        AcidProtection = 101,
        AcidVulnerability = 102,
        BludgeonProtection = 103,
        BludgeonVulnerability = 104,
        ColdProtection = 105,
        ColdVulnerability = 106,
        ElectricProtection = 107,
        ElectricVulnerability = 108,
        FireProtection = 109,
        FireVulnerability = 110,
        PierceProtection = 111,
        PierceVulnerability = 112,
        SlashProtection = 113,
        SlashVulnerability = 114,
        ArmorRaising = 115,
        ArmorLowering = 116,
        AcidMissile = 117,
        BludgeoningMissile = 118,
        ColdMissile = 119,
        ElectricMissile = 120,
        FireMissile = 121,
        PiercingMissile = 122,
        SlashingMissile = 123,
        AcidSeeker = 124,
        BludgeoningSeeker = 125,
        ColdSeeker = 126,
        ElectricSeeker = 127,
        FireSeeker = 128,
        PiercingSeeker = 129,
        SlashingSeeker = 130,
        AcidBurst = 131,
        BludgeoningBurst = 132,
        ColdBurst = 133,
        ElectricBurst = 134,
        FireBurst = 135,
        PiercingBurst = 136,
        SlashingBurst = 137,
        AcidBlast = 138,
        BludgeoningBlast = 139,
        ColdBlast = 140,
        ElectricBlast = 141,
        FireBlast = 142,
        PiercingBlast = 143,
        SlashingBlast = 144,
        AcidScatter = 145,
        BludgeoningScatter = 146,
        ColdScatter = 147,
        ElectricScatter = 148,
        FireScatter = 149,
        PiercingScatter = 150,
        SlashingScatter = 151,
        AttackModRaising = 152,
        AttackModLowering = 153,
        DamageRaising = 154,
        DamageLowering = 155,
        DefenseModRaising = 156,
        DefenseModLowering = 157,
        WeaponTimeRaising = 158,
        WeaponTimeLowering = 159,
        ArmorValueRaising = 160,
        ArmorValueLowering = 161,
        AcidResistanceRaising = 162,
        AcidResistanceLowering = 163,
        BludgeonResistanceRaising = 164,
        BludgeonResistanceLowering = 165,
        ColdResistanceRaising = 166,
        ColdResistanceLowering = 167,
        ElectricResistanceRaising = 168,
        ElectricResistanceLowering = 169,
        FireResistanceRaising = 170,
        FireResistanceLowering = 171,
        PierceResistanceRaising = 172,
        PierceResistanceLowering = 173,
        SlashResistanceRaising = 174,
        SlashResistanceLowering = 175,
        BludgeoningResistanceRaising = 176,
        BludgeoningResistanceLowering = 177,
        SlashingResistanceRaising = 178,
        SlashingResistanceLowering = 179,
        PiercingResistanceRaising = 180,
        PiercingResistanceLowering = 181,
        ElectricalResistanceRaising = 182,
        ElectricalResistanceLowering = 183,
        FrostResistanceRaising = 184,
        FrostResistanceLowering = 185,
        FlameResistanceRaising = 186,
        FlameResistanceLowering = 187,
        AcidicResistanceRaising = 188,
        AcidicResistanceLowering = 189,
        ArmorLevelRaising = 190,
        ArmorLevelLowering = 191,
        LockpickResistanceRaising = 192,
        LockpickResistanceLowering = 193,
        ManaConversionModLowering = 194,
        ManaConversionModRaising = 195,
        VisionRaising = 196,
        VisionLowering = 197,
        TransparencyRaising = 198,
        TransparencyLowering = 199,
        PortalTie = 200,
        PortalRecall = 201,
        PortalCreation = 202,
        PortalItemCreation = 203,
        Vitae = 204,
        AssessPersonRaising = 205,
        AssessPersonLowering = 206,
        AcidVolley = 207,
        BludgeoningVolley = 208,
        FrostVolley = 209,
        LightningVolley = 210,
        FlameVolley = 211,
        ForceVolley = 212,
        BladeVolley = 213,
        PortalSending = 214,
        LifestoneSending = 215,
        CookingRaising = 216,
        CookingLowering = 217,
        FletchingRaising = 218,
        FletchingLowering = 219,
        AlchemyLowering = 220,
        AlchemyRaising = 221,
        AcidRing = 222,
        BludgeoningRing = 223,
        ColdRing = 224,
        ElectricRing = 225,
        FireRing = 226,
        PiercingRing = 227,
        SlashingRing = 228,
        AcidWall = 229,
        BludgeoningWall = 230,
        ColdWall = 231,
        ElectricWall = 232,
        FireWall = 233,
        PiercingWall = 234,
        SlashingWall = 235,
        AcidStrike = 236,
        BludgeoningStrike = 237,
        ColdStrike = 238,
        ElectricStrike = 239,
        FireStrike = 240,
        PiercingStrike = 241,
        SlashingStrike = 242,
        AcidStreak = 243,
        BludgeoningStreak = 244,
        ColdStreak = 245,
        ElectricStreak = 246,
        FireStreak = 247,
        PiercingStreak = 248,
        SlashingStreak = 249,
        Dispel = 250,
        CreatureMysticRaising = 251,
        CreatureMysticLowering = 252,
        ItemMysticRaising = 253,
        ItemMysticLowering = 254,
        WarMysticRaising = 255,
        WarMysticLowering = 256,
        HealthRestoring = 257,
        HealthDepleting = 258,
        ManaRestoring = 259,
        ManaDepleting = 260,
        StrengthIncrease = 261,
        StrengthDecrease = 262,
        EnduranceIncrease = 263,
        EnduranceDecrease = 264,
        QuicknessIncrease = 265,
        QuicknessDecrease = 266,
        CoordinationIncrease = 267,
        CoordinationDecrease = 268,
        FocusIncrease = 269,
        FocusDecrease = 270,
        SelfIncrease = 271,
        SelfDecrease = 272,
        GreatVitalityRaising = 273,
        PoorVitalityLowering = 274,
        GreatVigorRaising = 275,
        PoorVigorLowering = 276,
        GreaterIntellectRaising = 277,
        LessorIntellectLowering = 278,
        LifeGiverRaising = 279,
        LifeTakerLowering = 280,
        StaminaGiverRaising = 281,
        StaminaTakerLowering = 282,
        ManaGiverRaising = 283,
        ManaTakerLowering = 284,
        AcidWardProtection = 285,
        AcidWardVulnerability = 286,
        FireWardProtection = 287,
        FireWardVulnerability = 288,
        ColdWardProtection = 289,
        ColdWardVulnerability = 290,
        ElectricWardProtection = 291,
        ElectricWardVulnerability = 292,
        LeadershipObedienceRaising = 293,
        LeadershipObedienceLowering = 294,
        MeleeDefenseShelterRaising = 295,
        MeleeDefenseShelterLowering = 296,
        MissileDefenseShelterRaising = 297,
        MissileDefenseShelterLowering = 298,
        MagicDefenseShelterRaising = 299,
        MagicDefenseShelterLowering = 300,
        HuntersAcumenRaising = 301,
        HuntersAcumenLowering = 302,
        StillWaterRaising = 303,
        StillWaterLowering = 304,
        StrengthofEarthRaising = 305,
        StrengthofEarthLowering = 306,
        TorrentRaising = 307,
        TorrentLowering = 308,
        GrowthRaising = 309,
        GrowthLowering = 310,
        CascadeAxeRaising = 311,
        CascadeAxeLowering = 312,
        CascadeDaggerRaising = 313,
        CascadeDaggerLowering = 314,
        CascadeMaceRaising = 315,
        CascadeMaceLowering = 316,
        CascadeSpearRaising = 317,
        CascadeSpearLowering = 318,
        CascadeStaffRaising = 319,
        CascadeStaffLowering = 320,
        StoneCliffsRaising = 321,
        StoneCliffsLowering = 322,
        MaxDamageRaising = 323,
        MaxDamageLowering = 324,
        BowDamageRaising = 325,
        BowDamageLowering = 326,
        BowRangeRaising = 327,
        BowRangeLowering = 328,
        ExtraDefenseModRaising = 329,
        ExtraDefenseModLowering = 330,
        ExtraBowSkillRaising = 331,
        ExtraBowSkillLowering = 332,
        ExtraAlchemySkillRaising = 333,
        ExtraAlchemySkillLowering = 334,
        ExtraArcaneLoreSkillRaising = 335,
        ExtraArcaneLoreSkillLowering = 336,
        ExtraAppraiseArmorSkillRaising = 337,
        ExtraAppraiseArmorSkillLowering = 338,
        ExtraCookingSkillRaising = 339,
        ExtraCookingSkillLowering = 340,
        ExtraCrossbowSkillRaising = 341,
        ExtraCrossbowSkillLowering = 342,
        ExtraDeceptionSkillRaising = 343,
        ExtraDeceptionSkillLowering = 344,
        ExtraLoyaltySkillRaising = 345,
        ExtraLoyaltySkillLowering = 346,
        ExtraFletchingSkillRaising = 347,
        ExtraFletchingSkillLowering = 348,
        ExtraHealingSkillRaising = 349,
        ExtraHealingSkillLowering = 350,
        ExtraMeleeDefenseSkillRaising = 351,
        ExtraMeleeDefenseSkillLowering = 352,
        ExtraAppraiseItemSkillRaising = 353,
        ExtraAppraiseItemSkillLowering = 354,
        ExtraJumpingSkillRaising = 355,
        ExtraJumpingSkillLowering = 356,
        ExtraLifeMagicSkillRaising = 357,
        ExtraLifeMagicSkillLowering = 358,
        ExtraLockpickSkillRaising = 359,
        ExtraLockpickSkillLowering = 360,
        ExtraAppraiseMagicItemSkillRaising = 361,
        ExtraAppraiseMagicItemSkillLowering = 362,
        ExtraManaConversionSkillRaising = 363,
        ExtraManaConversionSkillLowering = 364,
        ExtraAssessCreatureSkillRaising = 365,
        ExtraAssessCreatureSkillLowering = 366,
        ExtraAssessPersonSkillRaising = 367,
        ExtraAssessPersonSkillLowering = 368,
        ExtraRunSkillRaising = 369,
        ExtraRunSkillLowering = 370,
        ExtraSwordSkillRaising = 371,
        ExtraSwordSkillLowering = 372,
        ExtraThrownWeaponsSkillRaising = 373,
        ExtraThrownWeaponsSkillLowering = 374,
        ExtraUnarmedCombatSkillRaising = 375,
        ExtraUnarmedCombatSkillLowering = 376,
        ExtraAppraiseWeaponSkillRaising = 377,
        ExtraAppraiseWeaponSkillLowering = 378,
        ArmorIncrease = 379,
        ArmorDecrease = 380,
        ExtraAcidResistanceRaising = 381,
        ExtraAcidResistanceLowering = 382,
        ExtraBludgeonResistanceRaising = 383,
        ExtraBludgeonResistanceLowering = 384,
        ExtraFireResistanceRaising = 385,
        ExtraFireResistanceLowering = 386,
        ExtraColdResistanceRaising = 387,
        ExtraColdResistanceLowering = 388,
        ExtraAttackModRaising = 389,
        ExtraAttackModLowering = 390,
        ExtraArmorValueRaising = 391,
        ExtraArmorValueLowering = 392,
        ExtraPierceResistanceRaising = 393,
        ExtraPierceResistanceLowering = 394,
        ExtraSlashResistanceRaising = 395,
        ExtraSlashResistanceLowering = 396,
        ExtraElectricResistanceRaising = 397,
        ExtraElectricResistanceLowering = 398,
        ExtraWeaponTimeRaising = 399,
        ExtraWeaponTimeLowering = 400,
        BludgeonWardProtection = 401,
        BludgeonWardVulnerability = 402,
        SlashWardProtection = 403,
        SlashWardVulnerability = 404,
        PierceWardProtection = 405,
        PierceWardVulnerability = 406,
        StaminaRestoring = 407,
        StaminaDepleting = 408,
        Fireworks = 409,
        HealthDivide = 410,
        StaminaDivide = 411,
        ManaDivide = 412,
        CoordinationIncrease2 = 413,
        StrengthIncrease2 = 414,
        FocusIncrease2 = 415,
        EnduranceIncrease2 = 416,
        SelfIncrease2 = 417,
        MeleeDefenseMultiply = 418,
        MissileDefenseMultiply = 419,
        MagicDefenseMultiply = 420,
        AttributesDecrease = 421,
        LifeGiverRaising2 = 422,
        ItemEnchantmentRaising2 = 423,
        SkillsDecrease = 424,
        ExtraManaConversionBonus = 425,
        WarMysticRaising2 = 426,
        WarMysticLowering2 = 427,
        MagicDefenseShelterRaising2 = 428,
        ExtraLifeMagicSkillRaising2 = 429,
        CreatureMysticRaising2 = 430,
        ItemMysticRaising2 = 431,
        ManaRaising2 = 432,
        SelfRaising2 = 433,
        CreatureEnchantmentRaising2 = 434,
        SalvagingRaising = 435,
        ExtraSalvagingRaising = 436,
        ExtraSalvagingRaising2 = 437,
        CascadeAxeRaising2 = 438,
        ExtraBowSkillRaising2 = 439,
        ExtraThrownWeaponsSkillRaising2 = 440,
        ExtraCrossbowSkillRaising2 = 441,
        CascadeDaggerRaising2 = 442,
        CascadeMaceRaising2 = 443,
        ExtraUnarmedCombatSkillRaising2 = 444,
        CascadeSpearRaising2 = 445,
        CascadeStaffRaising2 = 446,
        ExtraSwordSkillRaising2 = 447,
        AcidProtectionRare = 448,
        AcidResistanceRaisingRare = 449,
        AlchemyRaisingRare = 450,
        AppraisalResistanceLoweringRare = 451,
        AppraiseArmorRaisingRare = 452,
        AppraiseItemRaisingRare = 453,
        AppraiseMagicItemRaisingRare = 454,
        AppraiseWeaponRaisingRare = 455,
        ArcaneLoreRaisingRare = 456,
        ArmorRaisingRare = 457,
        ArmorValueRaisingRare = 458,
        AssessMonsterRaisingRare = 459,
        AssessPersonRaisingRare = 460,
        AttackModRaisingRare = 461,
        AxeRaisingRare = 462,
        BludgeonProtectionRare = 463,
        BludgeonResistanceRaisingRare = 464,
        BowRaisingRare = 465,
        ColdProtectionRare = 466,
        ColdResistanceRaisingRare = 467,
        CookingRaisingRare = 468,
        CoordinationRaisingRare = 469,
        CreatureEnchantmentRaisingRare = 470,
        CrossbowRaisingRare = 471,
        DaggerRaisingRare = 472,
        DamageRaisingRare = 473,
        DeceptionRaisingRare = 474,
        DefenseModRaisingRare = 475,
        ElectricProtectionRare = 476,
        ElectricResistanceRaisingRare = 477,
        EnduranceRaisingRare = 478,
        FireProtectionRare = 479,
        FireResistanceRaisingRare = 480,
        FletchingRaisingRare = 481,
        FocusRaisingRare = 482,
        HealingRaisingRare = 483,
        HealthAcceleratingRare = 484,
        ItemEnchantmentRaisingRare = 485,
        JumpRaisingRare = 486,
        LeadershipRaisingRare = 487,
        LifeMagicRaisingRare = 488,
        LockpickRaisingRare = 489,
        LoyaltyRaisingRare = 490,
        MaceRaisingRare = 491,
        MagicDefenseRaisingRare = 492,
        ManaAcceleratingRare = 493,
        ManaConversionRaisingRare = 494,
        MeleeDefenseRaisingRare = 495,
        MissileDefenseRaisingRare = 496,
        PierceProtectionRare = 497,
        PierceResistanceRaisingRare = 498,
        QuicknessRaisingRare = 499,
        RunRaisingRare = 500,
        SelfRaisingRare = 501,
        SlashProtectionRare = 502,
        SlashResistanceRaisingRare = 503,
        SpearRaisingRare = 504,
        StaffRaisingRare = 505,
        StaminaAcceleratingRare = 506,
        StrengthRaisingRare = 507,
        SwordRaisingRare = 508,
        ThrownWeaponsRaisingRare = 509,
        UnarmedCombatRaisingRare = 510,
        WarMagicRaisingRare = 511,
        WeaponTimeRaisingRare = 512,
        ArmorIncreaseInkyArmor = 513,
        MagicDefenseShelterRaisingFiun = 514,
        ExtraRunSkillRaisingFiun = 515,
        ExtraManaConversionSkillRaisingFiun = 516,
        AttributesIncreaseCantrip1 = 517,
        ExtraMeleeDefenseSkillRaising2 = 518,
        ACTDPurchaseRewardSpell = 519,
        ACTDPurchaseRewardSpellHealth = 520,
        SaltAshAttackModRaising = 521,
        QuicknessIncrease2 = 522,
        ExtraAlchemySkillRaising2 = 523,
        ExtraCookingSkillRaising2 = 524,
        ExtraFletchingSkillRaising2 = 525,
        ExtraLockpickSkillRaising2 = 526,
        MucorManaWell = 527,
        StaminaRestoring2 = 528,
        AllegianceRaising = 529,
        HealthDoT = 530,
        HealthDoTSecondary = 531,
        HealthDoTTertiary = 532,
        HealthHoT = 533,
        HealthHoTSecondary = 534,
        HealthHoTTertiary = 535,
        HealthDivideSecondary = 536,
        HealthDivideTertiary = 537,
        SetSwordRaising = 538,
        SetAxeRaising = 539,
        SetDaggerRaising = 540,
        SetMaceRaising = 541,
        SetSpearRaising = 542,
        SetStaffRaising = 543,
        SetUnarmedRaising = 544,
        SetBowRaising = 545,
        SetCrossbowRaising = 546,
        SetThrownRaising = 547,
        SetItemEnchantmentRaising = 548,
        SetCreatureEnchantmentRaising = 549,
        SetWarMagicRaising = 550,
        SetLifeMagicRaising = 551,
        SetMeleeDefenseRaising = 552,
        SetMissileDefenseRaising = 553,
        SetMagicDefenseRaising = 554,
        SetStaminaAccelerating = 555,
        SetCookingRaising = 556,
        SetFletchingRaising = 557,
        SetLockpickRaising = 558,
        SetAlchemyRaising = 559,
        SetSalvagingRaising = 560,
        SetArmorExpertiseRaising = 561,
        SetWeaponExpertiseRaising = 562,
        SetItemTinkeringRaising = 563,
        SetMagicItemExpertiseRaising = 564,
        SetLoyaltyRaising = 565,
        SetStrengthRaising = 566,
        SetEnduranceRaising = 567,
        SetCoordinationRaising = 568,
        SetQuicknessRaising = 569,
        SetFocusRaising = 570,
        SetWillpowerRaising = 571,
        SetHealthRaising = 572,
        SetStaminaRaising = 573,
        SetManaRaising = 574,
        SetSprintRaising = 575,
        SetJumpingRaising = 576,
        SetSlashResistanceRaising = 577,
        SetBludgeonResistanceRaising = 578,
        SetPierceResistanceRaising = 579,
        SetFlameResistanceRaising = 580,
        SetAcidResistanceRaising = 581,
        SetFrostResistanceRaising = 582,
        SetLightningResistanceRaising = 583,
        CraftingLockPickRaising = 584,
        CraftingFletchingRaising = 585,
        CraftingCookingRaising = 586,
        CraftingAlchemyRaising = 587,
        CraftingArmorTinkeringRaising = 588,
        CraftingWeaponTinkeringRaising = 589,
        CraftingMagicTinkeringRaising = 590,
        CraftingItemTinkeringRaising = 591,
        SkillPercentAlchemyRaising = 592,
        TwoHandedRaising = 593,
        TwoHandedLowering = 594,
        ExtraTwoHandedSkillRaising = 595,
        ExtraTwoHandedSkillLowering = 596,
        ExtraTwoHandedSkillRaising2 = 597,
        TwoHandedRaisingRare = 598,
        SetTwoHandedRaising = 599,
        GearCraftRaising = 600,
        GearCraftLowering = 601,
        ExtraGearCraftSkillRaising = 602,
        ExtraGearCraftSkillLowering = 603,
        ExtraGearCraftSkillRaising2 = 604,
        GearCraftRaisingRare = 605,
        SetGearCraftRaising = 606,
        LoyaltyManaRaising = 607,
        LoyaltyStaminaRaising = 608,
        LeadershipHealthRaising = 609,
        TrinketDamageRaising = 610,
        TrinketDamageLowering = 611,
        TrinketHealthRaising = 612,
        TrinketStaminaRaising = 613,
        TrinketManaRaising = 614,
        TrinketXPRaising = 615,
        DeceptionArcaneLoreRaising = 616,
        HealOverTimeRaising = 617,
        DamageOverTimeRaising = 618,
        HealingResistRatingRaising = 619,
        AetheriaDamageRatingRaising = 620,
        AetheriaDamageReductionRaising = 621,
        AetheriaHealthRaising = 623,
        AetheriaStaminaRaising = 624,
        AetheriaManaRaising = 625,
        AetheriaCriticalDamageRaising = 626,
        AetheriaHealingAmplificationRaising = 627,
        AetheriaProcDamageRatingRaising = 628,
        AetheriaProcDamageReductionRaising = 629,
        AetheriaProcHealthOverTimeRaising = 630,
        AetheriaProcDamageOverTimeRaising = 631,
        AetheriaProcHealingReductionRaising = 632,
        RareDamageRatingRaising = 633,
        RareDamageReductionRatingRaising = 634,
        AetheriaEnduranceRaising = 635,
        NetherDamageOverTimeRaising = 636,
        NetherDamageOverTimeRaising2 = 637,
        NetherDamageOverTimeRaising3 = 638,
        NetherStreak = 639,
        NetherMissile = 640,
        NetherRing = 641,
        NetherDamageRatingLowering = 642,
        NetherDamageHealingReductionRaising = 643,
        VoidMagicLowering = 644,
        VoidMagicRaising = 645,
        VoidMysticRaising = 646,
        SetVoidMagicRaising = 647,
        VoidMagicRaisingRare = 648,
        VoidMysticRaising2 = 649,
        LuminanceDamageRatingRaising = 650,
        LuminanceDamageReductionRaising = 651,
        LuminanceHealthRaising = 652,
        AetheriaCriticalReductionRaising = 653,
        ExtraMissileDefenseSkillRaising = 654,
        ExtraMissileDefenseSkillLowering = 655,
        ExtraMissileDefenseSkillRaising2 = 656,
        AetheriaHealthResistanceRaising = 657,
        AetheriaDotResistanceRaising = 658,
        CloakSkillRaising = 659,
        CloakAllSkillRaising = 660,
        CloakMagicDefenseLowering = 661,
        CloakMeleeDefenseLowering = 662,
        CloakMissileDefenseLowering = 663,
        DirtyFightingLowering = 664,
        DirtyFightingRaising = 665,
        ExtraDirtyFightingRaising = 666,
        DualWieldLowering = 667,
        DualWieldRaising = 668,
        ExtraDualWieldRaising = 669,
        RecklessnessLowering = 670,
        RecklessnessRaising = 671,
        ExtraRecklessnessRaising = 672,
        ShieldLowering = 673,
        ShieldRaising = 674,
        ExtraShieldRaising = 675,
        SneakAttackLowering = 676,
        SneakAttackRaising = 677,
        ExtraSneakAttackRaising = 678,
        RareDirtyFightingRaising = 679,
        RareDualWieldRaising = 680,
        RareRecklessnessRaising = 681,
        RareShieldRaising = 682,
        RareSneakAttackRaising = 683,
        DFAttackSkillDebuff = 684,
        DFBleedDamage = 685,
        DFDefenseSkillDebuff = 686,
        DFHealingDebuff = 687,
        SetDirtyFightingRaising = 688,
        SetDualWieldRaising = 689,
        SetRecklessnessRaising = 690,
        SetShieldRaising = 691,
        SetSneakAttackRaising = 692,
        LifeGiverMhoire = 693,
        RareDamageRatingRaising2 = 694,
        SpellDamageRaising = 695,
        SummoningRaising = 696,
        SummoningLowering = 697,
        ExtraSummoningSkillRaising = 698,
        SetSummoningRaising = 699,
        ParagonEnduranceRaising = 704,
        ParagonManaRaising = 705,
        ParagonStaminaRaising = 706,
        ParagonDirtyFightingRaising = 707,
        ParagonDualWieldRaising = 708,
        ParagonRecklessnessRaising = 709,
        ParagonSneakAttackRaising = 710,
        ParagonDamageRatingRaising = 711,
        ParagonDamageReductionRatingRaising = 712,
        ParagonCriticalDamageRatingRaising = 713,
        ParagonCriticalDamageReductionRatingRaising = 714,
        ParagonAxeRaising = 715,        // light weapons
        ParagonDaggerRaising = 716,     // finesse weapons
        ParagonSwordRaising = 717,      // heavy weapons
        ParagonWarMagicRaising = 718,
        ParagonLifeMagicRaising = 719,
        ParagonVoidMagicRaising = 720,
        ParagonBowRaising = 721,        // missile weapons
        ParagonStrengthRaising = 722,
        ParagonCoordinationRaising = 723,
        ParagonQuicknessRaising = 724,
        ParagonFocusRaising = 725,
        ParagonWillpowerRaising = 726,
        ParagonTwoHandedRaising = 727,
        GauntletDamageReductionRatingRaising = 728,
        GauntletDamageRatingRaising = 729,
        GauntletHealingRatingRaising = 730,
        GauntletVitalityRaising = 731,
        GauntletCriticalDamageRatingRaising = 732,
        GauntletCriticalDamageReductionRatingRaising = 733,
    }

    /// <summary>
    /// The AttributeMask selects which creature attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum AttributeMask : ushort {
        /// <summary>
        /// TODO
        /// </summary>
        Strength = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Endurance = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        Quickness = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        Coordination = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        Focus = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        Self = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        Health = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        Stamina = 0x0080,

        /// <summary>
        /// TODO
        /// </summary>
        Mana = 0x0100,

    }

    /// <summary>
    /// The DamageType identifies the type of damage.
    /// </summary>
    [Flags]
    public enum DamageType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Slashing = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Piercing = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Bludgeoning = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        Cold = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        Fire = 0x10,

        /// <summary>
        /// TODO
        /// </summary>
        Acid = 0x20,

        /// <summary>
        /// TODO
        /// </summary>
        Electric = 0x40,

    }

    /// <summary>
    /// The HookAppraisalFlags identifies various properties for an item hooked.
    /// </summary>
    [Flags]
    public enum HookAppraisalFlags : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Inscribable = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        IsHealer = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        IsLockpick = 0x08,

    }

    /// <summary>
    /// The ArmorHighlightMask selects which armor attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum ArmorHighlightMask : ushort {
        /// <summary>
        /// TODO
        /// </summary>
        ArmorLevel = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        SlashingProtection = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        PiercingProtection = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        BludgeoningProtection = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        ColdProtection = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        FireProtection = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        AcidProtection = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        ElectricalProtection = 0x0080,

    }

    /// <summary>
    /// The ResistHighlightMask selects which wand attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum ResistHighlightMask : ushort {
        /// <summary>
        /// TODO
        /// </summary>
        ResistSlash = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        ResistPierce = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        ResistBludgeon = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        ResistFire = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        ResistCold = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        ResistAcid = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        ResistElectric = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        ResistHealthBoost = 0x0080,

        /// <summary>
        /// TODO
        /// </summary>
        ResistStaminaDrain = 0x0100,

        /// <summary>
        /// TODO
        /// </summary>
        ResistStaminaBoost = 0x0200,

        /// <summary>
        /// TODO
        /// </summary>
        ResistManaDrain = 0x0400,

        /// <summary>
        /// TODO
        /// </summary>
        ResistManaBoost = 0x0800,

        /// <summary>
        /// TODO
        /// </summary>
        ManaConversionMod = 0x1000,

        /// <summary>
        /// TODO
        /// </summary>
        ElementalDamageMod = 0x2000,

        /// <summary>
        /// TODO
        /// </summary>
        ResistNether = 0x4000,

    }

    /// <summary>
    /// The WeaponHighlightMask selects which weapon attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum WeaponHighlightMask : ushort {
        /// <summary>
        /// TODO
        /// </summary>
        AttackSkill = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        MeleeDefense = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        Speed = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        Damage = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        DamageVariance = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        DamageMod = 0x0020,

    }

    /// <summary>
    /// Additional attack information
    /// </summary>
    [Flags]
    public enum AttackConditionsMask : uint {
        /// <summary>
        /// TODO
        /// </summary>
        CriticalProtectionAugmentation = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Recklessness = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        SneakAttack = 0x04,

    }

    /// <summary>
    /// The DamageLocation indicates where damage was done.
    /// </summary>
    public enum DamageLocation : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Head = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Chest = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Abdomen = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        UpperArm = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        LowerArm = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        Hand = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        UpperLeg = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        LowerLeg = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        Foot = 0x08,

    }


    public enum PlayScript : uint {
        Invalid = 0x00,
        Test1 = 0x01,
        Test2 = 0x02,
        Test3 = 0x03,
        Launch = 0x04,
        Explode = 0x05,
        AttribUpRed = 0x06,
        AttribDownRed = 0x07,
        AttribUpOrange = 0x08,
        AttribDownOrange = 0x09,
        AttribUpYellow = 0x0A,
        AttribDownYellow = 0x0B,
        AttribUpGreen = 0x0C,
        AttribDownGreen = 0x0D,
        AttribUpBlue = 0x0E,
        AttribDownBlue = 0x0F,
        AttribUpPurple = 0x10,
        AttribDownPurple = 0x11,
        SkillUpRed = 0x12,
        SkillDownRed = 0x13,
        SkillUpOrange = 0x14,
        SkillDownOrange = 0x15,
        SkillUpYellow = 0x16,
        SkillDownYellow = 0x17,
        SkillUpGreen = 0x18,
        SkillDownGreen = 0x19,
        SkillUpBlue = 0x1A,
        SkillDownBlue = 0x1B,
        SkillUpPurple = 0x1C,
        SkillDownPurple = 0x1D,
        SkillDownBlack = 0x1E,
        HealthUpRed = 0x1F,
        HealthDownRed = 0x20,
        HealthUpBlue = 0x21,
        HealthDownBlue = 0x22,
        HealthUpYellow = 0x23,
        HealthDownYellow = 0x24,
        RegenUpRed = 0x25,
        RegenDownREd = 0x26,
        RegenUpBlue = 0x27,
        RegenDownBlue = 0x28,
        RegenUpYellow = 0x29,
        RegenDownYellow = 0x2A,
        ShieldUpRed = 0x2B,
        ShieldDownRed = 0x2C,
        ShieldUpOrange = 0x2D,
        ShieldDownOrange = 0x2E,
        ShieldUpYellow = 0x2F,
        ShieldDownYellow = 0x30,
        ShieldUpGreen = 0x31,
        ShieldDownGreen = 0x32,
        ShieldUpBlue = 0x33,
        ShieldDownBlue = 0x34,
        ShieldUpPurple = 0x35,
        ShieldDownPurple = 0x36,
        ShieldUpGrey = 0x37,
        ShieldDownGrey = 0x38,
        EnchantUpRed = 0x39,
        EnchantDownRed = 0x3A,
        EnchantUpOrange = 0x3B,
        EnchantDownOrange = 0x3C,
        EnchantUpYellow = 0x3D,
        EnchantDownYellow = 0x3E,
        EnchantUpGreen = 0x3F,
        EnchantDownGreen = 0x40,
        EnchantUpBlue = 0x41,
        EnchantDownBlue = 0x42,
        EnchantUpPurple = 0x43,
        EnchantDownPurple = 0x44,
        VitaeUpWhite = 0x45,
        VitaeDownBlack = 0x46,
        VisionUpWhite = 0x47,
        VisionDownBlack = 0x48,
        SwapHealth_Red_To_Yellow = 0x49,
        SwapHealth_Red_To_Blue = 0x4A,
        SwapHealth_Yellow_To_Red = 0x4B,
        SwapHealth_Yellow_To_Blue = 0x4C,
        SwapHealth_Blue_To_Red = 0x4D,
        SwapHealth_Blue_To_Yellow = 0x4E,
        TransUpWhite = 0x4F,
        TransDownBlack = 0x50,
        Fizzle = 0x51,
        PortalEntry = 0x52,
        PortalExit = 0x53,
        BreatheFlame = 0x54,
        BreatheFrost = 0x55,
        BreatheAcid = 0x56,
        BreatheLightning = 0x57,
        Create = 0x58,
        Destroy = 0x59,
        ProjectileCollision = 0x5A,
        SplatterLowLeftBack = 0x5B,
        SplatterLowLeftFront = 0x5C,
        SplatterLowRightBack = 0x5D,
        SplatterLowRightFront = 0x5E,
        SplatterMidLeftBack = 0x5F,
        SplatterMidLeftFront = 0x60,
        SplatterMidRightBack = 0x61,
        SplatterMidRightFront = 0x62,
        SplatterUpLeftBack = 0x63,
        SplatterUpLeftFront = 0x64,
        SplatterUpRightBack = 0x65,
        SplatterUpRightFront = 0x66,
        SparkLowLeftBack = 0x67,
        SparkLowLeftFront = 0x68,
        SparkLowRightBack = 0x69,
        SparkLowRightFront = 0x6A,
        SparkMidLeftBack = 0x6B,
        SparkMidLeftFront = 0x6C,
        SparkMidRightBack = 0x6D,
        SparkMidRightFront = 0x6E,
        SparkUpLeftBack = 0x6F,
        SparkUpLeftFront = 0x70,
        SparkUpRightBack = 0x71,
        SparkUpRightFront = 0x72,
        PortalStorm = 0x73,
        Hide = 0x74,
        UnHide = 0x75,
        Hidden = 0x76,
        DisappearDestroy = 0x77,
        SpecialState1 = 0x78,
        SpecialState2 = 0x79,
        SpecialState3 = 0x7A,
        SpecialState4 = 0x7B,
        SpecialState5 = 0x7C,
        SpecialState6 = 0x7D,
        SpecialState7 = 0x7E,
        SpecialState8 = 0x7F,
        SpecialState9 = 0x80,
        SpecialState0 = 0x81,
        SpecialStateRed = 0x82,
        SpecialStateOrange = 0x83,
        SpecialStateYellow = 0x84,
        SpecialStateGreen = 0x85,
        SpecialStateBlue = 0x86,
        SpecialStatePurple = 0x87,
        SpecialStateWhite = 0x88,
        SpecialStateBlack = 0x89,
        LevelUp = 0x8A,
        EnchantUpGrey = 0x8B,
        EnchantDownGrey = 0x8C,
        WeddingBliss = 0x8D,
        EnchantUpWhite = 0x8E,
        EnchantDownWhite = 0x8F,
        CampingMastery = 0x90,
        CampingIneptitude = 0x91,
        DispelLife = 0x92,
        DispelCreature = 0x93,
        DispelAll = 0x94,
        BunnySmite = 0x95,
        BaelZharonSmite = 0x96,
        WeddingSteele = 0x97,
        RestrictionEffectBlue = 0x98,
        RestrictionEffectGreen = 0x99,
        RestrictionEffectGold = 0x9A,
        LayingofHands = 0x9B,
        AugmentationUseAttribute = 0x9C,
        AugmentationUseSkill = 0x9D,
        AugmentationUseResistances = 0x9E,
        AugmentationUseOther = 0x9F,
        BlackMadness = 0xA0,
        AetheriaLevelUp = 0xA1,
        AetheriaSurgeDestruction = 0xA2,
        AetheriaSurgeProtection = 0xA3,
        AetheriaSurgeRegeneration = 0xA4,
        AetheriaSurgeAffliction = 0xA5,
        AetheriaSurgeFestering = 0xA6,
        HealthDownVoid = 0xA7,
        RegenDownVoid = 0xA8,
        SkillDownVoid = 0xA9,
        DirtyFightingHealDebuff = 0xAA,
        DirtyFightingAttackDebuff = 0xAB,
        DirtyFightingDefenseDebuff = 0xAC,
        DirtyFightingDamageOverTime = 0xAD
    }

    /// <summary>
    /// Spell mask
    /// </summary>
    [Flags]
    public enum SpellFlags {
        Resistable = 0x1,
        PKSensitive = 0x2,
        Beneficial = 0x4,
        SelfTargeted = 0x8,
        Reversed = 0x10,
        NotIndoor = 0x20,
        NotOutdoor = 0x40,
        NotResearchable = 0x80,
        Projectile = 0x100,
        CreatureSpell = 0x200,
        ExcludedFromItemDescriptions = 0x400,
        IgnoresManaConversion = 0x800,
        NonTrackingProjectile = 0x1000,
        FellowshipSpell = 0x2000,
        FastCast = 0x4000,
        IndoorLongRange = 0x8000,
        DamageOverTime = 0x10000,
        UNKNOWN = 0x20000
    }

    [Flags]
    public enum EnchantmentFlags {
        Undef = 0x0000000,
        Attribute = 0x0000001,
        Attribute2nd = 0x0000002,
        Int = 0x0000004,
        Float = 0x0000008,
        Skill = 0x0000010,
        BodyDamageValue = 0x0000020,
        BodyDamageVariance = 0x0000040,
        BodyArmorValue = 0x0000080,
        SingleStat = 0x0001000,
        MultipleStat = 0x0002000,
        Multiplicative = 0x0004000,
        Additive = 0x0008000,
        AttackSkills = 0x0010000,
        DefenseSkills = 0x0020000,
        Multiplicative_Degrade = 0x0100000,
        Additive_Degrade = 0x0200000,
        Vitae = 0x0800000,
        Cooldown = 0x1000000,
        Beneficial = 0x2000000,
        StatTypes = 0x00000FF,
    }

    /// <summary>
    /// Spell type
    /// </summary>
    public enum SpellType {
        Undef,
        Enchantment,
        Projectile,
        Boost,
        Transfer,
        PortalLink,
        PortalRecall,
        PortalSummon,
        PortalSending,
        Dispel,
        LifeProjectile,
        FellowBoost,
        FellowEnchantment,
        FellowPortalSending,
        FellowDispel,
        EnchantmentProjectile
    }

    /// <summary>
    /// The LogTextType indicates the kind of text going to the chat area.
    /// </summary>
    public enum LogTextType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Default = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Speech = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Tell = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        SpeechDirectSend = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        System = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        Combat = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        Magic = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        Channel = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        ChannelSend = 0x09,

        /// <summary>
        /// TODO
        /// </summary>
        Social = 0x0A,

        /// <summary>
        /// TODO
        /// </summary>
        SocialSend = 0x0B,

        /// <summary>
        /// TODO
        /// </summary>
        Emote = 0x0C,

        /// <summary>
        /// TODO
        /// </summary>
        Advancement = 0x0D,

        /// <summary>
        /// TODO
        /// </summary>
        Abuse = 0x0E,

        /// <summary>
        /// TODO
        /// </summary>
        Help = 0x0F,

        /// <summary>
        /// TODO
        /// </summary>
        Appraisal = 0x10,

        /// <summary>
        /// TODO
        /// </summary>
        Spellcasting = 0x11,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance = 0x12,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship = 0x13,

        /// <summary>
        /// TODO
        /// </summary>
        WorldBroadcast = 0x14,

        /// <summary>
        /// TODO
        /// </summary>
        CombatEnemy = 0x15,

        /// <summary>
        /// TODO
        /// </summary>
        CombatSelf = 0x16,

        /// <summary>
        /// TODO
        /// </summary>
        Recall = 0x17,

        /// <summary>
        /// TODO
        /// </summary>
        Craft = 0x18,

        /// <summary>
        /// TODO
        /// </summary>
        Salvaging = 0x19,

        /// <summary>
        /// TODO
        /// </summary>
        AdminTell = 0x1F,

    }

    /// <summary>
    /// The EndTradeReason identifies the reason trading was ended.
    /// </summary>
    public enum EndTradeReason : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Normal = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        EnteredCombat = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Cancelled = 0x51,

    }

    /// <summary>
    /// The TradeSide identifies the side of the trade window.
    /// </summary>
    public enum TradeSide : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Self = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Partner = 0x02,

    }

    /// <summary>
    /// The HouseType identifies the type of house.
    /// </summary>
    public enum HouseType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Cottage = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Villa = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Mansion = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        Apartment = 0x04,

    }

    /// <summary>
    /// Identifies the chess move attempt result.  Negative/0 values are failures.
    /// </summary>
    public enum ChessMoveResult : int {
        /// <summary>
        /// Its not your turn, please wait for your opponents move.
        /// </summary>
        FailureNotYourTurn = -0x03,

        /// <summary>
        /// The selected piece cannot move that direction
        /// </summary>
        FailureInvalidDirection = -0x64,

        /// <summary>
        /// The selected piece cannot move that far
        /// </summary>
        FailureInvalidDistance = -0x65,

        /// <summary>
        /// You tried to move an empty square
        /// </summary>
        FailureMovingEmptySquare = -0x66,

        /// <summary>
        /// The selected piece is not yours
        /// </summary>
        FailureMovingOpponentPiece = -0x67,

        /// <summary>
        /// You cannot move off the board
        /// </summary>
        FailureMovedPieceOffBoard = -0x68,

        /// <summary>
        /// You cannot attack your own pieces
        /// </summary>
        FailureAttackingOwnPiece = -0x69,

        /// <summary>
        /// That move would put you in check
        /// </summary>
        FailureCannotMoveIntoCheck = -0x6A,

        /// <summary>
        /// You can only move through empty squares
        /// </summary>
        FailurePathBlocked = -0x6B,

        /// <summary>
        /// You cannot castle out of check
        /// </summary>
        FailureCastleOutOfCheck = -0x6C,

        /// <summary>
        /// You cannot castle through check
        /// </summary>
        FailureCastleThroughCheck = -0x6D,

        /// <summary>
        /// You cannot castle after moving the King or Rook
        /// </summary>
        FailureCastlePieceMoved = -0x6E,

        /// <summary>
        /// That move is invalid
        /// </summary>
        FailureInvalidMove = 0x0,

        /// <summary>
        /// Successful move.
        /// </summary>
        Success = 0x1,

        /// <summary>
        /// Your opponent is in Check.
        /// </summary>
        OpponentInCheck = 0x400,

        /// <summary>
        /// You have checkmated your opponent!
        /// </summary>
        CheckMatedOpponent = 0x800,

    }

    /// <summary>
    /// Type of fellow update
    /// </summary>
    public enum FellowUpdateType : uint {
        /// <summary>
        /// TODO
        /// </summary>
        FullUpdate = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        UpdateStats = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        UpdateVitals = 0x03,

    }

    /// <summary>
    /// Stage a contract is in.  Values 4+ appear to provide contract specific update messages
    /// </summary>
    public enum ContractStage : uint {
        /// <summary>
        /// TODO
        /// </summary>
        New = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        InProgress = 0x02,

        /// <summary>
        /// If this is set, it looks at the time when repeats to show either Done, Available, or # to Repeat
        /// </summary>
        DoneOrPendingRepeat = 0x03,

    }

    /// <summary>
    /// Gender of a player
    /// </summary>
    public enum Gender : byte {
        /// <summary>
        /// TODO
        /// </summary>
        Invalid = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Male = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Female = 0x02,

    }

    /// <summary>
    /// Heritage of a player
    /// </summary>
    public enum HeritageGroup : byte {
        /// <summary>
        /// TODO
        /// </summary>
        Invalid = 0x00,

        /// <summary>
        /// TODO
        /// </summary>
        Aluvian = 0x01,

        /// <summary>
        /// TODO
        /// </summary>
        Gharundim = 0x02,

        /// <summary>
        /// TODO
        /// </summary>
        Sho = 0x03,

        /// <summary>
        /// TODO
        /// </summary>
        Viamontian = 0x04,

        /// <summary>
        /// TODO
        /// </summary>
        Shadowbound = 0x05,

        /// <summary>
        /// TODO
        /// </summary>
        Gearknight = 0x06,

        /// <summary>
        /// TODO
        /// </summary>
        Tumerok = 0x07,

        /// <summary>
        /// TODO
        /// </summary>
        Lugian = 0x08,

        /// <summary>
        /// TODO
        /// </summary>
        Empyrean = 0x09,

        /// <summary>
        /// TODO
        /// </summary>
        Penumbraen = 0x0A,

        /// <summary>
        /// TODO
        /// </summary>
        Undead = 0x0B,

        /// <summary>
        /// TODO
        /// </summary>
        Olthoi = 0x0C,

        /// <summary>
        /// TODO
        /// </summary>
        OlthoiAcid = 0x0D,

    }

    /// <summary>
    /// the type of highlight (outline) applied to the object&#39;s icon
    /// </summary>
    public enum IconHighlight : uint {
        /// <summary>
        /// TODO
        /// </summary>
        Invalid = 0x0000,

        /// <summary>
        /// TODO
        /// </summary>
        Magical = 0x0001,

        /// <summary>
        /// TODO
        /// </summary>
        Poisoned = 0x0002,

        /// <summary>
        /// TODO
        /// </summary>
        BoostHealth = 0x0004,

        /// <summary>
        /// TODO
        /// </summary>
        BoostMana = 0x0008,

        /// <summary>
        /// TODO
        /// </summary>
        BoostStamina = 0x0010,

        /// <summary>
        /// TODO
        /// </summary>
        Fire = 0x0020,

        /// <summary>
        /// TODO
        /// </summary>
        Lightning = 0x0040,

        /// <summary>
        /// TODO
        /// </summary>
        Frost = 0x0080,

        /// <summary>
        /// TODO
        /// </summary>
        Acid = 0x0100,

        /// <summary>
        /// TODO
        /// </summary>
        Bludgeoning = 0x0200,

        /// <summary>
        /// TODO
        /// </summary>
        Slashing = 0x0400,

        /// <summary>
        /// TODO
        /// </summary>
        Piercing = 0x0800,

        /// <summary>
        /// TODO
        /// </summary>
        Nether = 0x1000,

    }

    /// <summary>
    /// the type of wieldable item this is
    /// </summary>
    public enum WieldType : byte {
        /// <summary>
        /// TODO
        /// </summary>
        Invalid = 0x00000000,

        /// <summary>
        /// TODO
        /// </summary>
        MeleeWeapon = 0x00000001,

        /// <summary>
        /// TODO
        /// </summary>
        Armor = 0x00000002,

        /// <summary>
        /// TODO
        /// </summary>
        Clothing = 0x00000004,

        /// <summary>
        /// TODO
        /// </summary>
        Jewelry = 0x00000008,

    }

    /// <summary>
    /// The ChatDisplayMask identifies that types of chat that are displayed in each chat window. 
    /// </summary>
    public enum ChatDisplayMask : uint {
        /// <summary>
        /// Gameplay (main chat window only)
        /// </summary>
        Gameplay = 0x03912021,

        /// <summary>
        /// Mandatory (main chat window only, cannot be disabled)
        /// </summary>
        Mandatory = 0x0000c302,

        /// <summary>
        /// TODO
        /// </summary>
        AreaChat = 0x00001004,

        /// <summary>
        /// TODO
        /// </summary>
        Tells = 0x00000018,

        /// <summary>
        /// TODO
        /// </summary>
        Combat = 0x00600040,

        /// <summary>
        /// TODO
        /// </summary>
        Magic = 0x00020080,

        /// <summary>
        /// TODO
        /// </summary>
        Allegiance = 0x00040c00,

        /// <summary>
        /// TODO
        /// </summary>
        Fellowship = 0x00080000,

        /// <summary>
        /// TODO
        /// </summary>
        Errors = 0x04000000,

        /// <summary>
        /// TODO
        /// </summary>
        TradeChannel = 0x10000000,

        /// <summary>
        /// TODO
        /// </summary>
        LFGChannel = 0x20000000,

        /// <summary>
        /// TODO
        /// </summary>
        RoleplayChannel = 0x40000000,

    }

    [Flags]
    public enum PhysicsDescriptionFlag {
        None = 0x000000,
        CSetup = 0x000001,
        MTable = 0x000002,
        Velocity = 0x000004,
        Acceleration = 0x000008,
        Omega = 0x000010,
        Parent = 0x000020,
        Children = 0x000040,
        ObjScale = 0x000080,
        Friction = 0x000100,
        Elasticity = 0x000200,
        Timestamps = 0x000400,
        STable = 0x000800,
        PeTable = 0x001000,
        DefaultScript = 0x002000,
        DefaultScriptIntensity = 0x004000,
        Position = 0x008000,
        Movement = 0x010000,
        AnimationFrame = 0x020000,
        Translucency = 0x040000
    }

    public enum RadarBehavior : byte {
        Undefined = 0,
        ShowNever = 1,
        ShowMovement = 2,
        ShowAttacking = 3,
        ShowAlways = 4
    }

    public enum RadarColor : byte {
        Default = 0x00,
        Blue = 0x01,
        Gold = 0x02,
        White = 0x03,
        Purple = 0x04,
        Red = 0x05,
        Pink = 0x06,
        Green = 0x07,
        Yellow = 0x08,
        Cyan = 0x09,
        BrightGreen = 0x10,
        Admin = Cyan,
        Advocate = Pink,
        Creature = Gold,
        LifeStone = Blue,
        NPC = Yellow,
        PlayerKiller = Red,
        Portal = Purple,
        Sentinel = Cyan,
        Vendor = Yellow,
        Fellowship = BrightGreen,
        FellowshipLeader = BrightGreen,
        PKLite = Pink
    }

    public enum SkillTrainingType {
        Unusable = 0,
        Untrained = 1,
        Trained = 2,
        Specialized = 3
    }

    [Flags]
    public enum WeenieHeaderFlag : uint {
        None = 0x00000000,
        PluralName = 0x00000001,
        ItemsCapacity = 0x00000002,
        ContainersCapacity = 0x00000004,
        Value = 0x00000008,
        Usable = 0x00000010, // Usability
        UseRadius = 0x00000020,
        Monarch = 0x00000040,
        UiEffects = 0x00000080,
        AmmoType = 0x00000100,
        CombatUse = 0x00000200,
        Uses = 0x00000400,
        MaxUses = 0x00000800,
        StackSize = 0x00001000,
        MaxStackSize = 0x00002000,
        Container = 0x00004000,
        Wielder = 0x00008000,
        ValidEquipLocations = 0x00010000,
        CurrentlyWieldedLocation = 0x00020000, // Location
        Coverage = 0x00040000,
        TargetType = 0x00080000,
        RadarBlipColor = 0x00100000,
        Burden = 0x00200000,
        Spell = 0x00400000,
        RadarBehavior = 0x00800000,
        Workmanship = 0x01000000,
        Owner = 0x02000000,
        HouseRestrictions = 0x04000000,
        PhysicsScript = 0x08000000,
        HookableOn = 0x10000000,
        HookType = 0x20000000,
        IconOverlay = 0x40000000,
        MaterialType = 0x80000000
    }

    [Flags]
    public enum WeenieHeaderFlag2 : uint {
        None = 0x00,
        IconUnderlay = 0x01,
        Cooldown = 0x02,
        CooldownDuration = 0x04,
        PetOwner = 0x08
    }
}
