using System;

namespace UtilityBelt.Common.Enums {
    [Flags]
    public enum AnimationFlags {
        PosFrames = 0x1
    }
}
