namespace UtilityBelt.Common.Enums {
    public enum AnimationHookDir {
        Unknown     = -2,
        Backward    = -1,
        Both        = 0,
        Forward     = 1,
    }
}
