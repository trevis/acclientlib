namespace UtilityBelt.Common.Enums {
    public enum BSPType {
        Drawing     = 0,
        Physics     = 1,
        Cell        = 2,
    }
}
