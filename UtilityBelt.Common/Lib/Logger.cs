﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Common.Lib {
    public static class Logger {
        public static Action<string> LogAction { get; set; } = null;
        internal static void Log(string v) {
            LogAction?.Invoke(v);
        }
    }
}
