using ACE.DatLoader.FileTypes;
using System.Collections.Generic;

namespace ACE.DatLoader
{
    public class LanguageDatDatabase : DatDatabase
    {
        public LanguageDatDatabase(string filename, bool keepOpen = false) : base(filename, keepOpen)
        {
            CharacterTitles = ReadFromDat<StringTable>(StringTable.CharacterTitle_FileID);
        }

        public LanguageDatDatabase(Dictionary<uint, FileType> cache) : base() {
            foreach (var kv in cache) {
                FileCache.Add(kv.Key, kv.Value);
            }
            CharacterTitles = ReadFromDat<StringTable>(StringTable.CharacterTitle_FileID);
        }

        public StringTable CharacterTitles { get; }
    }
}
