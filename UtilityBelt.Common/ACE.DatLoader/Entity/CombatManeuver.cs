using System.IO;
using UtilityBelt.Common.Enums;

namespace ACE.DatLoader.Entity
{
    public class CombatManeuver : IUnpackable
    {
        public MotionDatStance Style { get; private set; }
        public AttackHeight AttackHeight { get; private set; }
        public AttackType AttackType { get; private set; }
        public uint MinSkillLevel { get; private set; }
        public MotionDatCommand Motion { get; private set; }

        public void Unpack(BinaryReader reader)
        {
            Style           = (MotionDatStance)reader.ReadUInt32();
            AttackHeight    = (AttackHeight)reader.ReadUInt32();
            AttackType      = (AttackType)reader.ReadUInt32();
            MinSkillLevel   = reader.ReadUInt32();
            Motion          = (MotionDatCommand)reader.ReadUInt32();
        }
    }
}
