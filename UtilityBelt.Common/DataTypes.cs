﻿using System;
using System.Collections.Generic;
using System.IO;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Lib;

namespace UtilityBelt.Common.Messages.Types {
    public interface IACDataType {
        /// <summary>
        /// Populates this data type instance from a BinaryReader buffer
        /// </summary>
        /// <param name="buffer">BinaryReader instance to read from</param>
        void ReadFromBuffer(BinaryReader buffer);
    }

    public interface IACMessageType : IACDataType {

    }

    public interface IACOutgoingMessageType {
        void WriteToBuffer(BinaryWriter writer);
    }

    /// <summary>
    /// Full spell ID combining the spell id with the spell layer.
    /// </summary>
    public class LayeredSpellId : IACDataType {
        /// <summary>
        /// ID of the spell
        /// </summary>
        public ushort Id;

        /// <summary>
        /// Layer of the spell, seperating multiple instances of the same spell
        /// </summary>
        public ushort Layer;

        /// <summary>
        /// Full spell id including layer
        /// </summary>
        public uint FullId => ((uint)Layer << 16) + Id;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt16(buffer);
            Layer = BinaryHelpers.ReadUInt16(buffer);
        }

        public override bool Equals(object obj) {
            return (obj is LayeredSpellId layered && layered.FullId == FullId);
        }

        public override int GetHashCode() {
            return (FullId).GetHashCode();
        }
    }

    /// <summary>
    /// TODO
    /// </summary>
    public class Vector3 : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public float X;

        /// <summary>
        /// TODO
        /// </summary>
        public float Y;

        /// <summary>
        /// TODO
        /// </summary>
        public float Z;

        public Vector3() {
        }

        public Vector3(float x, float y, float z) {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            X = BinaryHelpers.ReadSingle(buffer);
            Y = BinaryHelpers.ReadSingle(buffer);
            Z = BinaryHelpers.ReadSingle(buffer);
        }

        public static float Dot(Vector3 a, Vector3 b) {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        public static Vector3 Identity => new Vector3(0, 0, 0);

        public static Vector3 Normalize(Vector3 v) {
            float mag = (float)Math.Sqrt(Dot(v, v));

            if (mag < 2.2204460492503130808472633361816E-16)
                return Identity;

            return new Vector3(v.X / mag, v.Y / mag, v.Z / mag);
        }

        public static Vector3 Cross(Vector3 v1, Vector3 v2) {
            return new Vector3(
                v1.Y * v2.Z - v1.Z * v2.Y,
                v1.Z * v2.X - v1.X * v2.Z,
                v1.X * v2.Y - v1.Y * v2.X
            );
        }

        public float Length() {
            return (float)Math.Sqrt(X * X + Y * Y + Z * Z);
        }

        public void Normalize() {
            var normal = Vector3.Normalize(this);
            X = normal.X;
            Y = normal.Y;
            Z = normal.Z;
        }

        public static Vector3 operator -(Vector3 a) => new Vector3(-a.X, -a.Y, -a.Z);
        public static Vector3 operator +(Vector3 a, Vector3 b) => new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        public static Vector3 operator -(Vector3 a, Vector3 b) => new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        public static Vector3 operator *(Vector3 a, Vector3 b) => new Vector3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
        public static Vector3 operator /(Vector3 a, Vector3 b) => new Vector3(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
        public static Vector3 operator +(Vector3 a, float b) => new Vector3(a.X + b, a.Y + b, a.Z + b);
        public static Vector3 operator -(Vector3 a, float b) => new Vector3(a.X - b, a.Y - b, a.Z - b);
        public static Vector3 operator *(Vector3 a, float b) => new Vector3(a.X * b, a.Y * b, a.Z * b);
        public static Vector3 operator /(Vector3 a, float b) => new Vector3(a.X / b, a.Y / b, a.Z / b);

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class Quaternion : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public float W;

        /// <summary>
        /// TODO
        /// </summary>
        public float X;

        /// <summary>
        /// TODO
        /// </summary>
        public float Y;

        /// <summary>
        /// TODO
        /// </summary>
        public float Z;

        public Quaternion(float rotationX, float rotationY, float rotationZ, float rotationW) {
            X = rotationX;
            Y = rotationY;
            Z = rotationZ;
            W = rotationW;
        }

        public Quaternion() {
        }

        public static Quaternion Identity => new Quaternion(0, 0, 0, 0);

        public static Quaternion Normalize(Quaternion q) {
            float mag = (float)Math.Sqrt(Dot(q, q));

            if (mag < 2.2204460492503130808472633361816E-16)
                return Identity;

            return new Quaternion(q.X / mag, q.Y / mag, q.Z / mag, q.W / mag);
        }

        public static float Dot(Quaternion a, Quaternion b) {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;
        }

        public static Quaternion operator *(Quaternion a, Quaternion b) => new Quaternion(a.X * b.X, a.Y * b.Y, a.Z * b.Z, a.W * b.W);

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            W = BinaryHelpers.ReadSingle(buffer);
            X = BinaryHelpers.ReadSingle(buffer);
            Y = BinaryHelpers.ReadSingle(buffer);
            Z = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Landcell location, without orientation
    /// </summary>
    public class Origin : IACDataType {
        /// <summary>
        /// the landcell in which the object is located
        /// </summary>
        public uint Landcell;

        /// <summary>
        /// the location in the landcell for the object
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Landcell = BinaryHelpers.ReadUInt32(buffer);
            Position = new Vector3();
            Position.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Landcell location, including orientation
    /// </summary>
    public class Position : IACDataType {
        /// <summary>
        /// the landcell in which the object is located
        /// </summary>
        public uint Landcell;

        /// <summary>
        /// the location and orientation in the landcell
        /// </summary>
        public Frame Frame;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Landcell = BinaryHelpers.ReadUInt32(buffer);
            Frame = new Frame();
            Frame.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// A the location and orientation of an object within a landcell
    /// </summary>
    public class Frame : IACDataType {
        /// <summary>
        /// the location in a landcell in which the object is located
        /// </summary>
        public Vector3 Origin;

        /// <summary>
        /// a quaternion describing the object&#39;s orientation
        /// </summary>
        public Quaternion Orientation;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Origin = new Vector3();
            Origin.ReadFromBuffer(buffer);
            Orientation = new Quaternion();
            Orientation.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Character properties.
    /// </summary>
    public class ACBaseQualities : IACDataType {
        /// <summary>
        /// determines which property types appear in the message
        /// </summary>
        public uint Flags;

        /// <summary>
        /// Expect it always should be 0xA
        /// </summary>
        public ObjectType WeenieType;

        /// <summary>
        /// A table of int value properties for this weenie
        /// </summary>
        public PackableHashTable<IntId, int> IntProperties;

        /// <summary>
        /// A table of int64 value properties for this weenie
        /// </summary>
        public PackableHashTable<Int64Id, long> Int64Properties;

        /// <summary>
        /// A table of bool value properties for this weenie
        /// </summary>
        public PackableHashTable<BoolId, bool> BoolProperties;

        /// <summary>
        /// A table of float value properties for this weenie
        /// </summary>
        public PackableHashTable<FloatId, double> FloatProperties;

        /// <summary>
        /// A table of string value properties for this weenie
        /// </summary>
        public PackableHashTable<StringId, string> StringProperties;

        /// <summary>
        /// A table of data value properties for this weenie
        /// </summary>
        public PackableHashTable<DataId, uint> DataProperties;

        /// <summary>
        /// A table of instance value properties for this weenie
        /// </summary>
        public PackableHashTable<InstanceId, uint> InstanceProperties;

        /// <summary>
        /// A table of position value properties for this weenie
        /// </summary>
        public PackableHashTable<PositionPropertyID, Position> PositionProperties;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            WeenieType = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x00000001) != 0) {
                IntProperties = new PackableHashTable<IntId, int>();
                IntProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000080) != 0) {
                Int64Properties = new PackableHashTable<Int64Id, long>();
                Int64Properties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                BoolProperties = new PackableHashTable<BoolId, bool>();
                BoolProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                FloatProperties = new PackableHashTable<FloatId, double>();
                FloatProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000010) != 0) {
                StringProperties = new PackableHashTable<StringId, string>();
                StringProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                DataProperties = new PackableHashTable<DataId, uint>();
                DataProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                InstanceProperties = new PackableHashTable<InstanceId, uint>();
                InstanceProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000020) != 0) {
                PositionProperties = new PackableHashTable<PositionPropertyID, Position>();
                PositionProperties.ReadFromBuffer(buffer);
            }
        }

    }

    /// <summary>
    /// The ACQualities structure contains character property lists.
    /// </summary>
    public class ACQualities : IACDataType {
        /// <summary>
        /// Contains basic data types (int, float bool, etc.)
        /// </summary>
        public ACBaseQualities BaseQualities;

        /// <summary>
        /// determines which property vector types appear in the message
        /// </summary>
        public uint Flags;

        /// <summary>
        /// seems to indicate this object has health attribute
        /// </summary>
        public bool HasHealth;

        /// <summary>
        /// The character attributes
        /// </summary>
        public AttributeCache AttributeCache;

        /// <summary>
        /// The character skills
        /// </summary>
        public PackableHashTable<SkillId, Skill> Skills;

        /// <summary>
        /// TODO
        /// </summary>
        public Body Body;

        /// <summary>
        /// Spells in the characters spellbook
        /// </summary>
        public SpellBook SpellBook;

        /// <summary>
        /// The enchantments active on the character
        /// </summary>
        public EnchantmentRegistry Enchantments;

        /// <summary>
        /// Some kind of event filter
        /// </summary>
        public EventFilter EventFilter;

        /// <summary>
        /// TODO
        /// </summary>
        public EmoteTable EmoteTable;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableList<CreationProfile> CreationProfile;

        /// <summary>
        /// TODO
        /// </summary>
        public PageDataList PageData;

        /// <summary>
        /// TODO
        /// </summary>
        public GeneratorTable Generators;

        /// <summary>
        /// TODO
        /// </summary>
        public GeneratorRegistry GeneratorRegistry;

        /// <summary>
        /// TODO
        /// </summary>
        public GeneratorQueue GeneratorQueue;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            BaseQualities = new ACBaseQualities();
            BaseQualities.ReadFromBuffer(buffer);
            Flags = BinaryHelpers.ReadUInt32(buffer);
            HasHealth = BinaryHelpers.ReadBool(buffer);
            if (((uint)Flags & 0x00000001) != 0) {
                AttributeCache = new AttributeCache();
                AttributeCache.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                Skills = new PackableHashTable<SkillId, Skill>();
                Skills.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                Body = new Body();
                Body.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000100) != 0) {
                SpellBook = new SpellBook();
                SpellBook.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000200) != 0) {
                Enchantments = new EnchantmentRegistry();
                Enchantments.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                EventFilter = new EventFilter();
                EventFilter.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000010) != 0) {
                EmoteTable = new EmoteTable();
                EmoteTable.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000020) != 0) {
                CreationProfile = new PackableList<CreationProfile>();
                CreationProfile.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                PageData = new PageDataList();
                PageData.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000080) != 0) {
                Generators = new GeneratorTable();
                Generators.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000400) != 0) {
                GeneratorRegistry = new GeneratorRegistry();
                GeneratorRegistry.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000800) != 0) {
                GeneratorQueue = new GeneratorQueue();
                GeneratorQueue.ReadFromBuffer(buffer);
            }
        }

    }

    /// <summary>
    /// The AttributeCache structure contains information about a character attributes.
    /// </summary>
    public class AttributeCache : IACDataType {
        /// <summary>
        /// The attributes included in the character description - this is always 0x1FF
        /// </summary>
        public uint Flags;

        /// <summary>
        /// strength attribute information
        /// </summary>
        public Attribute Strength;

        /// <summary>
        /// endurance attribute information
        /// </summary>
        public Attribute Endurance;

        /// <summary>
        /// quickness attribute information
        /// </summary>
        public Attribute Quickness;

        /// <summary>
        /// coordination attribute information
        /// </summary>
        public Attribute Coordination;

        /// <summary>
        /// focus attribute information
        /// </summary>
        public Attribute Focus;

        /// <summary>
        /// self attribute information
        /// </summary>
        public Attribute Self;

        /// <summary>
        /// health vital information
        /// </summary>
        public SecondaryAttribute Health;

        /// <summary>
        /// stamina vital information
        /// </summary>
        public SecondaryAttribute Stamina;

        /// <summary>
        /// mana vital information
        /// </summary>
        public SecondaryAttribute Mana;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x00000001) != 0) {
                Strength = new Attribute();
                Strength.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                Endurance = new Attribute();
                Endurance.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                Quickness = new Attribute();
                Quickness.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                Coordination = new Attribute();
                Coordination.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000010) != 0) {
                Focus = new Attribute();
                Focus.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000020) != 0) {
                Self = new Attribute();
                Self.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                Health = new SecondaryAttribute();
                Health.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000080) != 0) {
                Stamina = new SecondaryAttribute();
                Stamina.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000100) != 0) {
                Mana = new SecondaryAttribute();
                Mana.ReadFromBuffer(buffer);
            }
        }

    }

    /// <summary>
    /// The Attribute structure contains information about a character attribute.
    /// </summary>
    public class Attribute : IACDataType {
        /// <summary>
        /// points raised
        /// </summary>
        public uint PointsRaised;

        /// <summary>
        /// innate points
        /// </summary>
        public uint InitPoints;

        /// <summary>
        /// XP spent on this attribute
        /// </summary>
        public uint ExperienceSpent;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PointsRaised = BinaryHelpers.ReadUInt32(buffer);
            InitPoints = BinaryHelpers.ReadUInt32(buffer);
            ExperienceSpent = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// The SecondaryAttribute structure contains information about a character vital.
    /// </summary>
    public class SecondaryAttribute : IACDataType {
        /// <summary>
        /// secondary attribute&#39;s data
        /// </summary>
        public Attribute Attribute;

        /// <summary>
        /// current value of the vital
        /// </summary>
        public uint Level;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Attribute = new Attribute();
            Attribute.ReadFromBuffer(buffer);
            Level = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// The Skill structure contains information about a character skill.
    /// </summary>
    public class Skill : IACDataType {
        /// <summary>
        /// points raised
        /// </summary>
        public ushort PointsRaised;

        /// <summary>
        /// If this is not 0, it appears to trigger the initLevel to be treated as extra XP applied to the skill
        /// </summary>
        public ushort AdjustExperience;

        /// <summary>
        /// skill state
        /// </summary>
        public SkillState SkillState;

        /// <summary>
        /// XP spent on this skill
        /// </summary>
        public uint ExperienceSpent;

        /// <summary>
        /// starting point for advancement of the skill (eg bonus points)
        /// </summary>
        public uint InitPoints;

        /// <summary>
        /// last use difficulty
        /// </summary>
        public uint ResistanceOfLastCheck;

        /// <summary>
        /// time skill was last used
        /// </summary>
        public double LastUsedTime;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PointsRaised = BinaryHelpers.ReadUInt16(buffer);
            AdjustExperience = BinaryHelpers.ReadUInt16(buffer);
            SkillState = (SkillState)BinaryHelpers.ReadUInt32(buffer);
            ExperienceSpent = BinaryHelpers.ReadUInt32(buffer);
            InitPoints = BinaryHelpers.ReadUInt32(buffer);
            ResistanceOfLastCheck = BinaryHelpers.ReadUInt32(buffer);
            LastUsedTime = BinaryHelpers.ReadDouble(buffer);
        }

    }

    /// <summary>
    /// Contains body part table
    /// </summary>
    public class Body : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<uint, BodyPart> BodyParts;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            BodyParts = new PackableHashTable<uint, BodyPart>();
            BodyParts.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Information on individual body parts. (Needs to be confirmed if this was used in prod)
    /// </summary>
    public class BodyPart : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public int HasBpsd;

        /// <summary>
        /// TODO
        /// </summary>
        public int Type;

        /// <summary>
        /// TODO
        /// </summary>
        public int Val;

        /// <summary>
        /// TODO
        /// </summary>
        public int Var;

        /// <summary>
        /// Armor info
        /// </summary>
        public ArmorCache Cache;

        /// <summary>
        /// TODO
        /// </summary>
        public int Bh;

        /// <summary>
        /// TODO
        /// </summary>
        public BodyPartSelectionData BodyPartSelection;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            HasBpsd = BinaryHelpers.ReadInt32(buffer);
            Type = BinaryHelpers.ReadInt32(buffer);
            Val = BinaryHelpers.ReadInt32(buffer);
            Var = BinaryHelpers.ReadInt32(buffer);
            Cache = new ArmorCache();
            Cache.ReadFromBuffer(buffer);
            Bh = BinaryHelpers.ReadInt32(buffer);
            if (((uint)HasBpsd & 0x00000001) != 0) {
                BodyPartSelection = new BodyPartSelectionData();
                BodyPartSelection.ReadFromBuffer(buffer);
            }
        }

    }

    /// <summary>
    /// Information on armor levels
    /// </summary>
    public class ArmorCache : IACDataType {
        /// <summary>
        /// Base armor level
        /// </summary>
        public int BaseArmor;

        /// <summary>
        /// Armor level vs slash damage
        /// </summary>
        public int VsSlash;

        /// <summary>
        /// Armor level vs pierce damage
        /// </summary>
        public int VsPierce;

        /// <summary>
        /// Armor level vs bludgeon damage
        /// </summary>
        public int VsBludgeon;

        /// <summary>
        /// Armor level vs cold damage
        /// </summary>
        public int VsCold;

        /// <summary>
        /// Armor level vs fire damage
        /// </summary>
        public int VsFire;

        /// <summary>
        /// Armor level vs acid damage
        /// </summary>
        public int VsAcid;

        /// <summary>
        /// Armor level vs electric damage
        /// </summary>
        public int VsElectric;

        /// <summary>
        /// Armor level vs nether damage
        /// </summary>
        public int VsNether;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            BaseArmor = BinaryHelpers.ReadInt32(buffer);
            VsSlash = BinaryHelpers.ReadInt32(buffer);
            VsPierce = BinaryHelpers.ReadInt32(buffer);
            VsBludgeon = BinaryHelpers.ReadInt32(buffer);
            VsCold = BinaryHelpers.ReadInt32(buffer);
            VsFire = BinaryHelpers.ReadInt32(buffer);
            VsAcid = BinaryHelpers.ReadInt32(buffer);
            VsElectric = BinaryHelpers.ReadInt32(buffer);
            VsNether = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class BodyPartSelectionData : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public int HLF;

        /// <summary>
        /// TODO
        /// </summary>
        public int MLF;

        /// <summary>
        /// TODO
        /// </summary>
        public int LLF;

        /// <summary>
        /// TODO
        /// </summary>
        public int HRF;

        /// <summary>
        /// TODO
        /// </summary>
        public int MRF;

        /// <summary>
        /// TODO
        /// </summary>
        public int LRF;

        /// <summary>
        /// TODO
        /// </summary>
        public int HLB;

        /// <summary>
        /// TODO
        /// </summary>
        public int MLB;

        /// <summary>
        /// TODO
        /// </summary>
        public int LLB;

        /// <summary>
        /// TODO
        /// </summary>
        public int HRB;

        /// <summary>
        /// TODO
        /// </summary>
        public int MRB;

        /// <summary>
        /// TODO
        /// </summary>
        public int LRB;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            HLF = BinaryHelpers.ReadInt32(buffer);
            MLF = BinaryHelpers.ReadInt32(buffer);
            LLF = BinaryHelpers.ReadInt32(buffer);
            HRF = BinaryHelpers.ReadInt32(buffer);
            MRF = BinaryHelpers.ReadInt32(buffer);
            LRF = BinaryHelpers.ReadInt32(buffer);
            HLB = BinaryHelpers.ReadInt32(buffer);
            MLB = BinaryHelpers.ReadInt32(buffer);
            LLB = BinaryHelpers.ReadInt32(buffer);
            HRB = BinaryHelpers.ReadInt32(buffer);
            MRB = BinaryHelpers.ReadInt32(buffer);
            LRB = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Contains information related to your spellbook
    /// </summary>
    public class SpellBook : IACDataType {
        /// <summary>
        /// Spells in the characters spellbook
        /// </summary>
        public PackableHashTable<LayeredSpellId, SpellBookPage> Spells;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Spells = new PackableHashTable<LayeredSpellId, SpellBookPage>();
            Spells.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Contains information related to the spell in your spellbook
    /// </summary>
    public class SpellBookPage : IACDataType {
        /// <summary>
        /// Final value has 2.0 subtracted if network value &gt; 2.0.  Believe this is the charge of the spell which was unused later
        /// </summary>
        public float CastingLikelihood;

        /// <summary>
        /// Client skips this value
        /// </summary>
        public int Unknown;

        /// <summary>
        /// Replaces castingLikelihood
        /// </summary>
        public float CastingLikelihood2;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            CastingLikelihood = BinaryHelpers.ReadSingle(buffer);
            if (CastingLikelihood < 2.0) {
                Unknown = BinaryHelpers.ReadInt32(buffer);
                CastingLikelihood2 = BinaryHelpers.ReadSingle(buffer);
            }
        }

    }

    /// <summary>
    /// Contains information related to the spells in effect on the character
    /// </summary>
    public class EnchantmentRegistry : IACDataType {
        /// <summary>
        /// Enchantment mask.
        /// </summary>
        public uint Flags;

        /// <summary>
        /// Life spells active on the player
        /// </summary>
        public PackableList<Enchantment> LifeSpells;

        /// <summary>
        /// Creature spells active on the player
        /// </summary>
        public PackableList<Enchantment> CreatureSpells;

        /// <summary>
        /// Cooldown spells active on the player
        /// </summary>
        public PackableList<Enchantment> Cooldowns;

        /// <summary>
        /// Vitae Penalty.
        /// </summary>
        public Enchantment Vitae;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x0001) != 0) {
                LifeSpells = new PackableList<Enchantment>();
                LifeSpells.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x0002) != 0) {
                CreatureSpells = new PackableList<Enchantment>();
                CreatureSpells.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x0008) != 0) {
                Cooldowns = new PackableList<Enchantment>();
                Cooldowns.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x0004) != 0) {
                Vitae = new Enchantment();
                Vitae.ReadFromBuffer(buffer);
            }
        }

    }

    /// <summary>
    /// The Enchantment structure describes an active enchantment.
    /// </summary>
    public class Enchantment : IACDataType {
        /// <summary>
        /// the spell ID of the enchantment
        /// </summary>
        public LayeredSpellId Id;

        /// <summary>
        /// the family of related spells this enchantment belongs to
        /// </summary>
        public ushort Category;

        /// <summary>
        /// Value greater or equal to 1 means we read spellSetID
        /// </summary>
        public ushort HasSpellSetID;

        /// <summary>
        /// the difficulty of the spell
        /// </summary>
        public uint PowerLevel;

        /// <summary>
        /// the amount of time this enchantment has been active
        /// </summary>
        public double StartTime;

        /// <summary>
        /// the duration of the spell
        /// </summary>
        public double Duration;

        /// <summary>
        /// the object ID of the creature or item that cast this enchantment
        /// </summary>
        public uint Caster;

        /// <summary>
        /// unknown
        /// </summary>
        public float DegradeModifier;

        /// <summary>
        /// unknown
        /// </summary>
        public float DegradeLimit;

        /// <summary>
        /// the time when this enchantment was cast
        /// </summary>
        public double LastTimeDegraded;

        /// <summary>
        /// Stat modification information
        /// </summary>
        public StatMod StatMod;

        /// <summary>
        /// Related to armor sets somehow?
        /// </summary>
        public uint SpellSetID;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = new LayeredSpellId();
            Id.ReadFromBuffer(buffer);
            Category = BinaryHelpers.ReadUInt16(buffer);
            HasSpellSetID = BinaryHelpers.ReadUInt16(buffer);
            PowerLevel = BinaryHelpers.ReadUInt32(buffer);
            StartTime = BinaryHelpers.ReadDouble(buffer);
            Duration = BinaryHelpers.ReadDouble(buffer);
            Caster = BinaryHelpers.ReadUInt32(buffer);
            DegradeModifier = BinaryHelpers.ReadSingle(buffer);
            DegradeLimit = BinaryHelpers.ReadSingle(buffer);
            LastTimeDegraded = BinaryHelpers.ReadDouble(buffer);
            StatMod = new StatMod();
            StatMod.ReadFromBuffer(buffer);
            if (HasSpellSetID > 0) {
                SpellSetID = BinaryHelpers.ReadUInt32(buffer);
            }
        }

    }

    /// <summary>
    /// Information on stat modification
    /// </summary>
    public class StatMod : IACDataType {
        /// <summary>
        /// flags that indicate the type of effect the spell has
        /// </summary>
        public EnchantmentFlags Flags;

        /// <summary>
        /// along with flags, indicates which attribute is affected by the spell
        /// </summary>
        public uint Key;

        /// <summary>
        /// the effect value/amount
        /// </summary>
        public float Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = (EnchantmentFlags)BinaryHelpers.ReadUInt32(buffer);
            Key = BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Contains a list of events to filter? Unknown what this does currently.
    /// </summary>
    public class EventFilter : IACDataType {
        /// <summary>
        /// List of events
        /// </summary>
        public PackableList<uint> Filters;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Filters = new PackableList<uint>();
            Filters.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Contains a list of emotes for NPCs? Unknown what this does currently.
    /// </summary>
    public class EmoteTable : IACDataType {
        /// <summary>
        /// Key may be an EmoteCategory?
        /// </summary>
        public PackableHashTable<uint, EmoteSetList> Emotes;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Emotes = new PackableHashTable<uint, EmoteSetList>();
            Emotes.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class EmoteSetList : IACDataType {
        /// <summary>
        /// List of emote sets
        /// </summary>
        public PackableList<EmoteSet> EmoteSets;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            EmoteSets = new PackableList<EmoteSet>();
            EmoteSets.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class EmoteSet : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public EmoteCategory Category;

        /// <summary>
        /// TODO
        /// </summary>
        public float Probability;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ClassID;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Style;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Substyle;

        /// <summary>
        /// TODO
        /// </summary>
        public string Quest;

        /// <summary>
        /// TODO
        /// </summary>
        public uint VendorType;

        /// <summary>
        /// TODO
        /// </summary>
        public float MinHealth;

        /// <summary>
        /// TODO
        /// </summary>
        public float MaxHealth;

        /// <summary>
        /// List of emotes
        /// </summary>
        public PackableList<Emote> Emotes;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Category = (EmoteCategory)BinaryHelpers.ReadUInt32(buffer);
            Probability = BinaryHelpers.ReadSingle(buffer);
            switch ((int)Category) {
                case 0x01:
                case 0x06:
                    ClassID = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x05:
                    Style = BinaryHelpers.ReadUInt32(buffer);
                    Substyle = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x0C:
                case 0x0D:
                case 0x16:
                case 0x17:
                case 0x1B:
                case 0x1C:
                case 0x1D:
                case 0x1E:
                case 0x1F:
                case 0x20:
                case 0x21:
                case 0x22:
                case 0x23:
                case 0x24:
                case 0x25:
                case 0x26:
                    Quest = BinaryHelpers.ReadString(buffer);
                    break;
                case 0x02:
                    VendorType = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x0F:
                    MinHealth = BinaryHelpers.ReadSingle(buffer);
                    MaxHealth = BinaryHelpers.ReadSingle(buffer);
                    break;
            }
            Emotes = new PackableList<Emote>();
            Emotes.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class Emote : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public EmoteType Type;

        /// <summary>
        /// TODO
        /// </summary>
        public float Delay;

        /// <summary>
        /// TODO
        /// </summary>
        public float Extent;

        /// <summary>
        /// TODO
        /// </summary>
        public string Message;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Amount;

        /// <summary>
        /// TODO
        /// </summary>
        public uint State;

        /// <summary>
        /// TODO
        /// </summary>
        public double Percent;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Min;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Max;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong Amount64;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong HeroXP64;

        /// <summary>
        /// TODO
        /// </summary>
        public uint SpellID;

        /// <summary>
        /// TODO
        /// </summary>
        public CreationProfile CreationProfile;

        /// <summary>
        /// Over 8 is invalid
        /// </summary>
        public int WealthRating;

        /// <summary>
        /// TODO
        /// </summary>
        public int TreasureClass;

        /// <summary>
        /// Valid values are 0 to 3 
        /// </summary>
        public int TreasureType;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Motion;

        /// <summary>
        /// TODO
        /// </summary>
        public Frame Frame;

        /// <summary>
        /// TODO
        /// </summary>
        public uint PhysicsScript;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Sound;

        /// <summary>
        /// TODO
        /// </summary>
        public string TestString;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong Min64;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong Max64;

        /// <summary>
        /// TODO
        /// </summary>
        public double MinFloat;

        /// <summary>
        /// TODO
        /// </summary>
        public double MaxFloat;

        /// <summary>
        /// TODO
        /// </summary>
        public bool Display;

        /// <summary>
        /// TODO
        /// </summary>
        public Position Position;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (EmoteType)BinaryHelpers.ReadUInt32(buffer);
            Delay = BinaryHelpers.ReadSingle(buffer);
            Extent = BinaryHelpers.ReadSingle(buffer);
            switch ((int)Type) {
                case 0x01:
                case 0x08:
                case 0x0A:
                case 0x0D:
                case 0x10:
                case 0x11:
                case 0x12:
                case 0x14:
                case 0x15:
                case 0x16:
                case 0x17:
                case 0x18:
                case 0x19:
                case 0x1A:
                case 0x1F:
                case 0x33:
                case 0x3A:
                case 0x3C:
                case 0x3D:
                case 0x40:
                case 0x41:
                case 0x43:
                case 0x44:
                case 0x4F:
                case 0x50:
                case 0x51:
                case 0x53:
                case 0x58:
                case 0x79:
                    Message = BinaryHelpers.ReadString(buffer);
                    break;
                case 0x20:
                case 0x21:
                case 0x46:
                case 0x54:
                case 0x55:
                case 0x56:
                case 0x59:
                case 0x66:
                case 0x67:
                case 0x68:
                case 0x69:
                case 0x6A:
                case 0x6B:
                case 0x6C:
                case 0x6D:
                    Message = BinaryHelpers.ReadString(buffer);
                    Amount = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x35:
                case 0x36:
                case 0x37:
                case 0x45:
                    State = BinaryHelpers.ReadUInt32(buffer);
                    Amount = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x73:
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x76:
                    State = BinaryHelpers.ReadUInt32(buffer);
                    Percent = BinaryHelpers.ReadDouble(buffer);
                    break;
                case 0x1E:
                case 0x3B:
                case 0x47:
                case 0x52:
                    Message = BinaryHelpers.ReadString(buffer);
                    Min = BinaryHelpers.ReadUInt32(buffer);
                    Max = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x2:
                case 0x3E:
                    Amount64 = BinaryHelpers.ReadUInt64(buffer);
                    HeroXP64 = BinaryHelpers.ReadUInt64(buffer);
                    break;
                case 0x70:
                case 0x71:
                    Amount64 = BinaryHelpers.ReadUInt64(buffer);
                    break;
                case 0x22:
                case 0x2F:
                case 0x30:
                case 0x5A:
                case 0x77:
                case 0x78:
                    Amount = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0xE:
                case 0x13:
                case 0x1B:
                case 0x49:
                    SpellID = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x3:
                case 0x4A:
                    CreationProfile = new CreationProfile();
                    CreationProfile.ReadFromBuffer(buffer);
                    break;
                case 0x4C:
                    Message = BinaryHelpers.ReadString(buffer);
                    CreationProfile = new CreationProfile();
                    CreationProfile.ReadFromBuffer(buffer);
                    break;
                case 0x38:
                    WealthRating = BinaryHelpers.ReadInt32(buffer);
                    TreasureClass = BinaryHelpers.ReadInt32(buffer);
                    TreasureType = BinaryHelpers.ReadInt32(buffer);
                    break;
                case 0x5:
                case 0x34:
                    Motion = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x4:
                case 0x6:
                case 0xB:
                case 0x57:
                    Frame = new Frame();
                    Frame.ReadFromBuffer(buffer);
                    break;
                case 0x7:
                    PhysicsScript = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x9:
                    Sound = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x1C:
                case 0x1D:
                    Amount = BinaryHelpers.ReadUInt32(buffer);
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x6E:
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x6F:
                    Amount = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x23:
                case 0x2D:
                case 0x2E:
                    Message = BinaryHelpers.ReadString(buffer);
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x26:
                case 0x4B:
                    Message = BinaryHelpers.ReadString(buffer);
                    TestString = BinaryHelpers.ReadString(buffer);
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x24:
                case 0x27:
                case 0x28:
                case 0x29:
                case 0x2A:
                case 0x2B:
                case 0x2C:
                    Message = BinaryHelpers.ReadString(buffer);
                    Min = BinaryHelpers.ReadUInt32(buffer);
                    Max = BinaryHelpers.ReadUInt32(buffer);
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x72:
                    Message = BinaryHelpers.ReadString(buffer);
                    Min64 = BinaryHelpers.ReadUInt64(buffer);
                    Max64 = BinaryHelpers.ReadUInt64(buffer);
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x25:
                    Message = BinaryHelpers.ReadString(buffer);
                    MinFloat = BinaryHelpers.ReadDouble(buffer);
                    MaxFloat = BinaryHelpers.ReadDouble(buffer);
                    State = BinaryHelpers.ReadUInt32(buffer);
                    break;
                case 0x31:
                    Percent = BinaryHelpers.ReadDouble(buffer);
                    Min64 = BinaryHelpers.ReadUInt64(buffer);
                    Max64 = BinaryHelpers.ReadUInt64(buffer);
                    break;
                case 0x32:
                    State = BinaryHelpers.ReadUInt32(buffer);
                    Percent = BinaryHelpers.ReadDouble(buffer);
                    Min = BinaryHelpers.ReadUInt32(buffer);
                    Max = BinaryHelpers.ReadUInt32(buffer);
                    Display = BinaryHelpers.ReadBool(buffer);
                    break;
                case 0x3F:
                case 0x63:
                case 0x64:
                    Position = new Position();
                    Position.ReadFromBuffer(buffer);
                    break;
            }
        }

    }

    /// <summary>
    /// Set information about an item for creation
    /// </summary>
    public class CreationProfile : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint WeenieClassId;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Palette;

        /// <summary>
        /// TODO
        /// </summary>
        public float Shade;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Destination;

        /// <summary>
        /// TODO
        /// </summary>
        public int StackSize;

        /// <summary>
        /// TODO
        /// </summary>
        public bool TryToBond;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            WeenieClassId = BinaryHelpers.ReadUInt32(buffer);
            Palette = BinaryHelpers.ReadUInt32(buffer);
            Shade = BinaryHelpers.ReadSingle(buffer);
            Destination = BinaryHelpers.ReadUInt32(buffer);
            StackSize = BinaryHelpers.ReadInt32(buffer);
            TryToBond = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// List of pages in a book
    /// </summary>
    public class PageDataList : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint MaxNumPages;

        /// <summary>
        /// TODO
        /// </summary>
        public uint MaxNumCharsPerPage;

        /// <summary>
        /// List of pages
        /// </summary>
        public PackableList<PageData> Pages;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            MaxNumPages = BinaryHelpers.ReadUInt32(buffer);
            MaxNumCharsPerPage = BinaryHelpers.ReadUInt32(buffer);
            Pages = new PackableList<PageData>();
            Pages.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Data for an individual page
    /// </summary>
    public class PageData : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint AuthorID;

        /// <summary>
        /// TODO
        /// </summary>
        public string AuthorName;

        /// <summary>
        /// TODO
        /// </summary>
        public string AuthorAccount;

        /// <summary>
        /// if HIWORD is not 0xFFFF, this is textIncluded. For our purpose this should always be 0xFFFF0002
        /// </summary>
        public uint Version;

        /// <summary>
        /// TODO
        /// </summary>
        public bool TextIncluded;

        /// <summary>
        /// TODO
        /// </summary>
        public bool IgnoreAuthor;

        /// <summary>
        /// TODO
        /// </summary>
        public string PageText;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            AuthorID = BinaryHelpers.ReadUInt32(buffer);
            AuthorName = BinaryHelpers.ReadString(buffer);
            AuthorAccount = BinaryHelpers.ReadString(buffer);
            Version = BinaryHelpers.ReadUInt32(buffer);
            TextIncluded = BinaryHelpers.ReadBool(buffer);
            IgnoreAuthor = BinaryHelpers.ReadBool(buffer);
            if (TextIncluded) {
                PageText = BinaryHelpers.ReadString(buffer);
            }
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class GeneratorTable : IACDataType {
        /// <summary>
        /// List of generator profiles
        /// </summary>
        public PackableList<GeneratorProfile> Generators;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Generators = new PackableList<GeneratorProfile>();
            Generators.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class GeneratorProfile : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public float Probability;

        /// <summary>
        /// TODO
        /// </summary>
        public uint TypeID;

        /// <summary>
        /// TODO
        /// </summary>
        public double Delay;

        /// <summary>
        /// TODO
        /// </summary>
        public uint InitCreate;

        /// <summary>
        /// TODO
        /// </summary>
        public uint MaxNum;

        /// <summary>
        /// TODO
        /// </summary>
        public uint WhenCreate;

        /// <summary>
        /// TODO
        /// </summary>
        public uint WhereCreate;

        /// <summary>
        /// TODO
        /// </summary>
        public uint StackSize;

        /// <summary>
        /// TODO
        /// </summary>
        public uint PTId;

        /// <summary>
        /// TODO
        /// </summary>
        public float Shade;

        /// <summary>
        /// TODO
        /// </summary>
        public Position Position;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Slot;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Probability = BinaryHelpers.ReadSingle(buffer);
            TypeID = BinaryHelpers.ReadUInt32(buffer);
            Delay = BinaryHelpers.ReadDouble(buffer);
            InitCreate = BinaryHelpers.ReadUInt32(buffer);
            MaxNum = BinaryHelpers.ReadUInt32(buffer);
            WhenCreate = BinaryHelpers.ReadUInt32(buffer);
            WhereCreate = BinaryHelpers.ReadUInt32(buffer);
            StackSize = BinaryHelpers.ReadUInt32(buffer);
            PTId = BinaryHelpers.ReadUInt32(buffer);
            Shade = BinaryHelpers.ReadSingle(buffer);
            Position = new Position();
            Position.ReadFromBuffer(buffer);
            Slot = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class GeneratorRegistry : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<uint, GeneratorRegistryNode> Registry;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Registry = new PackableHashTable<uint, GeneratorRegistryNode>();
            Registry.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class GeneratorRegistryNode : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint WeenieClassIdOrType;

        /// <summary>
        /// TODO
        /// </summary>
        public double TS;

        /// <summary>
        /// TODO
        /// </summary>
        public uint TreasureType;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Slot;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Checkpointed;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Shop;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Amount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            WeenieClassIdOrType = BinaryHelpers.ReadUInt32(buffer);
            TS = BinaryHelpers.ReadDouble(buffer);
            TreasureType = BinaryHelpers.ReadUInt32(buffer);
            Slot = BinaryHelpers.ReadUInt32(buffer);
            Checkpointed = BinaryHelpers.ReadUInt32(buffer);
            Shop = BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set of inventory items
    /// </summary>
    public class GeneratorQueue : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public PackableList<GeneratorQueueNode> Queue;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Queue = new PackableList<GeneratorQueueNode>();
            Queue.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class GeneratorQueueNode : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Slot;

        /// <summary>
        /// TODO
        /// </summary>
        public double When;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Slot = BinaryHelpers.ReadUInt32(buffer);
            When = BinaryHelpers.ReadDouble(buffer);
        }

    }

    /// <summary>
    /// The PlayerModule structure contains character options.
    /// </summary>
    public class PlayerModule : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Flags;

        /// <summary>
        /// TODO
        /// </summary>
        public CharacterOptions1 options;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ShortcutCount;

        /// <summary>
        /// Vector collection data for the shortcuts vector
        /// </summary>
        public class Shortcut {
            /// <summary>
            /// TODO
            /// </summary>
            public uint Position;

            /// <summary>
            /// TODO
            /// </summary>
            public uint Target;

            /// <summary>
            /// TODO
            /// </summary>
            public uint Unknown;

        }

        /// <summary>
        /// List of shortcuts
        /// </summary>
        public List<Shortcut> Shortcuts = new List<Shortcut>();

        /// <summary>
        /// SpellBar Tab 1 item count
        /// </summary>
        public uint Tab1Count;

        /// <summary>
        /// SpellBar tab 1 spell ids
        /// </summary>
        public List<uint> Tab1 = new List<uint>();

        /// <summary>
        /// SpellBar Tab 2 item count
        /// </summary>
        public uint Tab2Count;

        /// <summary>
        /// SpellBar tab 2 spell ids
        /// </summary>
        public List<uint> Tab2 = new List<uint>();

        /// <summary>
        /// SpellBar Tab 3 item count
        /// </summary>
        public uint Tab3Count;

        /// <summary>
        /// SpellBar tab 3 spell ids
        /// </summary>
        public List<uint> Tab3 = new List<uint>();

        /// <summary>
        /// SpellBar Tab 4 item count
        /// </summary>
        public uint Tab4Count;

        /// <summary>
        /// SpellBar tab 4 spell ids
        /// </summary>
        public List<uint> Tab4 = new List<uint>();

        /// <summary>
        /// SpellBar Tab 5 item count
        /// </summary>
        public uint Tab5Count;

        /// <summary>
        /// SpellBar tab 5 spell ids
        /// </summary>
        public List<uint> Tab5 = new List<uint>();

        /// <summary>
        /// SpellBar Tab 6 item count
        /// </summary>
        public uint Tab6Count;

        /// <summary>
        /// SpellBar tab 6 spell ids
        /// </summary>
        public List<uint> Tab6 = new List<uint>();

        /// <summary>
        /// SpellBar Tab 7 item count
        /// </summary>
        public uint Tab7Count;

        /// <summary>
        /// SpellBar tab 7 spell ids
        /// </summary>
        public List<uint> Tab7 = new List<uint>();

        /// <summary>
        /// SpellBar Tab 8 item count
        /// </summary>
        public uint Tab8Count;

        /// <summary>
        /// SpellBar tab 8 spell ids
        /// </summary>
        public List<uint> Tab8 = new List<uint>();

        /// <summary>
        /// Fill comps list count (for vendoring)
        /// </summary>
        public ushort FillcompsCount;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort FillcompsUnknown;

        /// <summary>
        /// Vector collection data for the fillcomps vector
        /// </summary>
        public class Fillcomp {
            /// <summary>
            /// Component Id to buy
            /// </summary>
            public uint Id;

            /// <summary>
            /// Number of components to buy
            /// </summary>
            public uint Count;

        }

        /// <summary>
        /// Fill comps list (for vendoring)
        /// </summary>
        public List<Fillcomp> Fillcomps = new List<Fillcomp>();

        /// <summary>
        /// TODO
        /// </summary>
        public uint Unknown1;

        /// <summary>
        /// TODO
        /// </summary>
        public uint OptionFlags;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Unknown2;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort OptionStringCount;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort OptionStringUnknown;

        /// <summary>
        /// Vector collection data for the optionStrings vector
        /// </summary>
        public class OptionString {
            /// <summary>
            /// TODO
            /// </summary>
            public uint Key;

            /// <summary>
            /// TODO
            /// </summary>
            public string Value;

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<OptionString> OptionStrings = new List<OptionString>();

        /// <summary>
        /// TODO
        /// </summary>
        public uint Unknown3;

        /// <summary>
        /// TODO
        /// </summary>
        public byte Unknown4;

        /// <summary>
        /// TODO
        /// </summary>
        public byte OptionPropertyCount;

        /// <summary>
        /// Vector collection data for the optionProperties vector
        /// </summary>
        public class OptionProperty {
            /// <summary>
            /// TODO
            /// </summary>
            public uint Type;

            /// <summary>
            /// TODO
            /// </summary>
            public uint Unknown1;

            /// <summary>
            /// TODO
            /// </summary>
            public uint WindowCount;

            /// <summary>
            /// Vector collection data for the windows vector
            /// </summary>
            public class Window {
                /// <summary>
                /// TODO
                /// </summary>
                public uint Type;

                /// <summary>
                /// TODO
                /// </summary>
                public byte Unknown;

                /// <summary>
                /// TODO
                /// </summary>
                public byte PropertyCount;

                /// <summary>
                /// Vector collection data for the properties vector
                /// </summary>
                public class Property {
                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint Key;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint Unknown;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public byte TitleSource;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint StringID;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint FileID;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public string value_a;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint unknown_1b;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public ushort unknown_1c;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint unknown_d;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public byte value_d;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint unknown_e;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint value_e;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint unknown_f;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint value_f;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint unknown_h;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint value_h;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint unknown_i;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint value_i;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public uint unknown_j;

                    /// <summary>
                    /// TODO
                    /// </summary>
                    public ulong value_j;

                }

                /// <summary>
                /// TODO
                /// </summary>
                public List<OptionProperty.Window.Property> Properties = new List<OptionProperty.Window.Property>();

            }

            /// <summary>
            /// TODO
            /// </summary>
            public List<OptionProperty.Window> Windows = new List<OptionProperty.Window>();

            /// <summary>
            /// TODO
            /// </summary>
            public uint Unknown3;

            /// <summary>
            /// TODO
            /// </summary>
            public float ActiveOpacity;

            /// <summary>
            /// TODO
            /// </summary>
            public uint Unknown4;

            /// <summary>
            /// TODO
            /// </summary>
            public float InactiveOpacity;

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<OptionProperty> OptionProperties = new List<OptionProperty>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            options = (CharacterOptions1)BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x00000001) != 0) {
                ShortcutCount = BinaryHelpers.ReadUInt32(buffer);
                for (var i = 0; i < ShortcutCount; i++) {
                    var position = BinaryHelpers.ReadUInt32(buffer);
                    var target = BinaryHelpers.ReadUInt32(buffer);
                    var unknown3 = BinaryHelpers.ReadUInt32(buffer);
                    Shortcuts.Add(new Shortcut() {
                        Position = position,
                        Target = target,
                        Unknown = unknown3,
                    });
                }
            }
            Tab1Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab1Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab1.Add(spell);
            }
            Tab2Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab2Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab2.Add(spell);
            }
            Tab3Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab3Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab3.Add(spell);
            }
            Tab4Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab4Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab4.Add(spell);
            }
            Tab5Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab5Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab5.Add(spell);
            }
            Tab6Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab6Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab6.Add(spell);
            }
            Tab7Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab7Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab7.Add(spell);
            }
            Tab8Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Tab8Count; i++) {
                var spell = BinaryHelpers.ReadUInt32(buffer);
                Tab8.Add(spell);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                FillcompsCount = BinaryHelpers.ReadUInt16(buffer);
                FillcompsUnknown = BinaryHelpers.ReadUInt16(buffer);
                for (var i = 0; i < FillcompsCount; i++) {
                    var component = BinaryHelpers.ReadUInt32(buffer);
                    var count = BinaryHelpers.ReadUInt32(buffer);
                    Fillcomps.Add(new Fillcomp() {
                        Id = component,
                        Count = count,
                    });
                }
            }
            if (((uint)Flags & 0x00000020) != 0) {
                Unknown1 = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                OptionFlags = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000100) != 0) {
                Unknown2 = BinaryHelpers.ReadUInt32(buffer);
                OptionStringCount = BinaryHelpers.ReadUInt16(buffer);
                OptionStringUnknown = BinaryHelpers.ReadUInt16(buffer);
                for (var i = 0; i < OptionStringCount; i++) {
                    var key = BinaryHelpers.ReadUInt32(buffer);
                    var value = BinaryHelpers.ReadString(buffer);
                    OptionStrings.Add(new OptionString() {
                        Key = key,
                        Value = value,
                    });
                }
            }
            if (((uint)Flags & 0x00000200) != 0) {
                Unknown3 = BinaryHelpers.ReadUInt32(buffer);
                Unknown4 = BinaryHelpers.ReadByte(buffer);
                OptionPropertyCount = BinaryHelpers.ReadByte(buffer);
                for (var i = 0; i < OptionPropertyCount; i++) {
                    var type = BinaryHelpers.ReadUInt32(buffer);
                    uint unknown_a = 0;
                    uint windowCount = 0;
                    var windows = new List<OptionProperty.Window>();
                    uint unknown_k = 0;
                    float activeOpacity = 0;
                    uint unknown_l = 0;
                    float inactiveOpacity = 0;
                    switch ((int)type) {
                        case 0x1000008c:
                            unknown_a = BinaryHelpers.ReadUInt32(buffer);
                            windowCount = BinaryHelpers.ReadUInt32(buffer);
                            for (var x = 0; x < windowCount; x++) {
                                var type_a = BinaryHelpers.ReadUInt32(buffer);
                                byte unknown_b = 0;
                                byte propertyCount = 0;
                                var properties = new List<OptionProperty.Window.Property>();
                                switch ((int)type_a) {
                                    case 0x1000008b:
                                        unknown_b = BinaryHelpers.ReadByte(buffer);
                                        propertyCount = BinaryHelpers.ReadByte(buffer);
                                        for (var y = 0; y < propertyCount; y++) {
                                            var key_a = BinaryHelpers.ReadUInt32(buffer);
                                            uint unknown_c = 0;
                                            byte titleSource = 0;
                                            uint stringID = 0;
                                            uint fileID = 0;
                                            string value_a = null;
                                            uint unknown_1b = 0;
                                            ushort unknown_1c = 0;
                                            uint unknown_d = 0;
                                            byte value_d = 0;
                                            uint unknown_e = 0;
                                            uint value_e = 0;
                                            uint unknown_f = 0;
                                            uint value_f = 0;
                                            uint unknown_h = 0;
                                            uint value_h = 0;
                                            uint unknown_i = 0;
                                            uint value_i = 0;
                                            uint unknown_j = 0;
                                            ulong value_j = 0;
                                            switch ((int)key_a) {
                                                case 0x1000008d:
                                                    unknown_c = BinaryHelpers.ReadUInt32(buffer);
                                                    titleSource = BinaryHelpers.ReadByte(buffer);
                                                    switch ((int)titleSource) {
                                                        case 0x00:
                                                            stringID = BinaryHelpers.ReadUInt32(buffer);
                                                            fileID = BinaryHelpers.ReadUInt32(buffer);
                                                            break;
                                                        case 0x01:
                                                            value_a = BinaryHelpers.ReadWString(buffer);
                                                            break;
                                                    }
                                                    unknown_1b = BinaryHelpers.ReadUInt32(buffer);
                                                    unknown_1c = BinaryHelpers.ReadUInt16(buffer);
                                                    break;
                                                case 0x1000008a:
                                                    unknown_d = BinaryHelpers.ReadUInt32(buffer);
                                                    value_d = BinaryHelpers.ReadByte(buffer);
                                                    break;
                                                case 0x10000089:
                                                    unknown_e = BinaryHelpers.ReadUInt32(buffer);
                                                    value_e = BinaryHelpers.ReadUInt32(buffer);
                                                    break;
                                                case 0x10000088:
                                                    unknown_f = BinaryHelpers.ReadUInt32(buffer);
                                                    value_f = BinaryHelpers.ReadUInt32(buffer);
                                                    break;
                                                case 0x10000087:
                                                    unknown_h = BinaryHelpers.ReadUInt32(buffer);
                                                    value_h = BinaryHelpers.ReadUInt32(buffer);
                                                    break;
                                                case 0x10000086:
                                                    unknown_i = BinaryHelpers.ReadUInt32(buffer);
                                                    value_i = BinaryHelpers.ReadUInt32(buffer);
                                                    break;
                                                case 0x1000007F:
                                                    unknown_j = BinaryHelpers.ReadUInt32(buffer);
                                                    value_j = BinaryHelpers.ReadUInt64(buffer);
                                                    break;
                                            }
                                            properties.Add(new OptionProperty.Window.Property() {
                                                Key = key_a,
                                                Unknown = unknown_c,
                                                TitleSource = titleSource,
                                                StringID = stringID,
                                                FileID = fileID,
                                                value_a = value_a,
                                                unknown_1b = unknown_1b,
                                                unknown_1c = unknown_1c,
                                                unknown_d = unknown_d,
                                                value_d = value_d,
                                                unknown_e = unknown_e,
                                                value_e = value_e,
                                                unknown_f = unknown_f,
                                                value_f = value_f,
                                                unknown_h = unknown_h,
                                                value_h = value_h,
                                                unknown_i = unknown_i,
                                                value_i = value_i,
                                                unknown_j = unknown_j,
                                                value_j = value_j,
                                            });
                                        }
                                        break;
                                }
                                windows.Add(new OptionProperty.Window() {
                                    Type = type_a,
                                    Unknown = unknown_b,
                                    PropertyCount = propertyCount,
                                    Properties = properties,
                                });
                            }
                            break;
                        case 0x10000081:
                            unknown_k = BinaryHelpers.ReadUInt32(buffer);
                            activeOpacity = BinaryHelpers.ReadSingle(buffer);
                            break;
                        case 0x10000080:
                            unknown_l = BinaryHelpers.ReadUInt32(buffer);
                            inactiveOpacity = BinaryHelpers.ReadSingle(buffer);
                            break;
                    }
                    OptionProperties.Add(new OptionProperty() {
                        Type = type,
                        Unknown1 = unknown_a,
                        WindowCount = windowCount,
                        Windows = windows,
                        Unknown3 = unknown_k,
                        ActiveOpacity = activeOpacity,
                        Unknown4 = unknown_l,
                        InactiveOpacity = inactiveOpacity,
                    });
                }
                if ((buffer.BaseStream.Position % 4) != 0) {
                    buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
                }
            }
        }

    }

    /// <summary>
    /// Set of shortcuts
    /// </summary>
    public class ShortCutManager : IACDataType {
        /// <summary>
        /// List of shortcuts.
        /// </summary>
        public PackableList<ShortCutData> Shortcuts;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Shortcuts = new PackableList<ShortCutData>();
            Shortcuts.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Shortcut
    /// </summary>
    public class ShortCutData : IACDataType {
        /// <summary>
        /// Position
        /// </summary>
        public uint Index;

        /// <summary>
        /// Object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// May not have been used in prod?  Maybe a remnet of before spell tabs?  I don&#39;t think you could put spells in shortcut spot...
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Index = BinaryHelpers.ReadUInt32(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// List of spells in spell tab
    /// </summary>
    public class SpellTab : IACDataType {
        /// <summary>
        /// List of spells on tab.
        /// </summary>
        public PackableList<LayeredSpellId> Spells;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Spells = new PackableList<LayeredSpellId>();
            Spells.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class GenericQualitiesData : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Flags;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<uint, uint> Ints;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<uint, bool> Bools;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<uint, float> Floats;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<uint, string> Strings;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x00000001) != 0) {
                Ints = new PackableHashTable<uint, uint>();
                Ints.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                Bools = new PackableHashTable<uint, bool>();
                Bools.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                Floats = new PackableHashTable<uint, float>();
                Floats.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                Strings = new PackableHashTable<uint, string>();
                Strings.ReadFromBuffer(buffer);
            }
        }

    }

    /// <summary>
    /// Set of inventory items
    /// </summary>
    public class ContentProfile : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Id;

        /// <summary>
        /// Whether or not this object is a container.
        /// </summary>
        public ContainerProperties ContainerProperties;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            ContainerProperties = (ContainerProperties)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set of inventory items
    /// </summary>
    public class InventoryPlacement : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Id;

        /// <summary>
        /// TODO
        /// </summary>
        public EquipMask Location;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Priority;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Location = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            Priority = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Allegience information
    /// </summary>
    public class AllegianceProfile : IACDataType {
        /// <summary>
        /// The number of allegiance members.
        /// </summary>
        public uint TotalMembers;

        /// <summary>
        /// Your personal number of followers.
        /// </summary>
        public uint TotalVassals;

        /// <summary>
        /// Allegiance hierarchy info
        /// </summary>
        public AllegianceHierarchy Hierarchy;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            TotalMembers = BinaryHelpers.ReadUInt32(buffer);
            TotalVassals = BinaryHelpers.ReadUInt32(buffer);
            Hierarchy = new AllegianceHierarchy();
            Hierarchy.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Allegience hierarchy information
    /// </summary>
    public class AllegianceHierarchy : IACDataType {
        /// <summary>
        /// Number of character allegiance records.
        /// </summary>
        public ushort RecordCount;

        /// <summary>
        /// Defines which properties are available. 0x0B seems to be the latest version which includes all properties.
        /// </summary>
        public ushort OldVersion;

        /// <summary>
        /// Taking a guess on these values.  Guessing they may only be valid on Monarchs.
        /// </summary>
        public PHashTable<uint, AllegianceOfficerLevel> Officers;

        /// <summary>
        /// Believe these may pass in the current officer title list.  Guessing they may only be valid on Monarchs.
        /// </summary>
        public PSmartArray<string> OfficerTitles;

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint MonarchBroadcastTime;

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint MonarchBroadcastsToday;

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint SpokesBroadcastTime;

        /// <summary>
        /// May only be valid for Monarchs/Speakers?
        /// </summary>
        public uint SpokesBroadcastsToday;

        /// <summary>
        /// Text for current motd. May only be valid for Monarchs/Speakers?
        /// </summary>
        public string MOTD;

        /// <summary>
        /// Who set the current motd. May only be valid for Monarchs/Speakers?
        /// </summary>
        public string MOTDSetBy;

        /// <summary>
        /// allegiance chat channel number
        /// </summary>
        public uint ChatRoomID;

        /// <summary>
        /// Location of monarchy bindpoint
        /// </summary>
        public Position Bindpoint;

        /// <summary>
        /// The name of the allegiance.
        /// </summary>
        public string AllegianceName;

        /// <summary>
        /// Time name was last set.  Seems to count upward for some reason.
        /// </summary>
        public uint NameLastSetTime;

        /// <summary>
        /// Whether allegiance is locked.
        /// </summary>
        public bool Locked;

        /// <summary>
        /// TODO
        /// </summary>
        public int ApprovedVassal;

        /// <summary>
        /// Monarch&#39;s data
        /// </summary>
        public AllegianceData MonarchData;

        /// <summary>
        /// Vector collection data for the records vector
        /// </summary>
        public class Record {
            /// <summary>
            /// The Object ID for the parent character to this character.  Used by the client to decide how to build the display in the Allegiance tab. 1 is the monarch.
            /// </summary>
            public uint TreeParent;

            /// <summary>
            /// TODO
            /// </summary>
            public AllegianceData AllegianceData;

        }

        /// <summary>
        /// Data for remaining people, which I believe should be your vassels.
        /// </summary>
        public List<Record> Records = new List<Record>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            RecordCount = BinaryHelpers.ReadUInt16(buffer);
            OldVersion = BinaryHelpers.ReadUInt16(buffer);
            Officers = new PHashTable<uint, AllegianceOfficerLevel>();
            Officers.ReadFromBuffer(buffer);
            OfficerTitles = new PSmartArray<string>();
            OfficerTitles.ReadFromBuffer(buffer);
            MonarchBroadcastTime = BinaryHelpers.ReadUInt32(buffer);
            MonarchBroadcastsToday = BinaryHelpers.ReadUInt32(buffer);
            SpokesBroadcastTime = BinaryHelpers.ReadUInt32(buffer);
            SpokesBroadcastsToday = BinaryHelpers.ReadUInt32(buffer);
            MOTD = BinaryHelpers.ReadString(buffer);
            MOTDSetBy = BinaryHelpers.ReadString(buffer);
            ChatRoomID = BinaryHelpers.ReadUInt32(buffer);
            Bindpoint = new Position();
            Bindpoint.ReadFromBuffer(buffer);
            AllegianceName = BinaryHelpers.ReadString(buffer);
            NameLastSetTime = BinaryHelpers.ReadUInt32(buffer);
            Locked = BinaryHelpers.ReadBool(buffer);
            ApprovedVassal = BinaryHelpers.ReadInt32(buffer);

            if (RecordCount == 0)
                return;

            MonarchData = new AllegianceData();
            MonarchData.ReadFromBuffer(buffer);
            for (var i = 0; i < RecordCount - 1; i++) {
                var treeParent = BinaryHelpers.ReadUInt32(buffer);
                var allegianceData = new AllegianceData();
                allegianceData.ReadFromBuffer(buffer);
                Records.Add(new Record() {
                    TreeParent = treeParent,
                    AllegianceData = allegianceData,
                });
            }
        }

    }

    /// <summary>
    /// Set of allegiance data for a specific player
    /// </summary>
    public class AllegianceData : IACDataType {
        /// <summary>
        /// Character ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// XP gained while logged off
        /// </summary>
        public uint ExperienceCached;

        /// <summary>
        /// Total allegiance XP contribution.
        /// </summary>
        public uint ExperienceTithed;

        /// <summary>
        /// TODO
        /// </summary>
        public AllegianceRecordFlags Bitfield;

        /// <summary>
        /// The gender of the character (for determining title).
        /// </summary>
        public Gender Gender;

        /// <summary>
        /// The heritage of the character (for determining title).
        /// </summary>
        public HeritageGroup Heritage;

        /// <summary>
        /// The numerical rank (1 is lowest).
        /// </summary>
        public ushort Rank;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Level;

        /// <summary>
        /// Character loyalty.
        /// </summary>
        public ushort Loyalty;

        /// <summary>
        /// Character leadership.
        /// </summary>
        public ushort Leadership;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong TimeOnline;

        /// <summary>
        /// TODO
        /// </summary>
        public uint AllegianceAge;

        /// <summary>
        /// The character name of this member
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ExperienceCached = BinaryHelpers.ReadUInt32(buffer);
            ExperienceTithed = BinaryHelpers.ReadUInt32(buffer);
            Bitfield = (AllegianceRecordFlags)BinaryHelpers.ReadUInt32(buffer);
            Gender = (Gender)BinaryHelpers.ReadByte(buffer);
            Heritage = (HeritageGroup)BinaryHelpers.ReadByte(buffer);
            Rank = BinaryHelpers.ReadUInt16(buffer);
            if ((Bitfield & AllegianceRecordFlags.HasPackedLevel) != 0) {
                Level = BinaryHelpers.ReadUInt32(buffer);
            }
            Loyalty = BinaryHelpers.ReadUInt16(buffer);
            Leadership = BinaryHelpers.ReadUInt16(buffer);
            if ((Bitfield & AllegianceRecordFlags.HasAllegianceAge) != 0) {
                TimeOnline = BinaryHelpers.ReadUInt32(buffer);
                AllegianceAge = BinaryHelpers.ReadUInt32(buffer);
            }
            else {
                TimeOnline = BinaryHelpers.ReadUInt64(buffer);
            }
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class FriendData : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint FriendsCount;

        /// <summary>
        /// Vector collection data for the friends vector
        /// </summary>
        public class Friend {
            /// <summary>
            /// Object id of the friended character
            /// </summary>
            public uint ObjectId;

            /// <summary>
            /// TODO
            /// </summary>
            public uint Online;

            /// <summary>
            /// TODO
            /// </summary>
            public uint Unknown1;

            /// <summary>
            /// Name of the friended character
            /// </summary>
            public string Name;

            /// <summary>
            /// TODO
            /// </summary>
            public uint OutFriendsCount;

            /// <summary>
            /// TODO
            /// </summary>
            public List<uint> OutFriends = new List<uint>();

            /// <summary>
            /// TODO
            /// </summary>
            public uint InFriendsCount;

            /// <summary>
            /// TODO
            /// </summary>
            public List<uint> InFriends = new List<uint>();

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<Friend> Friends = new List<Friend>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            FriendsCount = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < FriendsCount; i++) {
                var friend = BinaryHelpers.ReadUInt32(buffer);
                var online = BinaryHelpers.ReadUInt32(buffer);
                var unknown1 = BinaryHelpers.ReadUInt32(buffer);
                var name = BinaryHelpers.ReadString(buffer);
                var outFriendsCount = BinaryHelpers.ReadUInt32(buffer);
                var outFriends = new List<uint>();
                for (var x = 0; x < outFriendsCount; x++) {
                    var outFriend = BinaryHelpers.ReadUInt32(buffer);
                    outFriends.Add(outFriend);
                }
                var inFriendsCount = BinaryHelpers.ReadUInt32(buffer);
                var inFriends = new List<uint>();
                for (var x = 0; x < inFriendsCount; x++) {
                    var inFriend = BinaryHelpers.ReadUInt32(buffer);
                    inFriends.Add(inFriend);
                }
                Friends.Add(new Friend() {
                    ObjectId = friend,
                    Online = online,
                    Unknown1 = unknown1,
                    Name = name,
                    OutFriendsCount = outFriendsCount,
                    OutFriends = outFriends,
                    InFriendsCount = inFriendsCount,
                    InFriends = inFriends,
                });
            }
        }

    }

    /// <summary>
    /// Data related to an item, namely the amount and description
    /// </summary>
    public class ItemProfile : IACDataType {
        /// <summary>
        /// Packed data of Amount and PWDType
        /// </summary>
        public int PackedAmount;

        /// <summary>
        /// Derived from packedAmount. the number of items for sale (-1 for an unlimited supply)
        /// </summary>
        public int Amount { get => (int)(PackedAmount & 0xFFFFFF); }

        /// <summary>
        /// Derived from packedAmount. flag indicating whether the new or old PublicWeenieDesc is used
        /// </summary>
        public int PWDType { get => (int)(PackedAmount >> 24); }

        /// <summary>
        /// the object ID of the item
        /// </summary>
        public uint ObjectID;

        /// <summary>
        /// details about the item
        /// </summary>
        public PublicWeenieDesc WeenieDesc;

        /// <summary>
        /// details about the item
        /// </summary>
        public OldPublicWeenieDesc OldWeenieDesc;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PackedAmount = BinaryHelpers.ReadInt32(buffer);
            ObjectID = BinaryHelpers.ReadUInt32(buffer);
            switch ((int)PWDType) {
                case -1:
                    WeenieDesc = new PublicWeenieDesc();
                    WeenieDesc.ReadFromBuffer(buffer);
                    break;
                case 1:
                    OldWeenieDesc = new OldPublicWeenieDesc();
                    OldWeenieDesc.ReadFromBuffer(buffer);
                    break;
            }
        }

    }

    /// <summary>
    /// The PublicWeenieDesc structure defines an object&#39;s game behavior.
    /// </summary>
    public class PublicWeenieDesc : IACDataType {
        /// <summary>
        /// game data flags
        /// </summary>
        public uint Flags;

        /// <summary>
        /// object name
        /// </summary>
        public string Name;

        /// <summary>
        /// object weenie class id
        /// </summary>
        public uint WeenieClassId;

        /// <summary>
        /// icon ResourceID (minus 0x06000000)
        /// </summary>
        public uint Icon;

        /// <summary>
        /// object type
        /// </summary>
        public ObjectType Type;

        /// <summary>
        /// object behaviors
        /// </summary>
        public ObjectDescriptionFlag ObjectDescription;

        /// <summary>
        /// additional game data flags
        /// </summary>
        public uint Header2;

        /// <summary>
        /// plural object name (if not specified, use &lt;name&gt; followed by &#39;s&#39; or &#39;es&#39;)
        /// </summary>
        public string PluralName;

        /// <summary>
        /// number of item slots
        /// </summary>
        public byte ItemsCapacity;

        /// <summary>
        /// number of pack slots
        /// </summary>
        public byte ContainerCapacity;

        /// <summary>
        /// missile ammunition type
        /// </summary>
        public AmmoType AmmunitionType;

        /// <summary>
        /// object value
        /// </summary>
        public uint Value;

        /// <summary>
        /// TODO
        /// </summary>
        public UsableType Useability;

        /// <summary>
        /// distance a player will walk to use an object
        /// </summary>
        public float UseRadius;

        /// <summary>
        /// the object categories this object may be used on
        /// </summary>
        public ObjectType TargetType;

        /// <summary>
        /// the type of highlight (outline) applied to the object&#39;s icon
        /// </summary>
        public IconHighlight Effects;

        /// <summary>
        /// the type of wieldable item this is
        /// </summary>
        public WieldType CombatUse;

        /// <summary>
        /// the number of uses remaining for this item (also salvage quantity)
        /// </summary>
        public ushort UsesRemaining;

        /// <summary>
        /// the maximum number of uses possible for this item (also maximum salvage quantity)
        /// </summary>
        public ushort MaxUses;

        /// <summary>
        /// the number of items in this stack of objects
        /// </summary>
        public ushort StackSize;

        /// <summary>
        /// the maximum number of items possible in this stack of objects
        /// </summary>
        public ushort MaxStackSize;

        /// <summary>
        /// the ID of the container holding this object
        /// </summary>
        public uint ContainerId;

        /// <summary>
        /// the ID of the creature equipping this object
        /// </summary>
        public uint WielderId;

        /// <summary>
        /// the potential equipment slots this object may be placed in
        /// </summary>
        public EquipMask ValidLocations;

        /// <summary>
        /// the actual equipment slots this object is currently placed in
        /// </summary>
        public EquipMask Location;

        /// <summary>
        /// the parts of the body this object protects
        /// </summary>
        public CoverageMask Priority;

        /// <summary>
        /// radar dot color
        /// </summary>
        public byte RadarColor;

        /// <summary>
        /// radar type
        /// </summary>
        public byte RadarType;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort PhysicsScript;

        /// <summary>
        /// object workmanship
        /// </summary>
        public float Workmanship;

        /// <summary>
        /// total burden of this object
        /// </summary>
        public ushort Burden;

        /// <summary>
        /// the spell cast by this object
        /// </summary>
        public ushort SpellId;

        /// <summary>
        /// the owner of this object
        /// </summary>
        public uint OwnerId;

        /// <summary>
        /// the access control list for this dwelling object
        /// </summary>
        public RestrictionDB Restrictions;

        /// <summary>
        /// what type of dwelling hook is this
        /// </summary>
        public HookType HookObjectTypes;

        /// <summary>
        /// this player&#39;s monarch
        /// </summary>
        public uint Monarch;

        /// <summary>
        /// the types of hooks this object may be placed on (-1 for hooks)
        /// </summary>
        public HookType HookType;

        /// <summary>
        /// icon overlay ResourceID (minus 0x06000000)
        /// </summary>
        public uint IconOverlay;

        /// <summary>
        /// icon underlay ResourceID (minus 0x06000000)
        /// </summary>
        public uint IconUnderlay;

        /// <summary>
        /// the type of material this object is made of
        /// </summary>
        public MaterialType Material;

        /// <summary>
        /// TODO
        /// </summary>
        public uint CooldownID;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong CooldownDuration;

        /// <summary>
        /// TODO
        /// </summary>
        public uint PetOwnerId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            WeenieClassId = BinaryHelpers.ReadPackedDWORD(buffer);
            Icon = BinaryHelpers.ReadPackedDWORD(buffer);
            Type = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            ObjectDescription = (ObjectDescriptionFlag)BinaryHelpers.ReadUInt32(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
            if (((uint)ObjectDescription & 0x04000000) != 0) {
                Header2 = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000001) != 0) {
                PluralName = BinaryHelpers.ReadString(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                ItemsCapacity = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                ContainerCapacity = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00000100) != 0) {
                AmmunitionType = (AmmoType)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                Value = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000010) != 0) {
                Useability = (UsableType)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000020) != 0) {
                UseRadius = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00080000) != 0) {
                TargetType = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000080) != 0) {
                Effects = (IconHighlight)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000200) != 0) {
                CombatUse = (WieldType)BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00000400) != 0) {
                UsesRemaining = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000800) != 0) {
                MaxUses = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00001000) != 0) {
                StackSize = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00002000) != 0) {
                MaxStackSize = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00004000) != 0) {
                ContainerId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00008000) != 0) {
                WielderId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00010000) != 0) {
                ValidLocations = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00020000) != 0) {
                Location = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00040000) != 0) {
                Priority = (CoverageMask)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00100000) != 0) {
                RadarColor = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00800000) != 0) {
                RadarType = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x08000000) != 0) {
                PhysicsScript = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x01000000) != 0) {
                Workmanship = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00200000) != 0) {
                Burden = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00400000) != 0) {
                SpellId = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x02000000) != 0) {
                OwnerId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x04000000) != 0) {
                Restrictions = new RestrictionDB();
                Restrictions.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x20000000) != 0) {
                HookObjectTypes = (HookType)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                Monarch = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x10000000) != 0) {
                HookType = (HookType)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x40000000) != 0) {
                IconOverlay = BinaryHelpers.ReadPackedDWORD(buffer);
            }
            if (((uint)Header2 & 0x00000001) != 0) {
                IconUnderlay = BinaryHelpers.ReadPackedDWORD(buffer);
            }
            if (((uint)Flags & 0x80000000) != 0) {
                Material = (MaterialType)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Header2 & 0x00000002) != 0) {
                CooldownID = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Header2 & 0x00000004) != 0) {
                CooldownDuration = BinaryHelpers.ReadUInt64(buffer);
            }
            if (((uint)Header2 & 0x00000008) != 0) {
                PetOwnerId = BinaryHelpers.ReadUInt32(buffer);
            }
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// The RestrictionDB contains the access control list for a dwelling object.
    /// </summary>
    public class RestrictionDB : IACDataType {
        /// <summary>
        /// If high word is not 0, this value indicates the version of the message.
        /// </summary>
        public uint Version;

        /// <summary>
        /// 0 = private dwelling, 1 = open to public
        /// </summary>
        public uint Bitmask;

        /// <summary>
        /// allegiance monarch (if allegiance access granted)
        /// </summary>
        public uint MonarchID;

        /// <summary>
        /// Set of permissions on a per user basis. Key is the character id, value is 0 = dwelling access only, 1 = storage access also
        /// </summary>
        public PHashTable<uint, uint> Permissions;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Version = BinaryHelpers.ReadUInt32(buffer);
            Bitmask = BinaryHelpers.ReadUInt32(buffer);
            MonarchID = BinaryHelpers.ReadUInt32(buffer);
            Permissions = new PHashTable<uint, uint>();
            Permissions.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// The OldPublicWeenieDesc structure defines an object&#39;s game behavior.
    /// </summary>
    public class OldPublicWeenieDesc : IACDataType {
        /// <summary>
        /// game data flags
        /// </summary>
        public uint Flags;

        /// <summary>
        /// object name
        /// </summary>
        public string Name;

        /// <summary>
        /// object weenie class id
        /// </summary>
        public uint WeenieClassID;

        /// <summary>
        /// icon ResourceID (minus 0x06000000)
        /// </summary>
        public uint Icon;

        /// <summary>
        /// object type
        /// </summary>
        public ObjectType Type;

        /// <summary>
        /// object behaviors
        /// </summary>
        public ObjectDescriptionFlag ObjectDescription;

        /// <summary>
        /// plural object name (if not specified, use &lt;name&gt; followed by &#39;s&#39; or &#39;es&#39;)
        /// </summary>
        public string PluralName;

        /// <summary>
        /// number of item slots
        /// </summary>
        public byte ItemsCapacity;

        /// <summary>
        /// number of pack slots
        /// </summary>
        public byte ContainerCapacity;

        /// <summary>
        /// object value
        /// </summary>
        public uint Value;

        /// <summary>
        /// TODO
        /// </summary>
        public UsableType Useability;

        /// <summary>
        /// distance a player will walk to use an object
        /// </summary>
        public float UseRadius;

        /// <summary>
        /// the object categories this object may be used on
        /// </summary>
        public ObjectType TargetType;

        /// <summary>
        /// the type of highlight (outline) applied to the object&#39;s icon
        /// </summary>
        public IconHighlight Effects;

        /// <summary>
        /// missile ammunition type
        /// </summary>
        public AmmoType AmmunitionType;

        /// <summary>
        /// the type of wieldable item this is
        /// </summary>
        public WieldType CombatUse;

        /// <summary>
        /// the number of uses remaining for this item (also salvage quantity)
        /// </summary>
        public ushort UsesRemaining;

        /// <summary>
        /// the maximum number of uses possible for this item (also maximum salvage quantity)
        /// </summary>
        public ushort MaxUses;

        /// <summary>
        /// the number of items in this stack of objects
        /// </summary>
        public ushort StackSize;

        /// <summary>
        /// the maximum number of items possible in this stack of objects
        /// </summary>
        public ushort MaxStackSize;

        /// <summary>
        /// the ID of the container holding this object
        /// </summary>
        public uint ContainerId;

        /// <summary>
        /// the ID of the creature equipping this object
        /// </summary>
        public uint WielderId;

        /// <summary>
        /// the potential equipment slots this object may be placed in
        /// </summary>
        public EquipMask ValidLocations;

        /// <summary>
        /// the actual equipment slots this object is currently placed in
        /// </summary>
        public EquipMask Location;

        /// <summary>
        /// the parts of the body this object protects
        /// </summary>
        public CoverageMask Priority;

        /// <summary>
        /// radar dot color
        /// </summary>
        public byte RadarColor;

        /// <summary>
        /// radar type
        /// </summary>
        public byte RadarType;

        /// <summary>
        /// TODO
        /// </summary>
        public float ObviousDistance;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort VNDWeenieClassId;

        /// <summary>
        /// the spell cast by this object
        /// </summary>
        public ushort Spell;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HouseOwnerId;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort PhysicsScript;

        /// <summary>
        /// the access control list for this dwelling object
        /// </summary>
        public RestrictionDB Restrictions;

        /// <summary>
        /// the types of hooks this object may be placed on (-1 for hooks)
        /// </summary>
        public HookType HookType;

        /// <summary>
        /// what type of dwelling hook is this
        /// </summary>
        public HookType HookObjectTypes;

        /// <summary>
        /// this player&#39;s monarch
        /// </summary>
        public uint Monarch;

        /// <summary>
        /// icon overlay ResourceID (minus 0x06000000)
        /// </summary>
        public uint IconOverlay;

        /// <summary>
        /// the type of material this object is made of
        /// </summary>
        public MaterialType Material;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            WeenieClassID = BinaryHelpers.ReadPackedDWORD(buffer);
            Icon = BinaryHelpers.ReadPackedDWORD(buffer);
            Type = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            ObjectDescription = (ObjectDescriptionFlag)BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x00000001) != 0) {
                PluralName = BinaryHelpers.ReadString(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                ItemsCapacity = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                ContainerCapacity = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                Value = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000010) != 0) {
                Useability = (UsableType)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000020) != 0) {
                UseRadius = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00080000) != 0) {
                TargetType = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000080) != 0) {
                Effects = (IconHighlight)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000100) != 0) {
                AmmunitionType = (AmmoType)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000200) != 0) {
                CombatUse = (WieldType)BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00000400) != 0) {
                UsesRemaining = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000800) != 0) {
                MaxUses = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00001000) != 0) {
                StackSize = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00002000) != 0) {
                MaxStackSize = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00004000) != 0) {
                ContainerId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00008000) != 0) {
                WielderId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00010000) != 0) {
                ValidLocations = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00020000) != 0) {
                Location = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00040000) != 0) {
                Priority = (CoverageMask)BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00100000) != 0) {
                RadarColor = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x00800000) != 0) {
                RadarType = BinaryHelpers.ReadByte(buffer);
            }
            if (((uint)Flags & 0x01000000) != 0) {
                ObviousDistance = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00200000) != 0) {
                VNDWeenieClassId = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00400000) != 0) {
                Spell = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x02000000) != 0) {
                HouseOwnerId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x08000000) != 0) {
                PhysicsScript = BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x04000000) != 0) {
                Restrictions = new RestrictionDB();
                Restrictions.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x10000000) != 0) {
                HookType = (HookType)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x20000000) != 0) {
                HookObjectTypes = (HookType)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                Monarch = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x40000000) != 0) {
                IconOverlay = BinaryHelpers.ReadPackedDWORD(buffer);
            }
            if (((uint)Flags & 0x80000000) != 0) {
                Material = (MaterialType)BinaryHelpers.ReadUInt32(buffer);
            }
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Information related to a secure trade.
    /// </summary>
    public class Trade : IACDataType {
        /// <summary>
        /// ID of other participant in the trade
        /// </summary>
        public uint PartnerId;

        /// <summary>
        /// Some kind of sequence
        /// </summary>
        public ulong Stamp;

        /// <summary>
        /// Some kind of status for the trade TODO
        /// </summary>
        public uint Status;

        /// <summary>
        /// ID of person who initiated the trade
        /// </summary>
        public uint InitiatorId;

        /// <summary>
        /// Whether you accepted this trade
        /// </summary>
        public bool Accepted;

        /// <summary>
        /// Whether the partner accepted this trade
        /// </summary>
        public bool PartnerAccepted;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PartnerId = BinaryHelpers.ReadUInt32(buffer);
            Stamp = BinaryHelpers.ReadUInt64(buffer);
            Status = BinaryHelpers.ReadUInt32(buffer);
            InitiatorId = BinaryHelpers.ReadUInt32(buffer);
            Accepted = BinaryHelpers.ReadBool(buffer);
            PartnerAccepted = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// A jump with sequences
    /// </summary>
    public class JumpPack : IACDataType {
        /// <summary>
        /// Power of jump?
        /// </summary>
        public float Extent;

        /// <summary>
        /// Velocity data
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort ObjectServerControlSequence;

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort ObjectTeleportSequence;

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort ObjectForcePositionSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Extent = BinaryHelpers.ReadSingle(buffer);
            Velocity = new Vector3();
            Velocity.ReadFromBuffer(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectServerControlSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectTeleportSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// A set of data related to changing states with sequences
    /// </summary>
    public class MoveToStatePack : IACDataType {
        /// <summary>
        /// Raw motion data
        /// </summary>
        public RawMotionState RawMotionState;

        /// <summary>
        /// Position data
        /// </summary>
        public Position Position;

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort ObjectServerControlSequence;

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort ObjectTeleportSequence;

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort ObjectForcePositionSequence;

        /// <summary>
        /// Whether the player has contact with the ground, or if we are in longjump_mode(1 = contact, 2 = longjump_mode)
        /// </summary>
        public byte Contact;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            RawMotionState = new RawMotionState();
            RawMotionState.ReadFromBuffer(buffer);
            Position = new Position();
            Position.ReadFromBuffer(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectServerControlSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectTeleportSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer);
            Contact = BinaryHelpers.ReadByte(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Data related to the movement of the object sent from a client
    /// </summary>
    public class RawMotionState : IACDataType {
        /// <summary>
        /// Command ID
        /// </summary>
        public uint PackedFlags;

        /// <summary>
        /// Derived from flags. Sequence of the animation.
        /// </summary>
        public RawMotionFlags Flags { get => (RawMotionFlags)(PackedFlags & 0x7FF); }

        /// <summary>
        /// Derived from flags. Sequence of the animation.
        /// </summary>
        public ushort CommandListLength { get => (ushort)((PackedFlags >> 11)); }

        /// <summary>
        /// TODO
        /// </summary>
        public uint CurrentHoldkey;

        /// <summary>
        /// Current stance.  If not present, defaults to 0x3D (NonCombat)
        /// </summary>
        public MotionStance CurrentStyle;

        /// <summary>
        /// Command for our forward movement. If not present, defaults to 0x03 (Ready)
        /// </summary>
        public MotionCommand ForwardCommand;

        /// <summary>
        /// Whether forward key is being held
        /// </summary>
        public uint ForwardHoldkey;

        /// <summary>
        /// Forward movement speed. If not present, defaults to 1.0
        /// </summary>
        public float ForwardSpeed;

        /// <summary>
        /// Command for our sidestep movememnt. If not present, defaults to 0x00
        /// </summary>
        public MotionCommand SidestepCommand;

        /// <summary>
        /// Whether sidestep key is being held
        /// </summary>
        public uint SidestepHoldkey;

        /// <summary>
        /// Sidestep movement speed. If not present, defaults to 1.0
        /// </summary>
        public float SidestepSpeed;

        /// <summary>
        /// Command for our turn movememnt. If not present, defaults to 0x00
        /// </summary>
        public MotionCommand TurnCommand;

        /// <summary>
        /// Whether turn key is being held
        /// </summary>
        public uint TurnHoldkey;

        /// <summary>
        /// Turn movement speed. If not present, defaults to 1.0
        /// </summary>
        public float TurnSpeed;

        /// <summary>
        /// Vector collection data for the commands vector
        /// </summary>
        public class Command {
            /// <summary>
            /// Command ID
            /// </summary>
            public MotionCommand Id;

            /// <summary>
            /// Sequence of the animation.
            /// </summary>
            public ushort PackedSequence;

            /// <summary>
            /// Derived from packedSequence. Sequence of the animation.
            /// </summary>
            public ushort ServerActionSequence { get => (ushort)(PackedSequence & 0x7FFF); }

            /// <summary>
            /// Derived from packedSequence. Whether command is autonomous
            /// </summary>
            public ushort Autonomous { get => (ushort)((PackedSequence >> 15) & 0x1); }

            /// <summary>
            /// Command speed
            /// </summary>
            public float Speed;

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<Command> Commands = new List<Command>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PackedFlags = BinaryHelpers.ReadUInt32(buffer);
            if (((uint)PackedFlags & 0x00000001) != 0) {
                CurrentHoldkey = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)PackedFlags & 0x00000002) != 0) {
                CurrentStyle = (MotionStance)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x00000004) != 0) {
                ForwardCommand = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x0000008) != 0) {
                ForwardHoldkey = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)PackedFlags & 0x00000010) != 0) {
                ForwardSpeed = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)PackedFlags & 0x00000020) != 0) {
                SidestepCommand = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x00000040) != 0) {
                SidestepHoldkey = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)PackedFlags & 0x00000080) != 0) {
                SidestepSpeed = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)PackedFlags & 0x00000100) != 0) {
                TurnCommand = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x00000200) != 0) {
                TurnHoldkey = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)PackedFlags & 0x00000400) != 0) {
                TurnSpeed = BinaryHelpers.ReadSingle(buffer);
            }
            for (var i = 0; i < CommandListLength; i++) {
                var commandID = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
                var packedSequence = BinaryHelpers.ReadUInt16(buffer);
                var speed = BinaryHelpers.ReadSingle(buffer);
                Commands.Add(new Command() {
                    Id = commandID,
                    PackedSequence = packedSequence,
                    Speed = speed,
                });
            }
        }

    }

    /// <summary>
    /// An autonomous position with sequences
    /// </summary>
    public class AutonomousPositionPack : IACDataType {
        /// <summary>
        /// position
        /// </summary>
        public Position Position;

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort ObjectServerControlSequence;

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort ObjectTeleportSequence;

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort ObjectForcePositionSequence;

        /// <summary>
        /// Whether the player has contact with the ground
        /// </summary>
        public byte Contact;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Position = new Position();
            Position.ReadFromBuffer(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectServerControlSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectTeleportSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer);
            Contact = BinaryHelpers.ReadByte(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// A position with sequences
    /// </summary>
    public class PositionPack : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public PositionFlags Flags;

        /// <summary>
        /// the location of the object in the world
        /// </summary>
        public Origin Origin;

        /// <summary>
        /// The objects orientation quaternion W value
        /// </summary>
        public float WQuat;

        /// <summary>
        /// The objects orientation quaternion X value
        /// </summary>
        public float XQuat;

        /// <summary>
        /// The objects orientation quaternion Y value
        /// </summary>
        public float YQuat;

        /// <summary>
        /// The objects orientation quaternion X value
        /// </summary>
        public float ZQuat;

        /// <summary>
        /// The objects current velocity
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// TODO
        /// </summary>
        public uint PlacementId;

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The position sequence value for the object
        /// </summary>
        public ushort ObjectPositionSequence;

        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort ObjectTeleportSequence;

        /// <summary>
        /// The forced position sequence value for the object
        /// </summary>
        public ushort ObjectForcePositionSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = (PositionFlags)BinaryHelpers.ReadUInt32(buffer);
            Origin = new Origin();
            Origin.ReadFromBuffer(buffer);
            if ((((uint)Flags ^ 0x00000078) & 0x00000008) != 0) {
                WQuat = BinaryHelpers.ReadSingle(buffer);
            }
            if ((((uint)Flags ^ 0x00000078) & 0x00000010) != 0) {
                XQuat = BinaryHelpers.ReadSingle(buffer);
            }
            if ((((uint)Flags ^ 0x00000078) & 0x00000020) != 0) {
                YQuat = BinaryHelpers.ReadSingle(buffer);
            }
            if ((((uint)Flags ^ 0x00000078) & 0x00000040) != 0) {
                ZQuat = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00000001) != 0) {
                Velocity = new Vector3();
                Velocity.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                PlacementId = BinaryHelpers.ReadUInt32(buffer);
            }
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectPositionSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectTeleportSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer);
        }

    }

    /// <summary>
    /// Data related to the movement and animation of the object
    /// </summary>
    public class MovementData : IACDataType {
        /// <summary>
        /// The movement sequence value for this object
        /// </summary>
        public ushort ObjectMovementSequence;

        /// <summary>
        /// The server control sequence value for the object
        /// </summary>
        public ushort ObjectServerControlSequence;

        /// <summary>
        /// 0x0 - server controlled, 0x1 - autonomous
        /// </summary>
        public ushort Autonomous;

        /// <summary>
        /// Determines the type of movement that follows
        /// </summary>
        public MovementType MovementType;

        /// <summary>
        /// Options for this movement (sticky, standing long jump)
        /// </summary>
        public MovementOption OptionFlags;

        /// <summary>
        /// Current stance
        /// </summary>
        public MotionStance Stance;

        /// <summary>
        /// Set of motion data
        /// </summary>
        public InterpertedMotionState State;

        /// <summary>
        /// object to stick to
        /// </summary>
        public uint StickyObjectId;

        /// <summary>
        /// The id of target that&#39;s being moved to
        /// </summary>
        public uint TargetId;

        /// <summary>
        /// the location of the target in the world
        /// </summary>
        public Origin Origin;

        /// <summary>
        /// Set of movement parameters
        /// </summary>
        public MoveToMovementParameters MoveToParams;

        /// <summary>
        /// Run speed of the moving object
        /// </summary>
        public float RunRate;

        /// <summary>
        /// Heading of the target to turn to, this is used instead of the desiredHeading in the parameters
        /// </summary>
        public float DesiredHeading;

        /// <summary>
        /// Set of movement parameters
        /// </summary>
        public TurnToMovementParameters TurnToParams;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectMovementSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectServerControlSequence = BinaryHelpers.ReadUInt16(buffer);
            Autonomous = BinaryHelpers.ReadUInt16(buffer);
            MovementType = (MovementType)BinaryHelpers.ReadByte(buffer);
            OptionFlags = (MovementOption)BinaryHelpers.ReadByte(buffer);
            Stance = (MotionStance)BinaryHelpers.ReadUInt16(buffer);
            switch ((int)MovementType) {
                case 0x0000:
                    State = new InterpertedMotionState();
                    State.ReadFromBuffer(buffer);
                    if (((uint)OptionFlags & 0x01) != 0) {
                        StickyObjectId = BinaryHelpers.ReadUInt32(buffer);
                    }
                    break;
                case 0x0006:
                    TargetId = BinaryHelpers.ReadUInt32(buffer);
                    Origin = new Origin();
                    Origin.ReadFromBuffer(buffer);
                    MoveToParams = new MoveToMovementParameters();
                    MoveToParams.ReadFromBuffer(buffer);
                    RunRate = BinaryHelpers.ReadSingle(buffer);
                    break;
                case 0x0007:
                    Origin = new Origin();
                    Origin.ReadFromBuffer(buffer);
                    MoveToParams = new MoveToMovementParameters();
                    MoveToParams.ReadFromBuffer(buffer);
                    RunRate = BinaryHelpers.ReadSingle(buffer);
                    break;
                case 0x0008:
                    TargetId = BinaryHelpers.ReadUInt32(buffer);
                    DesiredHeading = BinaryHelpers.ReadSingle(buffer);
                    TurnToParams = new TurnToMovementParameters();
                    TurnToParams.ReadFromBuffer(buffer);
                    break;
                case 0x0009:
                    TurnToParams = new TurnToMovementParameters();
                    TurnToParams.ReadFromBuffer(buffer);
                    break;
            }
        }

    }

    /// <summary>
    /// Contains information for animations and general free motion
    /// </summary>
    public class InterpertedMotionState : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint PackedFlags;

        /// <summary>
        /// Derived from flags. 
        /// </summary>
        public uint CommandListLength { get => (uint)((PackedFlags >> 7) & 0x7F); }

        /// <summary>
        /// Stance.  If not present, defaults to 0x3D (NonCombat)
        /// </summary>
        public MotionStance CurrentStyle;

        /// <summary>
        /// Command for our forward movement. If not present, defaults to 0x03 (Ready)
        /// </summary>
        public MotionCommand ForwardCommand;

        /// <summary>
        /// Command for our sidestep movememnt. If not present, defaults to 0x00
        /// </summary>
        public MotionCommand SidestepCommand;

        /// <summary>
        /// Command for our turn movememnt. If not present, defaults to 0x00
        /// </summary>
        public MotionCommand TurnCommand;

        /// <summary>
        /// Forward movement speed. If not present, defaults to 1.0
        /// </summary>
        public float ForwardSpeed;

        /// <summary>
        /// Sidestep movement speed. If not present, defaults to 1.0
        /// </summary>
        public float SidestepSpeed;

        /// <summary>
        /// Turn movement speed. If not present, defaults to 1.0
        /// </summary>
        public float TurnSpeed;

        /// <summary>
        /// Vector collection data for the commands vector
        /// </summary>
        public class Command {
            /// <summary>
            /// Command ID
            /// </summary>
            public MotionCommand Id;

            /// <summary>
            /// Sequence of the animation.  Note the MSB appears to be used for autonomous flag.  Therefore the max size of this member would be 1/2 a WORD.
            /// </summary>
            public ushort PackedSequence;

            /// <summary>
            /// Derived from packedSequence. Sequence of the animation.
            /// </summary>
            public ushort ServerActionSequence { get => (ushort)(PackedSequence & 0x7FFF); }

            /// <summary>
            /// Derived from packedSequence. Seems to indicate the commmand is autonomous
            /// </summary>
            public ushort Autonomous { get => (ushort)((PackedSequence >> 15) & 0x1); }

            /// <summary>
            /// Command speed
            /// </summary>
            public float Speed;

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<Command> Commands = new List<Command>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PackedFlags = BinaryHelpers.ReadUInt32(buffer);
            if (((uint)PackedFlags & 0x00000001) != 0) {
                CurrentStyle = (MotionStance)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x00000002) != 0) {
                ForwardCommand = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x00000008) != 0) {
                SidestepCommand = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x00000020) != 0) {
                TurnCommand = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)PackedFlags & 0x00000004) != 0) {
                ForwardSpeed = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)PackedFlags & 0x00000010) != 0) {
                SidestepSpeed = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)PackedFlags & 0x00000040) != 0) {
                TurnSpeed = BinaryHelpers.ReadSingle(buffer);
            }
            for (var i = 0; i < CommandListLength; i++) {
                var commandID = (MotionCommand)BinaryHelpers.ReadUInt16(buffer);
                var packedSequence = BinaryHelpers.ReadUInt16(buffer);
                var speed = BinaryHelpers.ReadSingle(buffer);
                Commands.Add(new Command() {
                    Id = commandID,
                    PackedSequence = packedSequence,
                    Speed = speed,
                });
            }
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Set of movement parameters required for a MoveTo movement
    /// </summary>
    public class MoveToMovementParameters : IACDataType {
        /// <summary>
        /// bitmember of some options related to the motion (TODO needs further research)
        /// </summary>
        public uint Bitmember;

        /// <summary>
        /// The distance to the given location
        /// </summary>
        public float DistanceToObject;

        /// <summary>
        /// The minimum distance required for the movement
        /// </summary>
        public float MinDistance;

        /// <summary>
        /// The distance at which the movement will fail
        /// </summary>
        public float FailDistance;

        /// <summary>
        /// speed of animation
        /// </summary>
        public float AnimationSpeed;

        /// <summary>
        /// The distance from the location which determines whether you walk or run towards it.
        /// </summary>
        public float WalkRunThreshold;

        /// <summary>
        /// Heading the object is turning to
        /// </summary>
        public float DesiredHeading;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Bitmember = BinaryHelpers.ReadUInt32(buffer);
            DistanceToObject = BinaryHelpers.ReadSingle(buffer);
            MinDistance = BinaryHelpers.ReadSingle(buffer);
            FailDistance = BinaryHelpers.ReadSingle(buffer);
            AnimationSpeed = BinaryHelpers.ReadSingle(buffer);
            WalkRunThreshold = BinaryHelpers.ReadSingle(buffer);
            DesiredHeading = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Set of movement parameters required for a TurnTo motion
    /// </summary>
    public class TurnToMovementParameters : IACDataType {
        /// <summary>
        /// bitmember of some options related to the motion (TODO needs further research)
        /// </summary>
        public uint Bitmember;

        /// <summary>
        /// speed of animation
        /// </summary>
        public float AnimationSpeed;

        /// <summary>
        /// Heading the object is turning to
        /// </summary>
        public float DesiredHeading;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Bitmember = BinaryHelpers.ReadUInt32(buffer);
            AnimationSpeed = BinaryHelpers.ReadSingle(buffer);
            DesiredHeading = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// The ObjDesc structure defines an object&#39;s visual appearance.
    /// </summary>
    public class ObjDesc : IACDataType {
        /// <summary>
        /// always 0x11
        /// </summary>
        public byte Version;

        /// <summary>
        /// the number of palettes associated with this object
        /// </summary>
        public byte PaletteCount;

        /// <summary>
        /// the number of textures associated with this object
        /// </summary>
        public byte TextureCount;

        /// <summary>
        /// the number of models associated with this object
        /// </summary>
        public byte ModelCount;

        /// <summary>
        /// palette DataID (minus 0x04000000)
        /// </summary>
        public uint Palette;

        /// <summary>
        /// TODO
        /// </summary>
        public List<Subpalette> Subpalettes = new List<Subpalette>();

        /// <summary>
        /// TODO
        /// </summary>
        public List<TextureMapChange> TextureMapChanges = new List<TextureMapChange>();

        /// <summary>
        /// TODO
        /// </summary>
        public List<AnimPartChange> AnimPartChanges = new List<AnimPartChange>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Version = BinaryHelpers.ReadByte(buffer);
            PaletteCount = BinaryHelpers.ReadByte(buffer);
            TextureCount = BinaryHelpers.ReadByte(buffer);
            ModelCount = BinaryHelpers.ReadByte(buffer);
            if (PaletteCount > 0) {
                Palette = BinaryHelpers.ReadPackedDWORD(buffer);
            }
            for (var i = 0; i < PaletteCount; i++) {
                var subpalette = new Subpalette();
                subpalette.ReadFromBuffer(buffer);
                Subpalettes.Add(subpalette);
            }
            for (var i = 0; i < TextureCount; i++) {
                var tmChange = new TextureMapChange();
                tmChange.ReadFromBuffer(buffer);
                TextureMapChanges.Add(tmChange);
            }
            for (var i = 0; i < ModelCount; i++) {
                var apChange = new AnimPartChange();
                apChange.ReadFromBuffer(buffer);
                AnimPartChanges.Add(apChange);
            }
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Contains data for a subpalette
    /// </summary>
    public class Subpalette : IACDataType {
        /// <summary>
        /// palette DataID (minus 0x04000000)
        /// </summary>
        public uint Palette;

        /// <summary>
        /// The number of palette entries to skip
        /// </summary>
        public byte Offset;

        /// <summary>
        /// The number of palette entries to copy. This is multiplied by 8.  If it is 0, it defaults to 256*8.
        /// </summary>
        public byte NumColors;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Palette = BinaryHelpers.ReadPackedDWORD(buffer);
            Offset = BinaryHelpers.ReadByte(buffer);
            NumColors = BinaryHelpers.ReadByte(buffer);
        }

    }

    /// <summary>
    /// Contains data for texture map changes
    /// </summary>
    public class TextureMapChange : IACDataType {
        /// <summary>
        /// the index of the model we are replacing the texture in
        /// </summary>
        public byte PartIndex;

        /// <summary>
        /// texture DataID (minus 0x05000000)
        /// </summary>
        public uint OldTextureId;

        /// <summary>
        /// texture DataID (minus 0x05000000)
        /// </summary>
        public uint NewTextureId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PartIndex = BinaryHelpers.ReadByte(buffer);
            OldTextureId = BinaryHelpers.ReadPackedDWORD(buffer);
            NewTextureId = BinaryHelpers.ReadPackedDWORD(buffer);
        }

    }

    /// <summary>
    /// Contains data for animation part changes
    /// </summary>
    public class AnimPartChange : IACDataType {
        /// <summary>
        /// The index of the model
        /// </summary>
        public byte PartIndex;

        /// <summary>
        /// model DataID (minus 0x01000000)
        /// </summary>
        public uint PartId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PartIndex = BinaryHelpers.ReadByte(buffer);
            PartId = BinaryHelpers.ReadPackedDWORD(buffer);
        }

    }

    /// <summary>
    /// Data for a character creation
    /// </summary>
    public class CharGenResult : IACDataType {
        /// <summary>
        /// Account name
        /// </summary>
        public string Account;

        /// <summary>
        /// Always 1
        /// </summary>
        public uint One;

        /// <summary>
        /// TODO
        /// </summary>
        public HeritageGroup HeritageGroup;

        /// <summary>
        /// TODO
        /// </summary>
        public Gender Gender;

        /// <summary>
        /// TODO
        /// </summary>
        public uint EyesStrip;

        /// <summary>
        /// TODO
        /// </summary>
        public uint NoseStrip;

        /// <summary>
        /// TODO
        /// </summary>
        public uint MouthStrip;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HairColor;

        /// <summary>
        /// TODO
        /// </summary>
        public uint EyeColor;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HairStyle;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HeadgearStyle;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HeadgearColor;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ShirtStyle;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ShirtColor;

        /// <summary>
        /// TODO
        /// </summary>
        public uint TrousersStyle;

        /// <summary>
        /// TODO
        /// </summary>
        public uint TrousersColor;

        /// <summary>
        /// TODO
        /// </summary>
        public uint FootwearStyle;

        /// <summary>
        /// TODO
        /// </summary>
        public uint FootwearColor;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong SkinShade;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong HairShade;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong HeadgearShade;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong ShirtShade;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong TrousersShade;

        /// <summary>
        /// TODO
        /// </summary>
        public ulong FootwearShade;

        /// <summary>
        /// TODO
        /// </summary>
        public uint TemplateNum;

        /// <summary>
        /// Starting strength
        /// </summary>
        public uint Strength;

        /// <summary>
        /// Starting endurance
        /// </summary>
        public uint Endurance;

        /// <summary>
        /// Starting coordination
        /// </summary>
        public uint Coordination;

        /// <summary>
        /// Starting quickness
        /// </summary>
        public uint Quickness;

        /// <summary>
        /// Starting focus
        /// </summary>
        public uint Focus;

        /// <summary>
        /// Starting self
        /// </summary>
        public uint Self;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Slot;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ClassID;

        /// <summary>
        /// TODO
        /// </summary>
        public uint NumSkills;

        /// <summary>
        /// TODO
        /// </summary>
        public List<SkillState> Skills = new List<SkillState>();

        /// <summary>
        /// Character name
        /// </summary>
        public string Name;

        /// <summary>
        /// TODO
        /// </summary>
        public uint StartArea;

        /// <summary>
        /// TODO
        /// </summary>
        public uint IsAdmin;

        /// <summary>
        /// TODO
        /// </summary>
        public uint IsEnvoy;

        /// <summary>
        /// Seems to be the total of heritageGroup, gender, eyesStrip, noseStrip, mouthStrip, hairColor, eyeColor, hairStyle, headgearStyle, shirtStyle, trousersStyle, footwearStyle, templateNum, strength, endurance, coordination, quickness, focus, self. Perhaps used for some kind of validation?
        /// </summary>
        public uint Validation;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Account = BinaryHelpers.ReadString(buffer);
            One = BinaryHelpers.ReadUInt32(buffer);
            HeritageGroup = (HeritageGroup)BinaryHelpers.ReadByte(buffer);
            Gender = (Gender)BinaryHelpers.ReadByte(buffer);
            EyesStrip = BinaryHelpers.ReadUInt32(buffer);
            NoseStrip = BinaryHelpers.ReadUInt32(buffer);
            MouthStrip = BinaryHelpers.ReadUInt32(buffer);
            HairColor = BinaryHelpers.ReadUInt32(buffer);
            EyeColor = BinaryHelpers.ReadUInt32(buffer);
            HairStyle = BinaryHelpers.ReadUInt32(buffer);
            HeadgearStyle = BinaryHelpers.ReadUInt32(buffer);
            HeadgearColor = BinaryHelpers.ReadUInt32(buffer);
            ShirtStyle = BinaryHelpers.ReadUInt32(buffer);
            ShirtColor = BinaryHelpers.ReadUInt32(buffer);
            TrousersStyle = BinaryHelpers.ReadUInt32(buffer);
            TrousersColor = BinaryHelpers.ReadUInt32(buffer);
            FootwearStyle = BinaryHelpers.ReadUInt32(buffer);
            FootwearColor = BinaryHelpers.ReadUInt32(buffer);
            SkinShade = BinaryHelpers.ReadUInt64(buffer);
            HairShade = BinaryHelpers.ReadUInt64(buffer);
            HeadgearShade = BinaryHelpers.ReadUInt64(buffer);
            ShirtShade = BinaryHelpers.ReadUInt64(buffer);
            TrousersShade = BinaryHelpers.ReadUInt64(buffer);
            FootwearShade = BinaryHelpers.ReadUInt64(buffer);
            TemplateNum = BinaryHelpers.ReadUInt32(buffer);
            Strength = BinaryHelpers.ReadUInt32(buffer);
            Endurance = BinaryHelpers.ReadUInt32(buffer);
            Coordination = BinaryHelpers.ReadUInt32(buffer);
            Quickness = BinaryHelpers.ReadUInt32(buffer);
            Focus = BinaryHelpers.ReadUInt32(buffer);
            Self = BinaryHelpers.ReadUInt32(buffer);
            Slot = BinaryHelpers.ReadUInt32(buffer);
            ClassID = BinaryHelpers.ReadUInt32(buffer);
            NumSkills = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < NumSkills; i++) {
                var skillAdvancementClass = (SkillState)BinaryHelpers.ReadUInt32(buffer);
                Skills.Add(skillAdvancementClass);
            }
            Name = BinaryHelpers.ReadString(buffer);
            StartArea = BinaryHelpers.ReadUInt32(buffer);
            IsAdmin = BinaryHelpers.ReadUInt32(buffer);
            IsEnvoy = BinaryHelpers.ReadUInt32(buffer);
            Validation = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Basic information for a character used at the Login screen
    /// </summary>
    public class CharacterIdentity : IACDataType {
        /// <summary>
        /// The character ID for this entry.
        /// </summary>
        public uint Id;

        /// <summary>
        /// The name of this character.
        /// </summary>
        public string Name;

        /// <summary>
        /// When 0, this character is not being deleted (not shown crossed out). Otherwise, it&#39;s a countdown timer in the number of seconds until the character is submitted for deletion.
        /// </summary>
        public uint SecondsGreyedOut;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            SecondsGreyedOut = BinaryHelpers.ReadUInt32(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// The PhysicsDesc structure defines an object&#39;s physical behavior.
    /// </summary>
    public class PhysicsDesc : IACDataType {
        /// <summary>
        /// physics data flags
        /// </summary>
        public uint Flags;

        /// <summary>
        /// The current physics state for the object
        /// </summary>
        public PhysicsState State;

        /// <summary>
        /// the number of BYTEs in the following movement buffer
        /// </summary>
        public uint MovementByteCount;

        /// <summary>
        /// The movement buffer for this object defining its initial movement state
        /// </summary>
        public List<byte> Bytes = new List<byte>();

        /// <summary>
        /// Whether the movement is autonomous (0 for no, 1 for yes).  NOTE that this is only present if movementByteCount &gt; 0
        /// </summary>
        public bool Autonomous;

        /// <summary>
        /// The current animation frame.  NOTE this can only be present if 0x00010000 is not set
        /// </summary>
        public uint AnimationFrame;

        /// <summary>
        /// object position
        /// </summary>
        public Position Position;

        /// <summary>
        /// motion table id for this object
        /// </summary>
        public uint MotionTableId;

        /// <summary>
        /// sound table id for this object
        /// </summary>
        public uint SoundTableId;

        /// <summary>
        /// physics script table id for this object
        /// </summary>
        public uint PhysicsScriptTableId;

        /// <summary>
        /// setup table id for this object
        /// </summary>
        public uint SetupTableId;

        /// <summary>
        /// the creature equipping this object
        /// </summary>
        public uint ParentId;

        /// <summary>
        /// the slot in which this object is equipped, referenced in the Setup table of the dats
        /// </summary>
        public uint LocationId;

        /// <summary>
        /// the number of items equipped by this creature
        /// </summary>
        public uint NumChildren;

        /// <summary>
        /// Vector collection data for the children vector
        /// </summary>
        public class Child {
            /// <summary>
            /// TODO
            /// </summary>
            public uint objectId;

            /// <summary>
            /// the slot in which this object is equipped, referenced in the Setup table of the dats
            /// </summary>
            public uint locationId;

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<Child> Children = new List<Child>();

        /// <summary>
        /// the size of this object
        /// </summary>
        public float Scale;

        /// <summary>
        /// TODO
        /// </summary>
        public float Friction;

        /// <summary>
        /// TODO
        /// </summary>
        public float Elasticity;

        /// <summary>
        /// TODO
        /// </summary>
        public float Translucency;

        /// <summary>
        /// velocity vector x component
        /// </summary>
        public float VelocityX;

        /// <summary>
        /// velocity vector y component
        /// </summary>
        public float VelocityY;

        /// <summary>
        /// velocity vector z component
        /// </summary>
        public float VelocityZ;

        /// <summary>
        /// acceleration vector x component
        /// </summary>
        public float AccelerationX;

        /// <summary>
        /// acceleration vector y component
        /// </summary>
        public float AccelerationY;

        /// <summary>
        /// acceleration vector z component
        /// </summary>
        public float AccelerationZ;

        /// <summary>
        /// omega vector x component (rotation)
        /// </summary>
        public float OmegaX;

        /// <summary>
        /// omega vector y component (rotation)
        /// </summary>
        public float OmegaY;

        /// <summary>
        /// omega vector z component (rotation)
        /// </summary>
        public float OmegaZ;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultScript;

        /// <summary>
        /// TODO
        /// </summary>
        public float DefaultScriptIntensity;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectPositionSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectMovementSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectStateSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectVectorSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectTeleportSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectServerControlSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectForcePositionSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectVisualDescSequence;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            State = (PhysicsState)BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x00010000) != 0) {
                MovementByteCount = BinaryHelpers.ReadUInt32(buffer);
                if (MovementByteCount > 0) {
                    for (var i = 0; i < MovementByteCount; i++) {
                        var data = BinaryHelpers.ReadByte(buffer);
                        Bytes.Add(data);
                    }
                }
                Autonomous = BinaryHelpers.ReadBool(buffer);
            }
            if (((uint)Flags & 0x00020000) != 0) {
                AnimationFrame = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00008000) != 0) {
                Position = new Position();
                Position.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                MotionTableId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000800) != 0) {
                SoundTableId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00001000) != 0) {
                PhysicsScriptTableId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000001) != 0) {
                SetupTableId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000020) != 0) {
                ParentId = BinaryHelpers.ReadUInt32(buffer);
                LocationId = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                NumChildren = BinaryHelpers.ReadUInt32(buffer);
                for (var i = 0; i < NumChildren; i++) {
                    var itemID = BinaryHelpers.ReadUInt32(buffer);
                    var locationID = BinaryHelpers.ReadUInt32(buffer);
                    Children.Add(new Child() {
                        objectId = itemID,
                        locationId = locationID,
                    });
                }
            }
            if (((uint)Flags & 0x00000080) != 0) {
                Scale = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00000100) != 0) {
                Friction = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00000200) != 0) {
                Elasticity = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00040000) != 0) {
                Translucency = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                VelocityX = BinaryHelpers.ReadSingle(buffer);
                VelocityY = BinaryHelpers.ReadSingle(buffer);
                VelocityZ = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                AccelerationX = BinaryHelpers.ReadSingle(buffer);
                AccelerationY = BinaryHelpers.ReadSingle(buffer);
                AccelerationZ = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00000010) != 0) {
                OmegaX = BinaryHelpers.ReadSingle(buffer);
                OmegaY = BinaryHelpers.ReadSingle(buffer);
                OmegaZ = BinaryHelpers.ReadSingle(buffer);
            }
            if (((uint)Flags & 0x00002000) != 0) {
                DefaultScript = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00004000) != 0) {
                DefaultScriptIntensity = BinaryHelpers.ReadSingle(buffer);
            }
            ObjectPositionSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectMovementSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectStateSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectVectorSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectTeleportSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectServerControlSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectForcePositionSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectVisualDescSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class AdminAccountData : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public string Account;

        /// <summary>
        /// TODO
        /// </summary>
        public uint BookieId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Account = BinaryHelpers.ReadString(buffer);
            BookieId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class AdminPlayerData : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public string Name;

        /// <summary>
        /// TODO
        /// </summary>
        public uint BookieId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            BookieId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class VendorProfile : IACDataType {
        /// <summary>
        /// the categories of items the merchant will buy
        /// </summary>
        public ObjectType ObjectTypes;

        /// <summary>
        /// the lowest value of an item the merchant will buy
        /// </summary>
        public uint MinValue;

        /// <summary>
        /// the highest value of an item the merchant will buy
        /// </summary>
        public uint MaxValue;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Magic;

        /// <summary>
        /// the merchant&#39;s buy rate
        /// </summary>
        public float BuyPrice;

        /// <summary>
        /// the merchant&#39;s sell rate
        /// </summary>
        public float SellPrice;

        /// <summary>
        /// wcid of currency
        /// </summary>
        public uint CurrencyId;

        /// <summary>
        /// amount of currency player has
        /// </summary>
        public uint CurrencyAmount;

        /// <summary>
        /// name of currency
        /// </summary>
        public string CurrencyName;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectTypes = (ObjectType)BinaryHelpers.ReadUInt32(buffer);
            MinValue = BinaryHelpers.ReadUInt32(buffer);
            MaxValue = BinaryHelpers.ReadUInt32(buffer);
            Magic = BinaryHelpers.ReadUInt32(buffer);
            BuyPrice = BinaryHelpers.ReadSingle(buffer);
            SellPrice = BinaryHelpers.ReadSingle(buffer);
            CurrencyId = BinaryHelpers.ReadUInt32(buffer);
            CurrencyAmount = BinaryHelpers.ReadUInt32(buffer);
            CurrencyName = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class ArmorProfile : IACDataType {
        /// <summary>
        /// relative protection against slashing damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtSlashing;

        /// <summary>
        /// relative protection against piercing damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtPiercing;

        /// <summary>
        /// relative protection against bludgeoning damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtBludgeoning;

        /// <summary>
        /// relative protection against cold damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtCold;

        /// <summary>
        /// relative protection against fire damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtFire;

        /// <summary>
        /// relative protection against acid damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtAcid;

        /// <summary>
        /// relative protection against nether damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtNether;

        /// <summary>
        /// relative protection against lightning damage (multiply by AL for actual protection)
        /// </summary>
        public float ProtLightning;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ProtSlashing = BinaryHelpers.ReadSingle(buffer);
            ProtPiercing = BinaryHelpers.ReadSingle(buffer);
            ProtBludgeoning = BinaryHelpers.ReadSingle(buffer);
            ProtCold = BinaryHelpers.ReadSingle(buffer);
            ProtFire = BinaryHelpers.ReadSingle(buffer);
            ProtAcid = BinaryHelpers.ReadSingle(buffer);
            ProtNether = BinaryHelpers.ReadSingle(buffer);
            ProtLightning = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class CreatureAppraisalProfile : IACDataType {
        /// <summary>
        /// These Flags indication which members will be available for assess.
        /// </summary>
        public uint Flags;

        /// <summary>
        /// current health
        /// </summary>
        public uint Health;

        /// <summary>
        /// maximum health
        /// </summary>
        public uint HealthMax;

        /// <summary>
        /// strength
        /// </summary>
        public uint Strength;

        /// <summary>
        /// endurance
        /// </summary>
        public uint Endurance;

        /// <summary>
        /// quickness
        /// </summary>
        public uint Quickness;

        /// <summary>
        /// coordination
        /// </summary>
        public uint Coordination;

        /// <summary>
        /// focus
        /// </summary>
        public uint Focus;

        /// <summary>
        /// self
        /// </summary>
        public uint Self;

        /// <summary>
        /// current stamina
        /// </summary>
        public uint Stamina;

        /// <summary>
        /// current mana
        /// </summary>
        public uint Mana;

        /// <summary>
        /// maximum stamina
        /// </summary>
        public uint StaminaMax;

        /// <summary>
        /// maximum mana
        /// </summary>
        public uint ManaMax;

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public AttributeMask AttrHighlight;

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public AttributeMask AttrColor;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Flags = BinaryHelpers.ReadUInt32(buffer);
            Health = BinaryHelpers.ReadUInt32(buffer);
            HealthMax = BinaryHelpers.ReadUInt32(buffer);
            if (((uint)Flags & 0x00000008) != 0) {
                Strength = BinaryHelpers.ReadUInt32(buffer);
                Endurance = BinaryHelpers.ReadUInt32(buffer);
                Quickness = BinaryHelpers.ReadUInt32(buffer);
                Coordination = BinaryHelpers.ReadUInt32(buffer);
                Focus = BinaryHelpers.ReadUInt32(buffer);
                Self = BinaryHelpers.ReadUInt32(buffer);
                Stamina = BinaryHelpers.ReadUInt32(buffer);
                Mana = BinaryHelpers.ReadUInt32(buffer);
                StaminaMax = BinaryHelpers.ReadUInt32(buffer);
                ManaMax = BinaryHelpers.ReadUInt32(buffer);
            }
            if (((uint)Flags & 0x00000001) != 0) {
                AttrHighlight = (AttributeMask)BinaryHelpers.ReadUInt16(buffer);
                AttrColor = (AttributeMask)BinaryHelpers.ReadUInt16(buffer);
            }
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class WeaponProfile : IACDataType {
        /// <summary>
        /// the type of damage done by the weapon
        /// </summary>
        public DamageType DamageType;

        /// <summary>
        /// the speed of the weapon
        /// </summary>
        public uint Speed;

        /// <summary>
        /// the skill used by the weapon (-1 if none)
        /// </summary>
        public SkillId Skill;

        /// <summary>
        /// the maximum damage done by the weapon
        /// </summary>
        public uint Damage;

        /// <summary>
        /// the maximum damage variance of the weapon
        /// </summary>
        public double Variance;

        /// <summary>
        /// the damage modifier of the weapon
        /// </summary>
        public double DamageMod;

        /// <summary>
        /// TODO
        /// </summary>
        public double Length;

        /// <summary>
        /// the power of the weapon (this affects range)
        /// </summary>
        public double MaxVelocity;

        /// <summary>
        /// the attack skill bonus of the weapon
        /// </summary>
        public double SkillBonus;

        /// <summary>
        /// TODO
        /// </summary>
        public uint MaxVelocityEstimated;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            DamageType = (DamageType)BinaryHelpers.ReadUInt32(buffer);
            Speed = BinaryHelpers.ReadUInt32(buffer);
            Skill = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Damage = BinaryHelpers.ReadUInt32(buffer);
            Variance = BinaryHelpers.ReadDouble(buffer);
            DamageMod = BinaryHelpers.ReadDouble(buffer);
            Length = BinaryHelpers.ReadDouble(buffer);
            MaxVelocity = BinaryHelpers.ReadDouble(buffer);
            SkillBonus = BinaryHelpers.ReadDouble(buffer);
            MaxVelocityEstimated = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class HookAppraisalProfile : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public HookAppraisalFlags Bitfield;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ValidLocations;

        /// <summary>
        /// TODO
        /// </summary>
        public AmmoType AmmoType;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Bitfield = (HookAppraisalFlags)BinaryHelpers.ReadUInt32(buffer);
            ValidLocations = BinaryHelpers.ReadUInt32(buffer);
            AmmoType = (AmmoType)BinaryHelpers.ReadUInt16(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class SquelchDB : IACDataType {
        /// <summary>
        /// Account name and 
        /// </summary>
        public PackableHashTable<string, uint> AccountHash;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<uint, SquelchInfo> CharacterHash;

        /// <summary>
        /// Global squelch information
        /// </summary>
        public SquelchInfo GlobalInfo;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            AccountHash = new PackableHashTable<string, uint>();
            AccountHash.ReadFromBuffer(buffer);
            CharacterHash = new PackableHashTable<uint, SquelchInfo>();
            CharacterHash.ReadFromBuffer(buffer);
            GlobalInfo = new SquelchInfo();
            GlobalInfo.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set of information related to a squelch entry
    /// </summary>
    public class SquelchInfo : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public PackableList<LogTextType> Filters;

        /// <summary>
        /// the name of the squelched player
        /// </summary>
        public string Name;

        /// <summary>
        /// Whether this squelch applies to the entire account
        /// </summary>
        public bool Account;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Filters = new PackableList<LogTextType>();
            Filters.ReadFromBuffer(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            Account = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Set of information related to purchasing a housing
    /// </summary>
    public class HouseProfile : IACDataType {
        /// <summary>
        /// the number associated with this dwelling
        /// </summary>
        public uint Id;

        /// <summary>
        /// the object ID of the the current owner
        /// </summary>
        public uint OwnerId;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Bitmask;

        /// <summary>
        /// the level requirement to purchase this dwelling (-1 if no requirement)
        /// </summary>
        public int MinLevel;

        /// <summary>
        /// TODO
        /// </summary>
        public int MaxLevel;

        /// <summary>
        /// the rank requirement to purchase this dwelling (-1 if no requirement)
        /// </summary>
        public int MinAllegRank;

        /// <summary>
        /// TODO
        /// </summary>
        public int MaxAllegRank;

        /// <summary>
        /// TODO
        /// </summary>
        public bool MaintenanceFree;

        /// <summary>
        /// the type of dwelling (1=cottage, 2=villa, 3=mansion, 4=apartment)
        /// </summary>
        public HouseType Type;

        /// <summary>
        /// the name of the current owner
        /// </summary>
        public string OwnerName;

        /// <summary>
        /// Purchase cost
        /// </summary>
        public PackableList<HousePayment> BuyCost;

        /// <summary>
        /// Rent cost
        /// </summary>
        public PackableList<HousePayment> RentCost;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            OwnerId = BinaryHelpers.ReadUInt32(buffer);
            Bitmask = BinaryHelpers.ReadUInt32(buffer);
            MinLevel = BinaryHelpers.ReadInt32(buffer);
            MaxLevel = BinaryHelpers.ReadInt32(buffer);
            MinAllegRank = BinaryHelpers.ReadInt32(buffer);
            MaxAllegRank = BinaryHelpers.ReadInt32(buffer);
            MaintenanceFree = BinaryHelpers.ReadBool(buffer);
            Type = (HouseType)BinaryHelpers.ReadUInt32(buffer);
            OwnerName = BinaryHelpers.ReadString(buffer);
            BuyCost = new PackableList<HousePayment>();
            BuyCost.ReadFromBuffer(buffer);
            RentCost = new PackableList<HousePayment>();
            RentCost.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// The HousePayment structure contains information about a house purchase or maintenance item.
    /// </summary>
    public class HousePayment : IACDataType {
        /// <summary>
        /// the quantity required
        /// </summary>
        public uint Required;

        /// <summary>
        /// the quantity paid
        /// </summary>
        public uint Paid;

        /// <summary>
        /// the item&#39;s object type
        /// </summary>
        public uint WeenieClassId;

        /// <summary>
        /// the name of this item
        /// </summary>
        public string Name;

        /// <summary>
        /// the plural name of this item (if not specified, use &lt;name&gt; followed by &#39;s&#39; or &#39;es&#39;)
        /// </summary>
        public string PluralName;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Required = BinaryHelpers.ReadUInt32(buffer);
            Paid = BinaryHelpers.ReadUInt32(buffer);
            WeenieClassId = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            PluralName = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Set of information related to owning a housing
    /// </summary>
    public class HouseData : IACDataType {
        /// <summary>
        /// when the house was purchased (Unix timestamp)
        /// </summary>
        public uint BuyTime;

        /// <summary>
        /// when the current maintenance period began (Unix timestamp)
        /// </summary>
        public uint RentTime;

        /// <summary>
        /// the type of house (1=cottage, 2=villa, 3=mansion, 4=apartment)
        /// </summary>
        public HouseType Type;

        /// <summary>
        /// indicates maintenance is free this period, I assume admin controlled
        /// </summary>
        public bool MaintenanceFree;

        /// <summary>
        /// Purchase cost
        /// </summary>
        public PackableList<HousePayment> BuyCost;

        /// <summary>
        /// Rent cost
        /// </summary>
        public PackableList<HousePayment> RentCost;

        /// <summary>
        /// house position
        /// </summary>
        public Position position;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            BuyTime = BinaryHelpers.ReadUInt32(buffer);
            RentTime = BinaryHelpers.ReadUInt32(buffer);
            Type = (HouseType)BinaryHelpers.ReadUInt32(buffer);
            MaintenanceFree = BinaryHelpers.ReadBool(buffer);
            BuyCost = new PackableList<HousePayment>();
            BuyCost.ReadFromBuffer(buffer);
            RentCost = new PackableList<HousePayment>();
            RentCost.ReadFromBuffer(buffer);
            position = new Position();
            position.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set of information related to house access
    /// </summary>
    public class HAR : IACDataType {
        /// <summary>
        /// 0x10000002, seems to be some kind of version. Older version started with bitmask, so starting with 0x10000000 allows them to determine if this is V1 or V2.  The latter half appears to indicate wheither there is a roommate list or not.
        /// </summary>
        public uint Version;

        /// <summary>
        /// 0 is private house, 1 = open to public
        /// </summary>
        public uint Bitmask;

        /// <summary>
        /// populated when any allegiance access is specified
        /// </summary>
        public uint MonarchId;

        /// <summary>
        /// Set of guests with their ID as the key and some additional info for them
        /// </summary>
        public PackableHashTable<uint, GuestInfo> GuestList;

        /// <summary>
        /// List that has the ID&#39;s of your roommates
        /// </summary>
        public PList<uint> RoommateList;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Version = BinaryHelpers.ReadUInt32(buffer);
            Bitmask = BinaryHelpers.ReadUInt32(buffer);
            MonarchId = BinaryHelpers.ReadUInt32(buffer);
            GuestList = new PackableHashTable<uint, GuestInfo>();
            GuestList.ReadFromBuffer(buffer);
            RoommateList = new PList<uint>();
            RoommateList.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set of information related to a house guest
    /// </summary>
    public class GuestInfo : IACDataType {
        /// <summary>
        /// 0 is just access to house, 1 = access to storage
        /// </summary>
        public bool ItemStoragePermission;

        /// <summary>
        /// Name of the guest
        /// </summary>
        public string GuestName;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ItemStoragePermission = BinaryHelpers.ReadBool(buffer);
            GuestName = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Set of information related to a chess game move
    /// </summary>
    public class GameMoveData : IACDataType {
        /// <summary>
        /// Type of move
        /// </summary>
        public int Type;

        /// <summary>
        /// Player making the move
        /// </summary>
        public uint PlayerId;

        /// <summary>
        /// Team making this move
        /// </summary>
        public int Team;

        /// <summary>
        /// ID of piece being moved
        /// </summary>
        public int PieceToMove;

        /// <summary>
        /// TODO
        /// </summary>
        public int YGrid;

        /// <summary>
        /// x position to move the piece
        /// </summary>
        public int XTo;

        /// <summary>
        /// y position to move the piece
        /// </summary>
        public int YTo;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = BinaryHelpers.ReadInt32(buffer);
            PlayerId = BinaryHelpers.ReadUInt32(buffer);
            Team = BinaryHelpers.ReadInt32(buffer);
            switch ((int)Type) {
                case 0x4:
                    PieceToMove = BinaryHelpers.ReadInt32(buffer);
                    YGrid = BinaryHelpers.ReadInt32(buffer);
                    break;
                case 0x5:
                    PieceToMove = BinaryHelpers.ReadInt32(buffer);
                    YGrid = BinaryHelpers.ReadInt32(buffer);
                    XTo = BinaryHelpers.ReadInt32(buffer);
                    YTo = BinaryHelpers.ReadInt32(buffer);
                    break;
                case 0x6:
                    PieceToMove = BinaryHelpers.ReadInt32(buffer);
                    break;
            }
        }

    }

    /// <summary>
    /// Set of information related to a salvage operation
    /// </summary>
    public class SalvageOperationsResultData : IACDataType {
        /// <summary>
        /// Which skill was used for the salvaging action
        /// </summary>
        public SkillId SkillUsed;

        /// <summary>
        /// Set of items that could not be salvaged
        /// </summary>
        public PackableList<uint> NotSalvagable;

        /// <summary>
        /// Set of salvage returned
        /// </summary>
        public PackableList<SalvageResult> SalvageResults;

        /// <summary>
        /// Amount of units due to your Ciandra&#39;s Fortune augmentation
        /// </summary>
        public int AugBonus;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SkillUsed = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            NotSalvagable = new PackableList<uint>();
            NotSalvagable.ReadFromBuffer(buffer);
            SalvageResults = new PackableList<SalvageResult>();
            SalvageResults.ReadFromBuffer(buffer);
            AugBonus = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Set of information related to a salvage of an item
    /// </summary>
    public class SalvageResult : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public MaterialType Material;

        /// <summary>
        /// TODO
        /// </summary>
        public double Workmanship;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Units;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Material = (MaterialType)BinaryHelpers.ReadUInt32(buffer);
            Workmanship = BinaryHelpers.ReadDouble(buffer);
            Units = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set of information for a fellowship
    /// </summary>
    public class Fellowship : IACDataType {
        /// <summary>
        /// Set of fellowship members with their ID as the key and some additional info for them
        /// </summary>
        public PackableHashTable<uint, Fellow> fellowshipTable;

        /// <summary>
        /// the fellowship name
        /// </summary>
        public string Name;

        /// <summary>
        /// the object ID of the fellowship leader
        /// </summary>
        public uint LeaderId;

        /// <summary>
        /// XP sharing: 0=no, 1=yes
        /// </summary>
        public bool ShareExperience;

        /// <summary>
        /// Even XP sharing: 0=no, 1=yes
        /// </summary>
        public bool EventExperienceSplit;

        /// <summary>
        /// Open fellowship: 0=no, 1=yes
        /// </summary>
        public bool Open;

        /// <summary>
        /// Locked fellowship: 0=no, 1=yes
        /// </summary>
        public bool Locked;

        /// <summary>
        /// I suspect this is a list of recently disconnected fellows which can be reinvited, even in locked fellowship
        /// </summary>
        public PackableHashTable<uint, int> FellowsDeparted;

        /// <summary>
        /// Some kind of additional table structure, it is not read by the client.  Appears to be related to some fellowship wide flag data, such as for Colloseum
        /// </summary>
        public ushort UnknownCount;

        /// <summary>
        /// TODO
        /// </summary>
        public ushort UnknownTableSize;

        /// <summary>
        /// Vector collection data for the unknownTable vector
        /// </summary>
        public class Unknown {
            /// <summary>
            /// Identifies the quest/lock, such as AccessColosseum
            /// </summary>
            public string Key;

            /// <summary>
            /// Always 0 in captures so far
            /// </summary>
            public uint Unknown1;

            /// <summary>
            /// Value varies. May be 2 WORDs
            /// </summary>
            public uint Unknown2;

            /// <summary>
            /// Appears to be some kind of timestamp
            /// </summary>
            public uint Unknown3;

            /// <summary>
            /// Value stays constant at least for one occurence
            /// </summary>
            public uint Unknown4;

            /// <summary>
            /// Always 1 in captures so far
            /// </summary>
            public uint Unknown5;

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<Unknown> unknownTable = new List<Unknown>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            fellowshipTable = new PackableHashTable<uint, Fellow>();
            fellowshipTable.ReadFromBuffer(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            LeaderId = BinaryHelpers.ReadUInt32(buffer);
            ShareExperience = BinaryHelpers.ReadBool(buffer);
            EventExperienceSplit = BinaryHelpers.ReadBool(buffer);
            Open = BinaryHelpers.ReadBool(buffer);
            Locked = BinaryHelpers.ReadBool(buffer);
            FellowsDeparted = new PackableHashTable<uint, int>();
            FellowsDeparted.ReadFromBuffer(buffer);
            UnknownCount = BinaryHelpers.ReadUInt16(buffer);
            UnknownTableSize = BinaryHelpers.ReadUInt16(buffer);
            for (var i = 0; i < UnknownCount; i++) {
                var key = BinaryHelpers.ReadString(buffer);
                var unknownValue1 = BinaryHelpers.ReadUInt32(buffer);
                var unknownValue2 = BinaryHelpers.ReadUInt32(buffer);
                var unknownValue3 = BinaryHelpers.ReadUInt32(buffer);
                var unknownValue4 = BinaryHelpers.ReadUInt32(buffer);
                var unknownValue5 = BinaryHelpers.ReadUInt32(buffer);
                unknownTable.Add(new Unknown() {
                    Key = key,
                    Unknown1 = unknownValue1,
                    Unknown2 = unknownValue2,
                    Unknown3 = unknownValue3,
                    Unknown4 = unknownValue4,
                    Unknown5 = unknownValue5,
                });
            }
        }

    }

    /// <summary>
    /// The FellowInfo structure contains information about a fellowship member.
    /// </summary>
    public class Fellow : IACDataType {
        /// <summary>
        /// Perhaps cp stored up before distribution?
        /// </summary>
        public uint ExperienceCached;

        /// <summary>
        /// Perhaps lum stored up before distribution?
        /// </summary>
        public uint LuminanceCached;

        /// <summary>
        /// level of member
        /// </summary>
        public uint Level;

        /// <summary>
        /// Maximum Health
        /// </summary>
        public uint HealthMax;

        /// <summary>
        /// Maximum Stamina
        /// </summary>
        public uint StaminaMax;

        /// <summary>
        /// Maximum Mana
        /// </summary>
        public uint ManaMax;

        /// <summary>
        /// Current Health
        /// </summary>
        public uint Health;

        /// <summary>
        /// Current Stamina
        /// </summary>
        public uint Stamina;

        /// <summary>
        /// Current Mana
        /// </summary>
        public uint Mana;

        /// <summary>
        /// if 0 then noSharePhatLoot, if 16 (0x0010) then sharePhatLoot
        /// </summary>
        public uint ShareLoot;

        /// <summary>
        /// Name of Member
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ExperienceCached = BinaryHelpers.ReadUInt32(buffer);
            LuminanceCached = BinaryHelpers.ReadUInt32(buffer);
            Level = BinaryHelpers.ReadUInt32(buffer);
            HealthMax = BinaryHelpers.ReadUInt32(buffer);
            StaminaMax = BinaryHelpers.ReadUInt32(buffer);
            ManaMax = BinaryHelpers.ReadUInt32(buffer);
            Health = BinaryHelpers.ReadUInt32(buffer);
            Stamina = BinaryHelpers.ReadUInt32(buffer);
            Mana = BinaryHelpers.ReadUInt32(buffer);
            ShareLoot = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Contains information about a contract.
    /// </summary>
    public class ContractTracker : IACDataType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Version;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Id;

        /// <summary>
        /// TODO
        /// </summary>
        public ContractStage Stage;

        /// <summary>
        /// TODO
        /// </summary>
        public long TimeWhenDone;

        /// <summary>
        /// TODO
        /// </summary>
        public long TimeWhenRepeats;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Version = BinaryHelpers.ReadUInt32(buffer);
            Id = BinaryHelpers.ReadUInt32(buffer);
            Stage = (ContractStage)BinaryHelpers.ReadUInt32(buffer);
            TimeWhenDone = BinaryHelpers.ReadInt64(buffer);
            TimeWhenRepeats = BinaryHelpers.ReadInt64(buffer);
        }

    }

    /// <summary>
    /// Contains table of ContractTrackers
    /// </summary>
    public class ContractTrackerTable : IACDataType {
        /// <summary>
        /// Set of contract trackers  with the contractID as the key and some additional info for them
        /// </summary>
        public PackableHashTable<uint, ContractTracker> ContactTrackers;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ContactTrackers = new PackableHashTable<uint, ContractTracker>();
            ContactTrackers.ReadFromBuffer(buffer);
        }

    }

}