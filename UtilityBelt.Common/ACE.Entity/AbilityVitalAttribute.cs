using System;

using UtilityBelt.Common.Enums;

namespace ACE.Entity
{
    public class AbilityVitalAttribute : Attribute
    {
        public AbilityVitalAttribute(Vital vital)
        {
            Vital = vital;
        }

        public Vital Vital { get; }
    }
}
