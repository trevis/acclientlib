using System;

using UtilityBelt.Common.Enums;


namespace ACE.Entity.Models
{
    public static class WeenieExtensions
    {
        // =====================================
        // Get
        // Bool, DID, Float, IID, Int, Int64, String, Position
        // =====================================

        public static bool? GetProperty(this Weenie weenie, BoolId property)
        {
            if (weenie.PropertiesBool == null)
                return null;

            if (weenie.PropertiesBool.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static uint? GetProperty(this Weenie weenie, DataId property)
        {
            if (weenie.PropertiesDID == null)
                return null;

            if (weenie.PropertiesDID.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static double? GetProperty(this Weenie weenie, FloatId property)
        {
            if (weenie.PropertiesFloat == null)
                return null;

            if (weenie.PropertiesFloat.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static uint? GetProperty(this Weenie weenie, InstanceId property)
        {
            if (weenie.PropertiesIID == null)
                return null;

            if (weenie.PropertiesIID.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static int? GetProperty(this Weenie weenie, IntId property)
        {
            if (weenie.PropertiesInt == null)
                return null;

            if (weenie.PropertiesInt.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static long? GetProperty(this Weenie weenie, Int64Id property)
        {
            if (weenie.PropertiesInt64 == null)
                return null;

            if (weenie.PropertiesInt64.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static string GetProperty(this Weenie weenie, StringId property)
        {
            if (weenie.PropertiesString == null)
                return null;

            if (weenie.PropertiesString.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static PropertiesPosition GetProperty(this Weenie weenie, PositionPropertyID property)
        {
            if (weenie.PropertiesPosition == null)
                return null;

            if (weenie.PropertiesPosition.TryGetValue(property, out var value))
                return value;

            return null;
        }

        public static Position GetPosition(this Weenie weenie, PositionPropertyID property)
        {
            if (weenie.PropertiesPosition == null)
                return null;

            if (weenie.PropertiesPosition.TryGetValue(property, out var value))
                return new Position(value.ObjCellId, value.PositionX, value.PositionY, value.PositionZ, value.RotationX, value.RotationY, value.RotationZ, value.RotationW);

            return null;
        }


        // =====================================
        // Utility
        // =====================================

        public static string GetName(this Weenie weenie)
        {
            var name = weenie.GetProperty(StringId.Name);

            return name;
        }

        public static string GetPluralName(this Weenie weenie)
        {
            var pluralName = weenie.GetProperty(StringId.PluralName);

            if (pluralName == null)
                pluralName = weenie.GetProperty(StringId.Name) + "s";

            return pluralName;
        }

        public static ObjectType GetObjectType(this Weenie weenie)
        {
            var ObjectType = weenie.GetProperty(IntId.ObjectType) ?? 0;

            return (ObjectType)ObjectType;
        }

        public static int? GetValue(this Weenie weenie)
        {
            var value = weenie.GetProperty(IntId.Value);

            return value;
        }

        public static bool IsStackable(this Weenie weenie)
        {
            switch (weenie.WeenieType)
            {
                case WeenieType.Stackable:

                case WeenieType.Ammunition:
                case WeenieType.Coin:
                case WeenieType.CraftTool:
                case WeenieType.Food:
                case WeenieType.Gem:
                case WeenieType.Missile:
                case WeenieType.SpellComponent:

                    return true;
            }
            return false;
        }

        public static bool IsStuck(this Weenie weenie)
        {
            return weenie.GetProperty(BoolId.Stuck) ?? false;
        }

        public static bool RequiresBackpackSlotOrIsContainer(this Weenie weenie)
        {
            var requiresBackPackSlot = weenie.GetProperty(BoolId.RequiresBackpackSlot) ?? false;

            return requiresBackPackSlot || weenie.WeenieType == WeenieType.Container;
        }

        public static bool IsVendorService(this Weenie weenie)
        {
            return weenie.GetProperty(BoolId.VendorService) ?? false;
        }

        public static int GetStackUnitEncumbrance(this Weenie weenie)
        {
            if (weenie.IsStackable())
            {
                var stackUnitEncumbrance = weenie.GetProperty(IntId.StackUnitEncumbrance);

                if (stackUnitEncumbrance != null)
                    return stackUnitEncumbrance.Value;
            }
            return weenie.GetProperty(IntId.EncumbranceVal) ?? 0;
        }

        public static int GetMaxStackSize(this Weenie weenie)
        {
            if (weenie.IsStackable())
            {
                var maxStackSize = weenie.GetProperty(IntId.MaxStackSize);

                if (maxStackSize != null)
                    return maxStackSize.Value;
            }
            return 1;
        }
    }
}
