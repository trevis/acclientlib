using System;
using System.Collections.Generic;

using UtilityBelt.Common.Enums;


namespace ACE.Entity.Models
{
    /// <summary>
    /// Only populated collections and dictionaries are initialized.
    /// We do this to conserve memory in ACE.Server
    /// Be sure to check for null first.
    /// </summary>
    public interface IWeenie
    {
        uint WeenieClassId { get; set; }
        WeenieType WeenieType { get; set; }

        IDictionary<BoolId, bool> PropertiesBool { get; set; }
        IDictionary<DataId, uint> PropertiesDID { get; set; }
        IDictionary<FloatId, double> PropertiesFloat { get; set; }
        IDictionary<InstanceId, uint> PropertiesIID { get; set; }
        IDictionary<IntId, int> PropertiesInt { get; set; }
        IDictionary<Int64Id, long> PropertiesInt64 { get; set; }
        IDictionary<StringId, string> PropertiesString { get; set; }

        IDictionary<PositionPropertyID, PropertiesPosition> PropertiesPosition { get; set; }

        IDictionary<int, float /* probability */> PropertiesSpellBook { get; set; }

        IList<PropertiesAnimPart> PropertiesAnimPart { get; set; }
        IList<PropertiesPalette> PropertiesPalette { get; set; }
        IList<PropertiesTextureMap> PropertiesTextureMap { get; set; }

        // Properties for all world objects that typically aren't modified over the original weenie
        ICollection<PropertiesCreateList> PropertiesCreateList { get; set; }
        ICollection<PropertiesEmote> PropertiesEmote { get; set; }
        HashSet<int> PropertiesEventFilter { get; set; }
        IList<PropertiesGenerator> PropertiesGenerator { get; set; } // Using a list per this: https://github.com/ACEmulator/ACE/pull/2616, however, no order is guaranteed for db records

        // Properties for creatures
        IDictionary<AttributeId, PropertiesAttribute> PropertiesAttribute { get; set; }
        IDictionary<VitalId, PropertiesAttribute2nd> PropertiesAttribute2nd { get; set; }
        IDictionary<CombatBodyPart, PropertiesBodyPart> PropertiesBodyPart { get; set; }
        IDictionary<SkillId, PropertiesSkill> PropertiesSkill { get; set; }

        // Properties for books
        PropertiesBook PropertiesBook { get; set; }
        IList<PropertiesBookPageData> PropertiesBookPageData { get; set; }
    }
}
