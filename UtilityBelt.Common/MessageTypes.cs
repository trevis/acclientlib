﻿using System.IO;
using System.Collections.Generic;
using UtilityBelt.Common.Lib;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages;

namespace UtilityBelt.Common.Messages.Types {
    /// <summary>
    /// Allegiance update cancelled
    /// </summary>
    public class Allegiance_AllegianceUpdateAborted_S2C : IACMessageType {
        /// <summary>
        /// Failure type
        /// </summary>
        public StatusMessage Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Display a message in a popup message window.
    /// </summary>
    public class Communication_PopUpString_S2C : IACMessageType {
        /// <summary>
        /// the message text
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Set a single character option.
    /// </summary>
    public class Character_PlayerOptionChangedEvent_C2S : IACMessageType {
        /// <summary>
        /// the option being set
        /// </summary>
        public OptionPropertyID OptionId;

        /// <summary>
        /// the value of the option
        /// </summary>
        public bool Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            OptionId = (OptionPropertyID)BinaryHelpers.ReadInt32(buffer);
            Value = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Starts a melee attack against a target
    /// </summary>
    public class Combat_TargetedMeleeAttack_C2S : IACMessageType {
        /// <summary>
        /// ID of creature being attacked
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Height of the attack
        /// </summary>
        public AttackHeight AttackHeight;

        /// <summary>
        /// Attack power level
        /// </summary>
        public float PowerLevel;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            AttackHeight = (AttackHeight)BinaryHelpers.ReadUInt32(buffer);
            PowerLevel = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Starts a missle attack against a target
    /// </summary>
    public class Combat_TargetedMissileAttack_C2S : IACMessageType {
        /// <summary>
        /// ID of creature being attacked
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Height of the attack
        /// </summary>
        public AttackHeight AttackHeight;

        /// <summary>
        /// Accuracy level
        /// </summary>
        public float AccuracyLevel;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            AttackHeight = (AttackHeight)BinaryHelpers.ReadUInt32(buffer);
            AccuracyLevel = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Set AFK mode.
    /// </summary>
    public class Communication_SetAFKMode_C2S : IACMessageType {
        /// <summary>
        /// Whether the user is AFK
        /// </summary>
        public bool AFK;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            AFK = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Set AFK message.
    /// </summary>
    public class Communication_SetAFKMessage_C2S : IACMessageType {
        /// <summary>
        /// The message text
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Information describing your character.
    /// </summary>
    public class Login_PlayerDescription_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public ACQualities Qualities;

        /// <summary>
        /// TODO
        /// </summary>
        public PlayerModule PlayerModule;

        /// <summary>
        /// Number of items in your main pack
        /// </summary>
        public PackableList<ContentProfile> ContentProfile;

        /// <summary>
        /// Items being equipt.
        /// </summary>
        public PackableList<InventoryPlacement> InventoryPlacement;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Qualities = new ACQualities();
            Qualities.ReadFromBuffer(buffer);
            PlayerModule = new PlayerModule();
            PlayerModule.ReadFromBuffer(buffer);
            ContentProfile = new PackableList<ContentProfile>();
            ContentProfile.ReadFromBuffer(buffer);
            InventoryPlacement = new PackableList<InventoryPlacement>();
            InventoryPlacement.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Talking
    /// </summary>
    public class Communication_Talk_C2S : IACMessageType {
        /// <summary>
        /// The message text
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Removes a friend
    /// </summary>
    public class Social_RemoveFriend_C2S : IACMessageType {
        /// <summary>
        /// The id of the friend to remove
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Adds a friend
    /// </summary>
    public class Social_AddFriend_C2S : IACMessageType {
        /// <summary>
        /// The name of the friend to add
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Store an item in a container.
    /// </summary>
    public class Inventory_PutItemInContainer_C2S : IACMessageType {
        /// <summary>
        /// The item being stored
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The container the item is being stored in
        /// </summary>
        public uint ContainerId;

        /// <summary>
        /// The position in the container where the item is being placed
        /// </summary>
        public uint Location;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ContainerId = BinaryHelpers.ReadUInt32(buffer);
            Location = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Gets and weilds an item.
    /// </summary>
    public class Inventory_GetAndWieldItem_C2S : IACMessageType {
        /// <summary>
        /// The item being equipped
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The position in the container where the item is being placed
        /// </summary>
        public EquipMask Slot;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Slot = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Drop an item.
    /// </summary>
    public class Inventory_DropItem_C2S : IACMessageType {
        /// <summary>
        /// The item being dropped
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Swear allegiance
    /// </summary>
    public class Allegiance_SwearAllegiance_C2S : IACMessageType {
        /// <summary>
        /// The person you are swearing allegiance to
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Break allegiance
    /// </summary>
    public class Allegiance_BreakAllegiance_C2S : IACMessageType {
        /// <summary>
        /// The person you are breaking allegiance from
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Allegiance update request
    /// </summary>
    public class Allegiance_UpdateRequest_C2S : IACMessageType {
        /// <summary>
        /// Whether the UI panel is open
        /// </summary>
        public bool On;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            On = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Returns info related to your monarch, patron and vassals.
    /// </summary>
    public class Allegiance_AllegianceUpdate_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Rank;

        /// <summary>
        /// TODO
        /// </summary>
        public AllegianceProfile Profile;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Rank = BinaryHelpers.ReadUInt32(buffer);
            Profile = new AllegianceProfile();
            Profile.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Friends list update
    /// </summary>
    public class Social_FriendsUpdate_S2C : IACMessageType {
        /// <summary>
        /// The set of friend data for the player
        /// </summary>
        public FriendData Friend;

        /// <summary>
        /// The type of the update
        /// </summary>
        public FriendsUpdateType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Friend = new FriendData();
            Friend.ReadFromBuffer(buffer);
            Type = (FriendsUpdateType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Store an item in a container.
    /// </summary>
    public class Item_ServerSaysContainID_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the item being stored
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// the object ID of the container the item is being stored in
        /// </summary>
        public uint ContainerId;

        /// <summary>
        /// the item slot within the container where the item is being placed (0-based)
        /// </summary>
        public uint Slot;

        /// <summary>
        /// the type of item being stored (pack, foci or regular item)
        /// </summary>
        public ContainerProperties ContainerProperties;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ContainerId = BinaryHelpers.ReadUInt32(buffer);
            Slot = BinaryHelpers.ReadUInt32(buffer);
            ContainerProperties = (ContainerProperties)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Equip an item.
    /// </summary>
    public class Item_WearItem_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the item being equipped
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// the slot(s) the item uses
        /// </summary>
        public EquipMask Slot;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Slot = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sent every time an object you are aware of ceases to exist. Merely running out of range does not generate this message - in that case, the client just automatically stops tracking it after receiving no updates for a while (which I presume is a very short while).
    /// </summary>
    public class Item_ServerSaysRemove_S2C : IACMessageType {
        /// <summary>
        /// The object that ceases to exist.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Clears friend list
    /// </summary>
    public class Social_ClearFriends_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Teleport to the PKLite Arena
    /// </summary>
    public class Character_TeleToPKLArena_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Teleport to the PK Arena
    /// </summary>
    public class Character_TeleToPKArena_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Titles for the current character.
    /// </summary>
    public class Social_CharacterTitleTable_S2C : IACMessageType {
        /// <summary>
        /// the title ID of the currently active title
        /// </summary>
        public uint DisplayTitle;

        /// <summary>
        /// Titles character currently has.
        /// </summary>
        public PList<uint> Titles;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            var throwaway = BinaryHelpers.ReadUInt32(buffer); // what is this?
            DisplayTitle = BinaryHelpers.ReadUInt32(buffer);
            Titles = new PList<uint>();
            Titles.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set a title for the current character.
    /// </summary>
    public class Social_AddOrSetCharacterTitle_S2C : IACMessageType {
        /// <summary>
        /// the title ID of the new title
        /// </summary>
        public uint NewTitle;

        /// <summary>
        /// true if the title should be made the current title, false if it should just be added to the title list
        /// </summary>
        public bool SetAsDisplayTitle;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            NewTitle = BinaryHelpers.ReadUInt32(buffer);
            SetAsDisplayTitle = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Sets a character&#39;s display title
    /// </summary>
    public class Social_SetDisplayCharacterTitle_C2S : IACMessageType {
        /// <summary>
        /// Title id
        /// </summary>
        public uint TitleId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            TitleId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Query the allegiance name
    /// </summary>
    public class Allegiance_QueryAllegianceName_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clears the allegiance name
    /// </summary>
    public class Allegiance_ClearAllegianceName_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Direct message by ID
    /// </summary>
    public class Communication_TalkDirect_C2S : IACMessageType {
        /// <summary>
        /// The message text
        /// </summary>
        public string Message;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sets the allegiance name
    /// </summary>
    public class Allegiance_SetAllegianceName_C2S : IACMessageType {
        /// <summary>
        /// The new allegiance name
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Attempt to use an item with a target.
    /// </summary>
    public class Inventory_UseWithTargetEvent_C2S : IACMessageType {
        /// <summary>
        /// The item being used
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The target
        /// </summary>
        public uint TargetId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            TargetId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Attempt to use an item.
    /// </summary>
    public class Inventory_UseEvent_C2S : IACMessageType {
        /// <summary>
        /// The item being used
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sets an allegiance officer
    /// </summary>
    public class Allegiance_SetAllegianceOfficer_C2S : IACMessageType {
        /// <summary>
        /// The allegiance officer&#39;s name
        /// </summary>
        public string Name;

        /// <summary>
        /// Level of the officer
        /// </summary>
        public AllegianceOfficerLevel Level;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            Level = (AllegianceOfficerLevel)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sets an allegiance officer title for a given level
    /// </summary>
    public class Allegiance_SetAllegianceOfficerTitle_C2S : IACMessageType {
        /// <summary>
        /// The allegiance officer level
        /// </summary>
        public AllegianceOfficerLevel Level;

        /// <summary>
        /// The new title for officers at that level
        /// </summary>
        public string Title;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Level = (AllegianceOfficerLevel)BinaryHelpers.ReadUInt32(buffer);
            Title = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// List the allegiance officer titles
    /// </summary>
    public class Allegiance_ListAllegianceOfficerTitles_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clears the allegiance officer titles
    /// </summary>
    public class Allegiance_ClearAllegianceOfficerTitles_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Perform the allegiance lock action
    /// </summary>
    public class Allegiance_DoAllegianceLockAction_C2S : IACMessageType {
        /// <summary>
        /// Allegiance housing action to take
        /// </summary>
        public AllegianceLockAction Action;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Action = (AllegianceLockAction)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sets a person as an approved vassal
    /// </summary>
    public class Allegiance_SetAllegianceApprovedVassal_C2S : IACMessageType {
        /// <summary>
        /// Player who is being approved as a vassal
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Gags a person in allegiance chat
    /// </summary>
    public class Allegiance_AllegianceChatGag_C2S : IACMessageType {
        /// <summary>
        /// Player who is being gagged or ungagged
        /// </summary>
        public string Name;

        /// <summary>
        /// Whether the gag is on
        /// </summary>
        public bool Gag;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            Gag = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Perform the allegiance house action
    /// </summary>
    public class Allegiance_DoAllegianceHouseAction_C2S : IACMessageType {
        /// <summary>
        /// Allegiance housing action to take
        /// </summary>
        public AllegianceHouseAction Action;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Action = (AllegianceHouseAction)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Spend XP to raise a vital.
    /// </summary>
    public class Train_TrainAttribute2nd_C2S : IACMessageType {
        /// <summary>
        /// The ID of the vital
        /// </summary>
        public VitalId Type;

        /// <summary>
        /// The amount of XP being spent
        /// </summary>
        public uint Experience;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (VitalId)BinaryHelpers.ReadUInt32(buffer);
            Experience = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Spend XP to raise an attribute.
    /// </summary>
    public class Train_TrainAttribute_C2S : IACMessageType {
        /// <summary>
        /// The ID of the attribute
        /// </summary>
        public AttributeId Type;

        /// <summary>
        /// The amount of XP being spent
        /// </summary>
        public uint Experience;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (AttributeId)BinaryHelpers.ReadUInt32(buffer);
            Experience = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Spend XP to raise a skill.
    /// </summary>
    public class Train_TrainSkill_C2S : IACMessageType {
        /// <summary>
        /// The ID of the skill
        /// </summary>
        public SkillId Type;

        /// <summary>
        /// The amount of XP being spent
        /// </summary>
        public uint Experience;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Experience = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Spend skill credits to train a skill.
    /// </summary>
    public class Train_TrainSkillAdvancementClass_C2S : IACMessageType {
        /// <summary>
        /// The ID of the skill
        /// </summary>
        public SkillId Type;

        /// <summary>
        /// The number of skill credits being spent
        /// </summary>
        public uint Credits;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Credits = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Cast a spell with no target.
    /// </summary>
    public class Magic_CastUntargetedSpell_C2S : IACMessageType {
        /// <summary>
        /// The ID of the spell
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Cast a spell on a target
    /// </summary>
    public class Magic_CastTargetedSpell_C2S : IACMessageType {
        /// <summary>
        /// The target of the spell
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The ID of the spell
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Close Container - Only sent when explicitly closed
    /// </summary>
    public class Item_StopViewingObjectContents_S2C : IACMessageType {
        /// <summary>
        /// Chest or corpse being closed
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Changes the combat mode
    /// </summary>
    public class Combat_ChangeCombatMode_C2S : IACMessageType {
        /// <summary>
        /// New combat mode for player
        /// </summary>
        public CombatMode Mode;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Mode = (CombatMode)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Merges one stack with another
    /// </summary>
    public class Inventory_StackableMerge_C2S : IACMessageType {
        /// <summary>
        /// ID of object where items are being taken from
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// ID of object where items are being merged into
        /// </summary>
        public uint TargetId;

        /// <summary>
        /// Number of items from the source stack to be added to the destination stack
        /// </summary>
        public uint Amount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            TargetId = BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Split a stack and place it into a container
    /// </summary>
    public class Inventory_StackableSplitToContainer_C2S : IACMessageType {
        /// <summary>
        /// ID of object where items are being taken from
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// ID of container where items are being moved to
        /// </summary>
        public uint ContainerId;

        /// <summary>
        /// Slot in container where items are being moved to
        /// </summary>
        public uint Slot;

        /// <summary>
        /// Number of items from the stack to placed into the container
        /// </summary>
        public uint Amount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ContainerId = BinaryHelpers.ReadUInt32(buffer);
            Slot = BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Split a stack and place it into the world
    /// </summary>
    public class Inventory_StackableSplitTo3D_C2S : IACMessageType {
        /// <summary>
        /// ID of object where items are being taken from
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Number of items from the stack to placed into the world
        /// </summary>
        public uint Amount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Changes an account squelch
    /// </summary>
    public class Communication_ModifyCharacterSquelch_C2S : IACMessageType {
        /// <summary>
        /// 0 = unsquelch, 1 = squelch
        /// </summary>
        public bool Add;

        /// <summary>
        /// The character id who&#39;s acount should be squelched
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The character&#39;s name who&#39;s acount should be squelched
        /// </summary>
        public string Name;

        /// <summary>
        /// The message type to squelch or unsquelch
        /// </summary>
        public ChatMessageType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Add = BinaryHelpers.ReadBool(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            Type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Changes an account squelch
    /// </summary>
    public class Communication_ModifyAccountSquelch_C2S : IACMessageType {
        /// <summary>
        /// 0 = unsquelch, 1 = squelch
        /// </summary>
        public bool Add;

        /// <summary>
        /// The character who&#39;s acount should be squelched
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Add = BinaryHelpers.ReadBool(buffer);
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Changes the global filters, /filter -type as well as /chat and /notell
    /// </summary>
    public class Communication_ModifyGlobalSquelch_C2S : IACMessageType {
        /// <summary>
        /// 0 = unsquelch, 1 = squelch
        /// </summary>
        public bool Add;

        /// <summary>
        /// The message type to squelch or unsquelch
        /// </summary>
        public ChatMessageType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Add = BinaryHelpers.ReadBool(buffer);
            Type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Direct message by name
    /// </summary>
    public class Communication_TalkDirectByName_C2S : IACMessageType {
        /// <summary>
        /// The message text
        /// </summary>
        public string Message;

        /// <summary>
        /// Name of person you are sending a message to
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Buy from a vendor
    /// </summary>
    public class Vendor_Buy_C2S : IACMessageType {
        /// <summary>
        /// ID of vendor being bought from
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Items being purchased from the vendor
        /// </summary>
        public PackableList<ItemProfile> Items;

        /// <summary>
        /// the type of alternate currency being used
        /// </summary>
        public uint CurrencyId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Items = new PackableList<ItemProfile>();
            Items.ReadFromBuffer(buffer);
            CurrencyId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sell to a vendor
    /// </summary>
    public class Vendor_Sell_C2S : IACMessageType {
        /// <summary>
        /// ID of vendor being sold to
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Items being sold to the vendor
        /// </summary>
        public PackableList<ItemProfile> Items;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Items = new PackableList<ItemProfile>();
            Items.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Open the buy/sell panel for a merchant.
    /// </summary>
    public class Vendor_VendorInfo_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the merchant
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// the object ID of the merchant
        /// </summary>
        public VendorProfile Profile;

        /// <summary>
        /// Items available from the vendor
        /// </summary>
        public PackableList<ItemProfile> Items;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Profile = new VendorProfile();
            Profile.ReadFromBuffer(buffer);
            Items = new PackableList<ItemProfile>();
            Items.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Teleport to your lifestone. (/lifestone)
    /// </summary>
    public class Character_TeleToLifestone_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Opens barber UI
    /// </summary>
    public class Character_StartBarber_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint BasePalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HeadObject;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HeadTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultHeadTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint EyesTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultEyesTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint NoseTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultNoseTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint MouthTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultMouthTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint SkinPalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HairPalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint EyesPalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint SetupId;

        /// <summary>
        /// Specifies the toggle option for some races, such as floating empyrean or flaming head on undead
        /// </summary>
        public int Option1;

        /// <summary>
        /// Seems to be unused
        /// </summary>
        public int Option2;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            BasePalette = BinaryHelpers.ReadPackedDWORD(buffer);
            HeadObject = BinaryHelpers.ReadPackedDWORD(buffer);
            HeadTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultHeadTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            EyesTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultEyesTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            NoseTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultNoseTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            MouthTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultMouthTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            SkinPalette = BinaryHelpers.ReadPackedDWORD(buffer);
            HairPalette = BinaryHelpers.ReadPackedDWORD(buffer);
            EyesPalette = BinaryHelpers.ReadPackedDWORD(buffer);
            SetupId = BinaryHelpers.ReadPackedDWORD(buffer);
            Option1 = BinaryHelpers.ReadInt32(buffer);
            Option2 = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Failure to give an item
    /// </summary>
    public class Character_ServerSaysAttemptFailed_S2C : IACMessageType {
        /// <summary>
        /// Item that could not be given
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Failure reason code
        /// </summary>
        public StatusMessage Reason;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Reason = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// The client is ready for the character to materialize after portalling or logging on.
    /// </summary>
    public class Character_LoginCompleteNotification_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Create a fellowship
    /// </summary>
    public class Fellowship_Create_C2S : IACMessageType {
        /// <summary>
        /// Name of the fellowship
        /// </summary>
        public string Name;

        /// <summary>
        /// Whether fellowship shares xp
        /// </summary>
        public bool ShareExperience;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            ShareExperience = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Member left fellowship
    /// </summary>
    public class Fellowship_Quit_S2C : IACMessageType {
        /// <summary>
        /// Whether to disband the fellowship while quiting
        /// </summary>
        public bool Disband;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Disband = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Quit the fellowship
    /// </summary>
    public class Fellowship_Quit_C2S : IACMessageType {
        /// <summary>
        /// Whether to disband the fellowship while quiting
        /// </summary>
        public bool Disband;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Disband = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Member dismissed from fellowship
    /// </summary>
    public class Fellowship_Dismiss_S2C : IACMessageType {
        /// <summary>
        /// ID of player being dismissed from the fellowship
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Dismiss a player from the fellowship
    /// </summary>
    public class Fellowship_Dismiss_C2S : IACMessageType {
        /// <summary>
        /// ID of player being dismissed from the fellowship
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Recruit a player to the fellowship
    /// </summary>
    public class Fellowship_Recruit_C2S : IACMessageType {
        /// <summary>
        /// ID of player being recruited to the fellowship
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Update request
    /// </summary>
    public class Fellowship_UpdateRequest_C2S : IACMessageType {
        /// <summary>
        /// Whether the fellowship UI is visible
        /// </summary>
        public bool On;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            On = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Request update to book data (seems to be sent after failed add page)
    /// </summary>
    public class Writing_BookAddPage_C2S : IACMessageType {
        /// <summary>
        /// Object id of book
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Updates a page in a book
    /// </summary>
    public class Writing_BookModifyPage_C2S : IACMessageType {
        /// <summary>
        /// ID of book
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Number of page being updated
        /// </summary>
        public int Page;

        /// <summary>
        /// New text for the page
        /// </summary>
        public string Text;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Page = BinaryHelpers.ReadInt32(buffer);
            Text = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Add a page to a book
    /// </summary>
    public class Writing_BookData_C2S : IACMessageType {
        /// <summary>
        /// ID of book to add a page to
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Removes a page from a book
    /// </summary>
    public class Writing_BookDeletePage_C2S : IACMessageType {
        /// <summary>
        /// ID of book to remove a page from
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Number of page to remove
        /// </summary>
        public int Page;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Page = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Requests data for a page in a book
    /// </summary>
    public class Writing_BookPageData_C2S : IACMessageType {
        /// <summary>
        /// ID of book
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Number of page to get data for
        /// </summary>
        public int Page;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Page = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Sent when you first open a book, contains the entire table of contents.
    /// </summary>
    public class Writing_BookOpen_S2C : IACMessageType {
        /// <summary>
        /// The readable object you have just opened.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The total number of pages in the book.
        /// </summary>
        public uint MaxNumPages;

        /// <summary>
        /// The set of page data
        /// </summary>
        public PageDataList PageData;

        /// <summary>
        /// The inscription comment and the book title.
        /// </summary>
        public string Inscription;

        /// <summary>
        /// The author of the inscription (and not coincidentally, the book title).
        /// </summary>
        public uint ScribeId;

        /// <summary>
        /// The name of the inscription author.
        /// </summary>
        public string ScribeName;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            MaxNumPages = BinaryHelpers.ReadUInt32(buffer);
            PageData = new PageDataList();
            PageData.ReadFromBuffer(buffer);
            Inscription = BinaryHelpers.ReadString(buffer);
            ScribeId = BinaryHelpers.ReadUInt32(buffer);
            ScribeName = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Response to an attempt to add a page to a book.
    /// </summary>
    public class Writing_BookAddPageResponse_S2C : IACMessageType {
        /// <summary>
        /// The readable object being changed.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The number the of page being added in the book.
        /// </summary>
        public uint Page;

        /// <summary>
        /// Whether the request was successful
        /// </summary>
        public bool Success;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Page = BinaryHelpers.ReadUInt32(buffer);
            Success = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Response to an attempt to delete a page from a book.
    /// </summary>
    public class Writing_BookDeletePageResponse_S2C : IACMessageType {
        /// <summary>
        /// The readable object being changed.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The number the of page being deleted in the book.
        /// </summary>
        public uint Page;

        /// <summary>
        /// Whether the request was successful
        /// </summary>
        public bool Success;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Page = BinaryHelpers.ReadUInt32(buffer);
            Success = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Contains the text of a single page of a book, parchment or sign.
    /// </summary>
    public class Writing_BookPageDataResponse_S2C : IACMessageType {
        /// <summary>
        /// The object id for the readable object.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The 0-based index of the page you are currently viewing.
        /// </summary>
        public uint Page;

        /// <summary>
        /// TODO
        /// </summary>
        public PageData PageData;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Page = BinaryHelpers.ReadUInt32(buffer);
            PageData = new PageData();
            PageData.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Sets the inscription on an object
    /// </summary>
    public class Writing_SetInscription_C2S : IACMessageType {
        /// <summary>
        /// ID of object being inscribed
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Inscription text
        /// </summary>
        public string Inscription;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Inscription = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Get Inscription Response, doesn&#39;t seem to be really used by client
    /// </summary>
    public class Item_GetInscriptionResponse_S2C : IACMessageType {
        /// <summary>
        /// The inscription comment
        /// </summary>
        public string Inscription;

        /// <summary>
        /// The name of the inscription author.
        /// </summary>
        public string ScribeName;

        /// <summary>
        /// TODO
        /// </summary>
        public string ScribeAccount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Inscription = BinaryHelpers.ReadString(buffer);
            ScribeName = BinaryHelpers.ReadString(buffer);
            ScribeAccount = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Appraise something
    /// </summary>
    public class Item_Appraise_C2S : IACMessageType {
        /// <summary>
        /// The object being appraised
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// The result of an attempt to assess an item or creature.
    /// </summary>
    public class Item_SetAppraiseInfo_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the item or creature being assessed
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Flags;

        /// <summary>
        /// assessment successful: 0=no, 1=yes
        /// </summary>
        public bool Success;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<IntId, int> IntProperties;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<Int64Id, long> Int64Properties;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<BoolId, bool> BoolProperties;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<FloatId, double> FloatProperties;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<StringId, string> StringProperties;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableHashTable<DataId, uint> DataProperties;

        /// <summary>
        /// TODO
        /// </summary>
        public PSmartArray<LayeredSpellId> SpellBook;

        /// <summary>
        /// TODO
        /// </summary>
        public ArmorProfile ArmorProfile;

        /// <summary>
        /// TODO
        /// </summary>
        public CreatureAppraisalProfile CreatureProfile;

        /// <summary>
        /// TODO
        /// </summary>
        public WeaponProfile WeaponProfile;

        /// <summary>
        /// TODO
        /// </summary>
        public HookAppraisalProfile HookAppraisalProfile;

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public ArmorHighlightMask ProtHighlight;

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public ArmorHighlightMask ProtColor;

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public WeaponHighlightMask WeapHighlight;

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public WeaponHighlightMask WeapColor;

        /// <summary>
        /// highlight enable bitmask: 0=no, 1=yes
        /// </summary>
        public ResistHighlightMask ResistHighlight;

        /// <summary>
        /// highlight color bitmask: 0=red, 1=green
        /// </summary>
        public ResistHighlightMask ResistColor;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorHead;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorChest;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorGroin;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorBicep;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorWrist;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorHand;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorThigh;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorShin;

        /// <summary>
        /// Armor level
        /// </summary>
        public uint BaseArmorFoot;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Flags = BinaryHelpers.ReadUInt32(buffer);
            Success = BinaryHelpers.ReadBool(buffer);
            if (((uint)Flags & 0x00000001) != 0) {
                IntProperties = new PackableHashTable<IntId, int>();
                IntProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00002000) != 0) {
                Int64Properties = new PackableHashTable<Int64Id, long>();
                Int64Properties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000002) != 0) {
                BoolProperties = new PackableHashTable<BoolId, bool>();
                BoolProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000004) != 0) {
                FloatProperties = new PackableHashTable<FloatId, double>();
                FloatProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000008) != 0) {
                StringProperties = new PackableHashTable<StringId, string>();
                StringProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00001000) != 0) {
                DataProperties = new PackableHashTable<DataId, uint>();
                DataProperties.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000010) != 0) {
                SpellBook = new PSmartArray<LayeredSpellId>();
                SpellBook.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000080) != 0) {
                ArmorProfile = new ArmorProfile();
                ArmorProfile.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000100) != 0) {
                CreatureProfile = new CreatureAppraisalProfile();
                CreatureProfile.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000020) != 0) {
                WeaponProfile = new WeaponProfile();
                WeaponProfile.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000040) != 0) {
                HookAppraisalProfile = new HookAppraisalProfile();
                HookAppraisalProfile.ReadFromBuffer(buffer);
            }
            if (((uint)Flags & 0x00000200) != 0) {
                ProtHighlight = (ArmorHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                ProtColor = (ArmorHighlightMask)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000800) != 0) {
                WeapHighlight = (WeaponHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                WeapColor = (WeaponHighlightMask)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00000400) != 0) {
                ResistHighlight = (ResistHighlightMask)BinaryHelpers.ReadUInt16(buffer);
                ResistColor = (ResistHighlightMask)BinaryHelpers.ReadUInt16(buffer);
            }
            if (((uint)Flags & 0x00004000) != 0) {
                BaseArmorHead = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorChest = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorGroin = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorBicep = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorWrist = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorHand = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorThigh = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorShin = BinaryHelpers.ReadUInt32(buffer);
                BaseArmorFoot = BinaryHelpers.ReadUInt32(buffer);
            }
        }

    }

    /// <summary>
    /// Give an item to someone.
    /// </summary>
    public class Inventory_GiveObjectRequest_C2S : IACMessageType {
        /// <summary>
        /// The recipient of the item
        /// </summary>
        public uint TargetId;

        /// <summary>
        /// The item being given
        /// </summary>
        public uint ObjectID;

        /// <summary>
        /// The amount from a stack being given
        /// </summary>
        public uint Amount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            TargetId = BinaryHelpers.ReadUInt32(buffer);
            ObjectID = BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Advocate Teleport
    /// </summary>
    public class Advocate_Teleport_C2S : IACMessageType {
        /// <summary>
        /// Person being teleported
        /// </summary>
        public string Name;

        /// <summary>
        /// Destination to teleport target to
        /// </summary>
        public Position Destination;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            Destination = new Position();
            Destination.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Sends an abuse report.
    /// </summary>
    public class Character_AbuseLogRequest_C2S : IACMessageType {
        /// <summary>
        /// Name of character
        /// </summary>
        public string Name;

        /// <summary>
        /// 1
        /// </summary>
        public uint Status;

        /// <summary>
        /// Description of complaint
        /// </summary>
        public string Complaint;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            Status = BinaryHelpers.ReadUInt32(buffer);
            Complaint = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Joins a chat channel
    /// </summary>
    public class Communication_AddToChannel_C2S : IACMessageType {
        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint ChannelId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ChannelId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Leaves a chat channel
    /// </summary>
    public class Communication_RemoveFromChannel_C2S : IACMessageType {
        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint ChannelId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ChannelId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// ChannelBroadcast: Group Chat
    /// </summary>
    public class Communication_ChannelBroadcast_C2S : IACMessageType {
        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint ChannelId;

        /// <summary>
        /// Message being sent
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ChannelId = BinaryHelpers.ReadUInt32(buffer);
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Sends a message to a chat channel
    /// </summary>
    public class Communication_ChannelBroadcast_S2C : IACMessageType {
        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint ChannelId;

        /// <summary>
        /// the name of the player sending the message
        /// </summary>
        public string Name;

        /// <summary>
        /// Message being sent
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ChannelId = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// ChannelList: Provides list of characters listening to a channel, I assume in response to a command
    /// </summary>
    public class Communication_ChannelList_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public PackableList<string> Characters;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Characters = new PackableList<string>();
            Characters.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class Communication_ChannelList_C2S : IACMessageType {
        /// <summary>
        /// Channel id, TODO get enum
        /// </summary>
        public uint ChannelId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ChannelId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// ChannelIndex: Provides list of channels available to the player, I assume in response to a command
    /// </summary>
    public class Communication_ChannelIndex_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public PackableList<string> Channels;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Channels = new PackableList<string>();
            Channels.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Requests a channel index
    /// </summary>
    public class Communication_ChannelIndex_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Stop viewing the contents of a container
    /// </summary>
    public class Inventory_NoLongerViewingContents_C2S : IACMessageType {
        /// <summary>
        /// ID of the container
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set Pack Contents
    /// </summary>
    public class Item_OnViewContents_S2C : IACMessageType {
        /// <summary>
        /// The pack we are setting the contents of. This pack objects and the contained objects may be created before or after the message.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public PackableList<ContentProfile> Items;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Items = new PackableList<ContentProfile>();
            Items.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// For stackable items, this changes the number of items in the stack.
    /// </summary>
    public class Item_UpdateStackSize_S2C : IACMessageType {
        /// <summary>
        /// Sequence number for this message
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Item getting it&#39;s stack adjusted.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// New number of items in the stack.
        /// </summary>
        public uint Amount;

        /// <summary>
        /// New value for the item.
        /// </summary>
        public uint NewValue;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadUInt32(buffer);
            NewValue = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// ServerSaysMoveItem: Removes an item from inventory (when you place it on the ground or give it away)
    /// </summary>
    public class Item_ServerSaysMoveItem_S2C : IACMessageType {
        /// <summary>
        /// The item leaving your inventory.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Splits an item to a wield location.
    /// </summary>
    public class Inventory_StackableSplitToWield_C2S : IACMessageType {
        /// <summary>
        /// ID of object being split
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Equip location to place the stack
        /// </summary>
        public EquipMask Location;

        /// <summary>
        /// Amount of stack being split
        /// </summary>
        public int Amount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Location = (EquipMask)BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Add an item to the shortcut bar.
    /// </summary>
    public class Character_AddShortCut_C2S : IACMessageType {
        /// <summary>
        /// Shortcut information
        /// </summary>
        public ShortCutData Shortcut;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Shortcut = new ShortCutData();
            Shortcut.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Remove an item from the shortcut bar.
    /// </summary>
    public class Character_RemoveShortCut_C2S : IACMessageType {
        /// <summary>
        /// Position on the shortcut bar (0-8) of the item to be removed
        /// </summary>
        public uint Index;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Index = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// A Player Kill occurred nearby (also sent for suicides).
    /// </summary>
    public class Combat_HandlePlayerDeathEvent_S2C : IACMessageType {
        /// <summary>
        /// The death message
        /// </summary>
        public string Message;

        /// <summary>
        /// The ID of the departed.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The ID of the character doing the killing.
        /// </summary>
        public uint KillerId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            KillerId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set multiple character options.
    /// </summary>
    public class Character_CharacterOptionsEvent_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public PlayerModule options;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            options = new PlayerModule();
            options.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// HandleAttackDoneEvent: Melee attack completed
    /// </summary>
    public class Combat_HandleAttackDoneEvent_S2C : IACMessageType {
        /// <summary>
        /// Number of user attacks, doesn&#39;t appear to be used by client
        /// </summary>
        public uint Number;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Number = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// RemoveSpell: Delete a spell from your spellbook.
    /// </summary>
    public class Magic_RemoveSpell_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Removes a spell from the spell book
    /// </summary>
    public class Magic_RemoveSpell_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// You just died.
    /// </summary>
    public class Combat_HandleVictimNotificationEventSelf_S2C : IACMessageType {
        /// <summary>
        /// Your (typically mocking) death message.
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Message for a death, something you killed or your own death message.
    /// </summary>
    public class Combat_HandleVictimNotificationEventOther_S2C : IACMessageType {
        /// <summary>
        /// The text of the nearby or present death message.
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// HandleAttackerNotificationEvent: You have hit your target with a melee attack.
    /// </summary>
    public class Combat_HandleAttackerNotificationEvent_S2C : IACMessageType {
        /// <summary>
        /// the name of your target
        /// </summary>
        public string Name;

        /// <summary>
        /// the type of damage done
        /// </summary>
        public DamageType DamageType;

        /// <summary>
        /// percentage of targets hp removed by damage. 0.0 (low) to 1.0 (high)
        /// </summary>
        public float DamagePercent;

        /// <summary>
        /// the amount of damage done
        /// </summary>
        public uint DamageDone;

        /// <summary>
        /// critical hit: 0=no, 1=yes
        /// </summary>
        public bool Critical;

        /// <summary>
        /// additional information about the attack, such as whether it was a Sneak Attack, etc
        /// </summary>
        public AttackConditionsMask Conditions;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            DamageType = (DamageType)BinaryHelpers.ReadUInt32(buffer);
            DamagePercent = BinaryHelpers.ReadSingle(buffer);
            DamageDone = BinaryHelpers.ReadUInt32(buffer);
            Critical = BinaryHelpers.ReadBool(buffer);
            Conditions = (AttackConditionsMask)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// HandleDefenderNotificationEvent: You have been hit by a creature&#39;s melee attack.
    /// </summary>
    public class Combat_HandleDefenderNotificationEvent_S2C : IACMessageType {
        /// <summary>
        /// the name of the creature
        /// </summary>
        public string Name;

        /// <summary>
        /// the type of damage done
        /// </summary>
        public DamageType DamageType;

        /// <summary>
        /// percentage of targets hp removed by damage. 0.0 (low) to 1.0 (high)
        /// </summary>
        public float DamagePercent;

        /// <summary>
        /// the amount of damage done
        /// </summary>
        public uint DamageDone;

        /// <summary>
        /// the location of the damage done
        /// </summary>
        public DamageLocation Location;

        /// <summary>
        /// critical hit: 0=no, 1=yes
        /// </summary>
        public bool Critical;

        /// <summary>
        /// additional information about the attack, such as whether it was a Sneak Attack, etc.
        /// </summary>
        public AttackConditionsMask Conditions;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            DamageType = (DamageType)BinaryHelpers.ReadUInt32(buffer);
            DamagePercent = BinaryHelpers.ReadSingle(buffer);
            DamageDone = BinaryHelpers.ReadUInt32(buffer);
            Location = (DamageLocation)BinaryHelpers.ReadUInt32(buffer);
            Critical = BinaryHelpers.ReadBool(buffer);
            Conditions = (AttackConditionsMask)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// HandleEvasionAttackerNotificationEvent: Your target has evaded your melee attack.
    /// </summary>
    public class Combat_HandleEvasionAttackerNotificationEvent_S2C : IACMessageType {
        /// <summary>
        /// the name of your target
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// HandleEvasionDefenderNotificationEvent: You have evaded a creature&#39;s melee attack.
    /// </summary>
    public class Combat_HandleEvasionDefenderNotificationEvent_S2C : IACMessageType {
        /// <summary>
        /// the name of the creature
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Cancels an attack
    /// </summary>
    public class Combat_CancelAttack_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// HandleCommenceAttackEvent: Start melee attack
    /// </summary>
    public class Combat_HandleCommenceAttackEvent_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Query&#39;s a creatures health
    /// </summary>
    public class Combat_QueryHealth_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// QueryHealthResponse: Update a creature&#39;s health bar.
    /// </summary>
    public class Combat_QueryHealthResponse_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the creature
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// the amount of health remaining, scaled from 0.0 (none) to 1.0 (full)
        /// </summary>
        public float HealthPercent;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            HealthPercent = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Query a character&#39;s age
    /// </summary>
    public class Character_QueryAge_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// QueryAgeResponse: happens when you do /age in the game
    /// </summary>
    public class Character_QueryAgeResponse_S2C : IACMessageType {
        /// <summary>
        /// Name of target, or empty if self
        /// </summary>
        public string Name;

        /// <summary>
        /// Your age in the format 1mo 1d 1h 1m 1s
        /// </summary>
        public string Age;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            Age = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Query a character&#39;s birth day
    /// </summary>
    public class Character_QueryBirth_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// UseDone: Ready. Previous action complete
    /// </summary>
    public class Item_UseDone_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public StatusMessage Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Fellow update is done
    /// </summary>
    public class Fellowship_FellowUpdateDone_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Fellow stats are done
    /// </summary>
    public class Fellowship_FellowStatsDone_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Close Assess Panel
    /// </summary>
    public class Item_AppraiseDone_S2C : IACMessageType {
        /// <summary>
        /// Seems to always be 0. Client does not use it.
        /// </summary>
        public uint Unknown;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Unknown = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove Int Event
    /// </summary>
    public class Qualities_PrivateRemoveIntEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public IntId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (IntId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove Int Event
    /// </summary>
    public class Qualities_RemoveIntEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public IntId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (IntId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove Bool Event
    /// </summary>
    public class Qualities_PrivateRemoveBoolEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public BoolId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (BoolId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove Bool Event
    /// </summary>
    public class Qualities_RemoveBoolEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public BoolId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (BoolId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove Float Event
    /// </summary>
    public class Qualities_PrivateRemoveFloatEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public FloatId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (FloatId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove Float Event
    /// </summary>
    public class Qualities_RemoveFloatEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public FloatId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (FloatId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove String Event
    /// </summary>
    public class Qualities_PrivateRemoveStringEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public StringId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (StringId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove String Event
    /// </summary>
    public class Qualities_RemoveStringEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public StringId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (StringId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove DID Event
    /// </summary>
    public class Qualities_PrivateRemoveDataIDEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public DataId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (DataId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove DID Event
    /// </summary>
    public class Qualities_RemoveDataIDEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public DataId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (DataId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove IID Event
    /// </summary>
    public class Qualities_PrivateRemoveInstanceIDEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public InstanceId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (InstanceId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove IID Event
    /// </summary>
    public class Qualities_RemoveInstanceIDEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public InstanceId Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (InstanceId)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove Position Event
    /// </summary>
    public class Qualities_PrivateRemovePositionEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public PositionPropertyID Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove Position Event
    /// </summary>
    public class Qualities_RemovePositionEvent_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public PositionPropertyID Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Emote message
    /// </summary>
    public class Communication_Emote_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Indirect &#39;/e&#39; text.
    /// </summary>
    public class Communication_HearEmote_S2C : IACMessageType {
        /// <summary>
        /// The ID of the character performing the emote - used for squelch/radar filtering.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Name of the character performing the emote.
        /// </summary>
        public string Name;

        /// <summary>
        /// Text representation of the emote.
        /// </summary>
        public string Text;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            Text = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Soul emote message
    /// </summary>
    public class Communication_SoulEmote_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Contains the text associated with an emote action.
    /// </summary>
    public class Communication_HearSoulEmote_S2C : IACMessageType {
        /// <summary>
        /// The ID of the character performing the emote - used for squelch/radar filtering.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Name of the character performing the emote.
        /// </summary>
        public string Name;

        /// <summary>
        /// Text representation of the emote.
        /// </summary>
        public string Text;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            Text = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Add a spell to a spell bar.
    /// </summary>
    public class Character_AddSpellFavorite_C2S : IACMessageType {
        /// <summary>
        /// The spell&#39;s ID
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Position on the spell bar where the spell is to be added
        /// </summary>
        public uint Index;

        /// <summary>
        /// The spell bar number
        /// </summary>
        public uint Bar;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
            Index = BinaryHelpers.ReadUInt32(buffer);
            Bar = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Remove a spell from a spell bar.
    /// </summary>
    public class Character_RemoveSpellFavorite_C2S : IACMessageType {
        /// <summary>
        /// The spell&#39;s ID
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// The spell bar number
        /// </summary>
        public uint Bar;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
            Bar = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Request a ping
    /// </summary>
    public class Character_RequestPing_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Ping Reply
    /// </summary>
    public class Character_ReturnPing_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Squelch and Filter List
    /// </summary>
    public class Communication_SetSquelchDB_S2C : IACMessageType {
        /// <summary>
        /// The set of squelch information for the user
        /// </summary>
        public SquelchDB SquelchDB;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SquelchDB = new SquelchDB();
            SquelchDB.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Starts trading with another player.
    /// </summary>
    public class Trade_OpenTradeNegotiations_C2S : IACMessageType {
        /// <summary>
        /// ID of player to trade with
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Ends trading, when trade window is closed?
    /// </summary>
    public class Trade_CloseTradeNegotiations_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Adds an object to the trade.
    /// </summary>
    public class Trade_AddToTrade_C2S : IACMessageType {
        /// <summary>
        /// ID of object to add to the trade
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Slot in trade window to add the object
        /// </summary>
        public uint Slot;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Slot = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Accepts a trade.
    /// </summary>
    public class Trade_AcceptTrade_C2S : IACMessageType {
        /// <summary>
        /// The contents of the trade
        /// </summary>
        public Trade Trade;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Trade = new Trade();
            Trade.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Declines a trade, when cancel is clicked?
    /// </summary>
    public class Trade_DeclineTrade_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// RegisterTrade: Send to begin a trade and display the trade window
    /// </summary>
    public class Trade_RegisterTrade_S2C : IACMessageType {
        /// <summary>
        /// Person initiating the trade
        /// </summary>
        public uint InitiatorId;

        /// <summary>
        /// Person receiving the trade
        /// </summary>
        public uint PartnerId;

        /// <summary>
        /// Some kind of stamp
        /// </summary>
        public long Stamp;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            InitiatorId = BinaryHelpers.ReadUInt32(buffer);
            PartnerId = BinaryHelpers.ReadUInt32(buffer);
            Stamp = BinaryHelpers.ReadInt64(buffer);
        }

    }

    /// <summary>
    /// OpenTrade: Open trade window
    /// </summary>
    public class Trade_OpenTrade_S2C : IACMessageType {
        /// <summary>
        /// Person opening the trade
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// CloseTrade: End trading
    /// </summary>
    public class Trade_CloseTrade_S2C : IACMessageType {
        /// <summary>
        /// Reason trade was cancelled
        /// </summary>
        public EndTradeReason Reason;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Reason = (EndTradeReason)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// RemoveFromTrade: Item was removed from trade window
    /// </summary>
    public class Trade_AddToTrade_S2C : IACMessageType {
        /// <summary>
        /// The item being removed from trade window
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Side of the trade window object was removed
        /// </summary>
        public TradeSide Side;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Side = (TradeSide)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Removes an item from the trade window, not sure if this is used still?
    /// </summary>
    public class Trade_RemoveFromTrade_S2C : IACMessageType {
        /// <summary>
        /// The item being removed from the trade window
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Side of the trade window object was removed
        /// </summary>
        public TradeSide Side;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Side = (TradeSide)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// AcceptTrade: The trade was accepted
    /// </summary>
    public class Trade_AcceptTrade_S2C : IACMessageType {
        /// <summary>
        /// Person who accepted the trade
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// DeclineTrade: The trade was declined
    /// </summary>
    public class Trade_DeclineTrade_S2C : IACMessageType {
        /// <summary>
        /// Person who declined the trade
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Resets the trade, when clear all is clicked?
    /// </summary>
    public class Trade_ResetTrade_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// ResetTrade: The trade window was reset
    /// </summary>
    public class Trade_ResetTrade_S2C : IACMessageType {
        /// <summary>
        /// Person who cleared the window
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// TradeFailure: Failure to add a trade item
    /// </summary>
    public class Trade_TradeFailure_S2C : IACMessageType {
        /// <summary>
        /// Item that could not be added to trade window
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The numeric reason it couldn&#39;t be traded. Client does not appear to use this.
        /// </summary>
        public uint Reason;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Reason = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// ClearTradeAcceptance: Failure to complete a trade
    /// </summary>
    public class Trade_ClearTradeAcceptance_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clears the player&#39;s corpse looting consent list, /consent clear
    /// </summary>
    public class Character_ClearPlayerConsentList_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Display the player&#39;s corpse looting consent list, /consent who 
    /// </summary>
    public class Character_DisplayPlayerConsentList_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Remove your corpse looting permission for the given player, /consent remove 
    /// </summary>
    public class Character_RemoveFromPlayerConsentList_C2S : IACMessageType {
        /// <summary>
        /// Name of player to remove permission to loot the playes corpses
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Grants a player corpse looting permission, /permit add
    /// </summary>
    public class Character_AddPlayerPermission_C2S : IACMessageType {
        /// <summary>
        /// Name of player to grant permission to loot the playes corpses
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Buy a house
    /// </summary>
    public class House_BuyHouse_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Slumlord;

        /// <summary>
        /// items being used to buy the house
        /// </summary>
        public PackableList<uint> ItemIds;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Slumlord = BinaryHelpers.ReadUInt32(buffer);
            ItemIds = new PackableList<uint>();
            ItemIds.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Buy a dwelling or pay maintenance
    /// </summary>
    public class House_HouseProfile_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the dwelling&#39;s covenant crystal
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public HouseProfile Profile;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Profile = new HouseProfile();
            Profile.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Query your house info, during signin
    /// </summary>
    public class House_QueryHouse_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Abandons your house
    /// </summary>
    public class House_AbandonHouse_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Revokes a player&#39;s corpse looting permission, /permit remove
    /// </summary>
    public class Character_RemovePlayerPermission_C2S : IACMessageType {
        /// <summary>
        /// Name of player to remove permission to loot the playes corpses
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Pay rent for a house
    /// </summary>
    public class House_RentHouse_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Slumlord;

        /// <summary>
        /// items being used to pay the house rent
        /// </summary>
        public PackableList<uint> ItemIds;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Slumlord = BinaryHelpers.ReadUInt32(buffer);
            ItemIds = new PackableList<uint>();
            ItemIds.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Sets a new fill complevel for a component
    /// </summary>
    public class Character_SetDesiredComponentLevel_C2S : IACMessageType {
        /// <summary>
        /// class id of the component
        /// </summary>
        public uint WeenieClassId;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Amount;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            WeenieClassId = BinaryHelpers.ReadUInt32(buffer);
            Amount = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// House panel information for owners.
    /// </summary>
    public class House_HouseData_S2C : IACMessageType {
        /// <summary>
        /// the house data
        /// </summary>
        public HouseData Data;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Data = new HouseData();
            Data.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// House Data
    /// </summary>
    public class House_HouseStatus_S2C : IACMessageType {
        /// <summary>
        /// Type of message to display
        /// </summary>
        public uint Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Update Rent Time
    /// </summary>
    public class House_UpdateRentTime_S2C : IACMessageType {
        /// <summary>
        /// when the current maintenance period began (Unix timestamp)
        /// </summary>
        public uint RentTime;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            RentTime = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Update Rent Payment
    /// </summary>
    public class House_UpdateRentPayment_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public PackableList<HousePayment> Rent;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Rent = new PackableList<HousePayment>();
            Rent.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Adds a guest to your house guest list
    /// </summary>
    public class House_AddPermanentGuest_C2S : IACMessageType {
        /// <summary>
        /// Player name to add to your house guest list
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Removes a specific player from your house guest list, /house guest remove
    /// </summary>
    public class House_RemovePermanentGuest_C2S : IACMessageType {
        /// <summary>
        /// Player name to remove from your house guest list
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Sets your house open status
    /// </summary>
    public class House_SetOpenHouseStatus_C2S : IACMessageType {
        /// <summary>
        /// Whether the house is open or not
        /// </summary>
        public bool OpenHouse;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            OpenHouse = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Update Restrictions
    /// </summary>
    public class House_UpdateRestrictions_S2C : IACMessageType {
        /// <summary>
        /// Sequence value for restrictions list for this house
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Object having restrictions updated
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Restrictions database
        /// </summary>
        public RestrictionDB Restrictions;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Restrictions = new RestrictionDB();
            Restrictions.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Changes a specific players storage permission, /house storage add/remove
    /// </summary>
    public class House_ChangeStoragePermission_C2S : IACMessageType {
        /// <summary>
        /// Player name to boot from your house
        /// </summary>
        public string Name;

        /// <summary>
        /// Whether the player has permission on your storage
        /// </summary>
        public bool HasPermission;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            HasPermission = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Boots a specific player from your house /house boot
    /// </summary>
    public class House_BootSpecificHouseGuest_C2S : IACMessageType {
        /// <summary>
        /// Player name to boot from your house
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Removes all storage permissions, /house storage remove_all
    /// </summary>
    public class House_RemoveAllStoragePermission_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Requests your full guest list, /house guest list
    /// </summary>
    public class House_RequestFullGuestList_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Sets the allegiance message of the day, /allegiance motd set
    /// </summary>
    public class Allegiance_SetMotd_C2S : IACMessageType {
        /// <summary>
        /// Text motd to display
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Query the motd, /allegiance motd
    /// </summary>
    public class Allegiance_QueryMotd_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clear the motd, /allegiance motd clear
    /// </summary>
    public class Allegiance_ClearMotd_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// House Guest List
    /// </summary>
    public class House_UpdateHAR_S2C : IACMessageType {
        /// <summary>
        /// Set of house access records
        /// </summary>
        public HAR AccessRecords;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            AccessRecords = new HAR();
            AccessRecords.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Gets SlumLord info, sent after getting a failed house transaction
    /// </summary>
    public class House_QueryLord_C2S : IACMessageType {
        /// <summary>
        /// SlumLord ID to request info for
        /// </summary>
        public uint Slumlord;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Slumlord = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// House Profile
    /// </summary>
    public class House_HouseTransaction_S2C : IACMessageType {
        /// <summary>
        /// Type of message to display
        /// </summary>
        public uint Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Adds all to your storage permissions, /house storage add -all
    /// </summary>
    public class House_AddAllStoragePermission_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Removes all guests, /house guest remove_all
    /// </summary>
    public class House_RemoveAllPermanentGuests_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Boot everyone from your house, /house boot -all
    /// </summary>
    public class House_BootEveryone_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Teleports you to your house, /house recall
    /// </summary>
    public class House_TeleToHouse_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Queries an item&#39;s mana
    /// </summary>
    public class Item_QueryItemMana_C2S : IACMessageType {
        /// <summary>
        /// ID of object whos mana is being queried
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Update an item&#39;s mana bar.
    /// </summary>
    public class Item_QueryItemManaResponse_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the item
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// the amount of mana remaining, scaled from 0.0 (none) to 1.0 (full)
        /// </summary>
        public float Mana;

        /// <summary>
        /// show mana bar: 0=no, 1=yes
        /// </summary>
        public bool Success;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Mana = BinaryHelpers.ReadSingle(buffer);
            Success = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Modify whether house hooks are visibile or not, /house hooks on/off
    /// </summary>
    public class House_SetHooksVisibility_C2S : IACMessageType {
        /// <summary>
        /// Whether hooks are visible or not
        /// </summary>
        public bool Visible;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Visible = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Modify whether allegiance members are guests, /house guest add_allegiance/remove_allegiance
    /// </summary>
    public class House_ModifyAllegianceGuestPermission_C2S : IACMessageType {
        /// <summary>
        /// Whether we are adding or removing permissions
        /// </summary>
        public bool Add;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Add = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Modify whether allegiance members can access storage, /house storage add_allegiance/remove_allegiance
    /// </summary>
    public class House_ModifyAllegianceStoragePermission_C2S : IACMessageType {
        /// <summary>
        /// Whether we are adding or removing permissions
        /// </summary>
        public bool Add;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Add = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Joins a chess game
    /// </summary>
    public class Game_Join_C2S : IACMessageType {
        /// <summary>
        /// ID of the game the player is joining
        /// </summary>
        public uint GameId;

        /// <summary>
        /// Which team the player is joining as
        /// </summary>
        public uint Team;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            GameId = BinaryHelpers.ReadUInt32(buffer);
            Team = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Quits a chess game
    /// </summary>
    public class Game_Quit_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Makes a chess move
    /// </summary>
    public class Game_Move_C2S : IACMessageType {
        /// <summary>
        /// Current x location of piece being moved
        /// </summary>
        public int XFrom;

        /// <summary>
        /// Current y location of piece being moved
        /// </summary>
        public int YFrom;

        /// <summary>
        /// Destination x location of piece being moved
        /// </summary>
        public int XTo;

        /// <summary>
        /// Destination y location of piece being moved
        /// </summary>
        public int YTo;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            XFrom = BinaryHelpers.ReadInt32(buffer);
            YFrom = BinaryHelpers.ReadInt32(buffer);
            XTo = BinaryHelpers.ReadInt32(buffer);
            YTo = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Skip a move?
    /// </summary>
    public class Game_MovePass_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Offer or confirm stalemate
    /// </summary>
    public class Game_Stalemate_C2S : IACMessageType {
        /// <summary>
        /// Whether stalemate offer is active or not
        /// </summary>
        public bool On;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            On = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Lists available house /house available
    /// </summary>
    public class House_ListAvailableHouses_C2S : IACMessageType {
        /// <summary>
        /// Type of house being listed
        /// </summary>
        public HouseType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (HouseType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Display a list of available dwellings in the chat window.
    /// </summary>
    public class House_AvailableHouses_S2C : IACMessageType {
        /// <summary>
        /// The type of house (1=cottage, 2=villa, 3=mansion, 4=apartment)
        /// </summary>
        public HouseType Type;

        /// <summary>
        /// Landcell location of the houses
        /// </summary>
        public PackableList<uint> Landcells;

        /// <summary>
        /// The total number of houses of this type available
        /// </summary>
        public int Available;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (HouseType)BinaryHelpers.ReadUInt32(buffer);
            Landcells = new PackableList<uint>();
            Landcells.ReadFromBuffer(buffer);
            Available = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Display a confirmation panel.
    /// </summary>
    public class Character_ConfirmationRequest_S2C : IACMessageType {
        /// <summary>
        /// the type of confirmation panel to display
        /// </summary>
        public ConfirmationType Type;

        /// <summary>
        /// sequence number
        /// </summary>
        public uint ContextId;

        /// <summary>
        /// text to be included in the confirmation panel message
        /// </summary>
        public string Text;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (ConfirmationType)BinaryHelpers.ReadUInt32(buffer);
            ContextId = BinaryHelpers.ReadUInt32(buffer);
            Text = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Confirms a dialog
    /// </summary>
    public class Character_ConfirmationResponse_C2S : IACMessageType {
        /// <summary>
        /// the type of confirmation panel being confirmed
        /// </summary>
        public ConfirmationType Type;

        /// <summary>
        /// sequence number
        /// </summary>
        public uint ContextId;

        /// <summary>
        /// wether the confirmation request was accepted
        /// </summary>
        public bool Accepted;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (ConfirmationType)BinaryHelpers.ReadUInt32(buffer);
            ContextId = BinaryHelpers.ReadUInt32(buffer);
            Accepted = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Confirmation done
    /// </summary>
    public class Character_ConfirmationDone_S2C : IACMessageType {
        /// <summary>
        /// the type of confirmation panel being closed
        /// </summary>
        public ConfirmationType Type;

        /// <summary>
        /// sequence number
        /// </summary>
        public uint ContextId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (ConfirmationType)BinaryHelpers.ReadUInt32(buffer);
            ContextId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Boots a player from the allegiance, optionally all characters on their account
    /// </summary>
    public class Allegiance_BreakAllegianceBoot_C2S : IACMessageType {
        /// <summary>
        /// Name of character to boot
        /// </summary>
        public string Name;

        /// <summary>
        /// Whether to boot all characters on their account
        /// </summary>
        public bool Account;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            Account = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Teleports player to their allegiance housing, /house mansion_recall
    /// </summary>
    public class House_TeleToMansion_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Player is commiting suicide
    /// </summary>
    public class Character_Suicide_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Display an allegiance login/logout message in the chat window.
    /// </summary>
    public class Allegiance_AllegianceLoginNotificationEvent_S2C : IACMessageType {
        /// <summary>
        /// the object ID of the player logging in or out
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// 0=logout, 1=login
        /// </summary>
        public bool NowLoggedIn;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            NowLoggedIn = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Request allegiance info for a player
    /// </summary>
    public class Allegiance_AllegianceInfoRequest_C2S : IACMessageType {
        /// <summary>
        /// Name of player whom the request is targeting
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Returns data for a player&#39;s allegiance information
    /// </summary>
    public class Allegiance_AllegianceInfoResponseEvent_S2C : IACMessageType {
        /// <summary>
        /// Target of the request
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Allegiance Profile Data
        /// </summary>
        public AllegianceProfile Profile;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Profile = new AllegianceProfile();
            Profile.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Salvages items
    /// </summary>
    public class Inventory_CreateTinkeringTool_C2S : IACMessageType {
        /// <summary>
        /// The id of the object being used to salvage (your ust)
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Set of object ID&#39;s to be salvaged
        /// </summary>
        public PackableList<uint> ItemIds;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ItemIds = new PackableList<uint>();
            ItemIds.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Joining game response
    /// </summary>
    public class Game_JoinGameResponse_S2C : IACMessageType {
        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint Id;

        /// <summary>
        /// -1 indicates failure, otherwise which team you are for this game
        /// </summary>
        public int Team;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Team = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Start game
    /// </summary>
    public class Game_StartGame_S2C : IACMessageType {
        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint Id;

        /// <summary>
        /// Which team that should go first
        /// </summary>
        public int Team;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Team = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Move response
    /// </summary>
    public class Game_MoveResponse_S2C : IACMessageType {
        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint Id;

        /// <summary>
        /// If less than or equal to 0, then failure
        /// </summary>
        public ChessMoveResult Result;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Result = (ChessMoveResult)BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Opponent Turn
    /// </summary>
    public class Game_OpponentTurn_S2C : IACMessageType {
        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint Id;

        /// <summary>
        /// Team making this move
        /// </summary>
        public int Team;

        /// <summary>
        /// Data related to the piece move
        /// </summary>
        public GameMoveData MoveData;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Team = BinaryHelpers.ReadInt32(buffer);
            MoveData = new GameMoveData();
            MoveData.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Opponent Stalemate State
    /// </summary>
    public class Game_OpponentStalemateState_S2C : IACMessageType {
        /// <summary>
        /// Some kind of identifier for this game
        /// </summary>
        public uint Id;

        /// <summary>
        /// Team
        /// </summary>
        public int Team;

        /// <summary>
        /// 1 = offering stalemate, 0 = retracting stalemate
        /// </summary>
        public bool On;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Team = BinaryHelpers.ReadInt32(buffer);
            On = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Changes the spell book filter
    /// </summary>
    public class Character_SpellbookFilterEvent_C2S : IACMessageType {
        /// <summary>
        /// Mask containing the different filters applied to the spellbook
        /// </summary>
        public SpellBookFilterOptions Options;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Options = (SpellBookFilterOptions)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Display a status message in the chat window.
    /// </summary>
    public class Communication_WeenieError_S2C : IACMessageType {
        /// <summary>
        /// the type of status message to display
        /// </summary>
        public StatusMessage Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Display a parameterized status message in the chat window.
    /// </summary>
    public class Communication_WeenieErrorWithString_S2C : IACMessageType {
        /// <summary>
        /// the type of status message to display
        /// </summary>
        public StatusMessage Type;

        /// <summary>
        /// text to be included in the status message
        /// </summary>
        public string Text;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (StatusMessage)BinaryHelpers.ReadUInt32(buffer);
            Text = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// End of Chess game
    /// </summary>
    public class Game_GameOver_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Id;

        /// <summary>
        /// Which team was the winner for this game
        /// </summary>
        public int Winner;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Id = BinaryHelpers.ReadUInt32(buffer);
            Winner = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Teleport to the marketplace
    /// </summary>
    public class Character_TeleToMarketplace_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Enter PKLite mode
    /// </summary>
    public class Character_EnterPKLite_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Fellowship Assign a new leader
    /// </summary>
    public class Fellowship_AssignNewLeader_C2S : IACMessageType {
        /// <summary>
        /// ID of player to make the new leader of the fellowship
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Fellowship Change openness
    /// </summary>
    public class Fellowship_ChangeFellowOpeness_C2S : IACMessageType {
        /// <summary>
        /// Sets whether the fellowship is open or not
        /// </summary>
        public bool Open;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Open = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Set Turbine Chat channel numbers.
    /// </summary>
    public class Communication_ChatRoomTracker_S2C : IACMessageType {
        /// <summary>
        /// the channel number of the allegiance channel
        /// </summary>
        public uint AllegianceRoomId;

        /// <summary>
        /// the channel number of the general channel
        /// </summary>
        public uint GeneralChatRoomId;

        /// <summary>
        /// the channel number of the trade channel
        /// </summary>
        public uint TradeChatRoomId;

        /// <summary>
        /// the channel number of the looking-for-group channel
        /// </summary>
        public uint LFGChatRoomId;

        /// <summary>
        /// the channel number of the roleplay channel
        /// </summary>
        public uint RoleplayChatRoomId;

        /// <summary>
        /// the channel number of the olthoi channel
        /// </summary>
        public uint OlthoiChatRoomId;

        /// <summary>
        /// the channel number of the society channel
        /// </summary>
        public uint SocietyChatRoomId;

        /// <summary>
        /// the channel number of the Celestial Hand channel
        /// </summary>
        public uint SocietyCelHanChatRoomId;

        /// <summary>
        /// the channel number of the Eldrich Web channel
        /// </summary>
        public uint SocietyEldWebChatRoomId;

        /// <summary>
        /// the channel number of the Radiant Blood channel
        /// </summary>
        public uint SocietyRadBloChatRoomId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            AllegianceRoomId = BinaryHelpers.ReadUInt32(buffer);
            GeneralChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            TradeChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            LFGChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            RoleplayChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            OlthoiChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            SocietyChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            SocietyCelHanChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            SocietyEldWebChatRoomId = BinaryHelpers.ReadUInt32(buffer);
            SocietyRadBloChatRoomId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Boots a player from the allegiance chat
    /// </summary>
    public class Allegiance_AllegianceChatBoot_C2S : IACMessageType {
        /// <summary>
        /// Character name being booted
        /// </summary>
        public string Name;

        /// <summary>
        /// Reason for getting booted
        /// </summary>
        public string Reason;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
            Reason = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Bans a player from the allegiance
    /// </summary>
    public class Allegiance_AddAllegianceBan_C2S : IACMessageType {
        /// <summary>
        /// Character name being banned
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Removes a player ban from the allegiance
    /// </summary>
    public class Allegiance_RemoveAllegianceBan_C2S : IACMessageType {
        /// <summary>
        /// Character name being unbanned
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Display allegiance bans
    /// </summary>
    public class Allegiance_ListAllegianceBans_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Removes an allegiance officer
    /// </summary>
    public class Allegiance_RemoveAllegianceOfficer_C2S : IACMessageType {
        /// <summary>
        /// Character name being removed
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// List allegiance officers
    /// </summary>
    public class Allegiance_ListAllegianceOfficers_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Clear allegiance officers
    /// </summary>
    public class Allegiance_ClearAllegianceOfficers_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Recalls to players allegiance hometown
    /// </summary>
    public class Allegiance_RecallAllegianceHometown_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// TODO: QueryPluginList
    /// </summary>
    public class Admin_QueryPluginList_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Admin Returns a plugin list response
    /// </summary>
    public class Admin_QueryPluginListResponse_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Context;

        /// <summary>
        /// TODO
        /// </summary>
        public string PluginList;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Context = BinaryHelpers.ReadUInt32(buffer);
            PluginList = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// TODO: QueryPlugin
    /// </summary>
    public class Admin_QueryPlugin_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Admin Returns plugin info
    /// </summary>
    public class Admin_QueryPluginResponse_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Context;

        /// <summary>
        /// TODO
        /// </summary>
        public bool Success;

        /// <summary>
        /// TODO
        /// </summary>
        public string Name;

        /// <summary>
        /// TODO
        /// </summary>
        public string Author;

        /// <summary>
        /// TODO
        /// </summary>
        public string Email;

        /// <summary>
        /// TODO
        /// </summary>
        public string Webpage;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Context = BinaryHelpers.ReadUInt32(buffer);
            Success = BinaryHelpers.ReadBool(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            Author = BinaryHelpers.ReadString(buffer);
            Email = BinaryHelpers.ReadString(buffer);
            Webpage = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// TODO: QueryPluginResponse
    /// </summary>
    public class Admin_QueryPluginResponse2_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Salvage operation results
    /// </summary>
    public class Inventory_SalvageOperationsResultData_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public SalvageOperationsResultData Results;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Results = new SalvageOperationsResultData();
            Results.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Private Remove Int Event
    /// </summary>
    public class Qualities_PrivateRemoveInt64Event_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// TODO
        /// </summary>
        public Int64Id Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Type = (Int64Id)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Private Remove Int Event
    /// </summary>
    public class Qualities_RemoveInt64Event_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// sender ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public Int64Id Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (Int64Id)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// A message to be displayed in the chat window, spoken by a nearby player, NPC or creature
    /// </summary>
    public class Communication_HearSpeech_S2C : IACMessageType {
        /// <summary>
        /// message text
        /// </summary>
        public string Message;

        /// <summary>
        /// sender name
        /// </summary>
        public string Name;

        /// <summary>
        /// sender ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// message type
        /// </summary>
        public ChatMessageType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// A message to be displayed in the chat window, spoken by a nearby player, NPC or creature
    /// </summary>
    public class Communication_HearRangedSpeech_S2C : IACMessageType {
        /// <summary>
        /// message text
        /// </summary>
        public string Message;

        /// <summary>
        /// sender name
        /// </summary>
        public string Name;

        /// <summary>
        /// sender ID, must be between 0x50000001 and 0x6FFFFFFF to appear as clickable
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// broadcast range
        /// </summary>
        public float Range;

        /// <summary>
        /// message type
        /// </summary>
        public ChatMessageType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Range = BinaryHelpers.ReadSingle(buffer);
            Type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Someone has sent you a @tell.
    /// </summary>
    public class Communication_HearDirectSpeech_S2C : IACMessageType {
        /// <summary>
        /// the message text
        /// </summary>
        public string Message;

        /// <summary>
        /// the name of the creature sending you the message
        /// </summary>
        public string Name;

        /// <summary>
        /// the object ID of the creature sending you the message
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// the object ID of the recipient of the message (should be you)
        /// </summary>
        public uint TargetId;

        /// <summary>
        /// the message type, controls color and @filter processing
        /// </summary>
        public ChatMessageType Type;

        /// <summary>
        /// doesn&#39;t seem to be used by the client
        /// </summary>
        public uint SecretFlags;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
            Name = BinaryHelpers.ReadString(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            TargetId = BinaryHelpers.ReadUInt32(buffer);
            Type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
            SecretFlags = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Create or join a fellowship
    /// </summary>
    public class Fellowship_FullUpdate_S2C : IACMessageType {
        /// <summary>
        /// Full set of fellowship information
        /// </summary>
        public Fellowship Fellowship;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Fellowship = new Fellowship();
            Fellowship.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Disband your fellowship.
    /// </summary>
    public class Fellowship_Disband_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Add/Update a member to your fellowship.
    /// </summary>
    public class Fellowship_UpdateFellow_S2C : IACMessageType {
        /// <summary>
        /// The objectId of the fellowship member being updated.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Information about a fellow member
        /// </summary>
        public Fellow Fellow;

        /// <summary>
        /// The type of fellowship update
        /// </summary>
        public FellowUpdateType Type;

        public Fellowship_UpdateFellow_S2C() {
        }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Fellow = new Fellow();
            Fellow.ReadFromBuffer(buffer);
            Type = (FellowUpdateType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Add a spell to your spellbook.
    /// </summary>
    public class Magic_UpdateSpell_S2C : IACMessageType {
        /// <summary>
        /// the spell ID of the new spell
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Apply an enchantment to your character.
    /// </summary>
    public class Magic_UpdateEnchantment_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public Enchantment Enchantment;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Enchantment = new Enchantment();
            Enchantment.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Remove an enchantment from your character.
    /// </summary>
    public class Magic_RemoveEnchantment_S2C : IACMessageType {
        /// <summary>
        /// the spell ID of the enchantment to be removed
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Update multiple enchantments from your character.
    /// </summary>
    public class Magic_UpdateMultipleEnchantments_S2C : IACMessageType {
        /// <summary>
        /// List of enchantments getting updated
        /// </summary>
        public PackableList<Enchantment> Enchantments;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Enchantments = new PackableList<Enchantment>();
            Enchantments.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Remove multiple enchantments from your character.
    /// </summary>
    public class Magic_RemoveMultipleEnchantments_S2C : IACMessageType {
        /// <summary>
        /// List of enchantments getting removed
        /// </summary>
        public PackableList<LayeredSpellId> Enchantments;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Enchantments = new PackableList<LayeredSpellId>();
            Enchantments.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Silently remove all enchantments from your character, e.g. when you die (no message in the chat window).
    /// </summary>
    public class Magic_PurgeEnchantments_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Silently remove An enchantment from your character.
    /// </summary>
    public class Magic_DispelEnchantment_S2C : IACMessageType {
        /// <summary>
        /// the spell ID of the enchantment to be removed
        /// </summary>
        public LayeredSpellId SpellId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SpellId = new LayeredSpellId();
            SpellId.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Silently remove multiple enchantments from your character (no message in the chat window).
    /// </summary>
    public class Magic_DispelMultipleEnchantments_S2C : IACMessageType {
        /// <summary>
        /// List of enchantments getting removed
        /// </summary>
        public PackableList<LayeredSpellId> Enchantments;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Enchantments = new PackableList<LayeredSpellId>();
            Enchantments.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// A portal storm is brewing.
    /// </summary>
    public class Misc_PortalStormBrewing_S2C : IACMessageType {
        /// <summary>
        /// Less than or equal to 0.0 resets timer, otherwise sets timer
        /// </summary>
        public float Extent;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Extent = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// A portal storm is imminent.
    /// </summary>
    public class Misc_PortalStormImminent_S2C : IACMessageType {
        /// <summary>
        /// Less than or equal to 0.0 resets timer, otherwise sets timer
        /// </summary>
        public float Extent;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Extent = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// You have been portal stormed.
    /// </summary>
    public class Misc_PortalStorm_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// The portal storm has subsided.
    /// </summary>
    public class Misc_PortalStormSubsided_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Set or update a Character Int property value
    /// </summary>
    public class Qualities_PrivateUpdateInt_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Int property ID
        /// </summary>
        public IntId Key;

        /// <summary>
        /// Int property value
        /// </summary>
        public int Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (IntId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object Int property value
    /// </summary>
    public class Qualities_UpdateInt_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Int property ID
        /// </summary>
        public IntId Key;

        /// <summary>
        /// Int property value
        /// </summary>
        public int Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (IntId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Int64 property value
    /// </summary>
    public class Qualities_PrivateUpdateInt64_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Int64 property ID
        /// </summary>
        public Int64Id Key;

        /// <summary>
        /// Int64 property value
        /// </summary>
        public long Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (Int64Id)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadInt64(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Int64 property value
    /// </summary>
    public class Qualities_UpdateInt64_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Int64 property ID
        /// </summary>
        public Int64Id Key;

        /// <summary>
        /// Int64 property value
        /// </summary>
        public long Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (Int64Id)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadInt64(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Boolean property value
    /// </summary>
    public class Qualities_PrivateUpdateBool_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Boolean property ID
        /// </summary>
        public BoolId Key;

        /// <summary>
        /// Boolean property value (0=False, 1=True)
        /// </summary>
        public bool Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (BoolId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object Boolean property value
    /// </summary>
    public class Qualities_UpdateBool_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Boolean property ID
        /// </summary>
        public BoolId Key;

        /// <summary>
        /// Boolean property value (0=False, 1=True)
        /// </summary>
        public bool Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (BoolId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object float property value
    /// </summary>
    public class Qualities_PrivateUpdateFloat_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Float property ID
        /// </summary>
        public FloatId Key;

        /// <summary>
        /// Float property value
        /// </summary>
        public float Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (FloatId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object float property value
    /// </summary>
    public class Qualities_UpdateFloat_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Float property ID
        /// </summary>
        public FloatId Key;

        /// <summary>
        /// Float property value
        /// </summary>
        public float Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (FloatId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object String property value
    /// </summary>
    public class Qualities_PrivateUpdateString_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// String property ID
        /// </summary>
        public StringId Key;

        /// <summary>
        /// String property value
        /// </summary>
        public string Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (StringId)BinaryHelpers.ReadUInt32(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
            Value = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object String property value
    /// </summary>
    public class Qualities_UpdateString_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// String property ID
        /// </summary>
        public StringId Key;

        /// <summary>
        /// String property value
        /// </summary>
        public string Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (StringId)BinaryHelpers.ReadUInt32(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
            Value = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object DID property value
    /// </summary>
    public class Qualities_PrivateUpdateDataID_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Data property ID
        /// </summary>
        public DataId Key;

        /// <summary>
        /// Resource property value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (DataId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object DID property value
    /// </summary>
    public class Qualities_UpdateDataID_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Data property ID
        /// </summary>
        public DataId Key;

        /// <summary>
        /// Resource property value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (DataId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a IID property value
    /// </summary>
    public class Qualities_PrivateUpdateInstanceID_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Instance property ID
        /// </summary>
        public InstanceId Key;

        /// <summary>
        /// Link property value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (InstanceId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update an Object IID property value
    /// </summary>
    public class Qualities_UpdateInstanceID_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Instance property ID
        /// </summary>
        public InstanceId Key;

        /// <summary>
        /// Link property value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (InstanceId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Position property value
    /// </summary>
    public class Qualities_PrivateUpdatePosition_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// Position property ID
        /// </summary>
        public PositionPropertyID Key;

        /// <summary>
        /// Position property value
        /// </summary>
        public Position Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Value = new Position();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Position property value
    /// </summary>
    public class Qualities_UpdatePosition_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Position property ID
        /// </summary>
        public PositionPropertyID Key;

        /// <summary>
        /// Position property value
        /// </summary>
        public Position Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (PositionPropertyID)BinaryHelpers.ReadUInt32(buffer);
            Value = new Position();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Skill value
    /// </summary>
    public class Qualities_PrivateUpdateSkill_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillId Key;

        /// <summary>
        /// skill information
        /// </summary>
        public Skill Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Value = new Skill();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Skill value
    /// </summary>
    public class Qualities_UpdateSkill_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillId Key;

        /// <summary>
        /// skill information
        /// </summary>
        public Skill Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Value = new Skill();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Skill Level
    /// </summary>
    public class Qualities_PrivateUpdateSkillLevel_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillId Key;

        /// <summary>
        /// skill level value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Skill Level
    /// </summary>
    public class Qualities_UpdateSkillLevel_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillId Key;

        /// <summary>
        /// skill level value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Skill state
    /// </summary>
    public class Qualities_PrivateUpdateSkillAC_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillId Key;

        /// <summary>
        /// skill state
        /// </summary>
        public SkillState Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Value = (SkillState)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Skill state
    /// </summary>
    public class Qualities_UpdateSkillAC_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// skill ID
        /// </summary>
        public SkillId Key;

        /// <summary>
        /// skill state
        /// </summary>
        public SkillState Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (SkillId)BinaryHelpers.ReadUInt32(buffer);
            Value = (SkillState)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Attribute value
    /// </summary>
    public class Qualities_PrivateUpdateAttribute_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeId Key;

        /// <summary>
        /// attribute information
        /// </summary>
        public Attribute Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (AttributeId)BinaryHelpers.ReadUInt32(buffer);
            Value = new Attribute();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Attribute value
    /// </summary>
    public class Qualities_UpdateAttribute_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeId Key;

        /// <summary>
        /// attribute information
        /// </summary>
        public Attribute Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (AttributeId)BinaryHelpers.ReadUInt32(buffer);
            Value = new Attribute();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Attribute Level
    /// </summary>
    public class Qualities_PrivateUpdateAttributeLevel_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeId Key;

        /// <summary>
        /// current value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (AttributeId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Attribute Level
    /// </summary>
    public class Qualities_UpdateAttributeLevel_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// attribute ID
        /// </summary>
        public AttributeId Key;

        /// <summary>
        /// current value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (AttributeId)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_PrivateUpdateAttribute2nd_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// vital ID
        /// </summary>
        public VitalId Key;

        /// <summary>
        /// vital information
        /// </summary>
        public SecondaryAttribute Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (VitalId)BinaryHelpers.ReadUInt32(buffer);
            Value = new SecondaryAttribute();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_UpdateAttribute2nd_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// vital ID
        /// </summary>
        public VitalId Key;

        /// <summary>
        /// vital information
        /// </summary>
        public SecondaryAttribute Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (VitalId)BinaryHelpers.ReadUInt32(buffer);
            Value = new SecondaryAttribute();
            Value.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_PrivateUpdateAttribute2ndLevel_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// vital ID
        /// </summary>
        public CurVitalID Key;

        /// <summary>
        /// current value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            Key = (CurVitalID)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Set or update a Character Vital value
    /// </summary>
    public class Qualities_UpdateAttribute2ndLevel_S2C : IACMessageType {
        /// <summary>
        /// sequence number
        /// </summary>
        public byte Sequence;

        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// vital ID
        /// </summary>
        public CurVitalID Key;

        /// <summary>
        /// current value
        /// </summary>
        public uint Value;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Sequence = BinaryHelpers.ReadByte(buffer);
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Key = (CurVitalID)BinaryHelpers.ReadUInt32(buffer);
            Value = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Display a status message on the Action Viewscreen (the red text overlaid on the 3D area).
    /// </summary>
    public class Communication_TransientString_S2C : IACMessageType {
        /// <summary>
        /// the message text
        /// </summary>
        public string Message;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Message = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Completes the barber interaction
    /// </summary>
    public class Character_FinishBarber_C2S : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint BasePalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HeadObject;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HeadTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultHeadTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint EyesTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultEyesTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint NoseTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultNoseTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint MouthTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint DefaultMouthTexture;

        /// <summary>
        /// TODO
        /// </summary>
        public uint SkinPalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint HairPalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint EyesPalette;

        /// <summary>
        /// TODO
        /// </summary>
        public uint SetupId;

        /// <summary>
        /// Specifies the toggle option for some races, such as floating empyrean or flaming head on undead
        /// </summary>
        public int Option1;

        /// <summary>
        /// Seems to be unused
        /// </summary>
        public int Option2;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            BasePalette = BinaryHelpers.ReadPackedDWORD(buffer);
            HeadObject = BinaryHelpers.ReadPackedDWORD(buffer);
            HeadTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultHeadTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            EyesTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultEyesTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            NoseTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultNoseTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            MouthTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            DefaultMouthTexture = BinaryHelpers.ReadPackedDWORD(buffer);
            SkinPalette = BinaryHelpers.ReadPackedDWORD(buffer);
            HairPalette = BinaryHelpers.ReadPackedDWORD(buffer);
            EyesPalette = BinaryHelpers.ReadPackedDWORD(buffer);
            SetupId = BinaryHelpers.ReadPackedDWORD(buffer);
            Option1 = BinaryHelpers.ReadInt32(buffer);
            Option2 = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// Remove all bad enchantments from your character.
    /// </summary>
    public class Magic_PurgeBadEnchantments_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Sends all contract data
    /// </summary>
    public class Social_SendClientContractTrackerTable_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public ContractTrackerTable Contracts;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Contracts = new ContractTrackerTable();
            Contracts.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Updates a contract data
    /// </summary>
    public class Social_SendClientContractTracker_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public ContractTracker ContractTracker;

        /// <summary>
        /// TODO
        /// </summary>
        public bool DeleteContract;

        /// <summary>
        /// TODO
        /// </summary>
        public bool SetAsDisplayContract;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ContractTracker = new ContractTracker();
            ContractTracker.ReadFromBuffer(buffer);
            DeleteContract = BinaryHelpers.ReadBool(buffer);
            SetAsDisplayContract = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Abandons a contract
    /// </summary>
    public class Social_AbandonContract_C2S : IACMessageType {
        /// <summary>
        /// ID of contact being abandoned
        /// </summary>
        public uint ContractId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ContractId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// This appears to be an admin command to change the environment (light, fog, sounds, colors)
    /// </summary>
    public class Admin_Environs_S2C : IACMessageType {
        /// <summary>
        /// ID of option set to change the environs
        /// </summary>
        public EnvironChangeType EnvironsOptions;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            EnvironsOptions = (EnvironChangeType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sets both the position and movement, such as when materializing at a lifestone
    /// </summary>
    public class Movement_PositionAndMovementEvent_S2C : IACMessageType {
        /// <summary>
        /// ObjectID of the character doing the animation
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public PositionPack Position;

        /// <summary>
        /// Set of movement data
        /// </summary>
        public MovementData MovementData;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Position = new PositionPack();
            Position.ReadFromBuffer(buffer);
            MovementData = new MovementData();
            MovementData.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Performs a jump
    /// </summary>
    public class Movement_Jump_C2S : IACMessageType {
        /// <summary>
        /// set of jumping data
        /// </summary>
        public JumpPack JumpPack;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            JumpPack = new JumpPack();
            JumpPack.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Move to state data
    /// </summary>
    public class Movement_MoveToState_C2S : IACMessageType {
        /// <summary>
        /// set of move to data, currently not in client, may not be used?
        /// </summary>
        public MoveToStatePack MoveToStatePack;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            MoveToStatePack = new MoveToStatePack();
            MoveToStatePack.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Performs a movement based on input
    /// </summary>
    public class Movement_DoMovementCommand_C2S : IACMessageType {
        /// <summary>
        /// motion command
        /// </summary>
        public uint Motion;

        /// <summary>
        /// speed of movement
        /// </summary>
        public float Speed;

        /// <summary>
        /// run key being held
        /// </summary>
        public uint HoldKey;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Motion = BinaryHelpers.ReadUInt32(buffer);
            Speed = BinaryHelpers.ReadSingle(buffer);
            HoldKey = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sent whenever a character changes their clothes. It contains the entire description of what their wearing (and possibly their facial features as well). This message is only sent for changes, when the character is first created, the body of this message is included inside the creation message.
    /// </summary>
    public class Item_ObjDescEvent_S2C : IACMessageType {
        /// <summary>
        /// The ID of character whose visual description is changing.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Set of visual description information for the object
        /// </summary>
        public ObjDesc ObjectDesc;

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The position sequence value for the object
        /// </summary>
        public ushort ObjectVisualDescSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ObjectDesc = new ObjDesc();
            ObjectDesc.ReadFromBuffer(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectVisualDescSequence = BinaryHelpers.ReadUInt16(buffer);
        }

    }

    /// <summary>
    /// Sets the player visual desc, TODO confirm this
    /// </summary>
    public class Character_SetPlayerVisualDesc_S2C : IACMessageType {
        /// <summary>
        /// Set of visual description information for the player
        /// </summary>
        public ObjDesc ObjectDesc;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectDesc = new ObjDesc();
            ObjectDesc.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Character creation screen initilised.
    /// </summary>
    public class Character_CharGenVerificationResponse_S2C : IACMessageType {
        /// <summary>
        /// Type of response
        /// </summary>
        public CharGenResponseType Type;

        /// <summary>
        /// The character ID for this entry.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The name of this character.
        /// </summary>
        public string Name;

        /// <summary>
        /// When 0, this character is not being deleted (not shown crossed out). Otherwise, it&#39;s a countdown timer in the number of seconds until the character is submitted for deletion.
        /// </summary>
        public uint SecondsGreyedOut;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (CharGenResponseType)BinaryHelpers.ReadUInt32(buffer);
            switch ((int)Type) {
                case 0x1:
                    ObjectId = BinaryHelpers.ReadUInt32(buffer);
                    Name = BinaryHelpers.ReadString(buffer);
                    SecondsGreyedOut = BinaryHelpers.ReadUInt32(buffer);
                    if ((buffer.BaseStream.Position % 4) != 0) {
                        buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
                    }
                    break;
            }
        }

    }

    /// <summary>
    /// Sent when your subsciption is about to expire
    /// </summary>
    public class Login_AwaitingSubscriptionExpiration_S2C : IACMessageType {
        /// <summary>
        /// Time remaining before your subscription expires.
        /// </summary>
        public uint SecondsRemaining;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            SecondsRemaining = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Instructs the client to return to 2D mode - the character list.
    /// </summary>
    public class Login_LogOffCharacter_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Instructs the client to return to 2D mode - the character list.
    /// </summary>
    public class Login_LogOffCharacter_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Mark a character for deletetion.
    /// </summary>
    public class Character_CharacterDelete_C2S : IACMessageType {
        /// <summary>
        /// The account for the character
        /// </summary>
        public string Account;

        /// <summary>
        /// Slot to delete
        /// </summary>
        public int Slot;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Account = BinaryHelpers.ReadString(buffer);
            Slot = BinaryHelpers.ReadInt32(buffer);
        }

    }

    /// <summary>
    /// A character was marked for deletetion.
    /// </summary>
    public class Character_CharacterDelete_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Character creation result
    /// </summary>
    public class Character_SendCharGenResult_C2S : IACMessageType {
        /// <summary>
        /// The account for the character
        /// </summary>
        public string Account;

        /// <summary>
        /// The data for the character generation
        /// </summary>
        public CharGenResult CharGenResult;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Account = BinaryHelpers.ReadString(buffer);
            CharGenResult = new CharGenResult();
            CharGenResult.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// The character to log in.
    /// </summary>
    public class Login_SendEnterWorld_C2S : IACMessageType {
        /// <summary>
        /// The character ID of the character to log in
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The account name associated with the character
        /// </summary>
        public string Account;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Account = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// The list of characters on the current account.
    /// </summary>
    public class Login_LoginCharacterSet_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Status;

        /// <summary>
        /// The number of characters in the list.  Characters appear in the list ordered most-recently-used first, but are displayed alphabetically.
        /// </summary>
        public uint CharacterCount;

        /// <summary>
        /// TODO
        /// </summary>
        public List<CharacterIdentity> Characters = new List<CharacterIdentity>();

        /// <summary>
        /// This is a second list which goes into delSet, not sure what it is, deleted characters?
        /// </summary>
        public uint CharacterCount2;

        /// <summary>
        /// TODO
        /// </summary>
        public List<CharacterIdentity> DeleteCharacters = new List<CharacterIdentity>();

        /// <summary>
        /// The total count of character slots.
        /// </summary>
        public uint NumAllowedCharacters;

        /// <summary>
        /// The name for this account.
        /// </summary>
        public string Account;

        /// <summary>
        /// Whether or not Turbine Chat (Allegiance chat) enabled.
        /// </summary>
        public bool UseTurbineChat;

        /// <summary>
        /// Whether or not this account has purchansed ToD
        /// </summary>
        public bool HasThroneofDestiny;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Status = BinaryHelpers.ReadUInt32(buffer);
            CharacterCount = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < CharacterCount; i++) {
                var id = new CharacterIdentity();
                id.ReadFromBuffer(buffer);
                Characters.Add(id);
            }
            CharacterCount2 = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < CharacterCount2; i++) {
                var id = new CharacterIdentity();
                id.ReadFromBuffer(buffer);
                DeleteCharacters.Add(id);
            }
            NumAllowedCharacters = BinaryHelpers.ReadUInt32(buffer);
            Account = BinaryHelpers.ReadString(buffer);
            UseTurbineChat = BinaryHelpers.ReadBool(buffer);
            HasThroneofDestiny = BinaryHelpers.ReadBool(buffer);
        }

    }

    /// <summary>
    /// Failure to log in
    /// </summary>
    public class Character_CharacterError_S2C : IACMessageType {
        /// <summary>
        /// Identifies type of error
        /// </summary>
        public CharacterErrorType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Type = (CharacterErrorType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Stops a movement
    /// </summary>
    public class Movement_StopMovementCommand_C2S : IACMessageType {
        /// <summary>
        /// Motion being stopped
        /// </summary>
        public uint Motion;

        /// <summary>
        /// Key being held
        /// </summary>
        public uint HoldKey;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Motion = BinaryHelpers.ReadUInt32(buffer);
            HoldKey = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Asks server for a new object description
    /// </summary>
    public class Object_SendForceObjdesc_C2S : IACMessageType {
        /// <summary>
        /// Object to get new Obj Desc for
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Create an object somewhere in the world
    /// </summary>
    public class Item_CreateObject_S2C : IACMessageType {
        /// <summary>
        /// object ID
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public ObjDesc ObjectDesc;

        /// <summary>
        /// TODO
        /// </summary>
        public PhysicsDesc PhysicsDesc;

        /// <summary>
        /// TODO
        /// </summary>
        public PublicWeenieDesc WeenieDesc;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ObjectDesc = new ObjDesc();
            ObjectDesc.ReadFromBuffer(buffer);
            PhysicsDesc = new PhysicsDesc();
            PhysicsDesc.ReadFromBuffer(buffer);
            WeenieDesc = new PublicWeenieDesc();
            WeenieDesc.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Login of player
    /// </summary>
    public class Login_CreatePlayer_S2C : IACMessageType {
        /// <summary>
        /// ID of the character logging on - should be you.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Sent whenever an object is being deleted from the scene.
    /// </summary>
    public class Item_DeleteObject_S2C : IACMessageType {
        /// <summary>
        /// The object that was recently erased.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort objectInstanceSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            objectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Sets the position/motion of an object
    /// </summary>
    public class Movement_PositionEvent_S2C : IACMessageType {
        /// <summary>
        /// The object with the position changing.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The current or starting location.
        /// </summary>
        public PositionPack Position;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Position = new PositionPack();
            Position.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Sets the parent for an object, eg. equipting an object.
    /// </summary>
    public class Item_ParentEvent_S2C : IACMessageType {
        /// <summary>
        /// id of the parent object
        /// </summary>
        public uint ParentId;

        /// <summary>
        /// id of the child object
        /// </summary>
        public uint ChildId;

        /// <summary>
        /// Location object is being equipt to (Read from CSetup table in dat)
        /// </summary>
        public uint Location;

        /// <summary>
        /// Placement frame id
        /// </summary>
        public uint Placement;

        /// <summary>
        /// The instance sequence value for the parent object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The position sequence value for the child object
        /// </summary>
        public ushort ChildPositionSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ParentId = BinaryHelpers.ReadUInt32(buffer);
            ChildId = BinaryHelpers.ReadUInt32(buffer);
            Location = BinaryHelpers.ReadUInt32(buffer);
            Placement = BinaryHelpers.ReadUInt32(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ChildPositionSequence = BinaryHelpers.ReadUInt16(buffer);
        }

    }

    /// <summary>
    /// Sent when picking up an object
    /// </summary>
    public class Inventory_PickupEvent_S2C : IACMessageType {
        /// <summary>
        /// The object being picked up
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The instance sequence value for the object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The position sequence value for the object
        /// </summary>
        public ushort objectOositionSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            objectOositionSequence = BinaryHelpers.ReadUInt16(buffer);
        }

    }

    /// <summary>
    /// Set&#39;s the current state of the object. Client appears to only process the following state changes post creation: NoDraw, LightingOn, Hidden
    /// </summary>
    public class Item_SetState_S2C : IACMessageType {
        /// <summary>
        /// The object being changed
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The new state for the object
        /// </summary>
        public PhysicsState State;

        /// <summary>
        /// The instance sequence value for this object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The state sequence value for this object
        /// </summary>
        public ushort ObjectStateSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            State = (PhysicsState)BinaryHelpers.ReadUInt32(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectStateSequence = BinaryHelpers.ReadUInt16(buffer);
        }

    }

    /// <summary>
    /// These are animations. Whenever a human, monster or object moves - one of these little messages is sent. Even idle emotes (like head scratching and nodding) are sent in this manner.
    /// </summary>
    public class Movement_SetObjectMovement_S2C : IACMessageType {
        /// <summary>
        /// ID of the character moving
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The instance sequence value for this object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// Set of movement data
        /// </summary>
        public MovementData MovementData;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            MovementData = new MovementData();
            MovementData.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Changes an objects vector, for things like jumping
    /// </summary>
    public class Movement_VectorUpdate_S2C : IACMessageType {
        /// <summary>
        /// ID of the object being updated
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// new velocity component
        /// </summary>
        public Vector3 Velocity;

        /// <summary>
        /// new omega component
        /// </summary>
        public Vector3 Omega;

        /// <summary>
        /// The instance sequence value for this object (number of logins for players)
        /// </summary>
        public ushort ObjectInstanceSequence;

        /// <summary>
        /// The vector sequence value for this object
        /// </summary>
        public ushort ObjectVectorSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            Velocity = new Vector3();
            Velocity.ReadFromBuffer(buffer);
            Omega = new Vector3();
            Omega.ReadFromBuffer(buffer);
            ObjectInstanceSequence = BinaryHelpers.ReadUInt16(buffer);
            ObjectVectorSequence = BinaryHelpers.ReadUInt16(buffer);
        }

    }

    /// <summary>
    /// Applies a sound effect.
    /// </summary>
    public class Effects_SoundEvent_S2C : IACMessageType {
        /// <summary>
        /// ID of the object from which the effect originates. Can be you, another char/npc or an item.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The sound type ID, which is referenced in the Sound Table.
        /// </summary>
        public int SoundType;

        /// <summary>
        /// Volume to play the sound
        /// </summary>
        public float Volume;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            SoundType = BinaryHelpers.ReadInt32(buffer);
            Volume = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Instructs the client to show the portal graphic.
    /// </summary>
    public class Effects_PlayerTeleport_S2C : IACMessageType {
        /// <summary>
        /// The teleport sequence value for the object
        /// </summary>
        public ushort ObjectTeleportSequence;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectTeleportSequence = BinaryHelpers.ReadUInt16(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Sets an autonomy level
    /// </summary>
    public class Movement_AutonomyLevel_C2S : IACMessageType {
        /// <summary>
        /// Seems to be 0, 1 or 2. I think 0/1 is server controlled, 2 is client controlled
        /// </summary>
        public uint AutonomyLevel;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            AutonomyLevel = BinaryHelpers.ReadUInt32(buffer);
            if ((buffer.BaseStream.Position % 4) != 0) {
                buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);
            }
        }

    }

    /// <summary>
    /// Sends an autonomous position
    /// </summary>
    public class Movement_AutonomousPosition_C2S : IACMessageType {
        /// <summary>
        /// Set of autonomous position data
        /// </summary>
        public AutonomousPositionPack Position;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Position = new AutonomousPositionPack();
            Position.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Instructs the client to play a script. (Not seen so far, maybe prefered PlayScriptType)
    /// </summary>
    public class Effects_PlayScriptID_S2C : IACMessageType {
        /// <summary>
        /// ID of the object to play the script
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// ID of script to be played
        /// </summary>
        public uint ScriptId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ScriptId = BinaryHelpers.ReadPackedDWORD(buffer);
        }

    }

    /// <summary>
    /// Applies an effect with visual and sound by providing the type to be looked up in the Physics Script Table
    /// </summary>
    public class Effects_PlayScriptType_S2C : IACMessageType {
        /// <summary>
        /// ID of the object from which the effect originates. Can be you, another char/npc or an item.
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// The script type id
        /// </summary>
        public int ScriptId;

        /// <summary>
        /// Speed to play the particle effect at.  1.0 is default, lower for slower, higher for faster.
        /// </summary>
        public float Speed;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ScriptId = BinaryHelpers.ReadInt32(buffer);
            Speed = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Account has been banned
    /// </summary>
    public class Login_AccountBanned_S2C : IACMessageType {
        /// <summary>
        /// Timestamp when ban expires, or 0 for permaban
        /// </summary>
        public uint BannedUntil;

        /// <summary>
        /// I believe this will be empty (len=1) in current version
        /// </summary>
        public string OldText;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            BannedUntil = BinaryHelpers.ReadUInt32(buffer);
            OldText = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// The user has clicked &#39;Enter&#39;. This message does not contain the ID of the character logging on; that comes later.
    /// </summary>
    public class Login_SendEnterWorldRequest_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Performs a non autonomous jump
    /// </summary>
    public class Movement_Jump_NonAutonomous_C2S : IACMessageType {
        /// <summary>
        /// Power of jump
        /// </summary>
        public float Extent;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Extent = BinaryHelpers.ReadSingle(buffer);
        }

    }

    /// <summary>
    /// Admin Receive Account Data
    /// </summary>
    public class Admin_ReceiveAccountData_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint Unknown;

        /// <summary>
        /// Set of account data
        /// </summary>
        public PackableList<AdminAccountData> AdminAccountData;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Unknown = BinaryHelpers.ReadUInt32(buffer);
            AdminAccountData = new PackableList<AdminAccountData>();
            AdminAccountData.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Admin Receive Player Data
    /// </summary>
    public class Admin_ReceivePlayerData_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public int Unknown;

        /// <summary>
        /// Set of player data
        /// </summary>
        public PackableList<AdminPlayerData> AdminPlayerData;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Unknown = BinaryHelpers.ReadInt32(buffer);
            AdminPlayerData = new PackableList<AdminPlayerData>();
            AdminPlayerData.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Sent if player is an PSR, I assume it displays the server version number
    /// </summary>
    public class Admin_SendAdminGetServerVersion_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Seems to be a legacy friends command, /friends old, for when Jan 2006 event changed the friends list
    /// </summary>
    public class Social_SendFriendsCommand_C2S : IACMessageType {
        /// <summary>
        /// Only 0 is used in client, I suspect it is list/display
        /// </summary>
        public uint Command;

        /// <summary>
        /// I assume it would be used to pass a friend to add/remove.  Display passes null string.
        /// </summary>
        public string Name;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Command = BinaryHelpers.ReadUInt32(buffer);
            Name = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Admin command to restore a character
    /// </summary>
    public class Admin_SendAdminRestoreCharacter_C2S : IACMessageType {
        /// <summary>
        /// ID of character to restore
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// Name of character to restore
        /// </summary>
        public string RestoredCharName;

        /// <summary>
        /// Account name to restore the character on
        /// </summary>
        public string AccountToRestoreTo;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            RestoredCharName = BinaryHelpers.ReadString(buffer);
            AccountToRestoreTo = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Update an existing object&#39;s data.
    /// </summary>
    public class Item_UpdateObject_S2C : IACMessageType {
        /// <summary>
        /// the object being updated
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// updated model data
        /// </summary>
        public ObjDesc ObjectDesc;

        /// <summary>
        /// updated physics data
        /// </summary>
        public PhysicsDesc PhysicsDesc;

        /// <summary>
        /// updated game data
        /// </summary>
        public PublicWeenieDesc WeenieDesc;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ObjectId = BinaryHelpers.ReadUInt32(buffer);
            ObjectDesc = new ObjDesc();
            ObjectDesc.ReadFromBuffer(buffer);
            PhysicsDesc = new PhysicsDesc();
            PhysicsDesc.ReadFromBuffer(buffer);
            WeenieDesc = new PublicWeenieDesc();
            WeenieDesc.ReadFromBuffer(buffer);
        }

    }

    /// <summary>
    /// Account has been booted
    /// </summary>
    public class Login_AccountBooted_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public string AdditionalReasonText;

        /// <summary>
        /// TODO
        /// </summary>
        public string ReasonText;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            AdditionalReasonText = BinaryHelpers.ReadString(buffer);
            ReasonText = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Send or receive a message using Turbine Chat.
    /// </summary>
    public class Communication_TurbineChat_S2C : IACMessageType {
        /// <summary>
        /// the number of bytes that follow after this DWORD
        /// </summary>
        public uint MessageSize;

        /// <summary>
        /// the type of data contained in this message
        /// </summary>
        public TurbineChatType Type;

        /// <summary>
        /// TODO
        /// </summary>
        public uint BlobDispatchType;

        /// <summary>
        /// TODO
        /// </summary>
        public int TargetType;

        /// <summary>
        /// TODO
        /// </summary>
        public int TargetId;

        /// <summary>
        /// TODO
        /// </summary>
        public int TransportType;

        /// <summary>
        /// TODO
        /// </summary>
        public int TransportId;

        /// <summary>
        /// TODO
        /// </summary>
        public int Cookie;

        /// <summary>
        /// the number of bytes that follow after this DWORD
        /// </summary>
        public uint PayloadSize;

        /// <summary>
        /// the channel number of the message
        /// </summary>
        public uint RoomId;

        /// <summary>
        /// the name of the player sending the message
        /// </summary>
        public string DisplayName;

        /// <summary>
        /// the message text
        /// </summary>
        public string Text;

        /// <summary>
        /// the number of bytes that follow after this DWORD
        /// </summary>
        public uint ExtraDataSize;

        /// <summary>
        /// the object ID of the player sending the message
        /// </summary>
        public uint ObjectId;

        /// <summary>
        /// TODO
        /// </summary>
        public int Result;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ChatType;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ContextId;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ResponseId;

        /// <summary>
        /// TODO
        /// </summary>
        public uint MethodId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            MessageSize = BinaryHelpers.ReadUInt32(buffer);
            Type = (TurbineChatType)BinaryHelpers.ReadUInt32(buffer);
            BlobDispatchType = BinaryHelpers.ReadUInt32(buffer);
            TargetType = BinaryHelpers.ReadInt32(buffer);
            TargetId = BinaryHelpers.ReadInt32(buffer);
            TransportType = BinaryHelpers.ReadInt32(buffer);
            TransportId = BinaryHelpers.ReadInt32(buffer);
            Cookie = BinaryHelpers.ReadInt32(buffer);
            PayloadSize = BinaryHelpers.ReadUInt32(buffer);
            switch ((int)Type) {
                case 0x01:
                    switch ((int)BlobDispatchType) {
                        case 0x01:
                            RoomId = BinaryHelpers.ReadUInt32(buffer);
                            DisplayName = BinaryHelpers.ReadWString(buffer);
                            Text = BinaryHelpers.ReadWString(buffer);
                            ExtraDataSize = BinaryHelpers.ReadUInt32(buffer);
                            ObjectId = BinaryHelpers.ReadUInt32(buffer);
                            Result = BinaryHelpers.ReadInt32(buffer);
                            ChatType = BinaryHelpers.ReadUInt32(buffer);
                            break;
                    }
                    break;
                case 0x03:
                    switch ((int)BlobDispatchType) {
                        case 0x02:
                            ContextId = BinaryHelpers.ReadUInt32(buffer);
                            ResponseId = BinaryHelpers.ReadUInt32(buffer);
                            MethodId = BinaryHelpers.ReadUInt32(buffer);
                            RoomId = BinaryHelpers.ReadUInt32(buffer);
                            Text = BinaryHelpers.ReadWString(buffer);
                            ExtraDataSize = BinaryHelpers.ReadUInt32(buffer);
                            ObjectId = BinaryHelpers.ReadUInt32(buffer);
                            Result = BinaryHelpers.ReadInt32(buffer);
                            ChatType = BinaryHelpers.ReadUInt32(buffer);
                            break;
                    }
                    break;
                case 0x05:
                    switch ((int)BlobDispatchType) {
                        case 0x01:
                            ContextId = BinaryHelpers.ReadUInt32(buffer);
                            ResponseId = BinaryHelpers.ReadUInt32(buffer);
                            MethodId = BinaryHelpers.ReadUInt32(buffer);
                            Result = BinaryHelpers.ReadInt32(buffer);
                            break;
                        case 0x02:
                            ContextId = BinaryHelpers.ReadUInt32(buffer);
                            ResponseId = BinaryHelpers.ReadUInt32(buffer);
                            MethodId = BinaryHelpers.ReadUInt32(buffer);
                            Result = BinaryHelpers.ReadInt32(buffer);
                            break;
                    }
                    break;
            }
        }

    }

    /// <summary>
    /// Switch from the character display to the game display.
    /// </summary>
    public class Login_EnterGame_ServerReady_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Display a message in the chat window.
    /// </summary>
    public class Communication_TextboxString_S2C : IACMessageType {
        /// <summary>
        /// the message text
        /// </summary>
        public string Text;

        /// <summary>
        /// the message type, controls color and @filter processing
        /// </summary>
        public ChatMessageType Type;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Text = BinaryHelpers.ReadString(buffer);
            Type = (ChatMessageType)BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// The name of the current world.
    /// </summary>
    public class Login_WorldInfo_S2C : IACMessageType {
        /// <summary>
        /// the number of players connected
        /// </summary>
        public uint Connections;

        /// <summary>
        /// the max number of players allowed to connect
        /// </summary>
        public uint MaxConnections;

        /// <summary>
        /// the name of the current world
        /// </summary>
        public string WorldName;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Connections = BinaryHelpers.ReadUInt32(buffer);
            MaxConnections = BinaryHelpers.ReadUInt32(buffer);
            WorldName = BinaryHelpers.ReadString(buffer);
        }

    }

    /// <summary>
    /// Add or update a dat file Resource.
    /// </summary>
    public class DDD_DataMessage_S2C : IACMessageType {
        /// <summary>
        /// which dat file should store this resource
        /// </summary>
        public ResourceType IdDatFile;

        /// <summary>
        /// the resource type
        /// </summary>
        public uint ResourceType;

        /// <summary>
        /// the resource ID number
        /// </summary>
        public uint ResourceId;

        /// <summary>
        /// the file version number
        /// </summary>
        public uint IdIteration;

        /// <summary>
        /// the type of compression used
        /// </summary>
        public CompressionType Compression;

        /// <summary>
        /// version of some sort
        /// </summary>
        public uint Version;

        /// <summary>
        /// the number of bytes required for the remainder of this message, including this DWORD
        /// </summary>
        public uint DataSize;

        /// <summary>
        /// TODO
        /// </summary>
        public List<byte> Data = new List<byte>();

        /// <summary>
        /// the size of the uncompressed file
        /// </summary>
        public uint FileSize;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            IdDatFile = (ResourceType)BinaryHelpers.ReadInt64(buffer);
            ResourceType = BinaryHelpers.ReadUInt32(buffer);
            ResourceId = BinaryHelpers.ReadPackedDWORD(buffer);
            IdIteration = BinaryHelpers.ReadUInt32(buffer);
            Compression = (CompressionType)BinaryHelpers.ReadByte(buffer);
            Version = BinaryHelpers.ReadUInt32(buffer);
            DataSize = BinaryHelpers.ReadUInt32(buffer);
            switch ((int)Compression) {
                case 0x00:
                    for (var i = 0; i < DataSize; i++) {
                        var dbyte = BinaryHelpers.ReadByte(buffer);
                        Data.Add(dbyte);
                    }
                    break;
                case 0x01:
                    FileSize = BinaryHelpers.ReadUInt32(buffer);
                    for (var i = 0; i < DataSize; i++) {
                        var dbyte = BinaryHelpers.ReadByte(buffer);
                        Data.Add(dbyte);
                    }
                    break;
            }
        }

    }

    /// <summary>
    /// DDD request for data
    /// </summary>
    public class DDD_RequestDataMessage_C2S : IACMessageType {
        /// <summary>
        /// the resource type
        /// </summary>
        public uint ResourceType;

        /// <summary>
        /// the resource ID number
        /// </summary>
        public uint ResourceId;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ResourceType = BinaryHelpers.ReadUInt32(buffer);
            ResourceId = BinaryHelpers.ReadPackedDWORD(buffer);
        }

    }

    /// <summary>
    /// DDD error
    /// </summary>
    public class DDD_ErrorMessage_S2C : IACMessageType {
        /// <summary>
        /// the resource type
        /// </summary>
        public uint ResourceType;

        /// <summary>
        /// the resource ID number
        /// </summary>
        public uint ResourceId;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Error;

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ResourceType = BinaryHelpers.ReadUInt32(buffer);
            ResourceId = BinaryHelpers.ReadPackedDWORD(buffer);
            Error = BinaryHelpers.ReadUInt32(buffer);
        }

    }

    /// <summary>
    /// Add or update a dat file Resource.
    /// </summary>
    public class DDD_InterrogationMessage_S2C : IACMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint ServersRegion;

        /// <summary>
        /// TODO
        /// </summary>
        public uint NameRuleLanguage;

        /// <summary>
        /// TODO
        /// </summary>
        public uint ProductId;

        /// <summary>
        /// Number of supported languanges
        /// </summary>
        public uint SupportedLanguagesCount;

        /// <summary>
        /// TODO
        /// </summary>
        public List<uint> supportedLanguages = new List<uint>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ServersRegion = BinaryHelpers.ReadUInt32(buffer);
            NameRuleLanguage = BinaryHelpers.ReadUInt32(buffer);
            ProductId = BinaryHelpers.ReadUInt32(buffer);
            SupportedLanguagesCount = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < SupportedLanguagesCount; i++) {
                var language = BinaryHelpers.ReadUInt32(buffer);
                supportedLanguages.Add(language);
            }
        }

    }

    /// <summary>
    /// TODO
    /// </summary>
    public class DDD_InterrogationResponseMessage_C2S : IACMessageType, IACOutgoingMessageType {
        /// <summary>
        /// TODO
        /// </summary>
        public uint ClientLanguage;

        /// <summary>
        /// TODO
        /// </summary>
        public uint Count;

        /// <summary>
        /// TODO
        /// </summary>
        public List<long> Files = new List<long>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            ClientLanguage = BinaryHelpers.ReadUInt32(buffer);
            Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Count; i++) {
                var idDataFile = BinaryHelpers.ReadInt64(buffer);
                Files.Add(idDataFile);
            }
        }

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void WriteToBuffer(BinaryWriter writer) {
            // TODO: dont hardcode this

            writer.Write(1);
            writer.Write(3);

            // portal
            writer.Write(0);
            writer.Write(1);
            writer.Write(0x00000818u);
            writer.Write(0xfffff7e8u);
            writer.Write(1);

            // client_local_english
            writer.Write(1);
            writer.Write(3);
            writer.Write(0x000003e2u);
            writer.Write(0xfffffc1eu);
            writer.Write(1);

            // cell
            writer.Write(1);
            writer.Write(2);
            writer.Write(0x000003d6u);
            writer.Write(0xfffffc2au);
            writer.Write(1);

            writer.Write(0ul);
        }

    }

    /// <summary>
    /// A list of dat files that need to be patched
    /// </summary>
    public class DDD_BeginDDDMessage_S2C : IACMessageType {
        /// <summary>
        /// Amount of data expected
        /// </summary>
        public uint DataExpected;

        /// <summary>
        /// Total number of revisions to follow
        /// </summary>
        public uint RevisionCount;

        /// <summary>
        /// Vector collection data for the revisions vector
        /// </summary>
        public class Revision {
            /// <summary>
            /// Dat File header offset 0x0150, Dat File header offset 0x014C
            /// </summary>
            public ulong IdDatFile;

            /// <summary>
            /// The corresponding Dat file revision for this patch set
            /// </summary>
            public uint IdIteration;

            /// <summary>
            /// Number of resources to add in this revision
            /// </summary>
            public uint IdsToDownloadCount;

            /// <summary>
            /// TODO
            /// </summary>
            public List<uint> IdsToDownload = new List<uint>();

            /// <summary>
            /// Number of resources to purge in this revision
            /// </summary>
            public uint IdsToPurgeCount;

            /// <summary>
            /// TODO
            /// </summary>
            public List<uint> IdsToPurge = new List<uint>();

        }

        /// <summary>
        /// TODO
        /// </summary>
        public List<Revision> Revisions = new List<Revision>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            DataExpected = BinaryHelpers.ReadUInt32(buffer);
            RevisionCount = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < RevisionCount; i++) {
                var idDatFile = BinaryHelpers.ReadUInt64(buffer);
                var idIteration = BinaryHelpers.ReadUInt32(buffer);
                var IDsToDownloadCount = BinaryHelpers.ReadUInt32(buffer);
                var IDsToDownload = new List<uint>();
                for (var x = 0; x < IDsToDownloadCount; x++) {
                    var resource = BinaryHelpers.ReadPackedDWORD(buffer);
                    IDsToDownload.Add(resource);
                }
                var IDsToPurgeCount = BinaryHelpers.ReadUInt32(buffer);
                var IDsToPurge = new List<uint>();
                for (var x = 0; x < IDsToPurgeCount; x++) {
                    var resource = BinaryHelpers.ReadPackedDWORD(buffer);
                    IDsToPurge.Add(resource);
                }
                Revisions.Add(new Revision() {
                    IdDatFile = idDatFile,
                    IdIteration = idIteration,
                    IdsToDownloadCount = IDsToDownloadCount,
                    IdsToDownload = IDsToDownload,
                    IdsToPurgeCount = IDsToPurgeCount,
                    IdsToPurge = IDsToPurge,
                });
            }
        }

    }

    /// <summary>
    /// Ends DDD update
    /// </summary>
    public class DDD_OnEndDDD_S2C : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

    /// <summary>
    /// Ends DDD update
    /// </summary>
    public class DDD_EndDDDMessage_C2S : IACMessageType {
        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
        }

    }

}