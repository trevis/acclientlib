﻿using WattleScript.Interpreter;
using WattleScript.Interpreter.Loaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.ScriptEnvs.Lua;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting {
    public class UBScript : IDisposable {

        public enum State {
            Running,
            Paused,
            Stopped
        }

        public class LogEventArgs : EventArgs {
            public string Text { get; }

            public LogEventArgs(string text) {
                Text = text;
            }
        }

        private ILogger _log;

        public string Name { get; }
        public string Directory { get; }
        public ScriptContext Context { get; }
        public Script WattleScript;
        private bool didInit;
        public CancellationTokenSource CancellationTokenSource { get; private set; }
        public CancellationToken CancellationToken { get; private set; }

        public State CurrentState { get; private set; }

        public event EventHandler<LogEventArgs> OnLogText;

        public UBScript(string name, string directory, ScriptContext context, ILogger logger) {
            CancellationTokenSource = new CancellationTokenSource();
            CancellationToken = CancellationTokenSource.Token;

            _log = logger;
            Name = name;
            Directory = directory;
            Context = context;
        }

        private void Log(string text, LogLevel level = LogLevel.Information) {
            text = text.Replace("\t", " ");
            text = text.Replace(Directory.TrimEnd('\\') + '\\', "");
            _log.Log(level, $"[Script:{Name}] {text.Replace("\t", " ")}");
            OnLogText?.Invoke(this, new LogEventArgs(text.Replace("\t", " ")));
        }

        async internal Task Init() {
            if (didInit)
                return;

            WattleScript = new Script(CoreModules.Preset_SoftSandbox | CoreModules.Debug);
            WattleScript.Options.DebugPrint = (s) => {
                Log(s, LogLevel.Information);
            };
            WattleScript.Options.UseLuaErrorLocations = false;
            WattleScript.Options.ScriptLoader = new LuaScriptLoader(this);
            WattleScript.Options.AutoAwait = true;
            WattleScript.Options.Syntax = ScriptSyntax.Lua;
            WattleScript.Options.CheckThreadAccess = true;
            //WattleScript.Options.InstructionLimit = 0;

            Context.PopulateDefaultGlobals();

            foreach (var kv in Context.Globals) {
                WattleScript.Globals.Set(kv.Key, kv.Value);
            }

            CurrentState = State.Running;

            await LoadDirectoryScripts();
            didInit = true;
        }

        async private Task LoadDirectoryScripts() {
            if (Name.Equals("Global") || string.IsNullOrEmpty(Directory))
                return;

            // todo: support for projectinfo.json that allows for customized start script?
            var indexScript = "index.lua";
            var scriptPath = Path.Combine(Directory, indexScript);

            if (File.Exists(scriptPath)) {
                await RunFileAsync(scriptPath);
            }
            else {
                throw new Exception($"Could not find script: {scriptPath}");
            }
        }

        public void Pause() {
            CurrentState = State.Paused;
        }

        public void Unpause() {
            CurrentState = State.Running;
        }

        public DynValue RequireFile(string filename) {
            DynValue result = DynValue.Nil;
            try {
                var scriptPath = Path.Combine(Directory, filename);
                //Context.GameInterface.LogText($"Running script file {scriptPath}");
                result = WattleScript.DoFile(scriptPath);
            }
            catch (ScriptRuntimeException ex) {
                Log($"An error occured! {ex.DecoratedMessage}", LogLevel.Error);
            }
            catch (SyntaxErrorException ex) {
                Log($"A syntax error occured! {ex.DecoratedMessage}", LogLevel.Error);
            }
            catch (Exception ex) {
                Log($"Error running script:\n{ex}", LogLevel.Error);
            }

            return result;
        }

        async public Task<object> RunFileAsync(string filename) {
            DynValue result = DynValue.Nil;
            try {
                var scriptPath = Path.Combine(Directory, filename);
                //Context.GameInterface.LogText($"Running script file {scriptPath}");
                result = await WattleScript.DoFileAsync(scriptPath, CancellationToken);
            }
            catch (OperationCanceledException ex) {}
            catch (ScriptRuntimeException ex) {
                Log($"An error occured! {ex.DecoratedMessage}", LogLevel.Error);
                foreach (var x in ex.CallStack) {
                    Log($"  - {x.Location}", LogLevel.Error);
                }
            }
            catch (SyntaxErrorException ex) {
                Log($"A syntax error occured! {ex.DecoratedMessage}", LogLevel.Error);
            }
            catch (Exception ex) {
                Log($"Error running script:\n{ex}", LogLevel.Error);
            }

            return FixResult(result);
        }

        async public Task<object> RunTextAsync(string scriptCode) {
            DynValue result = DynValue.Nil;
            try {
                //Context.GameInterface.LogText($"Running script string: {scriptCode}");
                result = await WattleScript.DoStringAsync(scriptCode, CancellationToken);
            }
            catch (OperationCanceledException ex) {}
            catch (ScriptRuntimeException ex) {
                Log($"An error occured! {ex.DecoratedMessage}", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }
            catch (SyntaxErrorException ex) {
                Log($"A syntax error occured! {ex.DecoratedMessage}", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }
            catch (Exception ex) {
                Log($"Error running text:\n{ex}", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }

            return FixResult(result);
        }

        async public Task<object> RunTextAsyncNoCatch(string scriptCode) {
            return FixResult(await WattleScript.DoStringAsync(scriptCode, CancellationToken));
        }

        public object RunFile(string filename) {
            DynValue result = DynValue.Nil;
            try {
                var scriptPath = Path.Combine(Directory, filename);
                //Context.GameInterface.LogText($"Running script file {scriptPath}");
                result = WattleScript.DoFile(scriptPath);
            }
            catch (ScriptRuntimeException ex) {
                Log($"An error occured! {ex.DecoratedMessage}", LogLevel.Error);
            }
            catch (SyntaxErrorException ex) {
                Log($"A syntax error occured! {ex.DecoratedMessage}", LogLevel.Error);
            }
            catch (Exception ex) {
                Log($"Error running script:\n{ex}", LogLevel.Error);
            }

            return FixResult(result);
        }

        public object RunText(string scriptCode) {
            DynValue result = DynValue.Nil;
            try {
                //Context.GameInterface.LogText($"Running script string: {scriptCode}");
                result = WattleScript.DoString(scriptCode);
            }
            catch (ScriptRuntimeException ex) {
                Log($"An error occured! {ex.DecoratedMessage}", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }
            catch (SyntaxErrorException ex) {
                Log($"A syntax error occured! {ex.DecoratedMessage}", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }
            catch (Exception ex) {
                Log($"Error running text:\n{ex}", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }

            return FixResult(result);
        }

        public object RunTextNoCatch(string scriptCode) {
            return FixResult(WattleScript.DoString(scriptCode));
        }

        private object FixResult(DynValue result) {
            if (result.IsNil()) {
                return null;
            }

            switch (result.Type) {
                case DataType.Boolean:
                    return result.Boolean;
                case DataType.ClrFunction:
                    return result.Callback;
                case DataType.Function:
                    return result.Function;
                case DataType.Nil:
                    return null;
                case DataType.Number:
                    return result.Number;
                case DataType.String:
                    return result.String;
                case DataType.Table:
                    return result.Table;
                case DataType.TailCallRequest:
                    return result.TailCallData;
                case DataType.Thread:
                    return result.Coroutine;
                case DataType.Tuple:
                    return result.Tuple.Select(r => FixResult(r)).ToList();
                case DataType.UserData:
                    return result.UserData.Object;
                case DataType.Void:
                    return null;
                case DataType.YieldRequest:
                    return result.YieldRequest;
                default:
                    return result;
            }
        }

        public void Dispose() {
            try {
                CurrentState = State.Stopped;
                //WattleScript.Options.InstructionLimit = 1;
                CancellationTokenSource?.Cancel();
                Context.Dispose();
            }
            catch (Exception ex) { ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString()); }
        }

        internal void ThrowError(string text, Exception ex) {
            if (ex is ScriptRuntimeException scriptRuntimeEx) {
                Log($"{text}: {scriptRuntimeEx.DecoratedMessage}", LogLevel.Error);
            }
            else if (ex is SyntaxErrorException syntaxErrorEx) {
                Log($"{text}: {syntaxErrorEx.DecoratedMessage}", LogLevel.Error);
            }
            else {
                Log($"{text}: {ex.Message}", LogLevel.Error);
            }
        }
    }
}
