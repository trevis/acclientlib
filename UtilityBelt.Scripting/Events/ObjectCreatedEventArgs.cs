﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// ObjectCreatedEventArgs
    /// </summary>
    public class ObjectCreatedEventArgs : System.EventArgs {
        /// <summary>
        /// The object id that was created
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="objectId"></param>
        public ObjectCreatedEventArgs(uint objectId) {
            ObjectId = objectId;
        }
    }
}
