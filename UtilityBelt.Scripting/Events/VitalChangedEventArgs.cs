﻿using System;
using UtilityBelt.Common.Enums;

namespace UtilityBelt.Scripting.Events {
    public class VitalChangedEventArgs : EventArgs {

        /// <summary>
        /// The vital that changed
        /// </summary>
        public VitalId Type { get; }

        /// <summary>
        /// The new vital current value
        /// </summary>
        public int Value { get; }

        /// <summary>
        /// The old vital current value
        /// </summary>
        public int OldValue { get; }

        internal VitalChangedEventArgs(VitalId type, int value, int oldValue) {
            Type = type;
            Value = value;
            OldValue = oldValue;
        }
    }
}
