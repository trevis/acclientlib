﻿using System;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Information about your character's available luminance
    /// </summary>
    public class AvailableLuminanceChangedEventArgs : EventArgs {
        /// <summary>
        /// Your character's current available luminance
        /// </summary>
        public long AvailableLuminance { get; }

        /// <summary>
        /// Your character's old available luminance
        /// </summary>
        public long OldAvailableLuminance { get; }

        internal AvailableLuminanceChangedEventArgs(long availableLuminance, long oldAvailableLuminance) {
            AvailableLuminance = availableLuminance;
            OldAvailableLuminance = oldAvailableLuminance;
        }
    }
}