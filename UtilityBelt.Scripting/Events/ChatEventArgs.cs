﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Fired when we get a chat event from the server
    /// </summary>
    public class ChatEventArgs : EventArgs {
        /// <summary>
        /// The id of the sender, if available
        /// </summary>
        public uint SenderId { get; }

        /// <summary>
        /// The name of the sender, if available
        /// </summary>
        public string SenderName { get; }

        /// <summary>
        /// The message
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// The chat room, if available.
        /// </summary>
        public ChatChannel Room { get; }

        /// <summary>
        /// The message type
        /// </summary>
        public ChatMessageType Type { get; }

        /// <summary>
        /// Set to true to "eat" this message and prevent the client from displaying it.
        /// </summary>
        public bool Eat { get; set; } = false;

        public ChatEventArgs(ChatChannel room, ChatMessageType type, uint senderId, string senderName, string message) {
            SenderId = senderId;
            SenderName = senderName;
            Message = message;
            Room = room;
            Type = type;
        }
    }
}
