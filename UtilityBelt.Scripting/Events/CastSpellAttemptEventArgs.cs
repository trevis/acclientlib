﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Events {
    public class CastSpellAttemptEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the spell that was cast by your character
        /// </summary>
        public uint SpellId { get; }

        /// <summary>
        /// The target id of the spell that was cast
        /// </summary>
        public uint TargetId { get; }
        
        /// <summary>
        /// The type of spell cast (targeted / untargeted )
        /// </summary>
        public CastEventType EventType { get; set; }
        
        internal CastSpellAttemptEventArgs(uint spellId, uint targetId, CastEventType eventType) {
            SpellId = spellId;
            TargetId = targetId;
            EventType = eventType;
        }
    }
}
