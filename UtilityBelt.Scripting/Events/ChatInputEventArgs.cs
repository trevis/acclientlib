﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Event Args for when text is typed into the chatbox and sent.
    /// </summary>
    public class ChatInputEventArgs : EventArgs {
        /// <summary>
        /// The text that was sent to the chat input box.
        /// </summary>
        public string Text { get; internal set; }

        /// <summary>
        /// Set to true to "eat" this input and prevent it from being sent to the server.
        /// </summary>
        public bool Eat { get; set; } = false;

        internal ChatInputEventArgs(string text) {
            Text = text;
        }
    }
}
