﻿namespace UtilityBelt.Scripting.Events {
    public class ScriptEventArgs : System.EventArgs {
        public UBScript Script { get; }

        public ScriptEventArgs(UBScript script) {
            Script = script;
        }
    }
}