﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    public class TradeResetEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the person who reset the trade. Can be either you or your current trade partnet.
        /// </summary>
        public uint ResetterObjectId { get; }

        public TradeResetEventArgs(uint resetterObjectId) {
            ResetterObjectId = resetterObjectId;
        }
    }
}
