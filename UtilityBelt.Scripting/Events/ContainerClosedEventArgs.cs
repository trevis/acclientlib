﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// ContainerClosedEventArgs
    /// </summary>
    public class ContainerClosedEventArgs : System.EventArgs {
        /// <summary>
        /// The container weenie that was closed
        /// </summary>
        public WorldObject Container { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="container"></param>
        public ContainerClosedEventArgs(WorldObject container) {
            Container = container;
        }
    }
}
