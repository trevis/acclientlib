﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    public class TradeStartedEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the person who initiated the trade.
        /// </summary>
        public uint InitiatorId { get; }

        /// <summary>
        /// The id of the person receiving the trade
        /// </summary>
        public uint PartnerId { get; }

        public TradeStartedEventArgs(uint initiatorId, uint partnerId) {
            InitiatorId = initiatorId;
            PartnerId = partnerId;
        }
    }
}
