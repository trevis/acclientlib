﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    public class MessageBytesEventArgs : System.EventArgs {
        public byte[] Data { get; }

        public MessageBytesEventArgs(byte[] data) {
            Data = data;
        }
    }
}
