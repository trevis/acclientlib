﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Events {
    public class ObjectFailedToAddToTradeEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the object you attempted to add to the trade window
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The reason the item could not be added to the trade window
        /// </summary>
        public WeenieError Reason { get; }

        public ObjectFailedToAddToTradeEventArgs(uint objectId, WeenieError reason) {
            ObjectId = objectId;
            Reason = reason;
        }
    }
}
