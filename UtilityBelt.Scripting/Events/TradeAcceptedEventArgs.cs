﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    public class TradeAcceptedEventArgs : System.EventArgs {
        /// <summary>
        /// The id of the player who is accepting the trade.
        /// </summary>
        public uint AcceptorId { get; }

        public TradeAcceptedEventArgs(uint acceptorId) {
            AcceptorId = acceptorId;
        }
    }
}
