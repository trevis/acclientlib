﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Event args for when a green/clickable chat name is clicked (in the chat window)
    /// </summary>
    public class ChatNameClickedEventArgs : EventArgs {

        /// <summary>
        /// The id included in the clickable chat name. Usually this is an id of a player, but certain scripts/plugins
        /// may be using custom data here.
        /// </summary>
        public uint Id { get; }

        /// <summary>
        /// The display text of the clickable name. Usually this is the name of a player, but certain scripts/plugins
        /// may be using custom data here.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Set to true to "eat" this click and prevent the client from handling it.
        /// </summary>
        public bool Eat { get; set; } = false;

        internal ChatNameClickedEventArgs(uint id, string text) {
            Id = id;
            Text = text;
        }
    }
}
