﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Client state changed
    /// </summary>
    public class StateChangedEventArgs : System.EventArgs {
        /// <summary>
        /// The new state the client is in
        /// </summary>
        public ClientState NewState { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="newState"></param>
        public StateChangedEventArgs(ClientState newState) {
            NewState = newState;
        }
    }
}
