﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Event args for when a script requests custom directory access
    /// </summary>
    public class DirectoryAccessRequestEventArgs : EventArgs {
        /// <summary>
        /// The name of the script requesting access
        /// </summary>
        public string ScriptName { get; }

        /// <summary>
        /// The filepath of the directory being requested access to
        /// </summary>
        public string Directory { get; }

        /// <summary>
        /// The reason the script is requesting access.
        /// </summary>
        public string Reason { get; }

        /// <summary>
        /// The callback to be called when access is granted / denied.
        /// </summary>
        public Action<bool, string> Callback { get; }

        public DirectoryAccessRequestEventArgs(string scriptName, string directory, string reason, Action<bool, string> callback) {
            ScriptName = scriptName;
            Directory = directory;
            Reason = reason;
            Callback = callback;
        }
    }
}
