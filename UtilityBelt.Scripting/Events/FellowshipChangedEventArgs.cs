﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Events {
    public class FellowshipChangedEventArgs : System.EventArgs {

        /// <summary>
        /// The type of fellowship change
        /// </summary>
        public AddRemoveEventType Type { get; }

        /// <summary>
        /// The object id of the player that joined or left
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The fellowship member object
        /// </summary>
        public FellowshipMember Member { get; }

        internal FellowshipChangedEventArgs(AddRemoveEventType type, uint objectId, FellowshipMember member) {
            Type = type;
            ObjectId = objectId;
            Member = member;
        }
    }
}
