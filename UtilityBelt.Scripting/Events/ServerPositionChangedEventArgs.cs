﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Events {
    public class ServerPositionChangedEventArgs : System.EventArgs {
        /// <summary>
        /// The weenie that changed positions
        /// </summary>
        public WorldObject Weenie { get; }

        /// <summary>
        /// The new position
        /// </summary>
        public Position Position { get; }

        public ServerPositionChangedEventArgs(WorldObject weenie, Position position) {
            Weenie = weenie;
            Position = position;
        }
    }
}
