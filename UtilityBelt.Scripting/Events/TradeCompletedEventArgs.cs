﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    public class TradeCompletedEventArgs : System.EventArgs {
        /// <summary>
        /// The id of your trade partner
        /// </summary>
        public uint PartnerId { get; }

        /// <summary>
        /// A list of object ids that you traded away. (The objects your partner is trading for)
        /// </summary>
        public List<uint> YourItemIds { get; }

        /// <summary>
        /// A list of object ids that you traded for. (The objects your partner traded away)
        /// </summary>
        public List<uint> PartnerItemIds { get; }

        public TradeCompletedEventArgs(uint partnerId, List<uint> yourItemIds, List<uint> partnerItemIds) {
            PartnerId = partnerId;
            YourItemIds = yourItemIds;
            PartnerItemIds = partnerItemIds;
        }
    }
}
