﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Event arguments for when object cooldowns have been changed
    /// </summary>
    public class SharedCooldownsChangedEventArgs : EventArgs {
        /// <summary>
        /// Wether the enchantment was added or removed
        /// </summary>
        public AddRemoveEventType Type { get; }

        /// <summary>
        /// Enchantment information
        /// </summary>
        public Interop.SharedCooldown Cooldown { get; }

        internal SharedCooldownsChangedEventArgs(AddRemoveEventType type, Interop.SharedCooldown cooldown) {
            Type = type;
            Cooldown = cooldown;
        }
    }
}
