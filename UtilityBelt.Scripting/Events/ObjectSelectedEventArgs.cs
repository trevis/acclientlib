﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Event args for when an object is selected
    /// </summary>
    public class ObjectSelectedEventArgs : EventArgs {

        /// <summary>
        /// The id of the selected object
        /// </summary>
        public uint ObjectId { get; set; }

        internal ObjectSelectedEventArgs(uint objectId) {
            ObjectId = objectId;
        }
    }
}
