﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;

namespace UtilityBelt.Scripting.Events {
    public class TradeEndedEventArgs : System.EventArgs {
        /// <summary>
        /// The reason the trade was ended.
        /// </summary>
        public EndTradeReason Reason { get; }

        public TradeEndedEventArgs(EndTradeReason reason) {
            Reason = reason;
        }
    }
}
