﻿using System;

namespace UtilityBelt.Scripting.Events {
    public class TotalExperienceChangedEventArgs : EventArgs {
        /// <summary>
        /// Your character's current total experience
        /// </summary>
        public long TotalExperience { get; }

        /// <summary>
        /// Your character's old total experience
        /// </summary>
        public long OldTotalExperience { get; }

        internal TotalExperienceChangedEventArgs(long totalExperience, long oldTotalExperience) {
            TotalExperience = totalExperience;
            OldTotalExperience = oldTotalExperience;
        }
    }
}