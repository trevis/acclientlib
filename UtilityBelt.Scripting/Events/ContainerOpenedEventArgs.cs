﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// ContainerOpenedEventArgs
    /// </summary>
    public class ContainerOpenedEventArgs : System.EventArgs {
        /// <summary>
        /// The container weenie that was opened
        /// </summary>
        public WorldObject Container { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="container"></param>
        public ContainerOpenedEventArgs(WorldObject container) {
            Container = container;
        }
    }
}
