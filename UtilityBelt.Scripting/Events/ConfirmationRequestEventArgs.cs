﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Events {
    /// <summary>
    /// Event args for when an acclient popup confirmation dialog is shown
    /// </summary>
    public class ConfirmationRequestEventArgs : EventArgs {
        /// <summary>
        /// The id included in the clickable chat name. Usually this is an id of a player, but certain scripts/plugins
        /// may be using custom data here.
        /// </summary>
        public ClientConfirmationType Type { get; }

        /// <summary>
        /// The text in the confirmation dialog popup
        /// </summary>
        public string Text { get; } = "What the fuck";

        /// <summary>
        /// Set to true to click yes
        /// </summary>
        public bool ClickYes { get; set; } = false;

        /// <summary>
        /// Set to true to click no
        /// </summary>
        public bool ClickNo { get; set; } = false;

        [WattleScript.Interpreter.WattleScriptHidden]
        public ConfirmationRequestEventArgs(ClientConfirmationType type, string text) {
            Type = type;
            Text = text;
        }
    }
}
