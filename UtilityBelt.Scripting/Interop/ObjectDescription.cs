﻿using System.Collections.Generic;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Interop {
    public class ObjectDescription {
        public byte Version { get; internal set; }

        public byte PaletteCount { get; internal set; }

        public byte TextureCount { get; internal set; }

        public byte ModelCount { get; internal set; }

        public uint Palette { get; internal set; }

        public List<Subpalette> Subpallettes { get; } = new List<Subpalette>();

        public List<TextureMapChange> TMChanges { get; } = new List<TextureMapChange>();

        public List<AnimPartChange> APChanges { get; } = new List<AnimPartChange>();
    }
}