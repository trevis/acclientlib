﻿using ACE.Entity;
using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Lib;
using static UtilityBelt.Scripting.Lib.WorldState;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about the world around your character. This is the class that scripts get access to at `game.world`.
    /// </summary>
    public class World {
        private ScriptManager _manager;

        #region public events
        /// <summary>
        /// Fired when something is typed into the chat input box and sent. This will trigger when a player manually
        /// types something into the box and hits enter, as well as when a script uses the InvokeChat action.
        /// </summary>
        public event EventHandler<ChatInputEventArgs> OnChatInput;

        /// <summary>
        /// Fired when an object is selected in the client.
        /// </summary>
        public event AsyncEventHandler<ObjectSelectedEventArgs> OnObjectSelected;
        /// <summary>
        /// Fired when a green / clickable name is clicked in chat.
        /// </summary>
        public event EventHandler<ChatNameClickedEventArgs> OnChatNameClicked;

        /// <summary>
        /// Fired when your character opens a landscape container.
        /// </summary>
        public event EventHandler<ContainerOpenedEventArgs> OnContainerOpened;

        /// <summary>
        /// Fired when your character closes a landscape container.
        /// </summary>
        public event EventHandler<ContainerClosedEventArgs> OnContainerClosed;

        /// <summary>
        /// Fired once every second (ish).
        /// </summary>
        public event EventHandler<EventArgs> OnTick;

        /// <summary>
        /// Fired when the client learns about a new weenie instance.
        /// </summary>
        public event EventHandler<ObjectCreatedEventArgs> OnObjectCreated;

        /// <summary>
        /// Fired when the client forgets about a weenie instance (because it can't be seen, doesn't mean
        /// it was actually destroyed).
        /// </summary>
        public event EventHandler<ObjectReleasedEventArgs> OnObjectReleased;

        /// <summary>
        /// Fired when a new "channel" message is received. Channel messages are ones in
        /// general / trade / lfg / roleplay / allegiance / society
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnChannelMessage;

        /// <summary>
        /// This fires for all "chat" messages from the sevrer. like server motd, tells, all
        /// speech / emotes, etc. Check room / type to see what it is.
        /// to see what it is.
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnChatText;

        /// <summary>
        /// Fired when someone directly messages you
        /// </summary>
        public event EventHandler<ChatEventArgs> OnTell;

        /// <summary>
        /// Fired when someone says something locally, like with /say.
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnLocalMessage;

        /// <summary>
        /// Fired when someone sends a custom emote
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnEmote;

        /// <summary>
        /// Fired when someone uses a builtin emote like *atoyot*
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnSoulEmote;

        /// <summary>
        /// Fired when something is said in the fellowship chat. Note that
        /// SenderId is not available.  If the name is blank, you are the
        /// sender.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnFellowMessage;

        /// <summary>
        /// Fired when a confirmation popup dialog is shown. Use evtArgs.ClickYes=true to click yes, or evtArgs.ClickNo=true to click no.
        /// </summary>
        public event EventHandler<ConfirmationRequestEventArgs> OnConfirmationRequest;
        #endregion // public events

        /// <summary>
        /// The currently opened container, if any
        /// </summary>
        public WorldObject OpenContainer => _manager.GameState.WorldState.OpenContainer;

        /// <summary>
        /// Information about the physics environment around you
        /// </summary>
        public PhysicsEnvironment PhysicsEnvironment => _manager.GameState.WorldState.PhysicsEnvironment;

        /// <summary>
        /// Information about the currently open vendor, if any
        /// </summary>
        public Vendor Vendor => _manager.GameState.WorldState.Vendor;

        /// <summary>
        /// The currently selected weenie
        /// </summary>
        public WorldObject Selected => Get(_manager.Resolve<IClientActionsRaw>().SelectedObject());

        internal World() { }
        internal World(ScriptManager manager) {
            _manager = manager;

            _manager.GameState.WorldState.OnTick += WorldState_OnTick;
            _manager.GameState.WorldState.OnContainerClosed += WorldState_OnContainerClosed;
            _manager.GameState.WorldState.OnContainerOpened += WorldState_OnContainerOpened;
            _manager.GameState.WorldState.OnWeenieCreated += WorldState_OnWeenieCreated;
            _manager.GameState.WorldState.OnWeenieReleased += WorldState_OnWeenieReleased;
            _manager.GameState.WorldState.OnChannelMessage += WorldState_OnChannelMessage;
            _manager.GameState.WorldState.OnChatText += WorldState_OnChatText;
            _manager.GameState.WorldState.OnEmote += WorldState_OnEmote;
            _manager.GameState.WorldState.OnFellowMessage += WorldState_OnFellowMessage;
            _manager.GameState.WorldState.OnLocalMessage += WorldState_OnLocalMessage;
            _manager.GameState.WorldState.OnSoulEmote += WorldState_OnSoulEmote;
            _manager.GameState.WorldState.OnTell += WorldState_OnTell;
            _manager.GameState.WorldState.OnChatInput += WorldState_OnChatInput;
            _manager.GameState.WorldState.OnObjectSelected += WorldState_OnObjectSelected;
            _manager.GameState.WorldState.OnChatNameClicked += WorldState_OnChatNameClicked;
            _manager.GameState.WorldState.OnConfirmationRequest += WorldState_OnConfirmationRequest;
        }

        #region public api
        /// <summary>
        /// Get a weenie with the specified object id
        /// </summary>
        /// <param name="objectId">The spell id to get</param>
        /// <returns>The weenie object, or null if it doesn't exist</returns>
        public virtual WorldObject this[uint objectId] {
            get {
                return _manager.GameState.WorldState?.GetWeenie(objectId);
            }
        }

        /// <summary>
        /// Get a object by id
        /// </summary>
        /// <param name="objectId">The object id to get</param>
        /// <returns>the object, or null if it doesnt exist</returns>
        public WorldObject Get(uint objectId) {
            return _manager.GameState.WorldState?.GetWeenie(objectId);
        }

        /// <summary>
        /// Try to get an object by id
        /// </summary>
        /// <param name="objectId">The weenie id to get</param>
        /// <param name="weenie">The found object (or null if not found)</param>
        /// <returns>true if the object was found, false otherwise</returns>
        public bool TryGet(uint objectId, out WorldObject weenie) {
            return _manager.GameState.WorldState.TryGetWeenie(objectId, out weenie);
        }

        /// <summary>
        /// Check if an object exists
        /// </summary>
        /// <param name="objectId">The id of the object to check</param>
        /// <returns>true if the object is known to the client, false otherwise</returns>
        public bool Exists(uint objectId) {
            return _manager.GameState.WorldState.WeenieExists(objectId);
        }

        /// <summary>
        /// Gets a collections of all objects the client knows about
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetAll() {
            return _manager.GameState.WorldState.AllWeenies.ToList();
        }

        /// <summary>
        /// Gets a collections of all weenies that match the specified type
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetAll(ObjectType type) {
            return GetAll().Where(w => w.ObjectType == type).ToList();
        }

        /// <summary>
        /// Gets a collections of all weenies that match the specified object class
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetAll(ObjectClass objectClass) {
            return GetAll().Where(w => w.ObjectClass == objectClass).ToList();
        }

        /// <summary>
        /// Gets a collections of all weenies that match the specified matchFunction.
        /// return true from matchFunction to desigate the object as a match
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetAll(Func<WorldObject, bool> matchFunction) {
            return GetAll().Where(matchFunction).ToList();
        }

        /// <summary>
        /// Gets the nearest landscape world object of the specified ObjectType. This will never return your own character.
        /// </summary>
        /// <param name="objectType">The ObjectType to filter by</param>
        /// <param name="distanceType">Wether to use 2d or 3d distance</param>
        /// <returns>The closest WorldObject of the specified type, or null if none found.</returns>
        public WorldObject GetNearest(ObjectType objectType, DistanceType distanceType = DistanceType.T3D) {
            var me = ScriptManager.Instance.GameState.Character.Weenie;
            return GetAll().Where(w => w.ObjectType == objectType && w.Id != me.Id)
                .OrderBy(w => distanceType == DistanceType.T2D ? me.DistanceTo2D(w) : me.DistanceTo3D(w)).FirstOrDefault();
        }

        /// <summary>
        /// Gets the nearest landscape world object of the specified ObjectClass. This will never return your own character.
        /// </summary>
        /// <param name="objectClass">The ObjectClass to filter by</param>
        /// <param name="distanceType">Wether to use 2d or 3d distance</param>
        /// <returns>The closest WorldObject of the specified type, or null if none found.</returns>
        public WorldObject GetNearest(ObjectClass objectClass, DistanceType distanceType = DistanceType.T3D) {
            var me = ScriptManager.Instance.GameState.Character.Weenie;
            return GetAll().Where(w => w.ObjectClass == objectClass && w.Id != me.Id)
                .OrderBy(w => distanceType == DistanceType.T2D ? me.DistanceTo2D(w) : me.DistanceTo3D(w)).FirstOrDefault();
        }

        /// <summary>
        /// Gets the nearest landscape world object of the specified name. This is case-sensitive. This will never return your own character.
        /// </summary>
        /// <param name="name">The name to filter by</param>
        /// <param name="distanceType">Wether to use 2d or 3d distance</param>
        /// <returns>The closest WorldObject of the specified type, or null if none found.</returns>
        public WorldObject GetNearest(string name, DistanceType distanceType = DistanceType.T3D) {
            var me = ScriptManager.Instance.GameState.Character.Weenie;
            return GetAll().Where(w => w.Name == name && w.Id != me.Id)
                .OrderBy(w => distanceType == DistanceType.T2D ? me.DistanceTo2D(w) : me.DistanceTo3D(w)).FirstOrDefault();
        }

        /// <summary>
        /// Gets the nearest landscape world object that matches the specified filter function. This will never return your own character.
        /// </summary>
        /// <param name="matchFunction">The function to filter by</param>
        /// <param name="distanceType">Wether to use 2d or 3d distance</param>
        /// <returns>The closest WorldObject of the specified type, or null if none found.</returns>
        public WorldObject GetNearest(Func<WorldObject, bool> matchFunction, DistanceType distanceType = DistanceType.T3D) {
            var me = ScriptManager.Instance.GameState.Character.Weenie;
            return GetAll().Where(w => matchFunction(w) && w.Id != me.Id)
                .OrderBy(w => distanceType == DistanceType.T2D ? me.DistanceTo2D(w) : me.DistanceTo3D(w)).FirstOrDefault();
        }

        /// <summary>
        /// Gets a collections of all landscape (parent container == 0) objects the client knows about
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetLandscape() {
            return GetAll().Where(w => w.ContainerId == 0).ToList();
        }

        /// <summary>
        /// Gets a collections of all landscape (parent container == 0) weenies that match the specified type
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetLandscape(ObjectType type) {
            return GetLandscape().Where(w => w.ObjectType == type).ToList();
        }

        /// <summary>
        /// Gets a collections of all landscape (parent container == 0) weenies that match the specified object class
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetLandscape(ObjectClass objectClass) {
            return GetLandscape().Where(w => w.ObjectClass == objectClass).ToList();
        }

        /// <summary>
        /// Gets a collections of all landscape (parent container == 0) weenies that match the specified matchFunction.
        /// return true from matchFunction to desigate the object as a match
        /// </summary>
        /// <returns>A collection of objects</returns>
        public IList<WorldObject> GetLandscape(Func<WorldObject, bool> matchFunction) {
            return GetLandscape().Where(matchFunction).ToList();
        }
        #endregion // public api

        #region event handlers
        private void WorldState_OnConfirmationRequest(object sender, ConfirmationRequestEventArgs e) {
            OnConfirmationRequest?.InvokeSafely(this, e);
        }

        private void WorldState_OnChatNameClicked(object sender, ChatNameClickedEventArgs e) {
            OnChatNameClicked?.InvokeSafely(this, e);
        }

        private async Task WorldState_OnObjectSelected(object sender, ObjectSelectedEventArgs e) {
            if (OnObjectSelected is not null) await OnObjectSelected(this, e);
            //OnObjectSelected?.InvokeSafely(this, e);
            //return Task.CompletedTask;
        }
        private void WorldState_OnChatInput(object sender, ChatInputEventArgs e) {
            OnChatInput?.InvokeSafely(this, e);
        }
        private void WorldState_OnWeenieReleased(object sender, ObjectReleasedEventArgs e) {
            OnObjectReleased?.InvokeSafely(this, e);
        }

        private void WorldState_OnWeenieCreated(object sender, ObjectCreatedEventArgs e) {
            OnObjectCreated?.InvokeSafely(this, e);
        }

        private void WorldState_OnContainerOpened(object sender, ContainerOpenedEventArgs e) {
            OnContainerOpened?.InvokeSafely(this, e);
        }

        private void WorldState_OnContainerClosed(object sender, ContainerClosedEventArgs e) {
            OnContainerClosed?.InvokeSafely(this, e);
        }

        private void WorldState_OnTick(object sender, EventArgs e) {
            OnTick?.InvokeSafely(this, e);
        }

        private void WorldState_OnSoulEmote(object sender, ChatEventArgs e) {
            OnSoulEmote?.InvokeSafely(this, e);
        }

        private void WorldState_OnLocalMessage(object sender, ChatEventArgs e) {
            OnLocalMessage?.InvokeSafely(this, e);
        }

        private void WorldState_OnFellowMessage(object sender, ChatEventArgs e) {
            OnFellowMessage?.InvokeSafely(this, e);
        }

        private void WorldState_OnEmote(object sender, ChatEventArgs e) {
            OnEmote?.InvokeSafely(this, e);
        }

        private void WorldState_OnTell(object sender, ChatEventArgs e) {
            OnTell?.InvokeSafely(this, e);
        }

        private void WorldState_OnChatText(object sender, ChatEventArgs e) {
            OnChatText?.InvokeSafely(this, e);
        }

        private void WorldState_OnChannelMessage(object sender, ChatEventArgs e) {
            OnChannelMessage?.InvokeSafely(this, e);
        }
        #endregion // event handlers

        internal void Dispose() {
            if (_manager?.GameState?.WorldState == null)
                return;

            _manager.GameState.WorldState.OnTick -= WorldState_OnTick;
            _manager.GameState.WorldState.OnContainerClosed -= WorldState_OnContainerClosed;
            _manager.GameState.WorldState.OnContainerOpened -= WorldState_OnContainerOpened;
            _manager.GameState.WorldState.OnWeenieCreated -= WorldState_OnWeenieCreated;
            _manager.GameState.WorldState.OnWeenieReleased -= WorldState_OnWeenieReleased;
            _manager.GameState.WorldState.OnChannelMessage -= WorldState_OnChannelMessage;
            _manager.GameState.WorldState.OnChatText -= WorldState_OnChatText;
            _manager.GameState.WorldState.OnEmote -= WorldState_OnEmote;
            _manager.GameState.WorldState.OnFellowMessage -= WorldState_OnFellowMessage;
            _manager.GameState.WorldState.OnLocalMessage -= WorldState_OnLocalMessage;
            _manager.GameState.WorldState.OnSoulEmote -= WorldState_OnSoulEmote;
            _manager.GameState.WorldState.OnTell -= WorldState_OnTell;
            _manager.GameState.WorldState.OnChatInput -= WorldState_OnChatInput;
            _manager.GameState.WorldState.OnObjectSelected -= WorldState_OnObjectSelected;
            _manager.GameState.WorldState.OnChatNameClicked -= WorldState_OnChatNameClicked;
            _manager.GameState.WorldState.OnConfirmationRequest -= WorldState_OnConfirmationRequest;
        }
    }
}