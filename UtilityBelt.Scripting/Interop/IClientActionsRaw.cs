﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Provides a *raw* interface to perform client actions. There are no safety checks or callbacks.
    /// This should be implemented by custom clients to allow ubscripts to perform actions.
    /// Scripts should use the [ClientActions](xref:UBScript.Interop.ClientActions) interface, exposed at [Game.Actions](xref:UBScript.Interop.Game#UBScript_Interop_Game_Actions).
    /// </summary>
    public interface IClientActionsRaw : IDisposable {
        /// <summary>
        /// Get's the currently selected weenie in the client. Can be 0
        /// </summary>
        /// <returns>The id of the currently selected weenie</returns>
        uint SelectedObject();

        /// <summary>
        /// Increase the training of the specified skill
        /// </summary>
        /// <param name="skill">The skill to increase training of.</param>
        /// <param name="creditsToSpend">Amount of credits to spend</param>
        void SkillAdvance(SkillId skill, uint creditsToSpend);

        /// <summary>
        /// Break allegiance from your patron
        /// </summary>
        /// <param name="objectId">The objectId of the player to break allegiance from</param>
        void AllegianceBreak(uint objectId);

        /// <summary>
        /// Swear allegiance to a character
        /// </summary>
        /// <param name="objectId">The objectId of the player to swear allegiance to</param>
        void AllegianceSwear(uint objectId);

        /// <summary>
        /// Login the specified character by id. Must be at the character select screen.
        /// </summary>
        /// <param name="id">Id of the character to log in</param>
        void Login(uint id);

        /// <summary>
        /// Adds experience towards raising an attribute.
        /// </summary>
        /// <param name="attribute">The attribute to spend experience towards</param>
        /// <param name="experienceToSpend">The amount of experience to spend</param>
        void AttributeAddExperience(AttributeId attribute, uint experienceToSpend);

        /// <summary>
        /// Adds experience towards raising a skill.
        /// </summary>
        /// <param name="skill">The skill to spend experience towards</param>
        /// <param name="experienceToSpend">The amount of experience to spend</param>
        void SkillAddExperience(SkillId skill, uint experienceToSpend);

        /// <summary>
        /// Adds experience towards raising a vital.
        /// </summary>
        /// <param name="vital">The vital to spend experience towards</param>
        /// <param name="experienceToSpend">The amount of experience to spend</param>
        void VitalAddExperience(VitalId vital, uint experienceToSpend);

        /// <summary>
        /// Attempt to use one item on another.
        /// </summary>
        /// <param name="sourceWeenieId">source weenie id</param>
        /// <param name="targetWeenieId">target weenie id</param>
        void ApplyWeenie(uint sourceWeenieId, uint targetWeenieId);

        /// <summary>
        /// Attempt to wield a weenie (weapons / armor). The weenie must be in your inventory or the landscape.
        /// </summary>
        /// <param name="weenieId">the weenie id to wield</param>
        /// <param name="slot">The slot to place this weenie in. EquipMask.None for auto</param>
        void AutoWield(uint weenieId, EquipMask slot);

        /// <summary>
        /// Attempt to cast a spell by id. If this is an untargeted spell, set targetId to 0.
        /// </summary>
        /// <param name="spellId">SpellId to cast</param>
        /// <param name="targetId">TargetId to cast on (if none use 0)</param>
        void CastSpell(uint spellId, uint targetId);

        /// <summary>
        /// Try to switch to the specified combat mode. You must have the appropriate weapon type equipped first.
        /// </summary>
        /// <param name="combatMode">CombatMode to switch to</param>
        void SetCombatMode(CombatMode combatMode);

        /// <summary>
        /// Try and drop the specified weenie id. It must be in your inventory.
        /// </summary>
        /// <param name="weenieId">The weenie id to attempt to drop</param>
        void DropObject(uint weenieId);

        /// <summary>
        /// Try and split an object
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="targetId"></param>
        /// <param name="newStackSize"></param>
        /// <param name="slot"></param>
        void SplitObject(uint objectId, uint targetId, uint newStackSize, uint slot = 0);

        /// <summary>
        /// Attempt to create a fellowship.
        /// </summary>
        /// <param name="name">Name of the fellowship</param>
        /// <param name="shareExperience">Wether or not to share experience</param>
        void FellowshipCreate(string name, bool shareExperience);

        /// <summary>
        /// Attempt to disband your current fellowship. You must be the leader.
        /// </summary>
        void FellowshipDisband();

        /// <summary>
        /// Attempt to dismiss a member from the fellowship. You must be the leader
        /// </summary>
        /// <param name="objectId">The objectId of the member you want to dismiss</param>
        void FellowshipDismiss(uint objectId);

        /// <summary>
        /// Attempt to give leadership to another membership of the fellow. You must be the leader.
        /// </summary>
        /// <param name="objectId">The objectId of the member you want to give leadership to.</param>
        void FellowshipSetLeader(uint objectId);

        /// <summary>
        /// Attempt to quit your current fellowship.
        /// </summary>
        /// <param name="disband">Wether or not to disband the fellowship while quitting. If you specify true you must be the leader</param>
        void FellowshipQuit(bool disband);

        /// <summary>
        /// Attempt to recruit a member to the fellowship. The fellowship must be open (or you must be the leader).
        /// </summary>
        /// <param name="objectId">The objectId of the player to recruit</param>
        void FellowshipRecruit(uint objectId);

        /// <summary>
        /// Attempt to set the openness of the fellowship. Must be the leader.
        /// </summary>
        /// <param name="open">The openness to set the fellowship to</param>
        void FellowshipSetOpen(bool open);

        /// <summary>
        /// Attempt to give a weenie to another player/npc
        /// </summary>
        /// <param name="weenieId">The weenie id of the item to give</param>
        /// <param name="targetWeenieId">The weenie id of the target</param>
        void GiveWeenie(uint weenieId, uint targetWeenieId);

        /// <summary>
        /// Attempt to send a command to the chat window parser.  (As if you were typing into the chat box in a client)
        /// </summary>
        /// <param name="text">The text to send to the chat parser</param>
        void InvokeChatParser(string text);

        /// <summary>
        /// Attempt to log out the character
        /// </summary>
        void Logout();

        /// <summary>
        /// Attempt to move a weenie to another container.
        /// </summary>
        /// <param name="weenieId">The weenie id of the item to move</param>
        /// <param name="containerWeenieId">The weenie id of the container to move the item to</param>
        /// <param name="slot">The slot in the container to place the item. Slot 0 is the first slot.</param>
        /// <param name="stack">Wether or not to attempt to merge a stacked item with an existing stack in the container</param>
        void MoveWeenie(uint weenieId, uint containerWeenieId, uint slot, bool stack);

        /// <summary>
        /// Request assessment data for an object
        /// </summary>
        /// <param name="weenieId">The id of the weenie to request assessment data for</param>
        void Appraise(uint weenieId);

        /// <summary>
        /// Adds a salvageable item to the salvage panel.
        /// </summary>
        /// <param name="objectId">The id of the item to add to the salvage panel</param>
        void SalvagePanelAdd(uint objectId);

        /// <summary>
        /// Salvages the items in the salvage panel
        /// </summary>
        void SalvagePanelSalvage();

        /// <summary>
        /// Select a weenie. This call is client side, nothing gets sent to the server.
        /// </summary>
        /// <param name="weenieId">The id of the weenie to select</param>
        void SelectWeenie(uint weenieId);

        /// <summary>
        /// Attempt to use the specified weenie
        /// </summary>
        /// <param name="weenieId">The id of the weenie to use</param>
        void UseWeenie(uint weenieId);

        /// <summary>
        /// Enable / disable autorunning
        /// </summary>
        /// <param name="enabled">Wether to enable or not</param>
        void SetAutorun(bool enabled);

        /// <summary>
        /// Mark the current trade as accepted.
        /// </summary>
        void TradeAccept();

        /// <summary>
        /// Adds the specified objectId to your side of the trade window
        /// </summary>
        /// <param name="objectId">The id of the object to add</param>
        void TradeAdd(uint objectId);

        /// <summary>
        /// Decline the current trade. This does not end the trade, just removes acceptance.
        /// </summary>
        void TradeDecline();

        /// <summary>
        /// Ends the current trade.
        /// </summary>
        void TradeEnd();

        /// <summary>
        /// Reset the current trade window. Clears all trade items and acceptance on both sides.
        /// </summary>
        void TradeReset();

        /// <summary>
        /// Add a vendor object / template id to the buy list.
        /// </summary>
        /// <param name="objectId">The id of the object / template to add to the vendor buy list</param>
        /// <param name="amount">The amount to add</param>
        void VendorAddToBuyList(uint objectId, uint amount = 1);

        /// <summary>
        /// Add an object id to the vendor sell list.
        /// </summary>
        /// <param name="objectId">The id of the object to add to the vendor sell list</param>
        void VendorAddToSellList(uint objectId);

        /// <summary>
        /// Buys all the items in the vendor buy list
        /// </summary>
        public void VendorBuyAll();

        /// <summary>
        /// Sells all the items in the vendor sell list
        /// </summary>
        public void VendorSellAll();

        /// <summary>
        /// Clears out the vendor buy list
        /// </summary>
        public void VendorClearBuyList();

        /// <summary>
        /// Clears out the vendor sell list
        /// </summary>
        public void VendorClearSellList();

        /// <summary>
        /// Send a tell to an object id. This is used for spell professors
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="message"></param>
        public void SendTellByObjectId(uint objectId, string message);

        /// <summary>
        /// Accept the next confirmation request popup
        /// </summary>
        public void AcceptNextConfirmationRequest();
    }
}
