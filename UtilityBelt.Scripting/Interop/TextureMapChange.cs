﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Interop {
    public struct TextureMapChange {
        /// <summary>
        /// The part index that is changing textures
        /// </summary>
        public byte PartIndex { get; internal set; }

        /// <summary>
        /// The old texture id
        /// </summary>
        public uint OldTexID { get; internal set; }

        /// <summary>
        /// The new texture id
        /// </summary>
        public uint NewTexID { get; internal set; }
    }
}
