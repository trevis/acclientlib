﻿using ACE.DatLoader.FileTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Scripting.Interop {
    public interface IAsyncDatReader {
        public SecondaryAttributeTable SecondaryAttributeTable { get; }
        public CharGen CharGen { get; }
        public SkillTable SkillTable { get; }
        public SpellComponentsTable SpellComponentsTable { get; }
        public SpellTable SpellTable { get; }
        public XpTable XpTable { get; }
        public DualDidMapper DualDidMapper { get; }

        void Init();

        Task<T> ReadFromDat<T>(uint fileId) where T : FileType, new();
    }
}