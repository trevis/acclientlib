﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Interop {
    public class Vendor : IDisposable {
        private readonly ScriptManager _manager;
        private readonly ILogger _log;

        /// <summary>
        /// Raised when a vendor window is opened.
        /// </summary>
        public event EventHandler<System.EventArgs> OnOpened;

        /// <summary>
        /// Raised when a vendor window is closed.
        /// </summary>
        public event EventHandler<System.EventArgs> OnClosed;

        /// <summary>
        /// Returns true if a vendor window is open
        /// </summary>
        public bool IsOpen { get; private set; }

        /// <summary>
        /// The objectId of the currently opened vendor
        /// </summary>
        public uint VendorId { get; private set; }

        /// <summary>
        /// The lowest value item this vendor will buy
        /// </summary>
        public uint MinBuyValue { get; private set; }

        /// <summary>
        /// The maximum value item this vendor will buy
        /// </summary>
        public uint MaxBuyValue { get; private set; }

        /// <summary>
        /// Magic ?? (I think this is a flag for if the vendor sells magical items, why do we care?)
        /// </summary>
        public uint Magic { get; private set; }

        /// <summary>
        /// This vendor's buy price modifier. (for items the vendor is buying) (rate * itemValue)
        /// </summary>
        public float BuyPriceModifier { get; private set; }

        /// <summary>
        /// This vendor's sell price modifier. (for items the vendor is selling) (rate * itemValue)
        /// </summary>
        public float SellPriceModifier { get; private set; }

        /// <summary>
        /// The weenie class id of the currency this vendor accepts
        /// </summary>
        public uint CurrencyType { get; private set; }

        /// <summary>
        /// The amount of vendor currency the current character has
        /// </summary>
        public uint PlayerCurrencyAmount { get; private set; }

        /// <summary>
        /// The name of the currency this vendor accepts
        /// </summary>
        public string CurrencyName { get; private set; }

        /// <summary>
        /// The type of items this vendor will buy
        /// </summary>
        public ObjectType Category { get; private set; }

        /// <summary>
        /// The items the vendor has for sale
        /// </summary>
        public List<ItemProfile> Items { get; private set; } = new List<ItemProfile>();

        internal Vendor() {
            _manager = ScriptManager.Instance;
            _log = _manager.Resolve<ILogger>();

            _manager.MessageHandler.Incoming.Vendor_VendorInfo += MessageHandler_Vendor_VendorInfo_S2C;
        }

        #region Public API
        public void AddToBuyList() { }
        #endregion

        private void MessageHandler_Vendor_VendorInfo_S2C(object sender, Vendor_VendorInfo_S2C_EventArgs e) {
            var vendor = _manager.GameState.WorldState.GetWeenie(e.Data.ObjectId);
            if (vendor == null)
                return;

            if (VendorId != 0 && VendorId != e.Data.ObjectId) {
                ClearVendor();
            }

            vendor.OnPositionChanged += Vendor_OnPositionChanged;
            vendor.OnDestroyed += Vendor_OnDestroyed;

            VendorId = e.Data.ObjectId;
            MinBuyValue = e.Data.Profile.MinValue;
            MaxBuyValue = e.Data.Profile.MaxValue;
            Magic = e.Data.Profile.Magic;
            BuyPriceModifier = e.Data.Profile.BuyPrice;
            SellPriceModifier = e.Data.Profile.SellPrice;
            CurrencyType = e.Data.Profile.CurrencyId;
            PlayerCurrencyAmount = e.Data.Profile.CurrencyAmount;
            CurrencyName = e.Data.Profile.CurrencyName;
            Category = e.Data.Profile.ObjectTypes;

            Items = e.Data.Items.Items.ToList();

            if (!IsOpen) {
                IsOpen = true;
                _manager.GameState.Character.Weenie.OnPositionChanged += Weenie_OnPositionChanged;
                OnOpened?.Invoke(this, System.EventArgs.Empty);
            }
        }

        private void Vendor_OnDestroyed(object sender, ObjectReleasedEventArgs e) {
            ClearVendor();
        }

        private void Vendor_OnPositionChanged(object sender, ServerPositionChangedEventArgs e) {
            var vendor = _manager.GameState?.WorldState?.GetWeenie(VendorId);
            if (vendor?.DistanceTo3D(_manager.GameState.Character.Weenie) > vendor.Value(FloatId.UseRadius)) {
                ClearVendor();
            }
        }

        private void ClearVendor() {
            if (VendorId != 0) {
                var vendor = _manager.GameState.WorldState.GetWeenie(VendorId);
                if (vendor != null) {
                    vendor.OnPositionChanged -= Vendor_OnPositionChanged;
                    vendor.OnDestroyed -= Vendor_OnDestroyed;
                }
                _manager.GameState.Character.Weenie.OnPositionChanged -= Weenie_OnPositionChanged;
                IsOpen = false;
                VendorId = 0;
                Items.Clear();
                OnClosed?.Invoke(this, System.EventArgs.Empty);
            }
        }

        private void Weenie_OnPositionChanged(object sender, ServerPositionChangedEventArgs e) {
            var vendor = _manager.GameState.WorldState.GetWeenie(VendorId);

            if (vendor == null || _manager.GameState.Character.Weenie.DistanceTo3D(VendorId) > vendor.Value(FloatId.UseRadius)) {
                ClearVendor();
            }
        }

        public void Dispose() {
            _manager.MessageHandler.Incoming.Vendor_VendorInfo -= MessageHandler_Vendor_VendorInfo_S2C;
        }
    }
}
