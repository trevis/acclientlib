﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;

namespace UtilityBelt.Scripting.Interop {
    public abstract class QueueAction : IEqualityComparer {
        protected ScriptManager Manager => ScriptManager.Instance;
        private bool needsExecute = true;
        internal bool NeedsInit = true;
        private bool needsPreconditions = true;
        private bool isPaused = false;

        /// <summary>
        /// Fired when this action is started
        /// </summary>
        public event EventHandler<EventArgs> OnStarted;

        /// <summary>
        /// Fired when this action is completed.
        /// </summary>
        public event EventHandler<EventArgs> OnFinished;

        /// <summary>
        /// The priority of this action. Higher number is higher priority
        /// </summary>
        public int Priority {
            get => Options.Priority.HasValue ? Options.Priority.Value : (int)ActionType;
            set => Options.Priority = value;
        }

        /// <summary>
        /// Friendly name of this action. Used for debugging.
        /// </summary>
        public string Name => string.IsNullOrEmpty(Options?.Name) ? ToString() : Options.Name;

        /// <summary>
        /// A list of preconditions that were run for this action. You can inspect thier results
        /// in the case of ActionError.PreconditionsFailed
        /// </summary>
        public List<QueueAction> RanPreconditions { get; } = new List<QueueAction>();

        /// <summary>
        /// A queue of required precondition actions that must run 
        /// successfully before this action can run.
        /// </summary>
        public Queue<QueueAction> Preconditions { get; } = new Queue<QueueAction>();

        /// <summary>
        /// Timeout length of this action. Set this in Options.
        /// </summary>
        public TimeSpan Timeout => TimeSpan.FromMilliseconds(Options.TimeoutMilliseconds.Value);

        /// <summary>
        /// Options for this action
        /// </summary>
        public ActionOptions Options { get; }

        /// <summary>
        /// The datetime (utc) that this action was started at. Note that
        /// this is not updated until preconditions have all ran.
        /// </summary>
        public DateTime StartedAt { get; protected set; }

        /// <summary>
        /// The datetime (utc) that this action was finished at. Check IsFinished first.
        /// </summary>
        public DateTime FinishedAt { get; protected set; }

        /// <summary>
        /// True is this action is currently running preconditions
        /// </summary>
        public bool IsRunningPreconditions => Preconditions.Count > 0;

        /// <summary>
        /// True is this action has been started
        /// </summary>
        public bool IsStarted { get; protected set; }

        /// <summary>
        /// True is this action is currently running
        /// </summary>
        public bool IsRunning => IsStarted && !IsFinished;

        /// <summary>
        /// The current number of retries this action has needed.
        /// </summary>
        public int CurrentRetryCount { get; protected set; }

        /// <summary>
        /// True if this action is finished
        /// </summary>
        public bool IsFinished { get; protected set; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public abstract ActionType ActionType { get; }

        /// <summary>
        /// Wether or not this action was successful. Check IsFinished before checking this.
        /// </summary>
        public bool Success => Error == ActionError.None;

        /// <summary>
        /// The error this action currently has, if any
        /// </summary>
        public ActionError Error { get; protected set; } = ActionError.None;

        /// <summary>
        /// More details about the error, if available
        /// </summary>
        public string ErrorDetails { get; protected set; } = string.Empty;

        protected bool IsWaiting { get; set; }
        protected DateTime FinishAt { get; set; }

        private bool _isTemporary;

        protected QueueAction(ActionOptions options) {
            Options = options ?? new ActionOptions();

            UpdateDefaultOptions();

            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 5000;
            if (!Options.MaxRetryCount.HasValue)
                Options.MaxRetryCount = 5;
        }

        /// <summary>
        /// 
        /// </summary>
        internal virtual void Init(ScriptManager manager) {
            if (NeedsInit) {
                NeedsInit = false;
            }
        }

        public async Task<QueueAction> Await() {
            if (!Manager.GameState.ActionQueue.Contains(this)) {
                Manager.GameState.ActionQueue.Add(this);
            }
            await Task.Run(async () => {
                while (!IsFinished) {
                    await Task.Delay(1);
                }
            });
            return this;
        }

        /// <summary>
        /// Mark this action as completed / incompleted. This will not retry the action.
        /// </summary>
        /// <param name="error">Action error</param>
        /// <param name="errorText">Extra error details</param>
        public void SetPermanentResult(ActionError error, string errorText = null) {
            Error = error;
            ErrorDetails = errorText ?? error.ToString();
            _isTemporary = false;
            TryFinish();
        }

        public virtual void SetPermanentResultAfter(TimeSpan timeSpan, ActionError error, string errorText = null) {
            Error = error;
            ErrorDetails = errorText ?? error.ToString();
            IsWaiting = true;
            FinishAt = DateTime.UtcNow + timeSpan;
            _isTemporary = false;
        }

        /// <summary>
        /// Mark this action as completed / incompleted. If incompleted, this
        /// action will retry itself provided that CurrentRetryCount is less 
        /// than NumberRetriesAllowed.
        /// </summary>
        /// <param name="error">Action error</param>
        /// <param name="errorText">Extra error details</param>
        public virtual void SetTemporaryResult(ActionError error, string errorText = null) {
            Error = error;
            ErrorDetails = errorText ?? error.ToString();
            _isTemporary = true;
            TryFinish();
        }

        public virtual void SetTemporaryResultAfter(TimeSpan timeSpan, ActionError error, string errorText = null) {
            Error = error;
            ErrorDetails = errorText ?? error.ToString();
            IsWaiting = true;
            FinishAt = DateTime.UtcNow + timeSpan;
            _isTemporary = true;
        }

        private void RunPreconditions() {
            if (Preconditions.Count == 0)
                return;
            var currentPrecondition = Preconditions.Peek();
            if (!currentPrecondition.IsFinished) {
                if (currentPrecondition.NeedsInit) {
                    currentPrecondition.Init(Manager);
                }
                currentPrecondition.Update();
            }

            if (currentPrecondition.IsFinished) {
                Preconditions.Dequeue();
                RanPreconditions.Add(currentPrecondition);
                if (currentPrecondition.Success) {
                    RunPreconditions();
                    return;
                }
                else {
                    SetPermanentResult(ActionError.PreconditionFailed, currentPrecondition.ErrorDetails);
                    Preconditions.Clear();
                }
            }
            else {
                return;
            }
        }

        /// <summary>
        /// Called once a frame until this action is completed
        /// </summary>
        public virtual void Update() {
            if (IsFinished)
                return;

            if (IsWaiting) {
                if (DateTime.UtcNow >= FinishAt) {
                    TryFinish();
                }

                return;
            }

            // only update preconditions if we haven't already executed the action.
            // it will timeout and get new preconditions next time around.
            if (needsExecute) {
                if (!IsValid()) {
                    return;
                }
                if (needsPreconditions) {
                    UpdatePreconditions();
                    needsPreconditions = false;
                }
            }

            if (IsRunningPreconditions) {
                RunPreconditions();
            }

            if (!IsRunningPreconditions && !IsFinished) {
                if (!IsStarted) {
                    StartedAt = DateTime.UtcNow;
                    OnStarted?.Invoke(this, EventArgs.Empty);
                    IsStarted = true;
                    ScriptManager.Instance?.Resolve<ILogger>().LogTrace($"Action[{Name}] Start");
                    Start();
                }

                if (DateTime.UtcNow - StartedAt >= Timeout) {
                    // ehhhh
                    // todo: better timeout backing off
                    Options.TimeoutMilliseconds = (uint)(Timeout.TotalMilliseconds);
                    SetTemporaryResult(ActionError.TimedOut);
                    return;
                }

                if (needsExecute) {
                    if (ActionType != ActionType.Immediate && Manager.GameState?.Character?.IsBusy() == true) {
                        return;
                    }
                    StartedAt = DateTime.UtcNow;
                    ScriptManager.Instance?.Resolve<ILogger>().LogTrace($"Action[{Name}] Executing");
                    needsExecute = !Execute();
                    if (needsExecute) {
                        needsPreconditions = true;
                    }
                }
            }
        }

        private void TryFinish() {
            if (_isTemporary && !Success && CurrentRetryCount < Options.MaxRetryCount) {
                CurrentRetryCount++;
                StartedAt = DateTime.UtcNow;
                ScriptManager.Instance?.Resolve<ILogger>().LogTrace($"Action[{Name}] Retrying (Current {CurrentRetryCount}/{Options.MaxRetryCount})");
                needsExecute = true;
            }
            else if (!IsFinished) {
                IsFinished = true;
                FinishedAt = DateTime.UtcNow;
                ScriptManager.Instance?.Resolve<ILogger>().LogTrace($"Action[{Name}] Finished (Success:{Success} / Error:{Error}/{ErrorDetails})");
                OnFinished?.Invoke(this, EventArgs.Empty);
                Stop();
            }
        }

        protected abstract void UpdateDefaultOptions();

        /// <summary>
        /// Check if this action is valid. This should return false
        /// if this action can never succeed as well as call SetResult()
        /// </summary>
        public abstract bool IsValid();

        /// <summary>
        /// Allow the action to update its preconditions, if any
        /// </summary>
        protected abstract void UpdatePreconditions();

        /// <summary>
        /// Called once when the action is started (after preconditions have run)
        /// </summary>
        protected abstract void Start();

        /// <summary>
        /// Perform the actual action. This is called everytime the action
        /// needs to be performed, like after the initial Init call or after
        /// the action has temporarily failed and needs to be performed again.
        /// </summary>
        protected abstract bool Execute();

        /// <summary>
        /// Called once when the action is stopped
        /// </summary>
        protected abstract void Stop();

        public virtual new bool Equals(object x, object y) {
            if (x != null && y != null && x.GetType() == y.GetType()) {
                return !JsonConvert.SerializeObject(x).Equals(JsonConvert.SerializeObject(y));
            }
            return false;
        }

        public virtual int GetHashCode(object obj) {
            return JsonConvert.SerializeObject(obj).GetHashCode();
        }

        public override string ToString() {
            /*
            var propValues = new List<string>();

            foreach (var prop in GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)) {
                if (prop.PropertyType.IsEnum || prop.PropertyType.IsPrimitive || prop.Name.Contains("Error")) {
                    var propValue = prop.GetValue(this, null);
                    propValues.Add($"{prop.Name}={(propValue?.ToString() ?? "null")}");
                }
            }

            
            return $"{GetType().Name.Split('.').Last()} {(propValues.Count > 0 ? $" {{{string.Join(", ", propValues.ToArray())}}}]" : "")}";
            */
            return GetType().Name.Split('.').Last();
        }
    }
}
