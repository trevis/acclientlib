﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about a member of a fellowship
    /// </summary>
    public class FellowshipMember {
        /// <summary>
        /// The object id of the fellow member
        /// </summary>
        public uint Id { get; internal set; }

        /// <summary>
        /// Name of the fellow member
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Level of the fellow member
        /// </summary>
        public uint Level { get; internal set; }

        /// <summary>
        /// Max health of the fellow member
        /// </summary>
        public uint MaxHealth { get; internal set; }

        /// <summary>
        /// Max stamina of the fellow member
        /// </summary>
        public uint MaxStamina { get; internal set; }

        /// <summary>
        /// Max mana of the fellow member
        /// </summary>
        public uint MaxMana { get; internal set; }

        /// <summary>
        /// Current health of the fellow member
        /// </summary>
        public uint CurrentHealth { get; internal set; }

        /// <summary>
        /// Current stamina of the fellow member
        /// </summary>
        public uint CurrentStamina { get; internal set; }

        /// <summary>
        /// Current mana of the fellow member
        /// </summary>
        public uint CurrentMana { get; internal set; }
    }
}
