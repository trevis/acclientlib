﻿using ACE.DatLoader;
using ACE.DatLoader.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Reflection;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Lib;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Interop {
    public class Spell {
        private List<SpellComponent> _oldschoolComponents = null;
        private List<SpellComponent> _fociComponents = null;
        private readonly GameState _gameState;

        /// <summary>
        /// Spell Id
        /// </summary>
        public uint Id { get; }

        /// <summary>
        /// Spell name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Spell description
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// The school of magic this spell belongs to
        /// </summary>
        public MagicSchool School { get; private set; }

        /// <summary>
        /// Icon id
        /// </summary>
        public uint Icon { get; private set; }

        /// <summary>
        /// Spell category
        /// </summary>
        public SpellCategory Category { get; private set; }

        /// <summary>
        /// Spell flags
        /// </summary>
        public SpellFlags Flags { get; private set; }

        /// <summary>
        /// Mana cost
        /// </summary>
        public uint BaseMana { get; private set; }

        public float BaseRangeConstant { get; private set; }

        public float BaseRangeMod { get; private set; }

        /// <summary>
        /// Spell power / difficulty
        /// </summary>
        public uint Power { get; private set; }

        public float SpellEconomyMod { get; private set; }

        /// <summary>
        /// What formula version to use when calculating spell components
        /// </summary>
        public uint FormulaVersion { get; private set; }

        public float ComponentLoss { get; private set; }

        public SpellType MetaSpellType { get; private set; }

        public uint MetaSpellId { get; private set; }

        public double Duration { get; private set; }

        public float DegradeModifier { get; private set; }

        public float DegradeLimit { get; private set; }

        /// <summary>
        /// Portal duration for spells of type SpellType.PortalSummon
        /// </summary>
        public double PortalLifetime { get; private set; }

        /// <summary>
        /// A list of component ids for the spell formula. This is the old non-Foci formula.
        /// </summary>
        public List<uint> Formula { get; private set; }

        /// <summary>
        /// The scripting effect to play on the caster
        /// </summary>
        public PlayScript CasterEffect { get; private set; }

        /// <summary>
        /// The scripting effect to play on the target
        /// </summary>
        public PlayScript TargetEffect { get; private set; }

        /// <summary>
        /// The scripting effect to play on the caster when the spell fizzles
        /// </summary>
        public PlayScript FizzleEffect { get; private set; }

        public double RecoveryInterval { get; private set; }

        public float RecoveryAmount { get; private set; }

        public uint DisplayOrder { get; private set; }

        public uint NonComponentTargetType { get; private set; }

        public uint ManaMod { get; private set; }

        /// <summary>
        /// Uses the client spell level formula, which is used for things like spell filtering
        /// a 'rough heuristic' based on the first component of the spell, which is expected to be a scarab
        /// </summary>
        public uint Level {
            get {
                if (Components == null || Components.Count() == 0)
                    return 0;

                var firstComp = Components.First();
                if (firstComp.Category != 0)
                    return 0;

                return ScarabLevel[(Scarab)firstComp.Id];
            }
        }

        /// <summary>
        /// The stat modifier type, usually EnchantmentTypeFlags
        /// </summary>
        public EnchantmentFlags StatModType { get; private set; }

        /// <summary>
        /// The stat modifier key, used for lookup in the enchantment registry
        /// </summary>
        public int StatModKey { get; private set; }

        /// <summary>
        /// The amount to modify a stat
        /// </summary>
        public float StatModVal { get; private set; }

        /// <summary>
        /// The attribute this StatMod is for, if any.
        /// </summary>
        public AttributeId StatModAttribute => (StatModType & EnchantmentFlags.Attribute) != 0 ? (AttributeId)StatModKey : AttributeId.Undef;

        /// <summary>
        /// The vital this StatMod is for, if any.
        /// </summary>
        public UtilityBelt.Common.Enums.Vital StatModVital => (StatModType & EnchantmentFlags.Attribute2nd) != 0 ? (UtilityBelt.Common.Enums.Vital)StatModKey : UtilityBelt.Common.Enums.Vital.Undef;

        /// <summary>
        /// The skill this StatMod is for, if any.
        /// </summary>
        public SkillId StatModSkill => (StatModType & EnchantmentFlags.Skill) != 0 ? (SkillId)StatModKey : SkillId.Undef;

        /// <summary>
        /// The int property this StatMod is for, if any.
        /// </summary>
        public IntId StatModIntProp => (StatModType & EnchantmentFlags.Int) != 0 ? (IntId)StatModKey : IntId.Undef;

        /// <summary>
        /// The float property this StatMod is for, if any.
        /// </summary>
        public FloatId StatModFloatProp => (StatModType & EnchantmentFlags.Float) != 0 ? (FloatId)StatModKey : FloatId.Undef;

        /// <summary>
        /// The damage type for this spell
        /// </summary>
        public DamageType DamageType { get; private set; }

        /// <summary>
        /// The base amount of damage for this spell
        /// </summary>
        public int BaseIntensity { get; private set; }

        /// <summary>
        /// The maximum additional daamage for this spell
        /// </summary>
        public int Variance { get; private set; }

        /// <summary>
        /// The weenie class ID associated for this spell, ie. the projectile weenie class id
        /// </summary>
        public uint WeenieClassId { get; private set; }

        /// <summary>
        /// The total # of projectiles launched for this spell
        /// </summary>
        public int NumProjectiles { get; private set; }

        /// <summary>
        /// The maximum # of additional projectiles possibly launched
        /// </summary>
        public int NumProjectilesVariance { get; private set; }

        /// <summary>
        /// The total angle for multi-projectile spells,
        /// ie. 90 degrees for 3-5 projectiles, or 360 degrees for ring spells
        /// </summary>
        public float SpreadAngle { get; private set; }

        /// <summary>
        /// The vertical angle to launch this spell
        /// </summary>
        public float VerticalAngle { get; private set; }

        /// <summary>
        /// The default angle to launch this spell
        /// (relative to player, or global?)
        /// </summary>
        public float DefaultLaunchAngle { get; private set; }

        /// <summary>
        /// If this is on then projectile spells won't lead a target.
        /// Arc spells have this set to true.
        /// </summary>
        public bool NonTracking { get; private set; }

        /// <summary>
        /// The offset to apply to the spawn position
        /// </summary>
        public Vector3 CreateOffset { get; private set; }

        /// <summary>
        /// The minimum amount of padding to ensure for the spell to spawn
        /// </summary>
        public Vector3 Padding { get; private set; }

        /// <summary>
        /// The dimensions of the origin, used for Volley spells?
        /// </summary>
        public Vector3 Dims { get; private set; }

        /// <summary>
        /// The maximum variation for spawn position
        /// </summary>
        public Vector3 Peturbation { get; private set; }

        /// <summary>
        /// The imbued effect for this spell
        /// </summary>
        public uint ImbuedEffect { get; private set; }

        /// <summary>
        /// The creature class for a slayer spell
        /// </summary>
        public CreatureType SlayerCreatureType { get; private set; }

        /// <summary>
        /// The amount of additional damage for a slayer spell
        /// </summary>
        public float SlayerDamageBonus { get; private set; }

        /// <summary>
        /// The critical chance frequency for this spell
        /// </summary>
        public double CritFrequency { get; private set; }

        /// <summary>
        /// The critical damage multiplier for this spell
        /// </summary>
        public double CritMultiplier { get; private set; } = 1;

        /// <summary>
        /// If TRUE, ignores magic resistance
        /// </summary>
        public bool IgnoreMagicResist { get; private set; }

        /// <summary>
        /// The elemental damage multiplier for this spell
        /// </summary>
        public double ElementalModifier { get; private set; } = 1;

        /// <summary>
        /// The amount of source vital to drain for a life spell
        /// </summary>
        public float DrainPercentage { get; private set; }

        /// <summary>
        /// The percentage of DrainPercentage to damage a target for life projectiles
        /// </summary>
        public float DamageRatio { get; private set; } = 1.0f;

        /// <summary>
        /// DamageType used by LifeMagic spells that specifies Health, Mana, or Stamina for the Boost type spells
        /// </summary>
        public DamageType VitalDamageType { get; private set; }

        /// <summary>
        /// The minimum amount of vital boost from a life spell
        /// </summary>
        public int Boost { get; private set; }

        /// <summary>
        /// Boost + BoostVariance = the maximum amount of vital boost from a life spell
        /// </summary>
        public int BoostVariance { get; private set; }

        /// <summary>
        /// The source vital for a life spell
        /// </summary>
        public UtilityBelt.Common.Enums.Vital Source { get; private set; }

        /// <summary>
        /// The destination vital for a life spell
        /// </summary>
        public UtilityBelt.Common.Enums.Vital Destination { get; private set; }

        /// <summary>
        /// The propotion of source vital to transfer to destination vital
        /// </summary>
        public float Proportion { get; private set; } = 1.0f;

        /// <summary>
        /// The percent of source vital loss for a life magic transfer spell
        /// </summary>
        public float LossPercent { get; private set; }

        /// <summary>
        /// A static amount of source vital loss for a life magic transfer spell?
        /// Unused / unknown?
        /// </summary>
        public int SourceLoss { get; private set; }

        /// <summary>
        /// The maximum amount of vital transferred by a life magic spell
        /// </summary>
        public int TransferCap { get; private set; }

        /// <summary>
        /// The maximum destination vital boost for a life magic transfer spell?
        /// Unused / unknown?
        /// </summary>
        public int MaxBoostAllowed { get; private set; }

        /// <summary>
        /// Indicates the source and destination for life magic transfer spells
        /// </summary>
        public TransferFlags TransferFlags { get; private set; }

        /// <summary>
        /// Unknown index?
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// For SpellType.PortalSummon spells, Link is set to either 1 for LinkedPortalOneDID or 2 for LinkedPortalTwoDID
        /// </summary>
        public int Link { get; private set; }

        /// <summary>
        /// The destination landcell for a spell
        /// </summary>
        public uint DestinationLandcell { get; private set; }

        /// <summary>
        /// The destination origin for a spell
        /// </summary>
        public Vector3 DestinationOrigin { get; private set; }

        /// <summary>
        /// The destination orientation for a spell
        /// </summary>
        public Quaternion DestinationOrientation { get; private set; }

        /// <summary>
        /// The minimum spell power to dispel (unused?)
        /// </summary>
        public int MinPower { get; private set; }

        /// <summary>
        /// The maximum spell power to dispel
        /// </summary>
        public int MaxPower { get; private set; }

        /// <summary>
        /// Possible RNG for spell power to dispel (unused?)
        /// </summary>
        public float PowerVariance { get; private set; }

        /// <summary>
        /// The magic school to dispel, or undefined for all schools
        /// </summary>
        public MagicSchool DispelSchool { get; private set; }

        /// <summary>
        /// The type of spells to dispel
        /// 0 = all spells
        /// 1 = positive
        /// 2 = negative
        /// </summary>
        public DispelType Align { get; private set; }

        /// <summary>
        /// The maximum # of spells to dispel
        /// </summary>
        public int Number { get; private set; }

        /// <summary>
        /// Number * NumberVariance = the minimum # of spells to dispel
        /// </summary>
        public float NumberVariance { get; private set; }

        /// <summary>
        /// Damage over Time Duration?
        /// </summary>
        public float DotDuration { get; private set; }

        internal static Spell FromSpellBase(uint spellId, SpellBase spellBase) {
            if (spellBase == null)
                return null;

            var spell = new Spell(spellId);

            spell.BaseMana = spellBase.BaseMana;
            spell.BaseRangeConstant = spellBase.BaseRangeConstant;
            spell.BaseRangeMod = spellBase.BaseRangeMod;
            spell.Flags = (SpellFlags)spellBase.Bitfield;
            spell.CasterEffect = (PlayScript)spellBase.CasterEffect;
            spell.Category = (SpellCategory)spellBase.Category;
            spell.ComponentLoss = spellBase.ComponentLoss;
            spell.DegradeLimit = spellBase.DegradeLimit;
            spell.DegradeModifier = spellBase.DegradeModifier;
            spell.Description = spell.Description;
            spell.DisplayOrder = spellBase.DisplayOrder;
            spell.Duration = spellBase.Duration;
            spell.FizzleEffect = (PlayScript)spellBase.FizzleEffect;
            spell.FormulaVersion = spellBase.FormulaVersion;
            spell.Formula = spellBase.Formula;
            spell.Icon = spellBase.Icon;
            spell.ManaMod = spellBase.ManaMod;
            spell.MetaSpellId = spellBase.MetaSpellId;
            spell.Name = spellBase.Name;
            spell.NonComponentTargetType = spellBase.NonComponentTargetType;
            spell.PortalLifetime = spellBase.PortalLifetime;
            spell.Power = spellBase.Power;
            spell.RecoveryAmount = spellBase.RecoveryAmount;
            spell.RecoveryInterval = spellBase.RecoveryInterval;
            spell.School = (MagicSchool)spellBase.School;
            spell.SpellEconomyMod = spellBase.SpellEconomyMod;
            spell.TargetEffect = (PlayScript)spellBase.TargetEffect;

            if (ServerSpellData.TryGetSpell(spellId, out SpellInfo serverSpell)) {
                spell.Align = serverSpell.Align;
                spell.BaseIntensity = serverSpell.BaseIntensity;
                spell.Boost = serverSpell.Boost;
                spell.BoostVariance = serverSpell.BoostVariance;
                spell.CreateOffset = new Vector3(serverSpell.CreateOffset.X, serverSpell.CreateOffset.Y, serverSpell.CreateOffset.Z);
                spell.CritFrequency = serverSpell.CritFrequency;
                spell.CritMultiplier = serverSpell.CritMultiplier;
                spell.DamageRatio = serverSpell.DamageRatio;
                spell.DamageType = serverSpell.DamageType;
                spell.DefaultLaunchAngle = serverSpell.DefaultLaunchAngle;
                spell.Destination = serverSpell.Destination;
                spell.DestinationLandcell = serverSpell.DestinationLandcell;
                spell.DestinationOrientation = new Quaternion() {
                    W = serverSpell.DestinationOrientation.W,
                    X = serverSpell.DestinationOrientation.X,
                    Y = serverSpell.DestinationOrientation.Y,
                    Z = serverSpell.DestinationOrientation.Z
                };
                spell.DestinationOrigin = new Vector3(serverSpell.DestinationOrigin.X, serverSpell.DestinationOrigin.Y, serverSpell.DestinationOrigin.Z);
                spell.Dims = new Vector3(serverSpell.Dims.X, serverSpell.Dims.Y, serverSpell.Dims.Z);
                spell.DispelSchool = serverSpell.DispelSchool;
                spell.DotDuration = serverSpell.DotDuration;
                spell.DrainPercentage = serverSpell.DrainPercentage;
                spell.ElementalModifier = serverSpell.ElementalModifier;
                spell.IgnoreMagicResist = serverSpell.IgnoreMagicResist;
                spell.ImbuedEffect = serverSpell.ImbuedEffect;
                spell.Link = serverSpell.Link;
                spell.LossPercent = serverSpell.LossPercent;
                spell.MaxBoostAllowed = serverSpell.MaxBoostAllowed;
                spell.MaxPower = serverSpell.MaxPower;
                spell.MinPower = serverSpell.MinPower;
                spell.NonTracking = serverSpell.NonTracking;
                spell.Number = serverSpell.Number;
                spell.NumberVariance = serverSpell.NumberVariance;
                spell.NumProjectiles = serverSpell.NumProjectiles;
                spell.NumProjectilesVariance = serverSpell.NumProjectilesVariance;
                spell.Padding = new Vector3(serverSpell.Padding.X, serverSpell.Padding.Y, serverSpell.Padding.Z);
                spell.Peturbation = new Vector3(serverSpell.Peturbation.X, serverSpell.Peturbation.Y, serverSpell.Peturbation.Z);
                spell.PowerVariance = serverSpell.PowerVariance;
                spell.Proportion = serverSpell.Proportion;
                spell.SlayerCreatureType = serverSpell.SlayerCreatureType;
                spell.SlayerDamageBonus = serverSpell.SlayerDamageBonus;
                spell.Source = serverSpell.Source;
                spell.SourceLoss = serverSpell.SourceLoss;
                spell.SpreadAngle = serverSpell.SpreadAngle;
                spell.StatModKey = serverSpell.StatModKey;
                spell.StatModType = serverSpell.StatModType;
                spell.StatModVal = serverSpell.StatModVal;
                spell.TransferCap = serverSpell.TransferCap;
                spell.TransferFlags = serverSpell.TransferFlags;
                spell.Variance = serverSpell.Variance;
                spell.VerticalAngle = serverSpell.VerticalAngle;
                spell.VitalDamageType = serverSpell.VitalDamageType;
                spell.WeenieClassId = serverSpell.WeenieClassId;
            }

            return spell;
        }

        internal Spell(uint id) {
            Id = id;
            _gameState = ScriptManager.Instance.Resolve<GameState>();
        }

        #region public api
        /// <summary>
        /// Check if this spell is known to the current player
        /// </summary>
        /// <returns>true if current character knows this spell</returns>
        public bool IsKnown() {
            var isKnown = _gameState.Character?.SpellBook?.IsKnown(Id);
            return !isKnown.HasValue ? false : isKnown.Value;
        }

        /// <summary>
        /// List of old school (non-foci) spell components this spell requires
        /// </summary>
        public IList<SpellComponent> OldSchoolComponents {
            get {
                var spellComponentsTable = ScriptManager.Instance.Resolve<IAsyncDatReader>().SpellComponentsTable;
                if (_oldschoolComponents == null) {
                    _oldschoolComponents = Formula.Select(cId => SpellComponent.FromSpellComponentBase(cId, spellComponentsTable.SpellComponents[cId])).ToList();
                }
                return _oldschoolComponents;
            }
        }

        /// <summary>
        /// Check if the current character has the needed foci or infused magic augmentation
        /// </summary>
        /// <returns>true if character has required foci / infused magic augmentation</returns>
        public bool HasFociOrInfusedAugmentation() {
            uint fociWcid = 0;
            bool hasInfusedAugmentation = false;
            switch (School) {
                case MagicSchool.WarMagic:
                    fociWcid = 15271;
                    hasInfusedAugmentation = _gameState.Character.Weenie.Value(IntId.AugmentationInfusedWarMagic) > 0;
                    break;
                case MagicSchool.LifeMagic:
                    fociWcid = 15270;
                    hasInfusedAugmentation = _gameState.Character.Weenie.Value(IntId.AugmentationInfusedLifeMagic) > 0;
                    break;
                case MagicSchool.CreatureEnchantment:
                    fociWcid = 15270;
                    hasInfusedAugmentation = _gameState.Character.Weenie.Value(IntId.AugmentationInfusedCreatureMagic) > 0;
                    break;
                case MagicSchool.ItemEnchantment:
                    fociWcid = 15269;
                    hasInfusedAugmentation = _gameState.Character.Weenie.Value(IntId.AugmentationInfusedItemMagic) > 0;
                    break;
                case MagicSchool.VoidMagic:
                    fociWcid = 43173;
                    hasInfusedAugmentation = _gameState.Character.Weenie.Value(IntId.AugmentationInfusedVoidMagic) > 0;
                    break;
                default:
                    fociWcid = 0;
                    break;
            }

            if (hasInfusedAugmentation)
                return true;

            return _gameState.Character.Weenie.Containers.Any(w => w.ContainerProperties == ContainerProperties.Foci && w.ClassId == fociWcid);
        }

        /// <summary>
        /// Check if the current character has the required spell components to cast this spell. It
        /// automatically chooses between OldSchoolComponents and FociComponents depending on if the
        /// current character has the required foci / infused magic augmentation.
        /// </summary>
        /// <param name="missingComponents">A list of missing SpellComponents</param>
        /// <returns>true if the current character has the required components, false otherwise</returns>
        public bool HasComponents(out IEnumerable<SpellComponent> missingComponents) {
            // todo: optimize this...
            var missingComponentsList = new List<SpellComponent>();
            var allCharacterComponents = _gameState.Character.Weenie.AllItems.Where(w => w.ObjectType == ObjectType.SpellComponents);

            Dictionary<SpellComponent, uint> requiredComponents = new Dictionary<SpellComponent, uint>();
            foreach (var component in Components) {
                if (requiredComponents.ContainsKey(component)) {
                    requiredComponents[component] = requiredComponents[component] + 1;
                }
                else {
                    requiredComponents.Add(component, 1);
                }
            }

            foreach (var kv in requiredComponents) {
                var compCount = allCharacterComponents.Where(c => c.ClassId == kv.Key.ClassId).Sum(c => c.Value(IntId.StackSize));
                if (compCount < kv.Value) {
                    for (var i = compCount; i < kv.Value; i++)
                        missingComponentsList.Add(kv.Key);
                }
            }

            missingComponents = missingComponentsList;

            return missingComponentsList.Count == 0;
        }

        /// <summary>
        /// Check if the current character has the required spell components to cast this spell. It
        /// automatically chooses between OldSchoolComponents and FociComponents depending on if the
        /// current character has the required foci / infused magic augmentation.
        /// </summary>
        /// <returns>true if the current character has the required components, false otherwise</returns>
        public bool HasComponents() {
            var allCharacterComponents = _gameState.Character.Weenie.AllItems.Where(w => w.ObjectType == ObjectType.SpellComponents)
                .GroupBy(w => w.WeenieClassId).ToDictionary(x => x.Key, x => x.Sum(w => w.Value(IntId.StackSize)));

            var components = Components.GroupBy(c => c.ClassId).ToDictionary(c => c.Key, c => c.Count());

            foreach (var component in components) {
                if (!allCharacterComponents.TryGetValue(component.Key, out int amount) || amount < component.Value) {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the current character has the skill required to cast this spell successfully.
        /// </summary>
        /// <param name="spellDifficultyModifier">The number of skill points above the spell's difficulty that you must have in order for this check to pass</param>
        /// <returns>true if the current character has the required skill level (+spellDifficultyModifier), false otherwise</returns>
        public bool HasSkill(int spellDifficultyModifier = 20) {
            var currentSkill = _gameState.Character.GetMagicSkill(School);
            return currentSkill >= Power + spellDifficultyModifier;
        }
        #endregion // public api

        public enum Scarab {
            Lead = 1,
            Iron = 2,
            Copper = 3,
            Silver = 4,
            Gold = 5,
            Pyreal = 6,
            Diamond = 110,
            Platinum = 112,
            Dark = 192,
            Mana = 193
        }

        /// <summary>
        /// A mapping of scarabs => their spell levels
        /// If the first component in a spell is a scarab,
        /// the client uses this to determine the spell level,
        /// for things like the spellbook filters.
        /// </summary>
        public static Dictionary<Scarab, uint> ScarabLevel = new Dictionary<Scarab, uint>()
        {
            { Scarab.Lead,     1 },
            { Scarab.Iron,     2 },
            { Scarab.Copper,   3 },
            { Scarab.Silver,   4 },
            { Scarab.Gold,     5 },
            { Scarab.Pyreal,   6 },
            { Scarab.Diamond,  6 },
            { Scarab.Platinum, 7 },
            { Scarab.Dark,     7 },
            { Scarab.Mana,     8 }
        };

        /// <summary>
        /// A mapping of scarabs => their power levels
        /// </summary>
        public static Dictionary<Scarab, uint> ScarabPower = new Dictionary<Scarab, uint>()
        {
            { Scarab.Lead,     1 },
            { Scarab.Iron,     2 },
            { Scarab.Copper,   3 },
            { Scarab.Silver,   4 },
            { Scarab.Gold,     5 },
            { Scarab.Pyreal,   6 },
            { Scarab.Diamond,  7 },
            { Scarab.Platinum, 8 },
            { Scarab.Dark,     9 },
            { Scarab.Mana,    10 }
        };

        /// <summary>
        /// List of spell components this spell requires assuming the current character has the required
        /// foci / infused magic augmentation.
        /// </summary>
        public IList<SpellComponent> FociComponents {
            get {
                var spellComponentsTable = ScriptManager.Instance.Resolve<IAsyncDatReader>().SpellComponentsTable;
                if (_fociComponents == null) {
                    _fociComponents = OldSchoolComponents.Where(c => c.Id == 111 /* chorizite */ || c.Category == 0 /* scarab */).ToList();
                    var numTapers = 0;
                    var scarabs = _fociComponents.Where(c => Enum.IsDefined(typeof(Scarab), (int)c.Id));
                    if (scarabs.Count() > 0) {
                        switch (ScarabPower[(Scarab)scarabs.First().Id]) {
                            case 1:
                                numTapers = 1;
                                break;
                            case 2:
                                numTapers = 2;
                                break;
                            case 3:
                            case 4:
                            case 7:
                                numTapers = 3;
                                break;
                            case 5:
                            case 6:
                            case 8:
                            case 9:
                            case 10:
                                numTapers = 4;
                                break;
                        }
                    }

                    var prismaticTaper = SpellComponent.FromSpellComponentBase(188, spellComponentsTable.SpellComponents[188]);

                    for (var i = 0; i < numTapers; i++)
                        _fociComponents.Add(prismaticTaper);
                }
                return _fociComponents;
            }
        }

        /// <summary>
        /// List of SpellComponents this spell requires. This looks at your character's inventory and chooses between
        /// foci / oldschool formulas depending on if you have the required foci / infused magic augmentation.
        /// </summary>
        public IList<SpellComponent> Components {
            get {
                return HasFociOrInfusedAugmentation() ? FociComponents : OldSchoolComponents;
            }
        }

        public override string ToString() {
            return $"{Name} 0x{Id:X8}";
        }
    }
}