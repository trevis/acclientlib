﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Frames represent a specific position within a landblock. This includes a local offset, and an orientation.
    /// </summary>
    public class Frame {
        /// <summary>
        /// Frame origin
        /// </summary>
        public Vector3 Origin { get; set; } = new Vector3();

        /// <summary>
        /// Frame orientation
        /// </summary>
        public Quaternion Orientation { get; } = new Quaternion();

        public Frame(Vector3 origin, Quaternion orientation) {
            Origin = origin;
            Orientation = orientation;
        }

        public Frame() {
        
        }
    }
}
