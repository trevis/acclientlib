﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Interop {
    public struct AnimPartChange {
        public byte PartIndex { get; internal set; }

        public uint PartID { get; internal set; }
    }
}
