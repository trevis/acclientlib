﻿using ACE.DatLoader;
using WattleScript.Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Lib;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Lib;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about spells
    /// </summary>
    public class SpellBook : IDisposable {
        private ILogger _log;
        private List<uint> _knownSpells = new List<uint>();
        private Dictionary<uint, Spell> _spellCache = new Dictionary<uint, Spell>();

        private GameState _gameState;
        private ScriptManager _manager;

        #region public events
        /// <summary>
        /// Fired when your spellbook changes. (Added or removed spell)
        /// </summary>
        public event EventHandler<EventArgs> OnChanged;
        #endregion // public events

        #region public properties
        /// <summary>
        /// A list of known spell ids
        /// </summary>
        public virtual IEnumerable<uint> KnownSpellsIds => _knownSpells;

        /// <summary>
        /// The number of spells in this spell book
        /// </summary>
        public virtual int Count => _knownSpells.Count;
        #endregion // public properties

        /// <summary>
        /// Create a new spellbook for a character. This should be done immediately upon login, before Login_Playerdescription
        /// is sent to the client.
        /// </summary>
        [WattleScript.Interpreter.WattleScriptHidden]
        public SpellBook() {
            _manager = ScriptManager.Instance;
            _log = _manager.Resolve<ILogger>();
            _gameState = _manager.Resolve<GameState>();

            _manager.MessageHandler.Incoming.Login_PlayerDescription += MessageHandler_Login_PlayerDescription_S2C;
            _manager.MessageHandler.Incoming.Magic_RemoveSpell += MessageHandler_Magic_RemoveSpell_S2C;
            _manager.MessageHandler.Incoming.Magic_UpdateSpell += MessageHandler_Magic_UpdateSpell_S2C;
        }

        /// <summary>
        /// Get a spell with the specified id
        /// </summary>
        /// <param name="spellId">The spell id to get</param>
        /// <returns>The spell, or null if it doesn't exist</returns>
        public virtual Spell this[uint spellId] {
            get {
                if (_spellCache.TryGetValue(spellId, out Spell spell)) {
                    return spell;
                }

                var spellTable = _manager.Resolve<IAsyncDatReader>().SpellTable;

                if (spellTable.Spells.ContainsKey(spellId)) {
                    var newSpell = Spell.FromSpellBase(spellId, spellTable.Spells[spellId]);
                    _spellCache.Add(spellId, newSpell);
                    return newSpell;
                }

                return null;
            }
        }

        #region public api
        /// <summary>
        /// Get a spell by id
        /// </summary>
        /// <param name="spellId">The spell id to get</param>
        /// <returns>The spell if found, null otherwise</returns>
        public virtual Spell Get(uint spellId) {
            return this[spellId];
        }

        /// <summary>
        /// Try and get a spell by id
        /// </summary>
        /// <param name="spellId">The spell id to get</param>
        /// <param name="spell">The spell, if found</param>
        /// <returns>true if found, false otherwise</returns>
        public virtual bool TryGet(uint spellId, out Spell spell) {
            spell = this[spellId];

            return spell != null;
        }

        /// <summary>
        /// Check if the specified spell id is known by your character
        /// </summary>
        /// <param name="spellId">The spell id to check</param>
        /// <returns>true if your character knows this spell, false otherwise</returns>
        public virtual bool IsKnown(uint spellId) {
            return _knownSpells.Contains(spellId);
        }

        /// <summary>
        /// Check if a spell id is valid
        /// </summary>
        /// <param name="spellId">The spell id to check</param>
        /// <returns>true if this is a valid spell, false otherwise</returns>
        public virtual bool IsValid(uint spellId) {
            return this[spellId] != null;
        }

        /// <summary>
        /// Checks if the current character has the skill required to cast the specified spell successfully.
        /// </summary>
        /// <param name="spellId">The id of the spell to check</param>
        /// <param name="spellDifficultyModifier">The number of skill points above the spell's difficulty that you must have in order to cast</param>
        /// <returns>true if you have the skill to cast this spell</returns>
        public virtual bool HasSkillToCastSpell(uint spellId, int spellDifficultyModifier = 20) {
            if (!TryGet(spellId, out Spell spell)) {
                return false;
            }

            return spell.HasSkill(spellDifficultyModifier);
        }

        /// <summary>
        /// Get a list of all self buff spells that would benefit your character.
        /// </summary>
        /// <param name="skillModifier">Skill modifier to use. This number is added to your current skill level when checking if you have the required skill to cast a spell</param>
        /// <param name="force">If true, will ignore current character enchantments and return a full list of wanted buffs.</param>
        /// <param name="minDurationMilliseconds">Minimum duration left on an enchantment for it to be considered still active, and excluded from this list.</param>
        /// <returns>A list of buffs</returns>
        public IList<Spell> GetSelfBuffs(int skillModifier = 5, bool force = false, int minDurationMilliseconds = 300000) {
            var canCastSpell = (Spell s) => s.HasSkill(skillModifier) && s.HasComponents();
            var flags = (SpellFlags.SelfTargeted | SpellFlags.Beneficial);
            var knownSpells = _gameState.Character.SpellBook.KnownSpellsIds.Select(spellId => this[spellId]);

            var _selfBuffsBySkill = _gameState.Character.Weenie.Skills.Values
                .Where(skill => skill.Training >= SkillTrainingType.Trained)
                .Select(skill => {
                    var values = knownSpells.Where(spell => spell.StatModSkill == skill.Type && (spell.Flags & flags) == flags)
                            .GroupBy(spell => spell.Category)
                            .Select(category => {
                                return category.OrderByDescending(spell => spell.Level);
                            });
                    return new KeyValuePair<SkillId, IEnumerable<IOrderedEnumerable<Spell>>>(skill.Type, values);
                });

            var _selfBuffsByAttribute = _gameState.Character.Weenie.Attributes.Keys
                .Select(attrId => {
                    var values = knownSpells.Where(spell => spell.StatModAttribute == attrId && (spell.Flags & flags) == flags)
                            .GroupBy(spell => spell.Category)
                            .Select(category => {
                                return category.OrderByDescending(spell => spell.Level);
                            });
                    return new KeyValuePair<AttributeId, IEnumerable<IOrderedEnumerable<Spell>>>(attrId, values);
                });

            var _selfBuffsByVital = _gameState.Character.Weenie.Vitals.Keys
                .Select(vitalId => {
                    var values = knownSpells.Where(spell => spell.StatModVital == (UtilityBelt.Common.Enums.Vital)vitalId && (spell.Flags & flags) == flags)
                            .GroupBy(spell => spell.Category)
                            .Select(category => {
                                return category.OrderByDescending(spell => spell.Level);
                            });
                    return new KeyValuePair<VitalId, IEnumerable<IOrderedEnumerable<Spell>>>(vitalId, values);

                });

            var _selfLifes = knownSpells.Where(spell => spell.School == MagicSchool.LifeMagic && spell.Duration > 0 && (spell.Flags & flags) == flags)
                            .GroupBy(spell => spell.Category)
                            .Select(category => {
                                return new KeyValuePair<SpellCategory, IOrderedEnumerable<Spell>>(category.Key, category.OrderByDescending(spell => spell.Power));
                            });

            var _selfItems = knownSpells.Where(spell => spell.School == MagicSchool.ItemEnchantment && spell.Duration > 0 && (spell.Flags & flags) == flags)
                            .GroupBy(spell => spell.Category)
                            .Select(category => {
                                return new KeyValuePair<SpellCategory, IOrderedEnumerable<Spell>>(category.Key, category.OrderByDescending(spell => spell.Power));
                            });

            var creatureMagicBuffs = _selfBuffsBySkill.FirstOrDefault(kv => kv.Key == SkillId.CreatureEnchantment).Value
                .Select(set => set.Where(canCastSpell).FirstOrDefault());

            var magicAttrBuffs = _selfBuffsByAttribute.Where(kv => kv.Key == AttributeId.Focus || kv.Key == AttributeId.Self)
                .Select(buffs => buffs.Value)
                .SelectMany(s => s)
                .Select(set => set.Where(canCastSpell).FirstOrDefault());

            var altMagicSkillBuffs = _selfBuffsBySkill.Where(kv => kv.Key == SkillId.LifeMagic || kv.Key == SkillId.ItemEnchantment || kv.Key == SkillId.ManaConversion)
                .Select(buffs => buffs.Value)
                .SelectMany(s => s)
                .Select(set => set.Where(canCastSpell).FirstOrDefault());

            var wandBuffs = new List<Spell>() {
                    _selfItems.FirstOrDefault(kv => kv.Key == SpellCategory.ManaConversionModRaising).Value
                        .Where(canCastSpell).FirstOrDefault()
                };

            var preBuffs = creatureMagicBuffs.Concat(magicAttrBuffs).Concat(altMagicSkillBuffs).Concat(wandBuffs);

            var lifeBuffs = _selfLifes.Select(set => set.Value.Where(canCastSpell).FirstOrDefault());
            var itemBuffs = _selfItems.Select(set => set.Value.Where(canCastSpell).FirstOrDefault());
            var skillBuffs = _selfBuffsBySkill.Select(skillKv => {
                return skillKv.Value.Select(q => q.Where(canCastSpell).FirstOrDefault());
            }).SelectMany(i => i);
            var attrBuffs = _selfBuffsByAttribute.Select(attrKv => {
                return attrKv.Value.Select(q => q.Where(canCastSpell).FirstOrDefault());
            }).SelectMany(i => i);

            return preBuffs.Concat(attrBuffs).Concat(itemBuffs).Concat(skillBuffs).Concat(lifeBuffs)
                .Where(b => b != null && !_gameState.Character.GetActiveEnchantments().Any(e => {
                    return e.SpellId == b.Id && (force || e.ExpiresAt - DateTime.UtcNow >= TimeSpan.FromMilliseconds(minDurationMilliseconds));
                })).Distinct().ToList();
        }
        #endregion // public api

        private void AddSpell(uint spellId) {
            if (!_knownSpells.Contains(spellId)) {
                _knownSpells.Add(spellId);
                OnChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private void RemoveSpell(uint spellId) {
            if (_knownSpells.Contains(spellId)) {
                _knownSpells.Remove(spellId);
                OnChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        #region message handlers
        private void MessageHandler_Login_PlayerDescription_S2C(object sender, Login_PlayerDescription_S2C_EventArgs e) {
            if ((e.Data.Qualities.Flags & 0x00000100) != 0) {
                foreach (var kv in e.Data.Qualities.SpellBook.Spells.Table) {
                    var spellId = kv.Key.Id;
                    AddSpell(spellId);
                }
            }
        }

        private void MessageHandler_Magic_UpdateSpell_S2C(object sender, Magic_UpdateSpell_S2C_EventArgs e) {
            AddSpell(e.Data.SpellId.Id);
        }

        private void MessageHandler_Magic_RemoveSpell_S2C(object sender, Magic_RemoveSpell_S2C_EventArgs e) {
            RemoveSpell(e.Data.SpellId.Id);
        }
        #endregion // message handlers

        [WattleScriptHidden]
        public void Dispose() {
            _manager.MessageHandler.Incoming.Magic_RemoveSpell -= MessageHandler_Magic_RemoveSpell_S2C;
            _manager.MessageHandler.Incoming.Magic_UpdateSpell -= MessageHandler_Magic_UpdateSpell_S2C;
            _manager.MessageHandler.Incoming.Login_PlayerDescription -= MessageHandler_Login_PlayerDescription_S2C;
        }
    }
}
