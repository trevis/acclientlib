﻿using WattleScript.Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Interop {
    public class Promise<T> : Promise {
        private static ScriptManager _manager;
        private static ILogger _log;

        public Promise(ILogger logger, ScriptManager manager) : base(logger, manager) {
            _manager = manager;
            _log = logger;
        }

        public T2 Next<T2>(Func<T, T2> resolveAction) {
            return Next(resolveAction);
        }

        public static Promise<T> Rejected(T value) {
            var promise = new Promise<T>(_log, _manager);

            promise.Reject(value);

            return promise;
        }

        public static Promise<T> Resolved(T value) {
            var promise = new Promise<T>(_log, _manager);

            promise.Resolve(value);

            return promise;
        }
    }

    public class Promise {
        public struct ResolveRejectHandler {
            public Func<object, object> Func;
            public Promise Promise;

            public ResolveRejectHandler(Func<object, object> func, Promise promise) {
                Func = func;
                Promise = promise;
            }
        }

        private static ScriptManager _manager;
        private static ILogger _log;
        private object resolvedValue;
        private object rejectedValue;
        public string Name { get; set; }

        private List<ResolveRejectHandler> resolveHandlers = new List<ResolveRejectHandler>();
        private List<ResolveRejectHandler> rejectHandlers = new List<ResolveRejectHandler>();
        private List<ResolveRejectHandler> finallyHandlers = new List<ResolveRejectHandler>();
        public bool WasRejected { get; private set; } = false;
        public bool WasResolved { get; private set; } = false;

        private List<Promise> children = new List<Promise>();
        private Promise _parent;

        private static uint _nameCounter = 0;

        public Promise(ILogger logger, ScriptManager manager, Promise parent = null) {
            _log = logger;
            _manager = manager;
            _parent = parent;
            Name = $"UnknownPromise{++_nameCounter}";
        }

        public Task<object> Await() {
            var tcs = new TaskCompletionSource<object>();

            Finally((res) => {
                tcs.TrySetResult(res);
                return null;
            });

            return tcs.Task;
        }

        public virtual Promise Next(Func<object, object> resolveAction) {
            if (WasResolved) {
                resolveAction?.Invoke(resolvedValue);
                return Resolved(resolvedValue);
            }

            var resolvePromise = new Promise(_log, _manager, this);
            resolvePromise.Name = $"{Name}.Child{++_nameCounter}";

            children.Add(resolvePromise);

            resolveHandlers.Add(new ResolveRejectHandler(resolveAction, resolvePromise));

            // if the parent was rejected, we need to reject the children
            if (WasRejected) {
                resolvePromise.Reject(rejectedValue);
            }

            return resolvePromise;
        }

        public Promise Catch(Func<object, object> rejectAction) {
            if (WasRejected) {
                rejectAction?.Invoke(rejectedValue);
                return this;
            }

            rejectHandlers.Add(new ResolveRejectHandler(rejectAction, null));

            return this;
        }

        public Promise Finally(Func<object, object> finallyAction) {
            if (WasRejected) {
                finallyAction?.Invoke(rejectedValue);
                return this;
            }
            if (WasResolved) {
                finallyAction?.Invoke(resolvedValue);
                return this;
            }

            finallyHandlers.Add(new ResolveRejectHandler(finallyAction, null));

            return this;
        }

        // /ub lexec game.actions.setCombatMode(CombatMode.Magic).next(function(res) print("res: "..tostring(res)) end).catch(function(err) print("catch: "..tostring(err)) end).finally(function(err) print("finally: "..tostring(err)) end)
        public void Resolve(object arg) {
            try {
                //_log.Log($"Resolve: {Name} // {WasResolved} || {WasRejected}");
                if (WasResolved || WasRejected) {
                    _log.Log(LogLevel.Warning, $"Tried to resolve promise twice!");
                    return;
                }

                resolvedValue = arg;

                foreach (var handler in resolveHandlers) {
                    object innerRet = null;
                    try {
                        innerRet = handler.Func?.Invoke(arg);
                    }
                    catch (Exception ex) {
                        _log.LogError(ex, $"Unable to call promise handler for {this}");
                    }

                    if (innerRet != null && innerRet is DynValue dyn && dyn.ToObject() is Promise innerPromise) {
                        innerPromise.Next((res) => {
                            try {
                                handler.Promise.Resolve(res);
                            }
                            catch (Exception ex) { _log.LogError(ex, $"Unable to call promise handler for {this}"); }
                            return res;
                        }).Catch((res) => {
                            try {
                                handler.Promise.Reject(res);
                            }
                            catch (Exception ex) { _log.LogError(ex, $"Unable to call promise handler for {this}"); }
                            return res;
                        });
                    }
                    else if (innerRet != null && innerRet is DynValue dyn2 && dyn2.Type == DataType.Void) {
                        handler.Promise.Resolve(arg);
                    }
                    else {
                        handler.Promise.Resolve(innerRet);
                    }
                }

                DoFinally(arg);
                WasResolved = true;
            }
            catch (Exception ex) { _log.LogError(ex, $"Unable to resolve promise {this}"); }
        }

        public void Reject(object arg) {
            try {
                if (WasResolved || WasRejected) {
                    _log.Log(LogLevel.Warning, $"Tried to reject promise twice!");
                    return;
                }

                rejectedValue = arg;

                foreach (var handler in rejectHandlers) {
                    try {
                        handler.Func?.Invoke(arg);
                        handler.Promise?.Reject(arg);
                    }
                    catch (Exception ex) { _log.LogError(ex, $"Unable to call promise reject handler {this}"); }
                }

                // reject any children, so that catch/finally handlers will flow down
                foreach (var child in children) {
                    try {
                        child?.Reject(arg);
                    }
                    catch (Exception ex) { _log.LogError(ex, $"Unable to call promise reject handler {this}"); }
                }

                DoFinally(arg);
                WasRejected = true;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        private void DoFinally(object arg) {
            try {
                if (WasResolved || WasRejected) {
                    return;
                }

                foreach (var handler in finallyHandlers) {
                    try {
                        handler.Func?.Invoke(arg);
                    }
                    catch (Exception ex) { _log.LogError(ex.ToString()); }
                }
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        /// <summary>
        /// returns a resolved promise
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Promise Resolved(object value) {
            var promise = new Promise(_log, _manager);

            promise.Resolve(value);

            return promise;
        }

        /// <summary>
        /// Returns a rejected promise
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Promise Rejected(object value) {
            var promise = new Promise(_log, _manager);

            promise.Reject(value);

            return promise;
        }
    }
}
