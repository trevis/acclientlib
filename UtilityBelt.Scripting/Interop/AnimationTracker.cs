﻿using ACE.DatLoader.FileTypes;
using ACE.DatLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Lib;
using UtilityBelt.Common.Messages.Events;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about the character's current animation state
    /// </summary>
    public class AnimTracker : IDisposable {
        public class Animation {
            private bool sentFinished;

            public event EventHandler<EventArgs> OnFinished;

            public DateTime StartedAt { get; private set; } = DateTime.MinValue;
            public DateTime EndsAt { get; private set; } = DateTime.MinValue;
            public float DurationSeconds { get; } = 0;
            public string Name { get; } = "";
            public ushort ServerActionSequence { get; }
            public bool IsStarted { get; private set; } = false;

            public MotionStance Stance { get; private set; }
            public MotionCommand Motion { get; private set; }

            public Animation(float durationSeconds, string name, ushort serverActionSequence, MotionStance stance, MotionCommand motion) {
                DurationSeconds = (!float.IsInfinity(durationSeconds) && !float.IsNaN(durationSeconds)) ? durationSeconds : 0;
                Name = name;
                ServerActionSequence = serverActionSequence;
                Stance = stance;
                Motion = motion;
                //Logger.WriteToChat($"Create Anim: {Name} / {DurationSeconds}");
            }

            public TimeSpan TimeLeft => (IsStarted ? (EndsAt - DateTime.UtcNow) : TimeSpan.FromSeconds(DurationSeconds));

            public void Start() {
                IsStarted = true;
                StartedAt = DateTime.UtcNow;
                EndsAt = StartedAt + TimeSpan.FromSeconds(DurationSeconds);
            }

            public bool IsFinished() {
                var isFinished = IsStarted && DateTime.UtcNow >= EndsAt;
                if (isFinished && !sentFinished) {
                    OnFinished?.Invoke(this, EventArgs.Empty);
                    sentFinished = true;
                }
                return isFinished;
            }

            public void Reset() {
                IsStarted = false;
                EndsAt = DateTime.MinValue;
                sentFinished = false;
            }

            public override string ToString() {
                return $"{Stance}/{Motion} ({Math.Round(DurationSeconds * 1000f):N0}ms)";
            }

            internal void ForceFinish() {
                OnFinished?.Invoke(this, EventArgs.Empty);
            }
        }

        private ScriptManager _manager;
        private MotionTable motionTable = null;
        private string currentAnimation;
        public List<Animation> AnimationQueue { get; } = new List<Animation>();

        public bool IsMovingAutomatically { get; private set; }
        public MotionCommand ForwardCommand { get; private set; }
        public MovementType MovementType { get; private set; }
        public MotionStance Stance { get; private set; } = MotionStance.NonCombat;
        public bool IsJumping { get; private set; }
        public string CurrentAnimation { get => (DateTime.UtcNow > AnimEndTime) ? "" : currentAnimation; private set => currentAnimation = value; }

        public DateTime AnimEndTime = DateTime.MinValue;

        internal AnimTracker() {
            _manager = ScriptManager.Instance;
            _manager.GameState.OnRender2D += GameState_OnRender2D;

            _manager.MessageHandler.Incoming.Movement_PositionEvent += Incoming_Movement_PositionEvent;
            _manager.MessageHandler.Incoming.Movement_SetObjectMovement += Incoming_Movement_SetObjectMovement;
            _manager.MessageHandler.Outgoing.Movement_MoveToState += Outgoing_Movement_MoveToState;
            _manager.MessageHandler.Incoming.Effects_PlayerTeleport += Incoming_Effects_PlayerTeleport;
        }

        private void Incoming_Effects_PlayerTeleport(object sender, Effects_PlayerTeleport_S2C_EventArgs e) {
            foreach (var anim in AnimationQueue) {
                anim.ForceFinish();
            }
            AnimationQueue.Clear();
        }

        private void GameState_OnRender2D(object sender, EventArgs e) {
            TryLoadMotionTable();

            if (AnimationQueue.Count > 0) {
                var anim = AnimationQueue[0];
                if (anim.IsStarted) {
                    if (anim.IsFinished()) {
                        AnimationQueue.RemoveAt(0);

                        //Logger.WriteToChat($"Stopped Anim: {anim.Name}");
                    }
                }
                else {
                    anim.Start();
                    //Logger.WriteToChat($"Started Anim: {anim.Name}");
                }
            }
        }

        private async void TryLoadMotionTable() {
            var mTable = _manager.GameState.Character?.Weenie?.PhysicsDesc?.MotionTableID;
            if (motionTable == null && mTable.HasValue && mTable.Value != 0) {
                motionTable = await _manager.Resolve<IAsyncDatReader>().ReadFromDat<MotionTable>(mTable.Value);
            }
        }

        private void AddAnimationToQueue(Animation animation) {
            if (motionTable == null)
                return;

            if (animation.DurationSeconds <= 0 || animation.Motion == MotionCommand.RunForward || animation.Motion == MotionCommand.WalkForward)
                return;

            // support for breaking spell casting animations if already doing a spell casting animation
            if (animation.Stance == MotionStance.Magic && AnimationQueue.All(a => a.Stance == MotionStance.Magic))
                AnimationQueue.Clear();

            if (!AnimationQueue.Any(a => a.ServerActionSequence == animation.ServerActionSequence)) {
                AnimationQueue.Add(animation);
            }
        }

        private void Outgoing_Movement_MoveToState(object sender, Movement_MoveToState_C2S_EventArgs e) {
            if (motionTable == null)
                return;

            if (e.Data.MoveToStatePack.RawMotionState.CommandListLength > 0) {
                for (var i = 0; i < e.Data.MoveToStatePack.RawMotionState.Commands.Count; i++) {
                    var command = e.Data.MoveToStatePack.RawMotionState.Commands[i];
                    MotionDatCommand mCommand = (MotionDatCommand)Enum.Parse(typeof(MotionDatCommand), command.Id.ToString());
                    var animLength = motionTable.GetAnimationLength(mCommand);
                    //UBService.UBService.WriteLog($"ANIM {command.Id} // {animLength} // {motionTable.Id:X8}");
                    AddAnimationToQueue(new Animation(animLength, command.Id.ToString(), command.ServerActionSequence, Stance, command.Id));
                }
            }
        }

        private void Incoming_Movement_SetObjectMovement(object sender, Movement_SetObjectMovement_S2C_EventArgs e) {
            if (motionTable == null)
                return;

            if (e.Data.ObjectId != _manager.GameState.CharacterId)
                return;

            var oldStance = Stance;
            IsMovingAutomatically = e.Data.MovementData.Autonomous == 0;
            MovementType = e.Data.MovementData.MovementType;
            if (e.Data.MovementData.Stance != 0)
                Stance = e.Data.MovementData.Stance;

            if (oldStance != Stance) {
                float data;
                if (oldStance != MotionStance.NonCombat && Stance != MotionStance.NonCombat) {
                    data = motionTable.GetAnimationLength(MotionFromStance(Stance));
                }
                else if (oldStance != MotionStance.NonCombat) {
                    MotionDatStance mStance = (MotionDatStance)Enum.Parse(typeof(MotionDatStance), Stance.ToString());
                    data = motionTable.GetAnimationLength(mStance, MotionFromStance(oldStance), MotionFromStance(Stance));
                }
                else {
                    MotionDatStance mStance = (MotionDatStance)Enum.Parse(typeof(MotionDatStance), Stance.ToString());
                    data = motionTable.GetAnimationLength(MotionFromStance(Stance));
                }
                if (data > 0) {
                    //UBService.UBService.WriteLog($"Changing stance {oldStance} ({MotionFromStance(oldStance)}) -> {Stance} ({MotionFromStance(Stance)}) // {data}");
                    AddAnimationToQueue(new Animation(data, $"{oldStance} -> {Stance}", e.Data.MovementData.ObjectServerControlSequence, Stance, ForwardCommand));
                }
            }

            if (e.Data.MovementData.MovementType == MovementType.InterpertedMotionState) {
                var oldCommand = ForwardCommand;
                ForwardCommand = e.Data.MovementData.State.ForwardCommand;
                if (ForwardCommand == MotionCommand.Ready) {
                    IsMovingAutomatically = false;
                }
                var animLength = GetAnimationLength(Stance, ForwardCommand, e.Data.MovementData.State.ForwardSpeed);
                MotionDatCommand mCommand = (MotionDatCommand)Enum.Parse(typeof(MotionDatCommand), ForwardCommand.ToString());
                var animLength2 = motionTable.GetAnimationLength(mCommand);
                var actionStr = $"{Stance}/{ForwardCommand}";

                if (animLength <= 0 && animLength2 >= 0) {
                    animLength = animLength2;
                    actionStr = $"{ForwardCommand}";
                }

                if (e.Data.MovementData.State.CommandListLength > 0) {
                    for (var i = 0; i < e.Data.MovementData.State.Commands.Count; i++) {
                        var command = e.Data.MovementData.State.Commands[i];
                        MotionDatCommand mmCommand = (MotionDatCommand)Enum.Parse(typeof(MotionDatCommand), command.Id.ToString());
                        var actionLength = motionTable.GetAnimationLength(mmCommand);
                        var animation = new Animation(actionLength, command.Id.ToString(), command.ServerActionSequence, Stance, command.Id);
                        if (!AnimationQueue.Any(a => a.Motion == animation.Motion)) {
                            AnimationQueue.Add(animation);
                        }
                    }
                }

                AddAnimationToQueue(new Animation(animLength, actionStr, e.Data.MovementData.ObjectServerControlSequence, Stance, ForwardCommand));
            }
        }

        private MotionDatCommand MotionFromStance(MotionStance oldStance) {
            switch (oldStance) {
                case MotionStance.NonCombat:
                    return MotionDatCommand.Ready;
                default:
                    return (MotionDatCommand)Enum.Parse(typeof(MotionDatCommand), oldStance.ToString());
            }
        }

        public float GetAnimationLength(MotionStance stance, MotionCommand motion, float speed = 1.0f) {
            if (motionTable == null)
                return 0;

            var animLength = 0.0f;
            MotionDatStance mStance = (MotionDatStance)Enum.Parse(typeof(MotionDatStance), stance.ToString());
            MotionDatCommand mCommand = (MotionDatCommand)Enum.Parse(typeof(MotionDatCommand), motion.ToString());
            //MotionCommand mCurrentCommand = (MotionCommand)Enum.Parse(typeof(MotionCommand), currentMotion.ToString()); 
            animLength += motionTable.GetAnimationLength(mStance, mCommand) / speed;
            animLength += motionTable.GetAnimationLength(mStance, MotionDatCommand.Ready, mCommand);
            if (animLength > 0) {
                CurrentAnimation = $"{stance}/{motion}";
            }
            return animLength;
        }

        private void Incoming_Movement_PositionEvent(object sender, Movement_PositionEvent_S2C_EventArgs e) {
            if (e.Data.ObjectId != _manager.GameState.CharacterId)
                return;

            if ((e.Data.Position.Flags & UtilityBelt.Common.Enums.PositionFlags.IsGrounded) == UtilityBelt.Common.Enums.PositionFlags.IsGrounded) {
                IsJumping = false;
            }
            else {
                IsJumping = true;
            }
        }

        public void Dispose() {
            _manager.GameState.OnRender2D -= GameState_OnRender2D;
            _manager.MessageHandler.Incoming.Movement_PositionEvent -= Incoming_Movement_PositionEvent;
            _manager.MessageHandler.Incoming.Movement_SetObjectMovement -= Incoming_Movement_SetObjectMovement;
            _manager.MessageHandler.Outgoing.Movement_MoveToState -= Outgoing_Movement_MoveToState;
            _manager.MessageHandler.Incoming.Effects_PlayerTeleport -= Incoming_Effects_PlayerTeleport;
        }
    }
}
