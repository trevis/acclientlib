﻿using ACE.DatLoader;
using ACE.DatLoader.Entity;
using ACE.DatLoader.FileTypes;

namespace UtilityBelt.Scripting.Interop {
    public class SpellComponent {
        /// <summary>
        /// Component Id
        /// </summary>
        public uint Id { get; private set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; private set; }
        public uint Category { get; private set; }
        public float CDM { get; private set; }
        public uint Gesture { get; private set; }

        /// <summary>
        /// Icon
        /// </summary>
        public uint Icon { get; private set; }
        public string Text { get; private set; }
        public float Time { get; private set; }

        /// <summary>
        /// weenie class id of this component
        /// </summary>
        public uint ClassId {
            get {
                if (ScriptManager.Instance.Resolve<IAsyncDatReader>().DualDidMapper.ClientEnumToID.TryGetValue(Id, out uint classId)) {
                    return classId;
                }
                return 0;
            }
        }

        [WattleScript.Interpreter.WattleScriptHidden]
        internal static SpellComponent FromSpellComponentBase(uint id, SpellComponentBase spellComponentBase) {
            var spellComponent = new SpellComponent();

            spellComponent.Id = id;
            spellComponent.Name = spellComponentBase.Name;
            spellComponent.Category = spellComponentBase.Category;
            spellComponent.CDM = spellComponentBase.CDM;
            spellComponent.Gesture = spellComponentBase.Gesture;
            spellComponent.Icon = spellComponentBase.Icon;
            spellComponent.Text = spellComponentBase.Text;
            spellComponent.Time = spellComponentBase.Time;

            return spellComponent;
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }
    }
}