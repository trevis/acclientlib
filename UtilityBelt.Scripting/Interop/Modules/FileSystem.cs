﻿using ACE.DatLoader;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UtilityBelt.Scripting.Interop.Modules {
    /// <summary>
    /// Event args for when a filesystem object access is granted / denied
    /// </summary>
    public class AccessChangedEventArgs : EventArgs {
        /// <summary>
        /// Wether or not access was granted
        /// </summary>
        public bool AccessGranted { get; }

        internal AccessChangedEventArgs(bool accessGranted) {
            AccessGranted = accessGranted;
        }
    }

    /// <summary>
    /// Used to get access to different sandboxed filesystem directories.
    /// </summary>
    public class FileSystem {
        internal string ScriptName { get; set; } = "";
        internal string ScriptDataDirectory { get; set; } = "";
        internal string ScriptDirectory { get; set; } = "";
        internal string StorageDirectory { get; set; } = "";

        /// <summary>
        /// Get a new FileSystemAccess object sandboxed to the script directory.
        /// </summary>
        /// <returns>A new FileSystemAccess object</returns>
        public FileSystemAccess GetScript() {
            return new FileSystemAccess(ScriptName, ScriptDirectory, ScriptDataDirectory, ScriptDirectory);
        }

        /// <summary>ss
        /// Get a new FileSystemAccess object sandboxed to the script data directory.
        /// </summary>
        /// <returns>A new FileSystemAccess object</returns>
        public FileSystemAccess GetData() {
            return new FileSystemAccess(ScriptName, ScriptDirectory, ScriptDataDirectory, ScriptDataDirectory);
        }

        /// <summary>
        /// Get a new FileSystemAccess object sandboxed to the script data directory.
        /// </summary>
        /// <param name="dataDirectory">The directory to request access to.</param>
        /// <param name="reason">The reason you are requesting access. It's a good idea to tell the user what you
        /// are looking for so they can point to a custom directory if needed.</param>
        /// <returns>A new FileSystemAccess object</returns>
        public FileSystemAccess GetCustom(string dataDirectory, string reason) {
            return new FileSystemAccess(ScriptName, ScriptDirectory, ScriptDataDirectory, dataDirectory, reason);
        }
    }

    /// <summary>
    /// Sandboxed filesystem access module for scripts. The default filesystem module returns a filesystem object sandboxed
    /// to the current script directory.  To get a sandboxed filesystem in the scriptdata directory, use `require("filesystem").GetDataFileSystem()`
    /// </summary>
    public class FileSystemAccess {
        internal string ScriptName { get; set; } = "";
        internal string StorageDirectory { get; set; } = "";

        internal string Reason { get; set; } = "";

        public event EventHandler<AccessChangedEventArgs> OnAccessChanged;

        /// <summary>
        /// Wether filesystem access is granted for this directory.
        /// </summary>
        public bool IsApproved { get; private set; } = false;

        internal FileSystemAccess(string scriptName, string scriptDirectory, string scriptDataDirectory, string storageDirectory, string reason = null) {
            ScriptName = scriptName;
            Reason = reason;

            if (storageDirectory == scriptDirectory || storageDirectory == scriptDataDirectory) {
                StorageDirectory = storageDirectory;
                IsApproved = true;
            }
            else {
                IsApproved = false;
                ScriptManager.Instance?.RequestDirectoryAccess(ScriptName, storageDirectory, Reason ?? "No Reason Given.", (result, dataDirectory) => {
                    if (result) {
                        IsApproved = true;
                        StorageDirectory = dataDirectory;
                    }
                    OnAccessChanged?.Invoke(this, new AccessChangedEventArgs(IsApproved));
                });
            }
        }

        internal void EnsureDirectoryExists(string directoryPath) {
            if (!System.IO.Directory.Exists(directoryPath)) {
                System.IO.Directory.CreateDirectory(directoryPath);
            }
        }

        internal string ResolvePath(string filename) {
            if (filename.StartsWith(System.IO.Path.DirectorySeparatorChar.ToString())) {
                filename = filename.Substring(1);
            }

            return System.IO.Path.GetFullPath(System.IO.Path.Combine(StorageDirectory, filename));
        }

        /// <summary>
        /// Writes the specified text to filename. This will automatically create needed directories if they don't exist.
        /// </summary>
        /// <param name="filename">The filename to write to</param>
        /// <param name="contents">The string contents to write</param>
        /// <param name="error">The error, if any</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool WriteText(string filename, string contents, out string error) {
            if (!CanWriteFile(filename, out error)) {
                return false;
            }

            filename = ResolvePath(filename);

            EnsureDirectoryExists(System.IO.Path.GetDirectoryName(filename));

            System.IO.File.WriteAllText(filename, contents);

            return true;
        }

        /// <summary>
        /// Checks if a file can be written to. This checks that the path is valid, and it's not an existing directory path.
        /// </summary>
        /// <param name="filename">The filename to check</param>
        /// <param name="error">The error, if any</param>
        /// <returns>True if the file can be written to, false otherwise</returns>
        public bool CanWriteFile(string filename, out string error) {
            return IsValidFilePath(filename, out error);
        }

        /// <summary>
        /// Check if a file path is valid. This checks that the path isn't trying to escape the sandbox directory
        /// and that it doesn't contain any invalid characters.
        /// </summary>
        /// <param name="filename">The filename to check</param>
        /// <param name="error">The error, if any</param>
        /// <returns></returns>
        public bool IsValidFilePath(string filename, out string error) {
            var resolvedPath = ResolvePath(filename);
            var invalidPathChars = System.IO.Path.GetInvalidFileNameChars().Where(c => System.IO.Path.GetFileName(resolvedPath).Contains(c));

            if (invalidPathChars.Count() > 0) {
                error = $"Filename contains invalid characters: {string.Join("", invalidPathChars)}";
                return false;
            }

            if (!IsApproved) {
                error = "Access has not been approved to this sandbox directory.";
                return false;
            }

            if (!resolvedPath.StartsWith(StorageDirectory)) {
                error = $"Filename is invalid. Tried to escape sandbox: {filename} resolved to {resolvedPath}";
                return false;
            }

            if (System.IO.Directory.Exists(resolvedPath)) {
                error = "Filename is a directory.";
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Check if a directory path is valid. This checks that the path isn't trying to escape the sandbox directory
        /// and that it doesn't contain any invalid characters.
        /// </summary>
        /// <param name="path">The directory path to check</param>
        /// <param name="error">The error, if any</param>
        /// <returns>True if the path is a valid directory path</returns>
        public bool IsValidDirectoryPath(string path, out string error) {
            var invalidPathChars = System.IO.Path.GetInvalidPathChars().Where(c => path.Contains(c));
            if (invalidPathChars.Count() > 0) {
                error = $"Path contains invalid characters: {string.Join("", invalidPathChars)}";
                return false;
            }

            if (!IsApproved) {
                error = "Access has not been approved to this sandbox directory.";
                return false;
            }

            var resolvedPath = ResolvePath(path);
            if (!resolvedPath.StartsWith(StorageDirectory)) {
                error = $"Path is invalid. Tried to escape sandbox: {path} resolved to {resolvedPath}";
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Appends the specified text to filename. This will automatically create needed directories if they don't exist.
        /// </summary>
        /// <param name="filename">The filename to write to</param>
        /// <param name="contents">The string contents to write</param>
        /// <param name="error">The error, if any</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool AppendText(string filename, string contents, out string error) {
            if (!CanWriteFile(filename, out error)) {
                return false;
            }

            filename = ResolvePath(filename);

            EnsureDirectoryExists(System.IO.Path.GetDirectoryName(filename));

            System.IO.File.AppendAllText(filename, contents);

            error = null;
            return true;
        }

        /// <summary>
        /// Reads a string of all contents of filename.
        /// </summary>
        /// <param name="filename">The filename to read</param>
        /// <param name="error">The error, if any</param>
        /// <returns>The file contents, or null if there was an error</returns>
        public string ReadText(string filename, out string error) {
            if (!CanWriteFile(filename, out error)) {
                return null;
            }

            if (!FileExists(filename, out error)) {
                return null;
            }

            error = null;
            return System.IO.File.ReadAllText(ResolvePath(filename));
        }

        /// <summary>
        /// Read all lines of filename.
        /// </summary>
        /// <param name="filename">The filename to read</param>
        /// <param name="error">The error, if any</param>
        /// <returns>The file contents, as a list of strings, or null if there was an error</returns>
        public IList<string> ReadLines(string filename, out string error) {
            if (!CanWriteFile(filename, out error)) {
                return null;
            }

            if (!FileExists(filename, out error)) {
                return null;
            }

            error = null;
            return System.IO.File.ReadAllLines(ResolvePath(filename)).ToList();
        }

        /// <summary>
        /// Check if the specified file exists
        /// </summary>
        /// <param name="filename">The filename to check</param>
        /// <param name="error">The error, if any</param>
        /// <returns>true if the file exists, false otherwise</returns>
        public bool FileExists(string filename, out string error) {
            if (!IsApproved) {
                error = "Access has not been approved to this sandbox directory.";
                return false;
            }
            filename = ResolvePath(filename);
            if (!System.IO.File.Exists(filename)) {
                error = "File does not exist.";
                return false;
            }
            error = null;
            return true;
        }

        /// <summary>
        /// Check if the specified directory exists
        /// </summary>
        /// <param name="directory">The directory to check</param>
        /// <param name="error">The error, if any</param>
        /// <returns>true if the directory exists, false otherwise</returns>
        public bool DirectoryExists(string directory, out string error) {
            if (!IsApproved) {
                error = "Access has not been approved to this sandbox directory.";
                return false;
            }
            directory = ResolvePath(directory);
            if (!System.IO.Directory.Exists(directory)) {
                error = "Directory does not exist.";
                return false;
            }
            error = null;
            return true;
        }

        /// <summary>
        /// Attempt to create the specified directory. This will create intermediate directories as needed.
        /// </summary>
        /// <param name="path">The directory path to create</param>
        /// <param name="error">The error, if any</param>
        /// <returns>True if successful, false otherwise</returns>
        public bool CreateDirectory(string path, out string error) {
            if (!IsValidDirectoryPath(path, out error)) {
                return false;
            }

            System.IO.Directory.CreateDirectory(ResolvePath(path));

            return true;
        }

        /// <summary>
        /// Attempt to delete the specified directory. If recursive is set to false and this directory is not empty,
        /// this call will error.
        /// </summary>
        /// <param name="path">The directory path to delete</param>
        /// <param name="recursive">Set to true to remove all contents as well.</param>
        /// <param name="error">The error, if any</param>
        /// <returns>True if successful, false otherwise</returns>
        public bool DeleteDirectory(string path, bool recursive, out string error) {
            if (!IsValidDirectoryPath(path, out error)) {
                return false;
            }

            var resolvedPath = ResolvePath(path);
            if (DirectoryExists(resolvedPath, out error)) {
                if (!recursive && System.IO.Directory.GetFileSystemEntries(resolvedPath).Length > 0) {
                    error = "Directory is not empty.";
                    return false;
                }

                System.IO.Directory.Delete(path, recursive);
            }

            return true;
        }

        /// <summary>
        /// Attempts to delete the specified file.
        /// </summary>
        /// <param name="filename">The filename to delete.</param>
        /// <param name="error">The error, if any</param>
        /// <returns>True if successful, false otherwise</returns>
        public bool DeleteFile(string filename, out string error) {
            if (!IsValidFilePath(filename, out error)) {
                return false;
            }

            var resolvedPath = ResolvePath(filename);
            if (FileExists(resolvedPath, out error)) {
                System.IO.File.Delete(resolvedPath);
            }

            return true;
        }

        /// <summary>
        /// Gets a list of existing filenames for the given path. This is not recursive, only returns top-level files.
        /// </summary>
        /// <param name="path">The directory to get files from</param>
        /// <param name="error">The error, if any</param>
        /// <returns>A list of filenames</returns>
        public IList<string> GetFiles(string path, out string error) {
            if (!IsValidDirectoryPath(path, out error)) {
                return null;
            }

            if (!DirectoryExists(path, out error)) {
                return new List<string>();
            }

            var resolvedPath = ResolvePath(path);

            return System.IO.Directory.GetFiles(resolvedPath).Select(f => System.IO.Path.GetFileName(f)).ToList();
        }

        /// <summary>
        /// Gets a list of existing sub-directories in the given path. This is not recursive, only returns top-level directories.
        /// </summary>
        /// <param name="path">The directory to get sub-directories from</param>
        /// <param name="error">The error, if any</param>
        /// <returns>A list of directory names</returns>
        public IList<string> GetDirectories(string path, out string error) {
            if (!IsValidDirectoryPath(path, out error)) {
                return null;
            }

            if (!DirectoryExists(path, out error)) {
                return new List<string>();
            }

            var resolvedPath = ResolvePath(path);

            return System.IO.Directory.GetDirectories(resolvedPath).ToList();
        }
    }
}
