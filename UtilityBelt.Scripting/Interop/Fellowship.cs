﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Actions;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Information about the character's current fellowship
    /// </summary>
    public class Fellowship : IDisposable {
        private readonly ScriptManager _manager;
        private readonly ILogger _log;
        private readonly Dictionary<uint, FellowshipMember> _members = new Dictionary<uint, FellowshipMember>();

        /// <summary>
        /// Maximum number of fellowship members allowed
        /// </summary>
        public static readonly int MAX_FELLOW_COUNT = 9;

        #region public events
        /// <summary>
        /// Fired when a fellowship is joined or created by this character
        /// </summary>
        public event EventHandler<EventArgs> OnJoin;

        /// <summary>
        /// Fired when you are no long a part of the fellowship. The fellowship may still exist, you
        /// are just no longer a part of it at this point (quit, disbanded, or dismissed).
        /// </summary>
        public event EventHandler<EventArgs> OnLeave;

        /// <summary>
        /// Fired when someone other than yourself joins this fellowship.
        /// </summary>
        public event EventHandler<FellowshipChangedEventArgs> OnMembersChanged;
        #endregion // public events

        #region public properties
        /// <summary>
        /// Check if your character is in a fellowship
        /// </summary>
        public bool Exists { get; private set; }

        /// <summary>
        /// Check if a fellowship is open
        /// </summary>
        public bool IsOpen { get; private set; }

        /// <summary>
        /// Name of the fellowship
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The id of the fellowship leader
        /// </summary>
        public uint LeaderId { get; private set; }

        /// <summary>
        /// Wether or not this fellowship has experience sharing enabled
        /// </summary>
        public bool ShareExp { get; private set; }

        /// <summary>
        /// Wether or not this fellowship has even experience sharing
        /// </summary>
        public bool EvenExpSharing { get; private set; }

        /// <summary>
        /// Wether or not this fellowship is locked. Locked fellowships do not accept new members.
        /// </summary>
        public bool Locked { get; private set; }

        /// <summary>
        /// A dictionary of members in this fellowship. Includes things like current/max vitals of each member.
        /// The key is the object id of the member.
        /// </summary>
        public IDictionary<uint, FellowshipMember> Members => _members;
        #endregion // public properties

        public Fellowship() {
            _manager = ScriptManager.Instance;
            _log = _manager.Resolve<ILogger>();

            _manager.MessageHandler.Incoming.Fellowship_Disband += MessageHandler_Fellowship_Disband_S2C;
            _manager.MessageHandler.Incoming.Fellowship_Dismiss += MessageHandler_Fellowship_Dismiss_S2C;
            _manager.MessageHandler.Incoming.Fellowship_FellowStatsDone += MessageHandler_Fellowship_FellowStatsDone_S2C;
            _manager.MessageHandler.Incoming.Fellowship_FullUpdate += MessageHandler_Fellowship_FullUpdate_S2C;
            _manager.MessageHandler.Incoming.Fellowship_Quit += MessageHandler_Fellowship_Quit_S2C;
            _manager.MessageHandler.Incoming.Fellowship_UpdateFellow += MessageHandler_Fellowship_UpdateFellow_S2C;
        }

        #region public api

        /// <inheritdoc cref="ClientActions.FellowCreate(string, bool, ActionOptions, Action{FellowCreateAction})" />
        public FellowCreateAction Create(string name, bool shareExperience = true, ActionOptions options = null, Action<FellowCreateAction> callback = null) {
            return _manager.GameState.Actions.FellowCreate(name, shareExperience, options, callback);
        }

        /// <inheritdoc cref="ClientActions.FellowDisband(ActionOptions, Action{FellowDisbandAction})" />
        public FellowDisbandAction Disband(ActionOptions options = null, Action<FellowDisbandAction> callback = null) {
            return _manager.GameState.Actions.FellowDisband(options, callback);
        }


        /// <inheritdoc cref="ClientActions.FellowDismiss(uint, ActionOptions, Action{FellowDismissAction})" />
        public FellowDismissAction Dismiss(uint objectId, ActionOptions options = null, Action<FellowDismissAction> callback = null) {
            return _manager.GameState.Actions.FellowDismiss(objectId, options, callback);
        }

        /// <inheritdoc cref="ClientActions.FellowSetLeader(uint, ActionOptions, Action{FellowSetLeaderAction})" />
        public FellowSetLeaderAction SetLeader(uint objectId, ActionOptions options = null, Action<FellowSetLeaderAction> callback = null) {
            return _manager.GameState.Actions.FellowSetLeader(objectId, options, callback);
        }

        /// <inheritdoc cref="ClientActions.FellowQuit(bool, ActionOptions, Action{FellowQuitAction})" />
        public FellowQuitAction Quit(bool disband, ActionOptions options = null, Action<FellowQuitAction> callback = null) {
            return _manager.GameState.Actions.FellowQuit(disband, options, callback);
        }

        /// <inheritdoc cref="ClientActions.FellowRecruit(uint, ActionOptions, Action{FellowRecruitAction})" />
        public FellowRecruitAction Recruit(uint objectId, ActionOptions options = null, Action<FellowRecruitAction> callback = null) {
            return _manager.GameState.Actions.FellowRecruit(objectId, options, callback);
        }

        /// <inheritdoc cref="ClientActions.FellowSetOpen(bool, ActionOptions, Action{FellowSetOpenAction})" />
        public FellowSetOpenAction SetOpen(bool open, ActionOptions options = null, Action<FellowSetOpenAction> callback = null) {
            return _manager.GameState.Actions.FellowSetOpen(open, options, callback);
        }
        #endregion //public api

        private void ClearFellow() {
            Exists = false;
            Name = "";
        }

        private void MessageHandler_Fellowship_UpdateFellow_S2C(object sender, Fellowship_UpdateFellow_S2C_EventArgs e) {
            if (Exists == false) {
                Exists = true;
                OnJoin?.Invoke(this, System.EventArgs.Empty);
            }

            bool memberAdded = false;
            FellowshipMember member = null;

            if (!_members.TryGetValue(e.Data.ObjectId, out member)) {
                member = new FellowshipMember() {
                    Id = e.Data.ObjectId
                };
                _members.Add(member.Id, member);
                memberAdded = true;
            }

            switch (e.Data.Type) {
                case UtilityBelt.Common.Enums.FellowUpdateType.FullUpdate:
                    member.Level = e.Data.Fellow.Level;
                    member.Name = e.Data.Fellow.Name;
                    member.CurrentHealth = e.Data.Fellow.Health;
                    member.CurrentStamina = e.Data.Fellow.Stamina;
                    member.CurrentMana = e.Data.Fellow.Mana;
                    member.MaxHealth = e.Data.Fellow.HealthMax;
                    member.MaxStamina = e.Data.Fellow.StaminaMax;
                    member.MaxMana = e.Data.Fellow.ManaMax;
                    break;
                case UtilityBelt.Common.Enums.FellowUpdateType.UpdateVitals:
                    member.CurrentHealth = e.Data.Fellow.Health;
                    member.CurrentStamina = e.Data.Fellow.Stamina;
                    member.CurrentMana = e.Data.Fellow.Mana;
                    member.MaxHealth = e.Data.Fellow.HealthMax;
                    member.MaxStamina = e.Data.Fellow.StaminaMax;
                    member.MaxMana = e.Data.Fellow.ManaMax;
                    break;
                case UtilityBelt.Common.Enums.FellowUpdateType.UpdateStats:
                    // todo: what stats?
                    break;
            }

            if (memberAdded) {
                OnMembersChanged?.Invoke(this, new FellowshipChangedEventArgs(AddRemoveEventType.Added, member.Id, member));
            }
        }

        private void MessageHandler_Fellowship_Quit_S2C(object sender, Fellowship_Quit_S2C_EventArgs e) {
            ClearFellow();
            OnLeave?.Invoke(this, EventArgs.Empty);
        }

        private void MessageHandler_Fellowship_FullUpdate_S2C(object sender, Fellowship_FullUpdate_S2C_EventArgs e) {
            IsOpen = e.Data.Fellowship.Open;
            Name = e.Data.Fellowship.Name;
            LeaderId = e.Data.Fellowship.LeaderId;
            ShareExp = e.Data.Fellowship.ShareExperience;
            EvenExpSharing = e.Data.Fellowship.EventExperienceSplit;
            Locked = e.Data.Fellowship.Locked;

            foreach (var kv in e.Data.Fellowship.fellowshipTable.Table) {
                FellowshipMember member = null;
                if (!_members.TryGetValue(kv.Key, out member)) {
                    member = new FellowshipMember() {
                        Id = kv.Key
                    };
                    _members.Add(member.Id, member);
                }
                member.Level = kv.Value.Level;
                member.Name = kv.Value.Name;
                member.CurrentHealth = kv.Value.Health;
                member.CurrentStamina = kv.Value.Stamina;
                member.CurrentMana = kv.Value.Mana;
                member.MaxHealth = kv.Value.HealthMax;
                member.MaxStamina = kv.Value.StaminaMax;
                member.MaxMana = kv.Value.ManaMax;
            }

            if (Exists == false) {
                Exists = true;
                OnJoin?.Invoke(this, EventArgs.Empty);

                foreach (var member in _members.Values) {
                    OnMembersChanged?.Invoke(this, new FellowshipChangedEventArgs(AddRemoveEventType.Added, member.Id, member));
                }
            }
        }

        private void MessageHandler_Fellowship_FellowStatsDone_S2C(object sender, Fellowship_FellowStatsDone_S2C_EventArgs e) {
            
        }

        private void MessageHandler_Fellowship_Dismiss_S2C(object sender, Fellowship_Dismiss_S2C_EventArgs e) {
            if (e.Data.ObjectId == _manager.GameState.CharacterId) {
                ClearFellow();
                OnLeave?.Invoke(this, EventArgs.Empty);
            }
            else if (_members.TryGetValue(e.Data.ObjectId, out FellowshipMember member)) {
                _members.Remove(e.Data.ObjectId);
                OnMembersChanged?.Invoke(this, new FellowshipChangedEventArgs(AddRemoveEventType.Removed, e.Data.ObjectId, member));
            }
        }

        private void MessageHandler_Fellowship_Disband_S2C(object sender, Fellowship_Disband_S2C_EventArgs e) {
            ClearFellow();
            OnLeave?.Invoke(this, EventArgs.Empty);
        }

        public void Dispose() {
            _manager.MessageHandler.Incoming.Fellowship_Disband -= MessageHandler_Fellowship_Disband_S2C;
            _manager.MessageHandler.Incoming.Fellowship_Dismiss -= MessageHandler_Fellowship_Dismiss_S2C;
            _manager.MessageHandler.Incoming.Fellowship_FellowStatsDone -= MessageHandler_Fellowship_FellowStatsDone_S2C;
            _manager.MessageHandler.Incoming.Fellowship_FullUpdate -= MessageHandler_Fellowship_FullUpdate_S2C;
            _manager.MessageHandler.Incoming.Fellowship_Quit -= MessageHandler_Fellowship_Quit_S2C;
            _manager.MessageHandler.Incoming.Fellowship_UpdateFellow -= MessageHandler_Fellowship_UpdateFellow_S2C;
        }
    }
}
