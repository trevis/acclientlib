﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Common.Enums;
using ACE.DatLoader;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Events;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Represents a specific Attribute on a creature (Like Strength, Endurance, etc)
    /// </summary>
    public class Attribute {
        private WorldObject _weenie;
        private ScriptManager _manager;
        private CharacterState _characterState => _manager.GameState.Character;

        /// <summary>
        /// Attribute type
        /// </summary>
        public virtual AttributeId Type { get; }

        /// <summary>
        /// The amount of times this attribute has been raised
        /// </summary>
        public virtual uint PointsRaised { get; internal set; }

        /// <summary>
        /// starting point for advancement of the attribute (eg bonus points)
        /// </summary>
        public virtual uint InitLevel { get; internal set; }

        /// <summary>
        /// Total XP spent on this attribute
        /// </summary>
        public virtual uint Experience { get; internal set; }

        /// <summary>
        /// Base (unbuffed) attribute level
        /// </summary>
        public virtual int Base => (int)(InitLevel + PointsRaised);

        /// <summary>
        /// Current attribute level. Includes buffs/debuffs/vitae
        /// </summary>
        public virtual int Current {
            get {
                var multiplier = _characterState.GetEnchantmentsMultiplierModifier(Type);
                var additives = _characterState.GetEnchantmentsAdditiveModifier(Type);

                var effective = (int)Math.Round(Base * multiplier + additives);
                return Math.Max(effective, Base >= 10 ? 10 : 1);
            }
        }

        internal Attribute(AttributeId attributeId, WorldObject weenie) {
            Type = attributeId;
            _manager = ScriptManager.Instance;
            _weenie = weenie;
        }
    }
}
