﻿using ACE.DatLoader;
using ACE.DatLoader.FileTypes;
using ACE.Entity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Lib;
using UtilityBelt.Scripting.Interop;
using WattleScript.Interpreter;

namespace UtilityBelt.Scripting.Interop {
    /// <summary>
    /// Allows access to information about the local physics environment.
    /// If in a dungeon, its the entire dungeon environment.
    /// If in a building, it will be the building and outside landblocks if they are visible.
    /// If outside, it will be all adjacent landblocks.
    /// </summary>
    public class PhysicsEnvironment {
        private readonly ScriptManager _manager;
        private readonly ILogger _log;

        private Dictionary<uint, EnvCell> Cells = new Dictionary<uint, EnvCell>();
        private Dictionary<ushort, EnvCell> cellCache = new Dictionary<ushort, EnvCell>();
        private LandblockInfo _landblockInfo;
        private CellLandblock _landblockData;

        private uint _lastLandcell = 0;
        private List<uint> _visibleCells = new List<uint>();
        private List<uint> _visibleLandblocks = new List<uint>();

        /// <summary>
        /// True if your character is inside a dungeon. This does not count buildings.
        /// </summary>
        public bool IsDungeon { get; private set; }

        /// <summary>
        /// Wether your character is inside or not. Inside is buildings / dungeons.
        /// </summary>
        public bool IsInside => Cells.ContainsKey(CurrentLandcell);

        /// <summary>
        /// Wether or not your character is outside. Outside meaning on the landscape, not in a building or dungeon.
        /// </summary>
        public bool IsOutside => !IsInside;

        /// <summary>
        /// Current landcell your character is in
        /// </summary>
        public uint CurrentLandcell { get; private set; }

        /// <summary>
        /// Current landblock your character is in (landcell &amp; 0xFFFF0000)
        /// </summary>
        public uint CurrentLandblock => CurrentLandcell & 0xFFFF0000;

        /// <summary>
        /// A list of landblocks visible from your character's location. This does not mean you have a direct line of sight,
        /// but rather that it is close enough to not be despawned by the client.
        /// </summary>
        public IEnumerable<uint> VisibleLandblocks => _visibleLandblocks;

        /// <summary>
        /// A list of landcells that are visible from your characters location. This does not mean you have a direct line of sight,
        /// but rather that it is close enough to not be despawned by the client.
        /// </summary>
        public IEnumerable<uint> VisibleLandcells {
            get {
                if (_lastLandcell != CurrentLandcell) {
                    _lastLandcell = CurrentLandcell;
                    _visibleCells.Clear();

                    if (IsInside) {
                        if (Cells.TryGetValue(CurrentLandcell, out EnvCell currentEnvCell)) {
                            //_log.Log($"Current: {string.Join(", ", currentEnvCell.VisibleCells.Select(c => $"{c}").ToArray())}");
                            foreach (var cellId in currentEnvCell.VisibleCells) {
                                //_log.Log($"Trying to get cell: {(cellId)} @ {CurrentLandcell:X8} // Max: {(0x0100 + Cells.Count)}");
                                _visibleCells.Add(cellCache[(ushort)(cellId)].Id);
                            }
                        }
                        else {
                            _log.Log(LogLevel.Warning, $"Could not find current envcell");
                        }
                    }
                }

                return _visibleCells;
            }
        }

        /// <summary>
        /// True if you can see outside from your character's location
        /// </summary>
        public bool CanSeeOutside {
            get {
                if (IsDungeon) {
                    return false;
                }
                else if (Cells.TryGetValue(CurrentLandcell, out EnvCell currentEnvCell)) {
                    return currentEnvCell.SeenOutside;
                }
                return true;
             }
        }

        internal PhysicsEnvironment() {
            _manager = ScriptManager.Instance;
            _log = _manager.Resolve<ILogger>();
        }

        #region Public API
        /// <summary>
        /// Check if the given landcell id is visible. This does not mean you have a direct line of sight,
        /// but rather that it is close enough to not be despawned by the client.
        /// </summary>
        /// <param name="landcell">The landcell to check</param>
        /// <returns>true if visible, false if not</returns>
        public bool IsLandcellVisible(uint landcell) {
            if (landcell == CurrentLandcell)
                return true;

            if (IsDungeon || !CanSeeOutside) {
                return VisibleLandcells.Contains(landcell);
            }
            else {
                return VisibleLandblocks.Contains(landcell & 0xFFFF0000);
            }
        }
        #endregion // Public API

        internal void SetLocation(Position position) {
            var oldLandblock = CurrentLandblock;
            CurrentLandcell = position.Landcell;

            if (oldLandblock != CurrentLandblock) {
                LoadPhysicsEnvironment();
            }
        }

        private async void LoadPhysicsEnvironment() {
            var lbInfoId = 0xFFFE + CurrentLandblock;
            var lbDataId = 0xFFFF + CurrentLandblock;
            _log.Log(LogLevel.Debug, $"Attempting to load landblock: 0x{lbInfoId:X8} (Cell: 0x{CurrentLandcell:X8})");

            Cells.Clear();
            cellCache.Clear();
            _visibleLandblocks.Clear();

            _landblockInfo = await _manager.Resolve<IAsyncDatReader>().ReadFromDat<LandblockInfo>(lbInfoId);
            _landblockData = await _manager.Resolve<IAsyncDatReader>().ReadFromDat<CellLandblock>(lbDataId);

            for (var i = 0; i < _landblockInfo.NumCells; i++) {
                EnvCell cell = await _manager.Resolve<IAsyncDatReader>().ReadFromDat<EnvCell>((uint)((_landblockInfo.Id >> 16 << 16) + 0x00000100 + i));

                Cells.Add(cell.Id, cell);
                cellCache.Add((ushort)(0x0100 + i), cell);
            }

            IsDungeon = CheckIsDungeon();

            if (!IsDungeon) {
                _visibleLandblocks.Add(CurrentLandblock);
                _visibleLandblocks.AddRange(GetAdjacentLandblockIDs(CurrentLandblock));
            }
        }

        // logic from ace
        private bool CheckIsDungeon() {
            int lbx = (int)(CurrentLandblock >> 24);
            int lby = (int)(CurrentLandblock << 8 >> 24);

            // hack for NW island
            // did a worldwide analysis for adding watercells into the formula,
            // but they are inconsistently defined for some of the edges of map unfortunately
            if (lbx < 0x08 && lby > 0xF8) {
                return false;
            }

            // a dungeon landblock is determined by:
            // - all heights being 0
            // - having at least 1 EnvCell (0x100+)
            // - contains no buildings
            foreach (var height in _landblockData.Height) {
                if (height != 0) {
                    return false;
                }
            }
            
            return _landblockInfo != null && _landblockInfo.NumCells > 0 && _landblockInfo.Buildings != null && _landblockInfo.Buildings.Count == 0;
        }

        /// <summary>
        /// Returns the list of adjacent landblock IDs for a landblock
        /// </summary>
        private static List<uint> GetAdjacentLandblockIDs(uint landblock) {
            var adjacents = new List<uint>();

            var north = GetAdjacentID(landblock, Adjacency.North);
            var south = GetAdjacentID(landblock, Adjacency.South);
            var west = GetAdjacentID(landblock, Adjacency.West);
            var east = GetAdjacentID(landblock, Adjacency.East);
            var northwest = GetAdjacentID(landblock, Adjacency.NorthWest);
            var northeast = GetAdjacentID(landblock, Adjacency.NorthEast);
            var southwest = GetAdjacentID(landblock, Adjacency.SouthWest);
            var southeast = GetAdjacentID(landblock, Adjacency.SouthEast);

            if (north != 0)
                adjacents.Add(north);
            if (south != 0)
                adjacents.Add(south);
            if (west != 0)
                adjacents.Add(west);
            if (east != 0)
                adjacents.Add(east);
            if (northwest != 0)
                adjacents.Add(northwest);
            if (northeast != 0)
                adjacents.Add(northeast);
            if (southwest != 0)
                adjacents.Add(southwest);
            if (southeast != 0)
                adjacents.Add(southeast);

            return adjacents;
        }

        private enum Adjacency {
            NorthWest = 0,
            North = 1,
            NorthEast = 2,
            East = 3,
            SouthEast = 4,
            South = 5,
            SouthWest = 6,
            West = 7
        }

        /// <summary>
        /// From ACE: Returns an adjacent landblock ID for a landblock
        /// </summary>
        private static uint GetAdjacentID(uint landblock, Adjacency adjacency) {
            int lbx = (int)(landblock >> 24);
            int lby = (int)(landblock << 8 >> 24);

            switch (adjacency) {
                case Adjacency.North:
                    lby += 1;
                    break;
                case Adjacency.South:
                    lby -= 1;
                    break;
                case Adjacency.West:
                    lbx -= 1;
                    break;
                case Adjacency.East:
                    lbx += 1;
                    break;
                case Adjacency.NorthWest:
                    lby += 1;
                    lbx -= 1;
                    break;
                case Adjacency.NorthEast:
                    lby += 1;
                    lbx += 1;
                    break;
                case Adjacency.SouthWest:
                    lby -= 1;
                    lbx -= 1;
                    break;
                case Adjacency.SouthEast:
                    lby -= 1;
                    lbx += 1;
                    break;
            }

            if (lbx < 0 || lbx > 254 || lby < 0 || lby > 254)
                return 0;

            return (uint)lbx << 24 | (uint)lby << 16;
        }
    }
}
