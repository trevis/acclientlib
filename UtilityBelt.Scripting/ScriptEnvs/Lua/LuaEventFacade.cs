﻿using WattleScript.Interpreter.Interop;
using WattleScript.Interpreter;
using System;
using UtilityBelt.Scripting.Interop;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using static UtilityBelt.Scripting.ScriptEnvs.Lua.LuaEventFacade;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.ScriptEnvs.Lua {
    public class LuaEventFacade : IUserDataType {
        Func<object, ScriptExecutionContext, CallbackArguments, DynValue> m_AddCallback;
        Func<object, ScriptExecutionContext, CallbackArguments, DynValue> m_RemoveCallback;
        Func<object, ScriptExecutionContext, CallbackArguments, DynValue> m_OnceCallback;
        Func<object, ScriptExecutionContext, CallbackArguments, DynValue> m_UntilCallback;
        private readonly LuaEventDescriptor parent;
        object m_Object;

        internal class LuaEventCache {
            internal WeakReference Object;
            internal Action<object> RemoveCallback;
            internal Closure Closure;

            public LuaEventCache(object obj, Closure closure, Action<object> removeCallback) {
                Object = new WeakReference(obj);
                RemoveCallback = removeCallback;
                Closure = closure;
            }

            public void TryRemove() {
                try {
                    if (Object != null && Object.IsAlive) {
                        RemoveCallback(Object.Target);
                    }
                }
                catch (Exception ex) {
                    ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString());
                }
            }
        }

        private static Dictionary<string, List<LuaEventCache>> _registeredEventHandlers = new Dictionary<string, List<LuaEventCache>>();

        public LuaEventFacade(LuaEventDescriptor parent, object obj) {
            this.parent = parent;
            m_Object = obj;
            m_AddCallback = parent.AddCallback;
            m_RemoveCallback = parent.RemoveCallback;
            m_OnceCallback = parent.OnceCallback;
            m_UntilCallback = parent.UntilCallback;
        }

        internal static void RemoveScript(string name) {
            if (_registeredEventHandlers.TryGetValue(name, out var eventCache)) {
                foreach (var evt in eventCache) {
                    evt.TryRemove();
                }
                _registeredEventHandlers.Remove(name);
            }
        }

        private UBScript GetScript(Script script) {
            return ScriptManager.Instance?.GetAll()?.FirstOrDefault(s => s.WattleScript == script);
        }

        public DynValue Index(Script script, DynValue index, bool isDirectIndexing) {
            if (index.Type == DataType.String) {
                if (index.String.ToLower() == "add") {
                    return DynValue.NewCallback((c, a) => {
                        var closure = MakeClosure(a);
                        AddEventHandler(script, new LuaEventCache(m_Object, closure, (obj) => {
                            parent.RemoveCallback(obj, closure);
                        }));
                        return m_AddCallback(m_Object, c, a);
                    });
                }
                else if (index.String.ToLower() == "remove") {
                    return DynValue.NewCallback((c, a) => {
                        RemoveEventHandler(script, MakeClosure(a));
                        return m_RemoveCallback(m_Object, c, a);
                    });
                }
                else if (index.String.ToLower() == "once") {
                    return DynValue.NewCallback((c, a) => {
                        var closure = MakeClosure(a);
                        AddEventHandler(script, new LuaEventCache(m_Object, closure, (obj) => {
                            parent.RemoveCallback(obj, closure);
                        }));
                        return m_OnceCallback(m_Object, c, a);
                    });
                }
                else if (index.String.ToLower() == "until") {
                    return DynValue.NewCallback((c, a) => {
                        var closure = MakeClosure(a);
                        AddEventHandler(script, new LuaEventCache(m_Object, closure, (obj) => {
                            parent.RemoveCallback(obj, closure);
                        }));
                        return m_UntilCallback(m_Object, c, a);
                    });
                }
            }

            throw new ScriptRuntimeException("Events only support add, remove, once, until methods");
        }

        private Closure MakeClosure(CallbackArguments a) {
            return a.AsType(0, string.Format("userdata<{0}>.{1}.remove", parent.EventInfo.DeclaringType, parent.EventInfo.Name), DataType.Function, false).Function;
        }

        private void AddEventHandler(Script script, LuaEventCache luaEventCache) {
            var ubScript = GetScript(script);

            if (!_registeredEventHandlers.ContainsKey(ubScript.Name)) {
                _registeredEventHandlers.Add(ubScript.Name, new List<LuaEventCache>());
            }

            _registeredEventHandlers[ubScript.Name].Add(luaEventCache);
        }

        private void RemoveEventHandler(Script script, Closure closure) {
            var ubScript = GetScript(script);

            if (_registeredEventHandlers.TryGetValue(ubScript.Name, out var eventCache)) {
                foreach (var evt in eventCache.ToArray()) {
                    if (evt.Closure == closure) {
                        eventCache.Remove(evt);
                        evt.TryRemove();
                    }
                }
            }
        }

        public bool SetIndex(Script script, DynValue index, DynValue value, bool isDirectIndexing) {
            throw new ScriptRuntimeException("Events do not have settable fields");
        }

        public DynValue MetaIndex(Script script, string metaname) {
            return DynValue.Nil;
        }
    }
}