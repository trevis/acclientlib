﻿using WattleScript.Interpreter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace UtilityBelt.Scripting.ScriptEnvs.Lua {
    public static class LuaEnumExtensions {
        /// <summary>
        /// Check if an enum value has the specified enum flags
        /// </summary>
        /// <param name="source">source enum</param>
        /// <param name="flagToCheckFor">flag to check</param>
        /// <returns></returns>
        public static bool HasFlags(this Enum source, params Enum[] flagToCheckFor) {
            // /ub lexec return ObjectType.Clothing.hasFlags(ObjectType.Armor)
            var underlyingType = Enum.GetUnderlyingType(source.GetType());
            if ((underlyingType == typeof(byte)) || (underlyingType == typeof(ushort)) || (underlyingType == typeof(uint)) || (underlyingType == typeof(ulong))) {
                foreach (var flag in flagToCheckFor) {
                    if ((Convert.ToUInt64(source) & Convert.ToUInt64(flagToCheckFor)) != Convert.ToUInt64(flagToCheckFor)) {
                        return false;
                    }
                }

                return true;
            }
            else {
                foreach (var flag in flagToCheckFor) {
                    if ((Convert.ToInt64(source) & Convert.ToInt64(flagToCheckFor)) != Convert.ToInt64(flagToCheckFor)) {
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Add the specified flags to an enum, using binary 'or' operator
        /// </summary>
        /// <param name="source">source enum</param>
        /// <param name="flagsToOr">enum flags to binary op or to source</param>
        /// <returns>source enum with or'd flags</returns>
        public static object AddFlags(this Enum source, params Enum[] flagsToOr) {
            // /ub lexec return ObjectType.Clothing.AddFlags(ObjectType.Armor, ObjectType.MeleeWeapon, ObjectType.Jewelry)
            var underlyingType = Enum.GetUnderlyingType(source.GetType());
            if ((underlyingType == typeof(byte)) || (underlyingType == typeof(ushort)) || (underlyingType == typeof(uint)) || (underlyingType == typeof(ulong))) {
                var res = Convert.ToUInt64(source);
                foreach (var flag in flagsToOr) {
                    res |= Convert.ToUInt64(flag);
                }

                return Enum.ToObject(source.GetType(), Convert.ChangeType(res, underlyingType));
            }
            else {
                var res = Convert.ToInt64(source);
                foreach (var flag in flagsToOr) {
                    res |= Convert.ToInt64(flag);
                }

                return Enum.ToObject(source.GetType(), Convert.ChangeType(res, underlyingType));
            }
        }

        /// <summary>
        /// Remove the specified flags from an enum, using binary 'and not' operator
        /// </summary>
        /// <param name="source">source enum</param>
        /// <param name="flagsToOr">enum flags to binary op or to source</param>
        /// <returns>source enum with or'd flags</returns>
        public static object RemoveFlags(this Enum source, params Enum[] flagsToOr) {
            // /ub lexec return ObjectType.Clothing.AddFlags(ObjectType.Armor, ObjectType.MeleeWeapon, ObjectType.Jewelry).RemoveFlags(ObjectType.Clothing, ObjectType.Jewelry).HasFlag(ObjectType.MeleeWeapon)
            var underlyingType = Enum.GetUnderlyingType(source.GetType());
            if ((underlyingType == typeof(byte)) || (underlyingType == typeof(ushort)) || (underlyingType == typeof(uint)) || (underlyingType == typeof(ulong))) {
                var res = Convert.ToUInt64(source);
                foreach (var flag in flagsToOr) {
                    res &= ~Convert.ToUInt64(flag);
                }

                return Enum.ToObject(source.GetType(), Convert.ChangeType(res, underlyingType));
            }
            else {
                var res = Convert.ToInt64(source);
                foreach (var flag in flagsToOr) {
                    res &= ~Convert.ToInt64(flag);
                }

                return Enum.ToObject(source.GetType(), Convert.ChangeType(res, underlyingType));
            }
        }

        public static long ToNumber(this Enum source) {
            var underlyingType = Enum.GetUnderlyingType(source.GetType());
            if ((underlyingType == typeof(byte)) || (underlyingType == typeof(ushort)) || (underlyingType == typeof(uint)) || (underlyingType == typeof(ulong))) {
                return (long)Convert.ToUInt64(source);
            }
            else {
                return Convert.ToInt64(source);
            }
        }
    }
}
