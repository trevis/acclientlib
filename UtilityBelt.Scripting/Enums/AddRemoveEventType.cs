﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    public enum AddRemoveEventType {
        Added,
        Removed
    }
}
