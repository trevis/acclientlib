﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    public enum CastEventType {
        Untargetted = 0,
        Targetted = 1
    }
}
