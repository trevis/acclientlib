﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// Decal object classes, for compatibility
    /// </summary>
    public enum ObjectClass {
        Unknown,
        MeleeWeapon,
        Armor,
        Clothing,
        Jewelry,
        Monster,
        Food,
        Money,
        Misc,
        MissileWeapon,
        Container,
        Gem,
        SpellComponent,
        Key,
        Portal,
        TradeNote,
        ManaStone,
        Plant,
        BaseCooking,
        BaseAlchemy,
        BaseFletching,
        CraftedCooking,
        CraftedAlchemy,
        CraftedFletching,
        Player,
        Vendor,
        Door,
        Corpse,
        Lifestone,
        HealingKit,
        Lockpick,
        WandStaffOrb,
        Bundle,
        Book,
        Journal,
        Sign,
        Housing,
        Npc,
        Foci,
        Salvage,
        Ust,
        Services,
        Scroll,
        NumObjectClasses
    }
}
