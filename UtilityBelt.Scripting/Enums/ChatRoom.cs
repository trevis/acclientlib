﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// Chat channels
    /// </summary>
    public enum ChatChannel {
        /// <summary>
        /// None
        /// </summary>
        None,

        /// <summary>
        /// Allegiance chat
        /// </summary>
        Allegiance,

        /// <summary>
        /// General chat
        /// </summary>
        General,

        /// <summary>
        /// Trade chat
        /// </summary>
        Trade,
        
        /// <summary>
        /// Looking for group chat
        /// </summary>
        LFG,

        /// <summary>
        /// Roleplay chat
        /// </summary>
        Roleplay,

        /// <summary>
        /// Olthoi chat
        /// </summary>
        Olthoi,

        /// <summary>
        /// Society general chat?
        /// </summary>
        Society,

        /// <summary>
        /// Celestial Hand society chat
        /// </summary>
        CelestialHand,

        /// <summary>
        /// Eldrytch Web society chat
        /// </summary>
        EldrytchWeb,
        
        /// <summary>
        /// Radiant Blood society chat
        /// </summary>
        RadiantBlood
    }
}
