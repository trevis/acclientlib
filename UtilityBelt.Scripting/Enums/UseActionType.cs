﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// The type of interaction when calling a ObjectUse action. This is calculated automatically by the action.
    /// </summary>
    public enum UseActionType {
        /// <summary>
        /// Generic use action, just waits for a use done
        /// </summary>
        Default,

        /// <summary>
        /// Vendor use action, waits for vendor info to be sent over
        /// </summary>
        Vendor,

        /// <summary>
        /// Using an object that summons a portal
        /// </summary>
        SummonPortal,

        /// <summary>
        /// Using a portal
        /// </summary>
        Portal
    }
}
