﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    public enum FellowshipEventType {
        Create = 0,
        Quit = 1,
        Dismiss = 2,
        Recruit = 3,
        Disband = 4
    }
}
