﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// Represents the current state of the game client.
    /// </summary>
    public enum ClientState {
        /// <summary>
        /// Initial client state.
        /// </summary>
        Initial = 0,
        /// <summary>
        /// Client is done initializing.
        /// </summary>
        Game_Started = 1,
        /// <summary>
        /// Character select screen
        /// </summary>
        Character_Select_Screen = 2,
        /// <summary>
        /// Character creation screen
        /// </summary>
        Creating_Character = 3,
        /// <summary>
        /// Logging into the game
        /// </summary>
        Entering_Game = 4,
        /// <summary>
        /// Got Player details while logging in
        /// </summary>
        PlayerDesc_Received = 5,
        /// <summary>
        /// Fully logged in to the game
        /// </summary>
        In_Game = 6,
        /// <summary>
        /// Logging out
        /// </summary>
        Logging_Out = 7,
        /// <summary>
        /// Disconnected from server
        /// </summary>
        Disconnected = 8
    }
}
