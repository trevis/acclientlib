﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    public enum ScriptStartupType {
        Manual,
        Global,
        Account,
        Character
    }
}
