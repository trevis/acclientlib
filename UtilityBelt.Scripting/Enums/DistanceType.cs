﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Scripting.Enums {
    /// <summary>
    /// A distance type used for checking distances between things. Can either be 2d or 2d
    /// </summary>
    public enum DistanceType {
        /// <summary>
        /// 2d distance
        /// </summary>
        T2D,
        /// <summary>
        /// 3d distance
        /// </summary>
        T3D
    }
}
