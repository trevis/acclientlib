﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class SleepAction : QueueAction {
        private DateTime start;

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public uint Milliseconds { get; private set; }

        public SleepAction(uint milliseconds, ActionOptions options = null) : base(options) {
            Milliseconds = milliseconds;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.GameState.OnRender2D += GameState_OnRender2D;
        }

        private void GameState_OnRender2D(object sender, EventArgs e) {
            if (DateTime.UtcNow - start > TimeSpan.FromMilliseconds(Milliseconds)) {
                SetPermanentResult(ActionError.None);
            }
        }

        protected override bool Execute() {
            Options.TimeoutMilliseconds = Milliseconds * 2;
            start = DateTime.UtcNow;
            return true;
        }

        protected override void Stop() {
            Manager.GameState.OnTick -= GameState_OnRender2D;
        }
    }
}
