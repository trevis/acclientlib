﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    /// <summary>
    /// Send a tell to an object id. This is used for Spell Professors.
    /// </summary>
    public class SendTellByIdAction : QueueAction {
        /// <summary>
        /// The message to send
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// The id of the object to send a tell to
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        /// <summary>
        /// Create a new SendTellById action. This action is not run until it is added to the queue.
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="message"></param>
        /// <param name="options"></param>
        public SendTellByIdAction(uint objectId, string message, ActionOptions options = null) : base(options) {
            Message = message;
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            //if (Manager.GameState?.WorldState?.GetWeenie(ObjectId) == null) {
            //    SetPermanentResult(ActionError.InvalidSourceObject, "Could not find object to tell by id: {}");
            //    return false;
            //}

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().SendTellByObjectId(ObjectId, Message);
            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
