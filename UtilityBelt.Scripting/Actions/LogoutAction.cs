﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class LogoutAction : QueueAction {
        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.LoginLogoff;

        public LogoutAction(ActionOptions options = null) : base(options) {

        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            switch (Manager.GameState.State) {
                case ClientState.Entering_Game:
                case ClientState.PlayerDesc_Received:
                case ClientState.In_Game:
                    return true;
            }

            SetPermanentResult(ActionError.NotLoggedIn);

            return false;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.GameState.OnStateChanged += OnStateChanged;
        }

        protected override bool Execute() {
            // make sure we are ingame
            if (Manager.GameState.State != ClientState.In_Game) {
                SetTemporaryResult(ActionError.NotLoggedIn);
                return false;
            }

            Manager.Resolve<IClientActionsRaw>().Logout();
            return true;
        }

        protected override void Stop() {
            Manager.GameState.OnStateChanged -= OnStateChanged;
        }

        private void OnStateChanged(object sender, StateChangedEventArgs e) {
            if (e.NewState == ClientState.Character_Select_Screen) {
                SetPermanentResultAfter(TimeSpan.FromMilliseconds(1), ActionError.None);
            }
        }
    }
}
