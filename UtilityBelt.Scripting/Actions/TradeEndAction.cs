﻿using ACE.Entity;
using System;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class TradeEndAction : QueueAction {
        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Trade;

        public TradeEndAction(ActionOptions options = null) : base(options) {

        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must be trading
            if (!Manager.GameState.Character.Trade.IsOpen) {
                SetPermanentResult(ActionError.NoTradePartner);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Trade_CloseTrade += Incoming_Trade_CloseTrade;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().TradeEnd();
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Trade_CloseTrade -= Incoming_Trade_CloseTrade;
        }

        private void Incoming_Trade_CloseTrade(object sender, UtilityBelt.Common.Messages.Events.Trade_CloseTrade_S2C_EventArgs e) {
            SetPermanentResult(ActionError.None);
        }
    }
}
