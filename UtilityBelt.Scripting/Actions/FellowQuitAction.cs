﻿using ACE.Entity;
using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class FellowQuitAction : QueueAction {
        /// <summary>
        /// Wether or not the fellowship was disbanded when you quit
        /// </summary>
        public bool Disband { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Fellow;

        public FellowQuitAction(bool disband, ActionOptions options = null) : base(options) {
            Disband = disband;
        }

        protected override void UpdateDefaultOptions() {
            
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // check if we are in a fellowship
            if (!Manager.GameState.Character.Fellowship.Exists) {
                SetPermanentResult(ActionError.NotInFellow);
                return false;
            }

            // if disbanding, check if we are the fellowship leader
            if (Disband && Manager.GameState.Character.Fellowship.LeaderId != Manager.GameState.CharacterId) {
                SetPermanentResult(ActionError.NotTheLeader);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Fellowship_Quit += Incoming_Fellowship_Quit;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().FellowshipQuit(Disband);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Fellowship_Quit -= Incoming_Fellowship_Quit;
        }

        private void Incoming_Fellowship_Quit(object sender, UtilityBelt.Common.Messages.Events.Fellowship_Quit_S2C_EventArgs e) {
            SetPermanentResult(ActionError.None);
        }
    }
}
