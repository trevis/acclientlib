﻿using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class VendorAddToSellListAction : QueueAction {
        /// <summary>
        /// The id of the object that was being added to the vendor sell list
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Vendor;

        public VendorAddToSellListAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must have vendor open
            if (!Manager.GameState.WorldState.Vendor.IsOpen) {
                SetPermanentResult(ActionError.VendorNotOpen);
                return false;
            }

            // TODO: verify vendor will accept this item category, as well as will buy an item this expensive
            // TODO: verify we own this item

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {

        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().VendorAddToSellList(ObjectId);

            // we dont really verify this, since its client side only...
            SetPermanentResultAfter(TimeSpan.FromMilliseconds(1), ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
