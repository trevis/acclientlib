﻿using ACE.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectMoveAction : QueueAction {
        /// <summary>
        /// The object id to move
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The target object id to move it to
        /// </summary>
        public uint TargetId { get; }

        /// <summary>
        /// The slot in the container to move it to (0 is the first slot)
        /// </summary>
        public uint Slot { get; }

        /// <summary>
        /// Wether or not to try and merge this with an existing stack in the container
        /// </summary>
        public bool Stack { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Inventory;

        public ObjectMoveAction(uint objectId, uint targetId, uint slot = 0, bool stack = true, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            TargetId = targetId;
            Slot = slot;
            Stack = stack;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // item must exist
            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);
            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // target must exist
            var target = Manager.GameState.WorldState.GetWeenie(TargetId);
            if (target == null) {
                SetPermanentResult(ActionError.InvalidTargetObject);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_ServerSaysContainID += Incoming_Item_ServerSaysContainID;
            Manager.MessageHandler.Incoming.Character_ServerSaysAttemptFailed += Incoming_Character_ServerSaysAttemptFailed;
            Manager.MessageHandler.Incoming.Item_ServerSaysRemove += Incoming_Item_ServerSaysRemove;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().MoveWeenie(ObjectId, TargetId, Slot, Stack);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_ServerSaysContainID -= Incoming_Item_ServerSaysContainID;
            Manager.MessageHandler.Incoming.Character_ServerSaysAttemptFailed -= Incoming_Character_ServerSaysAttemptFailed;
            Manager.MessageHandler.Incoming.Item_ServerSaysRemove -= Incoming_Item_ServerSaysRemove;
        }

        private void Incoming_Item_ServerSaysRemove(object sender, Common.Messages.Events.Item_ServerSaysRemove_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                SetPermanentResultAfter(TimeSpan.FromMilliseconds(100), ActionError.None);
            }
        }

        private void Incoming_Character_ServerSaysAttemptFailed(object sender, UtilityBelt.Common.Messages.Events.Character_ServerSaysAttemptFailed_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                SetPermanentResult(ActionError.ServerError, $"Server Said: {e.Data.Reason}");
            }
        }

        private void Incoming_Item_ServerSaysContainID(object sender, UtilityBelt.Common.Messages.Events.Item_ServerSaysContainID_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId && e.Data.ContainerId == TargetId) {
                SetPermanentResultAfter(TimeSpan.FromMilliseconds(100), ActionError.None);
            }
        }
    }
}
