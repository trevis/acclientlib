﻿using ACE.Entity;
using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;
using static System.Collections.Specialized.BitVector32;

namespace UtilityBelt.Scripting.Actions {
    public class VendorSellAction : QueueAction {
        private bool _addedActionsToPreconditions;

        /// <summary>
        /// The id of the object that was being added to the vendor buy list
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The amount of items to add
        /// </summary>
        public uint Amount { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Vendor;

        //TODO: maybe an overload that accepts item name?
        public VendorSellAction(uint objectId, uint amount = 1, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            Amount = amount;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must have vendor open
            if (!Manager.GameState.WorldState.Vendor.IsOpen) {
                SetPermanentResult(ActionError.VendorNotOpen);
                return false;
            }

            // TODO: verify vendor will accept this item category, as well as will buy an item this expensive
            // TODO: verify we own this item

            return true;
        }

        protected override void UpdatePreconditions() {
            if (!_addedActionsToPreconditions) {
                Preconditions.Enqueue(new VendorClearSellListAction());
                if (Manager.GameState.WorldState.TryGetWeenie(ObjectId, out var wo)) {
                    if (wo.Value(IntId.StackSize, 1) != Amount) {
                        Preconditions.Enqueue(new ObjectSplitAction(wo.Id, Manager.GameState.Character.Id, Amount));
                    }
                }
                Preconditions.Enqueue(new VendorAddToSellListAction(ObjectId));
                Preconditions.Enqueue(new VendorBuyAllAction());
                _addedActionsToPreconditions = true;
            }
        }

        protected override void Start() {

        }

        protected override bool Execute() {
            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {

        }
    }
}
