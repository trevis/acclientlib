﻿using ACE.Entity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectUseAction : QueueAction {
        private bool _gotUseDone;
        private bool _gotVendorInfo;
        private bool _ignoreUseDone = false;
        private SleepAction _sharedCooldownSleep;

        /// <summary>
        /// The id of the used object
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The id of the target object.
        /// </summary>
        public uint TargetId { get; }

        /// <summary>
        /// If this was a UseActionType.SummonPortal (because using this object summons a portal), this is the portal that was summoned.
        /// </summary>
        public uint SummonedPortalId { get; private set; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => IsNavType() ? ActionType.Navigation : ActionType.Inventory;

        public UseActionType UseActionType {
            get {
                var wo = Manager.GameState.WorldState.GetWeenie(ObjectId);
                switch (wo?.ObjectClass) {
                    case ObjectClass.Vendor:
                        return UseActionType.Vendor;
                    case ObjectClass.Portal:
                        return UseActionType.Portal;
                    default:
                        if (wo?.SpellId == (uint)SpellId.SummonPortal1 || wo?.SpellId == (uint)SpellId.SummonPortal2 || wo?.SpellId == (uint)SpellId.SummonPortal3) {
                            return UseActionType.SummonPortal;
                        }
                        return UseActionType.Default;
                }
            }
        }

        public ObjectUseAction(uint objectId, uint targetId = 0, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            TargetId = targetId;
        }

        private bool IsNavType() {
            if (Manager.GameState.WorldState.TryGetWeenie(ObjectId, out WorldObject weenie)) {
                switch (weenie.ObjectType) {
                    case ObjectType.Portal:
                    case ObjectType.Creature:
                        return true;
                }
            }
            return false;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = (uint?)(IsNavType() ? 5000 : 15000);
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // item must exist
            if (!Manager.GameState.WorldState.WeenieExists(ObjectId)) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // target must exist (if specified)
            if (TargetId != 0 && !Manager.GameState.WorldState.WeenieExists(TargetId)) {
                SetPermanentResult(ActionError.InvalidTargetObject);
                return false;
            }
            return true;
        }

        protected override void UpdatePreconditions() {
            var wo = Manager.GameState.WorldState.GetWeenie(ObjectId);

            if (wo?.IsOnSharedCooldown() == true) {
                Manager.Resolve<ILogger>().LogTrace($"ObjectUse: {wo} on cooldown for {wo.CooldownTimeoutMilliseconds}ms.");
                // TODO: need an action that can just delay here... but it also shouldn't block any queues?
                _sharedCooldownSleep = new SleepAction((uint)wo.CooldownTimeoutMilliseconds);
                Preconditions.Enqueue(_sharedCooldownSleep);
                return;
            }
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Communication_TransientString += Incoming_Communication_TransientString;
            Manager.MessageHandler.Incoming.Item_UseDone += Incoming_Item_UseDone;
            Manager.GameState.Character.OnSharedCooldownsChanged += Character_OnSharedCooldownsChanged;
            switch (UseActionType) {
                case UseActionType.Vendor:
                    Manager.MessageHandler.Incoming.Vendor_VendorInfo += Incoming_Vendor_VendorInfo;
                    break;
                case UseActionType.SummonPortal:
                    Manager.MessageHandler.Incoming.Item_CreateObject += Incoming_Item_CreateObject;
                    break;
                case UseActionType.Portal:
                    Manager.GameState.Character.OnPortalSpaceExited += Character_OnPortalSpaceExited;
                    break;
            }
        }

        protected override bool Execute() {
            _ignoreUseDone = false;

            if (TargetId != 0) {
                Manager.Resolve<IClientActionsRaw>().ApplyWeenie(ObjectId, TargetId);
            }
            else {
                Manager.Resolve<IClientActionsRaw>().UseWeenie(ObjectId);

                // check for ust. Ust use doesn't send a message to the server, its all client side so we set result immediately
                if (Manager.GameState.WorldState.GetWeenie(ObjectId)?.ClassId == 20646) {
                    SetPermanentResult(ActionError.None);
                }
            }

            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_UseDone -= Incoming_Item_UseDone;
            Manager.MessageHandler.Incoming.Communication_TransientString -= Incoming_Communication_TransientString;
            Manager.GameState.Character.OnSharedCooldownsChanged -= Character_OnSharedCooldownsChanged;
            switch (UseActionType) {
                case UseActionType.Vendor:
                    Manager.MessageHandler.Incoming.Vendor_VendorInfo -= Incoming_Vendor_VendorInfo;
                    break;
                case UseActionType.SummonPortal:
                    Manager.MessageHandler.Incoming.Item_CreateObject -= Incoming_Item_CreateObject;
                    break;
                case UseActionType.Portal:
                    Manager.GameState.Character.OnPortalSpaceExited += Character_OnPortalSpaceExited;
                    break;
            }
        }

        private void Character_OnSharedCooldownsChanged(object sender, SharedCooldownsChangedEventArgs e) {
            var wo = Manager.GameState.WorldState.GetWeenie(ObjectId);
            if (e.Cooldown.Id == wo?.Value(IntId.SharedCooldown, -1) && _sharedCooldownSleep != null && _sharedCooldownSleep.IsFinished == false) {
                Manager.Resolve<ILogger>().LogTrace($"ObjectUse: {wo} shared cooldown ended early. Cancelling sleep");
                _sharedCooldownSleep?.SetPermanentResult(ActionError.None);
            }
        }

        private void Incoming_Communication_TransientString(object sender, Communication_TransientString_S2C_EventArgs e) {
            if (e.Data.Message.Contains("have used this item too recently")) {
                _ignoreUseDone = true;
                SetTemporaryResult(ActionError.OnCooldown, e.Data.Message);
            }
        }

        private void Character_OnPortalSpaceExited(object sender, EventArgs e) {
            SetPermanentResult(ActionError.None);
        }

        private void Incoming_Item_CreateObject(object sender, Common.Messages.Events.Item_CreateObject_S2C_EventArgs e) {
            if (Manager.GameState.WorldState.TryGetWeenie(e.Data.ObjectId, out var weenie)) {
                if (weenie.ObjectClass == ObjectClass.Portal) {
                    SummonedPortalId = weenie.Id;
                    SetPermanentResultAfter(TimeSpan.FromMilliseconds(100), ActionError.None);
                }
            }
        }

        private void TryFinishVendor() {
            if (_gotUseDone && _gotVendorInfo) {
                SetPermanentResultAfter(TimeSpan.FromMilliseconds(1), ActionError.None);
            }
        }

        private void Incoming_Vendor_VendorInfo(object sender, Common.Messages.Events.Vendor_VendorInfo_S2C_EventArgs e) {
            _gotVendorInfo = true;
            TryFinishVendor();
        }

        private void Incoming_Item_UseDone(object sender, UtilityBelt.Common.Messages.Events.Item_UseDone_S2C_EventArgs e) {
            switch (e.Data.Type) {
                case StatusMessage.None:
                    if (_ignoreUseDone)
                        return;
                    _gotUseDone = true;
                    switch (UseActionType) {
                        case UseActionType.Vendor:
                            TryFinishVendor();
                            break;
                        case UseActionType.Portal:
                        case UseActionType.SummonPortal:
                            break;
                        default:
                            SetPermanentResultAfter(TimeSpan.FromMilliseconds(1), ActionError.None);
                            break;
                    }
                    break;

                default:
                    SetTemporaryResult(ActionError.ServerError, $"Server Said: {e.Data.Type}");
                    break;
            }
        }

        public override string ToString() {
            return $"Action[ObjectUseAction:{UseActionType}] {ObjectId:X8} on {TargetId:X8}";
        }
    }
}
