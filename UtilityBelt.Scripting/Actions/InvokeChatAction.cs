﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;
using static System.Net.Mime.MediaTypeNames;

namespace UtilityBelt.Scripting.Actions {
    public class InvokeChatAction : QueueAction {
        /// <summary>
        /// The text that was sent to the chat parser
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public InvokeChatAction(string text, ActionOptions options = null) : base(options) {
            Text = text;
        }

        protected override void UpdateDefaultOptions() {
            
        }

        public override bool IsValid() {
            // make sure we are in game
            switch (Manager.GameState.State) {
                case ClientState.In_Game:
                case ClientState.Entering_Game:
                case ClientState.PlayerDesc_Received:
                    break;
                default:
                    SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            //Manager.MessageHandler.Incoming.Communication_TextboxString += Incoming_Communication_TextboxString;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().InvokeChatParser(Text);
            SetPermanentResultAfter(TimeSpan.FromMilliseconds(1), ActionError.None);
            return true;
        }

        protected override void Stop() {
            //Manager.MessageHandler.Incoming.Communication_TextboxString -= Incoming_Communication_TextboxString;
        }

        private void Incoming_Communication_TextboxString(object sender, UtilityBelt.Common.Messages.Events.Communication_TextboxString_S2C_EventArgs e) {
            if (e.Data.Text.StartsWith("Unknown command: ")) {
                SetPermanentResult(ActionError.UnknownCommand);
            }
        }
    }
}
