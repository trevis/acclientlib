﻿using ACE.DatLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class SkillAdvanceAction : QueueAction {
        /// <summary>
        /// The skill id you were attempting to spend experience in
        /// </summary>
        public SkillId Skill { get; }

        /// <summary>
        /// The amount of credits it costs to increase training
        /// </summary>
        public uint CreditsToSpend { get; private set; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public SkillAdvanceAction(SkillId skillId, ActionOptions options = null) : base(options) {
            Skill = skillId;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 3000;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            if (!Manager.GameState.Character.Weenie.Skills.ContainsKey(Skill)) {
                SetPermanentResult(ActionError.SkillDoesntExist);
                return false;
            }

            var skill = Manager.GameState.Character.Weenie.Skills[Skill];

            // make sure this skill isn't trained / specialized
            if (skill.Training >= SkillTrainingType.Trained) {
                SetPermanentResult(ActionError.AlreadyTrained);
                return false;
            }

            CreditsToSpend = skill.CostToIncreaseTraining;

            // check if we can raise this
            if (!skill.CanRaise || CreditsToSpend == 0) {
                SetPermanentResult(ActionError.CantAdvanceSkill);
                return false;
            }

            // make sure we have enough available skill credits
            if (CreditsToSpend > Manager.GameState.Character.Weenie.Value(IntId.AvailableSkillCredits)) {
                SetPermanentResult(ActionError.NotEnoughAvailableCredits);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkill += Incoming_Qualities_PrivateUpdateSkill;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().SkillAdvance(Skill, CreditsToSpend);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkill -= Incoming_Qualities_PrivateUpdateSkill;
        }

        private void Incoming_Qualities_PrivateUpdateSkill(object sender, UtilityBelt.Common.Messages.Events.Qualities_PrivateUpdateSkill_S2C_EventArgs e) {
            if (e.Data.Key == Skill) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
