﻿using ACE.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class ObjectSplitAction : QueueAction {
        /// <summary>
        /// The object id to move
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The target object id to move it to
        /// </summary>
        public uint TargetId { get; }

        /// <summary>
        /// The slot in the container to move it to (0 is the first slot)
        /// </summary>
        public uint Slot { get; }

        /// <summary>
        /// The new stack size we are splitting off to
        /// </summary>
        public uint NewStackSize { get; }

        /// <summary>
        /// The id of the newly created stack
        /// </summary>
        public uint NewStackObjectId { get; private set; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Inventory;

        public ObjectSplitAction(uint objectId, uint targetId, uint newStackSize, uint slot = 0, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
            TargetId = targetId;
            Slot = slot;
            NewStackSize = newStackSize;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // item must exist
            var weenie = Manager.GameState.WorldState.GetWeenie(ObjectId);
            if (weenie == null) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            //must be stackable
            if (weenie.Value(IntId.MaxStackSize, 1) <= 1) {
                SetPermanentResult(ActionError.CantSplitThisObject);
                return false;
            }

            // must have more current stacksize than the requested split amount
            if (NewStackSize >= weenie.Value(IntId.StackSize, 1)) {
                SetPermanentResult(ActionError.ObjectSplitAmountTooBig);
                return false;
            }

            // target must exist
            var target = Manager.GameState.WorldState.GetWeenie(TargetId);
            if (target == null) {
                SetPermanentResult(ActionError.InvalidTargetObject);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Item_CreateObject += Incoming_Item_CreateObject;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().SplitObject(ObjectId, TargetId, NewStackSize, Slot);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Item_CreateObject -= Incoming_Item_CreateObject;
        }

        private void Incoming_Item_CreateObject(object sender, Common.Messages.Events.Item_CreateObject_S2C_EventArgs e) {
            var existingWo = Manager.GameState.WorldState.GetWeenie(ObjectId);
            var newWo = Manager.GameState.WorldState.GetWeenie(e.Data.ObjectId);

            if (existingWo != null && newWo != null && Manager.GameState.Character.Weenie.AllItemIds.Contains(e.Data.ObjectId) && newWo.Value(IntId.StackSize) == NewStackSize) {
                NewStackObjectId = newWo.Id;
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
