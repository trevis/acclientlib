﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class BlankAction : QueueAction {
        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Misc;

        public BlankAction(ActionOptions options = null) : base(options) {
            
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            
        }

        protected override bool Execute() {
            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {
            
        }
    }
}
