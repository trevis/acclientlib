﻿using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class SalvageAction : QueueAction {
        private List<uint> _awaitingItemDestroy = new List<uint>();

        /// <summary>
        /// A list of object ids being salvaged
        /// </summary>
        public List<uint> ObjectIds { get; } = new List<uint>();

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public SalvageAction(ActionOptions options = null) : base(options) {
            
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            // must have ust
            if (!Manager.GameState.Character.Weenie.AllItems.Any(w => w.ClassId == 26046)) {
                SetPermanentResult(ActionError.NoUst);
                return false;
            }
            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Outgoing.Inventory_CreateTinkeringTool += Outgoing_Inventory_CreateTinkeringTool;
            Manager.MessageHandler.Incoming.Item_ServerSaysRemove += Incoming_Item_ServerSaysRemove;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().SalvagePanelSalvage();
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Outgoing.Inventory_CreateTinkeringTool -= Outgoing_Inventory_CreateTinkeringTool;
            Manager.MessageHandler.Incoming.Item_ServerSaysRemove -= Incoming_Item_ServerSaysRemove;
        }

        private void Outgoing_Inventory_CreateTinkeringTool(object sender, UtilityBelt.Common.Messages.Events.Inventory_CreateTinkeringTool_C2S_EventArgs e) {
            var usts = Manager.GameState.Character.Weenie.AllItems.Where(w => w.ClassId == 20646);
            if (usts.Any(w => w.Id == e.Data.ObjectId)) {
                ObjectIds.AddRange(e.Data.ItemIds.Items);
                _awaitingItemDestroy.AddRange(e.Data.ItemIds.Items);
            }
        }

        private void Incoming_Item_ServerSaysRemove(object sender, UtilityBelt.Common.Messages.Events.Item_ServerSaysRemove_S2C_EventArgs e) {
            _awaitingItemDestroy.Remove(e.Data.ObjectId);

            if (_awaitingItemDestroy.Count == 0) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
