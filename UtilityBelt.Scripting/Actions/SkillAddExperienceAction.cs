﻿using ACE.DatLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class SkillAddExperienceAction : QueueAction {
        /// <summary>
        /// The skill id you were attempting to spend experience in
        /// </summary>
        public SkillId Skill { get; }

        /// <summary>
        /// The amount of experience you were attempting to spend
        /// </summary>
        public uint ExperienceToSpend { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public SkillAddExperienceAction(SkillId skillId, uint experienceToSpend, ActionOptions options = null) : base(options) {
            Skill = skillId;
            ExperienceToSpend = experienceToSpend;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            var skill = Manager.GameState.Character.Weenie.Skills[Skill];
            var _datReader = Manager.Resolve<IAsyncDatReader>();

            // make sure this skill is at least trained
            if (skill.Training < SkillTrainingType.Trained) {
                SetPermanentResult(ActionError.NotTrained);
                return false;
            }

            // make sure we have enough unassigned experience
            if (ExperienceToSpend > Manager.GameState.Character.Weenie.Value(Int64Id.AvailableExperience)) {
                SetPermanentResult(ActionError.NotEnoughUnassignedExperience);
                return false;
            }

            // make sure this skill is not maxed out
            // Trained skills can be raised 208 points.
            // Specialized skills can be raised 226 points.
            // https://asheron.fandom.com/wiki/Skills
            // todo: unhardcode this?
            var maxIncrement = skill.Training == SkillTrainingType.Trained ? 208 : 226;
            if (skill.PointsRaised >= maxIncrement) {
                SetPermanentResult(ActionError.AlreadyMaxed);
                return false;
            }

            // make sure we aren't trying to spend more exp than the skill can take
            var skillXPTable = skill.Training == SkillTrainingType.Trained ? _datReader.XpTable.TrainedSkillXpList : _datReader.XpTable.SpecializedSkillXpList;
            if (ExperienceToSpend > skillXPTable[skillXPTable.Count - 1] - skill.Experience) {
                SetPermanentResult(ActionError.TooMuchSpendExperience);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkill += Incoming_Qualities_PrivateUpdateSkill;
            Manager.MessageHandler.Incoming.Communication_TextboxString += Incoming_Communication_TextboxString;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().SkillAddExperience(Skill, ExperienceToSpend);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkill -= Incoming_Qualities_PrivateUpdateSkill;
            Manager.MessageHandler.Incoming.Communication_TextboxString -= Incoming_Communication_TextboxString;
        }

        private void Incoming_Communication_TextboxString(object sender, Communication_TextboxString_S2C_EventArgs e) {
            if (e.Data.Type == ChatMessageType.Default && e.Data.Text.StartsWith($"Your attempt to raise ")) {
                SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Text}");
            }
        }

        private void Incoming_Qualities_PrivateUpdateSkill(object sender, Qualities_PrivateUpdateSkill_S2C_EventArgs e) {
            if (e.Data.Key == Skill) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
