﻿using ACE.DatLoader;
using ACE.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class VitalAddExperienceAction : QueueAction {
        /// <summary>
        /// The id of the vital you attempting to spend experience into
        /// </summary>
        public VitalId Vital { get; }

        /// <summary>
        /// The amount of experience you attempted to spend in this vital
        /// </summary>
        public uint ExperienceToSpend { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Immediate;

        public VitalAddExperienceAction(VitalId vitalId, uint experienceToSpend, ActionOptions options = null) : base(options) {
            Vital = vitalId;
            ExperienceToSpend = experienceToSpend;
        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            var vital = Manager.GameState.Character.Weenie.Vitals[Vital];

            // make sure we have enough unassigned experience
            if (ExperienceToSpend > Manager.GameState.Character.Weenie.Value(Int64Id.AvailableExperience)) {
                SetPermanentResult(ActionError.NotEnoughUnassignedExperience);
                return false;
            }

            // make sure this vital is not maxed out
            // todo: can/should we unhardcode this max?
            if (vital.PointsRaised >= 196) {
                SetPermanentResult(ActionError.AlreadyMaxed);
                return false;
            }

            // make sure we aren't trying to spend more exp than the vital can take
            var vitalXPTable = Manager.Resolve<IAsyncDatReader>().XpTable.VitalXpList;
            var experienceLeft = vitalXPTable[vitalXPTable.Count - 1] - vital.Experience;
            if (ExperienceToSpend > experienceLeft) {
                SetPermanentResult(ActionError.TooMuchSpendExperience);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute2nd += Incoming_Qualities_PrivateUpdateAttribute2nd;
            Manager.MessageHandler.Incoming.Communication_TextboxString += Incoming_Communication_TextboxString;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().VitalAddExperience(Vital, ExperienceToSpend);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute2nd -= Incoming_Qualities_PrivateUpdateAttribute2nd;
            Manager.MessageHandler.Incoming.Communication_TextboxString -= Incoming_Communication_TextboxString;
        }

        private void Incoming_Communication_TextboxString(object sender, Communication_TextboxString_S2C_EventArgs e) {
            if (e.Data.Type == ChatMessageType.Default && e.Data.Text.Equals($"Your attempt to raise {Regex.Replace(Vital.ToString(), "(\\B[A-Z])", " $1")} has failed.")) {
                SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Text}");
            }
        }

        private void Incoming_Qualities_PrivateUpdateAttribute2nd(object sender, Qualities_PrivateUpdateAttribute2nd_S2C_EventArgs e) {
            if (e.Data.Key == Vital) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
