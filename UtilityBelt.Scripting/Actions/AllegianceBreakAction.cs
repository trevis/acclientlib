﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class AllegianceBreakAction : QueueAction {
        /// <summary>
        /// The object id of your patron
        /// </summary>
        public uint ObjectId { get; private set; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Allegiance;

        public AllegianceBreakAction(ActionOptions options = null) : base(options) {

        }

        protected override void UpdateDefaultOptions() {

        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            if (!Manager.GameState.Character.Allegiance.Exists) {
                SetPermanentResult(ActionError.NotInAnAllegiance);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Allegiance_AllegianceUpdate += Incoming_Allegiance_AllegianceUpdate;
        }

        private void Incoming_Allegiance_AllegianceUpdate(object sender, UtilityBelt.Common.Messages.Events.Allegiance_AllegianceUpdate_S2C_EventArgs e) {
            var hasPatronRecord = e.Data.Profile?.Hierarchy?.Records?.Any(r => r.AllegianceData.ObjectId == Manager.GameState.CharacterId && r.TreeParent == ObjectId);
            if (!hasPatronRecord.HasValue || hasPatronRecord == false) {
                SetPermanentResult(ActionError.None);
            }
        }

        protected override bool Execute() {
            var id = Manager.GameState.Character.Allegiance.Patron?.Id;

            if (!id.HasValue) {
                SetPermanentResult(ActionError.NotInAnAllegiance);
                return false;
            }

            ObjectId = id.Value;

            Manager.Resolve<IClientActionsRaw>().AllegianceBreak(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Allegiance_AllegianceUpdate -= Incoming_Allegiance_AllegianceUpdate;
        }
    }
}
