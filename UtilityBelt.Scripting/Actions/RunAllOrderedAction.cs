﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Actions {
    public class RunAllOrderedAction : QueueAction {
        private bool _addedActionsToPreconditions = false;

        /// <summary>
        /// A list of ordered actions to be performed
        /// </summary>
        public List<QueueAction> Actions { get; } = new List<QueueAction>();

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Misc;
        
        public RunAllOrderedAction(IEnumerable<QueueAction> actions, ActionOptions options = null) : base(options) {
            Actions.AddRange(actions);
        }

        protected override void UpdateDefaultOptions() {
            
        }

        public override bool IsValid() {
            if (Actions.Count == 0) {
                SetPermanentResult(ActionError.NoActions);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            if (!_addedActionsToPreconditions) {
                foreach (var action in Actions) {
                    Preconditions.Enqueue(action);
                }
                _addedActionsToPreconditions = true;
            }
        }

        protected override void Start() {
            
        }

        protected override bool Execute() {
            SetPermanentResult(ActionError.None);
            return true;
        }

        protected override void Stop() {
            
        }
    }
}
