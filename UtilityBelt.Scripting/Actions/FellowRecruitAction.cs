﻿using ACE.Entity.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class FellowRecruitAction : QueueAction {
        /// <summary>
        /// The id of the player you are trying to recruit
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.Fellow;

        public FellowRecruitAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 30000;
        }

        public override bool IsValid() {
            // make sure we are in game
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            var fellowship = Manager.GameState.Character.Fellowship;

            if (!Manager.GameState.WorldState.TryGetWeenie(ObjectId, out Interop.WorldObject foundWeenie)) {
                SetPermanentResult(ActionError.InvalidSourceObject);
                return false;
            }

            // todo: check if object is within range.

            // check if we are in a fellowship
            if (!fellowship.Exists) {
                SetPermanentResult(ActionError.NotInFellow);
                return false;
            }

            // check if fellowship is full
            if (fellowship.Members.Count >= Interop.Fellowship.MAX_FELLOW_COUNT) {
                SetPermanentResult(ActionError.FellowshipIsFull);
                return false;
            }

            // check if the fellow is open (or we are the leader)
            if (!(fellowship.IsOpen || fellowship.LeaderId == Manager.GameState.CharacterId)) {
                SetPermanentResult(ActionError.FellowshipClosed);
                return false;
            }

            // check that they aren't already a member of the fellowship
            if (fellowship.Members.TryGetValue(ObjectId, out FellowshipMember value)) {
                SetPermanentResult(ActionError.AlreadyRecruited);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {

        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Fellowship_UpdateFellow += Incoming_Fellowship_UpdateFellow;
            Manager.MessageHandler.Incoming.Communication_TextboxString += Incoming_Communication_TextboxString;
            Manager.MessageHandler.Incoming.Communication_WeenieErrorWithString += Incoming_Communication_WeenieErrorWithString;
        }

        protected override bool Execute() {
            Manager.Resolve<IClientActionsRaw>().FellowshipRecruit(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Fellowship_UpdateFellow -= Incoming_Fellowship_UpdateFellow;
            Manager.MessageHandler.Incoming.Communication_TextboxString -= Incoming_Communication_TextboxString;
            Manager.MessageHandler.Incoming.Communication_WeenieErrorWithString -= Incoming_Communication_WeenieErrorWithString;
        }

        private void Incoming_Communication_WeenieErrorWithString(object sender, UtilityBelt.Common.Messages.Events.Communication_WeenieErrorWithString_S2C_EventArgs e) {
            switch (e.Data.Type) {
                case StatusMessage.FellowshipRecruitBusy:
                    SetTemporaryResult(ActionError.FellowshipRecruitBusy, $"Server said: {e.Data.Text}");
                    return;
                case StatusMessage.FellowshipIllegalLevel:
                case StatusMessage.YourFellowshipIsFull:
                    SetPermanentResult(ActionError.FellowshipIsFull, $"Server said: {e.Data.Text}");
                    return;
                case StatusMessage.YouDoNotBelongToAFellowship:
                    SetPermanentResult(ActionError.NotInFellow, $"Server said: {e.Data.Text}");
                    break;
                case StatusMessage.FellowshipDeclined:
                case StatusMessage.FellowshipIgnoringRequests:
                case StatusMessage.FellowshipIsLocked:
                case StatusMessage.FellowshipMaxDistanceExceeded:
                case StatusMessage.OlthoiCannotJoinFellowship:
                case StatusMessage.LockedFellowshipCannotRecruit:
                    SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Text}");
                    break;
            }
        }

        private void Incoming_Communication_TextboxString(object sender, UtilityBelt.Common.Messages.Events.Communication_TextboxString_S2C_EventArgs e) {
            var member = Manager.GameState.WorldState.GetWeenie(ObjectId);
            if (member != null && e.Data.Type == ChatMessageType.Default && (
                e.Data.Text.EndsWith($"{member.Name} is already a member of a Fellowship.") ||
                e.Data.Text.EndsWith($"{member.Name} is busy.") ||
                e.Data.Text.EndsWith($"{member.Name} is not accepting fellowship requests.")
            )) {
                if (e.Data.Text.EndsWith($"{member.Name} is busy.")) {
                    SetTemporaryResult(ActionError.FellowshipRecruitBusy, $"Server said: {e.Data.Text}");
                }
                else {
                    SetPermanentResult(ActionError.ServerError, $"Server said: {e.Data.Text}");
                }
            }
        }

        private void Incoming_Fellowship_UpdateFellow(object sender, UtilityBelt.Common.Messages.Events.Fellowship_UpdateFellow_S2C_EventArgs e) {
            if (e.Data.ObjectId == ObjectId) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
