﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib;

namespace UtilityBelt.Scripting.Actions {
    public class LoginAction : QueueAction {
        /// <summary>
        /// Id of the character being logged in
        /// </summary>
        public uint ObjectId { get; }

        /// <summary>
        /// The type of action this is. This determines its priority if
        /// Options.Priority is not set.
        /// </summary>
        public override ActionType ActionType => ActionType.LoginLogoff;

        public LoginAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 1000;
            if (!Options.MaxRetryCount.HasValue)
                Options.TimeoutMilliseconds = 5;
        }

        protected override void Start() {
            Manager.GameState.OnStateChanged += OnStateChanged;
        }

        public override bool IsValid() {
            // make sure character id exists
            if (!Manager.GameState.Characters.Any(c => c.Id == ObjectId)) {
                SetPermanentResult(ActionError.CharacterDoesntExist);
                return false;
            }

            // make sure character isn't pending deletion
            if (Manager.GameState.Characters.Any(c => c.Id == ObjectId && c.SecondsGreyedOut != 0)) {
                SetPermanentResult(ActionError.CharacterPendingDelete);
                return false;
            }

            return true;
        }

        protected override void UpdatePreconditions() {
            // If we are logged in already, log out
            if (Manager.GameState.State == ClientState.In_Game) {
                Preconditions.Enqueue(new LogoutAction());
                return;
            }
        }

        protected override bool Execute() {
            // make sure we are at character select
            if (Manager.GameState.State != ClientState.Character_Select_Screen) {
                SetPermanentResult(ActionError.NotAtCharacterSelect);
                return false;
            }

            Manager.Resolve<IClientActionsRaw>().Login(ObjectId);
            return true;
        }

        protected override void Stop() {
            Manager.GameState.OnStateChanged -= OnStateChanged;
        }

        private void OnStateChanged(object sender, StateChangedEventArgs e) {
            if (e.NewState == ClientState.PlayerDesc_Received && Manager.GameState.CharacterId == ObjectId) {
                SetPermanentResult(ActionError.None);
            }
        }
    }
}
