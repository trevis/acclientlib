﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace UtilityBelt.Scripting.Lib.ScriptTypes {
    public class ScriptTypeDocumentation {
        private static Regex trimRegex = new Regex("(^\\s+|\\s+$)", RegexOptions.Multiline | RegexOptions.Compiled);

        public string Summary { get; internal set; } = "";
        public Dictionary<string, string> Params { get; } = new Dictionary<string, string>();
        public string Returns { get; internal set; } = "";

        public ScriptTypeDocumentation() {

        }

        internal static ScriptTypeDocumentation FromMember(XElement e) {
            var docs = new ScriptTypeDocumentation();

            string summary;
            if (e.Elements("inheritdoc").Count() > 0) {
                summary = $"inheritdoc|{e.Element("inheritdoc").Attribute("cref")?.Value}";
            }
            else {
                summary = e.Element("summary")?.Value ?? "";
            }
            docs.Summary = trimRegex.Replace(summary, string.Empty);

            foreach (var pEl in e.Elements("param")) {
                docs.Params.Add(pEl.Attribute("name").Value, trimRegex.Replace(pEl.Value ?? "", string.Empty));
            }

            docs.Returns = trimRegex.Replace(e.Element("returns")?.Value ?? "", string.Empty);

            return docs;
        }
    }
}
