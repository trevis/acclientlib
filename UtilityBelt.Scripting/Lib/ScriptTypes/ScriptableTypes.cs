﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UtilityBelt.Scripting.Interop;
using Exception = System.Exception;

namespace UtilityBelt.Scripting.Lib.ScriptTypes {
    public static class ScriptableTypes {
        private static bool _didLoadScriptTypes = false;
        private static Dictionary<string, ScriptTypeModule> _modules = new Dictionary<string, ScriptTypeModule>();
        private static Dictionary<string, Assembly> _checkedScriptAssemblies = new Dictionary<string, Assembly>();
        private static Dictionary<string, List<ScriptTypeModule>> _pluginScriptModules = new Dictionary<string, List<ScriptTypeModule>>();

        public static ScriptTypeModule GetModule(string name) {
            LoadAllAssemblyScriptTypes();

            if (_modules.TryGetValue(name, out ScriptTypeModule value)) {
                return value;
            }

            return null;
        }

        public static Dictionary<string, ScriptTypeModule> GetAllModules() {
            LoadAllAssemblyScriptTypes();

            return _modules;
        }


        public static void ReloadAssemblyScriptTypes(Assembly assembly, string scriptTypesFile = null) {
            ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"Reloading assembly ScriptTypes: {assembly.GetName().Name} from {scriptTypesFile}");

            var name = assembly.GetName().Name;

            UnloadPluginAssemblyScriptTypes(name);

            _checkedScriptAssemblies.Add(name, assembly);
            var modules = TryLoadAssemblyScriptTypes(assembly, scriptTypesFile);
            if (modules.Count > 0) {
                _pluginScriptModules.Add(name, modules);
            }
        }

        private static void UnloadPluginAssemblyScriptTypes(string name) {
            _checkedScriptAssemblies.Remove(name);
            if (_pluginScriptModules.TryGetValue(name, out List<ScriptTypeModule> existingModules)) {
                _pluginScriptModules.Remove(name);
                foreach (var module in existingModules) {
                    _modules.Remove(module.Name);
                }

            }
            ScriptContext._moduleRetValues.Remove(name);
        }

        internal static void LoadPluginScriptTypes() {
            ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"Loading plugin ScriptTypes");
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            loadedAssemblies.Reverse();

            foreach (var assembly in loadedAssemblies) {
                var name = assembly.GetName().Name;
                if (!_checkedScriptAssemblies.ContainsKey(name)) {
                    ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"Found plugin assembly: {assembly.GetName().Name} (Dynamic: {assembly.IsDynamic})");
                    var module = TryLoadAssemblyScriptTypes(assembly);
                    _checkedScriptAssemblies.Add(name, assembly);
                    if (module != null) {
                        _pluginScriptModules.Add(name, module);
                    }
                }
            }
        }

        internal static void UnloadPluginScriptTypes() {
            ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"Unloading plugin ScriptTypes");
            foreach (var kv in _pluginScriptModules.Keys.ToList()) {
                UnloadPluginAssemblyScriptTypes(kv);
            }
            _pluginScriptModules.Clear();
        }

        private static void LoadAllAssemblyScriptTypes() {
            if (_didLoadScriptTypes) {
                return;
            }
            ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"Loading all assembly ScriptTypes");

            var baseModule = new ScriptTypeModule("base") {
                _scriptTypes = new Dictionary<Type, ScriptType>(),
                _types = new List<Type>() {
                    typeof(EventArgs),
                    typeof(Exception),
                    typeof(DateTime),
                    typeof(EventHandler),
                    typeof(System.Numerics.Vector2),
                    typeof(System.Numerics.Vector3),
                    typeof(System.Numerics.Vector4),
                },
                _extensions = new List<Type>(),
                _globals = new Dictionary<string, Type>() {
                    { "Vector2", typeof(System.Numerics.Vector2) },
                    { "Vector3", typeof(System.Numerics.Vector3) },
                    { "Vector4", typeof(System.Numerics.Vector4) },
                },

                _instances = new Dictionary<string, Type>(),
                Assembly = typeof(ScriptableTypes).Assembly
            };

            _modules.Add("base", baseModule);

            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            loadedAssemblies.Reverse();

            foreach (var assembly in loadedAssemblies) {
                if (_checkedScriptAssemblies.ContainsKey(assembly.GetName().Name)) {
                    ScriptManager.Instance?.Resolve<ILogger>()?.LogWarning($"Tried to load scriptable types for assembly twice! {assembly.FullName}");
                }
                else {
                    _checkedScriptAssemblies.Add(assembly.GetName().Name, assembly);
                    TryLoadAssemblyScriptTypes(assembly);
                }
            }

            _didLoadScriptTypes = true;
        }

        private static List<ScriptTypeModule> TryLoadAssemblyScriptTypes(Assembly assembly, string? scriptTypesFile = null) {
            var modules = new List<ScriptTypeModule>();
            try {
                if (string.IsNullOrEmpty(scriptTypesFile) && assembly.IsDynamic) {
                    return null;
                }
                else if (!string.IsNullOrEmpty(assembly.Location)) {
                    scriptTypesFile = assembly.Location.Replace(Path.GetExtension(assembly.Location), ".ScriptTypes.json");
                }

                if (string.IsNullOrEmpty(scriptTypesFile) || !File.Exists(scriptTypesFile)) {
                    return null;
                }

                var scriptTypes = JObject.Parse(File.ReadAllText(scriptTypesFile));

                modules.AddRange(LoadScriptTypes(assembly, scriptTypes));
            }
            catch (Exception ex) {
                ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString());
            }

            return modules;
        }

        public static List<ScriptTypeModule> LoadScriptTypes(Assembly assembly, JObject scriptTypes) {
            var modules = new List<ScriptTypeModule>();
            try {
                JArray packages = scriptTypes.SelectToken("packages") as JArray;
                if (packages == null) {
                    ScriptManager.Instance?.Resolve<ILogger>()?.LogError($"No packages root key in {assembly.FullName} script types");
                    return null;
                }
                ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"Loading script types from Assembly: {assembly.FullName}");

                foreach (JObject package in packages) {
                    var moduleName = package.Value<string>("module") ?? "base";
                    ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($" - Found package: {moduleName} ({packages.Count})");
                    if (!_modules.TryGetValue(moduleName, out ScriptTypeModule module)) {
                        ScriptManager.Instance?.Resolve<ILogger>()?.LogTrace($"   - Created module {moduleName}");
                        module = new ScriptTypeModule(moduleName) {
                            Assembly = assembly
                        };
                        if (moduleName != "base") {
                            _modules.Add(moduleName, module);
                        }
                    }

                    module.LoadPackageData(package);
                    modules.Add(module);
                }
            }
            catch (Exception ex) {
                ScriptManager.Instance?.Resolve<ILogger>()?.LogError(ex.ToString());
            }

            return modules;
        }
    }
}
