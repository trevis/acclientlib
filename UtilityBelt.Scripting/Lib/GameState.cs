﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Common.Lib;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Common.Messages;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Interop;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Lib {
    public class GameState : IDisposable {
        private WeakReference _scriptManagerRef;
        private ScriptManager _scriptManager => (ScriptManager)_scriptManagerRef.Target;

        protected MessageHandler _messageHandler { get; }

        private ILogger _log;

        private ClientState _clientState = ClientState.Initial;

        /// <summary>
        /// Fired when client state changes. See [ClientState](xref:UBScript.Enums.ClientState) for a list of valid states.
        /// </summary>
        public virtual event EventHandler<StateChangedEventArgs> OnStateChanged;

        /// <summary>
        /// Fired when the gamestate ticks. This is roughly once per second
        /// </summary>
        public virtual event EventHandler<EventArgs> OnTick;

        /// <summary>
        /// Fired every frame, during 2D rendering
        /// </summary>
        public virtual event EventHandler<EventArgs> OnRender2D;

        /// <summary>
        /// Fired every frame, during 3D rendering
        /// </summary>
        public virtual event EventHandler<EventArgs> OnRender3D;

        /// <summary>
        /// When the last tick happened
        /// </summary>
        public DateTime LastTick { get; private set; } = DateTime.UtcNow;

        /// <summary>
        /// A list of [CharacterIdentity](xref:UtilityBelt.Common.DataTypes.CharacterIdentity)'s available on the character select screen.
        /// 
        /// **Code Examples:**
        /// # [lua](#tab/lua)
        /// 
        /// <remarks><code language="lua"><![CDATA[
        /// -- loop through available characters on this account, printing their name and id
        /// for i, charIdentity in ipairs(game.CharacterSet) do
        ///   print("Found available character: ", charIdentity.name, charIdentity.id);
        /// end
        /// ]]></code></remarks>
        /// 
        /// # [csharp](#tab/csharp)
        /// 
        /// <remarks><code language="csharp"><![CDATA[
        /// // loop through available characters on this account, printing their name and id
        /// foreach (var charIdentity in GameState.CharacterSet) {
        ///    Console.WriteLine("Found available character: {charIdentity.name} {charIdentity.id}");
        /// }
        /// ]]></code></remarks>
        /// 
        /// ***
        /// 
        /// </summary>
        public virtual IList<CharacterIdentity> Characters { get; set; } = new List<CharacterIdentity>();

        /// <summary>
        /// The name of this server
        /// </summary>
        public virtual string ServerName { get; private set; } = "";

        /// <summary>
        /// The weenie id of the currently logged in character. 0 if not logged in.
        /// </summary>
        public virtual uint CharacterId { get; private set; } = 0;

        /// <summary>
        /// Information about the currently logged in character
        /// </summary>
        public virtual CharacterState Character { get; private set; } = null;

        /// <summary>
        /// Information about the game world
        /// </summary>
        public virtual WorldState WorldState { get; private set; } = null;

        /// <summary>
        /// The name of the account this client is connected as
        /// </summary>
        public virtual string AccountName { get; private set; } = "";

        /// <summary>
        /// Maximum number of allowed characters on this server
        /// </summary>
        public virtual uint MaxAllowedCharacters { get; private set; } = 0;

        /// <summary>
        /// The current state of the client. Use this to check if you are logged in
        /// </summary>
        public virtual ClientState State {
            get => _clientState;
            set {
                if (value != _clientState) {
                    _clientState = value;
                    OnStateChanged?.Invoke(this, new StateChangedEventArgs(State));
                }
            }
        }

        /// <summary>
        /// Client actions
        /// </summary>
        public virtual ClientActions Actions { get; } = null;

        /// <summary>
        /// Action Queue
        /// </summary>
        public ActionQueue ActionQueue { get; protected set; }

        public GameState(MessageHandler messageHandler, ScriptManager scriptManager, ILogger logger) {
            _scriptManagerRef = new WeakReference(scriptManager);
            _messageHandler = messageHandler;
            _log = logger;

            ActionQueue = new ActionQueue(scriptManager, this);
            Actions = _scriptManager.Resolve<ClientActions>();

            _messageHandler.Incoming.Login_LoginCharacterSet += MessageHandler_Login_LoginCharacterSet_S2C;
            _messageHandler.Incoming.Login_WorldInfo += MessageHandler_Login_WorldInfo_S2C;
            _messageHandler.Incoming.Login_PlayerDescription += MessageHandler_Login_PlayerDescription_S2C;
            _messageHandler.Outgoing.Login_SendEnterWorld += MessageHandler_Login_SendEnterWorld_C2S;
            _messageHandler.Incoming.Login_CreatePlayer += _messageHandler_Login_CreatePlayer_S2C;
            _messageHandler.Incoming.Login_LogOffCharacter += MessageHandler_Login_LogOffCharacter_S2C;
            _messageHandler.Outgoing.Character_LoginCompleteNotification += MessageHandler_Character_LoginCompleteNotification_C2S;

            State = ClientState.Game_Started;
        }

        #region public api
        //public Hud CreateHud(string name) {
        //    return null;
        //return UBService.UBService.Huds.CreateHud(name);
        //}
        #endregion // public api

        /// <summary>
        /// Should be called once every second or so. (Not by scripts, the script manager does this)
        /// </summary>
        internal void Tick() {
            ActionQueue.Run();
            if (DateTime.UtcNow - LastTick > TimeSpan.FromSeconds(1)) {
                LastTick = DateTime.UtcNow;
                WorldState?.Tick();
                Character?.Tick();
                OnTick?.Invoke(this, EventArgs.Empty);
            }
        }

        internal void Render2D() {
            OnRender2D?.Invoke(this, EventArgs.Empty);
        }

        internal void Render3D() {
            OnRender3D?.Invoke(this, EventArgs.Empty);
        }

        #region Message Handlers
        protected virtual void MessageHandler_Character_LoginCompleteNotification_C2S(object sender, Character_LoginCompleteNotification_C2S_EventArgs e) {
            // this gets fired every time you exit portal space, not just after logging in
            if (State != ClientState.In_Game)
                State = ClientState.In_Game;
        }

        protected virtual void MessageHandler_Login_LogOffCharacter_S2C(object sender, Login_LogOffCharacter_S2C_EventArgs e) {
            State = ClientState.Logging_Out;
            Character?.Dispose();
            WorldState?.Dispose();
            CharacterId = 0;
            Character = null;
            WorldState = null;
        }

        protected virtual void MessageHandler_Login_SendEnterWorld_C2S(object sender, Login_SendEnterWorld_C2S_EventArgs e) {
            try {
                Character?.Dispose();
                WorldState?.Dispose();

                CharacterId = e.Data.ObjectId;
                WorldState = new WorldState();
                Character = new CharacterState(CharacterId);

                State = ClientState.Entering_Game;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        protected void _messageHandler_Login_CreatePlayer_S2C(object sender, Login_CreatePlayer_S2C_EventArgs e) {

        }

        protected virtual void MessageHandler_Login_PlayerDescription_S2C(object sender, Login_PlayerDescription_S2C_EventArgs e) {
            State = ClientState.PlayerDesc_Received;
        }

        protected virtual void MessageHandler_Login_WorldInfo_S2C(object sender, Login_WorldInfo_S2C_EventArgs e) {
            ServerName = e.Data.WorldName;
        }

        protected virtual void MessageHandler_Login_LoginCharacterSet_S2C(object sender, Login_LoginCharacterSet_S2C_EventArgs e) {
            AccountName = e.Data.Account;
            MaxAllowedCharacters = e.Data.NumAllowedCharacters;
            var characters = e.Data.Characters.ToList();
            characters.Sort((a, b) => a.Name.CompareTo(b.Name));
            Characters = characters;

            // we have to delay here, there is a client bug where if you login too quickly the client stays
            // at the char select screen, but still logs in a character...

            var start = DateTime.UtcNow;
            EventHandler<EventArgs> gameState_OnTick = null;
            gameState_OnTick = (s, e) => {
                if (DateTime.UtcNow - start > TimeSpan.FromMilliseconds(1000)) {
                    OnTick -= gameState_OnTick;
                    State = ClientState.Character_Select_Screen;
                }
            };

            OnTick += gameState_OnTick;

        }
        #endregion // Message Handlers

        /// <summary>
        /// Cleans up. Only the script manager should call this, not individual scripts.
        /// </summary>
        public virtual void Dispose() {
            _messageHandler.Incoming.Login_LoginCharacterSet -= MessageHandler_Login_LoginCharacterSet_S2C;
            _messageHandler.Incoming.Login_WorldInfo -= MessageHandler_Login_WorldInfo_S2C;
            _messageHandler.Incoming.Login_PlayerDescription -= MessageHandler_Login_PlayerDescription_S2C;
            _messageHandler.Incoming.Login_CreatePlayer -= _messageHandler_Login_CreatePlayer_S2C;
            _messageHandler.Outgoing.Login_SendEnterWorld -= MessageHandler_Login_SendEnterWorld_C2S;
            _messageHandler.Incoming.Login_LogOffCharacter -= MessageHandler_Login_LogOffCharacter_S2C;
            _messageHandler.Outgoing.Character_LoginCompleteNotification -= MessageHandler_Character_LoginCompleteNotification_C2S;

            ActionQueue?.Dispose();
            Character?.Dispose();
            WorldState?.Dispose();

            CharacterId = 0;
            Character = null;
            WorldState = null;
            ActionQueue = null;
        }
    }
}
