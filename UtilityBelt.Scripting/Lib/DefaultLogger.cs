﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Scripting.Lib {
    /// <summary>
    /// Default logger implementation, just writes to console.
    /// </summary>
    public class DefaultLogger : ILogger {
        public IDisposable BeginScope<TState>(TState state) where TState : notnull {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel) {
            return true;
        }

        /// <summary>
        /// Log a message to the console with a given log level
        /// </summary>
        /// <param name="message">message to log</param>
        /// <param name="logLevel">log level</param>
        public void Log(string message, LogLevel logLevel = LogLevel.Information) {
            Console.WriteLine($"{logLevel}: {message}");
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="ex">Exception to log</param>
        public void Log(Exception ex) {
            Console.WriteLine($"Exception: {ex}");
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {
            Log(formatter(state, exception), logLevel);
        }
    }
}
