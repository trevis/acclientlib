﻿using System;
using System.Collections.Generic;
using System.Linq;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Common;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Types;
using System.ComponentModel;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Common.Messages;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Interop;
using System.Text;
using ACE.Entity.Models;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Lib {
    /// <summary>
    /// Information about the world around your character
    /// </summary>
    public class WorldState : IDisposable {
        public delegate Task AsyncEventHandler<TEventArgs>(object sender, TEventArgs e);

        private const int FORGET_OBJECT_TIME_SECONDS = 25;
        private ILogger _log;
        private ScriptManager _manager;

        private MessageHandler _messageHandler => _manager.MessageHandler;
        private GameState _gameState => _manager.GameState;

        private List<uint> _createdWeenies = new List<uint>();
        private Dictionary<uint, WorldObject> _weenies = new Dictionary<uint, WorldObject>();
        internal WorldObject CharacterWeenie { get; }

        internal Dictionary<ChatChannel, uint> ChatRoomIdsLookup = new Dictionary<ChatChannel, uint>();
        internal Dictionary<uint, ChatChannel> ChatRoomTypesLookup = new Dictionary<uint, ChatChannel>();

        /// <summary>
        /// This is used for client message event eat support. World.OnChatText must be run before the client OnShowChatText
        /// so that the client can use this value to decide if the message should be eaten.
        /// </summary>
        public bool EatNextChatMessage { get; set; } = false;
        /// <summary>
        /// Fired when something is typed into the chat input box and sent. This will trigger when a player manually
        /// types something into the box and hits enter, as well as when a script uses the InvokeChat action.
        /// </summary>
        public event EventHandler<ChatInputEventArgs> OnChatInput;

        /// <summary>
        /// Fired when an object is selected in the client.
        /// </summary>
        public event AsyncEventHandler<ObjectSelectedEventArgs> OnObjectSelected;

        /// <summary>
        /// Fired when a green / clickable name is clicked in chat.
        /// </summary>
        public event EventHandler<ChatNameClickedEventArgs> OnChatNameClicked;

        /// <summary>
        /// Fired when a new "channel" message is received. Channel messages are ones in
        /// general / trade / lfg / roleplay / allegiance / society
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnChannelMessage;

        /// <summary>
        /// This fires for all "chat" messages from the server. like server motd, tells, all
        /// speech / emotes, etc. Check room / type to see what it is.
        /// to see what it is.
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnChatText;

        /// <summary>
        /// Fired when someone directly messages you
        /// </summary>
        public event EventHandler<ChatEventArgs> OnTell;

        /// <summary>
        /// Fired when someone says something locally, like with /say.
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnLocalMessage;

        /// <summary>
        /// Fired when someone sends a custom emote
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnEmote;

        /// <summary>
        /// Fired when someone uses a builtin emote like *atoyot*
        /// These can be fired for your own messages as well.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnSoulEmote;

        /// <summary>
        /// Fired when something is said in the fellowship chat. Note that
        /// SenderId is not available.  If the name is blank, you are the
        /// sender.
        /// </summary>
        public event EventHandler<ChatEventArgs> OnFellowMessage;

        /// <summary>
        /// Fired when the client learns about a new object.
        /// </summary>
        public event EventHandler<ObjectCreatedEventArgs> OnWeenieCreated;

        /// <summary>
        /// Fired when the client forgets about an object.
        /// </summary>
        public event EventHandler<ObjectReleasedEventArgs> OnWeenieReleased;

        /// <summary>
        /// Fired when your character opens a landscape container.
        /// </summary>
        public event EventHandler<ContainerOpenedEventArgs> OnContainerOpened;

        /// <summary>
        /// Fired when your character closes a landscape container.
        /// </summary>
        public event EventHandler<ContainerClosedEventArgs> OnContainerClosed;

        /// <summary>
        /// Fired when a confirmation popup dialog is shown. Use evtArgs.ClickYes=true to click yes, or evtArgs.ClickNo=true to click no.
        /// </summary>
        public event EventHandler<ConfirmationRequestEventArgs> OnConfirmationRequest;

        /// <summary>
        /// Fired once every second (ish).
        /// </summary>
        public event EventHandler<EventArgs> OnTick;

        /// <summary>
        /// Information about the physics environment around you
        /// </summary>
        public virtual PhysicsEnvironment PhysicsEnvironment { get; }

        /// <summary>
        /// Information about the current vendor
        /// </summary>
        public virtual Vendor Vendor { get; }

        /// <summary>
        /// The currently opened container, if any
        /// </summary>
        public virtual WorldObject OpenContainer { get; private set; } = null;

        /// <summary>
        /// A list of all weenies that the client knows about.
        /// </summary>
        public virtual IList<WorldObject> AllWeenies => _gameState.WorldState._weenies.Values.Select(w => w).ToList();

        /// <summary>
        /// A list of all landscape objects. (weenies that have no container)
        /// </summary>
        public virtual IList<WorldObject> LandscapeWeenies => _weenies.Values.Where(w => w.ContainerId == 0).ToList();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="logger"></param>
        public WorldState() {
            _log = ScriptManager.Instance.Resolve<ILogger>();
            _manager = ScriptManager.Instance;

            PhysicsEnvironment = new PhysicsEnvironment();
            Vendor = new Vendor();

            CharacterWeenie = GetOrCreateWeenie(_manager.GameState.CharacterId);

            CharacterWeenie.OnPositionChanged += CharacterWeenie_OnPositionChanged;

            _messageHandler.Incoming.Item_CreateObject += MessageHandler_Item_CreateObject_S2C;
            _messageHandler.Incoming.Item_DeleteObject += MessageHandler_Item_DeleteObject_S2C;
            _messageHandler.Incoming.Item_ObjDescEvent += MessageHandler_Item_ObjDescEvent_S2C;
            _messageHandler.Incoming.Item_OnViewContents += MessageHandler_Item_OnViewContents_S2C;
            _messageHandler.Incoming.Item_ParentEvent += MessageHandler_Item_ParentEvent_S2C;
            _messageHandler.Incoming.Item_ServerSaysContainID += MessageHandler_Item_ServerSaysContainID_S2C;
            _messageHandler.Incoming.Item_ServerSaysMoveItem += MessageHandler_Item_ServerSaysMoveItem_S2C;
            _messageHandler.Incoming.Item_ServerSaysRemove += MessageHandler_Item_ServerSaysRemove_S2C;
            _messageHandler.Incoming.Item_SetAppraiseInfo += MessageHandler_Item_SetAppraiseInfo_S2C;
            _messageHandler.Incoming.Item_SetState += MessageHandler_Item_SetState_S2C;
            _messageHandler.Incoming.Item_StopViewingObjectContents += MessageHandler_Item_StopViewingObjectContents_S2C;
            _messageHandler.Incoming.Item_UpdateObject += MessageHandler_Item_UpdateObject_S2C;
            _messageHandler.Incoming.Item_UpdateStackSize += MessageHandler_Item_UpdateStackSize_S2C;
            _messageHandler.Incoming.Item_WearItem += MessageHandler_Item_WearItem_S2C;

            _messageHandler.Incoming.Inventory_PickupEvent += _messageHandler_Inventory_PickupEvent_S2C;

            _messageHandler.Incoming.Qualities_RemoveBoolEvent += MessageHandler_Qualities_RemoveBoolEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveDataIDEvent_S2C += MessageHandler_Qualities_RemoveDataIDEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveFloatEvent += MessageHandler_Qualities_RemoveFloatEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveInstanceIDEvent += MessageHandler_Qualities_RemoveInstanceIDEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveInt64Event += MessageHandler_Qualities_RemoveInt64Event_S2C;
            _messageHandler.Incoming.Qualities_RemoveIntEvent += MessageHandler_Qualities_RemoveIntEvent_S2C;
            _messageHandler.Incoming.Qualities_RemovePositionEvent += MessageHandler_Qualities_RemovePositionEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveStringEvent += MessageHandler_Qualities_RemoveStringEvent_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttribute2ndLevel += MessageHandler_Qualities_UpdateAttribute2ndLevel_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttribute2nd += MessageHandler_Qualities_UpdateAttribute2nd_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttributeLevel += MessageHandler_Qualities_UpdateAttributeLevel_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttribute += MessageHandler_Qualities_UpdateAttribute_S2C;
            _messageHandler.Incoming.Qualities_UpdateBool += MessageHandler_Qualities_UpdateBool_S2C;
            _messageHandler.Incoming.Qualities_UpdateDataID += MessageHandler_Qualities_UpdateDataID_S2C;
            _messageHandler.Incoming.Qualities_UpdateFloat += MessageHandler_Qualities_UpdateFloat_S2C;
            _messageHandler.Incoming.Qualities_UpdateInstanceID += MessageHandler_Qualities_UpdateInstanceID_S2C;
            _messageHandler.Incoming.Qualities_UpdateInt64 += MessageHandler_Qualities_UpdateInt64_S2C;
            _messageHandler.Incoming.Qualities_UpdateInt += MessageHandler_Qualities_UpdateInt_S2C;
            _messageHandler.Incoming.Qualities_UpdatePosition += MessageHandler_Qualities_UpdatePosition_S2C;
            _messageHandler.Incoming.Qualities_UpdateSkillAC += MessageHandler_Qualities_UpdateSkillAC_S2C;
            _messageHandler.Incoming.Qualities_UpdateSkillLevel += MessageHandler_Qualities_UpdateSkillLevel_S2C;
            _messageHandler.Incoming.Qualities_UpdateSkill += MessageHandler_Qualities_UpdateSkill_S2C;
            _messageHandler.Incoming.Qualities_UpdateString += MessageHandler_Qualities_UpdateString_S2C;

            _messageHandler.Incoming.Login_PlayerDescription += MessageHandler_Login_PlayerDescription_S2C;

            _messageHandler.Incoming.Movement_PositionAndMovementEvent += MessageHandler_Movement_PositionAndMovementEvent_S2C;
            _messageHandler.Incoming.Movement_PositionEvent += MessageHandler_Movement_PositionEvent_S2C;
            _messageHandler.Incoming.Movement_SetObjectMovement += MessageHandler_Movement_SetObjectMovement_S2C;
            _messageHandler.Incoming.Movement_VectorUpdate += MessageHandler_Movement_VectorUpdate_S2C;

            _messageHandler.Incoming.Item_QueryItemManaResponse += MessageHandler_Item_QueryItemManaResponse_S2C;

            _messageHandler.Incoming.Communication_HearSpeech += Incoming_Communication_HearSpeech;
            _messageHandler.Incoming.Communication_ChannelBroadcast += Incoming_Communication_ChannelBroadcast;
            _messageHandler.Incoming.Communication_TurbineChat += Incoming_Communication_TurbineChat;
            _messageHandler.Incoming.Communication_ChannelList += Incoming_Communication_ChannelList;
            _messageHandler.Incoming.Communication_ChatRoomTracker += Incoming_Communication_ChatRoomTracker;
            _messageHandler.Incoming.Communication_HearDirectSpeech += Incoming_Communication_HearDirectSpeech;
            _messageHandler.Incoming.Communication_HearEmote += Incoming_Communication_HearEmote;
            _messageHandler.Incoming.Communication_HearRangedSpeech += Incoming_Communication_HearRangedSpeech;
            _messageHandler.Incoming.Communication_HearSoulEmote += Incoming_Communication_HearSoulEmote;
            _messageHandler.Incoming.Communication_PopUpString += Incoming_Communication_PopUpString;
            _messageHandler.Incoming.Communication_TextboxString += Incoming_Communication_TextboxString;
            _messageHandler.Incoming.Communication_TransientString += Incoming_Communication_TransientString;
            _messageHandler.Incoming.Communication_WeenieError += Incoming_Communication_WeenieError;
            _messageHandler.Incoming.Communication_WeenieErrorWithString += Incoming_Communication_WeenieErrorWithString;

        }

        #region Public API
        /// <summary>
        /// Get a weenie by id
        /// </summary>
        /// <param name="weenieId">The weenie id to get</param>
        /// <returns>the weenie, or null if it doesnt exist</returns>
        public WorldObject GetWeenie(uint weenieId) {
            if (_weenies.TryGetValue(weenieId, out WorldObject result)) {
                return result;
            }

            return null;
        }

        /// <summary>
        /// Try to get a weenie
        /// </summary>
        /// <param name="weenieId">The weenie id to get</param>
        /// <param name="weenie">The found weenie (or null if not found)</param>
        /// <returns>true if the weenie was found, false otherwise</returns>
        public bool TryGetWeenie(uint weenieId, out WorldObject weenie) {
            weenie = GetWeenie(weenieId);
            return weenie != null;
        }

        /// <summary>
        /// Check if a weenie exists
        /// </summary>
        /// <param name="weenieId"></param>
        /// <returns></returns>
        public bool WeenieExists(uint weenieId) {
            return _weenies.ContainsKey(weenieId);
        }

        public bool HandleChatInputText(string text) {
            var eventArgs = new ChatInputEventArgs(text);
            OnChatInput?.InvokeSafely(this, eventArgs);
            return eventArgs.Eat;
        }

        public void HandleObjectSelected(uint objectId) {
            OnObjectSelected?.InvokeSafely(this, new ObjectSelectedEventArgs(objectId));
        }

        public bool HandleChatNameClicked(uint id, string text) {
            var eventArgs = new ChatNameClickedEventArgs(id, text);
            OnChatNameClicked?.InvokeSafely(this, eventArgs);
            return eventArgs.Eat;
        }

        public ConfirmationRequestEventArgs HandleConfirmationPopup(ConfirmationRequestEventArgs eventArgs) {
            OnConfirmationRequest?.InvokeSafely(this, eventArgs);
            return eventArgs;
        }
        #endregion // Public API

        internal void Tick() {
            // todo: make a proper queue
            var allWeenies = _weenies.Values.ToList();
            foreach (var weenie in allWeenies) {
                if (weenie.WielderId == _gameState.CharacterId || weenie.ContainerId == _gameState.CharacterId)
                    continue;

                var parent = weenie.GetTopmostParent();
                bool isVisible = false;

                if (PhysicsEnvironment.IsLandcellVisible(parent.ServerPosition.Landcell)) {
                    weenie.LastAccessTime = DateTime.UtcNow;
                    isVisible = true;
                }

                if (!isVisible && DateTime.UtcNow - weenie.LastAccessTime > TimeSpan.FromSeconds(FORGET_OBJECT_TIME_SECONDS)) {
                    //_log.Log($"Removing 0x{weenie.Id:X8} {weenie.Name} because LastSeenTime > 25s and parent: 0x{parent.Id:X8} {parent.Name}");
                    RemoveWeenie(weenie.Id);
                }
            }

            OnTick?.Invoke(this, EventArgs.Empty);
        }

        internal WorldObject GetOrCreateWeenie(uint weenieId) {
            if (_weenies.TryGetValue(weenieId, out WorldObject result)) {
                result.LastAccessTime = DateTime.UtcNow;
                return result;
            }

            var weenie = new WorldObject(_log, _manager, _gameState);
            weenie.Id = weenieId;
            weenie.LastAccessTime = DateTime.UtcNow;
            weenie.CreatedAt = DateTime.UtcNow;

            _weenies.Add(weenieId, weenie);

            return weenie;
        }

        protected void RemoveWeenie(uint weenieId) {
            if (weenieId == _gameState.Character.Weenie.Id)
                return;

            if (_weenies.TryGetValue(weenieId, out WorldObject weenieToRemove)) {
                var visibility = "";
                /*
                if (PhysicsEnvironment.IsDungeon) {
                    visibility = $"(landcell: {weenieToRemove.Position.Landcell:X8}, visible cells: {string.Join(", ", PhysicsEnvironment.VisibleLandcells.Select(lb => $"{lb:X8}").ToArray())})";
                }
                else {
                    visibility = $"(landblock: {weenieToRemove.Position.Landcell:X8}, adjacent: {string.Join(", ", PhysicsEnvironment.VisibleLandblocks.Select(lb => $"{lb:X8}").ToArray())})";
                }

                _log.Log($"Scripts_RemoveWeenie: 0x{weenieToRemove.Id:X8} {weenieToRemove.Name} // {visibility}");
                */

                // try update containers / equipment lists of parent
                if (weenieToRemove.ContainerId != 0 && _weenies.TryGetValue(weenieToRemove.ContainerId, out WorldObject container)) {
                    switch (weenieToRemove.ContainerProperties) {
                        case ContainerProperties.None:
                            container.ItemIds.Remove(weenieToRemove.Id);
                            break;
                        case ContainerProperties.Container:
                        case ContainerProperties.Foci:
                            container.ContainerIds.Remove(weenieToRemove.Id);
                            break;
                    }
                }

                if (weenieToRemove.WielderId != 0 && _weenies.TryGetValue(weenieToRemove.WielderId, out WorldObject wielder)) {
                    wielder.EquipmentIds.Remove(weenieToRemove.Id);
                }
            }

            var eventArgs = new ObjectReleasedEventArgs(weenieId);
            GetWeenie(weenieId)?.Dispose(eventArgs);

            _weenies.Remove(weenieId);
            OnWeenieReleased?.Invoke(this, eventArgs);
        }

        private void CharacterWeenie_OnPositionChanged(object sender, ServerPositionChangedEventArgs e) {
            PhysicsEnvironment.SetLocation(e.Position);
        }

        private string DebugPrint(object obj) {
            var str = new StringBuilder();
            foreach (var field in obj.GetType().GetFields()) {
                str.AppendLine($"{field.Name} = {field.GetValue(obj)}");
            }

            return str.ToString();
        }

        private void UpdateChatRoom(ChatChannel room, uint id) {
            ChatRoomIdsLookup.AddOrUpdate(room, id);
            ChatRoomTypesLookup.AddOrUpdate(id, room);
        }

        #region Message Handlers

        private void Incoming_Communication_WeenieErrorWithString(object sender, Communication_WeenieErrorWithString_S2C_EventArgs e) {
            //UBService.UBService.WriteLog($"Incoming_Communication_WeenieErrorWithString: \n{DebugPrint(e.Data)}");
        }

        private void Incoming_Communication_WeenieError(object sender, Communication_WeenieError_S2C_EventArgs e) {
            //UBService.UBService.WriteLog($"Incoming_Communication_WeenieError: \n{DebugPrint(e.Data)}");
        }

        private void Incoming_Communication_TransientString(object sender, Communication_TransientString_S2C_EventArgs e) {
            //UBService.UBService.WriteLog($"Incoming_Communication_TransientString: \n{DebugPrint(e.Data)}");
        }

        private void Incoming_Communication_TextboxString(object sender, Communication_TextboxString_S2C_EventArgs e) {
            OnChatText?.Invoke(this, new ChatEventArgs(ChatChannel.None, e.Data.Type, 0, "", e.Data.Text));
        }

        private void Incoming_Communication_PopUpString(object sender, Communication_PopUpString_S2C_EventArgs e) {
            //UBService.UBService.WriteLog($"Incoming_Communication_PopUpString: \n{DebugPrint(e.Data)}");
        }

        private void Incoming_Communication_HearSoulEmote(object sender, Communication_HearSoulEmote_S2C_EventArgs e) {
            //UBService.UBService.WriteLog($"Incoming_Communication_HearSoulEmote: \n{DebugPrint(e.Data)}");
            var chatEvent = new ChatEventArgs(ChatChannel.None, ChatMessageType.Emote, e.Data.ObjectId, e.Data.Name, e.Data.Text);
            OnSoulEmote?.Invoke(this, chatEvent);
            OnChatText?.Invoke(this, chatEvent);
            EatNextChatMessage = chatEvent.Eat;
        }

        private void Incoming_Communication_HearRangedSpeech(object sender, Communication_HearRangedSpeech_S2C_EventArgs e) {
            //UBService.UBService.WriteLog($"Incoming_Communication_HearRangedSpeech: \n{DebugPrint(e.Data)}");
        }

        private void Incoming_Communication_HearEmote(object sender, Communication_HearEmote_S2C_EventArgs e) {
            var chatEvent = new ChatEventArgs(ChatChannel.None, ChatMessageType.Emote, e.Data.ObjectId, e.Data.Name, e.Data.Text);
            OnEmote?.Invoke(this, chatEvent);
            OnChatText?.Invoke(this, chatEvent);
            EatNextChatMessage = chatEvent.Eat;
        }

        private void Incoming_Communication_HearDirectSpeech(object sender, Communication_HearDirectSpeech_S2C_EventArgs e) {
            var chatEvent = new ChatEventArgs(ChatChannel.None, e.Data.Type, e.Data.ObjectId, e.Data.Name, e.Data.Message);
            OnTell?.Invoke(this, chatEvent);
            OnChatText?.Invoke(this, chatEvent);
            EatNextChatMessage = chatEvent.Eat;
        }

        private void Incoming_Communication_ChatRoomTracker(object sender, Communication_ChatRoomTracker_S2C_EventArgs e) {
            UpdateChatRoom(ChatChannel.Allegiance, e.Data.AllegianceRoomId);
            UpdateChatRoom(ChatChannel.CelestialHand, e.Data.SocietyCelHanChatRoomId);
            UpdateChatRoom(ChatChannel.EldrytchWeb, e.Data.SocietyEldWebChatRoomId);
            UpdateChatRoom(ChatChannel.General, e.Data.GeneralChatRoomId);
            UpdateChatRoom(ChatChannel.LFG, e.Data.LFGChatRoomId);
            UpdateChatRoom(ChatChannel.Olthoi, e.Data.OlthoiChatRoomId);
            UpdateChatRoom(ChatChannel.RadiantBlood, e.Data.SocietyRadBloChatRoomId);
            UpdateChatRoom(ChatChannel.Roleplay, e.Data.RoleplayChatRoomId);
            UpdateChatRoom(ChatChannel.Society, e.Data.SocietyChatRoomId);
            UpdateChatRoom(ChatChannel.Trade, e.Data.TradeChatRoomId);
        }

        private void Incoming_Communication_ChannelList(object sender, Communication_ChannelList_S2C_EventArgs e) {
            //UBService.UBService.WriteLog($"Incoming_Communication_ChannelList: \n{DebugPrint(e.Data)}");
        }

        private void Incoming_Communication_TurbineChat(object sender, Communication_TurbineChat_S2C_EventArgs e) {
            if (e.Data.Type != TurbineChatType.ServerToClientMessage)
                return;
            ChatChannel room = ChatChannel.None;
            ChatRoomTypesLookup.TryGetValue(e.Data.RoomId, out room);
            var chatEvent = new ChatEventArgs(room, ChatMessageType.Default, e.Data.ObjectId, e.Data.DisplayName, e.Data.Text);
            OnChannelMessage?.Invoke(this, chatEvent);
            OnChatText?.Invoke(this, chatEvent);
            EatNextChatMessage = chatEvent.Eat;
        }

        private void Incoming_Communication_ChannelBroadcast(object sender, Communication_ChannelBroadcast_S2C_EventArgs e) {
            if (e.Data.ChannelId == 2048) {
                var chatEvent = new ChatEventArgs(ChatChannel.None, ChatMessageType.Fellowship, 0, e.Data.Name, e.Data.Message);
                OnFellowMessage?.Invoke(this, chatEvent);
                OnChatText?.Invoke(this, chatEvent);
                EatNextChatMessage = chatEvent.Eat;
            }
            else {
                //UBService.UBService.WriteLog($"Incoming_Communication_ChannelBroadcast: \n{DebugPrint(e.Data)}");
            }
        }

        private void Incoming_Communication_HearSpeech(object sender, Communication_HearSpeech_S2C_EventArgs e) {
            var chatEvent = new ChatEventArgs(ChatChannel.None, e.Data.Type, e.Data.ObjectId, e.Data.Name, e.Data.Message);
            OnLocalMessage?.Invoke(this, chatEvent);
            OnChatText?.Invoke(this, chatEvent);
            EatNextChatMessage = chatEvent.Eat;
        }

        private void _messageHandler_Inventory_PickupEvent_S2C(object sender, Inventory_PickupEvent_S2C_EventArgs e) {
            // todo: do we need this?
            //_log.Log($"Got : _messageHandler_Inventory_PickupEvent_S2C on 0x{e.Data.objectID:X8}");
        }

        private void MessageHandler_Item_QueryItemManaResponse_S2C(object sender, Item_QueryItemManaResponse_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.LastAccessTime = DateTime.UtcNow;
        }

        private void MessageHandler_Movement_VectorUpdate_S2C(object sender, Movement_VectorUpdate_S2C_EventArgs e) {
            // todo: what is omega
            // todo: physics lol
        }

        private void MessageHandler_Movement_SetObjectMovement_S2C(object sender, Movement_SetObjectMovement_S2C_EventArgs e) {
            // todo: physics lol
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdatePosition(PositionPropertyID.Location, e.Data.MovementData);
        }

        private void MessageHandler_Movement_PositionEvent_S2C(object sender, Movement_PositionEvent_S2C_EventArgs e) {
            // todo: physics lol
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdatePosition(PositionPropertyID.Location, e.Data.Position);
        }

        private void MessageHandler_Movement_PositionAndMovementEvent_S2C(object sender, Movement_PositionAndMovementEvent_S2C_EventArgs e) {
            // todo: physics lol
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdatePosition(PositionPropertyID.Location, e.Data.Position);
            weenie.UpdatePosition(PositionPropertyID.Location, e.Data.MovementData);
        }

        private void MessageHandler_Login_PlayerDescription_S2C(object sender, Login_PlayerDescription_S2C_EventArgs e) {
            // make sure parent exists
            GetOrCreateWeenie(_gameState.CharacterId);

            foreach (var contentProfile in e.Data.ContentProfile.Items) {
                var weenie = GetOrCreateWeenie(contentProfile.Id);
                weenie.ContainerProperties = contentProfile.ContainerProperties;
                weenie.AddOrUpdateValue(InstanceId.Container, _gameState.CharacterId);
            }

            foreach (var invPlacement in e.Data.InventoryPlacement.Items) {
                var weenie = GetOrCreateWeenie(invPlacement.Id);
                weenie.AddOrUpdateValue(InstanceId.Wielder, _gameState.CharacterId);
                weenie.AddOrUpdateValue(IntId.CurrentWieldedLocation, (int)invPlacement.Location);
            }
        }

        private void MessageHandler_Qualities_UpdateString_S2C(object sender, Qualities_UpdateString_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateSkill_S2C(object sender, Qualities_UpdateSkill_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateSkill(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateSkillLevel_S2C(object sender, Qualities_UpdateSkillLevel_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            // todo: is this right?
            weenie.UpdateSkillPointsRaised(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateSkillAC_S2C(object sender, Qualities_UpdateSkillAC_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateSkillTraining(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdatePosition_S2C(object sender, Qualities_UpdatePosition_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);

            if (e.Data.Value == null || e.Data.Value.Frame == null)
                return;

            weenie.UpdatePosition(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateInt64_S2C(object sender, Qualities_UpdateInt64_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateInstanceID_S2C(object sender, Qualities_UpdateInstanceID_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateFloat_S2C(object sender, Qualities_UpdateFloat_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateDataID_S2C(object sender, Qualities_UpdateDataID_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateBool_S2C(object sender, Qualities_UpdateBool_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateAttribute_S2C(object sender, Qualities_UpdateAttribute_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateAttribute(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateAttributeLevel_S2C(object sender, Qualities_UpdateAttributeLevel_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            // todo: is this right?
            weenie.UpdateAttributePointsRaised(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateAttribute2nd_S2C(object sender, Qualities_UpdateAttribute2nd_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateVital(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_UpdateAttribute2ndLevel_S2C(object sender, Qualities_UpdateAttribute2ndLevel_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateVitalCurrent((VitalId)e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_RemoveStringEvent_S2C(object sender, Qualities_RemoveStringEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_RemovePositionEvent_S2C(object sender, Qualities_RemovePositionEvent_S2C_EventArgs e) {
            // todo position ?
        }

        private void MessageHandler_Qualities_RemoveIntEvent_S2C(object sender, Qualities_RemoveIntEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_RemoveInt64Event_S2C(object sender, Qualities_RemoveInt64Event_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_RemoveInstanceIDEvent_S2C(object sender, Qualities_RemoveInstanceIDEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_RemoveFloatEvent_S2C(object sender, Qualities_RemoveFloatEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_RemoveDataIDEvent_S2C(object sender, Qualities_RemoveDataIDEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_RemoveBoolEvent_S2C(object sender, Qualities_RemoveBoolEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_UpdateInt_S2C(object sender, Qualities_UpdateInt_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Item_WearItem_S2C(object sender, Item_WearItem_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(InstanceId.Container, 0);
            weenie.AddOrUpdateValue(InstanceId.Wielder, _gameState.CharacterId);
            weenie.AddOrUpdateValue(IntId.CurrentWieldedLocation, (int)e.Data.Slot);
        }

        private void MessageHandler_Item_UpdateStackSize_S2C(object sender, Item_UpdateStackSize_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(IntId.StackSize, (int)e.Data.Amount);
            weenie.AddOrUpdateValue(IntId.Value, (int)e.Data.NewValue);
        }

        private void MessageHandler_Item_UpdateObject_S2C(object sender, Item_UpdateObject_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateWeenieDesc(e.Data.WeenieDesc);
            weenie.UpdateObjDesc(e.Data.ObjectDesc);
            weenie.UpdatePhysicsDesc(e.Data.PhysicsDesc);
        }

        private void MessageHandler_Item_SetState_S2C(object sender, Item_SetState_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(IntId.PhysicsState, (int)e.Data.State);
        }

        private void MessageHandler_Item_SetAppraiseInfo_S2C(object sender, Item_SetAppraiseInfo_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateStatsTable(e.Data.BoolProperties);
            weenie.UpdateStatsTable(e.Data.FloatProperties);
            weenie.UpdateStatsTable(e.Data.Int64Properties);
            weenie.UpdateStatsTable(e.Data.IntProperties);
            weenie.UpdateStatsTable(e.Data.StringProperties);
            weenie.UpdateStatsTable(e.Data.DataProperties);
            weenie.UpdateSpells(e.Data.SpellBook);
            weenie.LastAppraisalResponse = DateTime.UtcNow;
            weenie.HasAppraisalData = true;
        }

        private void MessageHandler_Item_ServerSaysRemove_S2C(object sender, Item_ServerSaysRemove_S2C_EventArgs e) {
            RemoveWeenie(e.Data.ObjectId);
        }

        private void MessageHandler_Item_ServerSaysMoveItem_S2C(object sender, Item_ServerSaysMoveItem_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.AddOrUpdateValue(InstanceId.Container, 0);
        }

        private void MessageHandler_Item_ServerSaysContainID_S2C(object sender, Item_ServerSaysContainID_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.ContainerProperties = e.Data.ContainerProperties;
            weenie.AddOrUpdateValue(InstanceId.Container, e.Data.ContainerId);

            if (_gameState.WorldState._weenies.TryGetValue(e.Data.ContainerId, out WorldObject newContainer)) {
                switch (e.Data.ContainerProperties) {
                    case ContainerProperties.None:
                        newContainer.ItemIds.Remove(weenie.Id);
                        newContainer.ItemIds.Insert((int)e.Data.Slot, weenie.Id);
                        break;
                    case ContainerProperties.Foci:
                    case ContainerProperties.Container:
                        newContainer.ItemIds.Remove(weenie.Id);
                        newContainer.ContainerIds.Insert((int)e.Data.Slot, weenie.Id);
                        break;
                }
            }
        }

        private void MessageHandler_Item_ParentEvent_S2C(object sender, Item_ParentEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ChildId);
            weenie.AddOrUpdateValue(InstanceId.Wielder, e.Data.ParentId);
        }

        private void MessageHandler_Item_OnViewContents_S2C(object sender, Item_OnViewContents_S2C_EventArgs e) {
            var container = GetOrCreateWeenie(e.Data.ObjectId);
            var weeniesToWatchFor = new List<uint>();

            container.ItemIds.Clear();
            container.ContainerIds.Clear();

            foreach (var contentProfile in e.Data.Items.Items) {
                var weenie = GetOrCreateWeenie(contentProfile.Id);
                weenie.ContainerProperties = contentProfile.ContainerProperties;
                weenie.AddOrUpdateValue(InstanceId.Container, e.Data.ObjectId);
                weeniesToWatchFor.Add(weenie.Id);
            }

            if (container.Container?.Id != _gameState.CharacterId) {
                OpenContainer = container;

                if (weeniesToWatchFor.Count == 0) {
                    OnContainerOpened?.Invoke(this, new ContainerOpenedEventArgs(container));
                }
                else {
                    // wait until we get a weenie description for each of the container children before firing onContainerOpened
                    EventHandler<Item_CreateObject_S2C_EventArgs> handleCreateObject = null;
                    handleCreateObject = (s, m) => {
                        if (weeniesToWatchFor.Contains(m.Data.ObjectId)) {
                            weeniesToWatchFor.Remove(m.Data.ObjectId);

                            if (weeniesToWatchFor.Count == 0) {
                                _messageHandler.Incoming.Item_CreateObject -= handleCreateObject;
                                OnContainerOpened?.Invoke(this, new ContainerOpenedEventArgs(container));
                            }
                        }
                    };

                    _messageHandler.Incoming.Item_CreateObject += handleCreateObject;
                }
            }
        }

        private void MessageHandler_Item_StopViewingObjectContents_S2C(object sender, Item_StopViewingObjectContents_S2C_EventArgs e) {
            var container = GetOrCreateWeenie(e.Data.ObjectId);

            OpenContainer = null;
            OnContainerClosed?.Invoke(this, new ContainerClosedEventArgs(container));
        }

        private void MessageHandler_Item_ObjDescEvent_S2C(object sender, Item_ObjDescEvent_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateObjDesc(e.Data.ObjectDesc);
        }

        private void MessageHandler_Item_CreateObject_S2C(object sender, Item_CreateObject_S2C_EventArgs e) {
            var weenie = GetOrCreateWeenie(e.Data.ObjectId);
            weenie.UpdateObjDesc(e.Data.ObjectDesc);
            weenie.UpdatePhysicsDesc(e.Data.PhysicsDesc);
            weenie.UpdateWeenieDesc(e.Data.WeenieDesc);

            OnWeenieCreated?.Invoke(this, new ObjectCreatedEventArgs(weenie.Id));

            //_log.Log($"Updated 0x{weenie.Id:X8} {weenie.Name} // flags: {(PhysicsDescriptionFlag)e.Data.physicsdesc.flags} // wielder: {weenie.Wielder?.Name}");
        }

        private void MessageHandler_Item_DeleteObject_S2C(object sender, Item_DeleteObject_S2C_EventArgs e) {
            RemoveWeenie(e.Data.ObjectId);
        }
        #endregion // Message Handlers

        public virtual void Dispose() {
            Vendor?.Dispose();

            CharacterWeenie.OnPositionChanged -= CharacterWeenie_OnPositionChanged;

            _messageHandler.Incoming.Item_CreateObject -= MessageHandler_Item_CreateObject_S2C;
            _messageHandler.Incoming.Item_DeleteObject -= MessageHandler_Item_DeleteObject_S2C;
            _messageHandler.Incoming.Item_ObjDescEvent -= MessageHandler_Item_ObjDescEvent_S2C;
            _messageHandler.Incoming.Item_OnViewContents -= MessageHandler_Item_OnViewContents_S2C;
            _messageHandler.Incoming.Item_ParentEvent -= MessageHandler_Item_ParentEvent_S2C;
            _messageHandler.Incoming.Item_ServerSaysContainID -= MessageHandler_Item_ServerSaysContainID_S2C;
            _messageHandler.Incoming.Item_ServerSaysMoveItem -= MessageHandler_Item_ServerSaysMoveItem_S2C;
            _messageHandler.Incoming.Item_ServerSaysRemove -= MessageHandler_Item_ServerSaysRemove_S2C;
            _messageHandler.Incoming.Item_SetAppraiseInfo -= MessageHandler_Item_SetAppraiseInfo_S2C;
            _messageHandler.Incoming.Item_SetState -= MessageHandler_Item_SetState_S2C;
            _messageHandler.Incoming.Item_StopViewingObjectContents -= MessageHandler_Item_StopViewingObjectContents_S2C;
            _messageHandler.Incoming.Item_UpdateObject -= MessageHandler_Item_UpdateObject_S2C;
            _messageHandler.Incoming.Item_UpdateStackSize -= MessageHandler_Item_UpdateStackSize_S2C;
            _messageHandler.Incoming.Item_WearItem -= MessageHandler_Item_WearItem_S2C;

            _messageHandler.Incoming.Inventory_PickupEvent -= _messageHandler_Inventory_PickupEvent_S2C;

            _messageHandler.Incoming.Qualities_RemoveBoolEvent -= MessageHandler_Qualities_RemoveBoolEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveDataIDEvent_S2C -= MessageHandler_Qualities_RemoveDataIDEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveFloatEvent -= MessageHandler_Qualities_RemoveFloatEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveInstanceIDEvent -= MessageHandler_Qualities_RemoveInstanceIDEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveInt64Event -= MessageHandler_Qualities_RemoveInt64Event_S2C;
            _messageHandler.Incoming.Qualities_RemoveIntEvent -= MessageHandler_Qualities_RemoveIntEvent_S2C;
            _messageHandler.Incoming.Qualities_RemovePositionEvent -= MessageHandler_Qualities_RemovePositionEvent_S2C;
            _messageHandler.Incoming.Qualities_RemoveStringEvent -= MessageHandler_Qualities_RemoveStringEvent_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttribute2ndLevel -= MessageHandler_Qualities_UpdateAttribute2ndLevel_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttribute2nd -= MessageHandler_Qualities_UpdateAttribute2nd_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttributeLevel -= MessageHandler_Qualities_UpdateAttributeLevel_S2C;
            _messageHandler.Incoming.Qualities_UpdateAttribute -= MessageHandler_Qualities_UpdateAttribute_S2C;
            _messageHandler.Incoming.Qualities_UpdateBool -= MessageHandler_Qualities_UpdateBool_S2C;
            _messageHandler.Incoming.Qualities_UpdateDataID -= MessageHandler_Qualities_UpdateDataID_S2C;
            _messageHandler.Incoming.Qualities_UpdateFloat -= MessageHandler_Qualities_UpdateFloat_S2C;
            _messageHandler.Incoming.Qualities_UpdateInstanceID -= MessageHandler_Qualities_UpdateInstanceID_S2C;
            _messageHandler.Incoming.Qualities_UpdateInt64 -= MessageHandler_Qualities_UpdateInt64_S2C;
            _messageHandler.Incoming.Qualities_UpdateInt -= MessageHandler_Qualities_UpdateInt_S2C;
            _messageHandler.Incoming.Qualities_UpdatePosition -= MessageHandler_Qualities_UpdatePosition_S2C;
            _messageHandler.Incoming.Qualities_UpdateSkillAC -= MessageHandler_Qualities_UpdateSkillAC_S2C;
            _messageHandler.Incoming.Qualities_UpdateSkillLevel -= MessageHandler_Qualities_UpdateSkillLevel_S2C;
            _messageHandler.Incoming.Qualities_UpdateSkill -= MessageHandler_Qualities_UpdateSkill_S2C;
            _messageHandler.Incoming.Qualities_UpdateString -= MessageHandler_Qualities_UpdateString_S2C;

            _messageHandler.Incoming.Login_PlayerDescription -= MessageHandler_Login_PlayerDescription_S2C;

            _messageHandler.Incoming.Movement_PositionAndMovementEvent -= MessageHandler_Movement_PositionAndMovementEvent_S2C;
            _messageHandler.Incoming.Movement_PositionEvent -= MessageHandler_Movement_PositionEvent_S2C;
            _messageHandler.Incoming.Movement_SetObjectMovement -= MessageHandler_Movement_SetObjectMovement_S2C;
            _messageHandler.Incoming.Movement_VectorUpdate -= MessageHandler_Movement_VectorUpdate_S2C;

            _messageHandler.Incoming.Item_QueryItemManaResponse -= MessageHandler_Item_QueryItemManaResponse_S2C;

            _messageHandler.Incoming.Communication_HearSpeech -= Incoming_Communication_HearSpeech;
            _messageHandler.Incoming.Communication_ChannelBroadcast -= Incoming_Communication_ChannelBroadcast;
            _messageHandler.Incoming.Communication_TurbineChat -= Incoming_Communication_TurbineChat;
            _messageHandler.Incoming.Communication_ChannelList -= Incoming_Communication_ChannelList;
            _messageHandler.Incoming.Communication_ChatRoomTracker -= Incoming_Communication_ChatRoomTracker;
            _messageHandler.Incoming.Communication_HearDirectSpeech -= Incoming_Communication_HearDirectSpeech;
            _messageHandler.Incoming.Communication_HearEmote -= Incoming_Communication_HearEmote;
            _messageHandler.Incoming.Communication_HearRangedSpeech -= Incoming_Communication_HearRangedSpeech;
            _messageHandler.Incoming.Communication_HearSoulEmote -= Incoming_Communication_HearSoulEmote;
            _messageHandler.Incoming.Communication_PopUpString -= Incoming_Communication_PopUpString;
            _messageHandler.Incoming.Communication_TextboxString -= Incoming_Communication_TextboxString;
            _messageHandler.Incoming.Communication_TransientString -= Incoming_Communication_TransientString;
            _messageHandler.Incoming.Communication_WeenieError -= Incoming_Communication_WeenieError;
            _messageHandler.Incoming.Communication_WeenieErrorWithString -= Incoming_Communication_WeenieErrorWithString;
        }
    }
}