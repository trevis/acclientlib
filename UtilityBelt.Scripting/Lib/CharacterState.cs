﻿using ACE.DatLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilityBelt.Common;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Events;
using Enchantment = UtilityBelt.Scripting.Interop.Enchantment;
using UtilityBelt.Common.Messages;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Common.Messages.Events;
using UtilityBelt.Scripting.Interop;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Scripting.Lib {
    public class CharacterState : IDisposable {
        private const uint VITAE_SPELL_ID = 666;
        private GameState _gameState => _manager.GameState;
        private ScriptManager _manager;
        private ILogger _log;
        private Dictionary<LayeredSpellId, Enchantment> _enchantments = new Dictionary<LayeredSpellId, Enchantment>();
        private Dictionary<LayeredSpellId, SharedCooldown> _sharedCooldowns = new Dictionary<LayeredSpellId, SharedCooldown>();
        private ClientActions _clientActions;

        /// <summary>
        /// Fired when portal space is entered (not on login, so the first event is [OnPortalSpaceExited](xref:UBScript.Interfaces.WorldState#OnPortalSpaceExited))
        /// </summary>
        public event EventHandler<EventArgs> OnPortalSpaceEntered;

        /// <summary>
        /// Fired when exiting portal space. (this fires for the first time after logging in, while exiting login portal space)
        /// </summary>
        public event EventHandler<EventArgs> OnPortalSpaceExited;

        /// <summary>
        /// Fired when active enchantments change
        /// </summary>
        public event EventHandler<EnchantmentsChangedEventArgs> OnEnchantmentsChanged;

        /// <summary>
        /// Fired when object cooldowns change
        /// </summary>
        public event EventHandler<SharedCooldownsChangedEventArgs> OnSharedCooldownsChanged;

        /// <summary>
        /// Fired when one of your vitals current value changes.
        /// </summary>
        public event EventHandler<VitalChangedEventArgs> OnVitalChanged;

        /// <summary>
        /// Fired when your total experience changes
        /// </summary>
        public event EventHandler<TotalExperienceChangedEventArgs> OnTotalExperienceChanged;

        /// <summary>
        /// Fired when your total luminance
        /// </summary>
        public event EventHandler<AvailableLuminanceChangedEventArgs> OnAvailableLuminanceChanged;

        /// <summary>
        /// Fired when your character levels up
        /// </summary>
        public event EventHandler<LevelChangedEventArgs> OnLevelChanged;

        /// <summary>
        /// Fired when your character's vitae changes
        /// </summary>
        public event EventHandler<VitaeChangedEventArgs> OnVitaeChanged;

        /// <summary>
        /// Fired any time your character attempts to cast a spell
        /// </summary>
        public event EventHandler<CastSpellAttemptEventArgs> OnCastSpellAttempt;

        /// <summary>
        /// Fired when your character dies
        /// </summary>
        public event EventHandler<DeathEventArgs> OnDeath;

        /// <summary>
        /// The id of the currently logged in character
        /// </summary>
        public uint Id { get; }

        /// <summary>
        /// The weenie of the currently logged in character
        /// </summary>
        public WorldObject Weenie => _gameState.WorldState.CharacterWeenie;

        /// <summary>
        /// 1 = no vitea, 0.95 = 5% vitae
        /// </summary>
        public float Vitae { get; private set; } = 1.0f;

        /// <summary>
        /// Character Options
        /// </summary>
        public CharacterOptions1 Options1 { get; private set; }

        /// <summary>
        /// Information about the currently logged in character's spellbook
        /// </summary>
        public Interop.SpellBook SpellBook { get; }

        /// <summary>
        /// Information about the currently logged in character's fellowship
        /// </summary>
        public Interop.Fellowship Fellowship { get; }

        /// <summary>
        /// Information about the currently logged in character's trade state
        /// </summary>
        public Interop.Trade Trade { get; }

        /// <summary>
        /// Information about the current character's animations
        /// </summary>
        public Interop.AnimTracker AnimTracker { get; }

        /// <summary>
        /// Information about the busy state of the current character
        /// </summary>
        public Interop.BusyTracker BusyTracker { get; }

        /// <summary>
        /// Information about your character's allegiance
        /// </summary>
        public Allegiance Allegiance { get; }

        /// <summary>
        /// Wether or not the currently logged in character is in portal space
        /// </summary>
        public virtual bool InPortalSpace { get; private set; } = true; // portal space while logging in

        /// <summary>
        /// A list of *all* enchantments on this character. This includes enchantments that may be overriden by other enchantments.
        /// </summary>
        public IList<Enchantment> AllEnchantments => _enchantments.Values.ToList();

        /// <summary>
        /// A list of all shared object cooldowns on this character.
        /// </summary>
        public IList<SharedCooldown> SharedCooldowns => _sharedCooldowns.Values.ToList();

        // this ensures level 8 item self spells always take precedence over level 8 item other spells
        private static HashSet<uint> Level8AuraSelfSpells = new HashSet<uint>
        {
            (uint)SpellId.BloodDrinkerSelf8,
            (uint)SpellId.DefenderSelf8,
            (uint)SpellId.HeartSeekerSelf8,
            (uint)SpellId.SpiritDrinkerSelf8,
            (uint)SpellId.SwiftKillerSelf8,
            (uint)SpellId.HermeticLinkSelf8,
        };

        // from ACE
        private HashSet<uint> _setSpells = null;
        private HashSet<uint> SetSpells {
            get {
                if (_setSpells == null) {
                    _setSpells = new HashSet<uint>();
                    var spellTable = _manager.Resolve<IAsyncDatReader>().SpellTable;
                    foreach (var spellSet in spellTable.SpellSet.Values) {
                        foreach (var tier in spellSet.SpellSetTiers.Values) {
                            foreach (var spell in tier.Spells) {
                                // cutoff for enchantment manager bug fix sorting
                                if (spell >= (uint)SpellId.SetCoordination1)
                                    _setSpells.Add(spell);
                            }
                        }
                    }
                }

                return _setSpells;
            }
        }

        public CharacterState(uint characterId) {
            Id = characterId;
            _manager = ScriptManager.Instance;
            _log = _manager.Resolve<ILogger>();
            _clientActions = _manager.Resolve<ClientActions>(); ;
            SpellBook = _manager.Resolve<Interop.SpellBook>();
            Fellowship = _manager.Resolve<Interop.Fellowship>();
            Trade = _manager.Resolve<Interop.Trade>();
            AnimTracker = _manager.Resolve<AnimTracker>();
            BusyTracker = new BusyTracker(AnimTracker);
            Allegiance = _manager.Resolve<Allegiance>();

            Weenie.OnVitalChanged += Weenie_OnVitalChanged;

            _manager.MessageHandler.Incoming.Login_PlayerDescription += MessageHandler_Login_PlayerDescription_S2C;

            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveBoolEvent += MessageHandler_Qualities_PrivateRemoveBoolEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateBool += MessageHandler_Qualities_PrivateUpdateBool_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveIntEvent += MessageHandler_Qualities_PrivateRemoveIntEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateInt += MessageHandler_Qualities_PrivateUpdateInt_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveInt64Event += MessageHandler_Qualities_PrivateRemoveInt64Event_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateInt64 += MessageHandler_Qualities_PrivateUpdateInt64_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveFloatEvent += MessageHandler_Qualities_PrivateRemoveFloatEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateFloat += MessageHandler_Qualities_PrivateUpdateFloat_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveStringEvent += MessageHandler_Qualities_PrivateRemoveStringEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateString += MessageHandler_Qualities_PrivateUpdateString_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveInstanceIDEvent += MessageHandler_Qualities_PrivateRemoveInstanceIDEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateInstanceID += MessageHandler_Qualities_PrivateUpdateInstanceID_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveDataIDEvent += MessageHandler_Qualities_PrivateRemoveDataIDEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateDataID += MessageHandler_Qualities_PrivateUpdateDataID_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute2ndLevel += MessageHandler_Qualities_PrivateUpdateAttribute2ndLevel_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute2nd += MessageHandler_Qualities_PrivateUpdateAttribute2nd_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttributeLevel += MessageHandler_Qualities_PrivateUpdateAttributeLevel_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute += MessageHandler_Qualities_PrivateUpdateAttribute_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkillAC += MessageHandler_Qualities_PrivateUpdateSkillAC_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkillLevel += MessageHandler_Qualities_PrivateUpdateSkillLevel_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkill += MessageHandler_Qualities_PrivateUpdateSkill_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemovePositionEvent += MessageHandler_Qualities_PrivateRemovePositionEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdatePosition += MessageHandler_Qualities_PrivateUpdatePosition_S2C;

            _manager.MessageHandler.Incoming.Magic_DispelEnchantment += MessageHandler_Magic_DispelEnchantment_S2C;
            _manager.MessageHandler.Incoming.Magic_DispelMultipleEnchantments += MessageHandler_Magic_DispelMultipleEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_PurgeBadEnchantments += MessageHandler_Magic_PurgeBadEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_PurgeEnchantments += MessageHandler_Magic_PurgeEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_RemoveEnchantment += MessageHandler_Magic_RemoveEnchantment_S2C;
            _manager.MessageHandler.Incoming.Magic_RemoveMultipleEnchantments += MessageHandler_Magic_RemoveMultipleEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_UpdateEnchantment += MessageHandler_Magic_UpdateEnchantment_S2C;
            _manager.MessageHandler.Incoming.Magic_UpdateMultipleEnchantments += MessageHandler_Magic_UpdateMultipleEnchantments_S2C;

            _manager.MessageHandler.Incoming.Effects_PlayerTeleport += _messageHandler_Effects_PlayerTeleport_S2C;
            _manager.MessageHandler.Incoming.Item_SetState += MessageHandler_Item_SetState_S2C;

            _manager.MessageHandler.Outgoing.Magic_CastTargetedSpell += Outgoing_Magic_CastTargetedSpell;
            _manager.MessageHandler.Outgoing.Magic_CastUntargetedSpell += Outgoing_Magic_CastUntargetedSpell;
            _manager.MessageHandler.Incoming.Combat_HandlePlayerDeathEvent += Incoming_Combat_HandlePlayerDeathEvent;
        }

        private void Incoming_Combat_HandlePlayerDeathEvent(object sender, Combat_HandlePlayerDeathEvent_S2C_EventArgs e) {
            if (e.Data.ObjectId == Id) {
                OnDeath?.Invoke(this, new DeathEventArgs(e.Data.Message, e.Data.KillerId));
            }
        }

        #region Public API

        /// <summary>
        /// Checks if the client is busy performing actions like spellcasting / using/moving an item, etc.
        /// </summary>
        /// <returns>true if your character is busy, false otherwise</returns>
        public bool IsBusy() {
            return BusyTracker.CurrentAction?.BusyAction != Enums.BusyAction.None;
        }


        /// <summary>
        /// A list of active enchantments on this character. Does not include overlapping enchantments
        /// </summary>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> GetActiveEnchantments() {
            return AllEnchantments
                .GroupBy(enchantment => enchantment.Category)
                .Select(category => {
                    return category
                    .OrderByDescending(enchantment => enchantment.Power)
                    .ThenByDescending(enchantment => Level8AuraSelfSpells.Contains(enchantment.SpellId))
                    .ThenByDescending(c => SetSpells.Contains(c.SpellId) ? c.SpellId : c.StartTime)
                    .First();
                }).ToList();
        }

        /// <summary>
        /// Gets a list of active enchantments on this character that affect the specified skill.
        /// </summary>
        /// <param name="skillId">The skill id to filter by</param>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> GetActiveEnchantments(SkillId skillId) {
            return AllEnchantments
                .Where(enchantment => (enchantment.Flags & EnchantmentFlags.Skill) != 0 && enchantment.StatKey == (uint)skillId)
                .GroupBy(enchantment => enchantment.Category)
                .Select(category => {
                    return category
                    .OrderByDescending(enchantment => enchantment.Power)
                    .ThenByDescending(enchantment => Level8AuraSelfSpells.Contains(enchantment.SpellId))
                    .ThenByDescending(c => SetSpells.Contains(c.SpellId) ? c.SpellId : c.StartTime)
                    .First();
                }).ToList();
        }

        /// <summary>
        /// Gets a list of active enchantments on this character that affect the specified attribute
        /// </summary>
        /// <param name="attributeId">the attribute id to filter by</param>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> GetActiveEnchantments(AttributeId attributeId) {
            return AllEnchantments
                .Where(enchantment => (enchantment.Flags & EnchantmentFlags.Attribute) != 0 && enchantment.StatKey == (uint)attributeId)
                .GroupBy(enchantment => enchantment.Category)
                .Select(category => {
                    return category
                    .OrderByDescending(enchantment => enchantment.Power)
                    .ThenByDescending(enchantment => Level8AuraSelfSpells.Contains(enchantment.SpellId))
                    .ThenByDescending(c => SetSpells.Contains(c.SpellId) ? c.SpellId : c.StartTime)
                    .First();
                }).ToList();
        }

        /// <summary>
        /// Gets a list of active enchantments on this character that affect the specified vital
        /// </summary>
        /// <param name="vitalId">The vital id to filter by</param>
        /// <returns>A list of enchantments</returns>
        public IList<Enchantment> GetActiveEnchantments(VitalId vitalId) {
            return AllEnchantments
                .Where(enchantment => (enchantment.Flags & EnchantmentFlags.Attribute2nd) != 0 && enchantment.StatKey == (uint)vitalId)
                .GroupBy(enchantment => enchantment.Category)
                .Select(category => {
                    return category
                    .OrderByDescending(enchantment => enchantment.Power)
                    .ThenByDescending(enchantment => Level8AuraSelfSpells.Contains(enchantment.SpellId))
                    .ThenByDescending(c => SetSpells.Contains(c.SpellId) ? c.SpellId : c.StartTime)
                    .First();
                }).ToList();
        }

        /// <summary>
        /// Get the character's current skill level in the specified magic school based on the characters active enchantments
        /// </summary>
        /// <param name="school">The magic school</param>
        /// <returns></returns>
        public int GetMagicSkill(MagicSchool school) {
            Interop.Skill value = null;

            switch (school) {
                case MagicSchool.WarMagic:
                    if (Weenie.Skills.TryGetValue(SkillId.WarMagic, out value)) {
                        return value.Current;
                    }
                    return 0;
                case MagicSchool.LifeMagic:
                    if (Weenie.Skills.TryGetValue(SkillId.LifeMagic, out value)) {
                        return value.Current;
                    }
                    return 0;
                case MagicSchool.VoidMagic:
                    if (Weenie.Skills.TryGetValue(SkillId.VoidMagic, out value)) {
                        return value.Current;
                    }
                    return 0;
                case MagicSchool.CreatureEnchantment:
                    if (Weenie.Skills.TryGetValue(SkillId.CreatureEnchantment, out value)) {
                        return value.Current;
                    }
                    return 0;
                case MagicSchool.ItemEnchantment:
                    if (Weenie.Skills.TryGetValue(SkillId.ItemEnchantment, out value)) {
                        return value.Current;
                    }
                    return 0;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Get the enchantments additive modifier for the specified attribute based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int GetEnchantmentsAdditiveModifier(AttributeId type) {
            return GetActiveEnchantments(type).Where(e => (e.Flags & EnchantmentFlags.Additive) != 0).Sum(e => (int)e.StatValue);
        }

        /// <summary>
        /// Get the enchantments additive modifier for the specified skill based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int GetEnchantmentsAdditiveModifier(SkillId type) {
            return GetActiveEnchantments(type).Where(e => (e.Flags & EnchantmentFlags.Additive) != 0).Sum(e => (int)e.StatValue);
        }

        /// <summary>
        /// Get the enchantments additive modifier for the specified vital based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int GetEnchantmentsAdditiveModifier(VitalId type) {
            return GetActiveEnchantments(type).Where(e => (e.Flags & EnchantmentFlags.Additive) != 0).Sum(e => (int)e.StatValue);
        }

        /// <summary>
        /// Get the enchantments multiplier modifier for the specified attribute based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public float GetEnchantmentsMultiplierModifier(AttributeId type) {
            var multiplier = 1.0f;
            foreach (var enchantment in GetActiveEnchantments(type).Where(e => (e.Flags & EnchantmentFlags.Multiplicative) != 0))
                multiplier *= enchantment.StatValue;

            return multiplier;
        }

        /// <summary>
        /// Get the enchantments multiplier modifier for the specified skill based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public float GetEnchantmentsMultiplierModifier(SkillId type) {
            var multiplier = 1.0f;
            foreach (var enchantment in GetActiveEnchantments(type).Where(e => (e.Flags & EnchantmentFlags.Multiplicative) != 0))
                multiplier *= enchantment.StatValue;

            return multiplier;
        }

        /// <summary>
        /// Get the enchantments multiplier modifier for the specified vital based on the characters active enchantments
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public float GetEnchantmentsMultiplierModifier(VitalId type) {
            var multiplier = 1.0f;
            foreach (var enchantment in GetActiveEnchantments(type).Where(e => (e.Flags & EnchantmentFlags.Multiplicative) != 0))
                multiplier *= enchantment.StatValue;

            return multiplier;
        }
        #endregion // Public API

        internal void Tick() {
            ExpireEnchantments();
            ExpireSharedCooldowns();
        }

        private void SetVitae(float vitae) {
            if (Vitae != vitae) {
                var oldVitae = Vitae;
                Vitae = vitae;
                OnVitaeChanged?.Invoke(this, new VitaeChangedEventArgs(Vitae, oldVitae));
            }
        }

        private void ExpireEnchantments() {
            var enchantments = AllEnchantments.ToArray();
            foreach (var enchantment in enchantments) {
                if (enchantment.ExpiresAt <= DateTime.UtcNow) {
                    RemoveEnchantment(enchantment.LayeredId);
                }
            }
        }

        private void ExpireSharedCooldowns() {
            var cooldowns = SharedCooldowns.ToArray();
            foreach (var cooldown in cooldowns) {
                if (cooldown.ExpiresAt <= DateTime.UtcNow) {
                    RemoveEnchantment(cooldown.LayeredId);
                }
            }
        }

        private void ApplyEnchantment(Common.Messages.Types.Enchantment enchantment) {
            if (enchantment.Id.Id == VITAE_SPELL_ID) {
                SetVitae(enchantment.StatMod.Value);
                return;
            }

            if (enchantment.StatMod.Flags.HasFlag(EnchantmentFlags.Cooldown)) {
                var cooldown = SharedCooldown.FromMessage(enchantment);
                if (_sharedCooldowns.ContainsKey(enchantment.Id)) {
                    _sharedCooldowns[enchantment.Id] = cooldown;
                }
                else {
                    _sharedCooldowns.Add(enchantment.Id, cooldown);
                }
                OnSharedCooldownsChanged?.Invoke(this, new SharedCooldownsChangedEventArgs(Enums.AddRemoveEventType.Added, cooldown));
            }
            else {
                var en = Enchantment.FromMessage(enchantment);
                if (_enchantments.ContainsKey(enchantment.Id)) {
                    _enchantments[enchantment.Id] = en;
                }
                else {
                    _enchantments.Add(enchantment.Id, en);
                }
                OnEnchantmentsChanged?.Invoke(this, new EnchantmentsChangedEventArgs(Enums.AddRemoveEventType.Added, en));
            }
        }

        private void RemoveEnchantment(LayeredSpellId layeredSpellId) {
            if (layeredSpellId.Id == VITAE_SPELL_ID) {
                SetVitae(0);
                return;
            }

            if (_enchantments.TryGetValue(layeredSpellId, out Enchantment enchantment)) {
                _enchantments.Remove(enchantment.LayeredId);
                OnEnchantmentsChanged?.Invoke(this, new EnchantmentsChangedEventArgs(Enums.AddRemoveEventType.Removed, enchantment));
            }

            if (_sharedCooldowns.TryGetValue(layeredSpellId, out SharedCooldown cooldown)) {
                _sharedCooldowns.Remove(cooldown.LayeredId);
                OnSharedCooldownsChanged?.Invoke(this, new SharedCooldownsChangedEventArgs(Enums.AddRemoveEventType.Removed, cooldown));
            }
        }

        private void Outgoing_Magic_CastUntargetedSpell(object sender, Magic_CastUntargetedSpell_C2S_EventArgs e) {
            OnCastSpellAttempt?.Invoke(this, new CastSpellAttemptEventArgs(e.Data.SpellId.Id, 0, Enums.CastEventType.Untargetted));
        }

        private void Outgoing_Magic_CastTargetedSpell(object sender, Magic_CastTargetedSpell_C2S_EventArgs e) {
            OnCastSpellAttempt?.Invoke(this, new CastSpellAttemptEventArgs(e.Data.SpellId.Id, 0, Enums.CastEventType.Targetted));
        }

        private void MessageHandler_Item_SetState_S2C(object sender, Item_SetState_S2C_EventArgs e) {
            if (e.Data.ObjectId == Id && (e.Data.State & UtilityBelt.Common.Enums.PhysicsState.Hidden) == 0) {
                InPortalSpace = false;
                OnPortalSpaceExited?.Invoke(this, EventArgs.Empty);
            }
        }

        private void Weenie_OnVitalChanged(object sender, VitalChangedEventArgs e) {
            OnVitalChanged?.Invoke(this, e);
        }

        private void _messageHandler_Effects_PlayerTeleport_S2C(object sender, Effects_PlayerTeleport_S2C_EventArgs e) {
            InPortalSpace = true;
            OnPortalSpaceEntered?.Invoke(this, EventArgs.Empty);
        }

        private void MessageHandler_Login_PlayerDescription_S2C(object sender, Login_PlayerDescription_S2C_EventArgs e) {
            Options1 = e.Data.PlayerModule.options;

            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000001) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.IntProperties);
            }
            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000080) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.Int64Properties);
            }
            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000002) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.BoolProperties);
            }
            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000004) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.FloatProperties);
            }
            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000010) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.StringProperties);
            }
            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000040) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.DataProperties);
            }
            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000008) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.InstanceProperties);
            }
            if ((e.Data.Qualities.BaseQualities.Flags & 0x00000020) != 0) {
                Weenie.UpdateStatsTable(e.Data.Qualities.BaseQualities.PositionProperties);
            }

            if ((e.Data.Qualities.Flags & 0x00000001) != 0) {
                var attribCache = e.Data.Qualities.AttributeCache;

                Weenie.UpdateAttribute(AttributeId.Coordination, attribCache.Coordination);
                Weenie.UpdateAttribute(AttributeId.Endurance, attribCache.Endurance);
                Weenie.UpdateAttribute(AttributeId.Focus, attribCache.Focus);
                Weenie.UpdateAttribute(AttributeId.Quickness, attribCache.Quickness);
                Weenie.UpdateAttribute(AttributeId.Self, attribCache.Self);
                Weenie.UpdateAttribute(AttributeId.Strength, attribCache.Strength);

                Weenie.UpdateVital(VitalId.Health, attribCache.Health, true);
                Weenie.UpdateVital(VitalId.Mana, attribCache.Mana, true);
                Weenie.UpdateVital(VitalId.Stamina, attribCache.Stamina, true);
            }

            // has skills table
            if ((e.Data.Qualities.Flags & 0x00000002) != 0) {
                foreach (var kv in e.Data.Qualities.Skills.Table) {
                    Weenie.UpdateSkill(kv.Key, kv.Value);
                }
            }

            // has enchantment registry
            if ((e.Data.Qualities.Flags & 0x00000200) != 0) {
                var enchantmentRegistry = e.Data.Qualities.Enchantments;
                // has life enchantments
                if ((enchantmentRegistry.Flags & 0x0001) != 0) {
                    foreach (var enchantment in enchantmentRegistry.LifeSpells.Items) {
                        ApplyEnchantment(enchantment);
                    }
                }

                // has creature enchantments
                if ((enchantmentRegistry.Flags & 0x0002) != 0) {
                    foreach (var enchantment in enchantmentRegistry.CreatureSpells.Items) {
                        ApplyEnchantment(enchantment);
                    }
                }

                // has vitae
                if ((enchantmentRegistry.Flags & 0x0004) != 0) {
                    ApplyEnchantment(enchantmentRegistry.Vitae);
                }

                // has cooldowns
                if ((enchantmentRegistry.Flags & 0x0008) != 0) {
                    foreach (var enchantment in enchantmentRegistry.Cooldowns.Items) {
                        ApplyEnchantment(enchantment);
                    }
                }
            }
        }

        #region Enchantment Message Handlers
        private void MessageHandler_Magic_UpdateMultipleEnchantments_S2C(object sender, Magic_UpdateMultipleEnchantments_S2C_EventArgs e) {
            foreach (var enchantment in e.Data.Enchantments.Items) {
                ApplyEnchantment(enchantment);
            }
        }

        private void MessageHandler_Magic_UpdateEnchantment_S2C(object sender, Magic_UpdateEnchantment_S2C_EventArgs e) {
            ApplyEnchantment(e.Data.Enchantment);
        }

        private void MessageHandler_Magic_RemoveMultipleEnchantments_S2C(object sender, Magic_RemoveMultipleEnchantments_S2C_EventArgs e) {
            foreach (var enchantment in e.Data.Enchantments.Items) {
                RemoveEnchantment(enchantment);
            }
        }

        private void MessageHandler_Magic_RemoveEnchantment_S2C(object sender, Magic_RemoveEnchantment_S2C_EventArgs e) {
            RemoveEnchantment(e.Data.SpellId);
        }

        private void MessageHandler_Magic_PurgeEnchantments_S2C(object sender, Magic_PurgeEnchantments_S2C_EventArgs e) {
            var enchantments = AllEnchantments.ToArray();
            foreach (var enchantment in enchantments) {
                if (enchantment.Duration > 0) {
                    RemoveEnchantment(enchantment.LayeredId);
                }
            }
        }

        private void MessageHandler_Magic_PurgeBadEnchantments_S2C(object sender, Magic_PurgeBadEnchantments_S2C_EventArgs e) {
            var enchantments = AllEnchantments.ToArray();
            foreach (var enchantment in enchantments) {
                if (enchantment.Duration > 0 && enchantment.StatValue < 0) {
                    RemoveEnchantment(enchantment.LayeredId);
                }
            }
        }

        private void MessageHandler_Magic_DispelMultipleEnchantments_S2C(object sender, Magic_DispelMultipleEnchantments_S2C_EventArgs e) {
            foreach (var layeredId in e.Data.Enchantments.Items) {
                RemoveEnchantment(layeredId);
            }
        }

        private void MessageHandler_Magic_DispelEnchantment_S2C(object sender, Magic_DispelEnchantment_S2C_EventArgs e) {
            RemoveEnchantment(e.Data.SpellId);
        }
        #endregion // Enchantment Message Handlers

        #region Private Update Handlers (for your character)
        private void MessageHandler_Qualities_PrivateRemovePositionEvent_S2C(object sender, Qualities_PrivateRemovePositionEvent_S2C_EventArgs e) {
            /*
            CharacterWeenie._PhysicsDesc._Position.Landcell = 0;
            CharacterWeenie._PhysicsDesc._Position._Frame._Origin.X = 0;
            CharacterWeenie._PhysicsDesc._Position._Frame._Origin.Y = 0;
            CharacterWeenie._PhysicsDesc._Position._Frame._Origin.Z = 0;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.X = 0;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.Y = 0;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.Z = 0;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.W = 0;
            */
        }

        private void MessageHandler_Qualities_PrivateUpdateSkill_S2C(object sender, Qualities_PrivateUpdateSkill_S2C_EventArgs e) {
            Weenie.UpdateSkill(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateUpdateSkillLevel_S2C(object sender, Qualities_PrivateUpdateSkillLevel_S2C_EventArgs e) {
            Weenie.UpdateSkillPointsRaised(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateUpdateSkillAC_S2C(object sender, Qualities_PrivateUpdateSkillAC_S2C_EventArgs e) {
            Weenie.UpdateSkillTraining(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateUpdateAttribute_S2C(object sender, Qualities_PrivateUpdateAttribute_S2C_EventArgs e) {
            Weenie.UpdateAttribute(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateUpdateAttributeLevel_S2C(object sender, Qualities_PrivateUpdateAttributeLevel_S2C_EventArgs e) {
            Weenie.UpdateAttributePointsRaised(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateUpdateAttribute2nd_S2C(object sender, Qualities_PrivateUpdateAttribute2nd_S2C_EventArgs e) {
            Weenie.UpdateVital(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateUpdateAttribute2ndLevel_S2C(object sender, Qualities_PrivateUpdateAttribute2ndLevel_S2C_EventArgs e) {
            Weenie.UpdateVitalCurrent((VitalId)e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateUpdateDataID_S2C(object sender, Qualities_PrivateUpdateDataID_S2C_EventArgs e) {
            Weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateRemoveDataIDEvent_S2C(object sender, Qualities_PrivateRemoveDataIDEvent_S2C_EventArgs e) {
            Weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_PrivateUpdateInstanceID_S2C(object sender, Qualities_PrivateUpdateInstanceID_S2C_EventArgs e) {
            Weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateRemoveInstanceIDEvent_S2C(object sender, Qualities_PrivateRemoveInstanceIDEvent_S2C_EventArgs e) {
            Weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_PrivateUpdateString_S2C(object sender, Qualities_PrivateUpdateString_S2C_EventArgs e) {
            Weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateRemoveStringEvent_S2C(object sender, Qualities_PrivateRemoveStringEvent_S2C_EventArgs e) {
            Weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_PrivateUpdateFloat_S2C(object sender, Qualities_PrivateUpdateFloat_S2C_EventArgs e) {
            Weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateRemoveFloatEvent_S2C(object sender, Qualities_PrivateRemoveFloatEvent_S2C_EventArgs e) {
            Weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_PrivateUpdateInt64_S2C(object sender, Qualities_PrivateUpdateInt64_S2C_EventArgs e) {
            var oldValue = Weenie.Value(e.Data.Key, int.MinValue);
            Weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);

            if (oldValue == int.MinValue)
                return;
            
            switch (e.Data.Key) {
                case Int64Id.TotalExperience:
                    OnTotalExperienceChanged?.Invoke(this, new TotalExperienceChangedEventArgs(e.Data.Value, oldValue));
                    break;
                case Int64Id.AvailableLuminance:
                    OnAvailableLuminanceChanged?.Invoke(this, new AvailableLuminanceChangedEventArgs(e.Data.Value, oldValue));
                    break;
            }
        }

        private void MessageHandler_Qualities_PrivateRemoveInt64Event_S2C(object sender, Qualities_PrivateRemoveInt64Event_S2C_EventArgs e) {
            Weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_PrivateUpdateInt_S2C(object sender, Qualities_PrivateUpdateInt_S2C_EventArgs e) {
            var oldValue = Weenie.Value(e.Data.Key, int.MinValue);
            Weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);

            if (oldValue == int.MinValue)
                return;

            switch (e.Data.Key) {
                case IntId.Level:
                    OnLevelChanged?.Invoke(this, new LevelChangedEventArgs(e.Data.Value, oldValue));
                    break;
            }
        }

        private void MessageHandler_Qualities_PrivateRemoveIntEvent_S2C(object sender, Qualities_PrivateRemoveIntEvent_S2C_EventArgs e) {
            Weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_PrivateUpdateBool_S2C(object sender, Qualities_PrivateUpdateBool_S2C_EventArgs e) {
            Weenie.AddOrUpdateValue(e.Data.Key, e.Data.Value);
        }

        private void MessageHandler_Qualities_PrivateRemoveBoolEvent_S2C(object sender, Qualities_PrivateRemoveBoolEvent_S2C_EventArgs e) {
            Weenie.RemoveValue(e.Data.Type);
        }

        private void MessageHandler_Qualities_PrivateUpdatePosition_S2C(object sender, Qualities_PrivateUpdatePosition_S2C_EventArgs e) {
            _log.LogError($"Qualities_PrivateUpdatePosition_S2C: 0x{(ushort)e.Data.Key:X4} {e.Data.Key} // landcell: 0x{e.Data.Value.Landcell:X8}");
            /*
            CharacterWeenie._PhysicsDesc._Position.Landcell = e.Data.value.landcell;
            CharacterWeenie._PhysicsDesc._Position._Frame._Origin.X = e.Data.value.frame.origin.x;
            CharacterWeenie._PhysicsDesc._Position._Frame._Origin.Y = e.Data.value.frame.origin.y;
            CharacterWeenie._PhysicsDesc._Position._Frame._Origin.Z = e.Data.value.frame.origin.z;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.X = e.Data.value.frame.orientation.x;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.Y = e.Data.value.frame.orientation.y;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.Z = e.Data.value.frame.orientation.z;
            CharacterWeenie._PhysicsDesc._Position._Frame._Orientation.W = e.Data.value.frame.orientation.w;
            */
        }
        #endregion // Private Update Handlers

        public void Dispose() {
            Weenie.OnVitalChanged -= Weenie_OnVitalChanged;
            SpellBook?.Dispose();
            Fellowship?.Dispose();
            Trade?.Dispose();

            BusyTracker?.Dispose();
            AnimTracker?.Dispose();
            Allegiance?.Dispose();

            _manager.MessageHandler.Incoming.Login_PlayerDescription -= MessageHandler_Login_PlayerDescription_S2C;

            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveBoolEvent -= MessageHandler_Qualities_PrivateRemoveBoolEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateBool -= MessageHandler_Qualities_PrivateUpdateBool_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveIntEvent -= MessageHandler_Qualities_PrivateRemoveIntEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateInt -= MessageHandler_Qualities_PrivateUpdateInt_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveInt64Event -= MessageHandler_Qualities_PrivateRemoveInt64Event_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateInt64 -= MessageHandler_Qualities_PrivateUpdateInt64_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveFloatEvent -= MessageHandler_Qualities_PrivateRemoveFloatEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateFloat -= MessageHandler_Qualities_PrivateUpdateFloat_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveStringEvent -= MessageHandler_Qualities_PrivateRemoveStringEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateString -= MessageHandler_Qualities_PrivateUpdateString_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveInstanceIDEvent -= MessageHandler_Qualities_PrivateRemoveInstanceIDEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateInstanceID -= MessageHandler_Qualities_PrivateUpdateInstanceID_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemoveDataIDEvent -= MessageHandler_Qualities_PrivateRemoveDataIDEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateDataID -= MessageHandler_Qualities_PrivateUpdateDataID_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute2ndLevel -= MessageHandler_Qualities_PrivateUpdateAttribute2ndLevel_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute2nd -= MessageHandler_Qualities_PrivateUpdateAttribute2nd_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttributeLevel -= MessageHandler_Qualities_PrivateUpdateAttributeLevel_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateAttribute -= MessageHandler_Qualities_PrivateUpdateAttribute_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkillAC -= MessageHandler_Qualities_PrivateUpdateSkillAC_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkillLevel -= MessageHandler_Qualities_PrivateUpdateSkillLevel_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdateSkill -= MessageHandler_Qualities_PrivateUpdateSkill_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateRemovePositionEvent -= MessageHandler_Qualities_PrivateRemovePositionEvent_S2C;
            _manager.MessageHandler.Incoming.Qualities_PrivateUpdatePosition -= MessageHandler_Qualities_PrivateUpdatePosition_S2C;

            _manager.MessageHandler.Incoming.Magic_DispelEnchantment -= MessageHandler_Magic_DispelEnchantment_S2C;
            _manager.MessageHandler.Incoming.Magic_DispelMultipleEnchantments -= MessageHandler_Magic_DispelMultipleEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_PurgeBadEnchantments += MessageHandler_Magic_PurgeBadEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_PurgeEnchantments -= MessageHandler_Magic_PurgeEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_RemoveEnchantment -= MessageHandler_Magic_RemoveEnchantment_S2C;
            _manager.MessageHandler.Incoming.Magic_RemoveMultipleEnchantments -= MessageHandler_Magic_RemoveMultipleEnchantments_S2C;
            _manager.MessageHandler.Incoming.Magic_UpdateEnchantment -= MessageHandler_Magic_UpdateEnchantment_S2C;
            _manager.MessageHandler.Incoming.Magic_UpdateMultipleEnchantments -= MessageHandler_Magic_UpdateMultipleEnchantments_S2C;

            _manager.MessageHandler.Incoming.Effects_PlayerTeleport -= _messageHandler_Effects_PlayerTeleport_S2C;
            _manager.MessageHandler.Incoming.Item_SetState -= MessageHandler_Item_SetState_S2C;

            _manager.MessageHandler.Outgoing.Magic_CastTargetedSpell -= Outgoing_Magic_CastTargetedSpell;
            _manager.MessageHandler.Outgoing.Magic_CastUntargetedSpell -= Outgoing_Magic_CastUntargetedSpell;
        }
    }
}
