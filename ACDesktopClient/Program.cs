﻿using NAudio.Wave;
using OpenGlSdlClientBackend;

namespace ACDesktopClient {
    internal class Program {
        private static OpenGlSdlClient backend;

        static void Main(string[] args) {
            backend = new OpenGlSdlClient();
            backend.OnStarted += Backend_OnStarted;

            backend.Run();
        }

        private static void Backend_OnStarted(object? sender, EventArgs e) {
            backend.ACClient.RegisterAudioHandler(PlayAudioBuffer);
        }

        private async static void PlayAudioBuffer(uint id, byte[] data) {
            using (var stream = new MemoryStream(data))
            using (var audioFile = new WaveFileReader(stream))
            using (var outputDevice = new WaveOutEvent()) {
                outputDevice.Init(audioFile);
                outputDevice.Play();
                while (outputDevice.PlaybackState == PlaybackState.Playing) {
                    await Task.Delay(1);
                }
            }
        }
    }
}