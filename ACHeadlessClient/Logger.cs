﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ACHeadlessClient {

    public class Logger : ILogger {
        public LogLevel LogLevel { get; set; }

        public Logger(LogLevel logLevel) {
            LogLevel = logLevel;
        }
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {
            Log(formatter(state, exception), logLevel);
        }

        public bool IsEnabled(LogLevel logLevel) {
            return logLevel >= LogLevel;
        }

        private class LogScope : IDisposable {
            public void Dispose() {

            }
        }

        public IDisposable BeginScope<TState>(TState state) where TState : notnull {
            return new LogScope();
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        public void Log(Exception ex) {
            WriteLog(ex.ToString(), LogLevel.Error);
        }

        public void Print(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        private void WriteLog(string text, LogLevel level) {
            if (IsEnabled(level))
                Console.WriteLine($"{level}: {text}");
        }
    }
}
