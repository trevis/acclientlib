﻿using ACClientLib;
using ACClientLib.Lib;
using ACClientLib.Lib.Networking.Transports;
using CommandLine.Text;
using CommandLine;
using Newtonsoft.Json;
using UtilityBelt.Scripting.Enums;
using static System.Net.Mime.MediaTypeNames;
using Microsoft.Extensions.Logging;
using UtilityBelt.Common.Messages.Types;

namespace ACHeadlessClient {
    internal class Program {
        public class Options {
            [Option('u', "username", Required = false, HelpText = "User / Account name.", Default = "admin")]
            public string Username { get; set; }

            [Option('p', "password", Required = false, HelpText = "Password.", Default = "test")]
            public string Password { get; set; } = "";

            [Option('c', "character", Required = false, HelpText = "Login character.", Default = "Admin")]
            public string Character { get; set; } = "";

            [Option('h', "host", Required = false, HelpText = "Server host ip.", Default = "127.0.0.1")]
            public string Host { get; set; }

            [Option("port", Required = false, HelpText = "Server port.", Default = 9000)]
            public int Port { get; set; }

            [Option('d', "datdir", Required = false, HelpText = "Game dat directory.", Default = @"C:\ACE\Dats\")]
            public string DatDirectory { get; set; }


            [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
            public bool Verbose { get; set; }
        }

        public static ACClient ACClient { get; private set; }

        public static Options opts { get; private set; }

        static void Main(string[] args) {
            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed<Options>(o => {
                       opts = o;
                       ACClient = new ACClient(new ACClientOptions() {
                           DatReader = new FSDatReader(o.DatDirectory),
                           Logger = new Logger(o.Verbose ? LogLevel.Trace : LogLevel.Information),
                           NetTransport = new UDPTransport()
                       });

                       ACClient.OnStateChanged += ACClient_OnStateChanged;
                       ACClient.Connect(o.Host, o.Port, o.Username, o.Password, new CancellationToken());

                       while (true) {
                           Console.WriteLine($"Enter Chat Text:");
                           ACClient.InvokeChatParser(Console.ReadLine());
                       }
                   });
        }

        private static void ACClient_OnStateChanged(object? sender, UtilityBelt.Scripting.Events.StateChangedEventArgs e) {
            Console.WriteLine($"Game state changed to: {e.NewState}");
            switch (e.NewState) {
                case ClientState.Character_Select_Screen:
                    CharacterIdentity? loginChar = null;
                    Console.WriteLine("Characters: ");
                    for (var i = 0; i < ACClient.ScriptHost.Scripts.GameState.Characters.Count; i ++) {
                        var character = ACClient.ScriptHost.Scripts.GameState.Characters[i];
                        var isLoginChar = character.Name.Replace("+", "").ToLower().Equals(opts.Character.ToLower());
                        Console.WriteLine($"  - {character.Name} (0x{character.Id:X8}){(isLoginChar ? " (Auto Login)" : "")}");

                        if (isLoginChar) {
                            loginChar = character;
                        }
                    }

                    if (loginChar == null) {
                        Console.WriteLine($"Could not find login character \"{opts.Character}\"... ");
                    }
                    else {
                        Console.WriteLine($"Logging in as {loginChar.Name} (0x{loginChar.Id:X8})");
                        ACClient.EnterWorld(loginChar.Id);
                    }

                    break;

                case ClientState.In_Game:
                    ACClient.ScriptHost.Scripts.GlobalScriptContext.RunText("print('Hello from lua! I\\'m logged in as:', game.Character.Weenie.Name)");

                    ACClient.ScriptHost.Scripts.GameState.WorldState.OnChatText += (s, e) => {
                        Console.WriteLine($"[{e.Room}] {e.SenderName} says, \"{e.Message}\"");
                    };
                    break;
            }
        }
    }
}