function yieldingChunkLoop(count, chunksize, callback, finished) {
    var i = 0;
    (async function chunk() {
        var end = Math.min(i + chunksize, count);
        await callback.call(null, i, end);
        i = end;
        if (i < count) {
            setTimeout(chunk, 0);
        } else {
            finished.call(null);
        }
    })();
}

function range(size, startAt = 0) {
    return [...Array(size).keys()].map(i => i + startAt);
}

function importDatFiles(fileIds, storeName, db, silent) {
    const start = Date.now();
    let fileCount = 0;

    return new Promise((resolve, reject) => {
        yieldingChunkLoop(fileIds.length, 500, async function (i, max) {
            return new Promise((innerResolve, innerReject) => {
                const tx = db.transaction(storeName, 'readwrite');
                tx.oncomplete = () => {
                    innerResolve();
                }
                const store = tx.objectStore(storeName);

                let didTx = false;
                for (; i <= max; i++) {
                    var f = _dat.getFile(fileIds[i]);
                    if (!f || !f.data) {
                        continue;
                    }
                    didTx = true;
                    store.put(f.data, fileIds[i]);
                    fileCount++;
                }
                if (!didTx) {
                    innerResolve();
                }
            });
        }, function () {
            resolve(fileCount);
            if (silent != true)
                console.log(` - Cached ${fileCount} files to ${storeName} store in ${Date.now() - start} ms`);
        });
    });
}