import { DatManager } from './datmanager.js'


class Game {
    #canvas;
    #datManager;
    #server = "ws://localhost:3000";
    #showingDatWindow = false;

    constructor(canvas, wasmClient, jsClient) {
        this.WasmClient = wasmClient;
        this.JSClient = jsClient;
        this.#canvas = canvas;
        this.#datManager = new DatManager();
    }

    async init() {
        await this.JSClient.MountFSAsync("/persist");
        await this.#datManager.db;

        this.WasmClient.Init();
    }

    start() {
        this.#hideLoadingOverlay();
        if (this.#datManager.needsDats) {
            this.#showDatUploadWindow();
        }
    }

    renderFrame(time) {
        this.WasmClient.RenderFrame(time);
    }

    async loadPortalFile(fileId) {
        return await this.#datManager.loadPortalFile(fileId);
    }

    #showDatUploadWindow() {
        const datUploadWindow = document.getElementById('dat-upload-overlay');

        datUploadWindow.style.display = 'block';

        const updateDatLines = () => {
            if (this.#showingDatWindow) {
                var pStatus = document.getElementById('portal-dat-status');
                (this.#datManager.hasPortalDat ? pStatus.classList.add('good') : pStatus.classList.remove('good'));

                var cStatus = document.getElementById('cell-dat-status');
                (this.#datManager.hasCellDat ? cStatus.classList.add('good') : cStatus.classList.remove('good'));

                var lStatus = document.getElementById('language-dat-status');
                (this.#datManager.hasLanguageDat ? lStatus.classList.add('good') : lStatus.classList.remove('good'));

                if (!this.#datManager.needsDats()) {
                    datUploadWindow.style.display = 'none';
                    this.#datManager.removeEventListener('dats', updateDatLines)
                }
            }
        };

        this.#showingDatWindow = true;
        this.#datManager.addEventListener('dats', updateDatLines)

        updateDatLines();
    }

    #hideLoadingOverlay() {
        var fadeTarget = document.getElementById("load-overlay")
        var fadeEffect = setInterval(function () {
            if (!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1;
            }
            if (fadeTarget.style.opacity > 0) {
                fadeTarget.style.opacity -= 0.05;
            } else {
                clearInterval(fadeEffect);
                fadeTarget.style['display'] = 'none';
            }
        }, 20);
    }
}

export { Game }