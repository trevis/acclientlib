const DB_VERSION = 2;


function isMobile() {
    return (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i));
}

class DatManager extends EventTarget {
    hasPortalDat = false;
    hasCellDat = false;
    hasLanguageDat = false;

    constructor() {
        super();

        this.portalWorker = new Worker("portaldatloader.js");
        this.cellWorker = new Worker("celldatloader.js");
        this.languageWorker = new Worker("languagedatloader.js");

        this.#setupDragDrop();

        this.#initWorker(this.portalWorker, 'hasPortalDat');
        this.#initWorker(this.cellWorker, 'hasCellDat');
        this.#initWorker(this.languageWorker, 'hasLanguageDat');
    }

    #initWorker(worker, loadedProp) {
        const $this = this;
        worker.addEventListener('message', function (e) {
            switch (e.data.cmd) {
                case 'loaded':
                    $this[loadedProp] = true;
                    $this.dispatchEvent(new Event('dats'));
                    break;
            }
        });
    }

    needsDats() {
        return !(this.hasCellDat && this.hasLanguageDat && this.hasPortalDat);
    }

    async loadPortalFile(fileId) {
        return this.loadWorkerFile(fileId, this.portalWorker);
    }

    async loadCellFile(fileId) {
        return this.loadWorkerFile(fileId, this.cellWorker);
    }

    async loadEnglishFile(fileId) {
        return this.loadWorkerFile(fileId, this.languageWorker);
    }

    async loadWorkerFile(fileId, worker) {
        const $this = this;

        return new Promise((resolve, reject) => {
            try {
                const messageHandler = function (e) {
                    switch (e.data.cmd) {
                        case 'file':
                            resolve(e.data.data);
                            worker.removeEventListener('message', messageHandler);
                            break;
                    }
                };
                worker.addEventListener('message', messageHandler);
                $this.#post(worker, "getfile", fileId);
            }
            catch (e) {
                reject(e);
            }
        });
    }

    #post(worker, cmd, id, data) {
        worker.postMessage({ cmd: cmd, id: id, data: data });
    }

    #load(worker, file) {
        const $this = this;

        return new Promise((resolve, reject) => {
            try {
                worker.addEventListener('message', function (e) {
                    switch (e.data.cmd) {
                        case 'loaded':
                            resolve();
                            break;
                    }
                });
                $this.#post(worker, 'open', 0, file);
            }
            catch (e) {
                reject(e);
            }
        });
    }

    #handleFile(item) {
        const $this = this;

        switch (item.name) {
            case "client_portal.dat":
                this.#handleDatFile(this.portalWorker, item)
                break;
            case "client_cell_1.dat":
                this.#handleDatFile(this.cellWorker, item)
                break;
            case "client_local_English.dat":
                this.#handleDatFile(this.languageWorker, item)
                break;
            default:
                console.log(`Unknown dat file: ${item.name}`);
                break;
        }
    }

    async #handleDatFile(worker, file) {
        const $this = this;

        console.log(`Loading ${file.name}`);

        return $this.#load(worker, file).then((res) => {
            console.log(`Finished loading ${file.name}`);
        }).catch((error) => {
            console.log(`Could not load ${file.name}`, error);
        });
    }

    #setupDragDrop() {
        const $this = this;
        const dropzone = document.body;

        document.getElementById('dat-file-input').addEventListener('change', function (e) {
            for (let i = 0; i < e.target.files.length; i++) {
                $this.#handleFile(e.target.files[i]);
            }
        });

        dropzone.ondrop = function (event) {
            event.preventDefault();
            this.className = "dropzone";

            for (let i = 0; i < event.dataTransfer.files.length; i++) {
                $this.#handleFile(event.dataTransfer.files[i]);
            }
        }

        dropzone.ondragover = function () {
            this.className = "dropzone dragover";
            return false;
        };

        dropzone.ondragleave = function () {
            this.className = "dropzone";
            return false;
        };
    }
}

export { DatManager }