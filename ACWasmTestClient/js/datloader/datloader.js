'use strict';

importScripts("archive.js");

///////////////////////////////////////////////////////////////////////////////

var _dat = null;
var _handlers = {};
var _opfsRoot = false;

function getFileAs(id, ctor) {
    var f = null;
    try {
        f = _dat.getFile(id);
        var t = ctor(f.getReader());
        return t;

    } catch (error) {
        console.log('failed to load', id.toHexStr(), f);
        throw error;
    }
}

function addHandler(cmd, cb) {
    _handlers[cmd] = cb;
}

function addFileHandler(cmd, ctor) {
    addHandler(cmd, (id, data) => {
        try {
            post(cmd, id, getFileAs(id, ctor));
        } catch (error) {
            console.log('file handler error', cmd, id, data);
        }
    });
}

function post(cmd, id, data) {
    //console.log('post', cmd, data);
    self.postMessage({ cmd: cmd, id: id, data: data });
}

async function saveDatFile(file) {
    const reader = new FileReader();

    reader.onload = async function (e) {
        _dat = new DatArchive(e.target.result);

        // notify load done
        post('loaded', null);

        const view = new DataView(e.target.result);


        if (_opfsRoot === false) {
            _opfsRoot = await navigator.storage.getDirectory()
        }

        const fileHandle = await _opfsRoot.getFileHandle(file.name, { create: true });
        const syncAccessHandle = await fileHandle.createSyncAccessHandle();

        syncAccessHandle.write(view);
        syncAccessHandle.flush();
        syncAccessHandle.close();
    };

    reader.onprogress = function (e) {
        // notify loading
        // e.lengthComputable
        // e.loaded
        // e.total
        // defered.notify(Math.round((e.loaded / e.total) * 100));
    };

    reader.onerror = function (e) {
        // notify error
    };

    reader.readAsArrayBuffer(file);
}

async function checkDatFile(fileName) {
    if (_opfsRoot === false) {
        _opfsRoot = await navigator.storage.getDirectory()
    }

    _opfsRoot.getFileHandle(fileName, { create: false }).then(async (fileHandle) => {
        const syncAccessHandle = await fileHandle.createSyncAccessHandle();
        const fileSize = syncAccessHandle.getSize();

        if (fileSize > 512) {
            const ar = new ArrayBuffer(fileSize);
            const buffer = new DataView(ar);

            syncAccessHandle.read(buffer, { at: 0 });
            _dat = new DatArchive(ar);

            // notify load done
            post('loaded', null);
        }
        syncAccessHandle.close();
    }).catch((err) => {
        console.error(err);
    })
}

addEventListener('message', function (e) {
    e = e.data;
    switch (e.cmd) {
        case 'open':
            saveDatFile(e.data);
            break;

        default:
            var cb = _handlers[e.cmd];
            if (cb) cb(e.id, e.data);
    }
});