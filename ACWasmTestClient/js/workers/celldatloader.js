importScripts("datloader.js");
importScripts("utils.js");

addHandler('getfile', getFile);
function getFile(id, data) {
    var f = _dat.getFile(id);
    self.postMessage({ cmd: 'file', id: id, data: (f && f.data ? f.data : false) });
}

checkDatFile('client_cell_1.dat');