// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

import { dotnet } from './dotnet.js'
import { Game } from './game.js'

const { setModuleImports, getAssemblyExports, getConfig } = await dotnet
    .withDiagnosticTracing(false)
    .withApplicationArgumentsFromQuery()
    .create();


const config = getConfig();
const exports = await getAssemblyExports(config.mainAssemblyName);

const _fileCache = {};

let socket = null;
let socketConnected = false;
let socketQueue = [];

const mImports = {
    canvas: canvas,
    log: (message) => {
        console.log(message);
    },
    SetStatusText: (text) => {
        //document.getElementById('out').innerHTML = text;
    },
    socket: {
        send: (port, data) => {
            return;
            if (socket == null) {
                console.log(`Connecting to server: ${host}:${port}`)

                // Create WebSocket connection.
                socket = new WebSocket(`ws://${host}:${port}`);

                // Connection opened
                socket.addEventListener("open", (event) => {
                    socketConnected = true;
                    socketQueue.forEach(q => {
                        socket.send(q);
                    })
                    //exports.MyClass.Start(document.getElementById("user").value, document.getElementById("pass").value);
                });

                // Listen for messages 
                socket.addEventListener("message", (event) => {
                    var fileReader = new FileReader();
                    fileReader.onload = function (evt) {
                        var ar = new Uint8Array(evt.target.result);
                        //exports.WasmClient.Receive(ar);
                    };

                    fileReader.readAsArrayBuffer(event.data);
                });
            }
            if (socketConnected) {
                socket.send(data);
            }
            else {
                socketQueue.push(data)
            }
        }
    },
    MountFS: (path) => {
        dotnet.instance.Module.FS.mkdir(path);
        dotnet.instance.Module.FS.mount(dotnet.instance.Module.FS.filesystems.IDBFS, {}, path);
        mImports.syncFS(true);
    },
    SyncFS: (populate) => {
        mImports.SyncFSAsync(populate);
    },
    MountFSAsync: (path) => {
        dotnet.instance.Module.FS.mkdir(path);
        dotnet.instance.Module.FS.mount(dotnet.instance.Module.FS.filesystems.IDBFS, {}, path);
        return mImports.SyncFSAsync(true);
    },
    SyncFSAsync: (populate) => {
        return new Promise((resolve, reject) => {
            dotnet.instance.Module.FS.syncfs(populate, (err) => {
                if (err) {
                    console.log('syncfs failed. Error:', err);
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    },
    fs: {
        isFileLoaded: (filename) => {
            return !!files[filename];
        }
    },
    start: () => {
        game.start();
    },
    run: () => {
        exports.WasmClient.StartClient();
    },
    SoundManager: {
        PlayAudio: (data) => {
            const x = new Audio(data);
            x.play();
        }
    },
    DatManager: {
        Portal: {
            PreLoadFile: async (fileId) => {
                const res = await game.loadPortalFile(fileId);

                _fileCache[fileId] = res;

                return true;
            },
            LoadFile: (fileId) => {
                const res = _fileCache[fileId];
                delete _fileCache[fileId];

                return res;
            }
        }
    }
};


setModuleImports('main.js', mImports);

if (navigator.storage && navigator.storage.persist) {
    navigator.storage.persist().then((persistent) => {
        if (persistent) {
            console.log("Storage will not be cleared except by explicit user action");
        } else {
            console.log("Storage may be cleared by the UA under storage pressure.");
        }
    });
}

const game = window.game = new Game(document.getElementById("canvas"), exports.WasmClient, mImports);


globalThis.getDotnetRuntime(0).Module.canvas = canvas;
globalThis.WasmClient = exports.WasmClient;

function cancelEvent(e) {
    e.preventDefault();
    e.stopPropagation();
}

//canvas.addEventListener("mousedown", cancelEvent);
//canvas.addEventListener("contextmenu", cancelEvent);

await game.init();

function resizeHandler() {
    canvas.style.width = `${window.innerWidth}px`;
    canvas.style.height = `${window.innerHeight}px`;
    exports.WasmClient.Resize(window.innerWidth, window.innerHeight);
}

globalThis.addEventListener("resize", resizeHandler);
resizeHandler();

function renderFrame(time) {
    game.renderFrame(time);
    window.requestAnimationFrame(renderFrame);
}

window.requestAnimationFrame(renderFrame);

await dotnet.run();