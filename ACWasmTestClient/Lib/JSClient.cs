﻿using System.Runtime.InteropServices.JavaScript;
using System.Threading.Tasks;

namespace ACWasmTestClient.Lib {
    public static partial class JSClient {

        [JSImport("log", "main.js")]
        internal static partial void Log(string message);

        [JSImport("start", "main.js")]
        internal static partial void GameStart();

        [JSImport("run", "main.js")]
        internal static partial void ClientStart();

        [JSImport("socket.send", "main.js")]
        internal static partial void Send(int port, byte[] data);

        [JSImport("DatManager.Portal.PreLoadFile", "main.js")]
        internal static partial Task<bool> PreLoadPortalFile(int fileId);

        [JSImport("DatManager.Portal.LoadFile", "main.js")]
        internal static partial byte[] LoadPortalFile(int fileId);

        [JSImport("SoundManager.PlayAudio", "main.js")]
        internal static partial void PlayAudio(string dataUrl);

        [JSImport("FetchLandblockFiles", "main.js")]
        public static partial void FetchLandblockFiles(int reqId, int lbId);

        [JSImport("MountFS", "main.js")]
        public static partial void MountFS(string path);

        [JSImport("SyncFS", "main.js")]
        public static partial void SyncFS(bool populate);
    }
}
