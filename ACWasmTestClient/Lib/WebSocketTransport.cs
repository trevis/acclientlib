﻿using ACClientLib.Lib.Networking;
using ACClientLib.Lib.Networking.Transports;
using System;
using System.Net;

namespace ACWasmTestClient.Lib {
    public class WebSocketTransport : INetworkTransport {
        public override event EventHandler<TransportDataEventArgs> OnData;

        public override void Send(ServerInfo si, byte[] data, int length, bool useRead) {
            JSClient.Send(useRead ? si.ReadAddress.Port : si.WriteAddress.Port, data);
        }

        public override void Start(string host, int port) {
            
        }

        public override void Update() {
            
        }

        public void Receive(IPEndPoint endpoint, byte[] data) {
            OnData?.Invoke(this, new TransportDataEventArgs(endpoint, data));
        }
    }
}
