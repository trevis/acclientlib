﻿using Microsoft.Extensions.Logging;
using System;

namespace ACWasmTestClient.Lib.Scripting {
    public class BrowserLogger : ILogger {
        public LogLevel LogLevel { get; set; }

        public BrowserLogger(LogLevel logLevel) {
            LogLevel = logLevel;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {
            Log(formatter(state, exception), logLevel);
        }

        public bool IsEnabled(LogLevel logLevel) {
            return logLevel >= LogLevel;
        }

        private class LogScope : IDisposable {
            public void Dispose() {

            }
        }

        public IDisposable BeginScope<TState>(TState state) where TState : notnull {
            return new LogScope();
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        public void Log(Exception ex) {
            WriteLog(ex.ToString(), LogLevel.Error);
        }

        public void Print(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        private void WriteLog(string text, LogLevel level) {
            if (IsEnabled(level))
                JSClient.Log($"{level}: {text}\n");
        }
    }
}
