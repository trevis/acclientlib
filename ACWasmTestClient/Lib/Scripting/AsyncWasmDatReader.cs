﻿using ACE.DatLoader.FileTypes;
using System.Collections.Generic;
using System;
using System.IO;
using UtilityBelt.Scripting.Interop;
using static ACE.DatLoader.FileTypes.SpellComponentsTable;
using Type = System.Type;
using System.Threading.Tasks;
using Environment = ACE.DatLoader.FileTypes.Environment;

namespace ACWasmTestClient.Lib.Scripting {
    public class AsyncWasmDatReader : IAsyncDatReader {
        private static Dictionary<Type, Func<uint, Task<object>>> _switch = new Dictionary<Type, Func<uint, Task<object>>> {
                    { typeof(CellLandblock), async (fileId) => await TryLoadCellFile<CellLandblock>(fileId) },
                    { typeof(LandblockInfo), async (fileId) => await TryLoadCellFile<LandblockInfo>(fileId) },

                    { typeof(GfxObj), async (fileId) => await TryLoadPortalFile<GfxObj>(fileId) },
                    { typeof(SetupModel), async (fileId) => await TryLoadPortalFile<SetupModel>(fileId) },
                    { typeof(Animation), async (fileId) => await TryLoadPortalFile<Animation>(fileId) },
                    { typeof(Palette), async (fileId) => await TryLoadPortalFile<Palette>(fileId) },
                    { typeof(SurfaceTexture), async (fileId) => await TryLoadPortalFile<SurfaceTexture>(fileId) },
                    { typeof(Texture), async (fileId) => await TryLoadPortalFile<Texture>(fileId) },
                    { typeof(MotionTable), async (fileId) => await TryLoadPortalFile<MotionTable>(fileId) },
                    { typeof(Wave), async (fileId) => await TryLoadPortalFile<Wave>(fileId) },
                    { typeof(Environment), async (fileId) => await TryLoadPortalFile<Environment>(fileId) },
                    { typeof(ChatPoseTable), async (fileId) => await TryLoadPortalFile<ChatPoseTable>(fileId) },
                    { typeof(BadData), async (fileId) => await TryLoadPortalFile<BadData>(fileId) },
                    { typeof(NameFilterTable), async (fileId) => await TryLoadPortalFile<NameFilterTable>(fileId) },
                    { typeof(PaletteSet), async (fileId) => await TryLoadPortalFile<PaletteSet>(fileId) },
                    { typeof(ClothingTable), async (fileId) => await TryLoadPortalFile<ClothingTable>(fileId) },
                    { typeof(GfxObjDegradeInfo), async (fileId) => await TryLoadPortalFile<GfxObjDegradeInfo>(fileId) },
                    { typeof(Scene), async (fileId) => await TryLoadPortalFile<Scene>(fileId) },
                    { typeof(RegionDesc), async (fileId) => await TryLoadPortalFile<RegionDesc>(fileId) },
                    { typeof(RenderTexture), async (fileId) => await TryLoadPortalFile<RenderTexture>(fileId) },
                    { typeof(SoundTable), async (fileId) => await TryLoadPortalFile<SoundTable>(fileId) },
                    { typeof(EnumMapper), async (fileId) => await TryLoadPortalFile<EnumMapper>(fileId) },
                    { typeof(DidMapper), async (fileId) => await TryLoadPortalFile<DidMapper>(fileId) },
                    { typeof(DualDidMapper), async (fileId) => await TryLoadPortalFile<DualDidMapper>(fileId) },
                    { typeof(ParticleEmitterInfo), async (fileId) => await TryLoadPortalFile<ParticleEmitterInfo>(fileId) },
                    { typeof(PhysicsScript), async (fileId) => await TryLoadPortalFile<PhysicsScript>(fileId) },
                    { typeof(PhysicsScriptTable), async (fileId) => await TryLoadPortalFile<PhysicsScriptTable>(fileId) },
                    { typeof(Font), async (fileId) => await TryLoadPortalFile<Font>(fileId) },
                    { typeof(SecondaryAttributeTable), async (fileId) => await TryLoadPortalFile<SecondaryAttributeTable>(fileId) },
                    { typeof(SkillTable), async (fileId) => await TryLoadPortalFile<SkillTable>(fileId) },
                    { typeof(SpellTable), async (fileId) => await TryLoadPortalFile<SpellTable>(fileId) },
                    { typeof(SpellComponentsTable), async (fileId) => await TryLoadPortalFile<SpellComponentsTable>(fileId) },
                    { typeof(XpTable), async (fileId) => await TryLoadPortalFile<XpTable>(fileId) },
                    { typeof(ContractTable), async (fileId) => await TryLoadPortalFile<ContractTable>(fileId) }
            };

        public SecondaryAttributeTable SecondaryAttributeTable { get; private set; }
        public CharGen CharGen { get; private set; }
        public SkillTable SkillTable { get; private set; }
        public SpellComponentsTable SpellComponentsTable { get; private set; }
        public SpellTable SpellTable { get; private set; }
        public XpTable XpTable { get; private set; }
        public DualDidMapper DualDidMapper { get; private set; }

        public async void Init() {
            //BadData = ReadFromDat<BadData>(BadData.FILE_ID);
            //ChatPoseTable = ReadFromDat<ChatPoseTable>(ChatPoseTable.FILE_ID);
            CharGen = await ReadFromDat<CharGen>(CharGen.FILE_ID);
            //ContractTable = ReadFromDat<ContractTable>(ContractTable.FILE_ID);
            //GeneratorTable = ReadFromDat<GeneratorTable>(GeneratorTable.FILE_ID);
            //NameFilterTable = ReadFromDat<NameFilterTable>(NameFilterTable.FILE_ID);
            //RegionDesc = ReadFromDat<RegionDesc>(RegionDesc.FILE_ID);
            SecondaryAttributeTable = await ReadFromDat<SecondaryAttributeTable>(SecondaryAttributeTable.FILE_ID);
            SkillTable = await ReadFromDat<SkillTable>(SkillTable.FILE_ID);
            SpellComponentsTable = await ReadFromDat<SpellComponentsTable>(SpellComponentsTable.FILE_ID);
            SpellTable = await ReadFromDat<SpellTable>(SpellTable.FILE_ID);
            //TabooTable = ReadFromDat<TabooTable>(TabooTable.FILE_ID);
            XpTable = await ReadFromDat<XpTable>(XpTable.FILE_ID);
            DualDidMapper = await ReadFromDat<DualDidMapper>(0x27000002);
        }

        private static async Task<T> TryLoadPortalFile<T>(uint fileId) where T : FileType, new() {
            try {
                if (await JSClient.PreLoadPortalFile((int)fileId)) {
                    var fileBytes = JSClient.LoadPortalFile((int)fileId);

                    if (fileBytes != null && fileBytes.Length > 0) {
                        T file = new();

                        using (var stream = new MemoryStream(fileBytes))
                        using (var reader = new BinaryReader(stream)) {
                            file.Unpack(reader);
                            JSClient.Log($"Loaded {file.GetType().Name}: 0x{file.Id:X8}");
                            return file;
                        }
                    }
                }
            }
            catch (Exception ex) {
                JSClient.Log(ex.ToString());
            }

            return null;
        }
        private static async Task<T> TryLoadCellFile<T>(uint fileId) where T : FileType, new() {
            try {
                if (await JSClient.PreLoadPortalFile((int)fileId)) {
                    var fileBytes = JSClient.LoadPortalFile((int)fileId);

                    if (fileBytes != null && fileBytes.Length > 0) {
                        T file = new();

                        using (var stream = new MemoryStream(fileBytes))
                        using (var reader = new BinaryReader(stream)) {
                            file.Unpack(reader);
                            JSClient.Log($"Loaded {file.GetType().Name}: 0x{file.Id:X8}");
                            return file;
                        }
                    }
                }
            }
            catch (Exception ex) {
                JSClient.Log(ex.ToString());
            }

            return null;
        }

        public async Task<T> ReadFromDat<T>(uint fileId) where T : FileType, new() {
            return await _switch[typeof(T)](fileId) as T;
        }
    }
}
