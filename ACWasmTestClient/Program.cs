using System;
using System.Runtime.InteropServices.JavaScript;
using System.Numerics;
using System.IO;
using System.Threading.Tasks;
using ACE.DatLoader.FileTypes;
using ACWasmTestClient.Lib.Scripting;
using ACWasmTestClient.Lib;
using OpenGlSdlClientBackend;
using ACDesktopClient.Lib;
using System.Net;

JSClient.Log("Hello, Browser!");

public partial class WasmClient {

    private static int _nextRequestId = 1;
    internal static bool IsLoading;
    internal static int CurrentRequest;
    internal static int TotalRequests;
    private static WebSocketTransport _wsTransport;
    private static OpenGlSdlClient backend;

    private static void Backend_OnStarted(object? sender, EventArgs e) {
        backend.ACClient.RegisterAudioHandler(PlayAudioBuffer);
    }

    private async static void PlayAudioBuffer(uint fileId, byte[] obj) {
        try {
            var datReader = new AsyncWasmDatReader();
            var wav = await datReader.ReadFromDat<Wave>((uint)fileId);

            if (wav == null) {
                return;
            }

            using (var stream = new MemoryStream()) {
                wav.ReadData(stream);

                // todo: load from stream.... lol
                // .... and unload

                File.WriteAllBytes($"/{fileId}.wav", stream.ToArray());

                var x = SDL2.SDL_mixer.Mix_LoadWAV($"/{fileId}.wav");

                SDL2.SDL_mixer.Mix_PlayChannel(-1, x, 0);
            }
        }
        catch (Exception ex) {
            ThrowError(ex);
        }
    }

    [JSExport]
    internal static void Init() {
        JSClient.Log("Starting");
        Directory.SetCurrentDirectory("/persist");

        SDL2.SDL_mixer.Mix_OpenAudio(SDL2.SDL_mixer.MIX_DEFAULT_FREQUENCY, SDL2.SDL_mixer.MIX_DEFAULT_FORMAT, 2, 2048);

        _wsTransport = new WebSocketTransport();
        backend = new OpenGlSdlClient(new AsyncWasmDatReader(), _wsTransport, false);
        backend.OnStarted += Backend_OnStarted;
        backend.OnAppStateChanged += Backend_OnAppStateChanged;

        JSClient.GameStart();

        //backend.Run();

    }

    private static void Backend_OnAppStateChanged(object sender, AppStateChangedEventArgs e) {
        if (e.NewState == AppState.CONNECTING) {
            JSClient.ClientStart();
        }
    }

    [JSExport]
    public static void StartClient() {
        Task.Run(() => {
            backend.StartClient();
        });
    }

    [JSExport]
    public async static void PlayWav(int fileId) {
        try {
            backend.ACClient.PlayAudio((uint)fileId);
        }
        catch (Exception ex) {
            ThrowError(ex);
        }
    }

    [JSExport]
    public static void Resize(int width, int height) {
        try {
            backend?.Renderer?.Resize(width, height);
        }
        catch (Exception ex) {
            ThrowError(ex);
        }
    }

    [JSExport]
    public static void RunLua(string code) {
        try {
            var res = backend?.ACClient?.ScriptHost?.Scripts?.GlobalScriptContext?.RunText(code);
            JSClient.Log($"Lua result: {res}");
        }
        catch (Exception ex) {
            ThrowError(ex);
        }
    }

    [JSExport]
    public static void RenderFrame(double time) {
        try {
            TrySyncFS();
            backend?.Update(time);
        }
        catch (Exception ex) {
            ThrowError(ex);
        }
    }

    private static DateTime _lastFSSync = DateTime.UtcNow;
    private static void TrySyncFS() {
        if (DateTime.UtcNow - _lastFSSync > TimeSpan.FromSeconds(5)) {
            _lastFSSync = DateTime.UtcNow;
            JSClient.SyncFS(false);
        }
    }

    internal static void ThrowError(Exception ex) {
        JSClient.Log($"Error: {ex}");
    }

    [JSExport]
    internal static void Receive(byte[] buffer) {
        _wsTransport?.Receive(null, buffer);
    }

    [JSExport]
    internal static void Say(string message) {
        backend?.ACClient?.InvokeChatParser(message);
    }
}
