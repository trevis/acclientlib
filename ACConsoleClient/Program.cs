﻿using ACClientLib;
using ACClientLib.Lib;
using ACClientLib.Lib.Networking.Transports;
using ACConsoleClient.Lib;
using ACConsoleClient.Views;
using CommandLine;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using Terminal.Gui;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;

namespace ACConsoleClient {
    internal class Program {
        private static Options opts;
        private static ChatView chatView;

        public class Options {
            [Option('u', "username", Required = false, HelpText = "User / Account name.", Default = "Admin")]
            public string Username { get; set; }

            [Option('p', "password", Required = false, HelpText = "Password.", Default = "test")]
            public string Password { get; set; } = "";

            [Option('c', "character", Required = false, HelpText = "Login character.", Default = "Admin")]
            public string Character { get; set; } = "";

            [Option('h', "host", Required = false, HelpText = "Server host ip.", Default = "127.0.0.1")]
            public string Host { get; set; }

            [Option("port", Required = false, HelpText = "Server port.", Default = 9000)]
            public int Port { get; set; }

            [Option('d', "datdir", Required = false, HelpText = "Game dat directory.", Default = @"C:\ACE\Dats\")]
            public string DatDirectory { get; set; }


            [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
            public bool Verbose { get; set; }
        }

        public static ACClient ACClient { get; private set; }

        static void Main(string[] args) {
            Parser.Default.ParseArguments<Options>(args)
                   .WithParsed<Options>(o => {
                       opts = o;

                       var logger = new Logger(o.Verbose ? LogLevel.Trace : LogLevel.Information);

                       var writer = new ConsoleWriter();
                       writer.WriteEvent += (s, e) => { };
                       writer.WriteLineEvent += (s, e) => {
                           if (!string.IsNullOrWhiteSpace(e?.Value)) {
                               chatView?.AddText(e.Value);
                           }
                       };
                       Console.SetOut(writer);

                       Application.Init();

                       ShowLogView();

                       ACClient = new ACClient(new ACClientOptions() {
                           DatReader = new FSDatReader(o.DatDirectory),
                           Logger = logger,
                           NetTransport = new UDPTransport()
                       });

                       if (!string.IsNullOrEmpty(opts.Username) && !string.IsNullOrEmpty(opts.Password)) {
                           ACClient.OnStateChanged += ACClient_OnStateChanged;
                           ACClient.Connect(o.Host, o.Port, opts.Username, opts.Password, new CancellationToken());
                       }
                       else {
                           ShowLogin();
                       }
                       Application.Run();

                       Application.Shutdown();
                   });
        }

        private static void ShowLogView() {
            chatView = new ChatView();

            chatView.OnChatInput += (s, e) => {
                ACClient.InvokeChatParser(e.Message);
            };

            Application.Top.Add(chatView);
        }

        public static void Log(string text) {
            chatView?.AddText(text);
        }

        private static void ShowLogin() {
            var loginView = new LoginView();

            loginView.OnLogin += (s, e) => {
                ACClient.OnStateChanged += ACClient_OnStateChanged;
                ACClient.Connect(opts.Host, opts.Port, loginView.Username, loginView.Password, new CancellationToken());

                Application.Top.Remove(loginView);
            };

            Application.Top.Add(loginView);
        }

        private static void ACClient_OnStateChanged(object? sender, StateChangedEventArgs e) {
            Console.WriteLine($"Game state changed to: {e.NewState}");
            switch (e.NewState) {
                case ClientState.Character_Select_Screen:
                    if (string.IsNullOrEmpty(opts.Character)) {
                        
                    }
                    else {
                        CharacterIdentity? loginChar = null;
                        Console.WriteLine("Characters: ");
                        for (var i = 0; i < ACClient.ScriptHost.Scripts.GameState.Characters.Count; i++) {
                            var character = ACClient.ScriptHost.Scripts.GameState.Characters[i];
                            var isLoginChar = character.Name.Replace("+", "").ToLower().Equals(opts.Character.ToLower());
                            Console.WriteLine($"  - {character.Name} (0x{character.Id:X8}){(isLoginChar ? " (Auto Login)" : "")}");

                            if (isLoginChar) {
                                loginChar = character;
                            }
                        }

                        if (loginChar == null) {
                            Console.WriteLine($"Could not find login character \"{opts.Character}\"... ");
                        }
                        else {
                            Console.WriteLine($"Logging in as {loginChar.Name} (0x{loginChar.Id:X8})");
                            ACClient.EnterWorld(loginChar.Id);
                        }
                    }

                    break;

                case ClientState.In_Game:
                    ACClient.ScriptHost.Scripts.GlobalScriptContext.RunText("print('Hello from lua! I\\'m logged in as:', game.Character.Weenie.Name)");

                    ACClient.ScriptHost.Scripts.GameState.WorldState.OnChatText += (s, e) => {
                        Console.WriteLine($"[{e.Room}] {e.SenderName} says, \"{e.Message}\"");
                    };
                    break;
            }
        }
    }
}