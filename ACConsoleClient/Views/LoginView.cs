﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace ACConsoleClient.Views {
    public class LoginView : Window {
        private TextField usernameText;
        private TextField passwordText;

        public event EventHandler<EventArgs> OnLogin;

        public string Username => usernameText?.Text.ToString() ?? "";
        public string Password => passwordText?.Text.ToString() ?? "";

        public LoginView() {
            Title = "Login";

            // Create input components and labels
            var usernameLabel = new Label() {
                Y = 2,
                Text = "Username:"
            };

            usernameText = new TextField("") {
                // Position text field adjacent to the label
                X = Pos.Right(usernameLabel) + 1,
                Y = Pos.Top(usernameLabel),

                // Fill remaining horizontal space
                Width = Dim.Fill(),
            };

            var passwordLabel = new Label() {
                Text = "Password:",
                X = Pos.Left(usernameLabel),
                Y = Pos.Bottom(usernameLabel) + 1
            };

            passwordText = new TextField("") {
                Secret = true,
                // align with the text box above
                X = Pos.Left(usernameText),
                Y = Pos.Top(passwordLabel),
                Width = Dim.Fill(),
            };

            // Create login button
            var btnLogin = new Button() {
                Text = "Login",
                Y = Pos.Bottom(passwordLabel) + 1,
                // center the login button horizontally
                X = Pos.Center(),
                IsDefault = true,
            };

            // When login button is clicked display a message popup
            btnLogin.Clicked += () => {
                OnLogin?.Invoke(this, EventArgs.Empty);
            };

            // Add the views to the Window
            Add(usernameLabel, usernameText, passwordLabel, passwordText, btnLogin);
        }
    }
}
