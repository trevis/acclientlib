﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace ACConsoleClient.Views {
    public class ChatInputEventArgs : EventArgs {
        public string Message { get; }

        public ChatInputEventArgs(string message) {  Message = message; }
    }

    public class ChatView : Window {
        private ListView scrollView;

        public event EventHandler<ChatInputEventArgs> OnChatInput;
        public List<string> _chatTextBuffer = new List<string>();

        public ChatView() {
            Title = "Chat";


            scrollView = new ListView(_chatTextBuffer) {
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Fill() - 2
            };

            // Create input components and labels
            var chatLabel = new Label() {
                Y = Pos.Bottom(scrollView) + 1,
                Text = "Username:"
            };

            var chatText = new TextField("") {
                // Position text field adjacent to the label
                X = Pos.Right(chatLabel) + 1,
                Y = Pos.Top(chatLabel),

                // Fill remaining horizontal space
                Width = Dim.Fill() - 11,
            };

            var btnSend = new Button() {
                Text = "Send",
                Y = Pos.Bottom(scrollView) + 1,
                // center the login button horizontally
                X = Pos.Right(chatText) + 1,
                IsDefault = true,
            };

            // When login button is clicked display a message popup
            btnSend.Clicked += () => {
                var message = chatText.Text.ToString();

                if (!string.IsNullOrWhiteSpace(message)) {
                    AddText(message);
                    OnChatInput?.Invoke(this, new ChatInputEventArgs(message));
                    chatText.SetFocus();
                }

                chatText.Text = "";
            };

            // Add the views to the Window
            Add(scrollView, chatLabel, chatText, btnSend);
            chatText.SetFocus();
        }

        public void AddText(string message) {
            _chatTextBuffer.Add(message);
            Application.MainLoop.Invoke(() => {
                scrollView.GetCurrentHeight(out var height);
                var move = _chatTextBuffer.Count - height + 1;
                if (move >= 0 && height > 0) {
                    scrollView.MoveEnd();
                    scrollView.ScrollUp(height - 1);
                }
                SetNeedsDisplay();
            });
        }
    }
}
