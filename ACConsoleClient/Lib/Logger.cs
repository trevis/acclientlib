﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Unix.Terminal.Curses;
using Terminal.Gui;
using Microsoft.Extensions.Logging;

namespace ACConsoleClient.Lib {
    public class ConsoleWriterEventArgs : EventArgs {
        public string Value { get; private set; }
        public ConsoleWriterEventArgs(string value) {
            Value = value;
        }
    }
    public class ConsoleWriter : TextWriter {
        public override Encoding Encoding { get { return Encoding.UTF8; } }

        public override void Write(string value) {
            WriteEvent(this, new ConsoleWriterEventArgs(value));
            base.Write(value);
        }

        public override void WriteLine(string value) {
            if (WriteLineEvent != null) WriteLineEvent(this, new ConsoleWriterEventArgs(value));
            base.WriteLine(value);
        }

        public event EventHandler<ConsoleWriterEventArgs> WriteEvent;
        public event EventHandler<ConsoleWriterEventArgs> WriteLineEvent;
    }


    public class Logger : ILogger {
        public LogLevel LogLevel { get; set; }

        public Logger(LogLevel logLevel) {
            LogLevel = logLevel;
        }
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {
            Log(formatter(state, exception), logLevel);
        }

        public bool IsEnabled(LogLevel logLevel) {
            return logLevel >= LogLevel;
        }

        private class LogScope : IDisposable {
            public void Dispose() {

            }
        }

        public IDisposable BeginScope<TState>(TState state) where TState : notnull {
            return new LogScope();
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        public void Log(Exception ex) {
            WriteLog(ex.ToString(), LogLevel.Error);
        }

        public void Print(string message, LogLevel logLevel = LogLevel.Information) {
            WriteLog(message, logLevel);
        }

        private void WriteLog(string text, LogLevel level) {
            if (IsEnabled(level))
                Program.Log($"{level}: {text}");
        }
    }
}
