﻿# 🎵 AC Alternative Client Demos

Welcome to the AC Alternative Client Demos repository! This collection showcases a range of proof-of-concept alternative AC clients and supporting projects that provide additional functionality and flexibility to your AC server interactions.

## 📚 ACClientLib

ACClientLib is a powerful library designed to enhance your connection and interaction capabilities with AC servers. It provides a comprehensive set of features and functions that allow seamless integration with AC servers.

## ⌨️ ACConsoleClient

ACConsoleClient is a versatile cross-platform console client developed using the dotnet core framework. It offers a highly portable solution, running smoothly on various operating systems such as macOS, Windows, and Linux. It uses Terminal.Gui for the user interface.

## 🖥️ ACDesktopClient

ACDesktopClient is a feature-rich cross-platform desktop client built using the OpenGL and SDL2 libraries. Leveraging the dotnet core framework, this client can operate on multiple platforms, including macOS, Windows, and Linux. Its powerful graphical capabilities provide a visually appealing and immersive user experience.

## ⌨️ ACHeadlessClient

ACConsoleClient is a versatile cross-platform console client developed using the dotnet core framework. It offers a highly portable solution, running smoothly on various operating systems such as macOS, Windows, and Linux. It provides no user interface, and is meant to be run in the background.

## 🌐 ACWasmTestClient

ACWasmTestClient introduces a groundbreaking web assembly client that utilizes OpenGL and SDL2. This client is compatible with major web browsers, including those on mobile devices. It showcases a custom INetworkTransport implementation integrated into ACClientLib, enabling connections to servers via websockets instead of UDP. This innovation opens up exciting possibilities for web-based AC client development.

## 🔌 ACE.Mods.WebSockets

ACE.Mods.WebSockets is an essential addition that empowers ACE servers with websocket support. By incorporating this module, clients running in web browsers can directly connect to the server. This seamless integration ensures a smooth and efficient experience for web-based AC clients.

## 🌉 ACWebBridge

ACWebBridge serves as a local UDP-to-websocket bridge, enabling web clients to connect seamlessly to UDP-based AC servers. This bridge eliminates compatibility issues and facilitates direct communication between web clients and AC servers. It is a crucial component for creating a comprehensive and accessible AC ecosystem.

## 🙏 Thanks

A special thanks goes to the ACE team for their exceptional contributions to this project. Their hard work and dedication have made this collection of alternative AC clients and supporting projects possible.

Additionally, we extend our gratitude to Paradox for their invaluable contributions. Their JavaScript DAT file reader, utilized in the web assembly client, has significantly enhanced the functionality and performance of the client. Furthermore, Paradox's networking code has played a vital role in enabling seamless communication between all the clients and the server.

