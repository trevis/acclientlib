﻿using ACE.Common;
using ACE.Server.Managers;
using ACE.Server.Mods;
using ACE.Server.Network;
using ACE.Server.Network.Enum;
using ACE.Server.Network.Handlers;
using ACE.Server.Network.Managers;
using ACE.Server.Network.Packets;
using log4net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ACE.Mods.WebSockets {
    public static class WebSocketManager {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog packetLog = LogManager.GetLogger(System.Reflection.Assembly.GetEntryAssembly(), "Packets");

        // Hard coded server Id, this will need to change if we move to multi-process or multi-server model
        public static ushort ServerId => 0xB;

        private static WebApplication app;
        private static WebApplicationBuilder builder;
        private static FieldInfo? sessionMapField;
        private static FieldInfo? sessionLockField;

        public static async void Start() {
            sessionMapField = typeof(NetworkManager).GetField("sessionMap", BindingFlags.Static | BindingFlags.NonPublic);
            sessionLockField = typeof(NetworkManager).GetField("sessionLock", BindingFlags.Static | BindingFlags.NonPublic);

            builder = WebApplication.CreateBuilder();
            builder.WebHost.UseUrls("http://localhost:3000");
            app = builder.Build();
            app.UseWebSockets();
            app.Map("/", async context =>
            {
                if (context.WebSockets.IsWebSocketRequest) {
                    var endPoint = $"{context.Connection.RemoteIpAddress}:{context.Connection.RemotePort}";
                    using (var session = FindOrCreateSession(context, await context.WebSockets.AcceptWebSocketAsync())) {
                        if (session == null) {
                            log.Info($"Failed to create session for {endPoint}");
                            return;
                        }

                        var buffer = new byte[1024 * 10];

                        var timeOut = new CancellationTokenSource(30000).Token;
                        WebSocketReceiveResult result;
                        try {
                            result = await session.WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), timeOut);
                        }
                        catch (OperationCanceledException) {
                            return;
                        }

                        var packet = new ClientPacket();

                        if (packet.Unpack(buffer, result.Count)) {
                            //ServerPerformanceMonitor.RestartEvent(ServerPerformanceMonitor.MonitorType.ProcessPacket_1);
                            if (packet.Header.HasFlag(PacketHeaderFlags.LoginRequest)) {
                                if (NetworkManager.GetAuthenticatedSessionCount() >= ConfigManager.Config.Server.Network.MaximumAllowedSessions) {
                                    log.InfoFormat("Login Request from {0} rejected. Server full.", endPoint);
                                    session.SendLoginRequestReject(CharacterError.LogonServerFull);
                                }
                                else if (ServerManager.ShutdownInProgress) {
                                    log.InfoFormat("Login Request from {0} rejected. Server is shutting down.", endPoint);
                                    session.SendLoginRequestReject(CharacterError.ServerCrash1);
                                }
                                else if (ServerManager.ShutdownInitiated && (ServerManager.ShutdownTime - DateTime.UtcNow).TotalMinutes < 2) {
                                    log.InfoFormat("Login Request from {0} rejected. Server shutting down in less than 2 minutes.", endPoint);
                                    session.SendLoginRequestReject(CharacterError.ServerCrash1);
                                }
                                else {
                                    log.DebugFormat("Login Request from {0}", endPoint);

                                    var ipAllowsUnlimited = ConfigManager.Config.Server.Network.AllowUnlimitedSessionsFromIPAddresses.Contains(context.Connection.RemoteIpAddress.ToString());
                                    if (ipAllowsUnlimited || ConfigManager.Config.Server.Network.MaximumAllowedSessionsPerIPAddress == -1 || NetworkManager.GetSessionEndpointTotalByAddressCount(context.Connection.RemoteIpAddress) < ConfigManager.Config.Server.Network.MaximumAllowedSessionsPerIPAddress) {

                                        if (session.State == SessionState.AuthConnectResponse) {
                                            // connect request packet sent to the client was corrupted in transit and session entered an unspecified state.
                                            // ignore the request and remove the broken session and the client will start a new session.
                                            NetworkManager.RemoveSession(session);
                                            log.Warn($"Bad handshake from {endPoint}, aborting session.");
                                        }

                                        session.ProcessPacket(packet);
                                    }
                                    else {
                                        log.InfoFormat("Login Request from {0} rejected. Session would exceed MaximumAllowedSessionsPerIPAddress limit.", endPoint);
                                        session.SendLoginRequestReject(CharacterError.LogonServerFull);
                                    }
                                }
                            }
                            else {
                                log.Info($"Unsolicited Packet {packet}, from {context.Connection.RemoteIpAddress}:{context.Connection.RemotePort}");
                            }
                        }
                        else {
                            log.Info($"Failed to unpack packet: {context.Connection.RemoteIpAddress}:{context.Connection.RemotePort} // {buffer}");
                        }

                        packet.ReleaseBuffer();

                        while (session.WebSocket.State == WebSocketState.Open || session.WebSocket.State == WebSocketState.Connecting) {
                            var timeOut2 = new CancellationTokenSource(30000).Token;
                            WebSocketReceiveResult result2;
                            try {
                                result2 = await session.WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), timeOut2);

                                var packet2 = new ClientPacket();

                                if (packet2.Unpack(buffer, result2.Count)) {

                                    if (packet2.Header.Flags.HasFlag(PacketHeaderFlags.ConnectResponse)) {
                                        packetLog.Debug($"{packet2}, {endPoint}");
                                        PacketInboundConnectResponse connectResponse = new PacketInboundConnectResponse(packet2);

                                        session.State = SessionState.AuthConnected;
                                        session.Network.sendResync = true;
                                        AuthenticationHandler.HandleConnectResponse(session);

                                    }
                                    else {
                                        session.ProcessPacket(packet2);
                                    }
                                }
                                packet2.ReleaseBuffer();
                            }
                            catch {
                                
                            }
                            await Task.Delay(1);
                        }
                        log.Info($"Drop Session: {context.Connection.RemoteIpAddress}:{context.Connection.RemotePort} // {session.WebSocket.State}");
                        session.Terminate(SessionTerminationReason.PacketHeaderDisconnect);
                        session.DropSession();
                    }
                }
                else {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
            });

            await app.RunAsync();
        }

        public static WebSocketSession? FindOrCreateSession(HttpContext context, WebSocket webSocket) {
            var sessionLock = sessionLockField?.GetValue(null) as ReaderWriterLockSlim;
            var sessionMap = sessionMapField?.GetValue(null) as Session[];

            if (sessionLock == null || sessionMap == null)
                return null;

            WebSocketSession? session = null;
            sessionLock.EnterUpgradeableReadLock();
            try {
                sessionLock.EnterWriteLock();
                try {
                    for (ushort i = 0; i < sessionMap.Length; i++) {
                        if (sessionMap[i] == null) {
                            log.Info($"Creating session for {context.Connection.RemoteIpAddress}:{context.Connection.RemotePort} with id {i}");
                            var endPoint = new IPEndPoint(context.Connection.RemoteIpAddress, context.Connection.RemotePort);
                            session = new WebSocketSession(webSocket, context, endPoint, i, ServerId);
                            sessionMap[i] = session;
                            break;
                        }
                    }
                }
                finally {
                    sessionLock.ExitWriteLock();
                }
            }
            finally {
                sessionLock.ExitUpgradeableReadLock();
            }

            return session;
        }

        public static async void Stop() {
            await app.StopAsync();
        }
    }
}
